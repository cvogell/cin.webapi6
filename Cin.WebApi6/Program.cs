using Cin.WebApi.Mapping;
using Cin.WebApi.Models;
using Cin.WebApi.OData;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.OData;

using ODataConventionModelBuilder = Microsoft.OData.ModelBuilder.ODataConventionModelBuilder;

var builder = WebApplication.CreateBuilder(args);

if (builder.Environment.IsDevelopment())
{
    builder.Logging.AddSimpleConsole(options =>
    {
        options.SingleLine = true;
        options.ColorBehavior = Microsoft.Extensions.Logging.Console.LoggerColorBehavior.Enabled;
        options.TimestampFormat = "hh:mm:ss:fff tt";
    });
}
else
{
    
    builder.Services.AddApplicationInsightsTelemetry(builder.Configuration["APPLICATIONINSIGHTS_CONNECTION_STRING"]);
    builder.Services.AddLogging(config =>
    {
        config.AddApplicationInsights();
        config.AddAzureWebAppDiagnostics();

    });
}

registerServices();
// Add services to the container.
builder.Services.AddAutoMapper(typeof(MapProfile));
builder.Services.AddMemoryCache();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
else
{
    
}

app.UseHttpsRedirection();
//app.UseAuthorization();
app.MapControllers();
if (app.Environment.IsDevelopment())
{
    app.MapGet("/debug/routes", (IEnumerable<EndpointDataSource> endpointSources) =>
        string.Join("\n", endpointSources.SelectMany(source => source.Endpoints)));
}
app.MapGet("/", () => { return; });
app.Run();

//https://learn.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-6.0
void registerServices()
{
    builder.Services.AddScoped<CrmRepo>();
    builder.Services.AddTransient<ODataReadHandler>();
    builder.Services.AddTransient<ODataWriteHandler<CustomAccount>>();
    builder.Services.AddControllers().AddOData(option =>
    {
        option.EnableAttributeRouting = true;
        option.EnableQueryFeatures();
        option.AddRouteComponents("odata", getBuilder().GetEdmModel());
    }).AddJsonOptions(o => {
        o.JsonSerializerOptions.PropertyNamingPolicy = null;
        o.JsonSerializerOptions.DictionaryKeyPolicy = null;
        o.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
    });
}

ODataConventionModelBuilder getBuilder()
{
    var modelBuilder = new ODataConventionModelBuilder();
    var odataVersion2 = new Version(2, 0);
    
    modelBuilder.EntitySet<CustomAccount>("Account").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomActivity>("Activity").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomAgreementTerm>("AgreementTerm").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomAgreementTermVariation>("AgreementTermVariation").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomAppointment>("Appointment").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomBusinessDivision>("BusinessDivision").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomCintasAgreementProcess>("CintasAgreementProcess").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomCintasLocation>("CintasLocation").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomCompetitorFS>("CompetitorFS").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomCompetitorUR>("CompetitorUR").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomContact>("Contact").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomLeadReferral>("LeadReferral").EntityType.Ignore(emp => emp.Metadata);
    modelBuilder.EntitySet<CustomMarkAsSold>("MarkAsSold").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomNacProcessing>("NacProcessing").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomPostalCode>("PostalCodeAlignment").EntityType.Ignore(emp => emp.__metadata);

    modelBuilder.EntitySet<CustomOpportunity>("Opportunity").EntityType.Ignore(emp => emp.__metadata);
    //opportunity.EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomOpportunityProduct>("OpportunityProduct").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomQuote>("Quote").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomQuoteDetail>("QuoteDetail").EntityType.Ignore(emp => emp.__metadata);
    //builder.EntitySet<CustomQuoteClose>("QuoteClose").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomWinQuote>("WinQuote").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomLoseQuote>("LoseQuote").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomCancelQuote>("CancelQuote").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomGlobalRentalSetting>("GlobalRentalSetting").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomSystemUser>("SystemUser").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomProduct>("Product").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomProductAssociation>("ProductAssociation").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomProductRelationship>("ProductRelationship").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomCustomerDetails>("CustomerDetails").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomProposalsAndAgreements>("ProposalsAndAgreements").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomOpportunityClose>("OpportunityClose").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomWinOpportunity>("WinOpportunity").EntityType.Ignore(emp => emp.__metadata);
    modelBuilder.EntitySet<CustomLoseOpportunity>("LoseOpportunity").EntityType.Ignore(emp => emp.__metadata);
    //builder.EntitySet<CustomAzureArchiveTable>("ArchiveToAzureTable").EntityType.Ignore(emp => emp.__metadata);

    modelBuilder.Namespace = "Cin.WebApi.Models";

    modelBuilder.DataServiceVersion = odataVersion2;
    modelBuilder.MaxDataServiceVersion = odataVersion2;



    return modelBuilder;
}