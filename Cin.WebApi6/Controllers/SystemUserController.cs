using Cin.WebApi.CrmModels;
using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Cin.WebApi.Xrm;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Web.Http.OData;
using AllowedQueryOptions = System.Web.Http.OData.Query.AllowedQueryOptions;
using Newtonsoft.Json;
using Cin.WebApi;
using System.Net;
using Cin.WebApi.Extensions;
using Microsoft.Xrm.Sdk.Messages;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using System.Xml;
namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class SystemUserController : BaseController
{   
    [HttpGet]
    [Route("odata/systemuser")]
    public ResultSet UserQuery()
    {
        SystemUserResultSet result = new SystemUserResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        _ = repo.WhoAmI(Request);
        try
        {

            responseQuery = repo.RetrieveUserQuery(Request,Entities.SystemUserName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.SystemUserName, responseQuery.Message);

            List<CustomSystemUser> res = new List<CustomSystemUser>();
            XmlSerializer ser = new XmlSerializer(typeof(SystemUserResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (SystemUserResultSet)ser.Deserialize(reader);
                foreach (CustomSystemUser quoteDetail in result.CustomSystemUser)
                {
                    var metadata = new MetadataTag
                    {
                        uri = appConfig.MetadataUrl + Entities.SystemUserName + quoteDetail.SystemUserId,
                        type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + Entities.SystemUserName
                    };
                    quoteDetail.__metadata = metadata;
                }
                res = result.CustomSystemUser;
            }
                response.d = new ResultsD();
            response.d.results = res;
            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}");
            response.d = new ResultsD
            {
                results = ex.Detail
            };
            return response;
        }
    }

    public SystemUserController(ILogger<SystemUserController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}