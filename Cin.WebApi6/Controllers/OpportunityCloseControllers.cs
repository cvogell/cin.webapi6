﻿using Cin.WebApi.CrmModels;
using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Cin.WebApi.Xrm;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Web.Http.OData;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.AspNetCore.Http.Extensions;
using System.Diagnostics;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class OpportunityCloseController : BaseController
{

    [EnableQuery(AllowedQueryOptions = System.Web.Http.OData.Query.AllowedQueryOptions.All, PageSize = 10)]
    [HttpGet("odata/opportunityclose")]
    public IQueryable<OpportunityClose> GetOpportunityClose()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/opportunityclose called by user {repo.Client.CallerId}");

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return Enumerable.Empty<OpportunityClose>().AsQueryable();

            XrmServiceContext context = new XrmServiceContext(repo.Client);
            return context.OpportunityCloseSet.AsQueryable();
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            throw ex;
        }
        catch(InvalidCastException ex)
        {
            Debugger.Break();
            throw ex;
        }
        catch(Exception ex)
        {
            error(ex.Message, ex);
            throw;
        }
    }

    [EnableQuery]
    [HttpGet("odata/opportunityclose({key})")]
    public OpportunityClose Retrieve(Guid key)
    {
        try
        {
            _ = repo.WhoAmI(Request);
            info($"odata/opportunityclose({key}) called by user {repo.Client.CallerId}");
            XrmServiceContext context = new XrmServiceContext(repo.Client);
            return context.OpportunityCloseSet.AsQueryable().FirstOrDefault(a => a.Id == key);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            throw ex;
        }
    }


    public OpportunityCloseController(ILogger<OpportunityClose> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}

[ApiController]
[Route("")]
public class LoseOpportunityController : BaseController
{

    [HttpPost("odata/loseopportunity")]
    public IActionResult Create(CustomLoseOpportunity loseOpportunity)
    {
        _ = repo.WhoAmI(Request);

        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(loseOpportunity);

        HttpResponseMessage httpResponse = new HttpResponseMessage();

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            UpdateRequest reqUpdateOpportunity = new UpdateRequest();
            Entity opportunity = new Entity();
            opportunity.LogicalName = Entities.opportunityLogicalName.ToLower();
            opportunity.Id = loseOpportunity.OpportunityId;
            opportunity[Attributes.targetStage] = Constants.po_targetstage_closed;
            UpdateRequest reqUpdateOpportunityMaster = new UpdateRequest();
            reqUpdateOpportunityMaster.Target = opportunity;
            var responseOpportunityMaster = (UpdateResponse)repo.Client.Execute(reqUpdateOpportunityMaster);

            LoseOpportunityRequest loseReq = new LoseOpportunityRequest();

            Entity opportunityClose = new Entity(Entities.opportunityCloseLogicalName.ToLower());
            opportunityClose.Attributes.Add(Attributes.opportunityid, new EntityReference(Entities.opportunityLogicalName.ToLower(), loseOpportunity.OpportunityId));
            opportunityClose.Attributes.Add(Attributes.subject, "Lost the opportunity.");

            loseReq.OpportunityClose = opportunityClose;
            loseReq.RequestName = "LoseOpportunity";
            OptionSetValue osv = new OptionSetValue();
            osv.Value = loseOpportunity.StatusCode;
            loseReq.Status = osv;

            var response = (LoseOpportunityResponse)repo.Client.Execute(loseReq);
            CreateResponseResult result = new CreateResponseResult();
            loseOpportunity.__metadata = CreateMetaDataTag("LoseOpportunity", loseOpportunity.Id);

            result.d = loseOpportunity;

            return Created(Request.GetDisplayUrl(),result);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }
    }
    public LoseOpportunityController(ILogger<LoseOpportunityController> logger, IMemoryCache memoryCache, 
        IConfiguration configuration, CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}


[ApiController]
[Route("")]
public class WinOpportunityController : BaseController
{
    
    [HttpPost("odata/winopportunity")]
    public IActionResult Create(CustomWinOpportunity winOpportunity)
    {
        _ = repo.WhoAmI(Request);

        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(winOpportunity);

        HttpResponseMessage httpResponse = new HttpResponseMessage();

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            UpdateRequest reqUpdateOpportunity = new UpdateRequest();
            Entity opportunity = new Entity();
            opportunity.LogicalName = Entities.opportunityLogicalName.ToLower();
            opportunity.Id = winOpportunity.OpportunityId;
            opportunity[Attributes.targetStage] = Constants.po_targetstage_closed;
            UpdateRequest reqUpdateOpportunityMaster = new UpdateRequest();
            reqUpdateOpportunityMaster.Target = opportunity;
            var responseOpportunityMaster = (UpdateResponse)repo.Client.Execute(reqUpdateOpportunityMaster);

            WinOpportunityRequest winReq = new WinOpportunityRequest();

            Entity opportunityClose = new Entity(Entities.opportunityCloseLogicalName.ToLower());
            opportunityClose.Attributes.Add("opportunityid", new EntityReference(Entities.opportunityLogicalName.ToLower(), winOpportunity.OpportunityId));
            opportunityClose.Attributes.Add("subject", "Won the opportunity.");

            winReq.OpportunityClose = opportunityClose;
            winReq.RequestName = "WinOpportunity";
            OptionSetValue osv = new OptionSetValue();
            osv.Value = winOpportunity.StatusCode;
            winReq.Status = osv;

            WinOpportunityResponse response = (WinOpportunityResponse)repo.Client.Execute(winReq);
            CreateResponseResult result = new CreateResponseResult();
            winOpportunity.__metadata = CreateMetaDataTag("WinOpportunity", winOpportunity.Id);
            result.d = winOpportunity;

            return Created(Request.GetDisplayUrl(),result);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }
    }

    public WinOpportunityController(ILogger<WinOpportunityController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}

