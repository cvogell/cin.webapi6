﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.Diagnostics.Contracts;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class CintasLocationController : BaseController
{
    [HttpGet("odata/cintaslocation")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/cintaslocation called by user {repo.Client.CallerId}");
        CintasLocationResultSet result = new CintasLocationResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {
            responseQuery = repo.RetrieveUserQuery(Request, Entities.cintasLocationName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.cintasLocationName, responseQuery.Message);
            List<CustomCintasLocation> res = new List<CustomCintasLocation>();
            XmlSerializer ser = new XmlSerializer(typeof(CintasLocationResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (CintasLocationResultSet)ser.Deserialize(reader);
                foreach (CustomCintasLocation location in result.CustomCintasLocation)
                {
                    location.__metadata = CreateMetaDataTag(Entities.cintasLocationName, 
                        location.Po_CintasLocationId.ToString());
                }
                res = result.CustomCintasLocation;
            }

            response.d = new ResultsD();
            response.d.results = res;

            debug($"CustomCintasLocation.Count: {result.CustomCintasLocation.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error::Message:{ex.Message}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }

    }


    public CintasLocationController(ILogger<CintasLocationController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
