﻿using Cin.WebApi.CrmModels;
using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Cin.WebApi.Xrm;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Web.Http.OData;
using AllowedQueryOptions = System.Web.Http.OData.Query.AllowedQueryOptions;
using Newtonsoft.Json;
using Cin.WebApi;
using System.Net;
using Cin.WebApi.Extensions;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.AspNetCore.Mvc;
namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class AccountController : BaseController
{
    [HttpGet("odata/accountcheck")]
    [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.All, PageSize = 10)]
    public IQueryable<Account> AccountCheck()
    {
        try
        {
            var who = repo.WhoAmI(Request);
            info($"odata/accountcheck called by user {repo.Client.CallerId}");

            if (who == null || repo.Client.CallerId == Guid.Empty)
                return Enumerable.Empty<Account>().AsQueryable();

            XrmServiceContext context = new XrmServiceContext(repo.Client);
            return context.AccountSet.AsQueryable();
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error($"{ex.GetType()}: {ex.Message}", ex);
            throw ex;
        }
    }

    [HttpGet]
    [Route("odata/account")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/account called by user {repo.Client.CallerId}");
        string deltaToken = GenerateDeltaToken(Request);
        bool useDeltas = appConfig.UseDeltas == "Y";
        
        CommonResponse responseQuery = new CommonResponse();
        CommonResponse responseFetchXML = new CommonResponse();

        if (useDeltas)
        {
            DeltaResultSet response = new DeltaResultSet();
            AccountDeltaResultSet result = new AccountDeltaResultSet();
            result.CustomAccountDelta = new List<object>();
            try
            {
                DateTime localDT = ParseDeltaToken(Request);
                if (TimeZoneInfo.Local.BaseUtcOffset < TimeZoneInfo.Utc.BaseUtcOffset && localDT != new DateTime(1980, 1, 1, 1, 0, 0))
                    localDT = TimeZoneInfo.ConvertTimeFromUtc(ParseDeltaToken(Request), TimeZoneInfo.Local);
                List<Entity> returnAccountList = null;
                string deltaFilter = (HasDeltaToken(Request)
                                        ? "<condition attribute='modifiedon' operator='ge' value='"
                                            + localDT.ToString("yyyy'-'MM'-'dd'T'HH:mm:ss'+00:00'")
                                            + "' />"
                                        : "");

                if (String.IsNullOrEmpty(deltaFilter))
                {
                    string oauthToken = repo.Token;

                    //OrganizationWebProxyClient client = repo.Client.getCrmConnection(Request);
                    //OrganizationResponse responseWhoamI = repo.WhoAmI();

                    //string userId = ((WhoAmIResponse)responseWhoamI).UserId.ToString();
                    //string userName = (string)RetrieveFieldFromEntityByKey(Request, Entities.SystemUserLogicalName, "systemuserid", ((WhoAmIResponse)responseWhoamI).UserId, Attributes.fullName);

                    //CinTrace.TraceWarning("User {1} ({2}) executing a full refresh.", "SMPWrapper", null, userName, userId);
                    //args[2] = userName;
                    //CinTrace.TraceCustomDebug(level: "DEBUG", format: "Request: {0}, Entity: {1}, User: {2}", args: args);
                }

                responseQuery = repo.RetrieveUserQueryWithPagination(Request, 
                    Entities.accountName, out returnAccountList, new string[]
                    {
                        Attributes.po_custoffs,
                        Attributes.po_custofur,
                        Attributes.po_custoffire,
                        Attributes.po_custoffas,
                        Attributes.po_custofchicagods,
                        Attributes.po_custofcatalog,
                        Attributes.po_competitor_ur,
                        Attributes.po_competitor_fs,
                        Attributes.po_competitor_ur_frc,
                        Attributes.po_competitor_ur_mdr,
                        Attributes.po_competitor_fs_deepclean,
                        Attributes.po_competitor_fs_mdr,
                        Attributes.po_estimatedvolume_ur_mdr,
                        Attributes.po_estimatedvolume_ur_frc,
                        Attributes.po_estimatedvolume_fs_mdr,
                        Attributes.po_estimatedvolume_fs_deepclean,
                        Attributes.po_estimatedvolume_acct_ur,
                        Attributes.po_estimatedvolume_acct_fs,
                        Attributes.po_estimatedclosedate_ur_mdr,
                        Attributes.po_estimatedclosedate_ur_frc,
                        Attributes.po_estimatedclosedate_fs_mdr,
                        Attributes.po_estimatedclosedate_fs_deepclean,
                        Attributes.po_estimatedclosedate_ur,
                        Attributes.po_estimatedclosedate_fs,
                        HasDeltaToken(Request)?
                        String.Format(Attributes.deltaFilter, "ge", localDT.ToString("yyyy'-'MM'-'dd'T'HH:mm:ss'+00:00'")):
                        ""});
                if (responseQuery.ResponseName == Constants.failureResponse)
                {
                    error($"Unexpected error::Message:{responseQuery.Message}||Entity:{responseQuery.EntityName}");

                    response.d = new DeltaResultsD();
                    response.d.results = responseQuery.Message;
                    return response;
                }

                List<Entity> returnOOZAccountList = null;
                responseFetchXML = repo.RetrieveFetchXMLQueryWithPagination(Request, Entities.accountName, out returnOOZAccountList,
@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
  <entity name='account'>
    <attribute name='name' />
    <attribute name='address1_city' />
    <attribute name='telephone1' />
    <attribute name='address1_line1' />
    <attribute name='address1_stateorprovince' />
    <attribute name='po_postalcodeid' />
    <attribute name='po_custofur' />
    <attribute name='po_custoffas' />
    <attribute name='po_custoffire' />
    <attribute name='po_custoffs' />
    <attribute name='po_custofchicagods' />
    <attribute name='po_custofcatalog' />
    <attribute name='po_naicsdescription' />
    <attribute name='po_naicscodeid' />
    <attribute name='modifiedon' />
    <attribute name='ctas_square_footage_opt' />
    <attribute name='ctas_no_employees_opt' />
    <attribute name='po_dba' />
    <attribute name='address1_composite' />
    <attribute name='accountid' />
    <attribute name='po_competitor_ur' />
    <attribute name='po_competitor_fs' />
    <attribute name='po_competitor_ur_frc' />
    <attribute name='po_competitor_ur_mdr' />
    <attribute name='po_competitor_fs_deepclean' />
    <attribute name='po_competitor_fs_mdr' />
    <attribute name='po_estimatedvolume_ur_mdr' />
    <attribute name='po_estimatedvolume_ur_frc' />
    <attribute name='po_estimatedvolume_fs_mdr' />
    <attribute name='po_estimatedvolume_fs_deepclean' />
    <attribute name='po_estimatedvolume_acct_ur' />
    <attribute name='po_estimatedvolume_acct_fs' />
    <attribute name='po_estimatedclosedate_ur_mdr' />
    <attribute name='po_estimatedclosedate_ur_frc' />
    <attribute name='po_estimatedclosedate_fs_mdr' />
    <attribute name='po_estimatedclosedate_fs_deepclean' />
    <attribute name='po_estimatedclosedate_ur' />
    <attribute name='po_estimatedclosedate_fs' />
    <order attribute='name' descending='false' />
    <filter type='and'>
      <condition attribute='statecode' operator='eq' value='0' />
" + deltaFilter + @"
    </filter>
    <link-entity name='opportunity' from='parentaccountid' to='accountid' link-type='inner' alias='ar'>
      <filter type='and'>
        <condition attribute='ownerid' operator='eq-userid' />
        <condition attribute='statecode' operator='eq' value='0' />
      </filter>
      <link-entity name='account' from='accountid' to='parentaccountid' link-type='inner' alias='as'>
        <filter type='and'>
          <condition attribute='statecode' operator='eq' value='0' />
        </filter>
      </link-entity>
    </link-entity>
  </entity>
</fetch>");

                if (responseFetchXML.ResponseName == Constants.failureResponse)
                {
                    error($"Unexpected error::Message:{responseFetchXML.Message}");

                    response.d = new DeltaResultsD();
                    response.d.results = responseFetchXML.Message;
                    return response;
                }

                debug("Deserializing Accounts");

                if (returnOOZAccountList.Count > 0) returnAccountList.AddRange(returnOOZAccountList);

                debug("Processing Active Accounts");
                foreach (Entity account in returnAccountList)
                {
                    var metadata = new DeltaMetadataTag();
                    metadata.id = $"{appConfig.MetadataUrl}{Entities.accountName}({account.Id})";
                    metadata.uri = metadata.id;
                    metadata.type = $"{appConfig.ModelNamespace}.{Constants.entityCustom}{Entities.accountName}";
                    //metadata.id = string.Format(ConfigurationManager.AppSettings[AppSettings.metadataURL].ToString() + Entities.accountName + "({0})", account.Id);
                    //metadata.uri = string.Format(ConfigurationManager.AppSettings[AppSettings.metadataURL].ToString() + Entities.accountName + "({0})", account.Id);
                    //metadata.type = ConfigurationManager.AppSettings[AppSettings.modelNameSpace].ToString() + Delimiters.Dot + Constants.entityCustom + Entities.accountName;

                    var cEntity = new CustomAccount();
                    cEntity.__metadata = metadata;

                    ParseResultAttribute(cEntity, account);

                    result.CustomAccountDelta.Add(cEntity);
                }
                debug("Active Accounts Processed");

                if (HasDeltaToken(Request))
                {

                    CommonResponse responseFetchTombstoneXML = new CommonResponse();
                    CommonResponse responseFetchOOZTombstoneXML = new CommonResponse();

                    List<Entity> returnOOZTombstoneList = null;
                    responseFetchOOZTombstoneXML = repo.RetrieveFetchXMLQueryWithPagination(Request, Entities.accountName, out returnOOZTombstoneList,
    @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
  <entity name='account'>
    <attribute name='name' />
    <attribute name='address1_city' />
    <attribute name='telephone1' />
    <attribute name='address1_line1' />
    <attribute name='address1_stateorprovince' />
    <attribute name='po_postalcodeid' />
    <attribute name='po_custofur' />
    <attribute name='po_custoffas' />
    <attribute name='po_custoffire' />
    <attribute name='po_custoffs' />
    <attribute name='po_custofchicagods' />
    <attribute name='po_custofcatalog' />
    <attribute name='po_naicsdescription' />
    <attribute name='po_naicscodeid' />
    <attribute name='modifiedon' />
    <attribute name='ctas_square_footage_opt' />
    <attribute name='ctas_no_employees_opt' />
    <attribute name='po_dba' />
    <attribute name='address1_composite' />
    <attribute name='accountid' />
    <attribute name='po_competitor_ur' />
    <attribute name='po_competitor_fs' />
    <attribute name='po_competitor_ur_frc' />
    <attribute name='po_competitor_ur_mdr' />
    <attribute name='po_competitor_fs_deepclean' />
    <attribute name='po_competitor_fs_mdr' />
    <attribute name='po_estimatedvolume_ur_mdr' />
    <attribute name='po_estimatedvolume_ur_frc' />
    <attribute name='po_estimatedvolume_fs_mdr' />
    <attribute name='po_estimatedvolume_fs_deepclean' />
    <attribute name='po_estimatedvolume_acct_ur' />
    <attribute name='po_estimatedvolume_acct_fs' />
    <attribute name='po_estimatedclosedate_ur_mdr' />
    <attribute name='po_estimatedclosedate_ur_frc' />
    <attribute name='po_estimatedclosedate_fs_mdr' />
    <attribute name='po_estimatedclosedate_fs_deepclean' />
    <attribute name='po_estimatedclosedate_ur' />
    <attribute name='po_estimatedclosedate_fs' />
    <order attribute='name' descending='false' />
    <filter type='and'>
      <condition attribute='statecode' operator='eq' value='1' />
" + deltaFilter + @"
    </filter>
    <link-entity name='opportunity' from='parentaccountid' to='accountid' link-type='inner' alias='ar'>
      <filter type='and'>
        <condition attribute='ownerid' operator='eq-userid' />
        <condition attribute='statecode' operator='eq' value='0' />
      </filter>
      <link-entity name='account' from='accountid' to='parentaccountid' link-type='inner' alias='as'>
        <filter type='and'>
          <condition attribute='statecode' operator='eq' value='1' />
        </filter>
      </link-entity>
    </link-entity>
  </entity>
</fetch>");

                    if (responseFetchOOZTombstoneXML.ResponseName == Constants.failureResponse)
                    {
                        error($"Unexpected error::Message:{responseFetchOOZTombstoneXML.Message}");

                        response.d = new DeltaResultsD();
                        response.d.results = responseFetchOOZTombstoneXML.Message;
                        return response;
                    }

                    List<Entity> returnTombstoneList = null;
                    responseFetchTombstoneXML = repo.RetrieveFetchXMLQueryWithPagination(Request, Entities.accountName, 
                        out returnTombstoneList,
    @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
  <entity name='account'>
    <attribute name='name' />
    <attribute name='address1_city' />
    <attribute name='telephone1' />
    <attribute name='address1_line1' />
    <attribute name='address1_stateorprovince' />
    <attribute name='po_postalcodeid' />
    <attribute name='po_custofur' />
    <attribute name='po_custoffas' />
    <attribute name='po_custoffire' />
    <attribute name='po_custoffs' />
    <attribute name='po_custofchicagods' />
    <attribute name='po_custofcatalog' />
    <attribute name='po_naicsdescription' />
    <attribute name='po_naicscodeid' />
    <attribute name='modifiedon' />
    <attribute name='ctas_square_footage_opt' />
    <attribute name='ctas_no_employees_opt' />
    <attribute name='po_dba' />
    <attribute name='address1_composite' />
    <attribute name='accountid' />
    <attribute name='po_competitor_ur' />
    <attribute name='po_competitor_fs' />
    <attribute name='po_competitor_ur_frc' />
    <attribute name='po_competitor_ur_mdr' />
    <attribute name='po_competitor_fs_deepclean' />
    <attribute name='po_competitor_fs_mdr' />
    <attribute name='po_estimatedvolume_ur_mdr' />
    <attribute name='po_estimatedvolume_ur_frc' />
    <attribute name='po_estimatedvolume_fs_mdr' />
    <attribute name='po_estimatedvolume_fs_deepclean' />
    <attribute name='po_estimatedvolume_acct_ur' />
    <attribute name='po_estimatedvolume_acct_fs' />
    <attribute name='po_estimatedclosedate_ur_mdr' />
    <attribute name='po_estimatedclosedate_ur_frc' />
    <attribute name='po_estimatedclosedate_fs_mdr' />
    <attribute name='po_estimatedclosedate_fs_deepclean' />
    <attribute name='po_estimatedclosedate_ur' />
    <attribute name='po_estimatedclosedate_fs' />
    <order attribute='name' descending='false' />
    <filter type='and'>
      <condition attribute='statecode' operator='eq' value='1' />
" + deltaFilter + @"
    </filter>
    <link-entity name='ctas_salesteam_account' from='accountid' to='accountid' visible='false' intersect='true'>
      <link-entity name='ctas_salesteam' from='ctas_salesteamid' to='ctas_salesteamid' alias='ab'>
        <filter type='and'>
          <condition attribute='ownerid' operator='eq-userid' />
        </filter>
      </link-entity>
    </link-entity>
  </entity>
</fetch>");

                    if (responseFetchTombstoneXML.ResponseName == Constants.failureResponse)
                    {
                        error($"Unexpected error::Message:{responseFetchTombstoneXML.Message}");

                        response.d = new DeltaResultsD();
                        response.d.results = responseFetchTombstoneXML.Message;
                        return response;
                    }

                    List<Entity> resFetchTombstoneXML = returnTombstoneList;

                    List<Entity> resFetchOOZTombstoneXML = returnOOZTombstoneList;


                    if (resFetchTombstoneXML.Count > 0)
                    {
                        resFetchOOZTombstoneXML.AddRange(resFetchTombstoneXML);
                    }

                    debug("Processing Inactive Accounts");
                    foreach (Entity account in resFetchOOZTombstoneXML)
                    {
                        var cEntity = new CustomAccountDelta();
                        cEntity.oData_Context = "$metadata#" + Entities.accountName + "/$deletedEntity";
                        cEntity.id = string.Format(appConfig.MetadataUrl + Entities.accountName + "(guid'{0}')", account.Id);
                        string sEntity = JsonConvert.SerializeObject(cEntity);
                        sEntity = sEntity.Replace("oData_Context", "@odata.context");
                        object entity = JsonConvert.DeserializeObject(sEntity);

                        result.CustomAccountDelta.Add(entity);
                    }

                    debug("Inactive Accounts Processed");
                }

                result.__delta = string.Format(appConfig.MetadataUrl + Entities.accountName + "?!deltatoken={0}", deltaToken);
                response.d = new DeltaResultsD();
                response.d.results = result.CustomAccountDelta;
                response.d.__delta = result.__delta;

                GC.Collect();
                return (ResultSet)response;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                error($"{ex.GetType()}: {ex.Message}", ex);
                response.d = new DeltaResultsD();
                response.d.results = ex.Detail;
                return response;
            }
            catch (System.Web.Http.HttpResponseException exc)
            {
                throw exc;
            }
            catch (Exception exc)
            {
                error($"{exc.GetType()}: {exc.Message}", exc);
                response.d = new DeltaResultsD();
                response.d.results = exc.InnerException;
                return response;
            }
        }
        else
        {
            ResultSet response = new ResultSet();
            AccountResultSet result = new AccountResultSet();
            result.CustomAccount = new List<CustomAccount>();

            try
            {
                List<Entity> returnAccountList = null;
                responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.accountName, 
                    out returnAccountList, new string[]
                    {
                        Attributes.po_custoffs,
                        Attributes.po_custofur,
                        Attributes.po_custoffire,
                        Attributes.po_custoffas,
                        Attributes.po_custofchicagods,
                        Attributes.po_custofcatalog,
                        Attributes.po_competitor_ur,
                        Attributes.po_competitor_fs,
                        Attributes.po_competitor_ur_frc,
                        Attributes.po_competitor_ur_mdr,
                        Attributes.po_competitor_fs_deepclean,
                        Attributes.po_competitor_fs_mdr,
                        Attributes.po_estimatedvolume_ur_mdr,
                        Attributes.po_estimatedvolume_ur_frc,
                        Attributes.po_estimatedvolume_fs_mdr,
                        Attributes.po_estimatedvolume_fs_deepclean,
                        Attributes.po_estimatedvolume_acct_ur,
                        Attributes.po_estimatedvolume_acct_fs,
                        Attributes.po_estimatedclosedate_ur_mdr,
                        Attributes.po_estimatedclosedate_ur_frc,
                        Attributes.po_estimatedclosedate_fs_mdr,
                        Attributes.po_estimatedclosedate_fs_deepclean,
                        Attributes.po_estimatedclosedate_ur,
                        Attributes.po_estimatedclosedate_fs });
                if (responseQuery.ResponseName == Constants.failureResponse)
                {
                    error($"Unexpected error::Message:{responseQuery.Message}||Entity:{responseQuery.EntityName}");

                    response.d = new ResultsD();
                    response.d.results = responseQuery.Message;
                    return response;
                }

                List<Entity> returnOOZAccountList = null;
                responseFetchXML = repo.RetrieveFetchXMLQueryWithPagination(Request, Entities.accountName, out returnOOZAccountList,
@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
  <entity name='account'>
    <attribute name='name' />
    <attribute name='address1_city' />
    <attribute name='telephone1' />
    <attribute name='address1_line1' />
    <attribute name='address1_stateorprovince' />
    <attribute name='po_postalcodeid' />
    <attribute name='po_custofur' />
    <attribute name='po_custoffas' />
    <attribute name='po_custoffire' />
    <attribute name='po_custoffs' />
    <attribute name='po_custofchicagods' />
    <attribute name='po_custofcatalog' />
    <attribute name='po_naicsdescription' />
    <attribute name='po_naicscodeid' />
    <attribute name='modifiedon' />
    <attribute name='ctas_square_footage_opt' />
    <attribute name='ctas_no_employees_opt' />
    <attribute name='po_dba' />
    <attribute name='address1_composite' />
    <attribute name='accountid' />
    <attribute name='po_competitor_ur' />
    <attribute name='po_competitor_fs' />
    <attribute name='po_competitor_ur_frc' />
    <attribute name='po_competitor_ur_mdr' />
    <attribute name='po_competitor_fs_deepclean' />
    <attribute name='po_competitor_fs_mdr' />
    <attribute name='po_estimatedvolume_ur_mdr' />
    <attribute name='po_estimatedvolume_ur_frc' />
    <attribute name='po_estimatedvolume_fs_mdr' />
    <attribute name='po_estimatedvolume_fs_deepclean' />
    <attribute name='po_estimatedvolume_acct_ur' />
    <attribute name='po_estimatedvolume_acct_fs' />
    <attribute name='po_estimatedclosedate_ur_mdr' />
    <attribute name='po_estimatedclosedate_ur_frc' />
    <attribute name='po_estimatedclosedate_fs_mdr' />
    <attribute name='po_estimatedclosedate_fs_deepclean' />
    <attribute name='po_estimatedclosedate_ur' />
    <attribute name='po_estimatedclosedate_fs' />
    <order attribute='name' descending='false' />
    <filter type='and'>
      <condition attribute='statecode' operator='eq' value='0' />
    </filter>
    <link-entity name='opportunity' from='parentaccountid' to='accountid' link-type='inner' alias='ar'>
      <filter type='and'>
        <condition attribute='ownerid' operator='eq-userid' />
        <condition attribute='statecode' operator='eq' value='0' />
      </filter>
      <link-entity name='account' from='accountid' to='parentaccountid' link-type='inner' alias='as'>
        <filter type='and'>
          <condition attribute='statecode' operator='eq' value='0' />
        </filter>
        <link-entity name='ctas_salesteam_account' from='accountid' to='accountid' visible='false' intersect='true'>
          <link-entity name='ctas_salesteam' from='ctas_salesteamid' to='ctas_salesteamid' alias='at'>
            <filter type='and'>
              <condition attribute='ownerid' operator='ne-userid' />
            </filter>
          </link-entity>
        </link-entity>
      </link-entity>
    </link-entity>
  </entity>
</fetch>");
                if (responseFetchXML.ResponseName == Constants.failureResponse)
                {
                    error($"Unexpected error::Message:{responseFetchXML.Message}||Entity:{responseFetchXML.EntityName}");

                    response.d = new ResultsD();
                    response.d.results = responseFetchXML.Message;
                    return response;
                }

                debug("Deserializing Accounts");

                if (returnOOZAccountList.Count > 0) returnAccountList.AddRange(returnOOZAccountList);

                debug("Processing Accounts");
                foreach (Entity account in returnAccountList)
                {
                    var metadata = getMetaTag(Entities.accountName, account.Id);
                    var cEntity = new CustomAccount();
                    cEntity.__metadata = metadata;

                    ParseResultAttribute(cEntity, account);

                    result.CustomAccount.Add(cEntity);
                }
                debug("Accounts Processed");

                response.d = new ResultsD();
                response.d.results = result.CustomAccount;
                return response;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                error($"{ex.GetType()}:{ex.Message}", ex);
                response.d = new ResultsD();
                response.d.results = ex.Detail;
                return response;
            }
            catch (Exception exc)
            {
                error($"{exc.GetType()}:{exc.Message}", exc);
                response.d = new ResultsD();
                response.d.results = exc.InnerException;
                return response;
            }
        }
    }

    [HttpPost]
    [Route("odata/account({key})")]
    //[Route("/account({key})")]
    public IActionResult Update(Guid key, CustomAccount accountToUpdate)
    {
        _ = repo.WhoAmI(Request);
        info($"odata/account({key}) called by user {repo.Client.CallerId}");
        if (key == Guid.Empty)
            return BadRequest();
        
        string jsonEntity = JsonConvert.SerializeObject(accountToUpdate);
        try
        {
            Entity account = new Entity();
            account.LogicalName = Entities.accountLogicalName.ToLower();
            account.Id = key;

            AssignCustomRecords(account, accountToUpdate);
            UpdateRequest reqToUpdate = new UpdateRequest();
            reqToUpdate.Target = account;
            var response = (UpdateResponse)repo.Client.Execute(reqToUpdate);

            if (response != null)
                return Ok();
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error($"{ex.GetType()}: {ex.Message}", ex);
            return BadRequest();
        }

        debug($"Request: {Request}, Entity: {accountToUpdate}");

        return Ok();
    }

    private void AssignCustomRecords(Entity account, CustomAccount clientEntity)
    {
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(clientEntity);
        debug($"AssignCustomRecords Post paylosd: {jsonEntity}");
        try
        {
            if (!string.IsNullOrEmpty(clientEntity.Name))
                account[Attributes.name] = clientEntity.Name;
            //if (action == Constants.Update)
            //    account[Attributes.accountid] = clientEntity.AccountId;
            if (clientEntity.Ownerid != null && !string.IsNullOrEmpty(clientEntity.Ownerid.Text))
                account[Attributes.ownerId] = new EntityReference(Constants.systemUser, new Guid(clientEntity.Ownerid.Text));
            if (!string.IsNullOrEmpty(clientEntity.Telephone1))
                account[Attributes.telephone1] = clientEntity.Telephone1;
            if (clientEntity.po_NAICSCodeid != null && !string.IsNullOrEmpty(clientEntity.po_NAICSCodeid.Text))
                account[Attributes.po_naicscodeid] = new EntityReference(Entities.po_Naicscode, new Guid(clientEntity.po_NAICSCodeid.Text));
            if (clientEntity.po_PostalCodeid != null && !string.IsNullOrEmpty(clientEntity.po_PostalCodeid.Text))
                account[Attributes.po_postalcodeid] = new EntityReference(Entities.po_Postalcode, new Guid(clientEntity.po_PostalCodeid.Text));
            if (!string.IsNullOrEmpty(clientEntity.po_ABINumber))
                account[Attributes.po_abinumber] = clientEntity.po_ABINumber;
            if (!string.IsNullOrEmpty(clientEntity.Address1_PostalCode))
                account[Attributes.address1_postalcode] = clientEntity.Address1_PostalCode;
            if (!string.IsNullOrEmpty(clientEntity.Address1_Longitude))
            {
                float dval = 0;
                float.TryParse(clientEntity.Address1_Longitude, out dval);
                account[Attributes.address1_longitude] = dval;

            }
            debug("AssignCustomRecords Stage 1");
            if (!string.IsNullOrEmpty(clientEntity.Address1_Line3))
                account[Attributes.address1_line3] = clientEntity.Address1_Line3;
            if (!string.IsNullOrEmpty(clientEntity.Address1_Line1))
                account[Attributes.address1_line1] = clientEntity.Address1_Line1;
            if (!string.IsNullOrEmpty(clientEntity.Address1_Latitude))
            {
                float dval = 0;
                float.TryParse(clientEntity.Address1_Latitude, out dval);
                account[Attributes.address1_latitude] = dval;

            }
            if (!string.IsNullOrEmpty(clientEntity.Address1_County))
                account[Attributes.address1_county] = clientEntity.Address1_County;
            if (!string.IsNullOrEmpty(clientEntity.Address1_Country))
                account[Attributes.address1_country] = clientEntity.Address1_Country;
            if (!string.IsNullOrEmpty(clientEntity.Address1_City))
                account[Attributes.address1_city] = clientEntity.Address1_City;
            if (!string.IsNullOrEmpty(clientEntity.Address1_stateorprovince))
                account[Attributes.address1_StateOrProvince] = clientEntity.Address1_stateorprovince;
            if (clientEntity.po_EstimatedVolume_UR_MDR != null && !string.IsNullOrEmpty(clientEntity.po_EstimatedVolume_UR_MDR.Text))
                account[Attributes.po_estimatedvolume_ur_mdr] = new Money(Convert.ToDecimal(clientEntity.po_EstimatedVolume_UR_MDR.Text));
            if (clientEntity.po_EstimatedVolume_UR_FRC != null && !string.IsNullOrEmpty(clientEntity.po_EstimatedVolume_UR_FRC.Text))
                account[Attributes.po_estimatedvolume_ur_frc] = new Money(Convert.ToDecimal(clientEntity.po_EstimatedVolume_UR_FRC.Text));
            if (clientEntity.po_EstimatedVolume_FS_MDR != null && !string.IsNullOrEmpty(clientEntity.po_EstimatedVolume_FS_MDR.Text))
                account[Attributes.po_estimatedvolume_fs_mdr] = new Money(Convert.ToDecimal(clientEntity.po_EstimatedVolume_FS_MDR.Text));
            if (clientEntity.po_EstimatedVolume_FS_DeepClean != null && !string.IsNullOrEmpty(clientEntity.po_EstimatedVolume_FS_DeepClean.Text))
                account[Attributes.po_estimatedvolume_fs_deepclean] = new Money(Convert.ToDecimal(clientEntity.po_EstimatedVolume_FS_DeepClean.Text));
            if (clientEntity.po_EstimatedVolume_Acct_UR != null && !string.IsNullOrEmpty(clientEntity.po_EstimatedVolume_Acct_UR.Text))
                account[Attributes.po_estimatedvolume_acct_ur] = new Money(Convert.ToDecimal(clientEntity.po_EstimatedVolume_Acct_UR.Text));
            if (clientEntity.po_EstimatedVolume_Acct_FS != null && !string.IsNullOrEmpty(clientEntity.po_EstimatedVolume_Acct_FS.Text))
                account[Attributes.po_estimatedvolume_acct_fs] = new Money(Convert.ToDecimal(clientEntity.po_EstimatedVolume_Acct_FS.Text));
            debug("AssignCustomRecords Stage 2");
            if (clientEntity.Po_estimatedclosedate_ur != null && !string.IsNullOrEmpty(clientEntity.Po_estimatedclosedate_ur.Text))
                account[Attributes.po_estimatedclosedate_ur] = Convert.ToDateTime(clientEntity.Po_estimatedclosedate_ur.Text);
            if (clientEntity.Po_estimatedclosedate_ur_mdr != null && !string.IsNullOrEmpty(clientEntity.Po_estimatedclosedate_ur_mdr.Text))
                account[Attributes.po_estimatedclosedate_ur_mdr] = Convert.ToDateTime(clientEntity.Po_estimatedclosedate_ur_mdr.Text);
            if (clientEntity.Po_estimatedclosedate_ur_frc != null && !string.IsNullOrEmpty(clientEntity.Po_estimatedclosedate_ur_frc.Text))
                account[Attributes.po_estimatedclosedate_ur_frc] = Convert.ToDateTime(clientEntity.Po_estimatedclosedate_ur_frc.Text);
            if (clientEntity.Po_estimatedclosedate_fs != null && !string.IsNullOrEmpty(clientEntity.Po_estimatedclosedate_fs.Text))
                account[Attributes.po_estimatedclosedate_fs] = Convert.ToDateTime(clientEntity.Po_estimatedclosedate_fs.Text);
            if (clientEntity.Po_estimatedclosedate_fs_mdr != null && !string.IsNullOrEmpty(clientEntity.Po_estimatedclosedate_fs_mdr.Text))
                account[Attributes.po_estimatedclosedate_fs_mdr] = Convert.ToDateTime(clientEntity.Po_estimatedclosedate_fs_mdr.Text);
            if (clientEntity.Po_estimatedclosedate_fs_deepclean != null && !string.IsNullOrEmpty(clientEntity.Po_estimatedclosedate_fs_deepclean.Text))
                account[Attributes.po_estimatedclosedate_fs_deepclean] = Convert.ToDateTime(clientEntity.Po_estimatedclosedate_fs_deepclean.Text);

            if (!string.IsNullOrEmpty(clientEntity.Address1_Line2))
                account[Attributes.address1_line2] = clientEntity.Address1_Line2;
            if (clientEntity.po_CustofUR != null && !string.IsNullOrEmpty(clientEntity.po_CustofUR.Text))
                account[Attributes.po_custofur] = clientEntity.po_CustofUR.Text.ToBool();
            if (clientEntity.po_CustofFS != null && !string.IsNullOrEmpty(clientEntity.po_CustofFS.Text))
                account[Attributes.po_custoffs] = clientEntity.po_CustofFS.Text.ToBool();
            if (clientEntity.po_CustofFire != null && !string.IsNullOrEmpty(clientEntity.po_CustofFire.Text))
                account[Attributes.po_custoffire] = clientEntity.po_CustofFire.Text.ToBool();
            if (clientEntity.po_CustofFAS != null && !string.IsNullOrEmpty(clientEntity.po_CustofFAS.Text))
                account[Attributes.po_custoffas] = clientEntity.po_CustofFAS.Text.ToBool();
            if (clientEntity.po_CustofChicagoDS != null && !string.IsNullOrEmpty(clientEntity.po_CustofChicagoDS.Text))
                account[Attributes.po_custofchicagods] = clientEntity.po_CustofChicagoDS.Text.ToBool();
            if (clientEntity.Po_Competitor_FS != null && !string.IsNullOrEmpty(clientEntity.Po_Competitor_FS.Text))
                account[Attributes.po_competitor_fs] = new EntityReference(Entities.po_Competitor_FS, new Guid(clientEntity.Po_Competitor_FS.Text));
            else
                account[Attributes.po_competitor_fs] = null;
            if (clientEntity.Po_Competitor_UR != null && !string.IsNullOrEmpty(clientEntity.Po_Competitor_UR.Text))
                account[Attributes.po_competitor_ur] = new EntityReference(Entities.po_Competitor_UR, new Guid(clientEntity.Po_Competitor_UR.Text));
            else
                account[Attributes.po_competitor_ur] = null;
            debug("AssignCustomRecords Stage 3");
            if (clientEntity.Po_Competitor_FS_MDR != null && !string.IsNullOrEmpty(clientEntity.Po_Competitor_FS_MDR.Text))
                account[Attributes.po_competitor_fs_mdr] = new EntityReference(Entities.po_Competitor_FS_MDR, new Guid(clientEntity.Po_Competitor_FS_MDR.Text));
            else
                account[Attributes.po_competitor_fs_mdr] = null;
            if (clientEntity.Po_Competitor_UR_MDR != null && !string.IsNullOrEmpty(clientEntity.Po_Competitor_UR_MDR.Text))
                account[Attributes.po_competitor_ur_mdr] = new EntityReference(Entities.po_Competitor_UR_MDR, new Guid(clientEntity.Po_Competitor_UR_MDR.Text));
            else
                account[Attributes.po_competitor_ur_mdr] = null;
            if (clientEntity.Po_Competitor_FS_DeepClean != null && !string.IsNullOrEmpty(clientEntity.Po_Competitor_FS_DeepClean.Text))
                account[Attributes.po_competitor_fs_deepclean] = new EntityReference(Entities.po_Competitor_FS_DeepClean, new Guid(clientEntity.Po_Competitor_FS_DeepClean.Text));
            else
                account[Attributes.po_competitor_fs_deepclean] = null;
            if (clientEntity.Po_Competitor_UR_FRC != null && !string.IsNullOrEmpty(clientEntity.Po_Competitor_UR_FRC.Text))
                account[Attributes.po_competitor_ur_frc] = new EntityReference(Entities.po_Competitor_UR_FRC, new Guid(clientEntity.Po_Competitor_UR_FRC.Text));
            else
                account[Attributes.po_competitor_ur_frc] = null;
            if (clientEntity.po_CustofCatalog != null && !string.IsNullOrEmpty(clientEntity.po_CustofCatalog.Text))
                account[Attributes.po_custofcatalog] = clientEntity.po_CustofCatalog.Text.ToBool();
            debug("AssignCustomRecords Stage 4");
        }
        catch (Exception ex)
        {
            error($"{ex.GetType()}: {ex.Message}",ex);
            throw;
        }
    }

    private void ParseResultAttribute(object cEntity, Entity account)
    {
        foreach (KeyValuePair<string, object> attr in account.Attributes.ToList())
        {
            switch (attr.Key)
            {
                case "name":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Name = (string)attr.Value;
                    break;
                case "po_custofur":
                    ((CustomAccount)cEntity).po_CustofUR = new Po_custofur();
                    if ((bool)attr.Value)
                    {
                        ((CustomAccount)cEntity).po_CustofUR.Text = "1";
                        ((CustomAccount)cEntity).po_CustofUR.Name = "Yes";
                    }
                    else
                    {
                        ((CustomAccount)cEntity).po_CustofUR.Text = "0";
                        ((CustomAccount)cEntity).po_CustofUR.Name = "No";
                    }
                    break;
                case "po_custoffs":
                    ((CustomAccount)cEntity).po_CustofFS = new Po_custoffs();
                    if ((bool)attr.Value)
                    {
                        ((CustomAccount)cEntity).po_CustofFS.Text = "1";
                        ((CustomAccount)cEntity).po_CustofFS.Name = "Yes";
                    }
                    else
                    {
                        ((CustomAccount)cEntity).po_CustofFS.Text = "0";
                        ((CustomAccount)cEntity).po_CustofFS.Name = "No";
                    }
                    break;
                case "po_custoffire":
                    ((CustomAccount)cEntity).po_CustofFire = new Po_custoffire();
                    if ((bool)attr.Value)
                    {
                        ((CustomAccount)cEntity).po_CustofFire.Text = "1";
                        ((CustomAccount)cEntity).po_CustofFire.Name = "Yes";
                    }
                    else
                    {
                        ((CustomAccount)cEntity).po_CustofFire.Text = "0";
                        ((CustomAccount)cEntity).po_CustofFire.Name = "No";
                    }
                    break;
                case "po_custoffas":
                    ((CustomAccount)cEntity).po_CustofFAS = new Po_custoffas();
                    if ((bool)attr.Value)
                    {
                        ((CustomAccount)cEntity).po_CustofFAS.Text = "1";
                        ((CustomAccount)cEntity).po_CustofFAS.Name = "Yes";
                    }
                    else
                    {
                        ((CustomAccount)cEntity).po_CustofFAS.Text = "0";
                        ((CustomAccount)cEntity).po_CustofFAS.Name = "No";
                    }
                    break;
                case "po_custofchicagods":
                    ((CustomAccount)cEntity).po_CustofChicagoDS = new Po_custofchicagods();
                    if ((bool)attr.Value)
                    {
                        ((CustomAccount)cEntity).po_CustofChicagoDS.Text = "1";
                        ((CustomAccount)cEntity).po_CustofChicagoDS.Name = "Yes";
                    }
                    else
                    {
                        ((CustomAccount)cEntity).po_CustofChicagoDS.Text = "0";
                        ((CustomAccount)cEntity).po_CustofChicagoDS.Name = "No";
                    }
                    break;
                case "po_custofcatalog":
                    ((CustomAccount)cEntity).po_CustofCatalog = new Po_custofcatalog();
                    if ((bool)attr.Value)
                    {
                        ((CustomAccount)cEntity).po_CustofCatalog.Text = "1";
                        ((CustomAccount)cEntity).po_CustofCatalog.Name = "Yes";
                    }
                    else
                    {
                        ((CustomAccount)cEntity).po_CustofCatalog.Text = "0";
                        ((CustomAccount)cEntity).po_CustofCatalog.Name = "No";
                    }
                    break;
                case "telephone1":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Telephone1 = (string)attr.Value;
                    break;
                case "po_abinumber":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).po_ABINumber = (string)attr.Value;
                    break;
                case "address1_postalcode":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Address1_PostalCode = (string)attr.Value;
                    break;
                case "address1_longitude":
                    if (!double.IsNaN((double)attr.Value))
                        ((CustomAccount)cEntity).Address1_Longitude = ((double)attr.Value).ToString();
                    break;
                case "address1_line3":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Address1_Line3 = (string)attr.Value;
                    break;
                case "address1_line1":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Address1_Line1 = (string)attr.Value;
                    break;
                case "address1_latitude":
                    if (!double.IsNaN((double)attr.Value))
                        ((CustomAccount)cEntity).Address1_Latitude = ((double)attr.Value).ToString();
                    break;
                case "address1_county":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Address1_County = (string)attr.Value;
                    break;
                case "address1_country":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Address1_Country = (string)attr.Value;
                    break;
                case "address1_city":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Address1_City = (string)attr.Value;
                    break;
                case "address1_line2":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Address1_Line2 = (string)attr.Value;
                    break;
                case "accountid":
                    if (!string.IsNullOrEmpty((string)(attr.Value.ToString())))
                        ((CustomAccount)cEntity).AccountId = new Guid((string)(attr.Value.ToString()));
                    break;
                case "po_postalcodeid":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                        if (jpv.Id != Guid.Empty)
                        {
                            ((CustomAccount)cEntity).po_PostalCodeid = new Po_postalcodeid();
                            ((CustomAccount)cEntity).po_PostalCodeid.Text = jpv.Id.ToString();
                            ((CustomAccount)cEntity).po_PostalCodeid.Name = jpv.Name;
                        }
                        break;
                    }
                case "po_competitor_ur":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                        if (jpv.Id != Guid.Empty)
                        {
                            ((CustomAccount)cEntity).Po_Competitor_UR = new Po_Competitor_UR();
                            ((CustomAccount)cEntity).Po_Competitor_UR.Text = jpv.Id.ToString();
                            ((CustomAccount)cEntity).Po_Competitor_UR.Name = jpv.Name;
                        }
                        break;
                    }
                case "po_competitor_fs":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                        if (jpv.Id != Guid.Empty)
                        {
                            ((CustomAccount)cEntity).Po_Competitor_FS = new Po_Competitor_FS();
                            ((CustomAccount)cEntity).Po_Competitor_FS.Text = jpv.Id.ToString();
                            ((CustomAccount)cEntity).Po_Competitor_FS.Name = jpv.Name;
                        }
                        break;
                    }
                case "po_competitor_ur_mdr":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                        if (jpv.Id != Guid.Empty)
                        {
                            ((CustomAccount)cEntity).Po_Competitor_UR_MDR = new Po_Competitor_UR_MDR();
                            ((CustomAccount)cEntity).Po_Competitor_UR_MDR.Text = jpv.Id.ToString();
                            ((CustomAccount)cEntity).Po_Competitor_UR_MDR.Name = jpv.Name;
                        }
                        break;
                    }
                case "po_competitor_fs_mdr":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                        if (jpv.Id != Guid.Empty)
                        {
                            ((CustomAccount)cEntity).Po_Competitor_FS_MDR = new Po_Competitor_FS_MDR();
                            ((CustomAccount)cEntity).Po_Competitor_FS_MDR.Text = jpv.Id.ToString();
                            ((CustomAccount)cEntity).Po_Competitor_FS_MDR.Name = jpv.Name;
                        }
                        break;
                    }
                case "po_competitor_ur_frc":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                        if (jpv.Id != Guid.Empty)
                        {
                            ((CustomAccount)cEntity).Po_Competitor_UR_FRC = new Po_Competitor_UR_FRC();
                            ((CustomAccount)cEntity).Po_Competitor_UR_FRC.Text = jpv.Id.ToString();
                            ((CustomAccount)cEntity).Po_Competitor_UR_FRC.Name = jpv.Name;
                        }
                        break;
                    }
                case "po_competitor_fs_deepclean":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                        if (jpv.Id != Guid.Empty)
                        {
                            ((CustomAccount)cEntity).Po_Competitor_FS_DeepClean = new Po_Competitor_FS_DeepClean();
                            ((CustomAccount)cEntity).Po_Competitor_FS_DeepClean.Text = jpv.Id.ToString();
                            ((CustomAccount)cEntity).Po_Competitor_FS_DeepClean.Name = jpv.Name;
                        }
                        break;
                    }
                case "address1_stateorprovince":
                    if (!string.IsNullOrEmpty((string)attr.Value))
                        ((CustomAccount)cEntity).Address1_stateorprovince = (string)attr.Value;
                    break;
                case "po_naicscodeid":
                    string sNAICSVal = JsonConvert.SerializeObject(attr.Value);
                    JsonEntityAttributeGuidValue jpvNAICS = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sNAICSVal);
                    if (jpvNAICS.Id != Guid.Empty)
                    {
                        ((CustomAccount)cEntity).po_NAICSCodeid = new Po_naicscodeid();
                        ((CustomAccount)cEntity).po_NAICSCodeid.Text = jpvNAICS.Id.ToString();
                        ((CustomAccount)cEntity).po_NAICSCodeid.Name = jpvNAICS.Name;
                    }
                    break;
                case "po_estimatedvolume_acct_ur":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                        ((CustomAccount)cEntity).po_EstimatedVolume_Acct_UR = new po_EstimatedVolume_Acct_UR();
                        ((CustomAccount)cEntity).po_EstimatedVolume_Acct_UR.Text = jpv.Value.ToString();
                        break;
                    }
                case "po_estimatedvolume_ur_mdr":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                        ((CustomAccount)cEntity).po_EstimatedVolume_UR_MDR = new po_EstimatedVolume_UR_MDR();
                        ((CustomAccount)cEntity).po_EstimatedVolume_UR_MDR.Text = jpv.Value.ToString();
                        break;
                    }
                case "po_estimatedvolume_acct_fs":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                        ((CustomAccount)cEntity).po_EstimatedVolume_Acct_FS = new po_EstimatedVolume_Acct_FS();
                        ((CustomAccount)cEntity).po_EstimatedVolume_Acct_FS.Text = jpv.Value.ToString();
                        break;
                    }
                case "po_estimatedvolume_ur_frc":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                        ((CustomAccount)cEntity).po_EstimatedVolume_UR_FRC = new po_EstimatedVolume_UR_FRC();
                        ((CustomAccount)cEntity).po_EstimatedVolume_UR_FRC.Text = jpv.Value.ToString();
                        break;
                    }
                case "po_estimatedvolume_fs_mdr":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                        ((CustomAccount)cEntity).po_EstimatedVolume_FS_MDR = new po_EstimatedVolume_FS_MDR();
                        ((CustomAccount)cEntity).po_EstimatedVolume_FS_MDR.Text = jpv.Value.ToString();
                        break;
                    }
                case "po_estimatedvolume_fs_deepclean":
                    {
                        string sVal = JsonConvert.SerializeObject(attr.Value);
                        JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                        ((CustomAccount)cEntity).po_EstimatedVolume_FS_DeepClean = new po_EstimatedVolume_FS_DeepClean();
                        ((CustomAccount)cEntity).po_EstimatedVolume_FS_DeepClean.Text = jpv.Value.ToString();
                        break;
                    }
                case "po_estimatedclosedate_ur":
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur = new Po_estimatedclosedate_ur();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur.Text = ((DateTime)attr.Value).ToString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur.Date = ((DateTime)attr.Value).ToShortDateString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur.Time = ((DateTime)attr.Value).ToShortTimeString();
                    break;
                case "po_estimatedclosedate_ur_mdr":
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur_mdr = new Po_estimatedclosedate_ur_mdr();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur_mdr.Text = ((DateTime)attr.Value).ToString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur_mdr.Date = ((DateTime)attr.Value).ToShortDateString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur_mdr.Time = ((DateTime)attr.Value).ToShortTimeString();
                    break;
                case "po_estimatedclosedate_ur_frc":
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur_frc = new Po_estimatedclosedate_ur_frc();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur_frc.Text = ((DateTime)attr.Value).ToString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur_frc.Date = ((DateTime)attr.Value).ToShortDateString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_ur_frc.Time = ((DateTime)attr.Value).ToShortTimeString();
                    break;
                case "po_estimatedclosedate_fs":
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs = new Po_estimatedclosedate_fs();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs.Text = ((DateTime)attr.Value).ToString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs.Date = ((DateTime)attr.Value).ToShortDateString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs.Time = ((DateTime)attr.Value).ToShortTimeString();
                    break;
                case "Po_estimatedclosedate_fs_mdr":
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs_mdr = new Po_estimatedclosedate_fs_mdr();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs_mdr.Text = ((DateTime)attr.Value).ToString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs_mdr.Date = ((DateTime)attr.Value).ToShortDateString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs_mdr.Time = ((DateTime)attr.Value).ToShortTimeString();
                    break;
                case "po_estimatedclosedate_fs_deepclean":
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs_deepclean = new Po_estimatedclosedate_fs_deepclean();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs_deepclean.Text = ((DateTime)attr.Value).ToString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs_deepclean.Date = ((DateTime)attr.Value).ToShortDateString();
                    ((CustomAccount)cEntity).Po_estimatedclosedate_fs_deepclean.Time = ((DateTime)attr.Value).ToShortTimeString();
                    break;

            }
        }
        foreach (KeyValuePair<string, string> fmtVal in account.FormattedValues.ToList())
        {
            switch (fmtVal.Key)
            {
                case "po_estimatedvolume_fs_mdr":
                    ((CustomAccount)cEntity).po_EstimatedVolume_FS_MDR.Name = fmtVal.Value;
                    break;
                case "po_estimatedvolume_fs_deepclean":
                    ((CustomAccount)cEntity).po_EstimatedVolume_FS_DeepClean.Name = fmtVal.Value;
                    break;
                case "po_estimatedvolume_ur_mdr":
                    ((CustomAccount)cEntity).po_EstimatedVolume_UR_MDR.Name = fmtVal.Value;
                    break;
                case "po_estimatedvolume_acct_ur":
                    ((CustomAccount)cEntity).po_EstimatedVolume_Acct_UR.Name = fmtVal.Value;
                    break;
                case "po_estimatedvolume_ur_frc":
                    ((CustomAccount)cEntity).po_EstimatedVolume_UR_FRC.Name = fmtVal.Value;
                    break;
                case "po_estimatedvolume_acct_fs":
                    ((CustomAccount)cEntity).po_EstimatedVolume_Acct_FS.Name = fmtVal.Value;
                    break;

            }
        }

    }


    public AccountController(ILogger<AccountController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
