using Cin.WebApi.CrmModels;
using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Cin.WebApi.Xrm;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Web.Http.OData;
using AllowedQueryOptions = System.Web.Http.OData.Query.AllowedQueryOptions;
using Newtonsoft.Json;
using Cin.WebApi;
using System.Net;
using Cin.WebApi.Extensions;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.AspNetCore.Mvc;
namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class CancelQuoteController : BaseController
{
    [HttpPost("odata/cancelquote")]
    public IActionResult Create(CustomCancelQuote cancelQuote)
    {
        var systemUserName = repo.WhoAmI(Request);
        debug($"Debug, Request: {Request.Method}, Entities: {Entities.QuoteCloseName},  User: {systemUserName}");
    
        try
        {
            SetStateRequest draftQuote = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(Entities.QuoteLogicalName, Attributes.quoteid, cancelQuote.QuoteId),
                State = new OptionSetValue((int)QuoteState.Draft),
                Status = new OptionSetValue((int)1) //in progress
            };
            repo.Client.Execute(draftQuote);

            SetStateRequest activateQuote = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(Entities.QuoteLogicalName, Attributes.quoteid, cancelQuote.QuoteId),
                State = new OptionSetValue((int)QuoteState.Active),
                Status = new OptionSetValue((int)2) //in progress
            };
            repo.Client.Execute(activateQuote);

            CloseQuoteRequest closeReq = new CloseQuoteRequest();
            Entity quoteClose = new Entity(Entities.QuoteCloseLogicalName.ToLower());
            quoteClose.Attributes.Add(Attributes.quoteid, new EntityReference(Entities.QuoteLogicalName.ToLower(), cancelQuote.QuoteId));
            quoteClose.Attributes.Add(Attributes.subject, "Canceled the quote.");

            closeReq.QuoteClose = quoteClose;
            closeReq.RequestName = "CloseQuote";
            OptionSetValue osv = new OptionSetValue();
            osv.Value = 6;
            closeReq.Status = osv;

            var response = (CloseQuoteResponse)repo.Client.Execute(closeReq);
            CreateResponseResult result = new CreateResponseResult();
            var metadata = new MetadataTag();
            metadata.uri = string.Format(appConfig.MetadataUrl + "CancelQuote" + "({0})", cancelQuote.Id);
            metadata.type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + "CancelQuote";
            cancelQuote.__metadata = metadata;

            result.d = cancelQuote;
            return Ok(result);
        }
        catch (Exception ex)
        {
            error($"Unxepected Error:: Message: {ex.Message}||Trace: {ex.StackTrace}");
            return BadRequest(ex);
        }
    }

    public CancelQuoteController(ILogger<CancelQuoteController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}

public class LoseQuoteController : BaseController
{
    [HttpPost("odata/losequote")]
    public IActionResult Create(CustomLoseQuote loseQuote)
    {
        var systemUserName = repo.WhoAmI(Request);
        debug($"Debug, Request: {Request.Method}, Entities: {Entities.QuoteCloseName},  User: {systemUserName}");

        try
        {
            SetStateRequest draftQuote = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(Entities.QuoteLogicalName, Attributes.quoteid, loseQuote.QuoteId),
                State = new OptionSetValue((int)QuoteState.Draft),
                Status = new OptionSetValue((int)1) //in progress
            };
            repo.Client.Execute(draftQuote);

            SetStateRequest activateQuote = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(Entities.QuoteLogicalName, Attributes.quoteid, loseQuote.QuoteId),
                State = new OptionSetValue((int)QuoteState.Active),
                Status = new OptionSetValue((int)2) //in progress
            };
            repo.Client.Execute(activateQuote);

            CloseQuoteRequest closeReq = new CloseQuoteRequest();
            Entity quoteClose = new Entity(Entities.QuoteCloseLogicalName.ToLower());
            quoteClose.Attributes.Add(Attributes.quoteid, new EntityReference(Entities.QuoteLogicalName.ToLower(), loseQuote.QuoteId));
            quoteClose.Attributes.Add(Attributes.subject, "Lost the quote.");

            closeReq.QuoteClose = quoteClose;
            closeReq.RequestName = "CloseQuote";
            OptionSetValue osv = new OptionSetValue();
            osv.Value = 5;
            closeReq.Status = osv;

            var response = (CloseQuoteResponse)repo.Client.Execute(closeReq);
            CreateResponseResult result = new CreateResponseResult();
            var metadata = new MetadataTag();
            metadata.uri = string.Format(appConfig.MetadataUrl + "LoseQuote" + "({0})", loseQuote.Id.ToString());
            metadata.type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + "LoseQuote";
            loseQuote.__metadata = metadata;

            result.d = loseQuote;

            return Ok(result);
        }
        catch (Exception ex)
        {
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}||");
            return BadRequest(ex);
        }
    }

    public LoseQuoteController(ILogger<LoseQuoteController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}

public class WinQuoteController : BaseController
{

    [HttpPost("odata/winquote")]
    public IActionResult Create(CustomWinQuote winQuote)
    {
        var systemUserName = repo.WhoAmI(Request);
        debug($"Debug, Request: {Request.Method}, Entities: {Entities.QuoteCloseName},  User: {systemUserName}");

        try
        {
            SetStateRequest draftQuote = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(Entities.QuoteLogicalName, Attributes.quoteid, winQuote.QuoteId),
                State = new OptionSetValue((int)QuoteState.Draft),
                Status = new OptionSetValue((int)1) //in progress
            };
            repo.Client.Execute(draftQuote);

            SetStateRequest activateQuote = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(Entities.QuoteLogicalName, Attributes.quoteid, winQuote.QuoteId),
                State = new OptionSetValue((int)QuoteState.Active),
                Status = new OptionSetValue((int)2) //in progress
            };
            repo.Client.Execute(activateQuote);

            WinQuoteRequest winReq = new WinQuoteRequest();

            Entity quoteClose = new Entity(Entities.QuoteCloseLogicalName.ToLower());
            quoteClose.Attributes.Add("quoteid", new EntityReference(Entities.QuoteLogicalName.ToLower(), winQuote.QuoteId));
            quoteClose.Attributes.Add("subject", "Won the Quote.");

            winReq.QuoteClose = quoteClose;
            winReq.RequestName = "WinQuote";
            OptionSetValue osv = new OptionSetValue();
            osv.Value = 4;
            winReq.Status = osv;

            WinQuoteResponse response = (WinQuoteResponse)repo.Client.Execute(winReq);
            CreateResponseResult result = new CreateResponseResult();
            var metadata = new MetadataTag();

            metadata.uri = string.Format(appConfig.MetadataUrl + "WinQuote" + "({0})", winQuote.Id.ToString());
            metadata.type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + "WinQuote";
            winQuote.__metadata = metadata;
            result.d = winQuote;
            return Ok(result);
        }
        catch (Exception ex)
        {
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}||");
            return BadRequest(ex);
        }
    }

    public WinQuoteController(ILogger<WinQuoteController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}