﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Cin.WebApi.Xrm;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk.WebServiceClient;
using Microsoft.Xrm.Sdk;
using System.Diagnostics.Contracts;
using System.ServiceModel;
using System.Web.Http.OData;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class EntityIDentificationOrgController : BaseController
{
    [EnableQuery]
    [HttpGet("odata/entityidentification")]
    public object Retrieve(CustomEntityIdentification cei)
    {
        try
        {
            _ = repo.WhoAmI(Request);
            info($"odata/entityidentification called by user {repo.Client.CallerId}");

            //XrmServiceContext context = new XrmServiceContext(repo.Client);
            //repo.Client.CallerId = Guid.Empty;

            var result = repo.RetrieveFieldFromEntityByKey(Request, cei.DynamicsEntity, 
                cei.KeyField, cei.KeyValue, cei.LookupField);

            return result;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            throw;
        }
    }

    public EntityIDentificationOrgController(ILogger<EntityIDentificationOrgController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
