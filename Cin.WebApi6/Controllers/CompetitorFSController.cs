﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.Diagnostics.Contracts;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class CompetitorFSController : BaseController
{
    [HttpGet("odata/competitorfs")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/competitorfs called by user {repo.Client.CallerId}");

        CompetitorFSResultSet result = new CompetitorFSResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {

            responseQuery = repo.RetrieveUserQuery(Request, Entities.CompetitorFSName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.CompetitorFSName, responseQuery.Message);

            List<CustomCompetitorFS> res = new List<CustomCompetitorFS>();
            XmlSerializer ser = new XmlSerializer(typeof(CompetitorFSResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (CompetitorFSResultSet)ser.Deserialize(reader);
                foreach (CustomCompetitorFS competitorFS in result.CustomCompetitorFS)
                {
                    competitorFS.__metadata = CreateMetaDataTag(Entities.CompetitorFSName, 
                        competitorFS.po_competitorfsId.ToString());
                }
                res = result.CustomCompetitorFS;
            }
            response.d = new ResultsD();
            response.d.results = res;

            debug($"result.CustomCompetitorFS.Count: {result.CustomCompetitorFS.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error: {ex.Message}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    public CompetitorFSController(ILogger<CompetitorFSController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
