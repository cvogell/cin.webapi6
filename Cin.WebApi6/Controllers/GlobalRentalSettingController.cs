﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;
using Microsoft.AspNetCore.Http.Extensions;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class GlobalRentalSettingController : BaseController
{
    
    [HttpGet("odata/globalrentalsetting")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/globalrentalsetting called by user {repo.Client.CallerId}");

        GlobalRentalSettingResultSet result = new GlobalRentalSettingResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {

            responseQuery = repo.RetrieveUserQuery(Request, Entities.globalRentalSettingName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.globalRentalSettingName, responseQuery.Message);

            List<CustomGlobalRentalSetting> res = new List<CustomGlobalRentalSetting>();
            XmlSerializer ser = new XmlSerializer(typeof(GlobalRentalSettingResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (GlobalRentalSettingResultSet)ser.Deserialize(reader);
                foreach (CustomGlobalRentalSetting rentalPolicy in result.CustomGlobalRentalSetting)
                {
                    rentalPolicy.__metadata = CreateMetaDataTag(Entities.globalRentalSettingName, rentalPolicy.Po_RentalPolicyId.ToString());
                }

                res = result.CustomGlobalRentalSetting;
            }

            response.d = new ResultsD();
            response.d.results = res;


            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    [HttpPost("odata/globalrentalsetting")]
    public IActionResult Create(CustomGlobalRentalSetting rentalPolicyCreate)
    {
        _ = repo.WhoAmI(Request);

        HttpResponseMessage response = new HttpResponseMessage();
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(rentalPolicyCreate);

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            Entity rentalPolicy = new Entity();
            rentalPolicy.Id = rentalPolicyCreate.Po_RentalPolicyId;
            rentalPolicy.LogicalName = Entities.globalRentalSettingLogicalName;

            if (string.IsNullOrEmpty(rentalPolicyCreate.Po_Name))
                return BadRequest(Constants.mandatoryName);

            AssignGlobalRentalSettingCustomValues(rentalPolicy, rentalPolicyCreate);

            CreateRequest reqCreateRentalPolicy = new CreateRequest();
            reqCreateRentalPolicy.Target = rentalPolicy;
            CreateResponse responseRentalPolicy = (CreateResponse)repo.Client.Execute(reqCreateRentalPolicy);

            if (responseRentalPolicy.ResponseName == Constants.create && responseRentalPolicy.id != Guid.Empty)
            {
                CreateResponseResult result = new CreateResponseResult();
                rentalPolicyCreate.__metadata = CreateMetaDataTag(Entities.globalRentalSettingName, responseRentalPolicy.id.ToString());
                rentalPolicyCreate.Po_RentalPolicyId = responseRentalPolicy.id;
                result.d = rentalPolicyCreate;
                return Created(Request.GetDisplayUrl(),result);
            }
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }

        return NoContent();
    }

    [HttpPost("odata/globalrentalsetting({key})")]
    public IActionResult Update(Guid key, CustomGlobalRentalSetting rentalPolicyUpdate)
    {
        _ = repo.WhoAmI(Request);

        HttpResponseMessage response = new HttpResponseMessage();
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(rentalPolicyUpdate);

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            Entity rentalPolicy = new Entity();
            rentalPolicy.LogicalName = Entities.globalRentalSettingLogicalName;

            if (key == Guid.Empty)
                return BadRequest(Attributes.po_RentalPolicyId + Constants.mandatoryId);

            rentalPolicy[Attributes.po_RentalPolicyId] = key;

            AssignGlobalRentalSettingCustomValues(rentalPolicy, rentalPolicyUpdate);

            UpdateRequest reqUpdateRentalPolicy = new UpdateRequest();
            reqUpdateRentalPolicy.Target = rentalPolicy;
            UpdateResponse responseRentalPolicy = (UpdateResponse)repo.Client.Execute(reqUpdateRentalPolicy);


            if (responseRentalPolicy != null)
                return NoContent();

            return Ok();
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }
    }

    protected void AssignGlobalRentalSettingCustomValues(Entity globalRentalSetting, CustomGlobalRentalSetting globalRentalSettingAssign)
    {
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(globalRentalSettingAssign);

        try
        {
            if (globalRentalSettingAssign.Po_Name != null)
                globalRentalSetting[Attributes.po_name] = globalRentalSettingAssign.Po_Name;

            if (globalRentalSettingAssign.Po_CintasAgreementProcessGuid != null)
                globalRentalSetting[Attributes.po_cintasagreementprocessguid] = globalRentalSettingAssign.Po_CintasAgreementProcessGuid;

            if (globalRentalSettingAssign.Po_NewBusinessOpportunityType != null && !string.IsNullOrEmpty(globalRentalSettingAssign.Po_NewBusinessOpportunityType.Text))
                globalRentalSetting[Attributes.po_newbusinessopportunitytype] = new OptionSetValue(Int32.Parse(globalRentalSettingAssign.Po_NewBusinessOpportunityType.Text));

            if (globalRentalSettingAssign.Po_PowerAddOpportunityType != null && !string.IsNullOrEmpty(globalRentalSettingAssign.Po_PowerAddOpportunityType.Text))
                globalRentalSetting[Attributes.po_poweraddopportunitytype] = new OptionSetValue(Int32.Parse(globalRentalSettingAssign.Po_PowerAddOpportunityType.Text));

            if (globalRentalSettingAssign.Po_DefaultUSCurrency != null && !string.IsNullOrEmpty(globalRentalSettingAssign.Po_DefaultUSCurrency.Text))
                globalRentalSetting[Attributes.po_defaultuscurrency] = new EntityReference(Constants.systemUser, new Guid(globalRentalSettingAssign.Po_DefaultUSCurrency.Text));

            if (globalRentalSettingAssign.Po_QuotePriceLevelId != null && !string.IsNullOrEmpty(globalRentalSettingAssign.Po_QuotePriceLevelId.Text))
                globalRentalSetting[Attributes.po_quotepricelevelid] = new EntityReference(Constants.systemUser, new Guid(globalRentalSettingAssign.Po_QuotePriceLevelId.Text));

            //if (rentalPolicyAssign.Po_ThreadColor != null && !string.IsNullOrEmpty(rentalPolicyAssign.Po_ThreadColor.Text))
            //    rentalPolicy[Attributes.po_ThreadColor] = new OptionSetValue(Convert.ToInt32(rentalPolicyAssign.Po_ThreadColor.Text));

            //if (rentalPolicyAssign.Po_TrimType != null && !string.IsNullOrEmpty(rentalPolicyAssign.Po_TrimType.Text))
            //    rentalPolicy[Attributes.po_TrimType] = new OptionSetValue(Convert.ToInt32(rentalPolicyAssign.Po_TrimType.Text));

            //if (rentalPolicyAssign.Po_BackgroundColor != null && !string.IsNullOrEmpty(rentalPolicyAssign.Po_BackgroundColor.Text))
            //    rentalPolicy[Attributes.po_BackgroundColor] = new OptionSetValue(Convert.ToInt32(rentalPolicyAssign.Po_BackgroundColor.Text));

            if (globalRentalSettingAssign.OwnerId != null && !string.IsNullOrEmpty(globalRentalSettingAssign.OwnerId.Text))
                globalRentalSetting[Attributes.ownerId] = new EntityReference(Constants.systemUser, new Guid(globalRentalSettingAssign.OwnerId.Text));
        }
        catch (Exception ex)
        {
            error(ex.Message, ex);
            throw;
        }
    }

    public GlobalRentalSettingController(ILogger<GlobalRentalSettingController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
