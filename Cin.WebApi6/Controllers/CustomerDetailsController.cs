﻿using Cin.WebApi.Models;
using Cin.WebApi;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.ServiceModel;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class CustomerDetailsController : BaseController
{
    [HttpGet("odata/customerdetails")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/customerdetails called by user {repo.Client.CallerId}");

        CustomerDetailsResultSet result = new CustomerDetailsResultSet();
        result.CustomCustomerDetails = new List<CustomCustomerDetails>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {
            string[] fieldFiltersSAP = { "ctas_sap_id|ne|", "po_sourcesystem|eq|SAP" };
            string[] fieldFiltersCorp = { "ctas_sap_id|ne|", "po_sourcesystem|eq|Corp" };
            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.CustomerDetailsName, fieldFiltersCorp);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }

            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc = CommonClass.XmlFrame(Entities.CustomerDetailsName, responseQuery.Message);
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.CustomerDetailsName, fieldFiltersSAP);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }

            List<JsonEntity> resObj2 = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);
            if (resObj2.Count > 0) resObj.AddRange(resObj2);


            foreach (JsonEntity customerDetail in resObj)
            {
                var cEntity = new CustomCustomerDetails();
                cEntity.__metadata = CreateMetaDataTag(Entities.CustomerDetailsName, customerDetail.Id);

                foreach (JsonEntityAttribute attr in customerDetail.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "po_contractnumber":
                            {
                                cEntity.Po_ContractNumber = new Po_ContractNumber();
                                cEntity.Po_ContractNumber.Text = ((long)attr.Value).ToString();
                                cEntity.Po_ContractNumber.Formattedvalue = ((long)attr.Value).ToString();
                                break;
                            }
                        case "po_corporatecode":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Po_CorporateCode = (string)attr.Value;
                            break;
                        case "ctas_sap_id":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Ctas_SAP_Id = (string)attr.Value;
                            break;
                        case "po_name":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Po_Name = (string)attr.Value;
                            break;
                        case "po_mlacode":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Po_MLACode = (string)attr.Value;
                            break;
                        case "po_customer_active":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Po_Customer_Active = (string)attr.Value;
                            break;
                        case "po_customernumber":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Po_CustomerNumber = (string)attr.Value;
                            break;
                        case "po_customername":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Po_CustomerName = (string)attr.Value;
                            break;
                        case "po_customerid":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                            {
                                Guid cid = Guid.Empty;
                                Guid.TryParse((string)attr.Value, out cid);
                                if (cid != Guid.Empty)
                                    cEntity.Po_CustomerId = cid;
                            }
                            break;
                        case "po_currentrtenum":
                            {
                                cEntity.Po_CurrentRTENum = new Po_CurrentRteNum();
                                cEntity.Po_CurrentRTENum.Text = ((long)attr.Value).ToString();
                                cEntity.Po_CurrentRTENum.Formattedvalue = ((long)attr.Value).ToString();
                            }
                            break;
                        case "po_fklocno":
                            {
                                if (!string.IsNullOrEmpty((string)attr.Value))
                                {
                                    cEntity.Po_FkLocNo = new Po_BusinessDivisionId();
                                    cEntity.Po_FkLocNo.Name = (string)attr.Value;
                                    cEntity.Po_FkLocNo.Text = (string)attr.Value;
                                }
                                break;
                            }
                        case "po_cintaslocationid":
                            {
                                string sVal2 = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv2 = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal2);
                                if (jpv2.Id != Guid.Empty)
                                {
                                    cEntity.Po_CintasLocationId = new Po_CintasLocationId();
                                    cEntity.Po_CintasLocationId.Text = jpv2.Id.ToString();
                                    cEntity.Po_CintasLocationId.Name = jpv2.Name;
                                }
                            }
                            break;
                        case "po_businessdivisionid":
                            {
                                string sVal2 = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv2 = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal2);
                                if (jpv2.Id != Guid.Empty)
                                {
                                    cEntity.Po_BusinessDivisionId = new Po_BusinessDivisionId();
                                    cEntity.Po_BusinessDivisionId.Text = jpv2.Id.ToString();
                                    cEntity.Po_BusinessDivisionId.Name = jpv2.Name;
                                }
                            }
                            break;
                        case "po_customertoaccount":
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.Po_CustomerToAccount = new Po_CustomerToAccount();
                                cEntity.Po_CustomerToAccount.Text = jpv.Id.ToString();
                                cEntity.Po_CustomerToAccount.Name = jpv.Name;
                            }
                            break;
                    }
                }

                result.CustomCustomerDetails.Add(cEntity);
            }
            //res = result.CustomCustomerDetails;
            //}
            response.d = new ResultsD();
            response.d.results = result.CustomCustomerDetails;
            debug($"result.CustomCustomerDetails.Count: {result.CustomCustomerDetails.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    protected void AssignCustomerCustomValues(Entity customer, CustomCustomerDetails customerDetailsAssign)
    {
        string jsonEntity = JsonConvert.SerializeObject(customerDetailsAssign);

        try
        {
            if (customerDetailsAssign.Po_Name != null)
                customer[Attributes.po_name] = customerDetailsAssign.Po_Name;
            if (customerDetailsAssign.Po_MLACode != null)
                customer[Attributes.po_mlacode] = customerDetailsAssign.Po_MLACode;
            if (customerDetailsAssign.Po_CustomerToAccount != null && !string.IsNullOrEmpty(customerDetailsAssign.Po_CustomerToAccount.Text))
                customer[Attributes.po_customertoaccount] = new EntityReference(Entities.accountLogicalName, new Guid(customerDetailsAssign.Po_CustomerToAccount.Text));
            if (customerDetailsAssign.Po_CustomerNumber != null)
                customer[Attributes.po_customernumber] = customerDetailsAssign.Po_CustomerNumber;
            if (customerDetailsAssign.Po_CustomerName != null)
                customer[Attributes.po_CustomerName] = customerDetailsAssign.Po_CustomerName;
            if (customerDetailsAssign.Po_FkLocNo != null)
                customer[Attributes.po_FkLocNo] = customerDetailsAssign.Po_FkLocNo;
            if (customerDetailsAssign.Po_Customer_Active != null)
                customer[Attributes.po_CUSTOMER_ACTIVE] = customerDetailsAssign.Po_Customer_Active;
            if (customerDetailsAssign.Po_CurrentRTENum != null && !string.IsNullOrEmpty(customerDetailsAssign.Po_CurrentRTENum.Text))
                customer[Attributes.po_CurrentRteNum] = Convert.ToInt32(customerDetailsAssign.Po_CurrentRTENum.Text);
            if (customerDetailsAssign.Po_CorporateCode != null)
                customer[Attributes.po_CorporateCode] = customerDetailsAssign.Po_CorporateCode;
            if (customerDetailsAssign.Po_ContractNumber != null && !string.IsNullOrEmpty(customerDetailsAssign.Po_ContractNumber.Text))
                customer[Attributes.po_ContractNumber] = Convert.ToInt32(customerDetailsAssign.Po_ContractNumber.Text);
            if (customerDetailsAssign.Po_CintasLocationId != null && !string.IsNullOrEmpty(customerDetailsAssign.Po_CintasLocationId.Text))
                customer[Attributes.po_CintasLocationId] = new EntityReference(Entities.cintasLocationLogicalName, new Guid(customerDetailsAssign.Po_CintasLocationId.Text));
            if (customerDetailsAssign.Po_BusinessDivisionId != null && !string.IsNullOrEmpty(customerDetailsAssign.Po_BusinessDivisionId.Text))
                customer[Attributes.po_businessdivisionid] = new EntityReference(Attributes.po_businessdivision, new Guid(customerDetailsAssign.Po_BusinessDivisionId.Text));
            if (customerDetailsAssign.Ctas_SAP_Id != null)
                customer[Attributes.ctas_SAP_ID] = customerDetailsAssign.Ctas_SAP_Id;
        }
        catch (Exception ex)
        {
            error($"{ex.Message}, entity: {jsonEntity}", ex);
            throw;
        }
    }

    public CustomerDetailsController(ILogger<CustomerDetailsController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
