using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Cin.WebApi;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class ProposalsAndAgreementController: BaseController
{

    [HttpDelete("odata/proposalsandagreements({key})")]
    public IActionResult Delete(Guid key)
    {
        try
        {
            repo.Client.Delete(Entities.proposalAndAgreementLogicalName, key);
            return Ok();
        }
        catch (Exception ex)
        {
            debug($"Exception:: Message:{ex.Message}||Trace:{ex.StackTrace}");
            return BadRequest(ex);
        }
    }

    [HttpGet("odata/proposalsandagreements")]
    public IActionResult UserQuery()
    {
        var systemUserName = repo.WhoAmI(Request);
        debug($"Debug:: Request: {Request.Method} Entities: {Entities.proposalAndAgreementName} User: {systemUserName}");
        ProposalsAndAgreementsResultSet result = new ProposalsAndAgreementsResultSet
        {
            CustomProposalsAndAgreements = new List<CustomProposalsAndAgreements>()
        };
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;
        try
        {
            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.proposalAndAgreementName);
            if(responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD
                {
                    results = responseQuery.Message
                };
                return Ok(response);
            }
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);
            foreach (JsonEntity proposalAgreement in resObj)
            {
                var metadata = new MetadataTag
                {
                    uri = string.Format(appConfig.MetadataUrl + Entities.proposalAndAgreementName + "({0})", proposalAgreement.Id),
                    type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + Entities.proposalAndAgreementName
                };
                var cEntity = new CustomProposalsAndAgreements
                {
                    __metadata = metadata
                };

                foreach (JsonEntityAttribute attr in proposalAgreement.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "ctas_signaturetype":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv2 = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                if (jpv2.Value > 0)
                                {
                                    cEntity.Ctas_SignatureType = new Ctas_SignatureType();
                                    cEntity.Ctas_SignatureType.Text = jpv2.Value.ToString();
                                    cEntity.Ctas_SignatureType.Formattedvalue = jpv2.Value.ToString();
                                }
                                break;
                            }
                        case "statecode":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.StateCode = new StateCode();
                                cEntity.StateCode.Text = jpv.Value.ToString();
                                break;
                            }
                        case "ctas_sentdate":
                            {
                                cEntity.Ctas_SentDate = new Ctas_SentDate();
                                cEntity.Ctas_SentDate.Text = ((DateTime)attr.Value).ToString();
                                cEntity.Ctas_SentDate.Date = ((DateTime)attr.Value).ToShortDateString();
                                cEntity.Ctas_SentDate.Time = ((DateTime)attr.Value).ToShortTimeString();
                                break;
                            }
                        case "ctas_customersigneddate":
                            {
                                cEntity.Ctas_CustomerSignedDate = new Ctas_CustomerSignedDate();
                                cEntity.Ctas_CustomerSignedDate.Text = ((DateTime)attr.Value).ToString();
                                cEntity.Ctas_CustomerSignedDate.Date = ((DateTime)attr.Value).ToShortDateString();
                                cEntity.Ctas_CustomerSignedDate.Time = ((DateTime)attr.Value).ToShortTimeString();
                                break;
                            }
                        case "ctas_expirationdate":
                            {
                                cEntity.Ctas_ExpirationDate = new Ctas_ExpirationDate();
                                cEntity.Ctas_ExpirationDate.Text = ((DateTime)attr.Value).ToString();
                                cEntity.Ctas_ExpirationDate.Date = ((DateTime)attr.Value).ToShortDateString();
                                cEntity.Ctas_ExpirationDate.Time = ((DateTime)attr.Value).ToShortTimeString();
                                break;
                            }
                        case "ctas_documentlocation":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.Ctas_DocumentLocation = new Ctas_DocumentLocation();
                                cEntity.Ctas_DocumentLocation.Text = jpv.Value.ToString();
                                break;
                            }
                        case "ctas_documenttype":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.Ctas_DocumentType = new Ctas_DocumentType();
                                cEntity.Ctas_DocumentType.Text = jpv.Value.ToString();
                                break;
                            }
                        case "ctas_azuredocumenturl":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Ctas_AzureDocumentUrl = (string)attr.Value;
                            break;
                        case "ctas_docusignenvelopeid":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Ctas_DocuSignEnvelopeId = (string)attr.Value;
                            break;
                        case "ctas_documenturl":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Ctas_DocumentUrl = (string)attr.Value;
                            break;
                        case "ctas_name":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Ctas_Name = (string)attr.Value;
                            break;
                        case "ctas_opportunity":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Ctas_Opportunity = new Ctas_Opportunity();
                                    cEntity.Ctas_Opportunity.Text = jpv.Id.ToString();
                                    cEntity.Ctas_Opportunity.Name = jpv.Name;
                                }
                                break;
                            }
                        case "ctas_propagrmtcurrentrevisionid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Ctas_PropagrmtCurrentRevisionId = new Ctas_PropagrmtCurrentRevisionId();
                                    cEntity.Ctas_PropagrmtCurrentRevisionId.Text = jpv.Id.ToString();
                                    cEntity.Ctas_PropagrmtCurrentRevisionId.Name = jpv.Name;
                                }
                                break;
                            }
                        case "ctas_proposalsandagreementsid":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Ctas_ProposalsAndAgreementsId = new Guid((string)attr.Value);
                            break;
                    }
                }

                foreach (JsonEntityFormattedValue fmv in proposalAgreement.FormattedValues)
                {
                    switch (fmv.Key)
                    {
                        case "ctas_signaturetype":
                            cEntity.Ctas_SignatureType.Name = fmv.Value;
                            break;
                        case "statecode":
                            cEntity.StateCode.Name = fmv.Value;
                            break;
                        case "ctas_documentlocation":
                            cEntity.Ctas_DocumentLocation.Name = fmv.Value;
                            break;
                        case "ctas_documenttype":
                            cEntity.Ctas_DocumentType.Name = fmv.Value;
                            break;
                    }
                }

                result.CustomProposalsAndAgreements.Add(cEntity);
            }
            response.d = new ResultsD
            {
                results = result.CustomProposalsAndAgreements
            };

            debug(JsonConvert.SerializeObject(response));
            debug($"Request: {Request.Method}, Entity: {Entities.proposalAndAgreementName}, User: {systemUserName}, Payload Length: {result.CustomProposalsAndAgreements.Count}");

            return Ok(response);
        }
        catch (Exception ex)
        {
            error($"Exception:: Message: {ex.Message}||Trace: {ex.StackTrace}");
            return BadRequest(ex);
        }
    }

    [HttpPost("odata/proposalsandagreements")]
    public IActionResult Create(CustomProposalsAndAgreements entityToCreate)
    {
        var systemUserName = repo.WhoAmI(Request);
        debug($"Request: {Request.Method}, Entity: {Entities.proposalAndAgreementName}, User: {systemUserName}");
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(entityToCreate);

        try
        {
            Entity proposalsAndAgreements = new Entity
            {
                Id = entityToCreate.Ctas_ProposalsAndAgreementsId,
                LogicalName = Entities.proposalAndAgreementLogicalName
            };
            ColumnSet colset = new ColumnSet("ctas_proposalsandagreementsid");
            Guid pandaId = Guid.Parse(entityToCreate.Ctas_ProposalsAndAgreementsId.ToString());
            Entity pandaResult = repo.Client.Retrieve("ctas_proposalsandagreements", pandaId, colset);
            string action = Constants.Create;

            if (pandaResult != null && pandaResult.Id != null && pandaResult.Id != Guid.Empty && pandaResult.Id == pandaId)
            {
                action = Constants.Update;
            }
            AssignCustomRecords(proposalsAndAgreements, entityToCreate, action);

            if (action == Constants.Create)
            {
                CreateRequest reqToCreate = new CreateRequest
                {
                    Target = proposalsAndAgreements
                };

                var response = (CreateResponse)repo.Client.Execute(reqToCreate);
                if (response.ResponseName == Constants.create && response.id != Guid.Empty)
                {
                    CreateResponseResult result = new CreateResponseResult();
                    var metadata = new MetadataTag
                    {
                        uri = string.Format(appConfig.MetadataUrl + Entities.proposalAndAgreementName + "({0})", response.id),
                        type = appConfig.ModelNamespace.ToString() + Delimiters.Dot + Constants.entityCustom + Entities.proposalAndAgreementName
                    };
                    entityToCreate.__metadata = metadata;
                    entityToCreate.Ctas_ProposalsAndAgreementsId = response.id;
                    result.d = entityToCreate;
                    return Ok(result);
                }
                return NoContent();
            }
            else
            {
                CreateResponseResult result = new CreateResponseResult();
                UpdateRequest reqToUpdate = new UpdateRequest();
                reqToUpdate.Target = proposalsAndAgreements;

                UpdateResponse response = (UpdateResponse)repo.Client.Execute(reqToUpdate);
                if (response != null)
                {
                    var metadata = new MetadataTag
                    {
                        uri = appConfig.MetadataUrl + Entities.proposalAndAgreementName + response.ResponseName,
                        type = appConfig.ModelNamespace.ToString() + Delimiters.Dot + Constants.entityCustom + Entities.proposalAndAgreementName
                    };
                    entityToCreate.__metadata = metadata;
                    result.d = entityToCreate;
                    return Ok(result);
                }
                return NoContent();
            }
        }
        catch (Exception ex)
        {
            error($"Exception:: Message: {ex.Message} || Trace: {ex.StackTrace}");
            return BadRequest(ex);
        }
    }

    [HttpPost("odata/proposalsandagreements({key})")]
    public IActionResult Update(Guid key, CustomProposalsAndAgreements entityToUpdate)
    {
        var systemUserName = repo.WhoAmI(Request);
        debug($"Request: {Request.Method}, Entity: {Entities.proposalAndAgreementName}, User: {systemUserName}");
        info("ProposalsAndAgreements Update started.");
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(entityToUpdate);

        try
        {
            Entity entityBeingUpdated = new Entity
            {
                LogicalName = Entities.proposalAndAgreementLogicalName.ToLower(),
                Id = key
            };
             AssignCustomRecords(entityBeingUpdated, entityToUpdate, Constants.Update);
            UpdateRequest reqToUpdate = new UpdateRequest();
            reqToUpdate.Target = entityBeingUpdated;
            UpdateResponse response = (UpdateResponse)repo.Client.Execute(reqToUpdate);
            if (response != null)
                return Ok(response);
            return BadRequest();
        }
        catch (Exception ex)
        {
            error($"Exception:: Message: {ex.Message} || Trace: {ex.StackTrace}");
            return BadRequest(ex);
        }
    }

    private void AssignCustomRecords(Entity entity, CustomProposalsAndAgreements clientEntity, string action)
    {
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(clientEntity);

        try
        {
            if (action == Constants.Update)
                entity[Attributes.ctas_proposalsandagreementsid] = clientEntity.Ctas_ProposalsAndAgreementsId;

            if (clientEntity.Ctas_Name != null)
                entity[Attributes.ctas_name] = clientEntity.Ctas_Name;
            if (clientEntity.Ctas_AzureDocumentUrl != null)
                entity[Attributes.ctas_azuredocumenturl] = clientEntity.Ctas_AzureDocumentUrl;
            if (clientEntity.Ctas_CustomerSignedDate != null && !string.IsNullOrEmpty(clientEntity.Ctas_CustomerSignedDate.Text))
                entity[Attributes.ctas_customersigneddate] = Convert.ToDateTime(clientEntity.Ctas_CustomerSignedDate.Text);
            if (clientEntity.Ctas_DocumentType != null && !string.IsNullOrEmpty(clientEntity.Ctas_DocumentType.Text))
                entity[Attributes.ctas_documenttype] = new OptionSetValue(Convert.ToInt32(clientEntity.Ctas_DocumentType.Text));
            if (clientEntity.Ctas_DocumentUrl != null)
                entity[Attributes.ctas_documenturl] = clientEntity.Ctas_DocumentUrl;
            if (clientEntity.Ctas_DocuSignEnvelopeId != null)
                entity[Attributes.ctas_docusignenvelopeid] = clientEntity.Ctas_DocuSignEnvelopeId;
            if (clientEntity.Ctas_ExpirationDate != null && !string.IsNullOrEmpty(clientEntity.Ctas_ExpirationDate.Text))
                entity[Attributes.ctas_expirationdate] = Convert.ToDateTime(clientEntity.Ctas_ExpirationDate.Text);
            if (clientEntity.Ctas_Opportunity != null && !string.IsNullOrEmpty(clientEntity.Ctas_Opportunity.Text))
                entity[Attributes.ctas_opportunity] = new EntityReference(Entities.opportunityLogicalName, new Guid(clientEntity.Ctas_Opportunity.Text));
            if (clientEntity.Ctas_PropagrmtCurrentRevisionId != null && !string.IsNullOrEmpty(clientEntity.Ctas_PropagrmtCurrentRevisionId.Text))
                entity[Attributes.ctas_propagrmtcurrentrevisionid] = new EntityReference(Entities.proposalAndAgreementLogicalName, new Guid(clientEntity.Ctas_PropagrmtCurrentRevisionId.Text));
            if (clientEntity.Ctas_SentDate != null && !string.IsNullOrEmpty(clientEntity.Ctas_SentDate.Text))
                entity[Attributes.ctas_sentdate] = Convert.ToDateTime(clientEntity.Ctas_SentDate.Text);
            if (clientEntity.Ctas_SignatureType != null && !string.IsNullOrEmpty(clientEntity.Ctas_SignatureType.Text))
                entity[Attributes.ctas_signaturetype] = new OptionSetValue(Convert.ToInt32(clientEntity.Ctas_SignatureType.Text));
            if (clientEntity.StateCode != null && !string.IsNullOrEmpty(clientEntity.StateCode.Text))
                entity[Attributes.statecode] = new OptionSetValue(Convert.ToInt32(clientEntity.StateCode.Text));
        }
        catch (Exception ex)
        {
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}||Request Entity:{jsonEntity}" );
            throw ex;
        }
    }

    public ProposalsAndAgreementController(ILogger<ProposalsAndAgreementController> logger,
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) :
        base(logger, memoryCache, configuration, crmRepo)
        {
        }
}