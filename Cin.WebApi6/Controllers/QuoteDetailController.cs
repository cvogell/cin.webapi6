using Cin.WebApi.Models;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using Newtonsoft.Json;
using Cin.WebApi;
using Cin.WebApi.Extensions;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.AspNetCore.Mvc;
using Cin.WebApi.Repos;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Cin.WebApi.CrmModels;

namespace Cin.WebApi6.Controllers;

public class QuoteDetailController : BaseController
{
    [HttpGet]
    [Route("odata/quotedetail")]
    public ResultSet UserQuery()
    {
        var systemUserName = repo.WhoAmI(Request);
        debug($"Request: {Request.Method}, Entity: {Entities.QuoteDetailName}, User: {systemUserName}");

        QuoteDetailResultSet result = new QuoteDetailResultSet();
        result.CustomQuoteDetail = new List<CustomQuoteDetail>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {

            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.QuoteDetailName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc = CommonClass.XmlFrame(Entities.QuoteDetailName, responseQuery.Message);
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            //List<CustomQuoteDetail> res = new List<CustomQuoteDetail>();
            //XmlSerializer ser = new XmlSerializer(typeof(QuoteDetailResultSet));
            //using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            //{
            //    result = (QuoteDetailResultSet)ser.Deserialize(reader);
            //    foreach (CustomQuoteDetail quoteDetail in result.CustomQuoteDetail)
            foreach (JsonEntity quoteDetail in resObj)
            {
                var cEntity = new CustomQuoteDetail();
                cEntity.__metadata = CreateMetaDataTag(Entities.QuoteDetailName, quoteDetail.Id);

                foreach (JsonEntityAttribute attr in quoteDetail.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "isproductoverridden":
                            cEntity.IsProductOverridden = new IsProductOverridden();
                            cEntity.IsProductOverridden.Text = ((bool)attr.Value).ToString();
                            break;
                        case "quoteid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                cEntity.QuoteId = new Quoteid();
                                cEntity.QuoteId.Text = jpv.Id.ToString();
                                cEntity.QuoteId.Name = jpv.Name;
                            }
                            break;
                        case "sequencenumber":
                            cEntity.SequenceNumber = new SequenceNumber();
                            cEntity.SequenceNumber.Text = ((long)attr.Value).ToString();
                            break;
                        case "lineitemnumber":
                            cEntity.LineItemNumber = ((long)attr.Value).ToString();
                            break;
                        case "producttypecode":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.ProductTypeCode = new ProductTypeCode();
                                cEntity.ProductTypeCode.Text = ((long)jpv.Value).ToString();
                            }
                            break;
                        case "po_itemno":
                            cEntity.po_ItemNo = (string)attr.Value;
                            break;
                        case "po_itemdelfreq":
                            cEntity.po_ItemDelFreq = (string)attr.Value;
                            break;
                        case "productdescription":
                            cEntity.ProductDescription = (string)attr.Value;
                            break;
                        case "quantity":
                            cEntity.Quantity = new Quantity();
                            cEntity.Quantity.Text = ((double)attr.Value).ToString();
                            break;
                        case "po_inventory":
                            cEntity.po_Inventory = new Po_inventory();
                            cEntity.po_Inventory.Text = ((double)attr.Value).ToString();
                            break;
                        case "po_usagecd":
                            cEntity.po_UsageCD = (string)attr.Value;
                            break;
                        case "createdby":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Createdby = new QuoteCreatedby();
                                    cEntity.Createdby.Text = jpv.Id.ToString();
                                    cEntity.Createdby.Name = jpv.Name;
                                }
                            }
                            break;
                        case "extendedamount":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.ExtendedAmount = new ExtendedAmount();
                                cEntity.ExtendedAmount.Text = jpv.Value.ToString();
                            }
                            break;
                        case "priceperunit":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.PricePerUnit = new PricePerUnit();
                                cEntity.PricePerUnit.Text = jpv.Value.ToString();
                            }
                            break;
                        case "transactioncurrencyid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.TransactionCurrencyId = new TransactionCurrencyId();
                                    cEntity.TransactionCurrencyId.Text = jpv.Id.ToString();
                                    cEntity.TransactionCurrencyId.Name = jpv.Name;
                                }
                            }
                            break;
                        case "quotedetailid":
                            cEntity.QuoteDetailId = new Guid((string)attr.Value);
                            break;
                    }
                }
                foreach (JsonEntityFormattedValue fmv in quoteDetail.FormattedValues)
                {
                    switch (fmv.Key)
                    {
                        case "isproductoverridden":
                            cEntity.IsProductOverridden.Name = fmv.Value;
                            break;
                        case "sequencenumber":
                            cEntity.SequenceNumber.Formattedvalue = fmv.Value;
                            break;
                        case "producttypecode":
                            cEntity.ProductTypeCode.Formattedvalue = fmv.Value;
                            break;
                        case "extendedamount":
                            cEntity.ExtendedAmount.Formattedvalue = fmv.Value;
                            break;
                        case "priceperunit":
                            cEntity.PricePerUnit.Formattedvalue = fmv.Value;
                            break;
                    }
                }

                result.CustomQuoteDetail.Add(cEntity);
            }
            response.d = new ResultsD();
            response.d.results = result.CustomQuoteDetail;

            debug($"Request: {Request.Method}, Entity: {Entities.QuoteDetailName}, User: {systemUserName}, Payload Length: {result.CustomQuoteDetail.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}");
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }


    [HttpDelete]
    [Route("odata/quotedetail({key})")]
    public IActionResult Delete(Guid key)
    {
        try
        {
            OrganizationResponse responseWhoamI = repo.WhoAmI(Request);
            string systemUserName = (string)RetrieveFieldFromEntityByKey(Request, Entities.SystemUserLogicalName, "systemuserid", ((WhoAmIResponse)responseWhoamI).UserId, Attributes.fullName);
            debug($"Request: {Request.Method}, user: {systemUserName}");
            repo.Client.Delete(Entities.QuoteDetailLogicalName, key);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error("Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}");
            return BadRequest(ex.Message);
        }
        return Ok();
    }


    [HttpPost]
    [Route("odata/quotedetail")]
    public IActionResult Create(CustomQuoteDetail quoteDetailToCreate)
    {
        OrganizationResponse responseWhoamI = repo.WhoAmI(Request);
        var systemUserName = RetrieveFieldFromEntityByKey(
            Request, Entities.SystemUserLogicalName, "systemuserid",
            ((WhoAmIResponse)responseWhoamI).UserId, Attributes.fullName);
        info($"UserQuery query with user {systemUserName}");
        debug($"Request: {Request.Method}, Entity: {Entities.QuoteDetailName}, User: {systemUserName}");

        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(quoteDetailToCreate);

        try
        {
            if (quoteDetailToCreate.IsProductOverridden == null || (quoteDetailToCreate.IsProductOverridden != null && string.IsNullOrEmpty(quoteDetailToCreate.IsProductOverridden.Text)))
                return BadRequest(Constants.productOverridenCheck.ToString());

            if (quoteDetailToCreate.IsProductOverridden.Text.ToBool() == false && (quoteDetailToCreate.ProductId == null || (quoteDetailToCreate.ProductId != null && string.IsNullOrEmpty(quoteDetailToCreate.ProductId.Text))))
                return BadRequest(Constants.opportunityProductIdCheck.ToString());

            if (quoteDetailToCreate.IsProductOverridden.Text.ToBool() == false && (quoteDetailToCreate.UoMId == null || (quoteDetailToCreate.UoMId != null && string.IsNullOrEmpty(quoteDetailToCreate.UoMId.Text))))
                return BadRequest(Constants.opportunityUOMIdCheck.ToString());

            if (quoteDetailToCreate.IsProductOverridden.Text.ToBool() == true && quoteDetailToCreate.ProductDescription == null)
                return BadRequest(Constants.opportunityProductDescCheck.ToString());


            Entity quoteDetail = new Entity();
            //quoteDetail.Id = quoteDetailToCreate.QuoteDetailId;
            quoteDetail = AssignCustomRecords(quoteDetailToCreate, Constants.Insert);

            CreateRequest createRequest = new CreateRequest();
            createRequest.Target = quoteDetail;
            var response = (CreateResponse)repo.Client.Execute(createRequest);

            if (response.ResponseName == Constants.create && response.id != Guid.Empty)
            {
                CreateResponseResult result = new CreateResponseResult();
                var metadata = new MetadataTag();
                metadata.uri = appConfig.MetadataUrl + Entities.QuoteName + quoteDetail.Id;
                metadata.type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + Entities.QuoteName;
                quoteDetailToCreate.__metadata = metadata;
                quoteDetailToCreate.QuoteDetailId = response.id;
                result.d = quoteDetailToCreate;
                debug($"Request: {Request.Method}, Entity: {Entities.QuoteDetailName}, User: {systemUserName}");
                return Ok(result);
            }
            else
            {
                error($"Unexpected Response::Name:{response.ResponseName}||ToString:{response}");
                debug($"Request: {Request.Method}, Entity: {Entities.QuoteDetailName}, User: {systemUserName}");    
                return BadRequest(response);
            }
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}||Request Entity:{jsonEntity}");
            return BadRequest(ex.Message);
        }
    }
    #region Json Input payload 
    //{
    //    "QuoteDetailId": "755e1be6-d5c5-e711-a94c-000d3a1be57e",
    //    "QuoteId": {
    //    "Id": "3f65589f-987c-e711-a947-000d3a1be57e",
    //    "LogicalName": "quote",    
    //    "KeyAttributes": [ ],
    //    "RowVersion": null
    //    },
    //    "ProductDescription": "Deep Clean Rajkumar",
    //    "ProductTypeCode": {
    //            "Value": 1
    //        },
    //    "ParentBundleId": null,
    //    "SequenceNumber": 1,
    //    "IsProductOverridden": true,
    //    "TransactionCurrencyId": {
    //    "Id": "5de3e994-eb3b-e311-93fd-00155dcfd124",
    //    "LogicalName": "transactioncurrency",    
    //    "KeyAttributes": [ ],
    //    "RowVersion": null
    //    },
    //    "Quantity": 10000,
    //    "PricePerUnit": {
    //    "Value": 20000
    //    },
    //    "ExtendedAmount": {
    //    "Value": 200000
    //    },
    //    "ProductId": null,
    //    "UoMId": null,
    //    "po_ItemNo": null,
    //    "po_UsageCD": null,
    //    "po_ItemDelFreq": null,
    //    "po_Inventory": null,
    //    "LineItemNumber": null
    //}


    #endregion

    [HttpPost("odata/quotedetail({key})")]
    public IActionResult Update(Guid key, CustomQuoteDetail quoteDetailToUpdate)
    {
        OrganizationResponse responseWhoamI = repo.WhoAmI(Request);
        var systemUserName = RetrieveFieldFromEntityByKey(
            Request, Entities.SystemUserLogicalName, "systemuserid",
            ((WhoAmIResponse)responseWhoamI).UserId, Attributes.fullName);
        debug($"Request: {Request.Method}, Entity: {Entities.QuoteDetailName}, User: {systemUserName}");

        
        try
        {
            Entity quoteDetail = new Entity();
            quoteDetailToUpdate.QuoteDetailId = key;
            quoteDetail = AssignCustomRecords(quoteDetailToUpdate, Constants.Update);
            debug($"Request: {Request.Method}, Entity: {Entities.QuoteDetailName}, User: {systemUserName}");
            UpdateRequest request = new UpdateRequest();
            request.Target = quoteDetail;
            UpdateResponse response = (UpdateResponse)repo.Client.Execute(request);
            if (response != null)
                return NoContent();
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}||");
            return BadRequest(ex.Message);
        }
        debug($"Request: {Request.Method}, Entity: {Entities.QuoteDetailName}, User: {systemUserName}");
        return Ok();
    }

    private Entity AssignCustomRecords(CustomQuoteDetail clientEntity, string action)
    {
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(clientEntity);

        try
        {
            Entity quoteDetail = new Entity();
            quoteDetail.Id = new Guid(clientEntity.QuoteDetailId.ToString());

            quoteDetail.LogicalName = Entities.QuoteDetailLogicalName;

            if (action == Constants.Update)
                quoteDetail[Attributes.quotedetailid] = clientEntity.QuoteDetailId;
            if (clientEntity.OwnerId != null && !string.IsNullOrEmpty(clientEntity.OwnerId.Text))
                quoteDetail[Attributes.ownerId] = new EntityReference(Constants.systemUser, new Guid(clientEntity.OwnerId.Text));
            if (clientEntity.QuoteId != null && !string.IsNullOrEmpty(clientEntity.QuoteId.Text))
                quoteDetail[Attributes.quoteid] = new EntityReference(Entities.QuoteLogicalName, new Guid(clientEntity.QuoteId.Text));
            if (clientEntity.ProductDescription != null)
                quoteDetail[Attributes.productdescription] = clientEntity.ProductDescription;
            if (clientEntity.ProductTypeCode != null && !string.IsNullOrEmpty(clientEntity.ProductTypeCode.Text))
                quoteDetail[Attributes.producttypecode] = new OptionSetValue(Convert.ToInt32(clientEntity.ProductTypeCode.Text));
            if (!string.IsNullOrEmpty(clientEntity.ParentBundleId))
                quoteDetail[Attributes.parentbundleid] = new Guid(clientEntity.ParentBundleId);
            if (clientEntity.SequenceNumber != null && !string.IsNullOrEmpty(clientEntity.SequenceNumber.Text))
                quoteDetail[Attributes.sequencenumber] = Convert.ToInt32(clientEntity.SequenceNumber.Text);
            if (clientEntity.IsProductOverridden != null && !string.IsNullOrEmpty(clientEntity.IsProductOverridden.Text))
                quoteDetail[Attributes.isproductoverridden] = clientEntity.IsProductOverridden.Text.ToBool();
            if (clientEntity.TransactionCurrencyId != null && !string.IsNullOrEmpty(clientEntity.TransactionCurrencyId.Text))
                quoteDetail[Attributes.transactioncurrencyid] = new EntityReference(Entities.transactionCurrency, new Guid(clientEntity.TransactionCurrencyId.Text));
            if (clientEntity.Quantity != null && !string.IsNullOrEmpty(clientEntity.Quantity.Text))
                quoteDetail[Attributes.quantity] = Convert.ToDecimal(clientEntity.Quantity.Text);
            if (clientEntity.PricePerUnit != null && !string.IsNullOrEmpty(clientEntity.PricePerUnit.Text))
                quoteDetail[Attributes.priceperunit] = new Money(Convert.ToDecimal(clientEntity.PricePerUnit.Text));
            if (clientEntity.ExtendedAmount != null && !string.IsNullOrEmpty(clientEntity.ExtendedAmount.Text))
                quoteDetail[Attributes.extendedamount] = new Money(Convert.ToDecimal(clientEntity.ExtendedAmount.Text));
            if (clientEntity.ProductId != null && !string.IsNullOrEmpty(clientEntity.ProductId.Text) && clientEntity.IsProductOverridden.Text.ToBool() == false)
                quoteDetail[Attributes.productid] = new EntityReference(Entities.ProductLogicalName, new Guid(clientEntity.ProductId.Text));
            if (clientEntity.po_ItemNo != null)
                quoteDetail[Attributes.po_itemno] = clientEntity.po_ItemNo;
            if (clientEntity.po_UsageCD != null)
                quoteDetail[Attributes.po_usagecd] = clientEntity.po_UsageCD;
            if (clientEntity.po_ItemDelFreq != null)
                quoteDetail[Attributes.po_itemdelfreq] = clientEntity.po_ItemDelFreq;
            if (clientEntity.po_Inventory != null && !string.IsNullOrEmpty(clientEntity.po_Inventory.Text))
                quoteDetail[Attributes.po_inventory] = Convert.ToDecimal(clientEntity.po_Inventory.Text);
            if (clientEntity.LineItemNumber != null)
                quoteDetail[Attributes.lineitemnumber] = Convert.ToInt32(clientEntity.LineItemNumber);
            if (clientEntity.UoMId != null && !string.IsNullOrEmpty(clientEntity.UoMId.Text) && clientEntity.IsProductOverridden.Text.ToBool() == false)
                quoteDetail[Attributes.UOMId] = new EntityReference(Entities.UOMLogicalName, new Guid(clientEntity.UoMId.Text));
            return quoteDetail;
        }
        catch (Exception ex)
        {
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}||Request Entity:{jsonEntity}");
            throw ex;
        }
    }

    public QuoteDetailController(ILogger<QuoteDetailController> logger, IMemoryCache memoryCache,
        IConfiguration configuration, CrmRepo crmRepo) :
            base(logger, memoryCache, configuration, crmRepo)
    { }

}
