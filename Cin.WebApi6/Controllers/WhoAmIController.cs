using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.Extensions.Caching.Memory;
using System.Net;
using Cin.WebApi.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class WhoAmIController : BaseController
{

    [HttpGet]
    [Route("odata/WhoAmI")]
    public IActionResult GetWhoAmI()
    {
        //var httpResponse = new HttpResponse();
        string oauthToken = Request.GetAuthToken();
        var systemUserName = repo.WhoAmI(Request);
        string timeperiod = DateTime.Now.Ticks.ToString(); 
        string userName = (string)RetrieveFieldFromEntityByKey(Request, Entities.SystemUserLogicalName, "systemuserid", ((WhoAmIResponse)systemUserName).UserId, Attributes.fullName);
        string userEmail = (string)RetrieveFieldFromEntityByKey(Request, Entities.SystemUserLogicalName, "systemuserid", ((WhoAmIResponse)systemUserName).UserId, Attributes.internalEmailAddress);       
        try
        {
            warning(DateTime.Now.ToString());
            string userId = ((WhoAmIResponse)systemUserName).UserId.ToString();
            debug($"Debug::Explicit WhoAmI Call TimeStamp: {timeperiod}, AuthToken: {oauthToken}, Entity: {Entities.SystemUserLogicalName}, UserName: {userName}, Response: ");
            //object userIds = null;
            //String userId = null;
            //if (responseWhoamI.Results.ContainsKey("UserId"))
            //{
            //    responseWhoamI.Results.TryGetValue("UserId", out userIds);
            //    if (userIds != null && (!(String.IsNullOrEmpty(userIds.ToString()))))
            //    {
            //        userId = userIds.ToString();
            //    }
            //}

            if (!String.IsNullOrEmpty(userId))
                Response.Headers.Add("UserId", userId);
            if (!String.IsNullOrEmpty(userName))
                Response.Headers.Add("UserName", userName);
            if (!String.IsNullOrEmpty(userEmail))
                Response.Headers.Add("UserEmail", userEmail);
            debug($"Explicit WhoAmI Call TimeStamp: {timeperiod}, AuthToken: {oauthToken}, Entity: {Entities.SystemUserLogicalName}, UserName: {userName}");
        }
        catch (Exception ex)
        {
            error($"Exception in WhoAmI Call: {ex.GetType()}: {ex.Message}");
            //Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            Response.Headers.Add("WWW-Authenticate", "Security Token Expired!");
            error($"Explicit WhoAmI Call TimeStamp: {timeperiod}, AuthToken: {oauthToken}, Entity: {Entities.SystemUserLogicalName}, UserName: {userName}");
            return Unauthorized();
        }
        return Ok();
    }

    public WhoAmIController(ILogger<WhoAmIController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
