using Cin.WebApi.CrmModels;
using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Cin.WebApi.Xrm;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Web.Http.OData;
using AllowedQueryOptions = System.Web.Http.OData.Query.AllowedQueryOptions;
using Newtonsoft.Json;
using Cin.WebApi;
using System.Net;
using Cin.WebApi.Extensions;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.AspNetCore.Mvc;
namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]

public class ProductRelationshipController : BaseController
{
    [HttpGet("odata/productrelationship")]
    public IActionResult UserQuery()
    {
        info("ProductRelationship.UserQuery() Started");
        var systemUserName = repo.WhoAmI(Request);
        info($"Debug: Request: {Request.Method}, Entity: {Entities.productRelationshipName}, User: {systemUserName}");
        ProductRelationshipResultSet result = new ProductRelationshipResultSet
        {
            CustomProductRelationship = new List<CustomProductRelationship>()
        };
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();

        try
        {
            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.productRelationshipName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD
                {
                    results = responseQuery.Message
                };
                return BadRequest(response);
            }
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            foreach (JsonEntity productRelationship in resObj)
                {
                var cEntity = new CustomProductRelationship
                {
                    __metadata = CreateMetaDataTag(Entities.productRelationshipName, productRelationship.Id)
                };
                
                foreach (JsonEntityAttribute attr in productRelationship.Attributes)
                    {
                        switch (attr.Key)
                        {
                            case "productsubstituteid":
                                if (!string.IsNullOrEmpty((string)(attr.Value.ToString())))
                                    cEntity.Productsubstituteid = new Guid((string)(attr.Value.ToString()));
                                break;
                            case "productid":
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                cEntity.Productid = new Productid
                                {
                                    Text = jpv.Id.ToString(),
                                    Name = jpv.Name
                                };
                            }
                                break;
                            case "substitutedproductid":
                                string sVal2 = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv2 = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal2);
                                if (jpv2.Id != Guid.Empty)
                                {
                                cEntity.Substitutedproductid = new Substitutedproductid
                                {
                                    Text = jpv2.Id.ToString(),
                                    Name = jpv2.Name
                                };
                            }
                                break;
                            case "direction":
                                string sVal3 = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv3 = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal3);
                                cEntity.Direction = new Direction
                                {
                                    Text = jpv3.Value.ToString()
                                };
                            break;
                            case "salesrelationshiptype":
                                string sVal4 = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv4 = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal4);
                                cEntity.Salesrelationshiptype = new Salesrelationshiptype
                                {
                                    Text = jpv4.Value.ToString()
                                };
                            break;
                        }
                    }

                    foreach (JsonEntityFormattedValue fmv in productRelationship.FormattedValues)
                    {
                        switch (fmv.Key)
                        {
                            case "salesrelationshiptype":
                                cEntity.Salesrelationshiptype.Name = fmv.Value;
                                break;
                            case "direction":
                                cEntity.Direction.Name = fmv.Value;
                                break;
                        }
                    }
                    result.CustomProductRelationship.Add(cEntity);
                }
            response.d = new ResultsD
            {
                results = result.CustomProductRelationship
            };
            debug($"Request: {Request.Method}, Entity: {Entities.productRelationshipName}, User: {systemUserName}, Payload Length: {result.CustomProductRelationship.Count}");
            return Ok(response);
        }
        catch (Exception ex)
        {
            error($"Exception:: Message: {ex.Message}|| Trace: {ex.StackTrace}");            
            return BadRequest(ex);
        }
    }

    public ProductRelationshipController(ILogger<ProductRelationshipController> logger,
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo):
        base(logger, memoryCache, configuration,crmRepo)
        {
        }
}