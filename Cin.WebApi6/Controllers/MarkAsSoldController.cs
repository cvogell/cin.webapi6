﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.WebServiceClient;
using Microsoft.Xrm.Sdk;
using System.Diagnostics.Contracts;
using System.Net;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;
using Microsoft.AspNetCore.Http.Extensions;
using Cin.WebApi.Extensions;
using System.Web.Http.Results;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class MarkAsSoldController : BaseController
{
    [HttpDelete("odata/markassold({key})")]
    public IActionResult Delete(Guid key)
    {
        HttpResponseMessage httpResponse = new HttpResponseMessage(HttpStatusCode.NoContent);

        try
        {
            _ = repo.WhoAmI(Request);
            info($"[DELETE] odata/markassold({key}) called by user {repo.Client.CallerId}");
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);
            if (key == Guid.Empty)
                return BadRequest(Constants.mandatoryId);

            repo.Client.Delete(Entities.markAsSoldLogicalName, key);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            return BadRequest(ex.Message);
        }
        return NoContent();
    }

    [HttpGet("odata/markassold")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/markassold called by user {repo.Client.CallerId}");
        MarkAsSoldResultSet result = new MarkAsSoldResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {

            responseQuery = repo.RetrieveUserQuery(Request, Entities.markAsSoldName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.markAsSoldName, responseQuery.Message);

            List<CustomMarkAsSold> res = new List<CustomMarkAsSold>();
            XmlSerializer ser = new XmlSerializer(typeof(MarkAsSoldResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (MarkAsSoldResultSet)ser.Deserialize(reader);
                foreach (CustomMarkAsSold markAsSold in result.CustomMarkAsSold)
                {
                    markAsSold.__metadata = CreateMetaDataTag(Entities.markAsSoldName, markAsSold.ActivityId.ToString());
                }
                res = result.CustomMarkAsSold;
            }
            response.d = new ResultsD();
            response.d.results = res;

            debug($"result.CustomMarkAsSold.Count: {result.CustomMarkAsSold.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    [HttpPost("odata/markassold")]
    public IActionResult Create(CustomMarkAsSold markAsSoldCreate)
    {
        _ = repo.WhoAmI(Request);
        info($"[POST] odata/markassold called by user {repo.Client.CallerId}");

        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(markAsSoldCreate);

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            Entity markAsSold = new Entity();
            markAsSold.Id = markAsSoldCreate.ActivityId;
            markAsSold.LogicalName = Entities.markAsSoldLogicalName;

            if (markAsSoldCreate.Po_SoldVolume == null || (markAsSoldCreate.Po_SoldVolume != null && string.IsNullOrEmpty(markAsSoldCreate.Po_SoldVolume.Text)))
                return BadRequest(Constants.soldVolume);

            if (markAsSoldCreate.Po_Sold_Acct_Type == null || (markAsSoldCreate.Po_Sold_Acct_Type != null && string.IsNullOrEmpty(markAsSoldCreate.Po_Sold_Acct_Type.Text)))
                return BadRequest(Constants.soldAcctType);

            if (markAsSoldCreate.ctas_Agreement_Date == null || (markAsSoldCreate.ctas_Agreement_Date != null && string.IsNullOrEmpty(markAsSoldCreate.ctas_Agreement_Date.Text)))
                return BadRequest(Constants.setToInstall);

            try
            {
                AssignMarkAsSoldCustomValues(markAsSold, markAsSoldCreate);
            }
            catch (Exception ex)
            {
                error(ex.Message, ex);
                return StatusCode(500);
            }

            CreateRequest reqCreateMarkAsSold = new CreateRequest();
            reqCreateMarkAsSold.Target = markAsSold;
            CreateResponse responseMarkAsSold = (CreateResponse)repo.Client.Execute(reqCreateMarkAsSold);

            if (responseMarkAsSold.ResponseName == Constants.create && responseMarkAsSold.id != Guid.Empty)
            {
                CreateResponseResult result = new CreateResponseResult();
                markAsSoldCreate.__metadata = CreateMetaDataTag(Entities.markAsSoldName, responseMarkAsSold.id.ToString());
                markAsSoldCreate.ActivityId = responseMarkAsSold.id;
                result.d = markAsSoldCreate;
                return Created(Request.GetDisplayUrl(),result);
            }
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }

        return NoContent();
    }

    [HttpPost("odata/markassold({key})")]
    public IActionResult Update(Guid key, CustomMarkAsSold markAsSoldUpdate)
    {
        _ = repo.WhoAmI(Request);
        info($"[update] odata/markassold({key}) called by user {repo.Client.CallerId}");

        HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.NoContent);
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(markAsSoldUpdate);

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            Entity markAsSold = new Entity();
            markAsSold.LogicalName = Entities.markAsSoldLogicalName;

            if (key == Guid.Empty)
                return BadRequest(Constants.mandatoryId);

            markAsSold[Attributes.activityid] = key;

            try
            {
                AssignMarkAsSoldCustomValues(markAsSold, markAsSoldUpdate);
            }
            catch(Exception ex)
            {
                error(ex.Message, ex);
                return StatusCode(500);
            }

            bool bCompleted = false;
            int stateCode = 0;
            Entity markAsSold365 = repo.Client.Retrieve(Entities.markAsSoldLogicalName, key, new ColumnSet(true));
            stateCode = ((OptionSetValue)markAsSold365.Attributes[Attributes.statecode]).Value;

            if (stateCode == 1)
            {
                SetStateRequest openMarkAsSold = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference(Entities.markAsSoldLogicalName, markAsSold365.Id),

                    State = new OptionSetValue((int)0),
                    Status = new OptionSetValue((int)1)
                };
                repo.Client.Execute(openMarkAsSold);

                bCompleted = true;
            }

            UpdateRequest reqUpdateMarkAsSold = new UpdateRequest();
            reqUpdateMarkAsSold.Target = markAsSold;
            UpdateResponse responseMarkAsSold = (UpdateResponse)repo.Client.Execute(reqUpdateMarkAsSold);

            if (bCompleted)
            {
                SetStateRequest completeMarkAsSold = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference(Entities.markAsSoldLogicalName, markAsSold365.Id),

                    State = new OptionSetValue((int)1),
                    Status = new OptionSetValue((int)2)
                };
                repo.Client.Execute(completeMarkAsSold);

                bCompleted = false;
            }


            if (responseMarkAsSold != null)
                return NoContent();

            return NoContent();
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }
    }


    protected void AssignMarkAsSoldCustomValues(Entity markAsSold, CustomMarkAsSold markAsSoldAssign)
    {
        try
        {
            if (markAsSoldAssign.ctas_Agreement_Date != null && !string.IsNullOrEmpty(markAsSoldAssign.ctas_Agreement_Date.Text))
                markAsSold[Attributes.ctas_agreement_date] = Convert.ToDateTime(markAsSoldAssign.ctas_Agreement_Date.Text);
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw new Exception ("Error in markAsSoldAssign.ctas_Agreement_Date.Text: " + 
                markAsSoldAssign.ctas_Agreement_Date.Text);
        }

        try
        {
            if (markAsSoldAssign.Po_AccountSold != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_AccountSold.Text))
                markAsSold[Attributes.po_accountsold] = markAsSoldAssign.Po_AccountSold.Text.ToBool();
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_Actual_Sold_Date != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_Actual_Sold_Date.Text))
                markAsSold[Attributes.po_actual_sold_date] = Convert.ToDateTime(markAsSoldAssign.Po_Actual_Sold_Date.Text);
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_Actual_Sold_Volume != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_Actual_Sold_Volume.Text))
                markAsSold[Attributes.po_actual_sold_volume] = new Money(Convert.ToDecimal(markAsSoldAssign.Po_Actual_Sold_Volume.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_Businessdivison != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_Businessdivison.Text))
                markAsSold[Attributes.po_businessdivison] = new EntityReference(Attributes.po_businessdivision, new Guid(markAsSoldAssign.Po_Businessdivison.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_EstimatedSoldVolume != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_EstimatedSoldVolume.Text))
                markAsSold[Attributes.po_estimatedsoldvolume] = new Money(Convert.ToDecimal(markAsSoldAssign.Po_EstimatedSoldVolume.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_Locationnumber != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_Locationnumber.Text))
                markAsSold[Attributes.po_locationnumber] = new EntityReference(Entities.cintasLocationLogicalName, new Guid(markAsSoldAssign.Po_Locationnumber.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.RegardingObjectId != null && markAsSoldAssign.RegardingObjectId.Type != null && markAsSoldAssign.RegardingObjectId.Type == "1")
                if (markAsSoldAssign.RegardingObjectId != null && !string.IsNullOrEmpty(markAsSoldAssign.RegardingObjectId.Text))
                    markAsSold[Attributes.RegardingObjectId] = new EntityReference(Entities.accountLogicalName, new Guid(markAsSoldAssign.RegardingObjectId.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.ctas_primary_rep != null && !string.IsNullOrEmpty(markAsSoldAssign.ctas_primary_rep.Text))
                markAsSold[Attributes.ctas_primary_rep] = 
                    new EntityReference(Entities.SystemUserLogicalName, 
                    new Guid(markAsSoldAssign.ctas_primary_rep.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_MarkComplete != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_MarkComplete.Text))
                markAsSold[Attributes.po_markcomplete] = markAsSoldAssign.Po_MarkComplete.Text.ToBool();
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_NAC != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_NAC.Text))
                markAsSold[Attributes.po_nac] = new EntityReference(Entities.NacProcessingLogicalName, new Guid(markAsSoldAssign.Po_NAC.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_Sold_Acct_Type != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_Sold_Acct_Type.Text))
                markAsSold[Attributes.po_sold_acct_type] = new OptionSetValue(Convert.ToInt32(markAsSoldAssign.Po_Sold_Acct_Type.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_SoldDate != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_SoldDate.Text))
                markAsSold[Attributes.po_solddate] = Convert.ToDateTime(markAsSoldAssign.Po_SoldDate.Text);
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_SoldVolume != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_SoldVolume.Text))
                markAsSold[Attributes.po_soldvolume] = new Money(Convert.ToDecimal(markAsSoldAssign.Po_SoldVolume.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.TransactionCurrencyId != null && !string.IsNullOrEmpty(markAsSoldAssign.TransactionCurrencyId.Text))
                markAsSold[Attributes.transactioncurrencyid] = new EntityReference(Entities.transactionCurrency, new Guid(markAsSoldAssign.TransactionCurrencyId.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.OwnerId != null && !string.IsNullOrEmpty(markAsSoldAssign.OwnerId.Text))
                markAsSold[Attributes.ownerId] = new EntityReference(Entities.SystemUserLogicalName, new Guid(markAsSoldAssign.OwnerId.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_opportunity != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_opportunity.Text))
                markAsSold[Attributes.po_opportunity] = new EntityReference(Entities.opportunityLogicalName, new Guid(markAsSoldAssign.Po_opportunity.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Po_businessline != null && !string.IsNullOrEmpty(markAsSoldAssign.Po_businessline.Text))
                markAsSold[Attributes.po_businessline] = new OptionSetValue(Convert.ToInt32(markAsSoldAssign.Po_businessline.Text));
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

        try
        {
            if (markAsSoldAssign.Ctas_power_add != null && !string.IsNullOrEmpty(markAsSoldAssign.Ctas_power_add.Text))
                markAsSold[Attributes.ctas_PowerAdd] = markAsSoldAssign.Ctas_power_add.Text.ToBool();
        }
        catch (Exception exc)
        {
            error(exc.Message, exc);
            throw;
        }

    }


    public MarkAsSoldController(ILogger<MarkAsSoldController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
