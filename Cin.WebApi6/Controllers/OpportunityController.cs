﻿using Cin.WebApi.Models;
using Cin.WebApi;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.ServiceModel;
using Microsoft.AspNetCore.Http.Extensions;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class OpportunityController : BaseController
{
    [HttpGet("odata/opportunity")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/opportunity called by user {repo.Client.CallerId}");

        OpportunityResultSet result = new OpportunityResultSet();
        result.CustomOpportunity = new List<CustomOpportunity>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {
            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.opportunityName, 
                new string[] { Attributes.cintaslocation });
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            foreach (JsonEntity opportunity in resObj)
            {
                var cEntity = new CustomOpportunity();
                cEntity.__metadata = CreateMetaDataTag(Entities.opportunityName, opportunity.Id);

                foreach (JsonEntityAttribute attr in opportunity.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "po_numberofsites":
                            {
                                cEntity.Po_NumberOfSites = new Po_NumberOfSites();
                                cEntity.Po_NumberOfSites.Text = ((long)attr.Value).ToString();
                                cEntity.Po_NumberOfSites.Formattedvalue = ((long)attr.Value).ToString();
                                break;
                            }
                        case "estimatedclosedate":
                            cEntity.EstimatedCloseDate = new EstimatedCloseDate();
                            cEntity.EstimatedCloseDate.Text = ((DateTime)attr.Value).ToString();
                            cEntity.EstimatedCloseDate.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.EstimatedCloseDate.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "po_proposalsentdate":
                            cEntity.Po_ProposalSentDate = new Po_ProposalSentDate();
                            cEntity.Po_ProposalSentDate.Text = ((DateTime)attr.Value).ToString();
                            cEntity.Po_ProposalSentDate.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.Po_ProposalSentDate.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "po_proposalexpireddate":
                            cEntity.Po_ProposalExpiredDate = new Po_ProposalExpiredDate();
                            cEntity.Po_ProposalExpiredDate.Text = ((DateTime)attr.Value).ToString();
                            cEntity.Po_ProposalExpiredDate.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.Po_ProposalExpiredDate.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "po_13weekenddate":
                            cEntity.Po_13WeekendDate = new Po_13WeekendDate();
                            cEntity.Po_13WeekendDate.Text = ((DateTime)attr.Value).ToString();
                            cEntity.Po_13WeekendDate.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.Po_13WeekendDate.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "stageid":
                            cEntity.StageId = (string)attr.Value;
                            break;
                        case "ctas_cintas_location":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Ctas_Cintas_Location = jpv.Id.ToString();
                                }
                            }
                            //cEntity.Ctas_Cintas_Location = (string)attr.Value;
                            break;
                        case "po_opportunitytype":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.Po_OpportunityType = new Po_OpportunityType();
                                cEntity.Po_OpportunityType.Text = jpv.Value.ToString();
                                break;
                            }
                        case "modifiedon":
                            cEntity.ModifiedOn = new ModifiedOn();
                            cEntity.ModifiedOn.Text = ((DateTime)attr.Value).ToString();
                            cEntity.ModifiedOn.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.ModifiedOn.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "name":
                            cEntity.Name = (string)attr.Value;
                            break;
                        case "po_targetstage":
                            cEntity.Po_TargetStage = (string)attr.Value;
                            break;
                        case "po_reasonforagreementrejection":
                            cEntity.Po_ReasonForAgreementRejection = (string)attr.Value;
                            break;
                        case "po_rejectedby":
                            cEntity.Po_RejectedBy = new RejectedBy();
                            cEntity.Po_RejectedBy.Text = (string)attr.Value;
                            cEntity.Po_RejectedBy.Name = (string)attr.Value;
                            break;
                        case "opportunityid":
                            cEntity.OpportunityId = new Guid((string)attr.Value);
                            break;
                        case "parentaccountid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.ParentAccountId = new ParentAccountid();
                                    cEntity.ParentAccountId.Text = jpv.Id.ToString();
                                    cEntity.ParentAccountId.Name = jpv.Name;
                                }
                            }
                            break;
                        case "parentcontactid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.ParentContactId = new ParentContactId();
                                    cEntity.ParentContactId.Text = jpv.Id.ToString();
                                    cEntity.ParentContactId.Name = jpv.Name;
                                }
                            }
                            break;
                        case "po_businessdivisionid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Po_BusinessDivisionId = new Po_BusinessDivisionId();
                                    cEntity.Po_BusinessDivisionId.Text = jpv.Id.ToString();
                                    cEntity.Po_BusinessDivisionId.Name = jpv.Name;
                                }
                            }
                            break;
                        case "createdby":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Createdby = new Createdby();
                                    cEntity.Createdby.Text = jpv.Id.ToString();
                                    cEntity.Createdby.Name = jpv.Name;
                                }
                            }
                            break;
                        case "po_installedvolume":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.Po_ActualVolume = new Po_ActualVolumeFV();
                                cEntity.Po_ActualVolume.Text = jpv.Value.ToString();
                                break;
                            }
                        case "estimatedvalue":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.EstimatedValue = new Estimatedvalue();
                                cEntity.EstimatedValue.Text = jpv.Value.ToString();
                                break;
                            }
                        case "po_markedassoldvolume":
                            {
                                //po_wornbinstalledvolume
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.Po_MarkedAsSoldWeeklyVolume = new Po_MarkedAsSoldWeeklyVolumeFV();
                                cEntity.Po_MarkedAsSoldWeeklyVolume.Text = jpv.Value.ToString();
                                break;
                            }
                        case "po_wornbinstalledvolume":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.Po_WorInstalledVolume = new Po_WorInstalledVolume();
                                cEntity.Po_WorInstalledVolume.Text = jpv.Value.ToString();
                                break;
                            }
                        case "po_keyedwornbuninstalledvolume":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.Po_WorNewBusinessVolume = new Po_WorNewBusinessVolumeFV();
                                cEntity.Po_WorNewBusinessVolume.Text = jpv.Value.ToString();
                                break;
                            }
                        case "po_keyeduninstalledvolume":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.Po_OrderedKeyedWeeklyVolume = new Po_OrderedKeyedWeeklyVolumeFV();
                                cEntity.Po_OrderedKeyedWeeklyVolume.Text = jpv.Value.ToString();
                                break;
                            }
                        case "ownerid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Ownerid = new OwnerId();
                                    cEntity.Ownerid.Text = jpv.Id.ToString();
                                    cEntity.Ownerid.Name = jpv.Name;
                                }
                            }
                            break;
                        case "transactioncurrencyid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.TransactionCurrencyId = new TransactionCurrencyId();
                                    cEntity.TransactionCurrencyId.Text = jpv.Id.ToString();
                                    cEntity.TransactionCurrencyId.Name = jpv.Name;
                                }
                            }
                            break;
                        case "statuscode":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.StatusCode = new StatusCode();
                                cEntity.StatusCode.Text = jpv.Value.ToString();
                                break;
                            }
                        case "statecode":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.StateCode = new StateCode();
                                cEntity.StateCode.Text = jpv.Value.ToString();
                                break;
                            }
                    }
                }
                foreach (JsonEntityFormattedValue fmv in opportunity.FormattedValues)
                {
                    switch (fmv.Key)
                    {
                        case "statuscode":
                            cEntity.StatusCode.Name = fmv.Value;
                            break;
                        case "statecode":
                            cEntity.StateCode.Name = fmv.Value;
                            break;
                        case "po_opportunitytype":
                            cEntity.Po_OpportunityType.Name = fmv.Value;
                            break;
                        case "estimatedvalue":
                            cEntity.EstimatedValue.Formattedvalue = fmv.Value;
                            break;
                        case "po_installedvolume":
                            cEntity.Po_ActualVolume.Formattedvalue = fmv.Value;
                            break;
                        case "po_markedassoldvolume":
                            cEntity.Po_MarkedAsSoldWeeklyVolume.Formattedvalue = fmv.Value;
                            break;
                        case "po_wornbinstalledvolume":
                            cEntity.Po_WorInstalledVolume.Formattedvalue = fmv.Value;
                            break;
                        case "po_keyeduninstalledvolume":
                            cEntity.Po_OrderedKeyedWeeklyVolume.Formattedvalue = fmv.Value;
                            break;
                        case "po_keyedwornbuninstalledvolume":
                            cEntity.Po_WorNewBusinessVolume.Formattedvalue = fmv.Value;
                            break;
                    }
                }

                //res = result.CustomOpportunity;
                result.CustomOpportunity.Add(cEntity);
            }

            response.d = new ResultsD();
            response.d.results = result.CustomOpportunity;
            debug($"result.CustomOpportunity.Count: {result.CustomOpportunity.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    [HttpPost("odata/opportunity({key})")]
    public IActionResult Update(Guid key, CustomOpportunity opportunityUpdate)
    {
        _ = repo.WhoAmI(Request);
        info($"[POST] odata/opportunity({key}) called by user {repo.Client.CallerId}");

        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(opportunityUpdate);

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            OpportunityResultSet result = new OpportunityResultSet();
            result.CustomOpportunity = new List<CustomOpportunity>();
            CommonResponse responseQuery = new CommonResponse();
            ResultSet responseSet = new ResultSet();
            string message = string.Empty;
            string curStateCode = "";

            responseQuery = repo.RetrieveFetchXMLQueryWithPagination(Request, Entities.opportunityName,
$@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
  <entity name='opportunity'>
    <attribute name='statecode' />
    <filter type='and'>
        <condition attribute='opportunityid' operator='eq' value='{key.ToString()}' />
    </filter>
  </entity>
</fetch>");

            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            foreach (JsonEntity opportunity in resObj)
            {
                foreach (JsonEntityAttribute attr in opportunity.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "statecode":
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                            curStateCode = jpv.Value.ToString();
                            break;
                        default:
                            break;
                    }
                }
            }

            if (string.IsNullOrEmpty(opportunityUpdate.Po_TargetStage) || opportunityUpdate.Po_TargetStage != "CLOSE")
                if (string.IsNullOrEmpty(opportunityUpdate.StateCode.Text) || (opportunityUpdate.StateCode.Text != "2" && opportunityUpdate.StateCode.Text != "3"))
                {
                    Entity opportunity = new Entity();
                    opportunity.LogicalName = Entities.opportunityLogicalName.ToLower();
                    opportunity.Id = key;

                    if (key == Guid.Empty)
                        return BadRequest(Entities.opportunityName + Constants.mandatoryId);


                    //opportunity[Attributes.opportunityId] = key;
                    AssignOpportunityCustom(opportunity, opportunityUpdate, Constants.Update, curStateCode);
                    UpdateRequest reqUpdateOpportunity = new UpdateRequest();
                    reqUpdateOpportunity.Target = opportunity;
                    var responseOpportunity = (UpdateResponse)repo.Client.Execute(reqUpdateOpportunity);


                    if (responseOpportunity != null)
                        return NoContent();
                }
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }
        catch (Exception efin)
        {
            error(efin.Message, efin);
            return BadRequest(efin.Message);
        }

        return NoContent();
    }

    [HttpPost("odata/opportunity")]
    public IActionResult Create(CustomOpportunity opportunityCreate)
    {
        _ = repo.WhoAmI(Request);
        info($"[POST] odata/opportunity called by user {repo.Client.CallerId}");
        string jsonEntity = JsonConvert.SerializeObject(opportunityCreate);

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            Entity opportunity = new Entity();
            opportunity.Id = opportunityCreate.OpportunityId;
            opportunity.LogicalName = Entities.opportunityLogicalName;

            if (string.IsNullOrEmpty(opportunityCreate.Name))
                return BadRequest(Constants.mandatoryName);

            AssignOpportunityCustom(opportunity, opportunityCreate, Constants.Insert);

            //var currency = CommonClass.RetrieveFieldFromEntityByKey(Request, Entities.SystemUserLogicalName, Attributes.systemuserid, client.CallerId, Attributes.transactioncurrencyid);
            //opportunity[Attributes.transactioncurrencyid] = (EntityReference)currency;

            CreateRequest reqCreateOpportunity = new CreateRequest();
            reqCreateOpportunity.Target = opportunity;
            var response = (CreateResponse)repo.Client.Execute(reqCreateOpportunity);
            if (response.ResponseName == Constants.create && response.id != Guid.Empty)
            {
                CreateResponseResult result = new CreateResponseResult();
                opportunityCreate.__metadata = CreateMetaDataTag(Entities.opportunityName, response.id);
                opportunityCreate.OpportunityId = response.id;
                result.d = opportunityCreate;
                return Created(Request.GetDisplayUrl(),result);
            }
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }

        return NoContent();
    }

    protected void AssignOpportunityCustom(Entity opportunity, CustomOpportunity opportunityAssign, 
        string action, string curStateCode = null)
    {
        string jsonEntity = JsonConvert.SerializeObject(opportunityAssign);
        try
        {
            if (opportunityAssign.ParentContactId != null && !string.IsNullOrEmpty(opportunityAssign.ParentContactId.Text))
                opportunity[Attributes.parentContactId] = new EntityReference(Entities.contactLogicalName, 
                    new Guid(opportunityAssign.ParentContactId.Text));
            if (opportunityAssign.ParentAccountId != null && !string.IsNullOrEmpty(opportunityAssign.ParentAccountId.Text))
                opportunity[Attributes.parentAccountId] = new EntityReference(Entities.accountLogicalName, 
                    new Guid(opportunityAssign.ParentAccountId.Text));
            if (!string.IsNullOrEmpty(opportunityAssign.Name))
                opportunity[Attributes.name] = opportunityAssign.Name;
            if (!string.IsNullOrEmpty(opportunityAssign.Ctas_Cintas_Location))
                opportunity[Attributes.cintaslocation] = new EntityReference(Entities.cintasLocationLogicalName, 
                    new Guid(opportunityAssign.Ctas_Cintas_Location));
            if (opportunityAssign.Ownerid != null && !string.IsNullOrEmpty(opportunityAssign.Ownerid.Text))
                opportunity[Attributes.ownerId] = new EntityReference(Constants.systemUser, 
                    new Guid(opportunityAssign.Ownerid.Text));
            if (opportunityAssign.Po_OpportunityType != null && 
                !string.IsNullOrEmpty(opportunityAssign.Po_OpportunityType.Text))
                opportunity[Attributes.po_OpportunityType] = 
                    new OptionSetValue(int.Parse(opportunityAssign.Po_OpportunityType.Text));
            if (!string.IsNullOrEmpty(opportunityAssign.ProcessId))
                opportunity[Attributes.processId] = new Guid(opportunityAssign.ProcessId);
            if (!string.IsNullOrEmpty(opportunityAssign.StageId) && 
                !string.IsNullOrEmpty(action) && action == Constants.Insert)
                opportunity[Attributes.stageId] = new Guid(opportunityAssign.StageId);
            if (opportunityAssign.Po_BusinessDivisionId != null && 
                !string.IsNullOrEmpty(opportunityAssign.Po_OpportunityType.Text))
                opportunity[Attributes.po_BusinessDivisionId] = 
                    new EntityReference(Attributes.po_businessdivision, 
                    new Guid(opportunityAssign.Po_BusinessDivisionId.Text));
            if (opportunityAssign.EstimatedCloseDate != null && !string.IsNullOrEmpty(opportunityAssign.EstimatedCloseDate.Text))
                opportunity[Attributes.estimatedCloseDate] = Convert.ToDateTime(opportunityAssign.EstimatedCloseDate.Text);
            if (opportunityAssign.StateCode != null && !string.IsNullOrEmpty(opportunityAssign.StateCode.Text))
            {
                opportunity[Attributes.stateCode] = new OptionSetValue(Int32.Parse(opportunityAssign.StateCode.Text));
                curStateCode = opportunityAssign.StateCode.Text;
            }
            if (string.IsNullOrEmpty(curStateCode) || (curStateCode == "0"))
                if (opportunityAssign.StatusCode != null && !string.IsNullOrEmpty(opportunityAssign.StatusCode.Text) 
                    && opportunityAssign.StatusCode.Text != "3")
                    opportunity[Attributes.statusCode] = new OptionSetValue(Int32.Parse(opportunityAssign.StatusCode.Text));
            //if (opportunityAssign.EstimatedValue != null && !string.IsNullOrEmpty(opportunityAssign.EstimatedValue.Text))
            //        opportunity[Attributes.estimatedValue] = new OptionSetValue(Int32.Parse(opportunityAssign.EstimatedValue.Text));
            if (opportunityAssign.EstimatedValue != null && !String.IsNullOrEmpty(opportunityAssign.EstimatedValue.Text))
                opportunity[Attributes.estimatedValue] = new Microsoft.Xrm.Sdk.Money(Convert.ToDecimal(opportunityAssign.EstimatedValue.Text));
            if (opportunityAssign.Po_ProposalSentDate != null && !string.IsNullOrEmpty(opportunityAssign.Po_ProposalSentDate.Text))
                opportunity[Attributes.po_ProposalSentDate] = Convert.ToDateTime(opportunityAssign.Po_ProposalSentDate.Text);
            if (opportunityAssign.TransactionCurrencyId != null && !string.IsNullOrEmpty(opportunityAssign.TransactionCurrencyId.Text))
                opportunity[Attributes.transactionCurrencyId] = new EntityReference(Entities.transactionCurrency, new Guid(opportunityAssign.TransactionCurrencyId.Text));
            if (opportunityAssign.Po_NumberOfSites != null && !string.IsNullOrEmpty(opportunityAssign.Po_NumberOfSites.Text))
                opportunity[Attributes.po_NumberOfSites] = Convert.ToInt32(opportunityAssign.Po_NumberOfSites.Text);
            if (opportunityAssign.ctas_AgreementSentDate != null && !string.IsNullOrEmpty(opportunityAssign.ctas_AgreementSentDate.Text))
                opportunity[Attributes.ctas_AgreementSentDate] = Convert.ToDateTime(opportunityAssign.ctas_AgreementSentDate.Text);
            if (opportunityAssign.Po_ProposalExpiredDate != null && !string.IsNullOrEmpty(opportunityAssign.Po_ProposalExpiredDate.Text))
                opportunity[Attributes.po_ProposalExpiredDate] = Convert.ToDateTime(opportunityAssign.Po_ProposalExpiredDate.Text);
            if (opportunityAssign.Po_RejectedBy != null && !string.IsNullOrEmpty(opportunityAssign.Po_RejectedBy.Text))
                opportunity[Attributes.po_Rejectedby] = opportunityAssign.Po_RejectedBy.Name;
            //opportunity[Attributes.po_Rejectedby] = new EntityReference(Constants.systemUser, new Guid(opportunityAssign.Po_RejectedBy.Text));
            if (!string.IsNullOrEmpty(opportunityAssign.Po_ReasonForAgreementRejection))
                opportunity[Attributes.po_ReasonForAgreementRejection] = opportunityAssign.Po_ReasonForAgreementRejection;
            if (opportunityAssign.Po_MarkedAsSoldWeeklyVolume != null && !string.IsNullOrEmpty(opportunityAssign.Po_MarkedAsSoldWeeklyVolume.Text))
                opportunity[Attributes.po_MarkedAsSoldWeeklyVolume] = new Money(Convert.ToDecimal(opportunityAssign.Po_MarkedAsSoldWeeklyVolume.Text));
            if (opportunityAssign.Po_WorNewBusinessVolume != null && !string.IsNullOrEmpty(opportunityAssign.Po_WorNewBusinessVolume.Text))
                opportunity[Attributes.po_WORNewBusinessVolume] = new Money(Convert.ToDecimal(opportunityAssign.Po_WorNewBusinessVolume.Text));
            if (opportunityAssign.Po_InstallDate != null && !string.IsNullOrEmpty(opportunityAssign.Po_InstallDate.Text))
                opportunity[Attributes.po_InstallDate] = Convert.ToDateTime(opportunityAssign.Po_InstallDate.Text);
            if (opportunityAssign.Po_13WeekendDate != null && !string.IsNullOrEmpty(opportunityAssign.Po_13WeekendDate.Text))
                opportunity[Attributes.po_13WeekEndDate] = Convert.ToDateTime(opportunityAssign.Po_13WeekendDate.Text);
            if (opportunityAssign.Po_TargetStage != null && !string.IsNullOrEmpty(opportunityAssign.Po_TargetStage))
            {
                if (opportunityAssign.Po_TargetStage != "CLOSE")
                    opportunity[Attributes.targetStage] = opportunityAssign.Po_TargetStage;
            }
        }
        catch (Exception ex)
        {
            error(ex.Message, ex);
            throw ex;
        }
    }

    public OpportunityController(ILogger<OpportunityController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
