﻿using Cin.WebApi.CrmModels;
using Cin.WebApi.Models;
using Cin.WebApi;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.ServiceModel;
using Cin.WebApi.Extensions;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.DataProtection.KeyManagement;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class OpportunityProductController : BaseController
{

    [HttpDelete("odata/opportunityproduct({key})")]
    public IActionResult Delete(Guid key)
    {
        _ = repo.WhoAmI(Request);
        info($"[DELETE] odata/opportunityproduct({key}) called by user {repo.Client.CallerId}");
        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);
            if (key == Guid.Empty)
                return BadRequest(Constants.mandatoryId);

            repo.Client.Delete(Entities.opportunityProductLogicalName, key);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            return BadRequest(ex.Message);
        }
        return NoContent();
    }

    [HttpGet("odata/opportunityproduct")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/opportunityproduct called by user {repo.Client.CallerId}");

        OpportunityProductResultSet result = new OpportunityProductResultSet();
        result.CustomOpportunityProduct = new List<CustomOpportunityProduct>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {

            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.opportunityProductName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc = CommonClass.XmlFrame(Entities.opportunityProductName, responseQuery.Message);
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            //List<CustomOpportunityProduct> res = new List<CustomOpportunityProduct>();
            //XmlSerializer ser = new XmlSerializer(typeof(OpportunityProductResultSet));
            //using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            //{
            //    result = (OpportunityProductResultSet)ser.Deserialize(reader);
            //    foreach (CustomOpportunityProduct oppProduct in result.CustomOpportunityProduct)
            foreach (JsonEntity oppProduct in resObj)
            {
                var cEntity = new CustomOpportunityProduct();
                cEntity.__metadata = CreateMetaDataTag(Entities.opportunityProductName, oppProduct.Id);

                foreach (JsonEntityAttribute attr in oppProduct.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "priceperunit":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = 
                                    JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.PricePerUnit = new PricePerUnit();
                                cEntity.PricePerUnit.Text = jpv.Value.ToString();
                                break;
                            }
                        case "lineitemnumber":
                            {
                                //cEntity.LineItemNumber = new LineItemNumber();
                                //cEntity.LineItemNumber.Text = ((long)attr.Value).ToString();
                                //cEntity.LineItemNumber.Formattedvalue = ((long)attr.Value).ToString();
                                break;
                            }
                        case "isproductoverridden":
                            cEntity.IsProductOverridden = new IsProductOverridden();
                            cEntity.IsProductOverridden.Text = ((bool)attr.Value).ToString();
                            break;
                        case "opportunityid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                cEntity.OpportunityId = new OpportunityId();
                                cEntity.OpportunityId.Text = jpv.Id.ToString();
                                cEntity.OpportunityId.Name = jpv.Name;
                            }
                            break;
                        case "sequencenumber":
                            cEntity.SequenceNumber = new SequenceNumber();
                            cEntity.SequenceNumber.Text = ((long)attr.Value).ToString();
                            break;
                        case "producttypecode":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.ProductTypeCode = new ProductTypeCode();
                                cEntity.ProductTypeCode.Text = ((long)jpv.Value).ToString();
                            }
                            break;
                        case "po_itemno":
                            cEntity.po_ItemNo = (string)attr.Value;
                            break;
                        case "po_itemdelfreq":
                            cEntity.po_ItemDelFreq = (string)attr.Value;
                            break;
                        case "productdescription":
                            cEntity.ProductDescription = (string)attr.Value;
                            break;
                        case "quantity":
                            cEntity.Quantity = new Quantity();
                            cEntity.Quantity.Text = ((double)attr.Value).ToString();
                            break;
                        case "po_inventory":
                            cEntity.po_Inventory = new Po_InventoryFV();
                            cEntity.po_Inventory.Text = ((double)attr.Value).ToString();
                            break;
                        case "po_usagecd":
                            cEntity.po_UsageCD = (string)attr.Value;
                            break;
                        case "createdby":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Createdby = new QuoteCreatedby();
                                    cEntity.Createdby.Text = jpv.Id.ToString();
                                    cEntity.Createdby.Name = jpv.Name;
                                }
                            }
                            break;
                        case "extendedamount":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                                cEntity.ExtendedAmount = new ExtendedAmount();
                                cEntity.ExtendedAmount.Text = jpv.Value.ToString();
                            }
                            break;
                        case "transactioncurrencyid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.TransactionCurrencyId = new TransactionCurrencyId();
                                    cEntity.TransactionCurrencyId.Text = jpv.Id.ToString();
                                    cEntity.TransactionCurrencyId.Name = jpv.Name;
                                }
                            }
                            break;
                        case "opportunityproductid":
                            cEntity.OpportunityProductId = new Guid((string)attr.Value);
                            break;
                    }
                }
                foreach (JsonEntityFormattedValue fmv in oppProduct.FormattedValues)
                {
                    switch (fmv.Key)
                    {
                        case "quantity":
                            cEntity.Quantity.Formattedvalue = fmv.Value;
                            break;
                        case "priceperunit":
                            cEntity.PricePerUnit.Formattedvalue = fmv.Value;
                            break;
                        case "isproductoverridden":
                            cEntity.IsProductOverridden.Name = fmv.Value;
                            break;
                        case "sequencenumber":
                            cEntity.SequenceNumber.Formattedvalue = fmv.Value;
                            break;
                        case "producttypecode":
                            cEntity.ProductTypeCode.Formattedvalue = fmv.Value;
                            break;
                        case "extendedamount":
                            cEntity.ExtendedAmount.Formattedvalue = fmv.Value;
                            break;
                    }
                }

                //res = result.CustomOpportunityProduct;
                result.CustomOpportunityProduct.Add(cEntity);
            }

            response.d = new ResultsD();
            response.d.results = result.CustomOpportunityProduct;

            debug($"result.CustomOpportunityProduct.Count: {result.CustomOpportunityProduct.Count}");
            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    [HttpPost("odata/opportunityproduct")]
    public IActionResult Create(CustomOpportunityProduct opportunityProdCreate)
    {
        _ = repo.WhoAmI(Request);
        info($"[POST] odata/opportunityproduct called by user {repo.Client.CallerId}");

        string jsonEntity = JsonConvert.SerializeObject(opportunityProdCreate);

        try
        {
            Entity opportunityProduct = new Entity();
            opportunityProduct.LogicalName = Entities.opportunityProductLogicalName;
            opportunityProduct.Id = opportunityProdCreate.OpportunityProductId;

            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            if (opportunityProdCreate.OpportunityId == null)
                return BadRequest(Constants.opportunityIdCheck);

            if (opportunityProdCreate.IsProductOverridden == null || (opportunityProdCreate.IsProductOverridden != null && string.IsNullOrEmpty(opportunityProdCreate.IsProductOverridden.Text)))
                return BadRequest(Constants.productOverridenCheck);

            if (!opportunityProdCreate.IsProductOverridden.Text.ToBool() && 
                (opportunityProdCreate.ProductId == null || (opportunityProdCreate.ProductId != null 
                && string.IsNullOrEmpty(opportunityProdCreate.ProductId.Text))))
                return BadRequest(Constants.opportunityProductIdCheck);

            if (!opportunityProdCreate.IsProductOverridden.Text.ToBool() && 
                (opportunityProdCreate.UoMId == null || (opportunityProdCreate.UoMId != null && 
                string.IsNullOrEmpty(opportunityProdCreate.UoMId.Text))))
                return BadRequest(Constants.opportunityUOMIdCheck);

            if (opportunityProdCreate.IsProductOverridden.Text.ToBool() && 
                opportunityProdCreate.ProductDescription == null)
                return BadRequest(Constants.opportunityProductDescCheck);

            AssignOpportunityProductCustomValue(opportunityProduct, opportunityProdCreate);

            CreateRequest reqCreateOpportunityProduct = new CreateRequest();
            reqCreateOpportunityProduct.Target = opportunityProduct;
            CreateResponse responseOpportunity = (CreateResponse)repo.Client.Execute(reqCreateOpportunityProduct);

            if (responseOpportunity.ResponseName == Constants.create && responseOpportunity.id != Guid.Empty)
            {
                CreateResponseResult result = new CreateResponseResult();
                opportunityProdCreate.__metadata = CreateMetaDataTag(Entities.opportunityProductName, responseOpportunity.id);
                opportunityProdCreate.OpportunityProductId = responseOpportunity.id;
                result.d = opportunityProdCreate;
                return Created(Request.GetDisplayUrl(),result);
            }
       }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }

        return NoContent();

    }

    [HttpPost("odata/opportunityproduct({key})")]
    public IActionResult Update(Guid key, OpportunityProduct opportunityProdUpdate)
    {
        _ = repo.WhoAmI(Request);
        info($"[POST] odata/opportunityproduct({key}) called by user {repo.Client.CallerId}");

        string jsonEntity = JsonConvert.SerializeObject(opportunityProdUpdate);

        try
        {
            Entity opportunityProduct = new Entity();
            opportunityProduct.LogicalName = Entities.opportunityProductLogicalName;

            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            if (key == Guid.Empty)
                return BadRequest(Entities.opportunityProductName + Constants.mandatoryId);

            //if (opportunityProdUpdate.IsProductOverridden == false && opportunityProdUpdate.ProductId == null)
            //    return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.opportunityProductIdCheck);

            //if (opportunityProdUpdate.IsProductOverridden == false && opportunityProdUpdate.UoMId == null)
            //    return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.opportunityUOMIdCheck);

            //if (opportunityProdUpdate.IsProductOverridden == true && opportunityProdUpdate.ProductDescription == null)
            //    return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.opportunityProductDescCheck);

            opportunityProduct[Attributes.opportunityProductId] = key;

            AssignOpportunityProductValue(opportunityProduct, opportunityProdUpdate);

            UpdateRequest reqUpdateOpportunityProduct = new UpdateRequest();
            reqUpdateOpportunityProduct.Target = opportunityProduct;
            UpdateResponse responseOpportunity = (UpdateResponse)repo.Client.Execute(reqUpdateOpportunityProduct);

            if (responseOpportunity != null)
                return NoContent();
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }

        return NoContent();
    }


    protected void AssignOpportunityProductValue(Entity opportunityProduct, OpportunityProduct opportunityProductAssign)
    {
        string jsonEntity = "OpportunityProduct: " + JsonConvert.SerializeObject(opportunityProductAssign);

        try
        {
            if (opportunityProductAssign.OpportunityId != null)
                opportunityProduct[Attributes.opportunityId] = opportunityProductAssign.OpportunityId;

            if (opportunityProductAssign.IsProductOverridden != null)
                opportunityProduct[Attributes.isProductOverridden] = opportunityProductAssign.IsProductOverridden;

            if (opportunityProductAssign.IsPriceOverridden != null)
                opportunityProduct[Attributes.isPriceOverridden] = opportunityProductAssign.IsPriceOverridden;

            if (opportunityProductAssign.IsPriceOverridden != null && opportunityProductAssign.IsProductOverridden == false)
                opportunityProduct[Attributes.UOMId] = opportunityProductAssign.UoMId;

            if (opportunityProductAssign.ProductId != null)
                opportunityProduct[Attributes.productId] = opportunityProductAssign.ProductId;

            if (opportunityProductAssign.ProductDescription != null)
                opportunityProduct[Attributes.productDescription] = opportunityProductAssign.ProductDescription;

            if (opportunityProductAssign.ExtendedAmount != null)
                opportunityProduct[Attributes.extendedAmount] = opportunityProductAssign.ExtendedAmount;

            if (opportunityProductAssign.po_Inventory != null)
                opportunityProduct[Attributes.po_Inventory] = opportunityProductAssign.po_Inventory;

            if (opportunityProductAssign.po_ItemDelFreq != null)
                opportunityProduct[Attributes.po_ItemDelFreq] = opportunityProductAssign.po_ItemDelFreq;

            if (opportunityProductAssign.po_ItemNo != null)
                opportunityProduct[Attributes.po_ItemNo] = opportunityProductAssign.po_ItemNo;

            if (opportunityProductAssign.po_UsageCD != null)
                opportunityProduct[Attributes.po_UsageCD] = opportunityProductAssign.po_UsageCD;

            if (opportunityProductAssign.PricePerUnit != null && opportunityProductAssign.IsPriceOverridden != null && opportunityProductAssign.IsPriceOverridden == true)
                opportunityProduct[Attributes.pricePerUnit] = opportunityProductAssign.PricePerUnit;

            if (opportunityProductAssign.Quantity != null)
                opportunityProduct[Attributes.Quantity] = opportunityProductAssign.Quantity;

            if (opportunityProductAssign.ProductTypeCode != null)
                opportunityProduct[Attributes.productTypeCode] = opportunityProductAssign.ProductTypeCode;

            if (opportunityProductAssign.SequenceNumber != null)
                opportunityProduct[Attributes.sequenceNumber] = opportunityProductAssign.SequenceNumber;

            if (opportunityProductAssign.TransactionCurrencyId != null)
                opportunityProduct[Attributes.transactionCurrencyId] = opportunityProductAssign.TransactionCurrencyId;

            if (opportunityProductAssign.OwnerId != null)
                opportunityProduct[Attributes.ownerId] = opportunityProductAssign.OwnerId;
        }
        catch (Exception ex)
        {
            error(ex.Message, ex);
            throw;
        }
    }

    protected void AssignOpportunityProductCustomValue(Entity opportunityProduct, CustomOpportunityProduct opportunityProductAssign)
    {
        string jsonEntity = "Opportunity Product " + Newtonsoft.Json.JsonConvert.SerializeObject(opportunityProductAssign);

        try
        {
            if (opportunityProductAssign.OpportunityId != null && !string.IsNullOrEmpty(opportunityProductAssign.OpportunityId.Text))
                opportunityProduct[Attributes.opportunityId] = new EntityReference(Entities.opportunityLogicalName, 
                    new Guid(opportunityProductAssign.OpportunityId.Text));

            if (opportunityProductAssign.IsProductOverridden != null && !string.IsNullOrEmpty(opportunityProductAssign.IsProductOverridden.Text))
                opportunityProduct[Attributes.isProductOverridden] = opportunityProductAssign.IsProductOverridden.Text.ToBool();

            if (opportunityProductAssign.IsPriceOverridden != null && !string.IsNullOrEmpty(opportunityProductAssign.IsPriceOverridden.Text))
                opportunityProduct[Attributes.isPriceOverridden] = opportunityProductAssign.IsPriceOverridden.Text.ToBool();

            if (opportunityProductAssign.UoMId != null && !string.IsNullOrEmpty(opportunityProductAssign.UoMId.Text) 
                && !string.IsNullOrEmpty(opportunityProductAssign.IsProductOverridden.Text) && opportunityProductAssign.IsProductOverridden.Text.ToBool() == false)
                opportunityProduct[Attributes.UOMId] = new EntityReference(Entities.UOMLogicalName, 
                    new Guid(opportunityProductAssign.UoMId.Text));

            if (opportunityProductAssign.ProductId != null && !string.IsNullOrEmpty(opportunityProductAssign.ProductId.Text) 
                && !string.IsNullOrEmpty(opportunityProductAssign.IsProductOverridden.Text) && opportunityProductAssign.IsProductOverridden.Text.ToBool() == false)
                opportunityProduct[Attributes.productId] = new EntityReference(Entities.ProductLogicalName, 
                    new Guid(opportunityProductAssign.ProductId.Text));

            if (opportunityProductAssign.ProductDescription != null)
                opportunityProduct[Attributes.productDescription] = opportunityProductAssign.ProductDescription;

            if (opportunityProductAssign.ExtendedAmount != null && !string.IsNullOrEmpty(opportunityProductAssign.ExtendedAmount.Text))
            {
                decimal converted = 0.0M;
                if (Decimal.TryParse(opportunityProductAssign.ExtendedAmount.Text, out converted))
                {
                    opportunityProduct[Attributes.extendedAmount] = new Money(converted);
                }
            }
            if (opportunityProductAssign.po_Inventory != null && opportunityProductAssign.po_Inventory.Text != null 
                && !string.IsNullOrEmpty(opportunityProductAssign.po_Inventory.Text))
            {
                decimal converted = 0.0M;
                if (Decimal.TryParse(opportunityProductAssign.po_Inventory.Text, out converted))
                {
                    try
                    {
                        opportunityProduct[Attributes.po_Inventory] = converted;
                    }
                    catch (Exception) { }
                }
            }
            if (opportunityProductAssign.po_ItemDelFreq != null)
                opportunityProduct[Attributes.po_ItemDelFreq] = opportunityProductAssign.po_ItemDelFreq;

            if (opportunityProductAssign.po_ItemNo != null)
                opportunityProduct[Attributes.po_ItemNo] = opportunityProductAssign.po_ItemNo;

            if (opportunityProductAssign.po_UsageCD != null)
                opportunityProduct[Attributes.po_UsageCD] = opportunityProductAssign.po_UsageCD;

            if (opportunityProductAssign.PricePerUnit != null && !string.IsNullOrEmpty(opportunityProductAssign.PricePerUnit.Text) 
                && !string.IsNullOrEmpty(opportunityProductAssign.IsPriceOverridden.Text) && opportunityProductAssign.IsPriceOverridden.Text.ToBool() == true)
            {
                decimal converted = 0.0M;
                if (Decimal.TryParse(opportunityProductAssign.PricePerUnit.Text, out converted))
                {
                    opportunityProduct[Attributes.pricePerUnit] = new Money(converted);
                }
            }
            if (opportunityProductAssign.Quantity != null && !string.IsNullOrEmpty(opportunityProductAssign.Quantity.Text))
            {
                decimal converted = 0.0M;
                if (Decimal.TryParse(opportunityProductAssign.Quantity.Text, out converted))
                {
                    opportunityProduct[Attributes.Quantity] = converted;
                }
            }
            if (opportunityProductAssign.ProductTypeCode != null && !string.IsNullOrEmpty(opportunityProductAssign.ProductTypeCode.Text))
                opportunityProduct[Attributes.productTypeCode] = new OptionSetValue(Convert.ToInt32(opportunityProductAssign.ProductTypeCode.Text));

            if (opportunityProductAssign.SequenceNumber != null && !string.IsNullOrEmpty(opportunityProductAssign.SequenceNumber.Text))
                opportunityProduct[Attributes.sequenceNumber] = Convert.ToInt32(opportunityProductAssign.SequenceNumber.Text);

            if (opportunityProductAssign.TransactionCurrencyId != null && !string.IsNullOrEmpty(opportunityProductAssign.TransactionCurrencyId.Text))
                opportunityProduct[Attributes.transactionCurrencyId] = new EntityReference(Entities.transactionCurrency, 
                    new Guid(opportunityProductAssign.TransactionCurrencyId.Text));

            if (opportunityProductAssign.OwnerId != null && !string.IsNullOrEmpty(opportunityProductAssign.OwnerId.Text))
                opportunityProduct[Attributes.ownerId] = new EntityReference(Constants.systemUser, new Guid(opportunityProductAssign.OwnerId.Text));
        }
        catch (Exception ex)
        {
            error(ex.Message, ex);
            throw;
        }
    }


    public OpportunityProductController(ILogger<OpportunityProductController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
