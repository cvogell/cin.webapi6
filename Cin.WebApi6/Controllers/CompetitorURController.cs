﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;
using Microsoft.AspNetCore.Mvc;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class CompetitorURController : BaseController
{
    [HttpGet("odata/competitorur")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/competitorur called by user {repo.Client.CallerId}");

        CompetitorURResultSet result = new CompetitorURResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {

            responseQuery = repo.RetrieveUserQuery(Request, Entities.CompetitorURName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.CompetitorURName, responseQuery.Message);

            List<CustomCompetitorUR> res = new List<CustomCompetitorUR>();
            XmlSerializer ser = new XmlSerializer(typeof(CompetitorURResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (CompetitorURResultSet)ser.Deserialize(reader);
                foreach (CustomCompetitorUR competitorUR in result.CustomCompetitorUR)
                {
                    competitorUR.__metadata = CreateMetaDataTag(Entities.CompetitorURName, competitorUR.Po_competitorurid.ToString());
                }
                res = result.CustomCompetitorUR;
            }
            response.d = new ResultsD();
            response.d.results = res;

            debug($"result.CustomCompetitorUR.Count: {result.CustomCompetitorUR.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"unexpected error: {ex.Message}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    public CompetitorURController(ILogger<CompetitorURController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
