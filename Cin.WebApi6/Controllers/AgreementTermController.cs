﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.Diagnostics.Contracts;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;
using Microsoft.AspNetCore.Mvc;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class AgreementTermController : BaseController
{
    [HttpGet("odata/AgreementTerm")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/AgreementTerm called by user {repo.Client.CallerId}");

        OrganizationResponse responseWhoamI = repo.WhoAmI(Request);
        AgreementTermResultSet result = new AgreementTermResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {
            responseQuery = repo.RetrieveUserQuery(Request, Entities.agreementTermName);
            //CinTrace.TraceInformation(responseQuery.ResponseName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.agreementTermName, responseQuery.Message);


            List<CustomAgreementTerm> res = new List<CustomAgreementTerm>();
            XmlSerializer ser = new XmlSerializer(typeof(AgreementTermResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (AgreementTermResultSet)ser.Deserialize(reader);
                foreach (CustomAgreementTerm agreementTerm in result.CustomAgreementTerm)
                {
                    agreementTerm.__metadata = CreateMetaDataTag(Entities.agreementTermName, 
                        agreementTerm.Po_agreementtermid.ToString());
                }
                res = result.CustomAgreementTerm;
            }
            response.d = new ResultsD();
            response.d.results = res;

            info($"{res.Count} agreements returned");
            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error::Message:{ex.Message}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    public AgreementTermController(ILogger<AgreementTermController> logger, IMemoryCache memoryCache, 
        IConfiguration configuration, CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
