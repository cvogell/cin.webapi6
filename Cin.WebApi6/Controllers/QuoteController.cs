using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Cin.WebApi.Xrm;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Web.Http.OData;
using AllowedQueryOptions = System.Web.Http.OData.Query.AllowedQueryOptions;
using Newtonsoft.Json;
using Cin.WebApi;
using System.Net;
using Cin.WebApi.Extensions;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.AspNetCore.Mvc;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class QuoteController : BaseController
{
    [HttpGet("odata/quote")]
    public ResultSet UserQuery()
    {
        OrganizationResponse responseWhoamI = repo.WhoAmI(Request);
        var user = RetrieveFieldFromEntityByKey(
            Request, Entities.SystemUserLogicalName, "systemuserid",
            responseWhoamI, Attributes.fullName);
        info($"UserQuery query with user {user}");

        QuoteResultSet result = new QuoteResultSet();
        result.CustomQuote = new List<CustomQuote>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {
            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.QuoteName);
            if (responseQuery.ResponseName == Constants.connectionFailure)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            foreach (JsonEntity quote in resObj)
            {
                var cEntity = new CustomQuote();
                cEntity.__metadata = CreateMetaDataTag(Entities.QuoteName, quote.Id);

                foreach (JsonEntityAttribute attr in quote.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "po_businessdivision":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.po_BusinessDivison = new Po_businessdivison();
                                cEntity.po_BusinessDivison.Text = jpv.Id.ToString();
                                cEntity.po_BusinessDivison.Name = jpv.Name;
                            }
                        }
                            break;
                        case "po_13weekcalcfield":
                            cEntity.po_13WeekCalcField = new Po_13weekcalcfield();
                            cEntity.po_13WeekCalcField.Text = ((DateTime)attr.Value).ToString();
                            cEntity.po_13WeekCalcField.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.po_13WeekCalcField.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "name":
                            cEntity.Name = (string)attr.Value;
                            break;
                        case "quotenumber":
                            cEntity.QuoteNumber = (string)attr.Value;
                            break;
                        case "quoteid":
                            cEntity.QuoteId = new Guid((string)attr.Value);
                            break;
                        case "opportunityid":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.OpportunityId = new OpportunityId();
                                cEntity.OpportunityId.Text = jpv.Id.ToString();
                                cEntity.OpportunityId.Name = jpv.Name;
                            }
                            break;
                        }
                        case "customerid":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.CustomerId = new CustomerId();
                                cEntity.CustomerId.Text = jpv.Id.ToString();
                                cEntity.CustomerId.Name = jpv.Name;
                            }
                        }
                            break;
                        case "createdby":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.Createdby = new QuoteCreatedby();
                                cEntity.Createdby.Text = jpv.Id.ToString();
                                cEntity.Createdby.Name = jpv.Name;
                            }
                        }
                            break;
                        case "pricelevelid":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.PriceLevelId = new Pricelevelid();
                                cEntity.PriceLevelId.Text = jpv.Id.ToString();
                                cEntity.PriceLevelId.Name = jpv.Name;
                            }
                        }
                            break;
                        case "transactioncurrencyid":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.TransactionCurrencyId = new TransactionCurrencyId();
                                cEntity.TransactionCurrencyId.Text = jpv.Id.ToString();
                                cEntity.TransactionCurrencyId.Name = jpv.Name;
                            }
                        }
                            break;
                        case "statuscode":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                            cEntity.StatusCode = new StatusCode();
                            cEntity.StatusCode.Text = jpv.Value.ToString();
                        }
                            break;
                        case "statecode":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                            cEntity.StateCode = new StateCode();
                            cEntity.StateCode.Text = jpv.Value.ToString();
                        }
                            break;
                        case "po_estimatedweeklyvolume":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                            cEntity.po_EstimatedWeeklyVolume = new Po_estimatedweeklyvolume();
                            cEntity.po_EstimatedWeeklyVolume.Text = jpv.Value.ToString();
                        }
                            break;
                        case "po_wornbinstalledvolume":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                            cEntity.po_WORNBInstalledVolume = new po_WORNBInstalledVolume();
                            cEntity.po_WORNBInstalledVolume.Text = jpv.Value.ToString();
                        }
                            break;
                        case "po_keyedwornbuninstalledvolume":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                            cEntity.po_WORNewBusinessVolume = new po_WORNewBusinessVolume();
                            cEntity.po_WORNewBusinessVolume.Text = jpv.Value.ToString();
                        }
                            break;
                        case "po_keyeduninstalledvolume":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                            cEntity.po_OrderedKeyedWeeklyVolume = new po_OrderedKeyedWeeklyVolume();
                            cEntity.po_OrderedKeyedWeeklyVolume.Text = jpv.Value.ToString();
                        }
                            break;
                        case "po_markedassoldvolume":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                            cEntity.po_MarkedasSoldWeeklyVolume = new po_MarkedasSoldWeeklyVolume();
                            cEntity.po_MarkedasSoldWeeklyVolume.Text = jpv.Value.ToString();
                        }
                            break;
                        case "po_installedvolume":
                        {
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeFloatValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeFloatValue>(sVal);
                            cEntity.po_ActualVolume = new Po_actualvolume();
                            cEntity.po_ActualVolume.Text = jpv.Value.ToString();
                        }
                            break;
                    }
                }
                foreach (JsonEntityFormattedValue fmv in quote.FormattedValues)
                    {
                        switch (fmv.Key)
                        {
                            case "statuscode":
                                cEntity.StatusCode.Name = fmv.Value;
                                break;
                            case "statecode":
                                cEntity.StateCode.Name = fmv.Value;
                                break;
                            case "po_estimatedweeklyvolume":
                                cEntity.po_EstimatedWeeklyVolume.Formattedvalue = fmv.Value;
                                break;
                            case "po_wornbinstalledvolume":
                                cEntity.po_WORNBInstalledVolume.Formattedvalue = fmv.Value;
                                break;
                            case "po_keyedwornbuninstalledvolume":
                                cEntity.po_WORNewBusinessVolume.Formattedvalue = fmv.Value;
                                break;
                            case "po_installedvolume":
                                cEntity.po_ActualVolume.Formattedvalue = fmv.Value;
                                break;
                        }
                    }
                result.CustomQuote.Add(cEntity);
            }
            response.d = new ResultsD();
            response.d.results = result.CustomQuote;

            debug($"Request: {Request}, Entity: {Entities.QuoteName}, User: {user}, Payload Length: {result.CustomQuote.Count}");
            return response;
        }
        catch (Exception ex)
        {
            error($"{ex}");
            response.d = new ResultsD();
            response.d.results = ex;
            return response;
        }
    }

    [HttpPost("odata/quote")]
    public IActionResult Create(CustomQuote quoteToCreate)
    {
        OrganizationResponse responseWhoamI = repo.WhoAmI(Request);
        var user = RetrieveFieldFromEntityByKey(
            Request, Entities.SystemUserLogicalName, "systemuserid",
            responseWhoamI, Attributes.fullName);
        info($"UserQuery query with user {user}");

        try
        {
            Entity quote = new Entity();
            quote = AssignCustomRecords(quoteToCreate, Constants.Insert);
            CreateRequest createRequest = new CreateRequest();
            createRequest.Target = quote;
            var response = (CreateResponse)repo.Client.Execute(createRequest);

            if (response.ResponseName == Constants.create && response.id != Guid.Empty)
            {
                CreateResponseResult result = new CreateResponseResult();
                var metadata = new MetadataTag();
                metadata.uri = appConfig.MetadataUrl + Entities.QuoteName + quote.Id;
                metadata.type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + Entities.QuoteName;
                quoteToCreate.__metadata = metadata;
                quoteToCreate.QuoteId = response.id;
                result.d = quoteToCreate;
                return Ok (result);
            }
            else
            {
                return NoContent();
            }
        }
        catch(Exception ex)
        {
            debug($"Exception::Message:{ex.Message}||Trace:{ex.StackTrace}");
            return BadRequest(ex);
        }
    }

    [HttpPost("odata/quote({key})")]
    public IActionResult Update(Guid key, CustomQuote quoteToUpdate)
    {
        OrganizationResponse responseWhoamI = repo.WhoAmI(Request);
        var user = RetrieveFieldFromEntityByKey(
            Request, Entities.SystemUserLogicalName, "systemuserid",
            responseWhoamI, Attributes.fullName);
        info($"UserQuery query with user {user}");
       
        try
        {
            Entity quote = new Entity();
            quoteToUpdate.QuoteId = key;
            quote = AssignCustomRecords(quoteToUpdate, Constants.Update);

            UpdateRequest request = new UpdateRequest();
            request.Target = quote;
            UpdateResponse response = (UpdateResponse)repo.Client.Execute(request);
            debug($"Request: {Request.Method}, Entity: {Entities.QuoteName}, User: {user}");
            if (response != null)
                return Ok(response);
            return NoContent();
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}");
            return BadRequest(ex.Message);
        }
    }

    private Entity AssignCustomRecords(CustomQuote clientEntity, string action)
        {
            string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(clientEntity);

            try
            {
                Entity quote = new Entity();
                quote.Id = new Guid(clientEntity.QuoteId.ToString());
                quote.LogicalName = Entities.QuoteLogicalName;
                if (clientEntity.StatusCode != null && !string.IsNullOrEmpty(clientEntity.StatusCode.Text))
                    quote[Attributes.statuscode] = new OptionSetValue(Convert.ToInt32(clientEntity.StatusCode.Text));
                if (clientEntity.StateCode != null && !string.IsNullOrEmpty(clientEntity.StateCode.Text))
                    quote[Attributes.statecode] = new OptionSetValue(Convert.ToInt32(clientEntity.StateCode.Text));
                if (clientEntity.StateCode != null && (string.IsNullOrEmpty(clientEntity.StateCode.Text) || (clientEntity.StateCode.Text != "2" && clientEntity.StateCode.Text != "3")) || action.Equals(Constants.Insert))
                {
                    if (clientEntity.Name != null)
                        quote[Attributes.name] = clientEntity.Name;
                    if (action == Constants.Update)
                        quote[Attributes.quoteid] = clientEntity.QuoteId;
                    if (clientEntity.OwnerId != null && !string.IsNullOrEmpty(clientEntity.OwnerId.Text))
                        quote[Attributes.ownerId] = new EntityReference(Constants.systemUser, new Guid(clientEntity.OwnerId.Text));
                    if (clientEntity.QuoteNumber != null && !string.IsNullOrEmpty(clientEntity.QuoteNumber))
                        quote[Attributes.quotenumber] = clientEntity.QuoteNumber;
                    if (clientEntity.po_WORNewBusinessVolume != null && !string.IsNullOrEmpty(clientEntity.po_WORNewBusinessVolume.Text))
                        quote[Attributes.po_wornewbusinessvolume] = new Money(Convert.ToDecimal(clientEntity.po_WORNewBusinessVolume.Text));
                    if (clientEntity.po_WORNBInstalledVolume != null && !string.IsNullOrEmpty(clientEntity.po_WORNBInstalledVolume.Text))
                        quote[Attributes.po_wornbinstalledvolume] = new Money(Convert.ToDecimal(clientEntity.po_WORNBInstalledVolume.Text));
                    if (clientEntity.po_OrderedKeyedWeeklyVolume != null && !string.IsNullOrEmpty(clientEntity.po_OrderedKeyedWeeklyVolume.Text))
                        quote[Attributes.po_orderedkeyedweeklyvolume] = new Money(Convert.ToDecimal(clientEntity.po_OrderedKeyedWeeklyVolume.Text));
                    if (clientEntity.po_BusinessDivison != null && !string.IsNullOrEmpty(clientEntity.po_BusinessDivison.Text))
                        quote[Attributes.po_businessdivison] = new EntityReference(Attributes.po_businessdivision, new Guid(clientEntity.po_BusinessDivison.Text));
                    if (clientEntity.po_13WeekCalcField != null && !string.IsNullOrEmpty(clientEntity.po_13WeekCalcField.Text))
                        quote[Attributes.po_13weekcalcfield] = Convert.ToDateTime(clientEntity.po_13WeekCalcField.Text);
                    if (clientEntity.OpportunityId != null && !string.IsNullOrEmpty(clientEntity.OpportunityId.Text))
                        quote[Attributes.opportunityid] = new EntityReference(Entities.opportunityLogicalName, new Guid(clientEntity.OpportunityId.Text));
                    if (clientEntity.PriceLevelId != null && !string.IsNullOrEmpty(clientEntity.PriceLevelId.Text))
                        quote[Attributes.pricelevelid] = new EntityReference(Entities.pricelevelLogicalName, new Guid(clientEntity.PriceLevelId.Text));
                    if (clientEntity.TransactionCurrencyId != null && !string.IsNullOrEmpty(clientEntity.TransactionCurrencyId.Text))
                        quote[Attributes.transactioncurrencyid] = new EntityReference(Entities.transactionCurrency, new Guid(clientEntity.TransactionCurrencyId.Text));
                    if (clientEntity.po_ActualVolume != null && !string.IsNullOrEmpty(clientEntity.po_ActualVolume.Text))
                        quote[Attributes.po_installedvolume] = new Money(Convert.ToDecimal(clientEntity.po_ActualVolume.Text));
                    if (clientEntity.po_EstimatedWeeklyVolume != null && !string.IsNullOrEmpty(clientEntity.po_EstimatedWeeklyVolume.Text))
                        quote[Attributes.po_estimatedweeklyvolume] = new Money(Convert.ToDecimal(clientEntity.po_EstimatedWeeklyVolume.Text));
                    if (clientEntity.CustomerId != null && !string.IsNullOrEmpty(clientEntity.CustomerId.Text))
                        quote[Attributes.customerid] = new EntityReference(Entities.accountLogicalName, new Guid(clientEntity.CustomerId.Text));
                }
                return quote;
            }
            catch (Exception ex)
            {
                error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}||Request Entity:{jsonEntity}");
                throw ex;
            }

        }
    public QuoteController(ILogger<QuoteController> logger, IMemoryCache memoryCache,
        IConfiguration configuration, CrmRepo crmRepo) :
            base(logger, memoryCache, configuration, crmRepo)
    { }
}