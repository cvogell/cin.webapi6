﻿using Cin.WebApi.Config;
using Cin.WebApi.Extensions;
using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.Text;
using System.Xml;

namespace Cin.WebApi6.Controllers;

//[ApiController]
public abstract class BaseController: Controller
{
    protected readonly CrmConfig crmConfig;
    protected readonly AppConfig appConfig;
    protected readonly QueryConfig queryConfig;
    protected readonly ILogger log;
    protected readonly IMemoryCache cache;
    protected readonly IConfiguration config;
    protected readonly CrmRepo repo;

    protected BaseController(ILogger logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo)
    {
        crmConfig = CrmConfig.Construct(configuration);
        log = logger;
        cache = memoryCache;
        config = configuration;
        repo = crmRepo;
        queryConfig = QueryConfig.Construct(configuration);
        appConfig = AppConfig.Construct(configuration);
    }

    #region logging

    protected void debug(string message)
    {
        log.LogDebug(message);
    }

    protected void info(string message) 
    {
        log.LogInformation(message);
    }

    protected void warning(string message, Exception? ex = null)
    {
        log.LogWarning(ex,message);
    }
    protected void error(string message, Exception? ex = null)
    {
        log.LogError(ex,message);
    }

    protected void critical(string message, Exception? ex = null)
    {
        log.LogCritical(ex,message);
    }
    #endregion

    #region xml
    protected string CreateXml(string xml, string cookie, int page, int count)
    {
        StringReader stringReader = new StringReader(xml);
        XmlTextReader reader = new XmlTextReader(stringReader);

        // Load document
        var doc = new XmlDocument();
        doc.Load(reader);

        return CreateXml(doc, cookie, page, count);
    }

    protected string CreateXml(XmlDocument doc, string cookie, int page, int count)
    {
        XmlAttributeCollection attrs = doc.DocumentElement?.Attributes;

        if (cookie != null)
        {
            XmlAttribute pagingAttr = doc.CreateAttribute("paging-cookie");
            pagingAttr.Value = cookie;
            attrs.Append(pagingAttr);
        }

        XmlAttribute pageAttr = doc.CreateAttribute("page");
        pageAttr.Value = System.Convert.ToString(page);
        attrs.Append(pageAttr);

        XmlAttribute countAttr = doc.CreateAttribute("count");
        countAttr.Value = System.Convert.ToString(count);
        attrs.Append(countAttr);

        StringBuilder sb = new StringBuilder(1024);
        StringWriter stringWriter = new StringWriter(sb);

        XmlTextWriter writer = new XmlTextWriter(stringWriter);
        doc.WriteTo(writer);
        writer.Close();

        return sb.ToString();
    }

    public static XmlDocument XmlFrame(string entityName, string xmlString)
    {
        var xml = new XmlDocument();
        xml.LoadXml(xmlString);
        string innerXml = xml.LastChild.InnerXml.Replace(Constants.resultStartTag, Delimiters.startWith + Constants.entityCustom + entityName + Delimiters.endWith).Replace(Constants.resultEndTag, Delimiters.LessThanSlash + Constants.entityCustom + entityName + Delimiters.endWith);
        var xmlNew = new XmlDocument();
        xmlNew.LoadXml(Delimiters.startWith + entityName + Constants.resultSetTag + innerXml + Delimiters.LessThanSlash + entityName + Constants.resultSetTag);
        return xmlNew;
    }

    #endregion

    #region delta tokens
    protected bool HasDeltaToken(HttpRequest request)
    {
        return request.Query.ContainsKey("!deltatoken");
    }

    protected DateTime ParseDeltaToken(HttpRequest request)
    {
        DateTime dt = new DateTime(1980, 1, 1, 1, 0, 0);

        try
        {            
            debug($"Current Token: {request.Query["!deltatoken"]}");
            //TODO: change to TryParse
            if (request.Query.ContainsKey("!deltatoken") && !string.IsNullOrEmpty(request.Query["!deltatoken"]))
            {
                string[] splitter = { "_" };
                string[] tokensplit = request.Query["!deltatoken"].ToString().Split(splitter, StringSplitOptions.None);
                if (tokensplit.Length > 1)
                    dt = new DateTime(
                        int.Parse(tokensplit[tokensplit.Length - 1].Substring(0, 4)),
                        int.Parse(tokensplit[tokensplit.Length - 1].Substring(4, 2)),
                        int.Parse(tokensplit[tokensplit.Length - 1].Substring(6, 2)),
                        int.Parse(tokensplit[tokensplit.Length - 1].Substring(8, 2)),
                        int.Parse(tokensplit[tokensplit.Length - 1].Substring(10, 2)),
                        int.Parse(tokensplit[tokensplit.Length - 1].Substring(12)));
            }
        }
        catch (Exception)
        {
            dt = new DateTime(1980, 1, 1, 1, 0, 0);
        }
        return dt;
    }

    protected string GenerateDeltaToken(HttpRequest request)
    {
        var who = repo.WhoAmI(request);
        var tokenWithoutBearer = repo.Token;
        string tokenSub = tokenWithoutBearer.Substring(tokenWithoutBearer.Length - 12);
        //NameValueCollection nvp = request.RequestUri.ParseQueryString();
        DateTime dt = DateTime.UtcNow;
        string sToken = tokenSub.Replace("_", "") + "_" + dt.ToString("yyyyMMddHHmmss");
        debug($"Generated Token: {sToken}");

        return sToken;
    }
    #endregion

    protected DeltaMetadataTag CreateDeltaMetadataTag(string entityName, string entityValue)
    {
        var mdTag = new DeltaMetadataTag();
        mdTag.uri = string.Concat(appConfig.MetadataUrl, entityName, entityValue);
        mdTag.id = mdTag.uri;
        mdTag.type = string.Concat(appConfig.ModelNamespace, Delimiters.Dot, Constants.entityCustom, entityName);
        return mdTag;

    }
    protected MetadataTag CreateMetaDataTag(string entityName, Guid entityValue)
    {
        return CreateMetaDataTag(entityName, entityValue.ToString());
    }
    
    protected MetadataTag CreateMetaDataTag(string entityName, string entityValue)
    {
        var mdTag = new MetadataTag();
        mdTag.uri = string.Concat(appConfig.MetadataUrl, entityName,'(', entityValue, ')');
        mdTag.type = string.Concat(appConfig.ModelNamespace, Delimiters.Dot, Constants.entityCustom, entityName);
        return mdTag;
    }

    #region Entity Operations
    protected object RetrieveFieldFromEntityByKey(HttpRequest request, string entity, string idfield, object recid, string lookupfield)
    {
        object result = new object();
        List<Entity> entityResult = new List<Entity>();

        CommonResponse responseFetchXML = repo.RetrieveFetchXMLQueryWithPagination(request, entity, out entityResult,
            $"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'><entity name='{entity}'><attribute name='{lookupfield}' /><filter type='and'><condition attribute='{idfield}' operator='eq' value='{recid.ToString()}' /></filter></entity></fetch>");

        if (responseFetchXML.ResponseName == Constants.failureResponse)
        {
            return "";
        }

        if (entityResult.Count < 1)
        {
            return "";
        }

        Entity entFound = entityResult.First<Entity>();
        foreach (KeyValuePair<string, object> attr in entFound.Attributes.ToList())
        {
            switch (attr.Key == lookupfield)
            {
                case true:
                    if (!string.IsNullOrEmpty(attr.Value.ToString()))
                        result = attr.Value;
                    break;
            }
        }
        return result;
    }

    #endregion

    protected DeltaMetadataTag getMetaTag(string name, Guid id)
    {
        var metadata = new DeltaMetadataTag();
        metadata.id = $"{appConfig.MetadataUrl}{name}({id})";
        metadata.uri = metadata.id;
        metadata.type = $"{appConfig.ModelNamespace}.{Constants.entityCustom}{Entities.accountName}";
        //metadata.id = string.Format(ConfigurationManager.AppSettings[AppSettings.metadataURL].ToString() + Entities.accountName + "({0})", account.Id);
        //metadata.uri = string.Format(ConfigurationManager.AppSettings[AppSettings.metadataURL].ToString() + Entities.accountName + "({0})", account.Id);
        //metadata.type = ConfigurationManager.AppSettings[AppSettings.modelNameSpace].ToString() + Delimiters.Dot + Constants.entityCustom + Entities.accountName;
        return metadata;
    }
}
