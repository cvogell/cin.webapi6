﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.Diagnostics.Contracts;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class NAICSCodeProductBundleAssociationController : BaseController
{
    [HttpGet("odata/NAICSCodeProductBundleAssociation")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/NAICSCodeProductBundleAssociation called by user {repo.Client.CallerId}");

        Po_naicscodeproductbundleassociationResultSet result = new Po_naicscodeproductbundleassociationResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {

            responseQuery = repo.RetrieveUserQuery(Request, Entities.po_naicscodeproductbundleassociation);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.po_naicscodeproductbundleassociation, responseQuery.Message);

            List<Custompo_naicscodeproductbundleassociation> res = new List<Custompo_naicscodeproductbundleassociation>();
            XmlSerializer ser = new XmlSerializer(typeof(Po_naicscodeproductbundleassociationResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (Po_naicscodeproductbundleassociationResultSet)ser.Deserialize(reader);
                foreach (Custompo_naicscodeproductbundleassociation naicsCodeProductBundleDetail in result.Custompo_naicscodeproductbundleassociation)
                {
                    naicsCodeProductBundleDetail.__metadata = CreateMetaDataTag(Entities.po_naicscodeproductbundleassociation, naicsCodeProductBundleDetail.Po_naicscodeproductbundleassociationid);
                }
                res = result.Custompo_naicscodeproductbundleassociation;
            }
            response.d = new ResultsD();
            response.d.results = res;

            debug($"result.Custompo_naicscodeproductbundleassociation.Count: {result.Custompo_naicscodeproductbundleassociation.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }


    public NAICSCodeProductBundleAssociationController(ILogger<NAICSCodeProductBundleAssociationController> logger, IMemoryCache memoryCache, 
        IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
