﻿using Cin.WebApi.Models;
using Cin.WebApi;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.ServiceModel;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class ActivityController : BaseController
{
    private const string strTimeZoneOffset = " +0:00";

    [HttpGet("odata/activity")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/activity called by user {repo.Client.CallerId}");

        //OrganizationResponse responseWhoamI = CrmConnection.getCrmConnection(Request).Execute(new WhoAmIRequest());
        //string systemUserName = (string)CommonClass.RetrieveFieldFromEntityByKey(Request, Entities.SystemUserLogicalName, "systemuserid", ((WhoAmIResponse)responseWhoamI).UserId, Attributes.fullName);
        //string[] args = { Request.Method.Method.ToString(), Entities.activityName, systemUserName, "" };
        //CinTrace.TraceCustomDebug(level: "DEBUG", format: "Request: {0}, Entity: {1}, User: {2}", args: args);

        ActivityResultSet result = new ActivityResultSet();
        result.CustomActivity = new List<CustomActivity>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {

            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.activityName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }

            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            string lastAccount = null;
            string curAccount = null;

            if (resObj?.Count > 0)
                foreach (JsonEntity activity in resObj)
                {
                    foreach (JsonEntityAttribute attr in activity.Attributes)
                    {
                        switch (attr.Key)
                        {
                            case "regardingobjectid":
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    curAccount = jpv.Id.ToString();
                                }
                                break;
                        }
                    }

                    if (lastAccount == null || curAccount == null || lastAccount != curAccount)
                    {
                        var cEntity = new CustomActivity();
                        cEntity.__metadata = CreateMetaDataTag(Entities.activityName,activity.Id);

                        foreach (JsonEntityAttribute attr in activity.Attributes)
                        {
                            switch (attr.Key)
                            {
                                case "regardingobjectid":
                                    string sVal = JsonConvert.SerializeObject(attr.Value);
                                    JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                    if (jpv?.Id != Guid.Empty)
                                    {
                                        cEntity.RegardingObjectId = new Regardingobjectid();
                                        cEntity.RegardingObjectId.Text = jpv.Id.ToString();
                                        cEntity.RegardingObjectId.Name = jpv.Name;
                                    }
                                    break;
                                case "activitytypecode":
                                    cEntity.ActivityTypeCode = new ProductTypeCode();
                                    cEntity.ActivityTypeCode.Text = (string)attr.Value;
                                    cEntity.ActivityTypeCode.Formattedvalue = (string)attr.Value;
                                    break;
                                case "subject":
                                    cEntity.Subject = (string)attr.Value;
                                    break;
                                case "activityid":
                                    if (!string.IsNullOrEmpty((string)(attr.Value.ToString())))
                                        cEntity.ActivityId = new Guid((string)(attr.Value.ToString()));
                                    break;
                                case "modifiedon":
                                    cEntity.ModifiedOn = new ActivityModifiedOn();
                                    //cEntity.ModifiedOn.Date = ((DateTime)attr.Value).Date.ToLongDateString();
                                    //cEntity.ModifiedOn.Time = ((DateTime)attr.Value).ToLongTimeString();
                                    cEntity.ModifiedOn.Text = ((DateTime)attr.Value).ToString() + strTimeZoneOffset;
                                    break;
                            }
                        }

                        lastAccount = curAccount;
                        result.CustomActivity.Add(cEntity);
                    }
                }

            debug(result.CustomActivity.Count.ToString() + " activities downloaded");
            response.d = new ResultsD();
            response.d.results = result.CustomActivity;

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error::Message:{ex.Message}||Trace:{ex.StackTrace}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }


    public ActivityController(ILogger<ActivityController> logger, IMemoryCache memoryCache, 
        IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
