﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class BusinessDivisionController : BaseController
{
    
    [HttpGet("odata/businessdivision")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/businessdivision called by user {repo.Client.CallerId}");

        BusinessDivisionResultSet result = new BusinessDivisionResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {

            responseQuery = repo.RetrieveUserQuery(Request, Entities.businessDivisionName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.businessDivisionName, responseQuery.Message);

            List<CustomBusinessDivision> res = new List<CustomBusinessDivision>();
            XmlSerializer ser = new XmlSerializer(typeof(BusinessDivisionResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (BusinessDivisionResultSet)ser.Deserialize(reader);
                foreach (CustomBusinessDivision businessDivision in result.CustomBusinessDivision)
                {
                    businessDivision.__metadata = CreateMetaDataTag(Entities.businessDivisionName, 
                        businessDivision.Po_businessdivisionid.ToString());
                }
                res = result.CustomBusinessDivision;
            }
            response.d = new ResultsD();
            response.d.results = res;

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error: {ex.Detail}",ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    public BusinessDivisionController(ILogger<BusinessDivisionController> logger, IMemoryCache memoryCache, 
        IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
