﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.Diagnostics.Contracts;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class BusinessDivisionBundleAssociationController : BaseController
{
    [HttpGet("odata/BusinessDivisionBundleAssociation")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/BusinessDivisionBundleAssociation called by user {repo.Client.CallerId}");


        Po_businessdivisionbundleassociationResultSet result = new Po_businessdivisionbundleassociationResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {
            responseQuery = repo.RetrieveUserQuery(Request, Entities.po_businessdivisionbundleassociation);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.po_businessdivisionbundleassociation, responseQuery.Message);

            List<Custompo_businessdivisionbundleassociation> res = new List<Custompo_businessdivisionbundleassociation>();
            XmlSerializer ser = new XmlSerializer(typeof(Po_businessdivisionbundleassociationResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = ser.Deserialize(reader) as Po_businessdivisionbundleassociationResultSet;
                foreach (Custompo_businessdivisionbundleassociation businessDevBundleDetail in result.Custompo_businessdivisionbundleassociation)
                {
                    businessDevBundleDetail.__metadata = CreateMetaDataTag(Entities.po_businessdivisionbundleassociation,
                        businessDevBundleDetail.Po_businessdivisionbundleassociationid);
                }
                res = result.Custompo_businessdivisionbundleassociation;
            }
            response.d = new ResultsD();
            response.d.results = res;

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error: {ex.Detail}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }



    public BusinessDivisionBundleAssociationController(ILogger<BusinessDivisionBundleAssociationController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
