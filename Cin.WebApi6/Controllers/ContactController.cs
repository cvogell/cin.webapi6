﻿using Cin.WebApi.Models;
using Cin.WebApi;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.ServiceModel;
using Microsoft.AspNetCore.Http.Extensions;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class ContactController : BaseController
{
    
    [HttpGet("odata/contact")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/contact called by user {repo.Client.CallerId}");

        ContactResultSet result = new ContactResultSet();
        result.CustomContact = new List<CustomContact>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {
            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.contactName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }

            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc = CommonClass.XmlFrame(logicalName, responseQuery.Message);
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            //List<CustomContact> res = new List<CustomContact>();
            //XmlSerializer ser = new XmlSerializer(typeof(ContactResultSet));
            //using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            //{
            //    result = (ContactResultSet)ser.Deserialize(reader);
            //    foreach (CustomContact contact in result.CustomContact)
            foreach (JsonEntity contact in resObj)
            {

                var cEntity = new CustomContact();
                cEntity.__metadata = CreateMetaDataTag(Entities.contactName, contact.Id);

                foreach (JsonEntityAttribute attr in contact.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "emailaddress1":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.EmailAddress1 = (string)attr.Value;
                            break;
                        case "salutation":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Salutation = (string)attr.Value;
                            break;
                        case "fullname":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.FullName = (string)attr.Value;
                            break;
                        case "contactid":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.ContactId = new Guid((string)attr.Value);
                            break;
                        case "firstname":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.FirstName = (string)attr.Value;
                            break;
                        case "lastname":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.LastName = (string)attr.Value;
                            break;
                        case "telephone1":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Telephone1 = (string)attr.Value;
                            break;
                        case "address1_postalcode":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Address1_PostalCode = (string)attr.Value;
                            break;
                        case "address1_line3":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Address1_Line3 = (string)attr.Value;
                            break;
                        case "address1_line1":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Address1_Line1 = (string)attr.Value;
                            break;
                        case "address1_country":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Address1_Country = (string)attr.Value;
                            break;
                        case "address1_city":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Address1_City = (string)attr.Value;
                            break;
                        case "address1_line2":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Address1_Line2 = (string)attr.Value;
                            break;
                        case "parentcustomerid":
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.ParentCustomerId = new Parentcustomerid();
                                cEntity.ParentCustomerId.Text = jpv.Id.ToString();
                                cEntity.ParentCustomerId.Name = jpv.Name;
                            }
                            break;
                        case "address1_stateorprovince":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Address1_StateOrProvince = (string)attr.Value;
                            break;
                        case "jobtitle":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.JobTitle = ((string)attr.Value).Length > 25 ? ((string)attr.Value).Substring(0, 25) : (string)attr.Value;
                            break;
                    }
                }
                result.CustomContact.Add(cEntity);

            }

            //res = result.CustomContact;
            //}
            response.d = new ResultsD();
            response.d.results = result.CustomContact;
            debug($"result.CustomContact.Count: {result.CustomContact.Count}");
            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error($"unexpected error: {ex.Message}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    [HttpPost("odata/contact")]
    public IActionResult Create(CustomContact contactCreate)
    {
        _ = repo.WhoAmI(Request);
        info($"[POST] odata/contact called by user {repo.Client.CallerId}");

        string jsonEntity = JsonConvert.SerializeObject(contactCreate);

        HttpResponseMessage httpResponse = new HttpResponseMessage();

        try
        {
            Entity contact = new Entity();
            contact.LogicalName = Entities.contactLogicalName;
            contact.Id = contactCreate.ContactId;

            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            if (string.IsNullOrEmpty(contactCreate.LastName))
                return BadRequest(Constants.lastNameCheck);

            AssignCustomContactValues(contact, contactCreate);
            CreateRequest reqCreateContact = new CreateRequest();
            reqCreateContact.Target = contact;
            CreateResponse responseContact = (CreateResponse)repo.Client.Execute(reqCreateContact);
            if (responseContact.ResponseName == Constants.create && responseContact.id != Guid.Empty)
            {
                CreateResponseResult result = new();
                contactCreate.__metadata = CreateMetaDataTag(Entities.contactName, responseContact.id.ToString());
                contactCreate.ContactId = responseContact.id;
                result.d = contactCreate;
                return Created(Request.GetDisplayUrl(),result);
                //httpResponse = Request.CreateResponse(HttpStatusCode.Created, result);
            }
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error($"Unexpected error: {ex.Message}. Entity: {jsonEntity}", ex);
            return BadRequest(ex.Message);
        }

        return NoContent();
    }

    [HttpPost("odata/contact({key})")]
    public IActionResult Update(Guid key, CustomContact contactUpdate)
    {
        _ = repo.WhoAmI(Request);
        info($"odata/contact[{key}] called by user {repo.Client.CallerId}");


        string jsonEntity = JsonConvert.SerializeObject(contactUpdate);

        try
        {
            Entity contact = new()
            {
                LogicalName = Entities.contactLogicalName
            };


            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);

            if (key == Guid.Empty)
                return BadRequest(Entities.contactName + Constants.mandatoryId);

            contact[Attributes.contactId] = key;
            AssignCustomContactValues(contact, contactUpdate);

            UpdateRequest reqUpdateContact = new()
            {
                Target = contact
            };
            UpdateResponse responseUpdateContact = (UpdateResponse)repo.Client.Execute(reqUpdateContact);
            if (responseUpdateContact != null)
                return NoContent();

        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error($"unexpected error: {ex.Message}, request entity: {jsonEntity}", ex);
            return BadRequest(ex.Message);
        }

        return NoContent();
    }

    protected void AssignCustomContactValues(Entity contact, CustomContact contactAssign)
    {
        string jsonEntity = JsonConvert.SerializeObject(contactAssign);
        try
        {
            if (contactAssign.FirstName != null)
                contact[Attributes.firstName] = contactAssign.FirstName;
            if (contactAssign.LastName != null)
                contact[Attributes.lastName] = contactAssign.LastName;
            if (contactAssign.JobTitle != null)
                contact[Attributes.jobTitle] = contactAssign.JobTitle;
            if (contactAssign.Salutation != null)
                contact[Attributes.salutation] = contactAssign.Salutation;
            if (contactAssign.Telephone1 != null)
                contact[Attributes.telephone1] = contactAssign.Telephone1;
            if (contactAssign.EmailAddress1 != null)
                contact[Attributes.emailAddress1] = contactAssign.EmailAddress1;
            if (contactAssign.Address1_Line1 != null)
                contact[Attributes.address1_line1] = contactAssign.Address1_Line1;
            if (contactAssign.Address1_Line2 != null)
                contact[Attributes.address1_line2] = contactAssign.Address1_Line2;
            if (contactAssign.Address1_Line3 != null)
                contact[Attributes.address1_line3] = contactAssign.Address1_Line3;
            if (contactAssign.Address1_StateOrProvince != null)
                contact[Attributes.address1_StateOrProvince] = contactAssign.Address1_StateOrProvince;
            if (contactAssign.Address1_PostalCode != null)
                contact[Attributes.address1_postalcode] = contactAssign.Address1_PostalCode;
            if (contactAssign.Address1_City != null)
                contact[Attributes.address1_city] = contactAssign.Address1_City;
            if (contactAssign.Address1_Country != null)
                contact[Attributes.address1_country] = contactAssign.Address1_Country;
            if (contactAssign.ParentCustomerId != null && !string.IsNullOrEmpty(contactAssign.ParentCustomerId.Text))
                contact[Attributes.parentcustomerid] = new EntityReference(Entities.accountLogicalName, new Guid(contactAssign.ParentCustomerId.Text));
            if (contactAssign.Ownerid != null && !string.IsNullOrEmpty(contactAssign.Ownerid.Text))
                contact[Attributes.ownerId] = new EntityReference(Constants.systemUser, new Guid(contactAssign.Ownerid.Text));
        }
        catch (Exception ex)
        {
            error($"unexpected error: {ex.Message}, request entity: {jsonEntity}", ex);
            throw;
        }
    }

    public ContactController(ILogger<ContactController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
