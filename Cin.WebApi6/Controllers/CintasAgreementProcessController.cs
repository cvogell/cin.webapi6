﻿using Cin.WebApi.Models;
using Cin.WebApi;
using Cin.WebApi.Repos;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.Diagnostics.Contracts;
using System.Reflection;
using System.ServiceModel;
using Microsoft.AspNetCore.Mvc;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class CintasAgreementProcessController : BaseController
{

    [HttpGet("odata/cintasagreementprocess")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/businessdivision called by user {repo.Client.CallerId}");


        CintasAgreementProcessResultSet result = new CintasAgreementProcessResultSet();
        result.CustomCintasAgreementProcess = new List<CustomCintasAgreementProcess>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {

            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.cintasAgreementProcessName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc = CommonClass.XmlFrame(Entities.cintasAgreementProcessName, responseQuery.Message);
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            //List<CustomCintasAgreementProcess> res = new List<CustomCintasAgreementProcess>();
            //XmlSerializer ser = new XmlSerializer(typeof(CintasAgreementProcessResultSet));
            //using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            //{
            //    result = (CintasAgreementProcessResultSet)ser.Deserialize(reader);
            //    foreach (CustomCintasAgreementProcess cintasAgreementProcess in result.CustomCintasAgreementProcess)
            foreach (JsonEntity cintasAgreementProcess in resObj)
            {
                var cEntity = new CustomCintasAgreementProcess();
                cEntity.__metadata = CreateMetaDataTag(Entities.cintasAgreementProcessName, cintasAgreementProcess.Id);

                foreach (JsonEntityAttribute attr in cintasAgreementProcess.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "statuscode":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeEnumValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal);
                                cEntity.Statuscode = new StatusCode();
                                cEntity.Statuscode.Text = jpv.Value.ToString();
                                break;
                            }
                        case "businessprocessflowinstanceid":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Businessprocessflowinstanceid = new Guid((string)attr.Value);
                            break;
                        case "bpf_opportunityid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Bpf_opportunityid = new Bpf_opportunityId();
                                    cEntity.Bpf_opportunityid.Text = jpv.Id.ToString();
                                    cEntity.Bpf_opportunityid.Name = jpv.Name;
                                }
                                break;
                            }
                        case "activestageid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Activestageid = new ActiveStageId();
                                    cEntity.Activestageid.Text = jpv.Id.ToString();
                                    cEntity.Activestageid.Name = jpv.Name;
                                }
                                break;
                            }
                        case "processid":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                                if (jpv.Id != Guid.Empty)
                                {
                                    cEntity.Processid = new ProcessId();
                                    cEntity.Processid.Text = jpv.Id.ToString();
                                    cEntity.Processid.Name = jpv.Name;
                                }
                                break;
                            }
                        case "bpf_name":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Bpf_name = (string)attr.Value;
                            break;
                        case "activestagestartedon":
                            cEntity.ActiveStageStartedOn = new ActiveStageStartedOn();
                            cEntity.ActiveStageStartedOn.Text = ((DateTime)attr.Value).ToString();
                            cEntity.ActiveStageStartedOn.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.ActiveStageStartedOn.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                    }
                }

                foreach (JsonEntityFormattedValue fmv in cintasAgreementProcess.FormattedValues)
                {
                    switch (fmv.Key)
                    {
                        case "statuscode":
                            cEntity.Statuscode.Name = fmv.Value;
                            break;
                    }
                }

                result.CustomCintasAgreementProcess.Add(cEntity);

            }
            //    res = result.CustomCintasAgreementProcess;
            response.d = new ResultsD();
            response.d.results = result.CustomCintasAgreementProcess;

            debug($"agreement process count: {result.CustomCintasAgreementProcess.Count}");
            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"Unexpected error: {ex.Message}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }


    public CintasAgreementProcessController(ILogger<CintasAgreementProcessController> logger, IMemoryCache memoryCache, 
        IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
