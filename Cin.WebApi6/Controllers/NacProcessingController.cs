﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.WebServiceClient;
using Microsoft.Xrm.Sdk;
using System.Diagnostics.Contracts;
using System.Net;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;
using Microsoft.AspNetCore.Http.Extensions;
using Cin.WebApi.Extensions;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class NacProcessingController : BaseController
{
    [HttpGet("odata/nacprocessing")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/nacprocessing called by user {repo.Client.CallerId}");

        NacProcessingResultSet result = new NacProcessingResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {

            responseQuery = repo.RetrieveUserQuery(Request, Entities.NacProcessingName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.NacProcessingName, responseQuery.Message);

            List<CustomNacProcessing> res = new List<CustomNacProcessing>();
            XmlSerializer ser = new XmlSerializer(typeof(NacProcessingResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (NacProcessingResultSet)ser.Deserialize(reader);
                foreach (CustomNacProcessing nacProcessing in result.CustomNacProcessing)
                {
                    nacProcessing.__metadata = CreateMetaDataTag(Entities.NacProcessingName, nacProcessing.po_nacprocessingId.ToString());
                }
                res = result.CustomNacProcessing;
            }
            response.d = new ResultsD();
            response.d.results = res;
            debug($"result.CustomNacProcessing.Count: {result.CustomNacProcessing.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    [HttpPost("odata/nacprocessing")]
    public IActionResult Create(CustomNacProcessing nacProcessingToCreate)
    {
        _ = repo.WhoAmI(Request);
        info($"[POST] odata/nacprocessing called by user {repo.Client.CallerId}");

        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(nacProcessingToCreate);

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);
            if (string.IsNullOrEmpty(nacProcessingToCreate.po_name))
                return BadRequest(Constants.mandatoryName);

            Entity nacProcessing = new Entity();
            nacProcessing = AssignCustomRecords(nacProcessingToCreate, Constants.Insert);
            CreateRequest createRequest = new CreateRequest();
            createRequest.Target = nacProcessing;
            var response = (CreateResponse)repo.Client.Execute(createRequest);

            if (response.ResponseName == Constants.create && response.id != Guid.Empty)
            {
                CreateResponseResult result = new CreateResponseResult();
                nacProcessingToCreate.__metadata = CreateMetaDataTag(Entities.NacProcessingName, response.id.ToString());
                nacProcessingToCreate.po_nacprocessingId = response.id;
                result.d = nacProcessingToCreate;
                return Created(Request.GetDisplayUrl(),result);
            }
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }

        return NoContent();

    }

    [HttpPost("odata/nacprocessing({key})")]
    public IActionResult Update(Guid key, CustomNacProcessing nacProcessingToUpdate)
    {
        _ = repo.WhoAmI(Request);
        info($"[POST] odata/nacprocessing({key}) called by user {repo.Client.CallerId}");

        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(nacProcessingToUpdate);

        try
        {
            if (repo.Client.CallerId == Guid.Empty)
                return BadRequest(Constants.connectionFailure);
            //if (string.IsNullOrEmpty(nacProcessingToUpdate.po_name))
            //    return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.mandatoryName);
            if (key == Guid.Empty)
                return BadRequest(Constants.mandatoryId);

            Entity nacProcessing = new Entity();
            nacProcessingToUpdate.po_nacprocessingId = key;
            nacProcessing = AssignCustomRecords(nacProcessingToUpdate, Constants.Update);
            UpdateRequest request = new UpdateRequest();
            request.Target = nacProcessing;
            UpdateResponse response = (UpdateResponse)repo.Client.Execute(request);
            if (response != null)
                return NoContent();

        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            return BadRequest(ex.Message);
        }

        return NoContent();
    }

    private Entity AssignCustomRecords(CustomNacProcessing clientEntity, string action)
    {
        string jsonEntity = Newtonsoft.Json.JsonConvert.SerializeObject(clientEntity);

        Entity nacProcessing = new Entity();
        nacProcessing.LogicalName = Entities.NacProcessingLogicalName;
        try
        {
            nacProcessing[Attributes.po_name] = clientEntity.po_name;
            if (action == Constants.Update)
                nacProcessing[Attributes.po_nacprocessingid] = clientEntity.po_nacprocessingId;
            if (clientEntity.OwnerId != null && !string.IsNullOrEmpty(clientEntity.OwnerId.Text))
                nacProcessing[Attributes.ownerId] = new EntityReference(Constants.systemUser, new Guid(clientEntity.OwnerId.Text));
            if (clientEntity.po_SoldDate_NAC != null && !string.IsNullOrEmpty(clientEntity.po_SoldDate_NAC.Text))
                nacProcessing[Attributes.po_solddate_nac] = Convert.ToDateTime(clientEntity.po_SoldDate_NAC.Text);
            if (clientEntity.po_SalesRep_NAC != null)
                nacProcessing[Attributes.po_salesrep_nac] = clientEntity.po_SalesRep_NAC;
            if (clientEntity.po_PowerAdd_NAC != null && !string.IsNullOrEmpty(clientEntity.po_PowerAdd_NAC.Text))
                nacProcessing[Attributes.po_poweradd_nac] = clientEntity.po_PowerAdd_NAC.Text.ToBool();
            if (clientEntity.po_EstimatedVolume_UR_NAC != null && !string.IsNullOrEmpty(clientEntity.po_EstimatedVolume_UR_NAC.Text))
                nacProcessing[Attributes.po_estimatedvolume_ur_nac] = new Money(Convert.ToDecimal(clientEntity.po_EstimatedVolume_UR_NAC.Text));
            if (clientEntity.po_WeeklyTotal_NAC != null && !string.IsNullOrEmpty(clientEntity.po_WeeklyTotal_NAC.Text))
                nacProcessing[Attributes.po_weeklytotal_nac] = new Money(Convert.ToDecimal(clientEntity.po_WeeklyTotal_NAC.Text));
            if (clientEntity.po_WORVolume_NAC != null && !string.IsNullOrEmpty(clientEntity.po_WORVolume_NAC.Text))
                nacProcessing[Attributes.po_worvolume_nac] = new Money(Convert.ToDecimal(clientEntity.po_WORVolume_NAC.Text));
            if (clientEntity.po_Mark_As_Sold != null && !string.IsNullOrEmpty(clientEntity.po_Mark_As_Sold.Text))
                nacProcessing[Attributes.po_mark_as_sold] = new EntityReference(Entities.markAsSoldLogicalName, new Guid(clientEntity.po_Mark_As_Sold.Text));
            if (clientEntity.po_Route_NAC != null)
                nacProcessing[Attributes.po_route_nac] = clientEntity.po_Route_NAC;
            if (clientEntity.po_InstallDate_NAC != null && !string.IsNullOrEmpty(clientEntity.po_InstallDate_NAC.Text))
                nacProcessing[Attributes.po_installdate_nac] = Convert.ToDateTime(clientEntity.po_InstallDate_NAC.Text);
            if (clientEntity.po_Notes_NAC != null)
                nacProcessing[Attributes.po_notes_nac] = clientEntity.po_Notes_NAC;
            if (clientEntity.po_BusinessDivision != null && !string.IsNullOrEmpty(clientEntity.po_BusinessDivision.Text))
                nacProcessing[Attributes.po_businessdivision] = new EntityReference(Attributes.po_businessdivision, 
                    new Guid(clientEntity.po_BusinessDivision.Text));
            if (clientEntity.po_LocationNumber != null && !string.IsNullOrEmpty(clientEntity.po_LocationNumber.Text))
                nacProcessing[Attributes.po_locationnumber] = new EntityReference(Entities.cintasLocationLogicalName, 
                    new Guid(clientEntity.po_LocationNumber.Text));
            if (clientEntity.po_ZipPostalCode_NAC != null)
                nacProcessing[Attributes.po_zippostalcode_nac] = clientEntity.po_ZipPostalCode_NAC;
            if (clientEntity.po_State_NAC != null)
                nacProcessing[Attributes.po_state_nac] = clientEntity.po_State_NAC;
            if (clientEntity.po_City_NAC != null)
                nacProcessing[Attributes.po_city_nac] = clientEntity.po_City_NAC;
            if (clientEntity.po_SoldVolume_NAC != null && !string.IsNullOrEmpty(clientEntity.po_SoldVolume_NAC.Text))
                nacProcessing[Attributes.po_soldvolume_nac] = new Money(Convert.ToDecimal(clientEntity.po_SoldVolume_NAC.Text));
            if (clientEntity.po_CustomerNumber_NAC != null)
                nacProcessing[Attributes.po_customernumber_nac] = clientEntity.po_CustomerNumber_NAC;
            if (clientEntity.po_Week_Nac != null && !string.IsNullOrEmpty(clientEntity.po_Week_Nac.Text))
                nacProcessing[Attributes.po_week_nac] = new OptionSetValue(Convert.ToInt32(clientEntity.po_Week_Nac.Text));
            if (clientEntity.po_Day_NAC != null && !string.IsNullOrEmpty(clientEntity.po_Day_NAC.Text))
                nacProcessing[Attributes.po_day_nac] = new OptionSetValue(Convert.ToInt32(clientEntity.po_Day_NAC.Text));
            if (clientEntity.ctas_primary_rep != null && !string.IsNullOrEmpty(clientEntity.ctas_primary_rep.Text))
                nacProcessing[Attributes.ctas_primary_rep] = new EntityReference(Entities.SystemUserLogicalName, 
                    new Guid(clientEntity.ctas_primary_rep.Text));
            if (clientEntity.Po_opportunity != null && !string.IsNullOrEmpty(clientEntity.Po_opportunity.Text))
                nacProcessing[Attributes.po_opportunity] = new EntityReference(Entities.opportunityLogicalName, 
                    new Guid(clientEntity.Po_opportunity.Text));

        }
        catch (Exception ex)
        {
            error(ex.Message, ex);
            throw ex;
        }
        return nacProcessing;
    }



    public NacProcessingController(ILogger<NacProcessingController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
