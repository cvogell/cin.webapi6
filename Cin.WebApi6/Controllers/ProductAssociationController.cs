using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Cin.WebApi;
using Microsoft.AspNetCore.Mvc;
namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class ProductAssociationController : BaseController
{

    [HttpGet("odata/productassociation")]
    public IActionResult UserQuery()
    {
        var systemUserName = repo.WhoAmI(Request);
        info("ProductAssociationController UserQuery Started");
        debug($"DEBUG:: Request: {Request.Method}, Entity: {Entities.ProductAssociationName}, user: {systemUserName}");

        ProductAssociationResultSet result = new ProductAssociationResultSet
        {
            CustomProductAssociation = new List<CustomProductAssociation>()
        };
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        
        try
        {
            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.ProductAssociationName);
            if(responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD()
                {
                    results = responseQuery.Message
                };
                return BadRequest(response);
            }

            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);
            foreach (JsonEntity productassociation in resObj)
            {
                var cEntity = new CustomProductAssociation
                {
                    __metadata = CreateMetaDataTag(Entities.ProductAssociationName, productassociation.Id)
                };

                foreach (JsonEntityAttribute attr in productassociation.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "productassociationid":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.ProductAssociationId = new Guid((string)attr.Value);
                            break;
                        case "productid":
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.ProductId = new ProductIdNR
                                {
                                    Text = jpv.Id.ToString(),
                                    Name = jpv.Name
                                };
                            }
                            break;
                        case "associatedproduct":
                            string sVal4 = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv4 = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal4);
                            cEntity.AssociatedProduct = new Associatedproduct
                            {
                                Text = jpv4.Id.ToString(),
                                Name = jpv4.Name
                            };
                            break;
                        case "uomid":
                            string sVal5 = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv5 = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal5);
                            cEntity.UoMId = new UomIdNR
                            {
                                Text = jpv5.Id.ToString(),
                                Name = jpv5.Name
                            };
                            break;
                        case "productisrequired":
                            string sVal2 = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeBooleanValue jpv2 = JsonConvert.DeserializeObject<JsonEntityAttributeBooleanValue>(sVal2);
                            cEntity.ProductIsRequired = new Productisrequired
                            {
                                Text = jpv2.StringValue
                            };
                            break;
                        case "propertycustomizationstatus":
                            string sVal3 = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeBooleanValue jpv3 = JsonConvert.DeserializeObject<JsonEntityAttributeBooleanValue>(sVal3);
                            cEntity.PropertyCustomizationStatus = new Propertycustomizationstatus
                            {
                                Text = jpv3.StringValue
                            };
                            break;
                    }
                }
                result.CustomProductAssociation.Add(cEntity);
            }
            response.d = new ResultsD()
            {
                results = result.CustomProductAssociation
            };
            debug($"Debug:: Request: {Request.Method}, Entity: {Entities.ProductAssociationName}, User: {systemUserName}, Payload Length: {result.CustomProductAssociation.Count}");
            return Ok(response);
        }
        catch (Exception ex)
        {
            error($"Exception:: Message: {ex.Message}, Trace: {ex.StackTrace}");
            return BadRequest(ex);
        }
    }
    public ProductAssociationController(ILogger<ProductAssociationController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}