using Cin.WebApi.CrmModels;
using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Cin.WebApi.Xrm;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Web.Http.OData;
using AllowedQueryOptions = System.Web.Http.OData.Query.AllowedQueryOptions;
using Newtonsoft.Json;
using Cin.WebApi;
using System.Net;
using Cin.WebApi.Extensions;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.AspNetCore.Mvc;
namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class ProductController : BaseController
{
    [HttpGet("odata/product")]
    public IActionResult UserQuery()
    {
        var systemUserName = repo.WhoAmI(Request);
        debug($"DEBUG:: Request: {Request.Method}, Entity: {Entities.ProductName}, User: {systemUserName}");

        ProductResultSet result = new ProductResultSet
        {
            CustomProduct = new List<CustomProduct>()
        };
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {
            responseQuery = repo.RetrieveUserQueryWithPagination(Request, Entities.ProductName);
            if(responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD
                {
                    results = responseQuery.Message
                };
                return BadRequest(response);
            }
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            foreach (JsonEntity product in resObj)
            {
                var cEntity = new CustomProduct
                {
                    __metadata = CreateMetaDataTag(Entities.ProductName, product.Id)
                };

                foreach (JsonEntityAttribute attr in product.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "description":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Description = (string)attr.Value;
                            break;
                        case "name":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.Name = (string)attr.Value;
                            break;
                        case "parentproductid":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.ParentProductId.Text = (string)attr.Value;
                            break;
                        case "productid":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.ProductId = new System.Guid((string)attr.Value);
                            break;
                        case "productnumber":
                            if (!string.IsNullOrEmpty((string)attr.Value))
                                cEntity.ProductNumber = (string)attr.Value;
                            break;
                        case "productstructure":
                            {
                                string sVal = JsonConvert.SerializeObject(attr.Value);
                                JsonEntityAttributeNumericValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeNumericValue>(sVal);
                                if (jpv.Value > 0)
                                {
                                    cEntity.ProductStructure = new Productstructure();
                                    cEntity.ProductStructure.Text = jpv.Value.ToString();
                                    cEntity.ProductStructure.Formattedvalue = jpv.Value.ToString();
                                }
                                break;
                            }
                        case "producttypecode":
                            string sVal2 = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeEnumValue jpv2 = JsonConvert.DeserializeObject<JsonEntityAttributeEnumValue>(sVal2);
                            if (jpv2.Value > 0)
                            {
                                cEntity.ProductTypeCode = new ProductTypeCode();
                                cEntity.ProductTypeCode.Text = jpv2.Value.ToString();
                                cEntity.ProductTypeCode.Formattedvalue = jpv2.Value.ToString();
                            }
                            break;
                    }
                }
                foreach (JsonEntityFormattedValue fmtVal in product.FormattedValues)
                {
                    switch (fmtVal.Key)
                    {
                        case "productstructure":
                            if (!string.IsNullOrEmpty(fmtVal.Value))
                                cEntity.ProductStructure.Name = fmtVal.Value;
                            break;
                        case "producttypecode":
                            if (!string.IsNullOrEmpty(fmtVal.Value))
                                cEntity.ProductTypeCode.Name = fmtVal.Value;
                            break;

                    }
                }
                result.CustomProduct.Add(cEntity);
            }
            response.d = new ResultsD()
            {
                results = result.CustomProduct
            };
            debug($"DEBUG:: Request: {Request.Method}, Entity: {Entities.ProductName}, User: {systemUserName}, PayLoad Length: {result.CustomProduct.Count}");
            return Ok(response);
        }
        catch (Exception ex)
        {
            debug($"Exception:: Message: {ex.Message}, Trace: {ex.StackTrace}");
            return BadRequest(ex);
        }
    }

    public ProductController(ILogger<ProductController> logger,
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) :
        base(logger, memoryCache, configuration,crmRepo)
        {
        }
}