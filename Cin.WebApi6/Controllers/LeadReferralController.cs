﻿using Cin.WebApi.Models;
using Cin.WebApi;
using Cin.WebApi.Repos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.Diagnostics.Contracts;
using System.ServiceModel;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class LeadReferralController : BaseController
{
    [HttpGet("odata/leadreferral")]
    public ResultSet FetchQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/leadreferral called by user {repo.Client.CallerId}");

        LeadReferralResultSet result = new LeadReferralResultSet();
        result.CustomLeadReferral = new List<CustomLeadReferral>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {
            responseQuery = repo.RetrieveFetchXMLQueryWithPagination(Request, Entities.leadreferralName,
                $@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
  <entity name='account'>
    <attribute name='name' />
    <attribute name='address1_city' />
    <attribute name='telephone1' />
    <attribute name='address1_line1' />
    <attribute name='address1_stateorprovince' />
    <attribute name='po_postalcodeid' />
    <attribute name='po_custofur' />
    <attribute name='po_custoffas' />
    <attribute name='po_custoffire' />
    <attribute name='po_custoffs' />
    <attribute name='po_custofchicagods' />
    <attribute name='po_custofcatalog' />
    <attribute name='po_naicsdescription' />
    <attribute name='po_naicscodeid' />
    <attribute name='primarycontactid' />
    <attribute name='po_dba' />
    <attribute name='accountid' />
    <order attribute='name' descending='false' />
    <filter type='and'>
      <condition attribute='statecode' operator='eq' value='0' />
    </filter>
    <link-entity name='email' from='regardingobjectid' to='accountid' link-type='inner' alias='em'>
    <attribute name='createdon' />
    <attribute name='modifiedon' />
      <filter type='and'>
        <condition attribute='createdon' operator='last-seven-days' />
        <condition attribute='ctas_cintas_com_lu' operator='not-null' />
      </filter>
      <link-entity name='activityparty' from='activityid' to='activityid' link-type='inner' alias='an'>
        <filter type='and'>
          <condition attribute='participationtypemask' operator='eq' value='2' />
          <condition attribute='partyid' operator='eq-userid' />
        </filter>
      </link-entity>
    </link-entity>
  </entity>
</fetch>");

            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            foreach (JsonEntity account in resObj)
            {
                var cEntity = new CustomLeadReferral();
                cEntity.Metadata = CreateDeltaMetadataTag(Entities.leadreferralName, account.Id);
                string sval;

                foreach (JsonEntityAttribute attr in account.Attributes)
                {
                    sval = JsonConvert.SerializeObject(attr.Value);
                    switch (attr.Key)
                    {
                        case "accountid":
                            if (!string.IsNullOrEmpty((string)(attr.Value.ToString())))
                                cEntity.AccountId = new Guid((string)(attr.Value.ToString()));
                            break;
                        case "name":
                            cEntity.Name = attr.Value.ToString();
                            break;
                        case "address1_city":
                            cEntity.Address1_City = attr.Value.ToString();
                            break;
                        case "telephone1":
                            cEntity.Telephone1 = attr.Value.ToString();
                            break;
                        case "address1_line1":
                            cEntity.Address1_Line1 = attr.Value.ToString();
                            break;
                        case "address1_stateorprovince":
                            cEntity.Address1_stateorprovince = attr.Value.ToString();
                            break;
                        case "po_postalcodeid":
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sval);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.po_Postalcodeid = new Po_postalcodeid();
                                cEntity.po_Postalcodeid.Text = jpv.Id.ToString();
                                cEntity.po_Postalcodeid.Name = jpv.Name;
                            }
                            break;
                        case "po_custofur":
                            cEntity.po_custofur = Boolean.Parse(attr.Value.ToString());
                            break;
                        case "po_custoffas":
                            cEntity.po_custoffas = Boolean.Parse(attr.Value.ToString());
                            break;
                        case "po_custoffire":
                            cEntity.po_custoffire = Boolean.Parse(attr.Value.ToString());
                            break;
                        case "po_custoffs":
                            cEntity.po_custoffs = Boolean.Parse(attr.Value.ToString());
                            break;
                        case "po_custofchicagods":
                            cEntity.po_custofchicagods = Boolean.Parse(attr.Value.ToString());
                            break;
                        case "po_custofcatalog":
                            cEntity.po_custofcatalog = Boolean.Parse(attr.Value.ToString());
                            break;
                        case "po_naicsdescription":
                            cEntity.po_NaicsDescription = (attr.Value.ToString());
                            break;
                        case "po_naicscodeid":
                            JsonEntityAttributeGuidValue jpvNAICS = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sval);
                            if (jpvNAICS.Id != Guid.Empty)
                            {
                                cEntity.po_NaicsCodeId = new Po_naicscodeid();
                                cEntity.po_NaicsCodeId.Text = jpvNAICS.Id.ToString();
                                cEntity.po_NaicsCodeId.Name = jpvNAICS.Name.ToString();
                            }
                            break;
                        case "po_dba":
                            cEntity.po_dba = attr.Value.ToString();
                            break;
                        /*                            case "ctas_no_employees_opt":
                                                        cEntity.ctas_noEmployeesOpt = JsonConvert.DeserializeObject<ctas_NoEmployeesOpt>(attr.Value.ToString());
                                                        break;
                                                    case "ctas_square_footage_opt":
                                                        cEntity.ctas_squareFootageOpt= JsonConvert.DeserializeObject<ctas_SquareFootageOpt>(attr.Value.ToString());
                                                        break;*/
                        case "em.modifiedon":
                            cEntity.ModifiedOn = new ModifiedOn();
                            var modifiedOn = Newtonsoft.Json.Linq.JObject.Parse(attr.Value.ToString());
                            if (modifiedOn != null)
                            {
                                cEntity.ModifiedOn.Text = ((DateTime)modifiedOn["Value"]).ToString();
                                cEntity.ModifiedOn.Date = ((DateTime)modifiedOn["Value"]).ToShortDateString();
                                cEntity.ModifiedOn.Time = ((DateTime)modifiedOn["Value"]).ToShortTimeString();
                            }
                            break;
                        case "em.createdon":
                            cEntity.CreatedOn = new CreatedOn();
                            var createdOn = Newtonsoft.Json.Linq.JObject.Parse(attr.Value.ToString());
                            if (createdOn != null)
                            {
                                cEntity.CreatedOn.Text = ((DateTime)createdOn["Value"]).ToString();
                                cEntity.CreatedOn.Date = ((DateTime)createdOn["Value"]).ToShortDateString();
                                cEntity.CreatedOn.Time = ((DateTime)createdOn["Value"]).ToShortTimeString();
                            }
                            break;
                    }
                }
                result.CustomLeadReferral.Add(cEntity);
            }
            response.d = new ResultsD();
            response.d.results = result.CustomLeadReferral;
            debug($"result.CustomLeadReferral.Count: {result.CustomLeadReferral.Count}");

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            error(ex.Message, ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }


    public LeadReferralController(ILogger<LeadReferralController> logger, IMemoryCache memoryCache, IConfiguration configuration, 
        CrmRepo crmRepo) : base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
