using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.Extensions.Caching.Memory;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using Cin.WebApi;
using Newtonsoft.Json;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class PostalCodeController : BaseController
{

    [HttpGet("odata/postalcodealignment")]
    public IActionResult UserQuery()
    {
        var systemUserName = repo.WhoAmI(Request);
        info($"odata/postalcodealignmentcalled by user {repo.Client.CallerId}");
        info("PostalCodeController UserQuery Started");
        debug($"DEBUG:: Request: {Request.Method}, Entity: {Entities.postalCodeName}, user: {systemUserName}");
        
        PostalCodeResultSet result = new PostalCodeResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();

        try
        {
            responseQuery = repo.RetrieveFetchXMLQueryWithPagination(Request, Entities.postalCodeAlignmentName, 
                                @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                    <entity name='po_postalcodealignment'>
                                        <attribute name='po_postalcode' />
                                        <attribute name='po_postalcodename' />
                                        <attribute name='po_location' />
                                        <attribute name='po_businessdivision' />
                                        <attribute name='po_salesrep' />
                                        <attribute name='ctas_import_source' />
                                        <attribute name='po_postalcodealignmentid' />
                                        <filter type='and'>
                                        <condition attribute='statecode' operator='eq' value='0' />
                                        <condition attribute='ctas_import_source' operator='eq' value='672950000' />
                                        <condition attribute='po_salesrep' operator='eq-userid' />
                                        </filter>
                                        <link-entity name='po_postalcode' from='po_postalcodeid' to='po_postalcode' visible='false' link-type='outer' alias='pc'>
                                        <attribute name='po_city' />
                                        <attribute name='po_state' />
                                        </link-entity>
                                    </entity>
                                </fetch>");
            if (responseQuery.ResponseName == Constants.connectionFailure)
            {
                response.d = new ResultsD
                {
                    results = responseQuery.Message
                };
                return BadRequest(response);
            }
            #region Code without pagination
            /*
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.postalCodeAlignmentName, responseQuery.Message);

            List<CustomPostalCode> res = new List<CustomPostalCode>();
            XmlSerializer ser = new XmlSerializer(typeof(PostalCodeResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (PostalCodeResultSet)ser.Deserialize(reader);
                foreach (CustomPostalCode pca in result.CustomPostalCode)
                {
                    var metadata = new MetadataTag
                    {
                        uri = string.Format(appConfig.MetadataUrl + Entities.postalCodeAlignmentName + "({0})", pca.Po_PostalCodeId),
                        type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + Entities.postalCodeName
                    };
                    pca.__metadata = metadata;
                    
                }
                res = result.CustomPostalCode;
            }
            response.d = new ResultsD
            {
                results = res
            };
            */
            #endregion
            // New Code to apply Pagination for Canada Sales Rep
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);
            result.CustomPostalCode = new List<CustomPostalCode>();
            foreach (JsonEntity postalCode in resObj)
            {
                var metadata = new MetadataTag();
                metadata.uri = string.Format(appConfig.MetadataUrl + Entities.postalCodeAlignmentName + "({0})", postalCode.Id);
                metadata.type = appConfig.ModelNamespace + Delimiters.Dot + Constants.entityCustom + Entities.postalCodeName;
                var cEntity = new CustomPostalCode();
                cEntity.__metadata = metadata;
                // Mapping the result with the PostalCode Class
                cEntity.Po_PostalCodeId = new Guid(postalCode.Id.ToString());
                foreach (JsonEntityAttribute attr in postalCode.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "po_name":
                            string name = JsonConvert.SerializeObject(attr.Value);
                            if (!string.IsNullOrEmpty(name))
                            {
                                cEntity.Po_name = name;
                            }
                            break;
                        case "po_postalcode":
                            if (!string.IsNullOrEmpty((string)(attr.Value.ToString())))
                            {
                                cEntity.Po_postalcode = new Po_postalcode()
                                {
                                    Text = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(attr.Value))["Name"].ToString()
                                };
                            }
                            break;
                        case "pc.po_state":
                            if (!string.IsNullOrEmpty((string)(attr.Value.ToString())))
                            {
                                cEntity.Po_state = new Po_state()
                                {
                                    Text = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(attr.Value))["Value"]
                                };
                            }
                            break;
                        case "pc.po_city":
                            if (!string.IsNullOrEmpty((string)(attr.Value.ToString())))
                            {
                                cEntity.Po_city = new Po_city()
                                {
                                    Text = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(attr.Value))["Value"]
                                };
                            }
                            break;
                    }
                }
                result.CustomPostalCode.Add(cEntity);
            }
            response.d = new ResultsD
            {
                results = result.CustomPostalCode
            };

            debug($"DEBUG:: Request: {Request.Method}, Entity: {Entities.postalCodeName}, User: {systemUserName}, Payload Length: {result.CustomPostalCode.Count}");
            return Ok(response);
        }
        catch (Exception ex)
        {
            error($"Exception:: Message: {ex.Message}, Trace: {ex.StackTrace}");
            return BadRequest(ex);
        }
    }
    public PostalCodeController(ILogger<PostalCodeController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}