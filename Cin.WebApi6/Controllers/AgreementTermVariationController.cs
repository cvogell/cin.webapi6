﻿using Cin.WebApi.Models;
using Cin.WebApi.Repos;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;
using Microsoft.AspNetCore.Mvc;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class AgreementTermVariationController : BaseController
{

    [HttpGet("odata/AgreementTermVariation")]
    public ResultSet UserQuery()
    {
        _ = repo.WhoAmI(Request);
        info($"odata/AgreementTermVariation called by user {repo.Client.CallerId}");


        AgreementTermVariationResultSet result = new AgreementTermVariationResultSet();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        try
        {
            responseQuery = repo.RetrieveUserQuery(Request, Entities.agreementTermVariationName);
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = XmlFrame(Entities.agreementTermVariationName, responseQuery.Message);


            List<CustomAgreementTermVariation> res = new List<CustomAgreementTermVariation>();
            XmlSerializer ser = new XmlSerializer(typeof(AgreementTermVariationResultSet));
            using (TextReader reader = new StringReader(xmlDoc.InnerXml))
            {
                result = (AgreementTermVariationResultSet)ser.Deserialize(reader);
                foreach (CustomAgreementTermVariation agreementTermVariation in result.CustomAgreementTermVariation)
                {
                    agreementTermVariation.__metadata = CreateMetaDataTag(Entities.agreementTermVariationName,
                        agreementTermVariation.Po_agreementtermvariationid.ToString());
                }
                res = result.CustomAgreementTermVariation;
            }
            response.d = new ResultsD
            {
                results = res
            };
            info($"returning {res.Count} records");
            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"{ex.Message}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }


    public AgreementTermVariationController(ILogger<AgreementTermVariationController> logger, 
        IMemoryCache memoryCache, IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
