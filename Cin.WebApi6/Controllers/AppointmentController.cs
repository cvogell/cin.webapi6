﻿using Cin.WebApi.Models;
using Cin.WebApi;
using Cin.WebApi.Repos;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System.ServiceModel;
using Microsoft.AspNetCore.Mvc;

namespace Cin.WebApi6.Controllers;

[ApiController]
[Route("")]
public class AppointmentController : BaseController
{
    [HttpGet("odata/appointment")]
    public ResultSet UserQuery()
    {
        repo.WhoAmI(Request);

        AppointmentResultSet result = new AppointmentResultSet();
        result.CustomAppointment = new List<CustomAppointment>();
        CommonResponse responseQuery = new CommonResponse();
        ResultSet response = new ResultSet();
        string message = string.Empty;

        try
        {
            responseQuery = repo.RetrieveFetchXMLQueryWithPagination(Request, Entities.appointmentName,
                $@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                          <entity name='appointment'>
                            <attribute name='activityid' />
                            <attribute name='scheduledstart' />
                            <attribute name='scheduledend' />
                            <attribute name='regardingobjectid' />
                            <attribute name='instancetypecode' />
                            <attribute name='prioritycode' />
                            <attribute name='location' />
                            <attribute name='po_meeting_with' />
                            <attribute name='description' />
                            <attribute name='ownerid' />
                            <attribute name='modifiedon' />
                            <attribute name='createdon' />
                            <attribute name='ctas_appointment_type' />
                            <order attribute='scheduledstart' descending='true' />
                            <filter type='and'>
                              <condition attribute='ctas_appointment_type' operator='eq' value='672950002' />
                                <filter type='or'>
                                <condition attribute='scheduledstart' operator='today' />
                                <condition attribute='scheduledstart' operator='next-x-days' value='{appConfig.NextXDays}' />
                                <condition attribute='scheduledstart' operator='last-x-days' value='{appConfig.LastXDays}' />
                                </filter>
                               <condition attribute='ownerid' operator='eq-userid' />
                            </filter>
                            <link-entity name='account' from='accountid' to='regardingobjectid' link-type='inner' alias='as'>
                            <attribute name='accountid' />
                              <filter type='and'>
                                <condition attribute='statecode' operator='eq' value='0' />
                              </filter>
                            </link-entity>
                          </entity>
                        </fetch>");
            //responseQuery = CommonClass.RetrieveUserQueryWithPagination(Request, Entities.appointmentName, new string[] 
            //{Attributes.scheduledStart, Attributes.scheduledEnd, Attributes.RegardingObjectId, Attributes.ownerId, Attributes.instanceTypeCode, Attributes.priorityCode, Attributes.ctas_Appointment_Type,
            // Attributes.location, Attributes.po_Meeting_With, Attributes.description, Attributes.requiredAttendees, Attributes.optionalAttendees, Attributes.modifiedOn, Attributes.createdOn});
            if (responseQuery.ResponseName == Constants.failureResponse)
            {
                response.d = new ResultsD();
                response.d.results = responseQuery.Message;
                return response;
            }
            List<JsonEntity> resObj = JsonConvert.DeserializeObject<List<JsonEntity>>(responseQuery.Message);

            foreach (JsonEntity appointment in resObj)
            {
                var cEntity = new CustomAppointment();
                cEntity.__metadata = CreateMetaDataTag(Entities.appointmentName, appointment.Id);

                foreach (JsonEntityAttribute attr in appointment.Attributes)
                {
                    switch (attr.Key)
                    {
                        case "activityid":
                            if (!string.IsNullOrEmpty((string)(attr.Value.ToString())))
                                cEntity.ActivityId = new Guid((string)(attr.Value.ToString()));
                            break;
                        case "modifiedon":
                            cEntity.ModifiedOn = new ModifiedOn();
                            cEntity.ModifiedOn.Text = ((DateTime)attr.Value).ToString();
                            cEntity.ModifiedOn.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.ModifiedOn.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "ownerid":
                            string sVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jpv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(sVal);
                            if (jpv.Id != Guid.Empty)
                            {
                                cEntity.Ownerid = new OwnerId();
                                cEntity.Ownerid.Text = jpv.Id.ToString();
                                cEntity.Ownerid.Name = jpv.Name;
                            }
                            break;
                        case "scheduledstart":
                            cEntity.ScheduledStart = new ScheduledStart();
                            cEntity.ScheduledStart.Text = ((DateTime)attr.Value).ToString();
                            cEntity.ScheduledStart.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.ScheduledStart.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "scheduledend":
                            cEntity.ScheduledEnd = new ScheduledEnd();
                            cEntity.ScheduledEnd.Text = ((DateTime)attr.Value).ToString();
                            cEntity.ScheduledEnd.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.ScheduledEnd.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "regardingobjectid":
                            //cEntity.RegardingObjectId = attr.Value.ToString();
                            string rVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityAttributeGuidValue jepv = JsonConvert.DeserializeObject<JsonEntityAttributeGuidValue>(rVal);
                            if (jepv.Id != Guid.Empty)
                            {
                                cEntity.RegardingObjectId = new Regardingobjectid();
                                cEntity.RegardingObjectId.Text = jepv.Id.ToString();
                                cEntity.RegardingObjectId.Name = jepv.Name;
                            }
                            break;
                        case "instancetypecode":
                            string iVal = JsonConvert.SerializeObject(attr.Value);
                            //JsonEntityAttribute  ijpv = JsonConvert.DeserializeObject<JsonEntityAttribute>(iVal);
                            JsonEntityFormattedValue jefv = JsonConvert.DeserializeObject<JsonEntityFormattedValue>(iVal);
                            cEntity.InstanceTypeCode = new InstanceTypeCode();
                            cEntity.InstanceTypeCode.Value = jefv.Value.ToString();
                            break;
                        case "prioritycode":
                            string pVal = JsonConvert.SerializeObject(attr.Value);
                            //JsonEntityAttribute  ijpv = JsonConvert.DeserializeObject<JsonEntityAttribute>(iVal);
                            JsonEntityFormattedValue pjefv = JsonConvert.DeserializeObject<JsonEntityFormattedValue>(pVal);
                            cEntity.PriorityCode = new PriorityCode();
                            cEntity.PriorityCode.Value = pjefv.Value.ToString();
                            break;
                        case "location":
                            cEntity.Location = attr.Value.ToString();
                            break;
                        case "po_meeting_with":
                            cEntity.po_Meeting_With = attr.Value.ToString();
                            break;
                        case "description":
                            cEntity.Description = attr.Value.ToString();
                            break;
                        case "requiredattendees":
                            /*string roaval = JsonConvert.SerializeObject(attr.Value);
                            RequiredAttendees ra = JsonConvert.DeserializeObject<RequiredAttendees>(roaval);
                            cEntity.RequiredAttendees = ra;*/
                            break;
                        case "optionalattendees":
                            /*string oaval = JsonConvert.SerializeObject(attr.Value);
                            OptionalAttendees oa = JsonConvert.DeserializeObject<OptionalAttendees>(oaval);
                            cEntity.OptionalAttendees = oa;*/
                            //cEntity.OptionalAttendees.EntityName = oa.EntityName;
                            //cEntity.OptionalAttendees.MoreRecords = oa.MoreRecords;
                            //cEntity.OptionalAttendees.TotalRecordCountLimitExceeded = oa.TotalRecordCountLimitExceeded;
                            break;
                        case "createdon":
                            cEntity.CreatedOn = new CreatedOn();
                            cEntity.CreatedOn.Text = ((DateTime)attr.Value).ToString();
                            cEntity.CreatedOn.Date = ((DateTime)attr.Value).ToShortDateString();
                            cEntity.CreatedOn.Time = ((DateTime)attr.Value).ToShortTimeString();
                            break;
                        case "ctas_appointment_type":
                            string cVal = JsonConvert.SerializeObject(attr.Value);
                            JsonEntityFormattedValue cjefv = JsonConvert.DeserializeObject<JsonEntityFormattedValue>(cVal);
                            cEntity.ctas_Appointment_Type = new ctas_Appointment_Type();
                            cEntity.ctas_Appointment_Type.Value = cjefv.Value;
                            break;
                    }
                }
                result.CustomAppointment.Add(cEntity);
            }
            response.d = new ResultsD();
            response.d.results = result.CustomAppointment;

            return response;
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            //response.results = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, true);
            error($"{ex.Message}", ex);
            response.d = new ResultsD();
            response.d.results = ex.Detail;
            return response;
        }
    }

    public AppointmentController(ILogger<AppointmentController> logger, IMemoryCache memoryCache, 
        IConfiguration configuration, CrmRepo crmRepo) : 
        base(logger, memoryCache, configuration, crmRepo)
    {
    }
}
