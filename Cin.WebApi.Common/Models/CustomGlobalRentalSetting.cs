﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;


[XmlRoot(ElementName = "CustomRentalPolicy")]
public class CustomGlobalRentalSetting
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_name;
    [XmlElement(ElementName = "po_name")]
    public string Po_Name
    {
        get => _po_name.EscapeSingleQuote();
        set => _po_name = value.EscapeSingleQuote();
    }

    private string _po_cintasagreementprocessguid;
    [XmlElement(ElementName = "po_cintasagreementprocessguid")]
    public string Po_CintasAgreementProcessGuid
    {
        get => _po_cintasagreementprocessguid.EscapeSingleQuote();
        set => _po_cintasagreementprocessguid = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_newbusinessopportunitytype")]
    public Po_NewBusinessOpportunityType Po_NewBusinessOpportunityType { get; set; }

    [XmlElement(ElementName = "po_poweraddopportunitytype")]
    public Po_PowerAddOpportunityType Po_PowerAddOpportunityType { get; set; }

    [XmlElement(ElementName = "po_defaultuscurrency")]
    public Po_DefaultUSCurrency Po_DefaultUSCurrency { get; set; }

    [XmlElement(ElementName = "po_quotepricelevelid")]
    public Po_QuotePriceLevelId Po_QuotePriceLevelId { get; set; }

    [Key]
    [XmlElement(ElementName = "po_rentalpolicyid")]
    public Guid Po_RentalPolicyId { get; set; }

    [XmlElement(ElementName = "createdby")]
    public Createdby Createdby { get; set; }

    [XmlElement(ElementName = "ownerid")]
    public OwnerId OwnerId { get; set; }

}


[XmlRoot(ElementName = "po_newbusinessopportunitytype")]
public class Po_NewBusinessOpportunityType
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_poweraddopportunitytype")]
public class Po_PowerAddOpportunityType
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_quotepricelevelid")]
public class Po_QuotePriceLevelId
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_defaultuscurrency")]
public class Po_DefaultUSCurrency
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    
    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "GlobalRentalSettingResultSet")]
public class GlobalRentalSettingResultSet
{
    [XmlElement(ElementName = "CustomGlobalRentalSetting")]
    public List<CustomGlobalRentalSetting> CustomGlobalRentalSetting { get; set; }
}

public class JSONGlobalRentalSetting
{
    public string odataetag { get; set; }
    public string po_rentalpolicyid { get; set; }
    public string _po_defaultuscurrency_value { get; set; }
    public string _po_defaultuscurrency_currencyname { get; set; }
    public string _po_quotepricelevelid_value { get; set; }
    public string _po_quotepricelevelid_name { get; set; }
    public int po_newbusinessopportunitytype { get; set; }
    public int po_poweraddopportunitytype { get; set; }
    public string po_name { get; set; }
    public string po_cintasagreementprocessguid { get; set; }
}