﻿using Cin.WebApi.Common;
using Cin.WebApi.Extensions;
using System.Xml.Serialization;

namespace Cin.WebApi.Models;

//ChatakonduV E0143633 Replaced DynamicsFormattedValue with DynamicsFormattedNumericValue
[XmlRoot(ElementName = "po_cintergy_competitor_id")]
public class Po_cintergy_competitor_id : DynamicsFormattedNumericValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get =>  _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}