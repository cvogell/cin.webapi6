﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomProposalsAndAgreements")]
public class CustomProposalsAndAgreements
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _ctas_name;
    [XmlElement(ElementName = "ctas_name")]
    public string Ctas_Name
    {
        get => _ctas_name.EscapeSingleQuote();
        set => _ctas_name = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "statecode")]
    public StateCode StateCode { get; set; }
    [XmlElement(ElementName = "ctas_signaturetype")]
    public Ctas_SignatureType Ctas_SignatureType { get; set; }
    [XmlElement(ElementName = "ctas_sentdate")]
    public Ctas_SentDate Ctas_SentDate { get; set; }
    [XmlElement(ElementName = "ctas_opportunity")]
    public Ctas_Opportunity Ctas_Opportunity { get; set; }
    [XmlElement(ElementName = "ctas_expirationdate")]
    public Ctas_ExpirationDate Ctas_ExpirationDate { get; set; }

    private string _ctas_docusignenvelopeid;
    [XmlElement(ElementName = "ctas_docusignenvelopeid")]
    public string Ctas_DocuSignEnvelopeId
    {
        get => _ctas_docusignenvelopeid.EscapeSingleQuote();
        set => _ctas_docusignenvelopeid = value.EscapeSingleQuote();
    }

    private string _ctas_documenturl;
    [XmlElement(ElementName = "ctas_documenturl")]
    public string Ctas_DocumentUrl
    {
        get => _ctas_documenturl.EscapeSingleQuote();
        set => _ctas_documenturl = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "ctas_documenttype")]
    public Ctas_DocumentType Ctas_DocumentType { get; set; }
    [XmlElement(ElementName = "ctas_documentlocation")]
    public Ctas_DocumentLocation Ctas_DocumentLocation { get; set; }
    [XmlElement(ElementName = "ctas_customersigneddate")]
    public Ctas_CustomerSignedDate Ctas_CustomerSignedDate { get; set; }
    [XmlElement(ElementName = "ctas_propagrmtcurrentrevisionid")]
    public Ctas_PropagrmtCurrentRevisionId Ctas_PropagrmtCurrentRevisionId { get; set; }

    [Key]
    [XmlElement(ElementName = "ctas_proposalsandagreementsid")]
    public Guid Ctas_ProposalsAndAgreementsId { get; set; }

    private string _ctas_azuredocumenturl;
    [XmlElement(ElementName = "ctas_azuredocumenturl")]
    public string Ctas_AzureDocumentUrl
    {
        get => _ctas_azuredocumenturl.EscapeSingleQuote();
        set => _ctas_azuredocumenturl = value.EscapeSingleQuote();
    }

}

[XmlRoot(ElementName = "ctas_signaturetype")]
public class Ctas_SignatureType : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_sentdate")]
public class Ctas_SentDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_opportunity")]
public class Ctas_Opportunity : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_expirationdate")]
public class Ctas_ExpirationDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_documenttype")]
public class Ctas_DocumentType : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_documentlocation")]
public class Ctas_DocumentLocation : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_customersigneddate")]
public class Ctas_CustomerSignedDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_propagrmtcurrentrevisionid")]
public class Ctas_PropagrmtCurrentRevisionId : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ProposalsAndAgreementsResultSet")]
public class ProposalsAndAgreementsResultSet
{
    [XmlElement(ElementName = "CustomProposalsAndAgreements")]
    public List<CustomProposalsAndAgreements> CustomProposalsAndAgreements { get; set; }
}
public class JSONProposalsAndAgreements
{
    public string odataetag { get; set; }
    public string ctas_name { get; set; }
    public string statecode { get; set; }
    public string ctas_signaturetype { get; set; }

    public string ctas_sentdate { get; set; }
    public string _ctas_opportunity_value { get; set; }
    public string _ctas_opportunity_name { get; set; }

    public string ctas_expirationdate { get; set; }
    public string ctas_docusignenvelopeid { get; set; }
    public string ctas_documenturl { get; set; }
    public string ctas_documenttype { get; set; }
    public string ctas_documentlocation { get; set; }
    public string ctas_customersigneddate { get; set; }
    public string _ctas_propagrmtcurrentrevisionid_value { get; set; }
    public string _ctas_propagrmtcurrentrevisionid_ctas_name { get; set; }

    public string ctas_proposalsandagreementsid { get; set; }
    public string ctas_azuredocumenturl { get; set; }
}