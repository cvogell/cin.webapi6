﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "po_parentterm")]
public class Po_parentterm : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "CustomAgreementTermVariation")]
public class CustomAgreementTermVariation
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_termvariationcode;
    [XmlElement(ElementName = "po_termvariationcode")]
    public string Po_termvariationcode
    {
        get => _po_termvariationcode.EscapeSingleQuote();
        set => _po_termvariationcode = value.EscapeSingleQuote();
    }

    private string _po_termvariationtext;
    [XmlElement(ElementName = "po_termvariationtext")]
    public string Po_termvariationtext
    {
        get => _po_termvariationtext.EscapeSingleQuote();
        set => _po_termvariationtext = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_sortorder")]
    public Po_sortorder Po_sortorder { get; set; }
    [XmlElement(ElementName = "po_parentterm")]
    public Po_parentterm Po_parentterm { get; set; }

    //[Key]
    //[XmlElement(ElementName = "po_agreementtermvariationid")]
    //public string Po_agreementtermvariationid { get; set; }
    private Guid _po_agreementtermvariationid;
    [Key]
    [XmlElement(ElementName = "po_agreementtermvariationid")]
    public Guid Po_agreementtermvariationid
    {
        get => _po_agreementtermvariationid;
        set => _po_agreementtermvariationid = value;
    }
}

[XmlRoot(ElementName = "AgreementTermVariationResultSet")]
public class AgreementTermVariationResultSet
{
    [XmlElement(ElementName = "CustomAgreementTermVariation")]
    public List<CustomAgreementTermVariation> CustomAgreementTermVariation { get; set; }
}

public class JSONAgreementTermVariation
{
    public string odataetag { get; set; }
    public string po_termvariationcode { get; set; }
    public string po_termvariationtext { get; set; }
    public int po_sortorder { get; set; }
    public string _po_parentterm_po_termcode { get; set; }
    public string _po_parentterm_value { get; set; }
    public Guid po_agreementtermvariationid { get; set; }
}