﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "countrycode")]
public class CountryCode : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "language")]
public class Language : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "contractregion")]
public class ContractRegion : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_sortorder")]
public class Po_sortorder : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "CustomAgreementTerm")]
public class CustomAgreementTerm
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_termcode;
    [XmlElement(ElementName = "po_termcode")]
    public string Po_termcode
    {
        get => _po_termcode.EscapeSingleQuote();
        set => _po_termcode = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_sortorder")]
    public Po_sortorder Po_sortorder { get; set; }
    [XmlElement(ElementName = "po_businessdivision")]
    public Ctas_ur_location_text Po_businessdivision { get; set; }
    [XmlElement(ElementName = "ctas_countrycode")]
    public CountryCode ctas_countrycode { get; set; }
    [XmlElement(ElementName = "ctas_language")]
    public Language ctas_language { get; set; }
    [XmlElement(ElementName = "ctas_contractregion")]
    public ContractRegion ctas_contractregion { get; set; }

    [Key]
    [XmlElement(ElementName = "po_agreementtermid")]
    public Guid Po_agreementtermid { get; set; }
}

[XmlRoot(ElementName = "AgreementTermResultSet")]
public class AgreementTermResultSet
{
    [XmlElement(ElementName = "CustomAgreementTerm")]
    public List<CustomAgreementTerm> CustomAgreementTerm { get; set; }
}

public class JSONAgreementTerm
{
    public string odataetag { get; set; }
    public string _po_businessdivision_value { get; set; }
    public string _po_businessdivision_po_businessdivision { get; set; }
    public string po_termcode { get; set; }
    public int ctas_countrycode { get; set; }
    public int po_sortorder { get; set; }
    public string po_agreementtermid { get; set; }
    public int ctas_language { get; set; }
    public int? ctas_contractregion { get; set; }
}