﻿using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;
using Cin.WebApi.Extensions;
using Cin.WebApi.Common;

namespace Cin.WebApi.Models;


[XmlRoot(ElementName = "po_13weekcalcfield")]
public class Po_13weekcalcfield : DynamicsDateTimeUTCValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "opportunityid")]
public class OpportunityId : DynamicsEntityNullReference
{
    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "pricelevelid")]
public class Pricelevelid : DynamicsEntityNullReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "customerid")]
public class CustomerId : DynamicsEntityNullReference
{
    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }


    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}


[XmlRoot(ElementName = "CustomQuote")]
public class CustomQuote
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }
    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "statuscode")]
    public StatusCode StatusCode { get; set; }
    [XmlElement(ElementName = "statecode")]
    public StateCode StateCode { get; set; }

    private string _quotenumber;
    [XmlElement(ElementName = "quotenumber")]
    public string QuoteNumber
    {
        get => _quotenumber.EscapeSingleQuote();
        set => _quotenumber = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_wornewbusinessvolume")]
    public po_WORNewBusinessVolume po_WORNewBusinessVolume { get; set; }
    [XmlElement(ElementName = "po_wornbinstalledvolume")]
    public po_WORNBInstalledVolume po_WORNBInstalledVolume { get; set; }
    [XmlElement(ElementName = "po_orderedkeyedweeklyvolume")]
    public po_OrderedKeyedWeeklyVolume po_OrderedKeyedWeeklyVolume { get; set; }
    [XmlElement(ElementName = "po_markedassoldweeklyvolume")]
    public po_MarkedasSoldWeeklyVolume po_MarkedasSoldWeeklyVolume { get; set; }
    [XmlElement(ElementName = "po_businessdivison")]
    public Po_businessdivison po_BusinessDivison { get; set; }
    [XmlElement(ElementName = "po_13weekcalcfield")]
    public Po_13weekcalcfield po_13WeekCalcField { get; set; }
    [XmlElement(ElementName = "opportunityid")]
    public OpportunityId OpportunityId { get; set; }
    [XmlElement(ElementName = "pricelevelid")]
    public Pricelevelid PriceLevelId { get; set; }

    [Key]
    [XmlElement(ElementName = "quoteid")]
    public Guid QuoteId { get; set; }
    [XmlElement(ElementName = "transactioncurrencyid")]
    public TransactionCurrencyId TransactionCurrencyId { get; set; }
    [XmlElement(ElementName = "po_actualvolume")]
    public Po_actualvolume po_ActualVolume { get; set; }
    [XmlElement(ElementName = "po_estimatedweeklyvolume")]
    public Po_estimatedweeklyvolume po_EstimatedWeeklyVolume { get; set; }
    [XmlElement(ElementName = "createdby")]
    public QuoteCreatedby Createdby { get; set; }

    [XmlElement(ElementName = "customerid")]
    public CustomerId CustomerId { get; set; }

    [XmlElement(ElementName = "ownerid")]
    public OwnerId OwnerId { get; set; }
}

[XmlRoot(ElementName = "createdby")]
public class QuoteCreatedby : DynamicsEntityNullReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}
[XmlRoot(ElementName = "po_markedassoldweeklyvolume")]
public class po_MarkedasSoldWeeklyVolume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_orderedkeyedweeklyvolume")]
public class po_OrderedKeyedWeeklyVolume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_wornbinstalledvolume")]
public class po_WORNBInstalledVolume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_wornewbusinessvolume")]
public class po_WORNewBusinessVolume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_actualvolume")]
public class Po_actualvolume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedweeklyvolume")]
public class Po_estimatedweeklyvolume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "QuoteResultSet")]
public class QuoteResultSet
{
    [XmlElement(ElementName = "CustomQuote")]
    public List<CustomQuote> CustomQuote { get; set; }
}
public class JSONQuote
{
    public string odataetag { get; set; }
    public string name { get; set; }
    public string statuscode { get; set; }
    public string statecode { get; set; }
    public string quotenumber { get; set; }
    public string po_wornewbusinessvolume { get; set; }
    public string po_wornbinstalledvolume { get; set; }
    public string po_orderedkeyedweeklyvolume { get; set; }
    public string po_markedassoldweeklyvolume { get; set; }
    public string _po_businessdivisionid_value { get; set; }
    public string _po_businessdivisionid_po_businessdivision { get; set; }
    public string po_13weekcalcfield { get; set; }
    public string _opportunityid_value { get; set; }
    public string _opportunityid_name { get; set; }
    public string _priceLevelid_value { get; set; }
    public string _priceLevelid_name { get; set; }
    public string quoteId { get; set; }
    public string _transactioncurrencyid_value { get; set; }
    public string _transactioncurrencyid_currencyname { get; set; }
    public string po_actualvolume { get; set; }
    public string po_installedvolume { get; set; }
    public string po_estimatedweeklyvolume { get; set; }
    public string _createdby_value { get; set; }
    public string _createdby_fullname { get; set; }
    public string _customerid_value { get; set; }
    public string _customerid_name { get; set; }
    public string ownerid { get; set; }

}
