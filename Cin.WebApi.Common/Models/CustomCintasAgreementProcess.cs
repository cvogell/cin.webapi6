﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomCintasAgreementProcess")]
public class CustomCintasAgreementProcess
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _bpf_name;
    [XmlElement(ElementName = "bpf_name")]
    public string Bpf_name
    {
        get => _bpf_name.EscapeSingleQuote();
        set => _bpf_name = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "statuscode")]
    public StatusCode Statuscode { get; set; }
    [XmlElement(ElementName = "processid")]
    public ProcessId Processid { get; set; }

    [XmlElement(ElementName = "bpf_opportunityid")]
    public Bpf_opportunityId Bpf_opportunityid { get; set; }

    [XmlElement(ElementName = "activestageid")]
    public ActiveStageId Activestageid { get; set; }
    [XmlElement(ElementName = "businessprocessflowinstanceid")]

    [Key]
    public Guid Businessprocessflowinstanceid { get; set; }

    [XmlElement(ElementName = "activestagestartedon")]
    public ActiveStageStartedOn ActiveStageStartedOn { get; set; }
}

[XmlRoot(ElementName = "processid")]
public class ProcessId : DynamicsEntityNullReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }
    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "bpf_opportunityid")]
public class Bpf_opportunityId : DynamicsEntityNullReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "activestageid")]
public class ActiveStageId : DynamicsEntityNullReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "activestagestartedon")]
public class ActiveStageStartedOn : DynamicsDateTimeUTCValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "CintasAgreementProcessResultSet")]
public class CintasAgreementProcessResultSet
{
    [XmlElement(ElementName = "CustomCintasAgreementProcess")]
    public List<CustomCintasAgreementProcess> CustomCintasAgreementProcess { get; set; }
}
public class JSONCintasAgreementProcess
{
    public string bpf_name { get; set; }
    public string statuscode { get; set; }

    public string _processid_value { get; set; }
    public string _processid_name { get; set; }

    public string _bpf_opportunityid_value { get; set; }
    public string _bpf_opportunityid_name { get; set; }
    public string _activestageid_value { get; set; }
    public string _activestageid_name { get; set; }
    public string businessprocessflowinstanceid { get; set; }
    public string activestagestartedon { get; set; }
}