﻿using System.Xml.Serialization;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

//public class CustomBusinessDivisionBundleAssociation
//{
//}


//[XmlRoot(ElementName = "po_businessdivision")]
//public class Po_businessdivision
//{
//    [XmlElement(ElementName = "name")]
//    public string Name { get; set; }
//    [XmlElement(ElementName = "type")]
//    public string Type { get; set; }
//    [XmlText]
//    public string Text { get; set; }
//}

[XmlRoot(ElementName = "po_bundle")]
public class Po_bundle
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "Custompo_businessdivisionbundleassociation")]
public class Custompo_businessdivisionbundleassociation
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }
    [XmlElement(ElementName = "po_businessdivision")]
    public Ctas_ur_location_text Po_businessdivision { get; set; }
    [XmlElement(ElementName = "po_bundle")]
    public Po_bundle Po_bundle { get; set; }
    private string _po_businessdivisionbundleassociationid;

    [XmlElement(ElementName = "po_businessdivisionbundleassociationid")]
    public string Po_businessdivisionbundleassociationid
    {
        get => _po_businessdivisionbundleassociationid.EscapeSingleQuote();
        set => _po_businessdivisionbundleassociationid = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_businessdivisionbundleassociationResultSet")]
public class Po_businessdivisionbundleassociationResultSet
{
    [XmlElement(ElementName = "Custompo_businessdivisionbundleassociation")]
    public List<Custompo_businessdivisionbundleassociation> Custompo_businessdivisionbundleassociation { get; set; }
}