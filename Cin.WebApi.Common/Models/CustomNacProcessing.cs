﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "po_solddate_nac")]
public class Po_solddate_nac
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_poweradd_nac")]
public class Po_poweradd_nac
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedvolume_ur_nac")]
public class Po_estimatedvolume_ur_nac
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "CustomNacProcessing")]
public class CustomNacProcessing
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }
    [XmlElement(ElementName = "po_solddate_nac")]
    public Po_solddate_nac po_SoldDate_NAC { get; set; }

    private string _po_salesrep_nac;
    [XmlElement(ElementName = "po_salesrep_nac")]
    public string po_SalesRep_NAC
    {
        get => _po_salesrep_nac.EscapeSingleQuote();
        set => _po_salesrep_nac = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_poweradd_nac")]
    public Po_poweradd_nac po_PowerAdd_NAC { get; set; }
    [XmlElement(ElementName = "po_estimatedvolume_ur_nac")]
    public Po_estimatedvolume_ur_nac po_EstimatedVolume_UR_NAC { get; set; }

    [Key]
    [XmlElement(ElementName = "po_nacprocessingid")]
    public Guid po_nacprocessingId { get; set; }
    [XmlElement(ElementName = "transactioncurrencyid")]
    public TransactionCurrencyId TransactionCurrencyId { get; set; }
    [XmlElement(ElementName = "po_weeklytotal_nac")]
    public po_WeeklyTotal_NAC po_WeeklyTotal_NAC { get; set; }
    [XmlElement(ElementName = "po_worvolume_nac")]
    public po_WORVolume_NAC po_WORVolume_NAC { get; set; }
    [XmlElement(ElementName = "po_mark_as_sold")]
    public po_Mark_As_Sold po_Mark_As_Sold { get; set; }

    private string _po_route_nac;
    [XmlElement(ElementName = "po_route_nac")]
    public string po_Route_NAC
    {
        get => _po_route_nac.EscapeSingleQuote();
        set => _po_route_nac = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_installdate_nac")]
    public Po_installdate_nac po_InstallDate_NAC { get; set; }

    private string _po_notes_nac;
    [XmlElement(ElementName = "po_notes_nac")]
    public string po_Notes_NAC
    {
        get => _po_notes_nac.EscapeSingleQuote();
        set => _po_notes_nac = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_businessdivision")]
    public Po_BusinessDivision po_BusinessDivision { get; set; }
    [XmlElement(ElementName = "po_locationnumber")]
    public Po_locationnumber po_LocationNumber { get; set; }

    private string _po_name;
    [XmlElement(ElementName = "po_name")]
    public string po_name
    {
        get => _po_name.EscapeSingleQuote();
        set => _po_name = value.EscapeSingleQuote();
    }

    private string _po_zippostalcode_nac;
    [XmlElement(ElementName = "po_zippostalcode_nac")]
    public string po_ZipPostalCode_NAC
    {
        get => _po_zippostalcode_nac.EscapeSingleQuote();
        set => _po_zippostalcode_nac = value.EscapeSingleQuote();
    }

    private string _po_state_nac;
    [XmlElement(ElementName = "po_state_nac")]
    public string po_State_NAC
    {
        get => _po_state_nac.EscapeSingleQuote();
        set => _po_state_nac = value.EscapeSingleQuote();
    }

    private string _po_city_nac;
    [XmlElement(ElementName = "po_city_nac")]
    public string po_City_NAC
    {
        get => _po_city_nac.EscapeSingleQuote();
        set => _po_city_nac = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_soldvolume_nac")]
    public Po_soldvolume_nac po_SoldVolume_NAC { get; set; }

    private string _po_customernumber_nac;
    [XmlElement(ElementName = "po_customernumber_nac")]
    public string po_CustomerNumber_NAC
    {
        get => _po_customernumber_nac.EscapeSingleQuote();
        set => _po_customernumber_nac = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_week_nac")]
    public Po_week_nac po_Week_Nac { get; set; }
    [XmlElement(ElementName = "po_day_nac")]
    public Po_day_nac po_Day_NAC { get; set; }
    [XmlElement(ElementName = "createdby")]
    public Createdby Createdby { get; set; }
    [XmlElement(ElementName = "po_opportunity")]
    public Po_opportunity Po_opportunity { get; set; }

    [XmlElement(ElementName = "ownerid")]
    public OwnerId OwnerId { get; set; }
    [XmlElement(ElementName = "ctas_primary_rep")]
    public ctas_primary_rep ctas_primary_rep { get; set; }
}

[XmlRoot(ElementName = "ctas_primary_rep")]
public class ctas_primary_rep
{
    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_mark_as_sold")]
public class po_Mark_As_Sold
{
    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_worvolume_nac")]

public class po_WORVolume_NAC
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_weeklytotal_nac")]
public class po_WeeklyTotal_NAC
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_installdate_nac")]
public class Po_installdate_nac
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_businessdivision")]
public class Po_BusinessDivision
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_locationnumber")]
public class Po_locationnumber : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_soldvolume_nac")]
public class Po_soldvolume_nac
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_week_nac")]
public class Po_week_nac
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_day_nac")]
public class Po_day_nac
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "NacProcessingResultSet")]
public class NacProcessingResultSet
{
    [XmlElement(ElementName = "CustomNacProcessing")]
    public List<CustomNacProcessing> CustomNacProcessing { get; set; }
}
