﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "activitytypecode")]
public class ActivityTypeCode
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }

}

[XmlRoot(ElementName = "Customactivitypointer")]
public class CustomActivity
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    [Key]
    [XmlElement(ElementName = "activityid")]
    public Guid ActivityId { get; set; }

    private string _subject;
    [XmlElement(ElementName = "subject")]
    public string Subject
    {
        get => _subject.EscapeSingleQuote();
        set => _subject = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "activitytypecode")]
    public ProductTypeCode ActivityTypeCode { get; set; }

    [XmlElement(ElementName = "modifiedon")]
    public ActivityModifiedOn ModifiedOn { get; set; }

    [XmlElement(ElementName = "regardingobjectid")]
    public Regardingobjectid RegardingObjectId { get; set; }
}


[XmlRoot(ElementName = "activitypointerResultSet")]
public class ActivityResultSet
{
    [XmlElement(ElementName = "Customactivitypointer")]
    public List<CustomActivity> CustomActivity { get; set; }
}
//ChatakonduV E0143633 added below class
public class JSONActivity
{
    public string odataetag { get; set; }
    public Guid activityid { get; set; }
    public string activitytypecode { get; set; }
    public DateTime modifiedon { get; set; }
    public string _regardingobjectid_value { get; set; }
    public string _regardingobjectid_name { get; set; }
    public string subject { get; set; }
}

[XmlRoot(ElementName = "modifiedon")]
public class ActivityModifiedOn : DynamicsDateTimeGMTValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}