﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomOpportunityClose")]
public class CustomOpportunityClose
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    // Took this out to ensure consistency with the metadata
    /*
    public enum OpportunityCloseState
    {

        [EnumMember()]
        Open = 0,

        [EnumMember()]
        Completed = 1,

        [EnumMember()]
        Canceled = 2,
    }
    */

    [Key]
    public Guid Id
    {
        get;
        set;
    }

    [Key]
    public Guid ActivityId
    {
        get;
        set;

    }

    public string ActivityTypeCode
    {
        get;
        set;
    }


    public int? ActualDurationMinutes
    {
        get;
        set;
    }


    public DateTime? ActualEnd
    {
        get;
        set;
    }


    public decimal? ActualRevenue
    {
        get;
        set;
    }


    public decimal? ActualRevenue_Base
    {
        get;
        set;
    }


    public DateTime? ActualStart
    {
        get;
        set;
    }


    public string Category
    {
        get;
        set;
    }


    public Guid? CompetitorId
    {
        get;
        set;
    }


    public Guid? CreatedBy
    {
        get;
        set;
    }


    public Guid? CreatedByExternalParty
    {
        get;
        set;
    }


    public DateTime? CreatedOn
    {
        get;
        set;
    }


    public Guid? CreatedOnBehalfBy
    {
        get;
        set;
    }


    public string Description
    {
        get;
        set;
    }


    public decimal? ExchangeRate
    {
        get;
        set;
    }


    public int? ImportSequenceNumber
    {
        get;
        set;
    }


    public bool? IsBilled
    {
        get;
        set;
    }


    public bool? IsRegularActivity
    {
        get;
        set;
    }


    public bool? IsWorkflowCreated
    {
        get;
        set;
    }


    public Guid? ModifiedBy
    {
        get;
        set;
    }


    public Guid? ModifiedByExternalParty
    {
        get;
        set;
    }


    public DateTime? ModifiedOn
    {
        get;
        set;
    }


    public Guid? ModifiedOnBehalfBy
    {
        get;
        set;
    }


    public Guid? OpportunityId
    {
        get;
        set;
    }


    public DateTime? OverriddenCreatedOn
    {
        get;
        set;
    }


    public Guid? OwnerId
    {
        get;
        set;
    }


    public Guid? OwningBusinessUnit
    {
        get;
        set;
    }


    public Guid? OwningTeam
    {
        get;
        set;
    }


    public Guid? OwningUser
    {
        get;
        set;
    }


    public int? ScheduledDurationMinutes
    {
        get;
        set;
    }


    public DateTime? ScheduledEnd
    {
        get;
        set;
    }


    public DateTime? ScheduledStart
    {
        get;
        set;
    }


    public Guid? ServiceId
    {
        get;
        set;
    }

    // Changed it from Enum OpportunityCloseState to String to ensure Consistency
    public string StateCode
    {
        get;
        set;
    }


    public int StatusCode
    {
        get;
        set;
    }


    public string Subcategory
    {
        get;
        set;
    }


    public string Subject
    {
        get;
        set;
    }


    public int? TimeZoneRuleVersionNumber
    {
        get;
        set;
    }


    public Guid? TransactionCurrencyId
    {
        get;
        set;
    }


    public int? UTCConversionTimeZoneCode
    {
        get;
        set;
    }


    public long? VersionNumber
    {
        get;
        set;
    }
}

[XmlRoot(ElementName = "CustomWinOpportunity")]
public class CustomWinOpportunity
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    public Guid Id
    {
        get;
        set;
    }

    public Guid OpportunityId
    {
        get;
        set;
    }

    public int StatusCode
    {
        get;
        set;
    }
}

[XmlRoot(ElementName = "CustomLoseOpportunity")]
public class CustomLoseOpportunity
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    public Guid Id
    {
        get;
        set;
    }

    public Guid OpportunityId
    {
        get;
        set;
    }

    public int StatusCode
    {
        get;
        set;
    }
}