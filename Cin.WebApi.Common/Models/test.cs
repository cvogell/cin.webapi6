﻿using System.Runtime.Serialization;

namespace Cin.WebApi.Models;


[DataContract()]
[Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("po_businessdivision")]

public class BusinessDivision : Entity
{

    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdby")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference CreatedBy
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return this.GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdonbehalfby")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference CreatedOnBehalfBy
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// An unique numeric ID which can be used to identify a Business Division.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ctas_business_division_id")]
    [DataMember]
    public int? ctas_Business_Division_ID
    {
        get
        {
            return this.GetAttributeValue<int?>("ctas_business_division_id");
        }
        set
        {
            this.SetAttributeValue("ctas_business_division_id", value);
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return this.GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            this.SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedby")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference ModifiedBy
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return this.GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedonbehalfby")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return this.GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            this.SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Owner Id
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ownerid")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference OwnerId
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ownerid");
        }
        set
        {
            this.SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the business unit that owns the record
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningbusinessunit")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference OwningBusinessUnit
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier for the team that owns the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningteam")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference OwningTeam
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier for the user that owns the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owninguser")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference OwningUser
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_abbreviation")]
    [DataMember]
    public string po_Abbreviation
    {
        get
        {
            return this.GetAttributeValue<string>("po_abbreviation");
        }
        set
        {
            this.SetAttributeValue("po_abbreviation", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_agreementexpireindays")]
    [DataMember]
    public int? po_AgreementExpireinDays
    {
        get
        {
            return this.GetAttributeValue<int?>("po_agreementexpireindays");
        }
        set
        {
            this.SetAttributeValue("po_agreementexpireindays", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_backgroundcolor")]
    [DataMember]
    public Microsoft.Xrm.Sdk.OptionSetValue po_BackgroundColor
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("po_backgroundcolor");
        }
        set
        {
            this.SetAttributeValue("po_backgroundcolor", value);
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_businessdivision")]
    [DataMember]
    public string po_businessdivision1
    {
        get
        {
            return this.GetAttributeValue<string>("po_businessdivision");
        }
        set
        {
            this.SetAttributeValue("po_businessdivision", value);
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_businessdivisionid")]
    [DataMember]
    public Guid? po_businessdivisionId
    {
        get
        {
            return this.GetAttributeValue<Guid?>("po_businessdivisionid");
        }
        set
        {
            this.SetAttributeValue("po_businessdivisionid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_businessdivisionid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            po_businessdivisionId = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_parentbusinessdivision")]
    [DataMember]
    public Microsoft.Xrm.Sdk.EntityReference po_ParentBusinessDivision
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("po_parentbusinessdivision");
        }
        set
        {
            this.SetAttributeValue("po_parentbusinessdivision", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_proposalexpireindays")]
    [DataMember]
    public int? po_ProposalExpireinDays
    {
        get
        {
            return this.GetAttributeValue<int?>("po_proposalexpireindays");
        }
        set
        {
            this.SetAttributeValue("po_proposalexpireindays", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_proposallostindays")]
    [DataMember]
    public int? po_ProposalLostinDays
    {
        get
        {
            return this.GetAttributeValue<int?>("po_proposallostindays");
        }
        set
        {
            this.SetAttributeValue("po_proposallostindays", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_threadcolor")]
    [DataMember]
    public Microsoft.Xrm.Sdk.OptionSetValue po_ThreadColor
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("po_threadcolor");
        }
        set
        {
            this.SetAttributeValue("po_threadcolor", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("po_trimtype")]
    [DataMember]
    public Microsoft.Xrm.Sdk.OptionSetValue po_TrimType
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("po_trimtype");
        }
        set
        {
            this.SetAttributeValue("po_trimtype", value);
        }
    }

    /// <summary>
    /// Status of the Business Division
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statecode")]
    [DataMember]
    public CintasDynamics.po_businessdivisionState? statecode
    {
        get
        {
            Microsoft.Xrm.Sdk.OptionSetValue optionSet = this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (CintasDynamics.po_businessdivisionState)Enum.ToObject(typeof(CintasDynamics.po_businessdivisionState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                this.SetAttributeValue("statecode", null);
            }
            else
            {
                this.SetAttributeValue("statecode", new Microsoft.Xrm.Sdk.OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the Business Division
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statuscode")]
    [DataMember]
    public Microsoft.Xrm.Sdk.OptionSetValue statuscode
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statuscode");
        }
        set
        {
            this.SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return this.GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            this.SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return this.GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            this.SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return this.GetAttributeValue<long?>("versionnumber");
        }
    }


}




