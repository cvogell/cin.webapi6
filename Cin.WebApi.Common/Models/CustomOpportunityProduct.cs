﻿using Cin.WebApi.Common;
using Cin.WebApi.Extensions;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Cin.WebApi.Models;

public class JSONOpportunityProduct
{
    public string odataetag { get; set; }
    public string sequencenumber { get; set; }
    public string quantity { get; set; }
    public string producttypecode { get; set; }
    public string productdescription { get; set; }
    public string priceperunit { get; set; }
    public string extendedamount { get; set; }
    public string _transactioncurrencyid_value { get; set; }
    public string _transactioncurrencyid_currencyname { get; set; }
    public string opportunityproductid { get; set; }
    public string _productid_value { get; set; }
    public string _productid_name { get; set; }
    public string po_usagecd { get; set; }
    public string po_itemno { get; set; }
    public string po_itemdelfreq { get; set; }
    public string po_inventory { get; set; }
    public string _createdby_value { get; set; }
    public string _createdby_fullname { get; set; }
    public string ownerid { get; set; }
    public string _opportunityid_value { get; set; }
    public string _opportunityid_name { get; set; }

    public string isproductoverridden { get; set; }
    public string ispriceoverridden { get; set; }
    public string _uomid_value { get; set; }
    public string _uomid_name { get; set; }
}
[XmlRoot(ElementName = "CustomOpportunityProduct")]
public class CustomOpportunityProduct
{

    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    [XmlElement(ElementName = "sequencenumber")]
    public SequenceNumber SequenceNumber { get; set; }
    [XmlElement(ElementName = "quantity")]
    public Quantity Quantity { get; set; }
    [XmlElement(ElementName = "producttypecode")]
    public ProductTypeCode ProductTypeCode { get; set; }

    private string _productdescription;
    [XmlElement(ElementName = "productdescription")]
    public string ProductDescription
    {
        get => _productdescription.EscapeSingleQuote();
        set => _productdescription = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "priceperunit")]
    public PricePerUnit PricePerUnit { get; set; }
    [XmlElement(ElementName = "extendedamount")]
    public ExtendedAmount ExtendedAmount { get; set; }
    [XmlElement(ElementName = "transactioncurrencyid")]
    public TransactionCurrencyId TransactionCurrencyId { get; set; }

    //[XmlElement(ElementName = "opportunityid")]
    //public Guid OpportunityId { get; set; }

    [Key]
    [XmlElement(ElementName = "opportunityproductid")]
    public Guid OpportunityProductId { get; set; }
    [XmlElement(ElementName = "productid")]
    public ProductIdNR ProductId { get; set; }

    private string _po_usagecd;
    [XmlElement(ElementName = "po_usagecd")]
    public string po_UsageCD
    {
        get => _po_usagecd.EscapeSingleQuote();
        set => _po_usagecd = value.EscapeSingleQuote();
    }

    private string _po_itemno;
    [XmlElement(ElementName = "po_itemno")]
    public string po_ItemNo
    {
        get => _po_itemno.EscapeSingleQuote();
        set => _po_itemno = value.EscapeSingleQuote();
    }

    private string _po_itemdelfreq;
    [XmlElement(ElementName = "po_itemdelfreq")]
    public string po_ItemDelFreq
    {
        get => _po_itemdelfreq.EscapeSingleQuote();
        set => _po_itemdelfreq = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_inventory")]
    public Po_InventoryFV po_Inventory { get; set; }

    [XmlElement(ElementName = "createdby")]
    public QuoteCreatedby Createdby { get; set; }

    [XmlElement(ElementName = "ownerid")]
    public OwnerId OwnerId { get; set; }

    [XmlElement(ElementName = "opportunityid")]
    public OpportunityId OpportunityId { get; set; }

    [XmlElement(ElementName = "isproductoverridden")]
    public IsProductOverridden IsProductOverridden { get; set; }

    [XmlElement(ElementName = "ispriceoverridden")]
    public IsPriceOverridden IsPriceOverridden { get; set; }

    [XmlElement(ElementName = "uomid")]
    public UoMId UoMId { get; set; }
}

[XmlRoot(ElementName = "uomid")]
public class UoMId : DynamicsEntityReference
{
    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ispriceoverridden")]
public class IsPriceOverridden : DynamicsOptionSetRefBoolean
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "sequencenumber")]
public class SequenceNumber : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}



[XmlRoot(ElementName = "priceperunit")]
public class PricePerUnit : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "extendedamount")]
public class ExtendedAmount : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_inventory")]
public class Po_InventoryFV : DynamicsFormattedValueOnly
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "OpportunityProductResultSet")]
public class OpportunityProductResultSet
{
    [XmlElement(ElementName = "CustomOpportunityProduct")]
    public List<CustomOpportunityProduct> CustomOpportunityProduct { get; set; }
}