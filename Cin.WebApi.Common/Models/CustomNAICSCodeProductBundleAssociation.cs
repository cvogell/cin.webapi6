﻿using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;


[XmlRoot(ElementName = "po_productbundle")]
public class Po_productbundle : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_naicscode")]
public class Po_naicscode : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "Custompo_naicscodeproductbundleassociation")]
public class Custompo_naicscodeproductbundleassociation
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }
    [XmlElement(ElementName = "po_productbundle")]
    public Po_productbundle Po_productbundle { get; set; }
    [XmlElement(ElementName = "po_naicscode")]
    public Po_naicscode Po_naicscode { get; set; }

    private string _po_naicscodeproductbundleassociationid;
    [XmlElement(ElementName = "po_naicscodeproductbundleassociationid")]
    public string Po_naicscodeproductbundleassociationid
    {
        get => _po_naicscodeproductbundleassociationid.EscapeSingleQuote();
        set => _po_naicscodeproductbundleassociationid = value.EscapeSingleQuote();
    }
}


[XmlRoot(ElementName = "po_naicscodeproductbundleassociationResultSet")]
public class Po_naicscodeproductbundleassociationResultSet
{
    [XmlElement(ElementName = "Custompo_naicscodeproductbundleassociation")]
    public List<Custompo_naicscodeproductbundleassociation> Custompo_naicscodeproductbundleassociation { get; set; }
}

public class JSONNAICcodeproductbundleassociation
{
    public string odataetag { get; set; }
    public string _po_productbundle_value { get; set; }
    public string _po_productbundle_productnumber { get; set; }
    public string _po_naicscode_value { get; set; }
    public string _po_naicscode_po_naicscode { get; set; }
}