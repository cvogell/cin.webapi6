﻿using System.Xml.Serialization;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;


[XmlRoot(ElementName = "CustomAzureArchiveTable")]
public class CustomAzureArchiveTable
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    //Fields for Archiving a file

    private string _fileName;
    [XmlElement(ElementName = "FileName")]
    public string FileName
    {
        get => _fileName.EscapeSingleQuote();
        set => _fileName = value.EscapeSingleQuote();
    }

    private string _documentType;
    [XmlElement(ElementName = "DocumentType")]
    public string DocumentType
    {
        get => _documentType.EscapeSingleQuote();
        set => _documentType = value.EscapeSingleQuote();
    }

    private string _opportunityID;
    [XmlElement(ElementName = "OpportunityID")]
    public string OpportunityID
    {
        get => _opportunityID.EscapeSingleQuote();
        set => _opportunityID = value.EscapeSingleQuote();
    }

    //Fields for archiving an Opportunity

    private string _accountLocation;
    [XmlElement(ElementName = "AccountLocation")]
    public string AccountLocation
    {
        get => _accountLocation.EscapeSingleQuote();
        set => _accountLocation = value.EscapeSingleQuote();
    }

    private string _author;
    [XmlElement(ElementName = "Author")]
    public string Author
    {
        get => _author.EscapeSingleQuote();
        set => _author = value.EscapeSingleQuote();
    }

    private string _contractNumber;
    [XmlElement(ElementName = "ContractNumber")]
    public string ContractNumber
    {
        get => _contractNumber.EscapeSingleQuote();
        set => _contractNumber = value.EscapeSingleQuote();
    }

    private string _revision;
    [XmlElement(ElementName = "Revision")]
    public string Revision
    {
        get => _revision.EscapeSingleQuote();
        set => _revision = value.EscapeSingleQuote();
    }

    private string _title;
    [XmlElement(ElementName = "Title")]
    public string Title
    {
        get => _title.EscapeSingleQuote();
        set => _title = value.EscapeSingleQuote();
    }

    //Fields for archiving a log

    private string _environment;
    [XmlElement(ElementName = "Environment")]
    public string Environment
    {
        get => _environment.EscapeSingleQuote();
        set => _environment = value.EscapeSingleQuote();
    }

    private string _message;
    [XmlElement(ElementName = "Message")]
    public string Message
    {
        get => _message.EscapeSingleQuote();
        set => _message = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlElement(ElementName = "Type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _username;
    [XmlElement(ElementName = "Username")]
    public string Username
    {
        get => _username.EscapeSingleQuote();
        set => _username = value.EscapeSingleQuote();
    }

    private string _fullname;
    [XmlElement(ElementName = "Fullname")]
    public string Fullname
    {
        get => _fullname.EscapeSingleQuote();
        set => _fullname = value.EscapeSingleQuote();
    }

    //Fields required by all types for archiving

    private string _tableName;
    [XmlElement(ElementName = "TableName")]
    public string TableName
    {
        get => _tableName.EscapeSingleQuote();
        set => _tableName = value.EscapeSingleQuote();
    }

    private string _tableType;
    [XmlElement(ElementName = "TableType")]

    public string TableType
    {
        get => _tableType.EscapeSingleQuote();
        set => _tableType = value.EscapeSingleQuote();
    }
}