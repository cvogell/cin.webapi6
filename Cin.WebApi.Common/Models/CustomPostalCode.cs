﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "po_city")]
public class Po_city : DynamicsFormattedValue
{
    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlElement(ElementName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_postalcode")]
public class Po_postalcode : DynamicsFormattedValue
{
    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlElement(ElementName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_state")]
public class Po_state : DynamicsFormattedValue
{
    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlElement(ElementName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_ur_location_text")]
public class Ctas_ur_location_text : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_fs_location_text")]
public class Ctas_fs_location_text : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_mdr_urlocation_text")]
public class Ctas_mdr_urlocation_text : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_mdr_fslocation_text")]
public class Ctas_mdr_fslocation_text : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_deep_clean_location_text")]
public class Ctas_deep_clean_location_text : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "CustomPostalCodeAlignment")]
public class CustomPostalCode
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_name;
    [XmlElement(ElementName = "po_name")]
    public string Po_name
    {
        get => _po_name.EscapeSingleQuote();
        set => _po_name = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_city")]
    public Po_city Po_city { get; set; }
    [XmlElement(ElementName = "po_postalcode")]
    public Po_postalcode Po_postalcode { get; set; }
    [XmlElement(ElementName = "po_state")]
    public Po_state Po_state { get; set; }

    //[XmlElement(ElementName = "ctas_ur_location_text")]
    //public Ctas_ur_location_text Ctas_ur_location_text { get; set; }

    //[XmlElement(ElementName = "ctas_fs_location_text")]
    //public Ctas_fs_location_text Ctas_fs_location_text { get; set; }

    //[XmlElement(ElementName = "ctas_mdr_urlocation_text")]
    //public Ctas_mdr_urlocation_text Ctas_mdr_urlocation_text { get; set; }

    //[XmlElement(ElementName = "ctas_mdr_fslocation_text")]
    //public Ctas_mdr_fslocation_text Ctas_mdr_fslocation_text { get; set; }

    //[XmlElement(ElementName = "ctas_deep_clean_location_text")]
    //public Ctas_deep_clean_location_text Ctas_deep_clean_location_text { get; set; }


    [Key]
    [XmlElement(ElementName = "po_postalcodeid")]
    public Guid Po_PostalCodeId { get; set; }

}

[XmlRoot(ElementName = "PostalCodeAlignmentResultSet")]
public class PostalCodeResultSet
{
    [XmlElement(ElementName = "CustomPostalCodeAlignment")]
    public List<CustomPostalCode> CustomPostalCode { get; set; }
}

public class JSONPostalCode
{
    public string odataetag { get; set; }
    public string po_name { get; set; }
    public string po_city { get; set; }
    public string po_postalcode { get; set; }
    public string po_state { get; set; }
    public string po_postalcodeid { get; set; }
}