﻿using Cin.WebApi.Common;
using Cin.WebApi.Extensions;
using System.Xml.Serialization;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "quantity")]
public class Quantity : DynamicsFormattedValueOnly
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();    
        set => _text = value.EscapeSingleQuote();
    }
}