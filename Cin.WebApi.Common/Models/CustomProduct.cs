﻿using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;
using Cin.WebApi.Extensions;
using Cin.WebApi.Common;

namespace Cin.WebApi.Models;


[XmlRoot(ElementName = "productstructure")]
public class Productstructure : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "CustomProduct")]
public class CustomProduct
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    [Key]
    [XmlElement(ElementName = "productid")]
    public Guid ProductId { get; set; }

    private string _productnumber;
    [XmlElement(ElementName = "productnumber")]
    public string ProductNumber
    {
        get => _productnumber.EscapeSingleQuote();
        set => _productnumber = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "producttypecode")]
    public ProductTypeCode ProductTypeCode { get; set; }
    [XmlElement(ElementName = "productstructure")]
    public Productstructure ProductStructure { get; set; }
    [XmlElement(ElementName = "parentproductid")]
    public Parentproductid ParentProductId { get; set; }

    private string _description;
    [XmlElement(ElementName = "description")]
    public string Description
    {
        get => _description.EscapeSingleQuote();
        set => _description = value.EscapeSingleQuote();
    }

}

[XmlRoot(ElementName = "parentproductid")]
public class Parentproductid : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ProductResultSet")]
public class ProductResultSet
{
    [XmlElement(ElementName = "CustomProduct")]
    public List<CustomProduct> CustomProduct { get; set; }
}

public class JSONProduct
{
    public string odataetag { get; set; }
    public string name { get; set; }
    public string productid { get; set; }
    public string productnumber { get; set; }
    public int? producttypecode { get; set; }
    public int? productstructure { get; set; }
    public string _parentproductid_value { get; set; }
    public string _parentproductid_name { get; set; }
    public string description { get; set; }
}