﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "parentcustomerid")]
public class Parentcustomerid : DynamicsEntityNullReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }


    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "CustomContact")]
public class CustomContact
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _fullname;
    [XmlElement(ElementName = "fullname")]
    public string FullName
    {
        get => _fullname.EscapeSingleQuote();
        set => _fullname = value.EscapeSingleQuote();
    }

    private string _salutation;
    [XmlElement(ElementName = "salutation")]
    public string Salutation
    {
        get => _salutation.EscapeSingleQuote();
        set => _salutation = value.EscapeSingleQuote();
    }

    private string _telephone1;
    [XmlElement(ElementName = "telephone1")]
    public string Telephone1
    {
        get => _telephone1.EscapeSingleQuote();
        set => _telephone1 = value.EscapeSingleQuote();
    }

    private string _jobtitle;
    [XmlElement(ElementName = "jobtitle")]
    public string JobTitle
    {
        get => _jobtitle.EscapeSingleQuote();
        set => _jobtitle = value.EscapeSingleQuote();
    }

    private string _emailaddress1;
    [XmlElement(ElementName = "emailaddress1")]
    public string EmailAddress1
    {
        get => _emailaddress1.EscapeSingleQuote();
        set => _emailaddress1 = value.EscapeSingleQuote();
    }

    private string _address1_stateorprovince;
    [XmlElement(ElementName = "address1_stateorprovince")]
    public string Address1_StateOrProvince
    {
        get => _address1_stateorprovince.EscapeSingleQuote();
        set => _address1_stateorprovince = value.EscapeSingleQuote();
    }

    private string _address1_postalcode;
    [XmlElement(ElementName = "address1_postalcode")]
    public string Address1_PostalCode
    {
        get => _address1_postalcode.EscapeSingleQuote();
        set => _address1_postalcode = value.EscapeSingleQuote();
    }

    private string _address1_line3;
    [XmlElement(ElementName = "address1_line3")]
    public string Address1_Line3
    {
        get => _address1_line3.EscapeSingleQuote();
        set => _address1_line3 = value.EscapeSingleQuote();
    }

    private string _address1_line2;
    [XmlElement(ElementName = "address1_line2")]
    public string Address1_Line2
    {
        get => _address1_line2.EscapeSingleQuote();
        set => _address1_line2 = value.EscapeSingleQuote();
    }

    private string _address1_line1;
    [XmlElement(ElementName = "address1_line1")]
    public string Address1_Line1
    {
        get => _address1_line1.EscapeSingleQuote();
        set => _address1_line1 = value.EscapeSingleQuote();
    }

    private string _address1_country;
    [XmlElement(ElementName = "address1_country")]
    public string Address1_Country
    {
        get => _address1_country.EscapeSingleQuote();
        set => _address1_country = value.EscapeSingleQuote();
    }

    private string _address1_city;
    [XmlElement(ElementName = "address1_city")]
    public string Address1_City
    {
        get => _address1_city.EscapeSingleQuote();
        set => _address1_city = value.EscapeSingleQuote();
    }

    private string _lastname;
    [XmlElement(ElementName = "lastname")]
    public string LastName
    {
        get => _lastname.EscapeSingleQuote();
        set => _lastname = value.EscapeSingleQuote();
    }

    private string _firstname;
    [XmlElement(ElementName = "firstname")]
    public string FirstName
    {
        get => _firstname.EscapeSingleQuote();
        set => _firstname = value.EscapeSingleQuote();
    }


    [Key]
    [XmlElement(ElementName = "contactid")]
    public Guid ContactId { get; set; }

    [XmlElement(ElementName = "createdby")]
    public Createdby CreatedBy { get; set; }

    [XmlElement(ElementName = "parentcustomerid")]
    public Parentcustomerid ParentCustomerId { get; set; }

    [XmlElement(ElementName = "ownerid")]
    public OwnerId Ownerid { get; set; }
}

[XmlRoot(ElementName = "ContactResultSet")]
public class ContactResultSet
{
    [XmlElement(ElementName = "CustomContact")]
    public List<CustomContact> CustomContact { get; set; }
}

public class JSONContact
{
    public string odataetag { get; set; }
    public string contactid { get; set; }
    public string _parentcustomerid_value { get; set; }
    public string _parentcustomerid_name { get; set; }
    public string fullname { get; set; }
    public string salutation { get; set; }
    public string firstname { get; set; }
    public string lastname { get; set; }
    public string jobtitle { get; set; }
    public string address1_line1 { get; set; }
    public string address1_line2 { get; set; }
    public string address1_city { get; set; }
    public string address1_stateorprovince { get; set; }
    public string address1_postalcode { get; set; }
    public string address1_country { get; set; }
    public string telephone1 { get; set; }
    public string emailaddress1 { get; set; }
}