﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "po_competitor_ur")]
public class Po_Competitor_UR : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    
    

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_competitor_fs_deepclean")]
public class Po_Competitor_FS_DeepClean : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    
    

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_competitor_fs")]
public class Po_Competitor_FS : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_competitor_ur_frc")]
public class Po_Competitor_UR_FRC : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_competitor_ur_mdr")]
public class Po_Competitor_UR_MDR : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    
    

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_competitor_fs_mdr")]
public class Po_Competitor_FS_MDR : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_naicscodeid")]
public class Po_naicscodeid : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_postalcodeid")]
public class Po_postalcodeid : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_custofur")]
public class Po_custofur : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_custoffs")]
public class Po_custoffs : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_custoffire")]
public class Po_custoffire : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_custoffas")]
public class Po_custoffas : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_custofchicagods")]
public class Po_custofchicagods : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_custofcatalog")]
public class Po_custofcatalog : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedclosedate_ur_mdr")]
public class Po_estimatedclosedate_ur_mdr : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedclosedate_ur_frc")]
public class Po_estimatedclosedate_ur_frc : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedclosedate_fs_mdr")]
public class Po_estimatedclosedate_fs_mdr : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedclosedate_fs_deepclean")]
public class Po_estimatedclosedate_fs_deepclean : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedclosedate_ur")]
public class Po_estimatedclosedate_ur : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedclosedate_fs")]
public class Po_estimatedclosedate_fs : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedvolume_acct_fs")]
public class po_EstimatedVolume_Acct_FS : DynamicsCurrencyValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedvolume_ur_mdr")]
public class po_EstimatedVolume_UR_MDR : DynamicsCurrencyValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedvolume_ur_frc")]
public class po_EstimatedVolume_UR_FRC : DynamicsCurrencyValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedvolume_acct_ur")]
public class po_EstimatedVolume_Acct_UR : DynamicsCurrencyValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedvolume_fs_mdr")]
public class po_EstimatedVolume_FS_MDR : DynamicsCurrencyValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedvolume_fs_deepclean")]
public class po_EstimatedVolume_FS_DeepClean : DynamicsCurrencyValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "AccountResultSet")]
public class AccountResultSet
{
    [XmlElement(ElementName = "CustomAccount")]
    public List<CustomAccount> CustomAccount { get; set; }
}

[XmlRoot(ElementName = "AccountDeltaResultSet")]
public class AccountDeltaResultSet
{
    [XmlElement(ElementName = "CustomAccountDelta")]
    public List<object> CustomAccountDelta { get; set; }

    [XmlElement(ElementName = "__delta")]
    public string __delta { get; set; }
}

[XmlRoot(ElementName = "CustomAccountDelta")]
public class CustomAccountDelta
{
    [XmlElement(ElementName = "@odata.context")]
    public string oData_Context { get; set; }

    [XmlElement(ElementName = "id")]
    public string id { get; set; }
}

[XmlRoot(ElementName = "CustomAccount")]
public class CustomAccount
{
    [XmlElement(ElementName = "__metadata")]
    public DeltaMetadataTag __metadata { get; set; }
    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _telephone1;
    [XmlElement(ElementName = "telephone1")]
    public string Telephone1
    {
        get => _telephone1.EscapeSingleQuote();
        set => _telephone1 = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_naicscodeid")]
    public Po_naicscodeid po_NAICSCodeid { get; set; }

    [XmlElement(ElementName = "po_postalcodeid")]
    public Po_postalcodeid po_PostalCodeid { get; set; }

    private string _po_abinumber;
    [XmlElement(ElementName = "po_abinumber")]
    public string po_ABINumber
    {
        get => _po_abinumber.EscapeSingleQuote();
        set => _po_abinumber = value.EscapeSingleQuote();
    }

    private string _address1_postalcode;
    [XmlElement(ElementName = "address1_postalcode")]
    public string Address1_PostalCode
    {
        get => _address1_postalcode.EscapeSingleQuote();
        set => _address1_postalcode = value.EscapeSingleQuote();
    }

    private string _address1_longitude;
    [XmlElement(ElementName = "address1_longitude")]
    public string Address1_Longitude
    {
        get => _address1_longitude.EscapeSingleQuote();
        set => _address1_longitude = value.EscapeSingleQuote();
    }

    private string _address1_line3;
    [XmlElement(ElementName = "address1_line3")]
    public string Address1_Line3
    {
        get => _address1_line3.EscapeSingleQuote();
        set => _address1_line3 = value.EscapeSingleQuote();
    }

    private string _address1_line1;
    [XmlElement(ElementName = "address1_line1")]
    public string Address1_Line1
    {
        get => _address1_line1.EscapeSingleQuote();
        set => _address1_line1 = value.EscapeSingleQuote();
    }

    private string _address1_latitude;
    [XmlElement(ElementName = "address1_latitude")]
    public string Address1_Latitude
    {
        get => _address1_latitude.EscapeSingleQuote();
        set => _address1_latitude = value.EscapeSingleQuote();
    }

    private string _address1_county;
    [XmlElement(ElementName = "address1_county")]
    public string Address1_County
    {
        get => _address1_county.EscapeSingleQuote();
        set => _address1_county = value.EscapeSingleQuote();
    }

    private string _address1_country;
    [XmlElement(ElementName = "address1_country")]
    public string Address1_Country
    {
        get => _address1_country.EscapeSingleQuote();
        set => _address1_country = value.EscapeSingleQuote();
    }

    private string _address1_city;
    [XmlElement(ElementName = "address1_city")]
    public string Address1_City
    {
        get => _address1_city.EscapeSingleQuote();
        set => _address1_city = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_estimatedvolume_ur_mdr")]
    public po_EstimatedVolume_UR_MDR po_EstimatedVolume_UR_MDR { get; set; }
    [XmlElement(ElementName = "po_estimatedvolume_ur_frc")]
    public po_EstimatedVolume_UR_FRC po_EstimatedVolume_UR_FRC { get; set; }
    [XmlElement(ElementName = "po_estimatedvolume_fs_mdr")]
    public po_EstimatedVolume_FS_MDR po_EstimatedVolume_FS_MDR { get; set; }
    [XmlElement(ElementName = "po_estimatedvolume_fs_deepclean")]
    public po_EstimatedVolume_FS_DeepClean po_EstimatedVolume_FS_DeepClean { get; set; }
    [XmlElement(ElementName = "po_estimatedvolume_acct_ur")]
    public po_EstimatedVolume_Acct_UR po_EstimatedVolume_Acct_UR { get; set; }
    [XmlElement(ElementName = "po_estimatedvolume_acct_fs")]
    public po_EstimatedVolume_Acct_FS po_EstimatedVolume_Acct_FS { get; set; }
    [XmlElement(ElementName = "po_estimatedclosedate_ur_mdr")]
    public Po_estimatedclosedate_ur_mdr Po_estimatedclosedate_ur_mdr { get; set; }
    [XmlElement(ElementName = "po_estimatedclosedate_ur_frc")]
    public Po_estimatedclosedate_ur_frc Po_estimatedclosedate_ur_frc { get; set; }
    [XmlElement(ElementName = "po_estimatedclosedate_fs_mdr")]
    public Po_estimatedclosedate_fs_mdr Po_estimatedclosedate_fs_mdr { get; set; }
    [XmlElement(ElementName = "po_estimatedclosedate_fs_deepclean")]
    public Po_estimatedclosedate_fs_deepclean Po_estimatedclosedate_fs_deepclean { get; set; }
    [XmlElement(ElementName = "po_estimatedclosedate_ur")]
    public Po_estimatedclosedate_ur Po_estimatedclosedate_ur { get; set; }
    [XmlElement(ElementName = "po_estimatedclosedate_fs")]
    public Po_estimatedclosedate_fs Po_estimatedclosedate_fs { get; set; }

    private string _address1_line2;
    [XmlElement(ElementName = "address1_line2")]
    public string Address1_Line2
    {
        get => _address1_line2.EscapeSingleQuote();
        set => _address1_line2 = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_custofur")]
    public Po_custofur po_CustofUR { get; set; }
    [XmlElement(ElementName = "po_custoffs")]
    public Po_custoffs po_CustofFS { get; set; }
    [XmlElement(ElementName = "po_custoffire")]
    public Po_custoffire po_CustofFire { get; set; }
    [XmlElement(ElementName = "po_custoffas")]
    public Po_custoffas po_CustofFAS { get; set; }
    [XmlElement(ElementName = "po_custofchicagods")]
    public Po_custofchicagods po_CustofChicagoDS { get; set; }
    [XmlElement(ElementName = "po_custofcatalog")]
    public Po_custofcatalog po_CustofCatalog { get; set; }

    [Key]
    [XmlElement(ElementName = "accountid")]
    public Guid AccountId { get; set; }

    [XmlElement(ElementName = "createdby")]
    public Createdby Createdby { get; set; }

    private string _address1_stateorprovince;
    [XmlElement(ElementName = "address1_stateorprovince")]
    public string Address1_stateorprovince
    {
        get => _address1_stateorprovince.EscapeSingleQuote();
        set => _address1_stateorprovince = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_competitor_ur")]
    public Po_Competitor_UR Po_Competitor_UR { get; set; }

    [XmlElement(ElementName = "po_competitor_fs")]
    public Po_Competitor_FS Po_Competitor_FS { get; set; }

    [XmlElement(ElementName = "po_competitor_ur_frc")]
    public Po_Competitor_UR_FRC Po_Competitor_UR_FRC { get; set; }

    [XmlElement(ElementName = "po_competitor_ur_mdr")]
    public Po_Competitor_UR_MDR Po_Competitor_UR_MDR { get; set; }

    [XmlElement(ElementName = "po_competitor_fs_deepclean")]
    public Po_Competitor_FS_DeepClean Po_Competitor_FS_DeepClean { get; set; }

    [XmlElement(ElementName = "po_competitor_fs_mdr")]
    public Po_Competitor_FS_MDR Po_Competitor_FS_MDR { get; set; }

    [XmlElement(ElementName = "ownerid")]
    public OwnerId Ownerid { get; set; }

}


public class JSONAccount
{
    public string odataetag { get; set; }
    public string AccountId { get; set; }
    public string Name { get; set; }
    public string Telephone1 { get; set; }
    public string _po_NAICSCodeid_value { get; set; }
    public string _po_NAICSCodeid_po_NAICSCode { get; set; }
    public string _po_PostalCodeid_value { get; set; }
    public string _po_PostalCodeid_po_postalcode { get; set; }
    public string po_ABINumber { get; set; }
    public string po_EstimatedVolume_Acct_UR { get; set; }
    public string po_EstimatedVolume_UR_MDR { get; set; }
    public string po_EstimatedVolume_UR_FRC { get; set; }
    public string po_EstimatedVolume_Acct_FS { get; set; }
    public string po_EstimatedVolume_FS_MDR { get; set; }
    public string po_EstimatedVolume_FS_DeepClean { get; set; }
    public string Po_estimatedclosedate_ur { get; set; }
    public string Po_estimatedclosedate_ur_mdr { get; set; }
    public string Po_estimatedclosedate_ur_frc { get; set; }
    public string Po_estimatedclosedate_fs { get; set; }
    public string Po_estimatedclosedate_fs_mdr { get; set; }
    public string Po_estimatedclosedate_fs_deepclean { get; set; }
    public string Address1_Line1 { get; set; }
    public string Address1_Line2 { get; set; }
    public string Address1_Line3 { get; set; }
    public string Address1_Latitude2 { get; set; }
    public string Address1_Longitude { get; set; }
    public string Address1_PostalCode { get; set; }
    public string Address1_City { get; set; }
    public string Address1_County { get; set; }
    public string Address1_stateorprovince { get; set; }
    public string Address1_Country { get; set; }
    public string po_CustofUR { get; set; }
    public string po_CustofFS { get; set; }
    public string po_CustofFire { get; set; }
    public string po_CustofFAS { get; set; }
    public string po_CustofChicagoDS { get; set; }
    public string po_CustofCatalog { get; set; }
    public string _Po_Competitor_UR_value { get; set; }
    public string _Po_Competitor_FS_value { get; set; }
    public string _Po_Competitor_UR_FRC_value { get; set; }
    public string _Po_Competitor_UR_MDR_value { get; set; }
    public string _Po_Competitor_FS_DeepClean_value { get; set; }
    public string _Po_Competitor_FS_MDR_value { get; set; }
    public string _Po_Competitor_UR_po_name { get; set; }
    public string _Po_Competitor_FS_po_name { get; set; }
    public string _Po_Competitor_UR_FRC_po_name { get; set; }
    public string _Po_Competitor_UR_MDR_po_name { get; set; }
    public string _Po_Competitor_FS_DeepClean_po_name { get; set; }
    public string _Po_Competitor_FS_MDR_po_name { get; set; }
}