﻿using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;
using Cin.WebApi.Extensions;
using Cin.WebApi.Common;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "productisrequired")]
public class Productisrequired : DynamicsOptionSetRefNoName
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "propertycustomizationstatus")]
public class Propertycustomizationstatus : DynamicsOptionSetRefNoName
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}



[XmlRoot(ElementName = "associatedproduct")]
public class Associatedproduct : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "CustomProductAssociation")]
public class CustomProductAssociation
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }
    [XmlElement(ElementName = "associatedproduct")]
    public Associatedproduct AssociatedProduct { get; set; }
    [Key]
    [XmlElement(ElementName = "productassociationid")]
    public Guid ProductAssociationId { get; set; }
    [XmlElement(ElementName = "uomid")]
    public UomIdNR UoMId { get; set; }
    [XmlElement(ElementName = "productisrequired")]
    public Productisrequired ProductIsRequired { get; set; }
    [XmlElement(ElementName = "propertycustomizationstatus")]
    public Propertycustomizationstatus PropertyCustomizationStatus { get; set; }
    [XmlElement(ElementName = "productid")]
    public ProductIdNR ProductId { get; set; }


}

[XmlRoot(ElementName = "ProductAssociationResultSet")]
public class ProductAssociationResultSet
{
    [XmlElement(ElementName = "CustomProductAssociation")]
    public List<CustomProductAssociation> CustomProductAssociation { get; set; }
}

public class JSONProductAssociation
{
    public string odataetag { get; set; }
    public string _associatedproduct_value { get; set; }
    public string _associatedproduct_name { get; set; }
    public string productassociationid { get; set; }
    public string _uomid_value { get; set; }
    public string _uomid_name { get; set; }
    public int? productisrequired { get; set; }
    public int? propertycustomizationstatus { get; set; }
    public string _productid_value { get; set; }
    public string _productid_name { get; set; }
}