﻿using System.ComponentModel.DataAnnotations;

namespace Cin.WebApi.Models;

public class QueryRequest
{
    [Key]
    public string QueryId { get; set; }

    public string UserId { get; set; }
}

public class ProposalsAndAgreementsRequest
{
    [Key]
    public string ctas_ProposalsAndAgreementsId { get; set; }
}