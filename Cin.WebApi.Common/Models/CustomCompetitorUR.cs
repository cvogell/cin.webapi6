﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;



[XmlRoot(ElementName = "CustomCompetitorUR")]
public class CustomCompetitorUR
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_name;
    [XmlElement(ElementName = "po_name")]
    public string Po_name
    {
        get => _po_name.EscapeSingleQuote();
        set => _po_name = value.EscapeSingleQuote();
    }

    private string _po_notes;
    [XmlElement(ElementName = "po_notes")]
    public string Po_notes
    {
        get => _po_notes.EscapeSingleQuote();
        set => _po_notes = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_cintergy_competitor_id")]
    public Po_cintergy_competitor_id Po_cintergy_competitor_id { get; set; }

    private string _po_cintergybusinessdivision;
    [XmlElement(ElementName = "po_cintergybusinessdivision")]
    public string Po_cintergybusinessdivision
    {
        get => _po_cintergybusinessdivision.EscapeSingleQuote();
        set => _po_cintergybusinessdivision = value.EscapeSingleQuote();
    }

    [Key]
    [XmlElement(ElementName = "po_competitorurid")]
    public Guid Po_competitorurid { get; set; }
}

[XmlRoot(ElementName = "CompetitorURResultSet")]
public class CompetitorURResultSet
{
    [XmlElement(ElementName = "CustomCompetitorUR")]
    public List<CustomCompetitorUR> CustomCompetitorUR { get; set; }
}

public class JSONCompetitorUR
{
    public string odataetag { get; set; }
    public string po_name { get; set; }
    public string po_notes { get; set; }
    public int? po_cintergy_competitor_id { get; set; }
    public string po_cintergybusinessdivision { get; set; }
    public string po_competitorurid { get; set; }
}