﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomBusinessDivision")]
public class CustomBusinessDivision
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_businessdivision;
    [XmlElement(ElementName = "po_businessdivision")]
    public string Po_businessdivision
    {
        get => _po_businessdivision.EscapeSingleQuote();
        set => _po_businessdivision = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_parentbusinessdivision")]
    public Po_ParentBusinessDivision Po_parentbusinessdivision { get; set; }

    private string _po_abbrevation;
    [XmlElement(ElementName = "po_abbrevation")]
    public string po_abbrevation
    {
        get => _po_abbrevation.EscapeSingleQuote();
        set => _po_abbrevation = value.EscapeSingleQuote();
    }

    private string _po_emblemadvantagebookprice;
    [XmlElement(ElementName = "po_emblemadvantagebookprice")]
    public string po_emblemadvantagebookprice
    {
        get => _po_emblemadvantagebookprice.EscapeSingleQuote();
        set => _po_emblemadvantagebookprice = value.EscapeSingleQuote();
    }

    private string _po_makeupadvantagebookprice;
    [XmlElement(ElementName = "po_makeupadvantagebookprice")]
    public string po_makeupadvantagebookprice
    {
        get => _po_makeupadvantagebookprice.EscapeSingleQuote();
        set => _po_makeupadvantagebookprice = value.EscapeSingleQuote();
    }

    private string _po_premiumuniformadvantagebookprice;
    [XmlElement(ElementName = "po_premiumuniformadvantagebookprice")]
    public string po_premiumuniformadvantagebookprice
    {
        get => _po_premiumuniformadvantagebookprice.EscapeSingleQuote();
        set => _po_premiumuniformadvantagebookprice = value.EscapeSingleQuote();
    }

    private string _po_servicechargebookprice;
    [XmlElement(ElementName = "po_servicechargebookprice")]
    public string po_servicechargebookprice
    {
        get => _po_servicechargebookprice.EscapeSingleQuote();
        set => _po_servicechargebookprice = value.EscapeSingleQuote();
    }

    private string _po_uniformadvantagebookprice;
    [XmlElement(ElementName = "po_uniformadvantagebookprice")]
    public string po_uniformadvantagebookprice
    {
        get => _po_uniformadvantagebookprice.EscapeSingleQuote();
        set => _po_uniformadvantagebookprice = value.EscapeSingleQuote();
    }

    [Key]
    [XmlElement(ElementName = "po_businessdivisionid")]
    public Guid Po_businessdivisionid { get; set; }
}

[XmlRoot(ElementName = "po_parentbusinessdivision")]
public class Po_ParentBusinessDivision : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "BusinessDivisionResultSet")]
public class BusinessDivisionResultSet
{
    [XmlElement(ElementName = "CustomBusinessDivision")]
    public List<CustomBusinessDivision> CustomBusinessDivision { get; set; }
}

public class JSONBusinessDivision
{
    public string odataetag { get; set; }
    public string po_businessdivision { get; set; }
    public string _po_parentbusinessdivision_po_businessdivision { get; set; }
    public string _po_parentbusinessdivision_value { get; set; }
    public string po_abbrevation { get; set; }
    public string po_emblemadvantagebookprice { get; set; }
    public string po_makeupadvantagebookprice { get; set; }
    public string po_premiumuniformadvantagebookprice { get; set; }
    public string po_servicechargebookprice { get; set; }
    public string po_uniformadvantagebookprice { get; set; }
    public string po_businessdivisionid { get; set; }
}