﻿namespace Cin.WebApi.Models;

public class CommonResponse
{
    public Guid Id { get; set; }

    public string ResponseName { get; set; }

    public string EntityName { get; set; }

    public string Message { get; set; }

    public object results { get; set; }
}