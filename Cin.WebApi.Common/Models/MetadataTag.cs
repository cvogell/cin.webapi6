﻿using System.Xml.Serialization;

namespace Cin.WebApi.Models;

public class MetadataTag
{
    [XmlElement(ElementName = "uri")]
    public string uri { get; set; }
    [XmlElement(ElementName = "type")]
    public string type { get; set; }
}

public class DeltaMetadataTag : MetadataTag
{
    [XmlElement(ElementName = "id")]
    public string id { get; set; }
}