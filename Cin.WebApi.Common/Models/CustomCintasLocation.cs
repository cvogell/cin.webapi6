﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomCintasLocation")]
public class CustomCintasLocation
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_cintaslocation;
    [XmlElement(ElementName = "po_cintaslocation")]
    public string Po_cintaslocation
    {
        get => _po_cintaslocation.EscapeSingleQuote();
        set => _po_cintaslocation = value.EscapeSingleQuote();
    }

    private string _po_state;
    [XmlElement(ElementName = "po_state")]
    public string Po_state
    {
        get => _po_state.EscapeSingleQuote();
        set => _po_state = value.EscapeSingleQuote();
    }

    private string _po_locationname;
    [XmlElement(ElementName = "po_locationname")]
    public string Po_locationname
    {
        get => _po_locationname.EscapeSingleQuote();
        set => _po_locationname = value.EscapeSingleQuote();
    }

    private string _po_locationaddressline1;
    [XmlElement(ElementName = "po_locationaddressline1")]
    public string Po_locationaddressline1
    {
        get => _po_locationaddressline1.EscapeSingleQuote();
        set => _po_locationaddressline1 = value.EscapeSingleQuote();
    }

    private string _po_city;
    [XmlElement(ElementName = "po_city")]
    public string Po_city
    {
        get => _po_city.EscapeSingleQuote();
        set => _po_city = value.EscapeSingleQuote();
    }

    [Key]
    [XmlElement(ElementName = "po_cintaslocationid")]
    public Guid Po_CintasLocationId { get; set; }
    [XmlElement(ElementName = "po_locationid")]
    public Po_LocationId Po_LocationId { get; set; }
    [XmlElement(ElementName = "po_businessdivisionid")]
    public Po_BusinessDivisionId Po_businessdivisionid { get; set; }
    [XmlElement(ElementName = "po_sappriceschedule")]
    public Po_sappriceschedule Po_sappriceschedule { get; set; }
}

[XmlRoot(ElementName = "po_locationid")]
public class Po_LocationId : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "CintasLocationResultSet")]
public class CintasLocationResultSet
{
    [XmlElement(ElementName = "CustomCintasLocation")]
    public List<CustomCintasLocation> CustomCintasLocation { get; set; }

}

[XmlRoot(ElementName = "po_sappriceschedule")]
public class Po_sappriceschedule : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

public class JSONCintasLocation
{
    public string odataetag { get; set; }
    public string po_cintaslocation { get; set; }
    public string po_state { get; set; }
    public string po_locationname { get; set; }
    public string po_locationaddressline1 { get; set; }
    public string po_city { get; set; }
    public string po_cintaslocationid { get; set; }
    public int? po_locationid { get; set; }
    public int? po_sappriceschedule { get; set; }
    public string _po_businessdivisionid_po_businessdivision { get; set; }//ChatakonduV E0143633 removed id to store the name of the business
    public string _po_businessdivisionid_value { get; set; }
}