﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;



[XmlRoot(ElementName = "CustomCompetitorFS")]
public class CustomCompetitorFS
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_name;
    [XmlElement(ElementName = "po_name")]
    public string Po_name
    {
        get => _po_name.EscapeSingleQuote();
        set => _po_name = value.EscapeSingleQuote();
    }

    private string _po_notes;
    [XmlElement(ElementName = "po_notes")]
    public string po_Notes
    {
        get => _po_notes.EscapeSingleQuote();
        set => _po_notes = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_cintergy_competitor_id")]
    public Po_cintergy_competitor_id po_Cintergy_Competitor_ID { get; set; }

    private string _po_cintergybusinessdivision;
    [XmlElement(ElementName = "po_cintergybusinessdivision")]
    public string po_CintergyBusinessDivision
    {
        get => _po_cintergybusinessdivision.EscapeSingleQuote();
        set => _po_cintergybusinessdivision = value.EscapeSingleQuote();
    }

    [Key]
    [XmlElement(ElementName = "po_competitorfsid")]
    public Guid po_competitorfsId { get; set; }
}

[XmlRoot(ElementName = "CompetitorFSResultSet")]
public class CompetitorFSResultSet
{
    [XmlElement(ElementName = "CustomCompetitorFS")]
    public List<CustomCompetitorFS> CustomCompetitorFS { get; set; }
}

public class JSONCompetitorFS
{
    public string odataetag { get; set; }
    public string po_name { get; set; }
    public string po_notes { get; set; }
    public int? po_cintergy_competitor_id { get; set; }
    public string po_cintergybusinessdivision { get; set; }
    public string po_competitorfsid { get; set; }

}