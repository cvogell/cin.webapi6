﻿namespace Cin.WebApi.Models;

public static class Constants
{
    public const string connectionFailure = "Connection Failure";

    public const string failureResponse = "Failure";

    public const string successResponse = "Success";

    public const string systemUser = "systemuser";

    public const string entityCustom = "Custom";

    public const string mandatoryId = "Id is mandatory";

    public const string recordUpdate = "Record Updated";

    public const string mandatoryName = "Name is mandatory";
    public const string mandatorySubject = "Subject is mandatory";

    public const string mandatoryProductDescription = "ProductDescription is mandatory";

    public const string mandatoryQuoteId = "QuoteId";
    public const string mandatoryQuoteNumber = "Quote Number is mandatory";
    public const string queryId = "QUERYID";

    public const string userId = "USERID";

    public const string callingUser = "calling user =";

    public const string queryFailure = "Query did not return any results";

    public const string resultStartTag = "<result>";

    public const string resultEndTag = "</result>";

    public const string resultSetTag = "ResultSet>";

    public const string odataRemoval = "/odata/";

    public const string create = "Create";

    public const string Id = "Id";

    public const string Post = "post";

    public const string opportunityIdCheck = "Please provide the opportunity Id for which the Product is created";

    public const string opportunityProductIdCheck = "You have selected for Existing Product and Product Id cannot be null";

    public const string opportunityUOMIdCheck = "You have selected for Existing Product and Product Unit Id cannot be null";

    public const string opportunityProductDescCheck = "You have selected for Write Line Product and Product Description cannot be null";

    public const string quoteIdCheck = "Please provide the Quote Id";

    public const string lastNameCheck = "Last Name is mandatory";

    public const string soldVolume = "Sold Volume is mandatory";

    public const string soldAcctType = "Sold Acct Type is mandatory";

    public const string setToInstall = "Set To Install Date is mandatory";

    public const string Insert = "INSERT";

    public const string Update = "UPDATE";
    public const string Create = "CREATE";

    public const string productOverridenCheck = "Please provide the IsProductOverriden value";

    public const string po_targetstage_closed = "CLOSE";
}

public static class Entities
{
    public const string accountName = "Account";
    public const string accountNameOData = "accounts";
    public const string accountLogicalName = "account";

    public const string activityName = "Activity";
    public const string activityLogicalName = "activitypointer";
    public const string activityOData = "activitypointers";

    public const string agreementTermName = "AgreementTerm";
    public const string agreementTermNameOData = "po_agreementterms";
    public const string agreementTermLogicalName = "po_agreementterm";

    public const string agreementTermVariationName = "AgreementTermVariation";
    public const string agreementTermVariationNameOData = "po_agreementtermvariations";
    public const string agreementTermVariationLogicalName = "po_agreementtermvariation";

    public const string appointmentName = "Appointment";
    public const string appointmentLogicalName = "appointment";
    public const string appointmentOData = "appointments";

    public const string leadreferralName = "LeadReferral";
    public const string leadreferralNameOData = "leadreferrals";
    public const string leadreferralLogicalName = "leadreferral";

    public const string opportunityName = "Opportunity";
    public const string opportunityLogicalName = "opportunity";
    public const string opportunityNameOData = "opportunities";

    public const string opportunityCloseName = "OpportunityClose";
    public const string opportunityCloseLogicalName = "opportunityclose";

    public const string pricelevelLogicalName = "pricelevel";
    public const string UOMLogicalName = "uom";

    public const string opportunityProductLogicalName = "opportunityproduct";
    public const string opportunityProductName = "OpportunityProduct";
    public const string opportunityProductODataName = "opportunityproducts";


    public const string QuoteName = "Quote";
    public const string QuoteLogicalName = "quote";
    public const string QuoteNameOData = "quotes";

    public const string QuoteDetailName = "QuoteDetail";
    public const string QuoteDetailLogicalName = "quotedetail";
    public const string QuoteDetailNameOData = "quotedetails";

    public const string QuoteCloseName = "QuoteClose";
    public const string QuoteCloseLogicalName = "quoteclose";

    public const string NacProcessingName = "NacProcessing";
    public const string NacProcessingLogicalName = "po_nacprocessing";

    public const string CompetitorFSName = "CompetitorFS";
    public const string CompetitorFSNameOdata = "po_competitorfses";
    public const string CompetitorFSLogicalName = "po_competitorfs";

    public const string CompetitorURName = "CompetitorUR";
    public const string CompetitorURLogicalName = "po_competitorur";
    public const string CompetitorURNameOdata = "po_competitorurs";

    public const string SystemUserName = "SystemUser";
    public const string SystemUserLogicalName = "systemuser";
    public const string SystemUserNameOData = "systemusers";

    public const string CustomerDetailsName = "CustomerDetails";
    public const string CustomerDetailsLogicalName = "po_customer";
    public const string CustomerDetailsOData = "po_customers";

    public const string contactName = "Contact";
    public const string contactLogicalName = "contact";
    public const string contactNameOData = "contacts";

    public const string cintasLocationName = "CintasLocation";
    public const string cintasLocationNameOData = "po_cintaslocations";
    public const string cintasLocationLogicalName = "po_cintaslocation";

    public const string businessDivisionName = "BusinessDivision";
    public const string businessDivisionLogicalName = "po_businessdivision";
    public const string businessDivisionNameOData = "po_businessdivisions";

    public const string globalRentalSettingName = "GlobalRentalSetting";
    public const string globalRentalSettingLogicalName = "po_rentalpolicy";
    public const string globalRentalSettingNameOData = "po_rentalpolicies";
    public const string ctasCrmEntitiesLogicalName = "ctas_crmentities";

    public const string cintasAgreementProcessName = "CintasAgreementProcess";
    public const string cintasAgreementProcessLogicalName = "po_cintasagreementprocess";
    public const string cintasAgreementProcessNameOdata = "po_cintasagreementprocesses";

    public const string markAsSoldName = "MarkAsSold";
    public const string markAsSoldLogicalName = "po_soldactivity";
    public const string markAsSoldOdata = "po_soldactivities";

    public const string ProductName = "Product";
    public const string ProductLogicalName = "product";
    public const string ProductNameOdata = "products";

    public const string ProductAssociationName = "ProductAssociation";
    public const string ProductAssociationLogicalName = "productassociation";
    public const string ProductAssociationNameOdata = "productassociations";

    public const string productRelationshipName = "ProductRelationship";
    public const string productRelationshipLogicalName = "productsubstitute";
    public const string productRelationshipNameOdata = "productsubstitutes";

    public const string transactionCurrency = "transactioncurrency";

    public const string po_Naicscode = "po_naicscode";

    public const string po_Postalcode = "po_postalcode";

    public const string po_businessdivisionbundleassociation = "po_businessdivisionbundleassociation";

    public const string po_naicscodeproductbundleassociation = "po_naicscodeproductbundleassociation";
    public const string po_naicscodeproductbundleassociationLogicalName = "po_naicscodeproductbundleassociation";
    public const string po_naicscodeproductbundleassociationODataName = "po_naicscodeproductbundleassociations";

    public const string po_Competitor_UR = "po_competitorur";
    public const string po_Competitor_FS = "po_competitorfs";
    public const string po_Competitor_UR_MDR = "po_competitorur";
    public const string po_Competitor_FS_MDR = "po_competitorfs";
    public const string po_Competitor_UR_FRC = "po_competitorur";
    public const string po_Competitor_FS_DeepClean = "po_competitorfs";

    public const string proposalAndAgreementName = "ProposalsAndAgreements";
    public const string proposalAndAgreementLogicalName = "ctas_proposalsandagreements";
    public const string proposalAndAgreementODataName = "ctas_proposalsandagreementses";

    public const string postalCodeAlignmentName = "PostalCodeAlignment";
    public const string postalCodeAlignmentLogicalName = "po_postalcodealignment";

    public const string postalCodeName = "PostalCode";
    public const string postalCodeLogicalName = "po_postalcode";
    public const string postalCodeNameOData = "po_postalcodes";


}

public static class Delimiters
{
    public const string Dot = ".";

    public const string startWith = "<";

    public const string endWith = ">";

    public const string LessThanSlash = "</";

    public const string colon = ":";
}

public static class AppSettings
{
    public const string metadataURL = "metadataUrl";

    public const string modelNameSpace = "modelNamespace";

    public const string useDeltas = "useDeltas";

    public const string deltaFilter = "modifiedon|{0}|{1}";
}

public static class Attributes
{
    public const string ctas_Appointment_Type = "ctas_appointment_type";

    public const string createdOn = "createdon";

    public const string modifiedOn = "modifiedon";

    public const string optionalAttendees = "optionalattendees";

    public const string requiredAttendees = "requiredattendees";

    public const string description = "description";

    public const string location = "location";

    public const string po_Meeting_With = "po_meeting_with";

    public const string priorityCode = "prioritycode";

    public const string instanceTypeCode = "instancetypecode";

    public const string scheduledStart = "scheduledstart";

    public const string scheduledEnd = "scheduledend";

    public const string deltaFilter = "modifiedon|{0}|{1}";

    public const string parentAccountId = "parentaccountid";

    public const string parentContactId = "parentcontactid";

    public const string opportunityId = "opportunityid";

    public const string name = "name";

    public const string cintaslocation = "ctas_cintas_location";

    public const string targetStage = "po_targetstage";

    public const string statusCode = "statuscode";

    public const string stateCode = "statecode";

    public const string estimatedValue = "estimatedvalue";

    public const string estimatedCloseDate = "estimatedclosedate";

    public const string ownerId = "ownerid";

    public const string transactionCurrencyId = "transactioncurrencyid";

    public const string po_ActualVolume = "po_actualvolume";

    public const string po_ProposalSentDate = "po_proposalsentdate";

    public const string po_OpportunityType = "po_opportunitytype";

    public const string po_BusinessDivisionId = "po_businessdivisionid";

    public const string po_NumberOfSites = "po_numberofsites";

    public const string ctas_AgreementSentDate = "ctas_agreementsentdate";

    public const string ctas_PowerAdd = "ctas_power_add";

    public const string po_ProposalExpiredDate = "po_proposalexpireddate";

    public const string po_Rejectedby = "po_rejectedby";

    public const string po_ReasonForAgreementRejection = "po_reasonforagreementrejection";

    public const string po_MarkedAsSoldWeeklyVolume = "po_markedassoldweeklyvolume";

    public const string po_WORNewBusinessVolume = "po_wornewbusinessvolume";

    public const string po_InstallDate = "po_wornewbusinessvolume";

    public const string po_13WeekEndDate = "po_13weekenddate";

    public const string opportunityProductId = "opportunityproductid";

    public const string isProductOverridden = "isproductoverridden";

    public const string isPriceOverridden = "ispriceoverridden";

    public const string UOMId = "uomid";

    public const string productId = "productid";

    public const string productDescription = "productdescription";

    public const string extendedAmount = "extendedamount";

    public const string po_Inventory = "po_inventory";

    public const string po_ItemDelFreq = "po_itemdelfreq";

    public const string po_ItemNo = "po_itemno";

    public const string po_UsageCD = "po_usagecd";

    public const string pricePerUnit = "priceperunit";

    public const string Quantity = "quantity";

    public const string productTypeCode = "producttypecode";

    public const string sequenceNumber = "sequencenumber";

    public const string po_RentalPolicyId = "po_rentalpolicyid";

    public const string po_ThreadColor = "po_threadcolor";

    public const string po_TrimType = "po_trimtype";

    public const string po_BackgroundColor = "po_backgroundcolor";

    public const string lastName = "lastname";

    public const string firstName = "firstname";

    public const string jobTitle = "jobtitle";

    public const string contactId = "contactid";

    public const string internalEmailAddress = "internalemailaddress";

    public const string emailAddress1 = "emailaddress1";

    public const string address1_StateOrProvince = "address1_stateorprovince";

    public const string salutation = "salutation";

    public const string activityid = "activityid";

    public const string ctas_agreement_date = "ctas_agreement_date";

    public const string po_soldvolume = "po_soldvolume";

    public const string po_solddate = "po_solddate";

    public const string po_sold_acct_type = "po_sold_acct_type";

    public const string po_nac = "po_nac";

    public const string po_markcomplete = "po_markcomplete";

    public const string po_estimatedsoldvolume = "po_estimatedsoldvolume";

    public const string po_actual_sold_volume = "po_actual_sold_volume";

    public const string po_actual_sold_date = "po_actual_sold_date";

    public const string po_accountsold = "po_accountsold";

    public const string customerid = "customerid";

    public const string po_competitor_fs = "po_competitor_fs";

    public const string po_competitor_ur = "po_competitor_ur";

    public const string po_competitor_fs_mdr = "po_competitor_fs_mdr";

    public const string po_competitor_ur_mdr = "po_competitor_ur_mdr";

    public const string po_competitor_fs_deepclean = "po_competitor_fs_deepclean";

    public const string po_competitor_ur_frc = "po_competitor_ur_frc";

    public const string parentcustomerid = "parentcustomerid";

    public const string customerId = "po_customerid";

    public const string po_mlacode = "po_mlacode";

    public const string po_customertoaccount = "po_customertoaccount";

    public const string po_customernumber = "po_customernumber";

    public const string po_FkLocNo = "po_fklocno";

    public const string RegardingObjectId = "regardingobjectid";

    public const string po_CustomerName = "po_customername";

    public const string po_CUSTOMER_ACTIVE = "po_customer_active";

    public const string po_CurrentRteNum = "po_currentrtenum";

    public const string po_CorporateCode = "po_corporatecode";

    public const string po_ContractNumber = "po_contractnumber";

    public const string po_CintasLocationId = "po_cintaslocationid";

    public const string po_businessdivisionid = "po_businessdivisionid";

    public const string po_businessline = "po_businessline";

    public const string ctas_SAP_ID = "ctas_sap_id";

    public const string customer = "customer";

    public const string processId = "processid";

    public const string stageId = "stageid";

    public const string subject = "subject";

    public const string fullName = "fullname";

    public const string systemuserid = "systemuserid";

    public const string ctas_countrycode = "ctas_countrycode";
    public const string ctas_language = "ctas_language";
    public const string ctas_contractregion = "ctas_contractregion";
    public const string po_parentterm = "po_parentterm";

    public const string po_postalcode = "po_postalcode";
    public const string po_city = "po_city";
    public const string po_state = "po_state";

    public const string po_sappriceschedule = "po_sappriceschedule";

    public const string productisrequired = "productisrequired";
    public const string propertycustomizationstatus = "propertycustomizationstatus";

    public const string productstructure = "productstructure";

    public const string direction = "direction";
    public const string salesrelationshiptype = "salesrelationshiptype";

    public const string ctas_partner_role = "ctas_partner_role";
    public const string ctas_business_division = "ctas_businessdivision";
    public const string activitytypecode = "activitytypecode";


    #region Accounts

    public const string accountid = "accountid";
    public const string telephone1 = "telephone1";
    public const string po_naicscodeid = "po_naicscodeid";
    public const string po_postalcodeid = "po_postalcodeid";
    public const string po_abinumber = "po_abinumber";
    public const string address1_postalcode = "address1_postalcode";
    public const string address1_longitude = "address1_longitude";
    public const string address1_line3 = "address1_line3";
    public const string address1_line1 = "address1_line1";
    public const string address1_latitude = "address1_latitude";
    public const string address1_county = "address1_county";
    public const string address1_country = "address1_country";
    public const string address1_city = "address1_city";
    public const string po_estimatedvolume_ur_mdr = "po_estimatedvolume_ur_mdr";
    public const string po_estimatedvolume_ur_frc = "po_estimatedvolume_ur_frc";
    public const string po_estimatedvolume_fs_mdr = "po_estimatedvolume_fs_mdr";
    public const string po_estimatedvolume_fs_deepclean = "po_estimatedvolume_fs_deepclean";
    public const string po_estimatedvolume_acct_ur = "po_estimatedvolume_acct_ur";
    public const string po_estimatedvolume_acct_fs = "po_estimatedvolume_acct_fs";
    public const string po_estimatedclosedate_ur = "po_estimatedclosedate_ur";
    public const string po_estimatedclosedate_ur_mdr = "po_estimatedclosedate_ur_mdr";
    public const string po_estimatedclosedate_ur_frc = "po_estimatedclosedate_ur_frc";
    public const string po_estimatedclosedate_fs = "po_estimatedclosedate_fs";
    public const string po_estimatedclosedate_fs_mdr = "po_estimatedclosedate_fs_mdr";
    public const string po_estimatedclosedate_fs_deepclean = "po_estimatedclosedate_fs_deepclean";
    public const string address1_line2 = "address1_line2";
    public const string po_custofur = "po_custofur";
    public const string po_custoffs = "po_custoffs";
    public const string po_custoffire = "po_custoffire";
    public const string po_custoffas = "po_custoffas";
    public const string po_custofchicagods = "po_custofchicagods";
    public const string po_custofcatalog = "po_custofcatalog";
    #endregion

    #region Quote
    public const string quoteid = "quoteid";
    public const string statuscode = "statuscode";
    public const string statecode = "statecode";
    public const string quotenumber = "quotenumber";
    public const string po_wornewbusinessvolume = "po_wornewbusinessvolume";
    public const string po_wornbinstalledvolume = "po_wornbinstalledvolume";
    public const string po_orderedkeyedweeklyvolume = "po_orderedkeyedweeklyvolume";
    public const string po_businessdivison = "po_businessdivison";
    public const string po_13weekcalcfield = "po_13weekcalcfield";
    public const string opportunityid = "opportunityid";
    public const string pricelevelid = "pricelevelid";
    public const string transactioncurrencyid = "transactioncurrencyid";
    public const string po_actualvolume = "po_actualvolume";
    public const string po_installedvolume = "po_installedvolume";
    public const string po_estimatedweeklyvolume = "po_estimatedweeklyvolume";
    #endregion

    #region Quote Close
    public const string quotecloseid = "quotecloseid";
    #endregion

    #region Quote Details
    public const string quotedetailid = "quotedetailid";

    public const string productdescription = "productdescription";
    public const string producttypecode = "producttypecode";
    public const string parentbundleid = "parentbundleid";
    public const string sequencenumber = "sequencenumber";
    public const string isproductoverridden = "isproductoverridden";
    public const string quantity = "quantity";
    public const string priceperunit = "priceperunit";
    public const string extendedamount = "extendedamount";
    public const string productid = "productid";
    public const string po_itemno = "po_itemno";
    public const string po_usagecd = "po_usagecd";
    public const string po_itemdelfreq = "po_itemdelfreq";
    public const string po_inventory = "po_inventory";
    public const string lineitemnumber = "lineitemnumber";

    #endregion

    #region Nac Processing
    public const string po_name = "po_name";
    public const string po_nacprocessingid = "po_nacprocessingid";
    public const string po_solddate_nac = "po_solddate_nac";
    public const string po_salesrep_nac = "po_salesrep_nac";
    public const string po_poweradd_nac = "po_poweradd_nac";
    public const string po_estimatedvolume_ur_nac = "po_estimatedvolume_ur_nac";
    public const string po_weeklytotal_nac = "po_weeklytotal_nac";
    public const string po_worvolume_nac = "po_worvolume_nac";
    public const string po_mark_as_sold = "po_mark_as_sold";
    public const string po_route_nac = "po_route_nac";
    public const string po_installdate_nac = "po_installdate_nac";
    public const string po_notes_nac = "po_notes_nac";
    public const string po_businessdivision = "po_businessdivision";
    public const string po_locationnumber = "po_locationnumber";
    public const string po_zippostalcode_nac = "po_zippostalcode_nac";
    public const string po_state_nac = "po_state_nac";
    public const string po_city_nac = "po_city_nac";
    public const string po_soldvolume_nac = "po_soldvolume_nac";
    public const string po_customernumber_nac = "po_customernumber_nac";
    public const string po_week_nac = "po_week_nac";
    public const string po_day_nac = "po_day_nac";
    public const string po_opportunity = "po_opportunity";
    public const string po_cintasagreementprocessguid = "po_cintasagreementprocessguid";
    public const string po_defaultuscurrency = "po_defaultuscurrency";
    public const string po_newbusinessopportunitytype = "po_newbusinessopportunitytype";
    public const string po_poweraddopportunitytype = "po_poweraddopportunitytype";
    public const string po_quotepricelevelid = "po_quotepricelevelid";
    public const string ctas_primary_rep = "ctas_primary_rep";
    #endregion

    #region Proposals And Agreements
    public const string ctas_name = "ctas_name";
    public const string ctas_azuredocumenturl = "ctas_azuredocumenturl";
    public const string ctas_customersigneddate = "ctas_customersigneddate";
    public const string ctas_documenttype = "ctas_documenttype";
    public const string ctas_documenturl = "ctas_documenturl";
    public const string ctas_docusignenvelopeid = "ctas_docusignenvelopeid";
    public const string ctas_expirationdate = "ctas_expirationdate";
    public const string ctas_opportunity = "ctas_opportunity";
    public const string ctas_propagrmtcurrentrevisionid = "ctas_propagrmtcurrentrevisionid";
    public const string ctas_proposalsandagreementsid = "ctas_proposalsandagreementsid";
    public const string ctas_sentdate = "ctas_sentdate";
    public const string ctas_signaturetype = "ctas_signaturetype";
    public const string ctas_documentlocation = "ctas_documentlocation";
    #endregion
}