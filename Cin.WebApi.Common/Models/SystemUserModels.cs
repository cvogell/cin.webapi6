﻿using Cin.WebApi.Common;
using Cin.WebApi.Extensions;
using System.Xml.Serialization;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "ownerid")]
public class OwnerId : DynamicsEntityReference
{
    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "createdby")]
public class Createdby : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}