﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

//ChatakonduV E0143633 added below class
public class JSONMarkAsSold
{
    public string odataetag { get; set; }
    public string subject { get; set; }
    public string po_soldvolume { get; set; }
    public DateTime? po_solddate { get; set; }
    public string po_sold_acct_type { get; set; }
    public string po_nac { get; set; }
    public string po_markcomplete { get; set; }
    public string _po_locationnumber_value { get; set; }
    public string _po_locationnumber_po_cintaslocation { get; set; }
    public string _regardingobjectid_value { get; set; }
    public string _regardingobjectid_name { get; set; }
    public string _ctas_primary_rep_value { get; set; }
    public string _ctas_primary_rep_ctas_primary_rep { get; set; }
    public string po_estimatedsoldvolume { get; set; }
    public string _po_businessdivison_value { get; set; }
    public string _po_businessdivison_po_businessdivison { get; set; }
    public string po_actual_sold_volume { get; set; }
    public DateTime? po_actual_sold_date { get; set; }
    public string po_accountsold { get; set; }
    public string activityid { get; set; }
    public string _transactioncurrencyid_value { get; set; }
    public string _transactioncurrencyid_currencyname { get; set; }
    public string createdby { get; set; }
    public string ownerid { get; set; }
    public DateTime? ctas_agreement_date { get; set; }
    public string _po_opportunity_value { get; set; }
    public string _po_opportunity_name { get; set; }
    public string po_businessline { get; set; }
    public string ctas_power_add { get; set; }


}
[XmlRoot(ElementName = "CustomMarkAsSold")]
public class CustomMarkAsSold
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _subject;
    [XmlElement(ElementName = "subject")]
    public string Subject
    {
        get => _subject.EscapeSingleQuote();
        set => _subject = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_soldvolume")]
    public Po_SoldVolume Po_SoldVolume { get; set; }
    [XmlElement(ElementName = "po_solddate")]
    public Po_SoldDate Po_SoldDate { get; set; }
    [XmlElement(ElementName = "po_sold_acct_type")]
    public Po_Sold_Acct_Type Po_Sold_Acct_Type { get; set; }
    [XmlElement(ElementName = "po_nac")]
    public Po_NAC Po_NAC { get; set; }
    [XmlElement(ElementName = "po_markcomplete")]
    public Po_MarkComplete Po_MarkComplete { get; set; }
    [XmlElement(ElementName = "po_locationnumber")]
    public Po_locationnumber Po_Locationnumber { get; set; }
    [XmlElement(ElementName = "regardingobjectid")]
    public Regardingobjectid RegardingObjectId { get; set; }
    [XmlElement(ElementName = "ctas_primary_rep")]
    public Ctasprimaryrepfull ctas_primary_rep { get; set; }
    [XmlElement(ElementName = "po_estimatedsoldvolume")]
    public Po_EstimatedSoldVolume Po_EstimatedSoldVolume { get; set; }
    [XmlElement(ElementName = "po_businessdivison")]
    public Po_businessdivison Po_Businessdivison { get; set; }
    [XmlElement(ElementName = "po_actual_sold_volume")]
    public Po_Actual_Sold_Volume Po_Actual_Sold_Volume { get; set; }
    [XmlElement(ElementName = "po_actual_sold_date")]
    public Po_Actual_Sold_Date Po_Actual_Sold_Date { get; set; }
    [XmlElement(ElementName = "po_accountsold")]
    public Po_AccountSold Po_AccountSold { get; set; }
    [XmlElement(ElementName = "activityid")]
    [Key]
    public Guid ActivityId { get; set; }
    [XmlElement(ElementName = "transactioncurrencyid")]
    public MASTransactionCurrencyId TransactionCurrencyId { get; set; }
    [XmlElement(ElementName = "createdby")]
    public Createdby CreatedBy { get; set; }

    [XmlElement(ElementName = "ownerid")]
    public OwnerId OwnerId { get; set; }

    [XmlElement(ElementName = "ctas_agreement_date")]
    public ctas_Agreement_Date ctas_Agreement_Date { get; set; }
    [XmlElement(ElementName = "po_opportunity")]
    public Po_opportunity Po_opportunity { get; set; }
    [XmlElement(ElementName = "po_businessline")]
    public Po_businessline Po_businessline { get; set; }
    [XmlElement(ElementName = "ctas_power_add")]
    public CtasPowerAdd Ctas_power_add { get; set; }
}

[XmlRoot(ElementName = "transactioncurrencyid")]
public class MASTransactionCurrencyId : DynamicsEntityReference
{
    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "regardingobjectid")]
public class Regardingobjectid : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_primary_rep")]
public class Ctasprimaryrepfull : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

public class CtasPowerAdd : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_agreement_date")]
public class ctas_Agreement_Date : DynamicsDateTimeLocalValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_soldvolume")]
public class Po_SoldVolume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_solddate")]
public class Po_SoldDate : DynamicsDateTimeLocalValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_sold_acct_type")]
public class Po_Sold_Acct_Type : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_nac")]
public class Po_NAC
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_markcomplete")]
public class Po_MarkComplete : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_estimatedsoldvolume")]
public class Po_EstimatedSoldVolume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_actual_sold_volume")]
public class Po_Actual_Sold_Volume : DynamicsCurrencyValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_actual_sold_date")]
public class Po_Actual_Sold_Date : DynamicsDateTimeLocalValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_accountsold")]
public class Po_AccountSold : DynamicsBooleanValue
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "MarkAsSoldResultSet")]
public class MarkAsSoldResultSet
{
    [XmlElement(ElementName = "CustomMarkAsSold")]
    public List<CustomMarkAsSold> CustomMarkAsSold { get; set; }
}

[XmlRoot(ElementName = "po_businessdivison")]
public class Po_businessdivison : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    //[XmlText]
    //public string Text { get; set; }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_opportunity")]
public class Po_opportunity : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    //[XmlText]
    //public string Text { get; set; }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_businessline")]
public class Po_businessline : DynamicsOptionSetRef
{ 
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}