﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomOpportunity")]
public class CustomOpportunity
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "ctas_cintas_location")]
    public string Ctas_Cintas_Location { get; set; }
    [XmlElement(ElementName = "po_actualvolume")]
    public Po_ActualVolumeFV Po_ActualVolume { get; set; }
    [XmlElement(ElementName = "ownerid")]
    public OwnerId Ownerid { get; set; }
    [XmlElement(ElementName = "po_businessdivisionid")]
    public Po_BusinessDivisionId Po_BusinessDivisionId { get; set; }
    [XmlElement(ElementName = "parentaccountid")]
    public ParentAccountid ParentAccountId { get; set; }
    [XmlElement(ElementName = "statuscode")]
    public StatusCode StatusCode { get; set; }
    [XmlElement(ElementName = "statecode")]
    public StateCode StateCode { get; set; }

    private string? _stageid;
    [XmlElement(ElementName = "stageid")]
    public string? StageId
    {
        get => _stageid?.FormatGuidString();
        set => _stageid = value?.FormatGuidString();
    }

    private string _po_targetstage;
    [XmlElement(ElementName = "po_targetstage")]
    public string Po_TargetStage
    {
        get => _po_targetstage.EscapeSingleQuote();
        set => _po_targetstage = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "parentcontactid")]
    public ParentContactId ParentContactId { get; set; }
    [XmlElement(ElementName = "estimatedvalue")]
    public Estimatedvalue EstimatedValue { get; set; }
    [XmlElement(ElementName = "estimatedclosedate")]
    public EstimatedCloseDate EstimatedCloseDate { get; set; }
    [XmlElement(ElementName = "po_proposalexpireddate")]
    public Po_ProposalExpiredDate Po_ProposalExpiredDate { get; set; }
    [XmlElement(ElementName = "po_opportunitytype")]
    public Po_OpportunityType Po_OpportunityType { get; set; }
    [XmlElement(ElementName = "po_numberofsites")]
    public Po_NumberOfSites Po_NumberOfSites { get; set; }
    [XmlElement(ElementName = "transactioncurrencyid")]
    public TransactionCurrencyId TransactionCurrencyId { get; set; }

    [Key]
    [XmlElement(ElementName = "opportunityid")]
    public Guid OpportunityId { get; set; }
    [XmlElement(ElementName = "po_wornewbusinessvolume")]
    public Po_WorNewBusinessVolumeFV Po_WorNewBusinessVolume { get; set; }
    [XmlElement(ElementName = "po_worinstalledvolume")]
    public Po_WorInstalledVolume Po_WorInstalledVolume { get; set; }
    [XmlElement(ElementName = "po_rejectedby")]
    public RejectedBy Po_RejectedBy { get; set; }

    private string _po_reasonforagreementrejection;
    [XmlElement(ElementName = "po_reasonforagreementrejection")]
    public string Po_ReasonForAgreementRejection
    {
        get => _po_reasonforagreementrejection.EscapeSingleQuote();
        set => _po_reasonforagreementrejection = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_markedassoldweeklyvolume")]
    public Po_MarkedAsSoldWeeklyVolumeFV Po_MarkedAsSoldWeeklyVolume { get; set; }
    [XmlElement(ElementName = "po_orderedkeyedweeklyvolume")]
    public Po_OrderedKeyedWeeklyVolumeFV Po_OrderedKeyedWeeklyVolume { get; set; }

    [XmlElement(ElementName = "po_installdate")]
    public Po_InstallDate Po_InstallDate { get; set; }
    [XmlElement(ElementName = "po_13weekenddate")]
    public Po_13WeekendDate Po_13WeekendDate { get; set; }
    [XmlElement(ElementName = "ctas_agreementsentdate")]
    public ctas_AgreementSentDate ctas_AgreementSentDate { get; set; }

    [XmlElement(ElementName = "po_proposalsentdate")]
    public Po_ProposalSentDate Po_ProposalSentDate { get; set; }

    [XmlElement(ElementName = "createdby")]
    public Createdby Createdby { get; set; }

    [XmlElement(ElementName = "createdon")]
    public CreatedOn CreatedOn { get; set; }

    [XmlElement(ElementName = "modifiedon")]
    public ModifiedOn ModifiedOn { get; set; }

    private string _processid;
    [XmlElement(ElementName = "processid")]
    public string ProcessId
    {
        get => _processid.EscapeSingleQuote();
        set => _processid = value.EscapeSingleQuote();
    }

}


[XmlRoot(ElementName = "po_actualvolume")]
public class Po_ActualVolumeFV : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_rejectedby")]
public class RejectedBy : DynamicsEntityReference
{
    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "parentaccountid")]
public class ParentAccountid : DynamicsEntityReference
{
    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}



[XmlRoot(ElementName = "parentcontactid")]
public class ParentContactId : DynamicsEntityReference
{
    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "estimatedvalue")]
public class Estimatedvalue : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "createdon")]
public class CreatedOn : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "modifiedon")]
public class ModifiedOn : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "estimatedclosedate")]
public class EstimatedCloseDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_proposalexpireddate")]
public class Po_ProposalExpiredDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_opportunitytype")]
public class Po_OpportunityType : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_numberofsites")]
public class Po_NumberOfSites : DynamicsIntValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_wornewbusinessvolume")]
public class Po_WorNewBusinessVolumeFV : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_worinstalledvolume")]
public class Po_WorInstalledVolume : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlElement(ElementName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_markedassoldweeklyvolume")]
public class Po_MarkedAsSoldWeeklyVolumeFV : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_orderedkeyedweeklyvolume")]
public class Po_OrderedKeyedWeeklyVolumeFV :DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_installdate")]
public class Po_InstallDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_13weekenddate")]
public class Po_13WeekendDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_proposalsentdate")]
public class Po_ProposalSentDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_agreementsentdate")]
public class ctas_AgreementSentDate : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote( );
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "OpportunityResultSet")]
public class OpportunityResultSet
{
    [XmlElement(ElementName = "CustomOpportunity")]
    public List<CustomOpportunity> CustomOpportunity { get; set; }


}

public class JSONOpportunity
{
    public string odataetag { get; set; }
    public string Name { get; set; }
    public string Ctas_Cintas_Location { get; set; }
    public string Po_ActualVolume { get; set; }

    public string Po_BusinessDivisionId_value { get; set; }
    public string Po_BusinessDivisionId__po_businessdivision { get; set; }
    public string ParentAccountId_value { get; set; }
    public string ParentAccountId_Name { get; set; }
    public int StatusCode { get; set; }
    public int StateCode { get; set; }
    public string StageId { get; set; }
    public string Po_TargetStage { get; set; }
    public string ParentContactId_value { get; set; }
    public string ParentContactId_FullName { get; set; }
    public string EstimatedValue { get; set; }

    [XmlElement(ElementName = "estimatedclosedate")]
    public EstimatedCloseDate EstimatedCloseDate { get; set; }
    [XmlElement(ElementName = "po_proposalexpireddate")]
    public Po_ProposalExpiredDate Po_ProposalExpiredDate { get; set; }
    [XmlElement(ElementName = "po_opportunitytype")]
    public Po_OpportunityType Po_OpportunityType { get; set; }
    [XmlElement(ElementName = "po_numberofsites")]
    public Po_NumberOfSites Po_NumberOfSites { get; set; }
    [XmlElement(ElementName = "transactioncurrencyid")]
    public TransactionCurrencyId TransactionCurrencyId { get; set; }

    [Key]
    [XmlElement(ElementName = "opportunityid")]
    public Guid OpportunityId { get; set; }
    [XmlElement(ElementName = "po_wornewbusinessvolume")]
    public Po_WorNewBusinessVolumeFV Po_WorNewBusinessVolume { get; set; }
    [XmlElement(ElementName = "po_worinstalledvolume")]
    public Po_WorInstalledVolume Po_WorInstalledVolume { get; set; }
    [XmlElement(ElementName = "po_rejectedby")]
    public RejectedBy Po_RejectedBy { get; set; }

    private string _po_reasonforagreementrejection;
    [XmlElement(ElementName = "po_reasonforagreementrejection")]
    public string Po_ReasonForAgreementRejection
    {
        get => _po_reasonforagreementrejection.EscapeSingleQuote();
        set => _po_reasonforagreementrejection = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_markedassoldweeklyvolume")]
    public Po_MarkedAsSoldWeeklyVolumeFV Po_MarkedAsSoldWeeklyVolume { get; set; }
    [XmlElement(ElementName = "po_orderedkeyedweeklyvolume")]
    public Po_OrderedKeyedWeeklyVolumeFV Po_OrderedKeyedWeeklyVolume { get; set; }

    [XmlElement(ElementName = "po_installdate")]
    public Po_InstallDate Po_InstallDate { get; set; }
    [XmlElement(ElementName = "po_13weekenddate")]
    public Po_13WeekendDate Po_13WeekendDate { get; set; }
    [XmlElement(ElementName = "ctas_agreementsentdate")]
    public ctas_AgreementSentDate ctas_AgreementSentDate { get; set; }

    [XmlElement(ElementName = "po_proposalsentdate")]
    public Po_ProposalSentDate Po_ProposalSentDate { get; set; }

    [XmlElement(ElementName = "createdby")]
    public Createdby Createdby { get; set; }

    [XmlElement(ElementName = "createdon")]
    public CreatedOn CreatedOn { get; set; }

    [XmlElement(ElementName = "modifiedon")]
    public ModifiedOn ModifiedOn { get; set; }

    private string _processid;
    [XmlElement(ElementName = "processid")]
    public string ProcessId
    {
        get => _processid.EscapeSingleQuote();
        set => _processid = value.EscapeSingleQuote();
    }
}