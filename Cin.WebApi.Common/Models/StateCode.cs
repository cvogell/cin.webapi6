﻿using Cin.WebApi.Common;
using Cin.WebApi.Extensions;
using System.Xml.Serialization;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "statecode")]
public class StateCode : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }

}