﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomAppointment")]
public class CustomAppointment
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }
    [Key]
    [XmlElement(ElementName = "activityid")]
    public Guid ActivityId { get; set; }
    //scheduledstart
    [XmlElement(ElementName = "scheduledstart")]
    public ScheduledStart ScheduledStart { get; set; }
    //scheduledend
    [XmlElement(ElementName = "scheduledend")]
    public ScheduledEnd ScheduledEnd { get; set; }
    //regardingobjectid
    [XmlElement(ElementName = "regardingobjectid")]
    public Regardingobjectid RegardingObjectId { get; set; }
    //instancetypecode
    [XmlElement(ElementName = "instancetypecode")]
    public InstanceTypeCode InstanceTypeCode { get; set; }
    //prioritycode
    [XmlElement(ElementName = "prioritycode")]
    public PriorityCode PriorityCode { get; set; }
    //location
    [XmlElement(ElementName = "location")]
    public string? Location { get; set; }
    //po_meeting_with
    [XmlElement(ElementName = "po_meeting_with")]
    public string? po_Meeting_With { get; set; }
    //description
    [XmlElement(ElementName = "description")]
    public string? Description { get; set; }
    /*        //requiredattendees
            [XmlElement(ElementName = "requiredattendees")]
            public RequiredAttendees RequiredAttendees { get; set; }*/
    //ownerid
    [XmlElement(ElementName = "ownerid")]
    public OwnerId Ownerid { get; set; }
    /*        //optionalattendees
            [XmlElement(ElementName = "optionalattendees")]
            public OptionalAttendees OptionalAttendees { get; set; }*/
    //modifiedon
    [XmlElement(ElementName = "modifiedon")]
    public ModifiedOn ModifiedOn { get; set; }
    //createdon
    [XmlElement(ElementName = "createdon")]
    public CreatedOn CreatedOn { get; set; }
    //ctas_cintas_location
    [XmlElement(ElementName = "ctas_cintas_location")]
    public string? Ctas_Cintas_Location { get; set; }
    //ctas_appointment_type
    [XmlElement(ElementName = "ctas_appointment_type")]
    public ctas_Appointment_Type ctas_Appointment_Type { get; set; }
    //statecode
    [XmlElement(ElementName = "statecode")]
    public string? StateCode { get; set; }
}

[XmlRoot(ElementName = "ctas_Appointment_Type")]
public class ctas_Appointment_Type : DynamicsFormattedValue
{
    private string _value;
    [XmlAttribute(AttributeName = "value")]
    public string? Value
    {
        get => _value.EscapeSingleQuote();
        set => _value = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "prioritycode")]
public class PriorityCode : DynamicsFormattedValue
{
    private string _value;
    [XmlAttribute(AttributeName = "value")]
    public string? Value
    {
        get => _value.EscapeSingleQuote();
        set => _value = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "instancetypecode")]
public class InstanceTypeCode : DynamicsFormattedValue
{
    private string _value;
    [XmlAttribute(AttributeName = "value")]
    public string? Value
    {
        get => _value.EscapeSingleQuote();
        set => _value = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "scheduledstart")]
public class ScheduledStart : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string? Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string? Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote() ;
    }

    private string _text;
    [XmlText]
    public string? Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "scheduledend")]
public class ScheduledEnd : DynamicsDateTimeValue
{
    private string _date;
    [XmlAttribute(AttributeName = "date")]
    public string? Date
    {
        get => _date.EscapeSingleQuote();
        set => _date = value.EscapeSingleQuote();
    }

    private string _time;
    [XmlAttribute(AttributeName = "time")]
    public string? Time
    {
        get => _time.EscapeSingleQuote();
        set => _time = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string? Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "optionalAttendees")]
public class OptionalAttendees : DynamicsVariantEntityReference
{
    private bool _moreRecords;
    [XmlAttribute(AttributeName = "MoreRecords")]
    public bool MoreRecords
    {
        get => _moreRecords;
        set => _moreRecords = value;
    }

    private bool _totalRecordCountLimitExceeded;
    [XmlAttribute(AttributeName = "TotalRecordCountLimitExceeded")]
    public bool TotalRecordCountLimitExceeded
    {
        get => _totalRecordCountLimitExceeded;
        set => _totalRecordCountLimitExceeded = value;
    }

    private string _entityName;
    [XmlAttribute(AttributeName = "EntityName")]
    public string EntityName
    {
        get => _entityName;
        set => _entityName = value;
    }

    private int _totalRecordCount;
    [XmlAttribute(AttributeName = "TotalRecordCount")]
    public int TotalRecordCount
    {
        get => _totalRecordCount;
        set => _totalRecordCount = value;
    }

    private int _minActiveRowVersion;
    [XmlAttribute(AttributeName = "MinActiveRowVersion")]
    public int MinActiveRowVersion
    {
        get => _minActiveRowVersion;
        set => _minActiveRowVersion = value;
    }

    private string _pagingCookie;
    [XmlAttribute(AttributeName = "PagingCookie")]
    public string PagingCookie
    {
        get => _pagingCookie;
        set => _pagingCookie = value;
    }

    private string[] _entities;
    [XmlAttribute(AttributeName = "Entities")]
    public string[] Entities;

    public OptionalAttendees()
    {
    }

}

[XmlRoot(ElementName = "requiredAttendees")]
public class RequiredAttendees : DynamicsVariantEntityReference
{
    private bool _moreRecords;
    [XmlAttribute(AttributeName = "MoreRecords")]
    public bool MoreRecords
    {
        get => _moreRecords;
        set => _moreRecords = value;
    }

    private bool _totalRecordCountLimitExceeded;
    [XmlAttribute(AttributeName = "TotalRecordCountLimitExceeded")]
    public bool TotalRecordCountLimitExceeded
    {
        get => _totalRecordCountLimitExceeded;
        set => _totalRecordCountLimitExceeded = value;
    }

    private string _entityName;
    [XmlAttribute(AttributeName = "EntityName")]
    public string EntityName
    {
        get => _entityName;
        set => _entityName = value;
    }

    private int _totalRecordCount;
    [XmlAttribute(AttributeName = "TotalRecordCount")]
    public int TotalRecordCount
    {
        get => _totalRecordCount;
        set => _totalRecordCount = value;
    }

    private int _minActiveRowVersion;
    [XmlAttribute(AttributeName = "MinActiveRowVersion")]
    public int MinActiveRowVersion
    {
        get => _minActiveRowVersion;
        set => _minActiveRowVersion = value;
    }

    private string _pagingCookie;
    [XmlAttribute(AttributeName = "PagingCookie")]
    public string PagingCookie
    {
        get => _pagingCookie;
        set => _pagingCookie = value;
    }

    private string[] _entities;
    [XmlAttribute(AttributeName = "Entities")]
    public string[] Entities;
}

[XmlRoot(ElementName = "AppointmentResultSet")]
public class AppointmentResultSet
{
    [XmlElement(ElementName = "CustomAppointment")]
    public List<CustomAppointment> CustomAppointment { get; set; }


}