﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomSystemUser")]
public class CustomSystemUser
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _fullname;
    [XmlElement(ElementName = "fullname")]
    public string FullName
    {
        get => _fullname.EscapeSingleQuote();
        set => _fullname = value.EscapeSingleQuote();
    }

    private string _lastname;
    [XmlElement(ElementName = "lastname")]
    public string LastName
    {
        get => _lastname.EscapeSingleQuote();
        set => _lastname = value.EscapeSingleQuote();
    }

    private string _firstname;
    [XmlElement(ElementName = "firstname")]
    public string FirstName
    {
        get => _firstname.EscapeSingleQuote();
        set => _firstname = value.EscapeSingleQuote();
    }

    private string _internalemailaddress;
    [XmlElement(ElementName = "internalemailaddress")]
    public string InternalEMailAddress
    {
        get => _internalemailaddress.EscapeSingleQuote();
        set => _internalemailaddress = value.EscapeSingleQuote();
    }

    [Key]
    [XmlElement(ElementName = "systemuserid")]
    public Guid SystemUserId { get; set; }
    [XmlElement(ElementName = "parentsystemuserid")]
    public Parentsystemuserid ParentSystemUserId { get; set; }
    [XmlElement(ElementName = "ctas_partner_role")]
    public Ctas_partner_role ctas_Partner_Role { get; set; }
    [XmlElement(ElementName = "ctas_manager_for_reporting")]
    public Ctas_manager_for_reporting ctas_Manager_for_Reporting { get; set; }
    [XmlElement(ElementName = "ctas_director_for_reporting")]
    public Ctas_director_for_reporting ctas_Director_for_Reporting { get; set; }
    [XmlElement(ElementName = "ctas_business_division_id")]
    public Ctas_business_division_id ctas_Business_Division_ID { get; set; }
    [XmlElement(ElementName = "ctas_sr_vice_president_for_reporting")]
    public Ctas_sr_vice_president_for_reporting ctas_Sr_Vice_President_for_Reporting { get; set; }

    private string _ctas_loc_market;
    [XmlElement(ElementName = "ctas_loc_market")]
    public string ctas_Loc_Market
    {
        get => _ctas_loc_market.EscapeSingleQuote();
        set => _ctas_loc_market = value.EscapeSingleQuote();
    }
    //ChatakonduV E0143633 removed t letter
    [XmlElement(ElementName = "po_cintaslocationid")]
    public Po_CintasLocationId Po_CintasLocationId { get; set; }

    [XmlElement(ElementName = "po_businessdivisionid")]
    public Po_BusinessDivisionId Po_businessdivisionid { get; set; }
    [XmlElement(ElementName = "po_nacsalessupportid")]
    public Po_nacsalessupportid Po_nacsalessupportid { get; set; }
}
//ChatakonduV E0143633 removed t letter
[XmlRoot(ElementName = "po_cintaslocationid")]
public class Po_CintasLocationId : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "parentsystemuserid")]
public class Parentsystemuserid : DynamicsEntityReference
{
    private string _dsc;
    [XmlAttribute(AttributeName = "dsc")]
    public string Dsc
    {
        get => _dsc.EscapeSingleQuote();
        set => _dsc = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_partner_role")]
public class Ctas_partner_role : DynamicsOptionSetRef
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_manager_for_reporting")]
public class Ctas_manager_for_reporting : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_director_for_reporting")]
public class Ctas_director_for_reporting : DynamicsEntityReference
{
    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "ctas_business_division_id")]
public class Ctas_business_division_id : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "ctas_sr_vice_president_for_reporting")]
public class Ctas_sr_vice_president_for_reporting : DynamicsEntityReference
{
    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}


[XmlRoot(ElementName = "po_nacsalessupportid")]
public class Po_nacsalessupportid : DynamicsEntityReference
{
    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "SystemUserResultSet")]
public class SystemUserResultSet
{
    [XmlElement(ElementName = "CustomSystemUser")]
    public List<CustomSystemUser> CustomSystemUser { get; set; }
}

public class JSONSystemUser
{
    public string odataetag { get; set; }
    public string fullname { get; set; }
    public string lastname { get; set; }
    public string firstname { get; set; }
    public string internalemailaddress { get; set; }
    public string systemuserid { get; set; }
    public string _parentsystemuserid_value { get; set; }
    public string _parentsystemuserid_fullname { get; set; }
    public int? ctas_partner_role { get; set; }
    public string _ctas_manager_for_reporting_value { get; set; }
    public string _ctas_manager_for_reporting_fullname { get; set; }
    public string _ctas_director_for_reporting_value { get; set; }
    public string _ctas_director_for_reporting_fullname { get; set; }
    public int? ctas_business_division_id { get; set; }//ChatakonduV E0143633 made this field as nullable type
    public string _ctas_sr_vice_president_for_reporting_value { get; set; }
    public string _ctas_sr_vice_president_for_reporting_fullname { get; set; }
    public string ctas_Loc_Market { get; set; }
    public string _po_cintaslocationid_value { get; set; }
    public string _po_cintaslocationid_po_cintaslocation { get; set; }
    public string _po_businessdivisionid_value { get; set; }
    public string _po_businessdivisionid_po_businessdivision { get; set; }
    public string _po_nacsalessupportid_value { get; set; }
    public string _po_nacsalessupportid_fullname { get; set; }


}