﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "substitutedproductid")]
public class Substitutedproductid 
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "productid")]
public class Productid : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

//ChatakonduV E0143633 Replaced DynamicsOptionSetRef with DynamicsOptionSetRefNoName
[XmlRoot(ElementName = "direction")]
public class Direction : DynamicsOptionSetRefNoName
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

//ChatakonduV E0143633 Replaced DynamicsOptionSetRef with DynamicsOptionSetRefNoName
[XmlRoot(ElementName = "salesrelationshiptype")]
public class Salesrelationshiptype : DynamicsOptionSetRefNoName
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "CustomProductRelationship")]
public class CustomProductRelationship : DynamicsEntityReference
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    [XmlElement(ElementName = "substitutedproductid")]
    public Substitutedproductid Substitutedproductid { get; set; }
    [XmlElement(ElementName = "productid")]
    public Productid Productid { get; set; }
    [XmlElement(ElementName = "direction")]
    public Direction Direction { get; set; }
    [XmlElement(ElementName = "salesrelationshiptype")]
    public Salesrelationshiptype Salesrelationshiptype { get; set; }

    //[Key]
    //[XmlElement(ElementName = "productsubstituteid")]
    //public string Productsubstituteid { get; set; }

    private Guid _productsubstituteid;
    [Key]
    [XmlElement(ElementName = "productsubstituteid")]
    public Guid Productsubstituteid
    {
        get => _productsubstituteid;
        set => _productsubstituteid = value;
    }
}

[XmlRoot(ElementName = "ProductRelationshipResultSet")]
public class ProductRelationshipResultSet
{
    [XmlElement(ElementName = "CustomProductRelationship")]
    public List<CustomProductRelationship> CustomProductRelationship { get; set; }
}

public class JSONProductRelationship
{
    public string odataetag { get; set; }
    public string _substitutedproductid_value { get; set; }
    public string _substitutedproductid_name { get; set; }
    public string _productid_value { get; set; }
    public string _productid_name { get; set; }
    public int? salesrelationshiptype { get; set; }
    public int? direction { get; set; }
    public string productsubstituteid { get; set; }
}