﻿using System.Xml.Serialization;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "ResultSet")]
public class ResultSet
{
    public ResultsD d { get; set; }
}

public class ResultsD
{
    public object results { get; set; }
}

[XmlRoot(ElementName = "DeltaResultSet")]
public class DeltaResultSet : ResultSet
{
    public new DeltaResultsD d { get; set; }
}

public class DeltaResultsD
{
    public object results { get; set; }
    public string __delta { get; set; }
}