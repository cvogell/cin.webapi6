﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

public class CustomLeadReferral
{
    [XmlElement(ElementName = "__metadata")]
    public DeltaMetadataTag Metadata { get; set; }

    private string _address1_city;
    [XmlElement(ElementName = "address1_city")]
    public string Address1_City
    {
        get => _address1_city.EscapeSingleQuote();
        set => _address1_city = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlElement(ElementName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    [Key]
    [XmlElement(ElementName = "accountid")]
    public Guid AccountId { get; set; }

    private string _telephone1;
    [XmlElement(ElementName = "telephone1")]
    public string Telephone1
    {
        get => _telephone1.EscapeSingleQuote();
        set => _address1_city = value.EscapeSingleQuote();
    }

    private string _address1_line1;
    [XmlElement(ElementName = "address1_line1")]
    public string Address1_Line1
    {
        get => _address1_line1.EscapeSingleQuote();
        set => _address1_line1 = value.EscapeSingleQuote();
    }

    private string _address1_stateorprovince;
    [XmlElement(ElementName = "address1_stateorprovince")]
    public string Address1_stateorprovince
    {
        get => _address1_stateorprovince.EscapeSingleQuote();
        set => _address1_stateorprovince = value.EscapeSingleQuote();
    }


    [XmlElement(ElementName = "po_dba")]
    public string po_dba { get; set; }
    [XmlElement(ElementName = "po_naicsdescription")]
    public string po_NaicsDescription { get; set; }
    [XmlElement(ElementName = "po_naicscodeid")]
    public Po_naicscodeid po_NaicsCodeId { get; set; }
    /*        [XmlElement(ElementName = "ctas_NoEmployeesOpt")]
            public ctas_NoEmployeesOpt ctas_noEmployeesOpt { get; set; }
            [XmlElement(ElementName = "ctas_square_footage_opt")]
            public ctas_SquareFootageOpt ctas_squareFootageOpt { get; set; }*/
    [XmlElement(ElementName = "modifiedon")]
    public ModifiedOn ModifiedOn { get; set; }
    [XmlElement(ElementName = "createdon")]
    public CreatedOn CreatedOn { get; set; }
    [XmlElement(ElementName = "po_postalcodeid")]
    public Po_postalcodeid po_Postalcodeid { get; set; }
    [XmlElement(ElementName = "po_custofur")]
    public bool po_custofur { get; set; }
    [XmlElement(ElementName = "po_custoffas")]
    public bool po_custoffas { get; set; }
    [XmlElement(ElementName = "po_custoffs")]
    public bool po_custoffs { get; set; }
    [XmlElement(ElementName = "po_custoffire")]
    public bool po_custoffire { get; set; }
    [XmlElement(ElementName = "po_custofchicagods")]
    public bool po_custofchicagods { get; set; }
    [XmlElement(ElementName = "po_custofcatalog")]
    public bool po_custofcatalog { get; set; }

}

/*    [XmlRoot(ElementName = "po_Dba")]
    public class po_Dba : DynamicsEntityReference
    {
        private string _name;
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get => CommonClass.EscapeSingleQuote(_name);
            set => _name = CommonClass.EscapeSingleQuote(value);
        }

        private string _type;
        [XmlAttribute(AttributeName = "type")]
        public string Type
        {
            get => CommonClass.EscapeSingleQuote(_type);
            set => _type = CommonClass.EscapeSingleQuote(value);
        }

        private string _text;
        [XmlText]
        public string Text
        {
            get => CommonClass.FormatGuidString(_text);
            set => _text = CommonClass.FormatGuidString(value);
        }
    }*/

public class Po_NaicsDescription : DynamicsEntityReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

/*    [XmlRoot(ElementName = "ctas_NoEmployeesOpt")]
    public class ctas_NoEmployeesOpt : DynamicsEntityReference
    {
        private string _name;
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get => CommonClass.EscapeSingleQuote(_name);
            set => _name = CommonClass.EscapeSingleQuote(value);
        }

        private string _type;
        [XmlAttribute(AttributeName = "type")]
        public string Type
        {
            get => CommonClass.EscapeSingleQuote(_type);
            set => _type = CommonClass.EscapeSingleQuote(value);
        }

        private string _text;
        [XmlText]
        public string Text
        {
            get => CommonClass.FormatGuidString(_text);
            set => _text = CommonClass.FormatGuidString(value);
        }
    }

    [XmlRoot(ElementName = "ctas_SquareFootageOpt")]
    public class ctas_SquareFootageOpt : DynamicsEntityReference
    {
        private string _name;
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get => CommonClass.EscapeSingleQuote(_name);
            set => _name = CommonClass.EscapeSingleQuote(value);
        }

        private string _type;
        [XmlAttribute(AttributeName = "type")]
        public string Type
        {
            get => CommonClass.EscapeSingleQuote(_type);
            set => _type = CommonClass.EscapeSingleQuote(value);
        }

        private string _text;
        [XmlText]
        public string Text
        {
            get => CommonClass.FormatGuidString(_text);
            set => _text = CommonClass.FormatGuidString(value);
        }
    }*/

[XmlRoot(ElementName = "LeadReferralResultSet")]
public class LeadReferralResultSet
{
    [XmlElement(ElementName = "CustomLeadReferral")]
    public List<CustomLeadReferral> CustomLeadReferral { get; set; }
}
