﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Common;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;


[XmlRoot(ElementName = "po_customertoaccount")]
public class Po_CustomerToAccount : DynamicsEntityNullReference
{
    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}

[XmlRoot(ElementName = "po_currentrtenum")]
public class Po_CurrentRteNum : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "po_contractnumber")]
public class Po_ContractNumber : DynamicsFormattedValue
{
    private string _formattedvalue;
    [XmlAttribute(AttributeName = "formattedvalue")]
    public string Formattedvalue
    {
        get => _formattedvalue.EscapeSingleQuote();
        set => _formattedvalue = value.EscapeSingleQuote();
    }

    private string _text;
    [XmlText]
    public string Text
    {
        get => _text.EscapeSingleQuote();
        set => _text = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "CustomCustomerDetails")]
public class CustomCustomerDetails
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    private string _po_name;
    [XmlElement(ElementName = "po_name")]
    public string Po_Name
    {
        get => _po_name.EscapeSingleQuote();
        set => _po_name = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_customertoaccount")]
    public Po_CustomerToAccount Po_CustomerToAccount { get; set; }

    private string _po_customernumber;
    [XmlAttribute(AttributeName = "po_customernumber")]
    public string Po_CustomerNumber
    {
        get => _po_customernumber.EscapeSingleQuote();
        set => _po_customernumber = value.EscapeSingleQuote();
    }


    private string _po_customername;
    [XmlElement(ElementName = "po_customername")]
    public string Po_CustomerName
    {
        get => _po_customername.EscapeSingleQuote();
        set => _po_customername = value.EscapeSingleQuote();
    }

    private string _po_customer_active;
    [XmlElement(ElementName = "po_customer_active")]
    public string Po_Customer_Active
    {
        get => _po_customer_active.EscapeSingleQuote();
        set => _po_customer_active = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_currentrtenum")]
    public Po_CurrentRteNum Po_CurrentRTENum { get; set; }
    [XmlElement(ElementName = "po_contractnumber")]
    public Po_ContractNumber Po_ContractNumber { get; set; }
    [XmlElement(ElementName = "po_customerid")]
    [Key]
    public Guid Po_CustomerId { get; set; }
    private string _po_corporatecode;
    [XmlElement(ElementName = "po_corporatecode")]
    public string Po_CorporateCode
    {
        get => _po_corporatecode.EscapeSingleQuote();
        set => _po_corporatecode = value.EscapeSingleQuote();
    }

    private string _po_mlacode;
    [XmlElement(ElementName = "po_mlacode")]
    public string Po_MLACode
    {
        get => _po_mlacode.EscapeSingleQuote();
        set => _po_mlacode = value.EscapeSingleQuote();
    }

    private string _ctas_sap_id;
    [XmlElement(ElementName = "ctas_sap_id")]
    public string Ctas_SAP_Id
    {
        get => _ctas_sap_id.EscapeSingleQuote();
        set => _ctas_sap_id = value.EscapeSingleQuote();
    }

    [XmlElement(ElementName = "po_businessdivisionid")]
    public Po_BusinessDivisionId Po_BusinessDivisionId { get; set; }
    [XmlElement(ElementName = "po_fklocno")]
    public Po_BusinessDivisionId Po_FkLocNo { get; set; }
    [XmlElement(ElementName = "po_cintaslocationid")]
    public Po_CintasLocationId Po_CintasLocationId { get; set; }
}

[XmlRoot(ElementName = "CustomerDetailsResultSet")]
public class CustomerDetailsResultSet
{
    [XmlElement(ElementName = "CustomCustomerDetails")]
    public List<CustomCustomerDetails> CustomCustomerDetails { get; set; }
}
public class JSONCustomerDetails
{
    public string odataetag { get; set; }
    public string po_name { get; set; }
    public string po_customertoaccount { get; set; }
    public string _po_customertoaccount_value { get; set; }
    public string _po_customertoaccount_name { get; set; }
    public string po_customernumber { get; set; }
    public string po_customername { get; set; }
    public string po_customer_active { get; set; }
    public string po_currentrtenum { get; set; }
    public string po_contractnumber { get; set; }
    public string po_customerid { get; set; }
    public string po_corporatecode { get; set; }
    public string po_mlacode { get; set; }
    public string ctas_sap_id { get; set; }
    public string _po_businessdivisionid_value { get; set; }
    public string _po_businessdivisionid_po_businessdivision { get; set; }
    public string po_fklocno { get; set; }
    public string _po_cintaslocationid_value { get; set; }
    public string _po_cintaslocationid_po_cintaslocation { get; set; }
}
