﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomEntityIdentification")]
public class CustomEntityIdentification
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag Metadata { get; set; }

    [Key]
    public object KeyValue
    {
        get;
        set;
    }

    public string DynamicsEntity
    {
        get;
        set;
    }

    public string KeyField
    {
        get;
        set;
    }
    public string LookupField
    {
        get;
        set;
    }
}

[XmlRoot(ElementName = "CustomEntityReference")]
public class CustomEntityReference
{
    private string _yomi;
    [XmlAttribute(AttributeName = "yomi")]
    public string Yomi
    {
        get => _yomi.EscapeSingleQuote();
        set => _yomi = value.EscapeSingleQuote();
    }

    private string _name;
    [XmlAttribute(AttributeName = "name")]
    public string Name
    {
        get => _name.EscapeSingleQuote();
        set => _name = value.EscapeSingleQuote();
    }

    private string _type;
    [XmlAttribute(AttributeName = "type")]
    public string Type
    {
        get => _type.EscapeSingleQuote();
        set => _type = value.EscapeSingleQuote();
    }

    private string? _text;
    [XmlText]
    public string? Text
    {
        get => _text?.FormatGuidString();
        set => _text = value?.FormatGuidString();
    }
}