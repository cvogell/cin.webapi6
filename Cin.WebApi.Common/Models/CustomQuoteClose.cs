﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Cin.WebApi.Extensions;

namespace Cin.WebApi.Models;

[XmlRoot(ElementName = "CustomQuoteDetail")]
public class CustomQuoteClose
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }
    [Key]
    [XmlElement(ElementName = "quotecloseid")]
    public Guid QuoteCloseId { get; set; }

    [XmlElement(ElementName = "quoteid")]
    public Guid? QuoteId
    {
        get;
        set;
    }

    private string _subject;
    [XmlElement(ElementName = "subject")]
    public string Subject
    {
        get => _subject.EscapeSingleQuote();
        set => _subject = value.EscapeSingleQuote();
    }
}

[XmlRoot(ElementName = "QuoteCloseResultSet")]
public class QuoteCloseResultSet
{
    [XmlElement(ElementName = "CustomQuoteClose")]
    public List<CustomQuoteClose> CustomQuoteClose { get; set; }
}

[XmlRoot(ElementName = "CustomCancelQuote")]
public class CustomCancelQuote
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    public Guid Id
    {
        get;
        set;
    }

    public Guid QuoteId
    {
        get;
        set;
    }
}

[XmlRoot(ElementName = "CustomLoseQuote")]
public class CustomLoseQuote
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    public Guid Id
    {
        get;
        set;
    }

    public Guid QuoteId
    {
        get;
        set;
    }
}

[XmlRoot(ElementName = "CustomWinQuote")]
public class CustomWinQuote
{
    [XmlElement(ElementName = "__metadata")]
    public MetadataTag __metadata { get; set; }

    public Guid Id
    {
        get;
        set;
    }

    public Guid QuoteId
    {
        get;
        set;
    }
}