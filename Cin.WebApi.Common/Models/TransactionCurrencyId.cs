﻿using Cin.WebApi.Common;
using Cin.WebApi.Extensions;
using System.Xml.Serialization;

namespace Cin.WebApi.Models
{
    //ChatakonduV E0143633 modified below line for class ref from DynamicsEntityReference to DynamicsEntityNullReference

    [XmlRoot(ElementName = "transactioncurrencyid")]
    public class TransactionCurrencyId : DynamicsEntityNullReference
    {
        private string _dsc;
        [XmlAttribute(AttributeName = "dsc")]
        public string Dsc
        {
            get => _dsc.EscapeSingleQuote();
            set => _dsc = value.EscapeSingleQuote();
        }

        private string _name;
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get => _name.EscapeSingleQuote();
            set => _name = value.EscapeSingleQuote();
        }

        private string _type;
        [XmlAttribute(AttributeName = "type")]
        public string Type
        {
            get => _type.EscapeSingleQuote();
            set => _type = value.EscapeSingleQuote();
        }

        private string? _text;
        [XmlText]
        public string? Text
        {
            get => _text?.FormatGuidString();
            set => _text = value?.FormatGuidString();
        }
    }
}