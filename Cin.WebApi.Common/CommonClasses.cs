﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cin.WebApi.Common;
public class DynamicsBooleanValue { }
public class DynamicsFormattedValue { }
public class DynamicsFormattedValueOnly { }

//ChatakonduV E0143633 Added to format numeric numbers
public class DynamicsFormattedNumericValue {    }

public class DynamicsOptionSetRef { }
public class DynamicsOptionSetRefBoolean { }
//ChatakonduV E0143633 Added mainly for productrelationshipcontroller
public class DynamicsOptionSetRefNoName { }

public class DynamicsEntityReference { }
public class DynamicsVariantEntityReference { }
public class DynamicsEntityNullReference { }

public class DynamicsDateTimeValue { }
public class DynamicsDateTimeLocalValue { }
public class DynamicsDateTimeUTCValue { }

public class DynamicsDateTimeGMTValue { }

public class DynamicsCurrencyValue { }
public class DynamicsIntValue { }
