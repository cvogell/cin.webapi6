﻿namespace Cin.WebApi;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

public class JsonEntityAttribute
{
    public string Key { get; set; }
    public object Value { get; set; }
}

public class JsonEntityFormattedValue
{
    public string Key { get; set; }
    public string Value { get; set; }
}

public class JsonEntity
{
    public string LogicalName { get; set; }
    public string Id { get; set; }
    public List<JsonEntityAttribute> Attributes { get; set; }
    public List<JsonEntityFormattedValue> FormattedValues { get; set; }
}

public class JsonEntityAttributeEnumValue
{
    public long Value { get; set; }
}

public class JsonEntityAttributeBooleanValue
{
    public int Value { get; set; }

    public string StringValue { get => (Value.ToString()); }

    public bool BooleanValue { get => (Value == 1 ? true : false); }
}

public class JsonEntityAttributeGuidValue
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}

public class JsonEntityAttributeNumericValue
{
    public int Value { get; set; }
}

public class JsonEntityAttributeFloatValue
{
    public float Value { get; set; }
}
