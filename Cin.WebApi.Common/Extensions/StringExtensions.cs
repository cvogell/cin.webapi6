﻿namespace Cin.WebApi.Extensions;

public static class StringExtensions
{
    public static string EscapeSingleQuote(this string value)
    {
        if (string.IsNullOrEmpty(value)) return value;
        try
        {
            return value.Replace('\u2019', '\'').Replace('\u201c', '"').Replace('\u201d', '"');
        }
        catch { return value; }
    }

    public static string? FormatGuidString(this string guid)
    {
        if (string.IsNullOrEmpty (guid)) return guid;
        try
        {
            guid = guid.Replace("{", string.Empty).Replace("}", string.Empty);
            if (string.IsNullOrEmpty(guid)) return null;
            var g = new Guid(guid);
            return g.ToString("D").ToLowerInvariant();
        }catch { return null; }
    }

    public static bool ToBool(this string value)
    {
        if (value == "0") return false;
        if (value == "1") return true;

        return Convert.ToBoolean(value);

    }
}
