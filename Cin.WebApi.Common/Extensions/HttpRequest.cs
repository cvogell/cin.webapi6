﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Cin.WebApi.Extensions;
public static class HttpRequestExtensions
{
    public static string GetAuthToken(this HttpRequest request)
    {
        if ((request.Headers.ContainsKey("dynamics")) && !string.IsNullOrEmpty(request.Headers["dynamics"].ToString()))
        {
            IEnumerable<string> headerValues = request.Headers["dynamics"];
            var id = headerValues.FirstOrDefault();
            string token = id.ToString();
            return token;
        }
        else if ((!request.Headers.ContainsKey("Authorization")) ||
            StringValues.IsNullOrEmpty(request.Headers.Authorization))
        {
            return string.Empty;
        }
        else
        {
            string token = request.Headers.Authorization.ToString();
            return token;
        }

    }

}
