﻿using Microsoft.Xrm.Sdk;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;


[DataContract()]
[System.CodeDom.Compiler.GeneratedCode("CrmSvcUtil", "8.2.1.8676")]
public enum ProductState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,

    [EnumMember()]
    Draft = 2,

    [EnumMember()]
    UnderRevision = 3,
}


[DataContract()]
[Microsoft.Xrm.Sdk.Client.EntityLogicalName("product")]

public class Product : Entity
{



    /// <summary>
    /// Unique identifier of the user who created the product.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Shows the external party who created the record.
    /// </summary>
    [AttributeLogicalName("createdbyexternalparty")]
    [DataMember]
    public EntityReference CreatedByExternalParty
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdbyexternalparty");
        }
    }

    /// <summary>
    /// Date and time when the product was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the product.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Current cost for the product item. Used in price calculations.
    /// </summary>
    [AttributeLogicalName("currentcost")]
    [DataMember]
    public Money CurrentCost
    {
        get
        {
            return GetAttributeValue<Money>("currentcost");
        }
        set
        {
            SetAttributeValue("currentcost", value);
        }
    }

    /// <summary>
    /// Base currency equivalent of the current cost for the product item.
    /// </summary>
    [AttributeLogicalName("currentcost_base")]
    [DataMember]
    public Money CurrentCost_Base
    {
        get
        {
            return GetAttributeValue<Money>("currentcost_base");
        }
    }

    /// <summary>
    /// Default unit for the product.
    /// </summary>
    [AttributeLogicalName("defaultuomid")]
    [DataMember]
    public EntityReference DefaultUoMId
    {
        get
        {
            return GetAttributeValue<EntityReference>("defaultuomid");
        }
        set
        {
            SetAttributeValue("defaultuomid", value);
        }
    }

    /// <summary>
    /// Default unit group for the product.
    /// </summary>
    [AttributeLogicalName("defaultuomscheduleid")]
    [DataMember]
    public EntityReference DefaultUoMScheduleId
    {
        get
        {
            return GetAttributeValue<EntityReference>("defaultuomscheduleid");
        }
        set
        {
            SetAttributeValue("defaultuomscheduleid", value);
        }
    }

    /// <summary>
    /// Description of the product.
    /// </summary>
    [AttributeLogicalName("description")]
    [DataMember]
    public string Description
    {
        get
        {
            return GetAttributeValue<string>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /// <summary>
    /// Internal Use Only
    /// </summary>
    [AttributeLogicalName("dmtimportstate")]
    [DataMember]
    public int? DMTImportState
    {
        get
        {
            return GetAttributeValue<int?>("dmtimportstate");
        }
        set
        {
            SetAttributeValue("dmtimportstate", value);
        }
    }

    /// <summary>
    /// Shows the default image for the record.
    /// </summary>
    [AttributeLogicalName("entityimage")]
    [DataMember]
    public byte[] EntityImage
    {
        get
        {
            return GetAttributeValue<byte[]>("entityimage");
        }
        set
        {
            SetAttributeValue("entityimage", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_timestamp")]
    [DataMember]
    public long? EntityImage_Timestamp
    {
        get
        {
            return GetAttributeValue<long?>("entityimage_timestamp");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_url")]
    [DataMember]
    public string EntityImage_URL
    {
        get
        {
            return GetAttributeValue<string>("entityimage_url");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("entityimageid")]
    [DataMember]
    public Guid? EntityImageId
    {
        get
        {
            return GetAttributeValue<Guid?>("entityimageid");
        }
    }

    /// <summary>
    /// Exchange rate for the currency associated with the product with respect to the base currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Hierarchy path of the product.
    /// </summary>
    [AttributeLogicalName("hierarchypath")]
    [DataMember]
    public string HierarchyPath
    {
        get
        {
            return GetAttributeValue<string>("hierarchypath");
        }
    }

    /// <summary>
    /// Unique identifier of the data import or data migration that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Information that specifies whether the product is a kit.
    /// </summary>
    [AttributeLogicalName("iskit")]
    [DataMember]
    public bool? IsKit
    {
        get
        {
            return GetAttributeValue<bool?>("iskit");
        }
        set
        {
            SetAttributeValue("iskit", value);
        }
    }

    /// <summary>
    /// Information about whether the product is a stock item.
    /// </summary>
    [AttributeLogicalName("isstockitem")]
    [DataMember]
    public bool? IsStockItem
    {
        get
        {
            return GetAttributeValue<bool?>("isstockitem");
        }
        set
        {
            SetAttributeValue("isstockitem", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who last modified the product.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Shows the external party who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedbyexternalparty")]
    [DataMember]
    public EntityReference ModifiedByExternalParty
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedbyexternalparty");
        }
    }

    /// <summary>
    /// Date and time when the product was last modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who last modified the product.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Name of the product.
    /// </summary>
    [AttributeLogicalName("name")]
    [DataMember]
    public string Name
    {
        get
        {
            return GetAttributeValue<string>("name");
        }
        set
        {
            SetAttributeValue("name", value);
        }
    }

    /// <summary>
    /// Unique identifier of the organization associated with the product.
    /// </summary>
    [AttributeLogicalName("organizationid")]
    [DataMember]
    public EntityReference OrganizationId
    {
        get
        {
            return GetAttributeValue<EntityReference>("organizationid");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Specifies the parent product family hierarchy.
    /// </summary>
    [AttributeLogicalName("parentproductid")]
    [DataMember]
    public EntityReference ParentProductId
    {
        get
        {
            return GetAttributeValue<EntityReference>("parentproductid");
        }
        set
        {
            SetAttributeValue("parentproductid", value);
        }
    }

    /// <summary>
    /// List price of the product.
    /// </summary>
    [AttributeLogicalName("price")]
    [DataMember]
    public Money Price
    {
        get
        {
            return GetAttributeValue<Money>("price");
        }
        set
        {
            SetAttributeValue("price", value);
        }
    }

    /// <summary>
    /// Base currency equivalent of the list price of the product
    /// </summary>
    [AttributeLogicalName("price_base")]
    [DataMember]
    public Money Price_Base
    {
        get
        {
            return GetAttributeValue<Money>("price_base");
        }
    }

    /// <summary>
    /// Select the default price list for the product.
    /// </summary>
    [AttributeLogicalName("pricelevelid")]
    [DataMember]
    public EntityReference PriceLevelId
    {
        get
        {
            return GetAttributeValue<EntityReference>("pricelevelid");
        }
        set
        {
            SetAttributeValue("pricelevelid", value);
        }
    }

    /// <summary>
    /// Shows the ID of the process.
    /// </summary>
    [AttributeLogicalName("processid")]
    [DataMember]
    public Guid? ProcessId
    {
        get
        {
            return GetAttributeValue<Guid?>("processid");
        }
        set
        {
            SetAttributeValue("processid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the product.
    /// </summary>
    [AttributeLogicalName("productid")]
    [DataMember]
    public Guid? ProductId
    {
        get
        {
            return GetAttributeValue<Guid?>("productid");
        }
        set
        {
            SetAttributeValue("productid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("productid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            ProductId = value;
        }
    }

    /// <summary>
    /// User-defined product ID.
    /// </summary>
    [AttributeLogicalName("productnumber")]
    [DataMember]
    public string ProductNumber
    {
        get
        {
            return GetAttributeValue<string>("productnumber");
        }
        set
        {
            SetAttributeValue("productnumber", value);
        }
    }

    /// <summary>
    /// Product Structure.
    /// </summary>
    [AttributeLogicalName("productstructure")]
    [DataMember]
    public OptionSetValue ProductStructure
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("productstructure");
        }
        set
        {
            SetAttributeValue("productstructure", value);
        }
    }

    /// <summary>
    /// Type of product.
    /// </summary>
    [AttributeLogicalName("producttypecode")]
    [DataMember]
    public OptionSetValue ProductTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("producttypecode");
        }
        set
        {
            SetAttributeValue("producttypecode", value);
        }
    }

    /// <summary>
    /// URL for the Website associated with the product.
    /// </summary>
    [AttributeLogicalName("producturl")]
    [DataMember]
    public string ProductUrl
    {
        get
        {
            return GetAttributeValue<string>("producturl");
        }
        set
        {
            SetAttributeValue("producturl", value);
        }
    }

    /// <summary>
    /// Number of decimal places that can be used in monetary amounts for the product.
    /// </summary>
    [AttributeLogicalName("quantitydecimal")]
    [DataMember]
    public int? QuantityDecimal
    {
        get
        {
            return GetAttributeValue<int?>("quantitydecimal");
        }
        set
        {
            SetAttributeValue("quantitydecimal", value);
        }
    }

    /// <summary>
    /// Quantity of the product in stock.
    /// </summary>
    [AttributeLogicalName("quantityonhand")]
    [DataMember]
    public decimal? QuantityOnHand
    {
        get
        {
            return GetAttributeValue<decimal?>("quantityonhand");
        }
        set
        {
            SetAttributeValue("quantityonhand", value);
        }
    }

    /// <summary>
    /// Product size.
    /// </summary>
    [AttributeLogicalName("size")]
    [DataMember]
    public string Size
    {
        get
        {
            return GetAttributeValue<string>("size");
        }
        set
        {
            SetAttributeValue("size", value);
        }
    }

    /// <summary>
    /// Shows the ID of the stage.
    /// </summary>
    [AttributeLogicalName("stageid")]
    [DataMember]
    public Guid? StageId
    {
        get
        {
            return GetAttributeValue<Guid?>("stageid");
        }
        set
        {
            SetAttributeValue("stageid", value);
        }
    }

    /// <summary>
    /// Standard cost of the product.
    /// </summary>
    [AttributeLogicalName("standardcost")]
    [DataMember]
    public Money StandardCost
    {
        get
        {
            return GetAttributeValue<Money>("standardcost");
        }
        set
        {
            SetAttributeValue("standardcost", value);
        }
    }

    /// <summary>
    /// Base currency equivalent of the standard cost of the product.
    /// </summary>
    [AttributeLogicalName("standardcost_base")]
    [DataMember]
    public Money StandardCost_Base
    {
        get
        {
            return GetAttributeValue<Money>("standardcost_base");
        }
    }

    /// <summary>
    /// Status of the product.
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public ProductState? StateCode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (ProductState)Enum.ToObject(typeof(ProductState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the product.
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// Stock volume of the product.
    /// </summary>
    [AttributeLogicalName("stockvolume")]
    [DataMember]
    public decimal? StockVolume
    {
        get
        {
            return GetAttributeValue<decimal?>("stockvolume");
        }
        set
        {
            SetAttributeValue("stockvolume", value);
        }
    }

    /// <summary>
    /// Stock weight of the product.
    /// </summary>
    [AttributeLogicalName("stockweight")]
    [DataMember]
    public decimal? StockWeight
    {
        get
        {
            return GetAttributeValue<decimal?>("stockweight");
        }
        set
        {
            SetAttributeValue("stockweight", value);
        }
    }

    /// <summary>
    /// Select a category for the product.
    /// </summary>
    [AttributeLogicalName("subjectid")]
    [DataMember]
    public EntityReference SubjectId
    {
        get
        {
            return GetAttributeValue<EntityReference>("subjectid");
        }
        set
        {
            SetAttributeValue("subjectid", value);
        }
    }

    /// <summary>
    /// Name of the product's supplier.
    /// </summary>
    [AttributeLogicalName("suppliername")]
    [DataMember]
    public string SupplierName
    {
        get
        {
            return GetAttributeValue<string>("suppliername");
        }
        set
        {
            SetAttributeValue("suppliername", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the currency associated with the product.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("traversedpath")]
    [DataMember]
    public string TraversedPath
    {
        get
        {
            return GetAttributeValue<string>("traversedpath");
        }
        set
        {
            SetAttributeValue("traversedpath", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Date from which this product is valid.
    /// </summary>
    [AttributeLogicalName("validfromdate")]
    [DataMember]
    public DateTime? ValidFromDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("validfromdate");
        }
        set
        {
            SetAttributeValue("validfromdate", value);
        }
    }

    /// <summary>
    /// Date to which this product is valid.
    /// </summary>
    [AttributeLogicalName("validtodate")]
    [DataMember]
    public DateTime? ValidToDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("validtodate");
        }
        set
        {
            SetAttributeValue("validtodate", value);
        }
    }

    /// <summary>
    /// Unique identifier of vendor supplying the product.
    /// </summary>
    [AttributeLogicalName("vendorid")]
    [DataMember]
    public string VendorID
    {
        get
        {
            return GetAttributeValue<string>("vendorid");
        }
        set
        {
            SetAttributeValue("vendorid", value);
        }
    }

    /// <summary>
    /// Name of the product vendor.
    /// </summary>
    [AttributeLogicalName("vendorname")]
    [DataMember]
    public string VendorName
    {
        get
        {
            return GetAttributeValue<string>("vendorname");
        }
        set
        {
            SetAttributeValue("vendorname", value);
        }
    }

    /// <summary>
    /// Unique part identifier in vendor catalog of this product.
    /// </summary>
    [AttributeLogicalName("vendorpartnumber")]
    [DataMember]
    public string VendorPartNumber
    {
        get
        {
            return GetAttributeValue<string>("vendorpartnumber");
        }
        set
        {
            SetAttributeValue("vendorpartnumber", value);
        }
    }

    /// <summary>
    /// Version number of the product.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }

    /// <summary>
    /// 1:N opportunity_products
    /// </summary>
    [RelationshipSchemaName("opportunity_products")]
    [DataMember]
    public IEnumerable<OpportunityProduct> opportunity_products
    {
        get
        {
            return GetRelatedEntities<OpportunityProduct>("opportunity_products", null);
        }
        set
        {
            SetRelatedEntities("opportunity_products", null, value);
        }
    }


}