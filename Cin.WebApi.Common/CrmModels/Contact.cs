﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("contact")]
public class Contact : Entity
{

    /// <summary>
    /// Unique identifier of the account with which the contact is associated.
    /// </summary>       

    [AttributeLogicalName("accountid")]
    [DataMember]
    public EntityReference AccountId
    {
        get
        {
            return GetAttributeValue<EntityReference>("accountid");
        }
    }

    /// <summary>
    /// Select the contact's role within the company or sales process, such as decision maker, employee, or influencer.
    /// </summary>
    [AttributeLogicalName("accountrolecode")]
    [DataMember]
    public OptionSetValue AccountRoleCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("accountrolecode");
        }
        set
        {
            SetAttributeValue("accountrolecode", value);
        }
    }

    /// <summary>
    /// Unique identifier for address 1.
    /// </summary>
    [AttributeLogicalName("address1_addressid")]
    [DataMember]
    public Guid? Address1_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("address1_addressid");
        }
        set
        {
            SetAttributeValue("address1_addressid", value);
        }
    }

    /// <summary>
    /// Select the primary address type.
    /// </summary>
    [AttributeLogicalName("address1_addresstypecode")]
    [DataMember]
    public OptionSetValue Address1_AddressTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address1_addresstypecode");
        }
        set
        {
            SetAttributeValue("address1_addresstypecode", value);
        }
    }

    /// <summary>
    /// City name for address 1.
    /// </summary>
    [AttributeLogicalName("address1_city")]
    [DataMember]
    public string Address1_City
    {
        get
        {
            return GetAttributeValue<string>("address1_city");
        }
        set
        {
            SetAttributeValue("address1_city", value);
        }
    }

    /// <summary>
    /// Shows the complete primary address.
    /// </summary>
    [AttributeLogicalName("address1_composite")]
    [DataMember]
    public string Address1_Composite
    {
        get
        {
            return GetAttributeValue<string>("address1_composite");
        }
    }

    /// <summary>
    /// Country/region name for address 1.
    /// </summary>
    [AttributeLogicalName("address1_country")]
    [DataMember]
    public string Address1_Country
    {
        get
        {
            return GetAttributeValue<string>("address1_country");
        }
        set
        {
            SetAttributeValue("address1_country", value);
        }
    }

    /// <summary>
    /// Type the county for the primary address.
    /// </summary>
    [AttributeLogicalName("address1_county")]
    [DataMember]
    public string Address1_County
    {
        get
        {
            return GetAttributeValue<string>("address1_county");
        }
        set
        {
            SetAttributeValue("address1_county", value);
        }
    }

    /// <summary>
    /// Type the fax number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address1_fax")]
    [DataMember]
    public string Address1_Fax
    {
        get
        {
            return GetAttributeValue<string>("address1_fax");
        }
        set
        {
            SetAttributeValue("address1_fax", value);
        }
    }

    /// <summary>
    /// Select the freight terms for the primary address to make sure shipping orders are processed correctly.
    /// </summary>
    [AttributeLogicalName("address1_freighttermscode")]
    [DataMember]
    public OptionSetValue Address1_FreightTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address1_freighttermscode");
        }
        set
        {
            SetAttributeValue("address1_freighttermscode", value);
        }
    }

    /// <summary>
    /// Latitude for address 1.
    /// </summary>
    [AttributeLogicalName("address1_latitude")]
    [DataMember]
    public double? Address1_Latitude
    {
        get
        {
            return GetAttributeValue<double?>("address1_latitude");
        }
        set
        {
            SetAttributeValue("address1_latitude", value);
        }
    }

    /// <summary>
    /// First line for entering address 1 information.
    /// </summary>
    [AttributeLogicalName("address1_line1")]
    [DataMember]
    public string Address1_Line1
    {
        get
        {
            return GetAttributeValue<string>("address1_line1");
        }
        set
        {
            SetAttributeValue("address1_line1", value);
        }
    }

    /// <summary>
    /// Second line for entering address 1 information.
    /// </summary>
    [AttributeLogicalName("address1_line2")]
    [DataMember]
    public string Address1_Line2
    {
        get
        {
            return GetAttributeValue<string>("address1_line2");
        }
        set
        {
            SetAttributeValue("address1_line2", value);
        }
    }

    /// <summary>
    /// Type the third line of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_line3")]
    [DataMember]
    public string Address1_Line3
    {
        get
        {
            return GetAttributeValue<string>("address1_line3");
        }
        set
        {
            SetAttributeValue("address1_line3", value);
        }
    }

    /// <summary>
    /// Longitude for address 1.
    /// </summary>
    [AttributeLogicalName("address1_longitude")]
    [DataMember]
    public double? Address1_Longitude
    {
        get
        {
            return GetAttributeValue<double?>("address1_longitude");
        }
        set
        {
            SetAttributeValue("address1_longitude", value);
        }
    }

    /// <summary>
    /// Name to enter for address 1.
    /// </summary>
    [AttributeLogicalName("address1_name")]
    [DataMember]
    public string Address1_Name
    {
        get
        {
            return GetAttributeValue<string>("address1_name");
        }
        set
        {
            SetAttributeValue("address1_name", value);
        }
    }

    /// <summary>
    /// ZIP Code or postal code for address 1.
    /// </summary>
    [AttributeLogicalName("address1_postalcode")]
    [DataMember]
    public string Address1_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("address1_postalcode");
        }
        set
        {
            SetAttributeValue("address1_postalcode", value);
        }
    }

    /// <summary>
    /// Type the post office box number of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_postofficebox")]
    [DataMember]
    public string Address1_PostOfficeBox
    {
        get
        {
            return GetAttributeValue<string>("address1_postofficebox");
        }
        set
        {
            SetAttributeValue("address1_postofficebox", value);
        }
    }

    /// <summary>
    /// Type the name of the main contact at the account's primary address.
    /// </summary>
    [AttributeLogicalName("address1_primarycontactname")]
    [DataMember]
    public string Address1_PrimaryContactName
    {
        get
        {
            return GetAttributeValue<string>("address1_primarycontactname");
        }
        set
        {
            SetAttributeValue("address1_primarycontactname", value);
        }
    }

    /// <summary>
    /// Select a shipping method for deliveries sent to this address.
    /// </summary>
    [AttributeLogicalName("address1_shippingmethodcode")]
    [DataMember]
    public OptionSetValue Address1_ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address1_shippingmethodcode");
        }
        set
        {
            SetAttributeValue("address1_shippingmethodcode", value);
        }
    }

    /// <summary>
    /// State or province for address 1.
    /// </summary>
    [AttributeLogicalName("address1_stateorprovince")]
    [DataMember]
    public string Address1_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("address1_stateorprovince");
        }
        set
        {
            SetAttributeValue("address1_stateorprovince", value);
        }
    }

    /// <summary>
    /// Type the main phone number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address1_telephone1")]
    [DataMember]
    public string Address1_Telephone1
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone1");
        }
        set
        {
            SetAttributeValue("address1_telephone1", value);
        }
    }

    /// <summary>
    /// Type a second phone number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address1_telephone2")]
    [DataMember]
    public string Address1_Telephone2
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone2");
        }
        set
        {
            SetAttributeValue("address1_telephone2", value);
        }
    }

    /// <summary>
    /// Type a third phone number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address1_telephone3")]
    [DataMember]
    public string Address1_Telephone3
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone3");
        }
        set
        {
            SetAttributeValue("address1_telephone3", value);
        }
    }

    /// <summary>
    /// Type the UPS zone of the primary address to make sure shipping charges are calculated correctly and deliveries are made promptly, if shipped by UPS.
    /// </summary>
    [AttributeLogicalName("address1_upszone")]
    [DataMember]
    public string Address1_UPSZone
    {
        get
        {
            return GetAttributeValue<string>("address1_upszone");
        }
        set
        {
            SetAttributeValue("address1_upszone", value);
        }
    }

    /// <summary>
    /// Select the time zone, or UTC offset, for this address so that other people can reference it when they contact someone at this address.
    /// </summary>
    [AttributeLogicalName("address1_utcoffset")]
    [DataMember]
    public int? Address1_UTCOffset
    {
        get
        {
            return GetAttributeValue<int?>("address1_utcoffset");
        }
        set
        {
            SetAttributeValue("address1_utcoffset", value);
        }
    }

    /// <summary>
    /// Unique identifier for address 2.
    /// </summary>
    [AttributeLogicalName("address2_addressid")]
    [DataMember]
    public Guid? Address2_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("address2_addressid");
        }
        set
        {
            SetAttributeValue("address2_addressid", value);
        }
    }

    /// <summary>
    /// Select the secondary address type.
    /// </summary>
    [AttributeLogicalName("address2_addresstypecode")]
    [DataMember]
    public OptionSetValue Address2_AddressTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address2_addresstypecode");
        }
        set
        {
            SetAttributeValue("address2_addresstypecode", value);
        }
    }

    /// <summary>
    /// Type the city for the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_city")]
    [DataMember]
    public string Address2_City
    {
        get
        {
            return GetAttributeValue<string>("address2_city");
        }
        set
        {
            SetAttributeValue("address2_city", value);
        }
    }

    /// <summary>
    /// Shows the complete secondary address.
    /// </summary>
    [AttributeLogicalName("address2_composite")]
    [DataMember]
    public string Address2_Composite
    {
        get
        {
            return GetAttributeValue<string>("address2_composite");
        }
    }

    /// <summary>
    /// Type the country or region for the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_country")]
    [DataMember]
    public string Address2_Country
    {
        get
        {
            return GetAttributeValue<string>("address2_country");
        }
        set
        {
            SetAttributeValue("address2_country", value);
        }
    }

    /// <summary>
    /// Type the county for the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_county")]
    [DataMember]
    public string Address2_County
    {
        get
        {
            return GetAttributeValue<string>("address2_county");
        }
        set
        {
            SetAttributeValue("address2_county", value);
        }
    }

    /// <summary>
    /// Type the fax number associated with the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_fax")]
    [DataMember]
    public string Address2_Fax
    {
        get
        {
            return GetAttributeValue<string>("address2_fax");
        }
        set
        {
            SetAttributeValue("address2_fax", value);
        }
    }

    /// <summary>
    /// Select the freight terms for the secondary address to make sure shipping orders are processed correctly.
    /// </summary>
    [AttributeLogicalName("address2_freighttermscode")]
    [DataMember]
    public OptionSetValue Address2_FreightTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address2_freighttermscode");
        }
        set
        {
            SetAttributeValue("address2_freighttermscode", value);
        }
    }

    /// <summary>
    /// Type the latitude value for the secondary address for use in mapping and other applications.
    /// </summary>
    [AttributeLogicalName("address2_latitude")]
    [DataMember]
    public double? Address2_Latitude
    {
        get
        {
            return GetAttributeValue<double?>("address2_latitude");
        }
        set
        {
            SetAttributeValue("address2_latitude", value);
        }
    }

    /// <summary>
    /// Type the first line of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_line1")]
    [DataMember]
    public string Address2_Line1
    {
        get
        {
            return GetAttributeValue<string>("address2_line1");
        }
        set
        {
            SetAttributeValue("address2_line1", value);
        }
    }

    /// <summary>
    /// Type the second line of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_line2")]
    [DataMember]
    public string Address2_Line2
    {
        get
        {
            return GetAttributeValue<string>("address2_line2");
        }
        set
        {
            SetAttributeValue("address2_line2", value);
        }
    }

    /// <summary>
    /// Type the third line of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_line3")]
    [DataMember]
    public string Address2_Line3
    {
        get
        {
            return GetAttributeValue<string>("address2_line3");
        }
        set
        {
            SetAttributeValue("address2_line3", value);
        }
    }

    /// <summary>
    /// Type the longitude value for the secondary address for use in mapping and other applications.
    /// </summary>
    [AttributeLogicalName("address2_longitude")]
    [DataMember]
    public double? Address2_Longitude
    {
        get
        {
            return GetAttributeValue<double?>("address2_longitude");
        }
        set
        {
            SetAttributeValue("address2_longitude", value);
        }
    }

    /// <summary>
    /// Type a descriptive name for the secondary address, such as Corporate Headquarters.
    /// </summary>
    [AttributeLogicalName("address2_name")]
    [DataMember]
    public string Address2_Name
    {
        get
        {
            return GetAttributeValue<string>("address2_name");
        }
        set
        {
            SetAttributeValue("address2_name", value);
        }
    }

    /// <summary>
    /// Type the ZIP Code or postal code for the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_postalcode")]
    [DataMember]
    public string Address2_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("address2_postalcode");
        }
        set
        {
            SetAttributeValue("address2_postalcode", value);
        }
    }

    /// <summary>
    /// Type the post office box number of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_postofficebox")]
    [DataMember]
    public string Address2_PostOfficeBox
    {
        get
        {
            return GetAttributeValue<string>("address2_postofficebox");
        }
        set
        {
            SetAttributeValue("address2_postofficebox", value);
        }
    }

    /// <summary>
    /// Type the name of the main contact at the account's secondary address.
    /// </summary>
    [AttributeLogicalName("address2_primarycontactname")]
    [DataMember]
    public string Address2_PrimaryContactName
    {
        get
        {
            return GetAttributeValue<string>("address2_primarycontactname");
        }
        set
        {
            SetAttributeValue("address2_primarycontactname", value);
        }
    }

    /// <summary>
    /// Select a shipping method for deliveries sent to this address.
    /// </summary>
    [AttributeLogicalName("address2_shippingmethodcode")]
    [DataMember]
    public OptionSetValue Address2_ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address2_shippingmethodcode");
        }
        set
        {
            SetAttributeValue("address2_shippingmethodcode", value);
        }
    }

    /// <summary>
    /// Type the state or province of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_stateorprovince")]
    [DataMember]
    public string Address2_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("address2_stateorprovince");
        }
        set
        {
            SetAttributeValue("address2_stateorprovince", value);
        }
    }

    /// <summary>
    /// Type the main phone number associated with the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_telephone1")]
    [DataMember]
    public string Address2_Telephone1
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone1");
        }
        set
        {
            SetAttributeValue("address2_telephone1", value);
        }
    }

    /// <summary>
    /// Type a second phone number associated with the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_telephone2")]
    [DataMember]
    public string Address2_Telephone2
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone2");
        }
        set
        {
            SetAttributeValue("address2_telephone2", value);
        }
    }

    /// <summary>
    /// Type a third phone number associated with the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_telephone3")]
    [DataMember]
    public string Address2_Telephone3
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone3");
        }
        set
        {
            SetAttributeValue("address2_telephone3", value);
        }
    }

    /// <summary>
    /// Type the UPS zone of the secondary address to make sure shipping charges are calculated correctly and deliveries are made promptly, if shipped by UPS.
    /// </summary>
    [AttributeLogicalName("address2_upszone")]
    [DataMember]
    public string Address2_UPSZone
    {
        get
        {
            return GetAttributeValue<string>("address2_upszone");
        }
        set
        {
            SetAttributeValue("address2_upszone", value);
        }
    }

    /// <summary>
    /// Select the time zone, or UTC offset, for this address so that other people can reference it when they contact someone at this address.
    /// </summary>
    [AttributeLogicalName("address2_utcoffset")]
    [DataMember]
    public int? Address2_UTCOffset
    {
        get
        {
            return GetAttributeValue<int?>("address2_utcoffset");
        }
        set
        {
            SetAttributeValue("address2_utcoffset", value);
        }
    }

    /// <summary>
    /// Unique identifier for address 3.
    /// </summary>
    [AttributeLogicalName("address3_addressid")]
    [DataMember]
    public Guid? Address3_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("address3_addressid");
        }
        set
        {
            SetAttributeValue("address3_addressid", value);
        }
    }

    /// <summary>
    /// Select the third address type.
    /// </summary>
    [AttributeLogicalName("address3_addresstypecode")]
    [DataMember]
    public OptionSetValue Address3_AddressTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address3_addresstypecode");
        }
        set
        {
            SetAttributeValue("address3_addresstypecode", value);
        }
    }

    /// <summary>
    /// Type the city for the 3rd address.
    /// </summary>
    [AttributeLogicalName("address3_city")]
    [DataMember]
    public string Address3_City
    {
        get
        {
            return GetAttributeValue<string>("address3_city");
        }
        set
        {
            SetAttributeValue("address3_city", value);
        }
    }

    /// <summary>
    /// Shows the complete third address.
    /// </summary>
    [AttributeLogicalName("address3_composite")]
    [DataMember]
    public string Address3_Composite
    {
        get
        {
            return GetAttributeValue<string>("address3_composite");
        }
    }

    /// <summary>
    /// the country or region for the 3rd address.
    /// </summary>
    [AttributeLogicalName("address3_country")]
    [DataMember]
    public string Address3_Country
    {
        get
        {
            return GetAttributeValue<string>("address3_country");
        }
        set
        {
            SetAttributeValue("address3_country", value);
        }
    }

    /// <summary>
    /// Type the county for the third address.
    /// </summary>
    [AttributeLogicalName("address3_county")]
    [DataMember]
    public string Address3_County
    {
        get
        {
            return GetAttributeValue<string>("address3_county");
        }
        set
        {
            SetAttributeValue("address3_county", value);
        }
    }

    /// <summary>
    /// Type the fax number associated with the third address.
    /// </summary>
    [AttributeLogicalName("address3_fax")]
    [DataMember]
    public string Address3_Fax
    {
        get
        {
            return GetAttributeValue<string>("address3_fax");
        }
        set
        {
            SetAttributeValue("address3_fax", value);
        }
    }

    /// <summary>
    /// Select the freight terms for the third address to make sure shipping orders are processed correctly.
    /// </summary>
    [AttributeLogicalName("address3_freighttermscode")]
    [DataMember]
    public OptionSetValue Address3_FreightTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address3_freighttermscode");
        }
        set
        {
            SetAttributeValue("address3_freighttermscode", value);
        }
    }

    /// <summary>
    /// Type the latitude value for the third address for use in mapping and other applications.
    /// </summary>
    [AttributeLogicalName("address3_latitude")]
    [DataMember]
    public double? Address3_Latitude
    {
        get
        {
            return GetAttributeValue<double?>("address3_latitude");
        }
        set
        {
            SetAttributeValue("address3_latitude", value);
        }
    }

    /// <summary>
    /// the first line of the 3rd address.
    /// </summary>
    [AttributeLogicalName("address3_line1")]
    [DataMember]
    public string Address3_Line1
    {
        get
        {
            return GetAttributeValue<string>("address3_line1");
        }
        set
        {
            SetAttributeValue("address3_line1", value);
        }
    }

    /// <summary>
    /// the second line of the 3rd address.
    /// </summary>
    [AttributeLogicalName("address3_line2")]
    [DataMember]
    public string Address3_Line2
    {
        get
        {
            return GetAttributeValue<string>("address3_line2");
        }
        set
        {
            SetAttributeValue("address3_line2", value);
        }
    }

    /// <summary>
    /// the third line of the 3rd address.
    /// </summary>
    [AttributeLogicalName("address3_line3")]
    [DataMember]
    public string Address3_Line3
    {
        get
        {
            return GetAttributeValue<string>("address3_line3");
        }
        set
        {
            SetAttributeValue("address3_line3", value);
        }
    }

    /// <summary>
    /// Type the longitude value for the third address for use in mapping and other applications.
    /// </summary>
    [AttributeLogicalName("address3_longitude")]
    [DataMember]
    public double? Address3_Longitude
    {
        get
        {
            return GetAttributeValue<double?>("address3_longitude");
        }
        set
        {
            SetAttributeValue("address3_longitude", value);
        }
    }

    /// <summary>
    /// Type a descriptive name for the third address, such as Corporate Headquarters.
    /// </summary>
    [AttributeLogicalName("address3_name")]
    [DataMember]
    public string Address3_Name
    {
        get
        {
            return GetAttributeValue<string>("address3_name");
        }
        set
        {
            SetAttributeValue("address3_name", value);
        }
    }

    /// <summary>
    /// the ZIP Code or postal code for the 3rd address.
    /// </summary>
    [AttributeLogicalName("address3_postalcode")]
    [DataMember]
    public string Address3_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("address3_postalcode");
        }
        set
        {
            SetAttributeValue("address3_postalcode", value);
        }
    }

    /// <summary>
    /// the post office box number of the 3rd address.
    /// </summary>
    [AttributeLogicalName("address3_postofficebox")]
    [DataMember]
    public string Address3_PostOfficeBox
    {
        get
        {
            return GetAttributeValue<string>("address3_postofficebox");
        }
        set
        {
            SetAttributeValue("address3_postofficebox", value);
        }
    }

    /// <summary>
    /// Type the name of the main contact at the account's third address.
    /// </summary>
    [AttributeLogicalName("address3_primarycontactname")]
    [DataMember]
    public string Address3_PrimaryContactName
    {
        get
        {
            return GetAttributeValue<string>("address3_primarycontactname");
        }
        set
        {
            SetAttributeValue("address3_primarycontactname", value);
        }
    }

    /// <summary>
    /// Select a shipping method for deliveries sent to this address.
    /// </summary>
    [AttributeLogicalName("address3_shippingmethodcode")]
    [DataMember]
    public OptionSetValue Address3_ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address3_shippingmethodcode");
        }
        set
        {
            SetAttributeValue("address3_shippingmethodcode", value);
        }
    }

    /// <summary>
    /// the state or province of the third address.
    /// </summary>
    [AttributeLogicalName("address3_stateorprovince")]
    [DataMember]
    public string Address3_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("address3_stateorprovince");
        }
        set
        {
            SetAttributeValue("address3_stateorprovince", value);
        }
    }

    /// <summary>
    /// Type the main phone number associated with the third address.
    /// </summary>
    [AttributeLogicalName("address3_telephone1")]
    [DataMember]
    public string Address3_Telephone1
    {
        get
        {
            return GetAttributeValue<string>("address3_telephone1");
        }
        set
        {
            SetAttributeValue("address3_telephone1", value);
        }
    }

    /// <summary>
    /// Type a second phone number associated with the third address.
    /// </summary>
    [AttributeLogicalName("address3_telephone2")]
    [DataMember]
    public string Address3_Telephone2
    {
        get
        {
            return GetAttributeValue<string>("address3_telephone2");
        }
        set
        {
            SetAttributeValue("address3_telephone2", value);
        }
    }

    /// <summary>
    /// Type a third phone number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address3_telephone3")]
    [DataMember]
    public string Address3_Telephone3
    {
        get
        {
            return GetAttributeValue<string>("address3_telephone3");
        }
        set
        {
            SetAttributeValue("address3_telephone3", value);
        }
    }

    /// <summary>
    /// Type the UPS zone of the third address to make sure shipping charges are calculated correctly and deliveries are made promptly, if shipped by UPS.
    /// </summary>
    [AttributeLogicalName("address3_upszone")]
    [DataMember]
    public string Address3_UPSZone
    {
        get
        {
            return GetAttributeValue<string>("address3_upszone");
        }
        set
        {
            SetAttributeValue("address3_upszone", value);
        }
    }

    /// <summary>
    /// Select the time zone, or UTC offset, for this address so that other people can reference it when they contact someone at this address.
    /// </summary>
    [AttributeLogicalName("address3_utcoffset")]
    [DataMember]
    public int? Address3_UTCOffset
    {
        get
        {
            return GetAttributeValue<int?>("address3_utcoffset");
        }
        set
        {
            SetAttributeValue("address3_utcoffset", value);
        }
    }

    /// <summary>
    /// For system use only.
    /// </summary>
    [AttributeLogicalName("aging30")]
    [DataMember]
    public Money Aging30
    {
        get
        {
            return GetAttributeValue<Money>("aging30");
        }
    }

    /// <summary>
    /// Shows the Aging 30 field converted to the system's default base currency. The calculations use the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("aging30_base")]
    [DataMember]
    public Money Aging30_Base
    {
        get
        {
            return GetAttributeValue<Money>("aging30_base");
        }
    }

    /// <summary>
    /// For system use only.
    /// </summary>
    [AttributeLogicalName("aging60")]
    [DataMember]
    public Money Aging60
    {
        get
        {
            return GetAttributeValue<Money>("aging60");
        }
    }

    /// <summary>
    /// Shows the Aging 60 field converted to the system's default base currency. The calculations use the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("aging60_base")]
    [DataMember]
    public Money Aging60_Base
    {
        get
        {
            return GetAttributeValue<Money>("aging60_base");
        }
    }

    /// <summary>
    /// For system use only.
    /// </summary>
    [AttributeLogicalName("aging90")]
    [DataMember]
    public Money Aging90
    {
        get
        {
            return GetAttributeValue<Money>("aging90");
        }
    }

    /// <summary>
    /// Shows the Aging 90 field converted to the system's default base currency. The calculations use the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("aging90_base")]
    [DataMember]
    public Money Aging90_Base
    {
        get
        {
            return GetAttributeValue<Money>("aging90_base");
        }
    }

    /// <summary>
    /// Enter the date of the contact's wedding or service anniversary for use in customer gift programs or other communications.
    /// </summary>
    [AttributeLogicalName("anniversary")]
    [DataMember]
    public DateTime? Anniversary
    {
        get
        {
            return GetAttributeValue<DateTime?>("anniversary");
        }
        set
        {
            SetAttributeValue("anniversary", value);
        }
    }

    /// <summary>
    /// Type the contact's annual income for use in profiling and financial analysis.
    /// </summary>
    [AttributeLogicalName("annualincome")]
    [DataMember]
    public Money AnnualIncome
    {
        get
        {
            return GetAttributeValue<Money>("annualincome");
        }
        set
        {
            SetAttributeValue("annualincome", value);
        }
    }

    /// <summary>
    /// Shows the Annual Income field converted to the system's default base currency. The calculations use the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("annualincome_base")]
    [DataMember]
    public Money AnnualIncome_Base
    {
        get
        {
            return GetAttributeValue<Money>("annualincome_base");
        }
    }

    /// <summary>
    /// Type the name of the contact's assistant.
    /// </summary>
    [AttributeLogicalName("assistantname")]
    [DataMember]
    public string AssistantName
    {
        get
        {
            return GetAttributeValue<string>("assistantname");
        }
        set
        {
            SetAttributeValue("assistantname", value);
        }
    }

    /// <summary>
    /// Type the phone number for the contact's assistant.
    /// </summary>
    [AttributeLogicalName("assistantphone")]
    [DataMember]
    public string AssistantPhone
    {
        get
        {
            return GetAttributeValue<string>("assistantphone");
        }
        set
        {
            SetAttributeValue("assistantphone", value);
        }
    }

    /// <summary>
    /// Enter the contact's birthday for use in customer gift programs or other communications.
    /// </summary>
    [AttributeLogicalName("birthdate")]
    [DataMember]
    public DateTime? BirthDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("birthdate");
        }
        set
        {
            SetAttributeValue("birthdate", value);
        }
    }

    /// <summary>
    /// Type a second business phone number for this contact.
    /// </summary>
    [AttributeLogicalName("business2")]
    [DataMember]
    public string Business2
    {
        get
        {
            return GetAttributeValue<string>("business2");
        }
        set
        {
            SetAttributeValue("business2", value);
        }
    }

    /// <summary>
    /// Type a callback phone number for this contact.
    /// </summary>
    [AttributeLogicalName("callback")]
    [DataMember]
    public string Callback
    {
        get
        {
            return GetAttributeValue<string>("callback");
        }
        set
        {
            SetAttributeValue("callback", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_age")]
    [DataMember]
    public int? cdi_age
    {
        get
        {
            return GetAttributeValue<int?>("cdi_age");
        }
        set
        {
            SetAttributeValue("cdi_age", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_allowtextmessages")]
    [DataMember]
    public bool? cdi_allowtextmessages
    {
        get
        {
            return GetAttributeValue<bool?>("cdi_allowtextmessages");
        }
        set
        {
            SetAttributeValue("cdi_allowtextmessages", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_facebook")]
    [DataMember]
    public string cdi_facebook
    {
        get
        {
            return GetAttributeValue<string>("cdi_facebook");
        }
        set
        {
            SetAttributeValue("cdi_facebook", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_grade")]
    [DataMember]
    public string cdi_grade
    {
        get
        {
            return GetAttributeValue<string>("cdi_grade");
        }
        set
        {
            SetAttributeValue("cdi_grade", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_identifiedon")]
    [DataMember]
    public DateTime? cdi_identifiedon
    {
        get
        {
            return GetAttributeValue<DateTime?>("cdi_identifiedon");
        }
        set
        {
            SetAttributeValue("cdi_identifiedon", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_image")]
    [DataMember]
    public string cdi_image
    {
        get
        {
            return GetAttributeValue<string>("cdi_image");
        }
        set
        {
            SetAttributeValue("cdi_image", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_ip")]
    [DataMember]
    public string cdi_ip
    {
        get
        {
            return GetAttributeValue<string>("cdi_ip");
        }
        set
        {
            SetAttributeValue("cdi_ip", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_latitude")]
    [DataMember]
    public string cdi_latitude
    {
        get
        {
            return GetAttributeValue<string>("cdi_latitude");
        }
        set
        {
            SetAttributeValue("cdi_latitude", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_linkedin")]
    [DataMember]
    public string cdi_linkedin
    {
        get
        {
            return GetAttributeValue<string>("cdi_linkedin");
        }
        set
        {
            SetAttributeValue("cdi_linkedin", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_longitude")]
    [DataMember]
    public string cdi_Longitude
    {
        get
        {
            return GetAttributeValue<string>("cdi_longitude");
        }
        set
        {
            SetAttributeValue("cdi_longitude", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_partitionkey")]
    [DataMember]
    public string cdi_partitionkey
    {
        get
        {
            return GetAttributeValue<string>("cdi_partitionkey");
        }
        set
        {
            SetAttributeValue("cdi_partitionkey", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_rowkey")]
    [DataMember]
    public string cdi_rowkey
    {
        get
        {
            return GetAttributeValue<string>("cdi_rowkey");
        }
        set
        {
            SetAttributeValue("cdi_rowkey", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_score")]
    [DataMember]
    public int? cdi_score
    {
        get
        {
            return GetAttributeValue<int?>("cdi_score");
        }
        set
        {
            SetAttributeValue("cdi_score", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_social")]
    [DataMember]
    public string cdi_social
    {
        get
        {
            return GetAttributeValue<string>("cdi_social");
        }
        set
        {
            SetAttributeValue("cdi_social", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_totalscore")]
    [DataMember]
    public int? cdi_totalscore
    {
        get
        {
            return GetAttributeValue<int?>("cdi_totalscore");
        }
        set
        {
            SetAttributeValue("cdi_totalscore", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_twitter")]
    [DataMember]
    public string cdi_twitter
    {
        get
        {
            return GetAttributeValue<string>("cdi_twitter");
        }
        set
        {
            SetAttributeValue("cdi_twitter", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_visitorkey")]
    [DataMember]
    public string cdi_visitorkey
    {
        get
        {
            return GetAttributeValue<string>("cdi_visitorkey");
        }
        set
        {
            SetAttributeValue("cdi_visitorkey", value);
        }
    }

    /// <summary>
    /// Type the names of the contact's children for reference in communications and client programs.
    /// </summary>
    [AttributeLogicalName("childrensnames")]
    [DataMember]
    public string ChildrensNames
    {
        get
        {
            return GetAttributeValue<string>("childrensnames");
        }
        set
        {
            SetAttributeValue("childrensnames", value);
        }
    }

    /// <summary>
    /// Type the company phone of the contact.
    /// </summary>
    [AttributeLogicalName("company")]
    [DataMember]
    public string Company
    {
        get
        {
            return GetAttributeValue<string>("company");
        }
        set
        {
            SetAttributeValue("company", value);
        }
    }

    /// <summary>
    /// Unique identifier of the contact.
    /// </summary>
    [AttributeLogicalName("contactid")]
    [DataMember]
    public Guid? ContactId
    {
        get
        {
            return GetAttributeValue<Guid?>("contactid");
        }
        set
        {
            SetAttributeValue("contactid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("contactid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            ContactId = value;
        }
    }

    /// <summary>
    /// Shows who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Shows the external party who created the record.
    /// </summary>
    [AttributeLogicalName("createdbyexternalparty")]
    [DataMember]
    public EntityReference CreatedByExternalParty
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdbyexternalparty");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Type the credit limit of the contact for reference when you address invoice and accounting issues with the customer.
    /// </summary>
    [AttributeLogicalName("creditlimit")]
    [DataMember]
    public Money CreditLimit
    {
        get
        {
            return GetAttributeValue<Money>("creditlimit");
        }
        set
        {
            SetAttributeValue("creditlimit", value);
        }
    }

    /// <summary>
    /// Shows the Credit Limit field converted to the system's default base currency for reporting purposes. The calculations use the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("creditlimit_base")]
    [DataMember]
    public Money CreditLimit_Base
    {
        get
        {
            return GetAttributeValue<Money>("creditlimit_base");
        }
    }

    /// <summary>
    /// Select whether the contact is on a credit hold, for reference when addressing invoice and accounting issues.
    /// </summary>
    [AttributeLogicalName("creditonhold")]
    [DataMember]
    public bool? CreditOnHold
    {
        get
        {
            return GetAttributeValue<bool?>("creditonhold");
        }
        set
        {
            SetAttributeValue("creditonhold", value);
        }
    }

    /// <summary>
    /// decision maker Bias score. Range 2 to -2
    /// </summary>
    [AttributeLogicalName("ctas_bias_score")]
    [DataMember]
    public OptionSetValue ctas_Bias_Score
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_bias_score");
        }
        set
        {
            SetAttributeValue("ctas_bias_score", value);
        }
    }

    /// <summary>
    /// This ties the contact back to a Lead which came into Cintas.com or some other lead source
    /// </summary>
    [AttributeLogicalName("ctas_cintas_com_lu")]
    [DataMember]
    public EntityReference ctas_Cintas_com_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_cintas_com_lu");
        }
        set
        {
            SetAttributeValue("ctas_cintas_com_lu", value);
        }
    }

    /// <summary>
    /// ABI Number from Cintergy
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_abi_number")]
    [DataMember]
    public string ctas_cintergy_abi_number
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_abi_number");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_abi_number", value);
        }
    }

    /// <summary>
    /// Best time to call from Cintergy.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_best_time_to_call")]
    [DataMember]
    public string ctas_cintergy_best_time_to_call
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_best_time_to_call");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_best_time_to_call", value);
        }
    }

    /// <summary>
    /// Cintergy Business Division ID
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_business_division_id")]
    [DataMember]
    public int? ctas_cintergy_business_division_id
    {
        get
        {
            return GetAttributeValue<int?>("ctas_cintergy_business_division_id");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_business_division_id", value);
        }
    }

    /// <summary>
    /// Name of the Cintergy Business Division.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_business_division_name")]
    [DataMember]
    public string ctas_cintergy_business_division_name
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_business_division_name");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_business_division_name", value);
        }
    }

    /// <summary>
    /// First Name of the Cintergy owning user.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_owning_user_first_name")]
    [DataMember]
    public string ctas_cintergy_owning_user_first_name
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_owning_user_first_name");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_owning_user_first_name", value);
        }
    }

    /// <summary>
    /// Cintergy Owning User ID
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_owning_user_id")]
    [DataMember]
    public int? ctas_cintergy_owning_user_id
    {
        get
        {
            return GetAttributeValue<int?>("ctas_cintergy_owning_user_id");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_owning_user_id", value);
        }
    }

    /// <summary>
    /// Cintergy owning user last name.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_owning_user_last_name")]
    [DataMember]
    public string ctas_cintergy_owning_user_last_name
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_owning_user_last_name");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_owning_user_last_name", value);
        }
    }

    /// <summary>
    /// Phone 1 from Cintergy.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_phone1")]
    [DataMember]
    public string ctas_cintergy_phone1
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_phone1");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_phone1", value);
        }
    }

    /// <summary>
    /// Phone type is used to identify the phone number type. Direct Line, Mobile, Main.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_phone1_type")]
    [DataMember]
    public string ctas_cintergy_phone1_type
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_phone1_type");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_phone1_type", value);
        }
    }

    /// <summary>
    /// Phone 2 from Cintergy
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_phone2")]
    [DataMember]
    public string ctas_cintergy_phone2
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_phone2");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_phone2", value);
        }
    }

    /// <summary>
    /// Phone type is used to identify the phone number type. Direct Line, Mobile, Main.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_phone2_type")]
    [DataMember]
    public string ctas_cintergy_phone2_type
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_phone2_type");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_phone2_type", value);
        }
    }

    /// <summary>
    /// Phone3 from Cintergy.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_phone3")]
    [DataMember]
    public string ctas_cintergy_phone3
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_phone3");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_phone3", value);
        }
    }

    /// <summary>
    /// Phone type is used to identify the phone number type. Direct Line, Mobile, Main.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_phone3_type")]
    [DataMember]
    public string ctas_cintergy_phone3_type
    {
        get
        {
            return GetAttributeValue<string>("ctas_cintergy_phone3_type");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_phone3_type", value);
        }
    }

    /// <summary>
    /// Designates primary phone number from Cintergy.
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_primary_phone_id")]
    [DataMember]
    public bool? ctas_cintergy_primary_phone_id
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_cintergy_primary_phone_id");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_primary_phone_id", value);
        }
    }

    /// <summary>
    /// Used to define what is wrong with the contact
    /// </summary>
    [AttributeLogicalName("ctas_contact_errors")]
    [DataMember]
    public OptionSetValue ctas_Contact_Errors
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_contact_errors");
        }
        set
        {
            SetAttributeValue("ctas_contact_errors", value);
        }
    }

    /// <summary>
    /// Contact Feedback
    /// </summary>
    [AttributeLogicalName("ctas_contact_feedback")]
    [DataMember]
    public string ctas_Contact_Feedback
    {
        get
        {
            return GetAttributeValue<string>("ctas_contact_feedback");
        }
        set
        {
            SetAttributeValue("ctas_contact_feedback", value);
        }
    }

    /// <summary>
    /// Look up field to the Contact Entity.
    /// </summary>
    [AttributeLogicalName("ctas_contact_lu")]
    [DataMember]
    public EntityReference ctas_Contact_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_contact_lu");
        }
        set
        {
            SetAttributeValue("ctas_contact_lu", value);
        }
    }

    /// <summary>
    /// Rating of the contact
    /// </summary>
    [AttributeLogicalName("ctas_contact_rating")]
    [DataMember]
    public OptionSetValue ctas_Contact_Rating
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_contact_rating");
        }
        set
        {
            SetAttributeValue("ctas_contact_rating", value);
        }
    }

    /// <summary>
    /// This will give a description of the contact ratings
    /// </summary>
    [AttributeLogicalName("ctas_contact_rating_definition")]
    [DataMember]
    public string ctas_Contact_Rating_Definition
    {
        get
        {
            return GetAttributeValue<string>("ctas_contact_rating_definition");
        }
        set
        {
            SetAttributeValue("ctas_contact_rating_definition", value);
        }
    }

    /// <summary>
    /// Unique identifier for Phone Call associated with Contact.
    /// </summary>
    [AttributeLogicalName("ctas_contact_relationshipid")]
    [DataMember]
    public EntityReference ctas_Contact_RelationshipId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_contact_relationshipid");
        }
        set
        {
            SetAttributeValue("ctas_contact_relationshipid", value);
        }
    }

    /// <summary>
    /// This will identify if the contact came from an outside source.
    /// </summary>
    [AttributeLogicalName("ctas_contact_source")]
    [DataMember]
    public OptionSetValue ctas_Contact_Source
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_contact_source");
        }
        set
        {
            SetAttributeValue("ctas_contact_source", value);
        }
    }

    /// <summary>
    /// Defines what type of contact. ie: Primary Contact. Default Value is Standard Contact
    /// </summary>
    [AttributeLogicalName("ctas_contact_type")]
    [DataMember]
    public OptionSetValue ctas_Contact_Type
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_contact_type");
        }
        set
        {
            SetAttributeValue("ctas_contact_type", value);
        }
    }

    /// <summary>
    /// Determines if the contact is part of a account which uses Vertex. This field is used by Vertex Services.
    /// </summary>
    [AttributeLogicalName("ctas_crmvertexcontact")]
    [DataMember]
    public bool? ctas_CRMVertexContact
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_crmvertexcontact");
        }
        set
        {
            SetAttributeValue("ctas_crmvertexcontact", value);
        }
    }

    /// <summary>
    /// Shows which department the contact in. This field is used by Vertex Services.
    /// </summary>
    [AttributeLogicalName("ctas_crmvertexdepartment")]
    [DataMember]
    public string ctas_CRMVertexDepartment
    {
        get
        {
            return GetAttributeValue<string>("ctas_crmvertexdepartment");
        }
        set
        {
            SetAttributeValue("ctas_crmvertexdepartment", value);
        }
    }

    /// <summary>
    /// Direct phone line to contact.
    /// </summary>
    [AttributeLogicalName("ctas_direct_line")]
    [DataMember]
    public string ctas_Direct_Line
    {
        get
        {
            return GetAttributeValue<string>("ctas_direct_line");
        }
        set
        {
            SetAttributeValue("ctas_direct_line", value);
        }
    }

    /// <summary>
    /// Identified duplicate email
    /// </summary>
    [AttributeLogicalName("ctas_duplicate_email")]
    [DataMember]
    public bool? ctas_Duplicate_Email
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_duplicate_email");
        }
        set
        {
            SetAttributeValue("ctas_duplicate_email", value);
        }
    }

    /// <summary>
    /// E-BIZ ID
    /// </summary>
    [AttributeLogicalName("ctas_e_biz_id")]
    [DataMember]
    public string ctas_E_BIZ_ID
    {
        get
        {
            return GetAttributeValue<string>("ctas_e_biz_id");
        }
        set
        {
            SetAttributeValue("ctas_e_biz_id", value);
        }
    }

    /// <summary>
    /// The date the email was last updated
    /// </summary>
    [AttributeLogicalName("ctas_email_update_date")]
    [DataMember]
    public DateTime? ctas_Email_Update_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_email_update_date");
        }
        set
        {
            SetAttributeValue("ctas_email_update_date", value);
        }
    }

    /// <summary>
    /// Lookup to the Cintas External List Campaign
    /// </summary>
    [AttributeLogicalName("ctas_ext_list_camp_lu")]
    [DataMember]
    public EntityReference ctas_Ext_List_Camp_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_ext_list_camp_lu");
        }
        set
        {
            SetAttributeValue("ctas_ext_list_camp_lu", value);
        }
    }

    /// <summary>
    /// Contact came from D&B Contact list  during G&K Acquisition.
    /// </summary>
    [AttributeLogicalName("ctas_gk_contact")]
    [DataMember]
    public bool? ctas_GK_Contact
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_gk_contact");
        }
        set
        {
            SetAttributeValue("ctas_gk_contact", value);
        }
    }

    /// <summary>
    /// Impact of Decision Maker
    /// </summary>
    [AttributeLogicalName("ctas_impact")]
    [DataMember]
    public OptionSetValue ctas_Impact
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_impact");
        }
        set
        {
            SetAttributeValue("ctas_impact", value);
        }
    }

    /// <summary>
    /// Perception of Decision Maker
    /// </summary>
    [AttributeLogicalName("ctas_perception")]
    [DataMember]
    public OptionSetValue ctas_Perception
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_perception");
        }
        set
        {
            SetAttributeValue("ctas_perception", value);
        }
    }

    /// <summary>
    /// Decision Maker Personality
    /// </summary>
    [AttributeLogicalName("ctas_personality")]
    [DataMember]
    public OptionSetValue ctas_Personality
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_personality");
        }
        set
        {
            SetAttributeValue("ctas_personality", value);
        }
    }

    /// <summary>
    /// Expiration Date for Sales Fusion
    /// </summary>
    [AttributeLogicalName("ctas_sales_fusion_exp")]
    [DataMember]
    public DateTime? ctas_Sales_Fusion_Exp
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_sales_fusion_exp");
        }
        set
        {
            SetAttributeValue("ctas_sales_fusion_exp", value);
        }
    }

    /// <summary>
    /// Scoring for Sales Fusion
    /// </summary>
    [AttributeLogicalName("ctas_sales_fusion_scoring")]
    [DataMember]
    public int? ctas_Sales_Fusion_Scoring
    {
        get
        {
            return GetAttributeValue<int?>("ctas_sales_fusion_scoring");
        }
        set
        {
            SetAttributeValue("ctas_sales_fusion_scoring", value);
        }
    }

    /// <summary>
    /// Suppressed company name.
    /// </summary>
    [AttributeLogicalName("ctas_suppressed_company_name")]
    [DataMember]
    public string ctas_Suppressed_Company_Name
    {
        get
        {
            return GetAttributeValue<string>("ctas_suppressed_company_name");
        }
        set
        {
            SetAttributeValue("ctas_suppressed_company_name", value);
        }
    }

    /// <summary>
    /// Unique identifier for TAR Tracking associated with Contact.
    /// </summary>
    [AttributeLogicalName("ctas_tar_track_lu_id")]
    [DataMember]
    public EntityReference ctas_TAR_Track_LU_Id
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_tar_track_lu_id");
        }
        set
        {
            SetAttributeValue("ctas_tar_track_lu_id", value);
        }
    }

    /// <summary>
    /// Team LU
    /// </summary>
    [AttributeLogicalName("ctas_team_lu")]
    [DataMember]
    public EntityReference ctas_Team_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_team_lu");
        }
        set
        {
            SetAttributeValue("ctas_team_lu", value);
        }
    }

    /// <summary>
    /// This field will be used in the Cintas.com Lead process to identify contacts which already exist in Dynamics. They will be deleted within a week of creation.
    /// </summary>
    [AttributeLogicalName("ctas_temp_mkt_contact")]
    [DataMember]
    public bool? ctas_Temp_Mkt_Contact
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_temp_mkt_contact");
        }
        set
        {
            SetAttributeValue("ctas_temp_mkt_contact", value);
        }
    }

    /// <summary>
    /// Win/Win - Personal and Professional Note field.
    /// </summary>
    [AttributeLogicalName("ctas_winwin_personal_and_professional")]
    [DataMember]
    public string ctas_WinWin_Personal_and_Professional
    {
        get
        {
            return GetAttributeValue<string>("ctas_winwin_personal_and_professional");
        }
        set
        {
            SetAttributeValue("ctas_winwin_personal_and_professional", value);
        }
    }

    /// <summary>
    /// Select the size of the contact's company for segmentation and reporting purposes.
    /// </summary>
    [AttributeLogicalName("customersizecode")]
    [DataMember]
    public OptionSetValue CustomerSizeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("customersizecode");
        }
        set
        {
            SetAttributeValue("customersizecode", value);
        }
    }

    /// <summary>
    /// Type of business associated with the contact.
    /// </summary>
    [AttributeLogicalName("customertypecode")]
    [DataMember]
    public OptionSetValue CustomerTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("customertypecode");
        }
        set
        {
            SetAttributeValue("customertypecode", value);
        }
    }

    /// <summary>
    /// Choose the default price list associated with the contact to make sure the correct product prices for this customer are applied in sales opportunities, quotes, and orders.
    /// </summary>
    [AttributeLogicalName("defaultpricelevelid")]
    [DataMember]
    public EntityReference DefaultPriceLevelId
    {
        get
        {
            return GetAttributeValue<EntityReference>("defaultpricelevelid");
        }
        set
        {
            SetAttributeValue("defaultpricelevelid", value);
        }
    }

    /// <summary>
    /// Type the department or business unit where the contact works in the parent company or business.
    /// </summary>
    [AttributeLogicalName("department")]
    [DataMember]
    public string Department
    {
        get
        {
            return GetAttributeValue<string>("department");
        }
        set
        {
            SetAttributeValue("department", value);
        }
    }

    /// <summary>
    /// Type additional information to describe the contact, such as an excerpt from the company's website.
    /// </summary>
    [AttributeLogicalName("description")]
    [DataMember]
    public string Description
    {
        get
        {
            return GetAttributeValue<string>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /// <summary>
    /// Select whether the contact accepts bulk email sent through marketing campaigns or quick campaigns. If Do Not Allow is selected, the contact can be added to marketing lists, but will be excluded from the email.
    /// </summary>
    [AttributeLogicalName("donotbulkemail")]
    [DataMember]
    public bool? DoNotBulkEMail
    {
        get
        {
            return GetAttributeValue<bool?>("donotbulkemail");
        }
        set
        {
            SetAttributeValue("donotbulkemail", value);
        }
    }

    /// <summary>
    /// Select whether the contact accepts bulk postal mail sent through marketing campaigns or quick campaigns. If Do Not Allow is selected, the contact can be added to marketing lists, but will be excluded from the letters.
    /// </summary>
    [AttributeLogicalName("donotbulkpostalmail")]
    [DataMember]
    public bool? DoNotBulkPostalMail
    {
        get
        {
            return GetAttributeValue<bool?>("donotbulkpostalmail");
        }
        set
        {
            SetAttributeValue("donotbulkpostalmail", value);
        }
    }

    /// <summary>
    /// Select whether the contact allows direct email sent from Microsoft Dynamics 365. If Do Not Allow is selected, Microsoft Dynamics 365 will not send the email.
    /// </summary>
    [AttributeLogicalName("donotemail")]
    [DataMember]
    public bool? DoNotEMail
    {
        get
        {
            return GetAttributeValue<bool?>("donotemail");
        }
        set
        {
            SetAttributeValue("donotemail", value);
        }
    }

    /// <summary>
    /// Select whether the contact allows faxes. If Do Not Allow is selected, the contact will be excluded from any fax activities distributed in marketing campaigns.
    /// </summary>
    [AttributeLogicalName("donotfax")]
    [DataMember]
    public bool? DoNotFax
    {
        get
        {
            return GetAttributeValue<bool?>("donotfax");
        }
        set
        {
            SetAttributeValue("donotfax", value);
        }
    }

    /// <summary>
    /// Select whether the contact accepts phone calls. If Do Not Allow is selected, the contact will be excluded from any phone call activities distributed in marketing campaigns.
    /// </summary>
    [AttributeLogicalName("donotphone")]
    [DataMember]
    public bool? DoNotPhone
    {
        get
        {
            return GetAttributeValue<bool?>("donotphone");
        }
        set
        {
            SetAttributeValue("donotphone", value);
        }
    }

    /// <summary>
    /// Select whether the contact allows direct mail. If Do Not Allow is selected, the contact will be excluded from letter activities distributed in marketing campaigns.
    /// </summary>
    [AttributeLogicalName("donotpostalmail")]
    [DataMember]
    public bool? DoNotPostalMail
    {
        get
        {
            return GetAttributeValue<bool?>("donotpostalmail");
        }
        set
        {
            SetAttributeValue("donotpostalmail", value);
        }
    }

    /// <summary>
    /// Select whether the contact accepts marketing materials, such as brochures or catalogs. Contacts that opt out can be excluded from marketing initiatives.
    /// </summary>
    [AttributeLogicalName("donotsendmm")]
    [DataMember]
    public bool? DoNotSendMM
    {
        get
        {
            return GetAttributeValue<bool?>("donotsendmm");
        }
        set
        {
            SetAttributeValue("donotsendmm", value);
        }
    }

    /// <summary>
    /// Select the contact's highest level of education for use in segmentation and analysis.
    /// </summary>
    [AttributeLogicalName("educationcode")]
    [DataMember]
    public OptionSetValue EducationCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("educationcode");
        }
        set
        {
            SetAttributeValue("educationcode", value);
        }
    }

    /// <summary>
    /// Type the primary email address for the contact.
    /// </summary>
    [AttributeLogicalName("emailaddress1")]
    [DataMember]
    public string EMailAddress1
    {
        get
        {
            return GetAttributeValue<string>("emailaddress1");
        }
        set
        {
            SetAttributeValue("emailaddress1", value);
        }
    }

    /// <summary>
    /// Type the secondary email address for the contact.
    /// </summary>
    [AttributeLogicalName("emailaddress2")]
    [DataMember]
    public string EMailAddress2
    {
        get
        {
            return GetAttributeValue<string>("emailaddress2");
        }
        set
        {
            SetAttributeValue("emailaddress2", value);
        }
    }

    /// <summary>
    /// Type an alternate email address for the contact.
    /// </summary>
    [AttributeLogicalName("emailaddress3")]
    [DataMember]
    public string EMailAddress3
    {
        get
        {
            return GetAttributeValue<string>("emailaddress3");
        }
        set
        {
            SetAttributeValue("emailaddress3", value);
        }
    }

    /// <summary>
    /// Type the employee ID or number for the contact for reference in orders, service cases, or other communications with the contact's organization.
    /// </summary>
    [AttributeLogicalName("employeeid")]
    [DataMember]
    public string EmployeeId
    {
        get
        {
            return GetAttributeValue<string>("employeeid");
        }
        set
        {
            SetAttributeValue("employeeid", value);
        }
    }

    /// <summary>
    /// Shows the default image for the record.
    /// </summary>
    [AttributeLogicalName("entityimage")]
    [DataMember]
    public byte[] EntityImage
    {
        get
        {
            return GetAttributeValue<byte[]>("entityimage");
        }
        set
        {
            SetAttributeValue("entityimage", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_timestamp")]
    [DataMember]
    public long? EntityImage_Timestamp
    {
        get
        {
            return GetAttributeValue<long?>("entityimage_timestamp");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_url")]
    [DataMember]
    public string EntityImage_URL
    {
        get
        {
            return GetAttributeValue<string>("entityimage_url");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("entityimageid")]
    [DataMember]
    public Guid? EntityImageId
    {
        get
        {
            return GetAttributeValue<Guid?>("entityimageid");
        }
    }

    /// <summary>
    /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Identifier for an external user.
    /// </summary>
    [AttributeLogicalName("externaluseridentifier")]
    [DataMember]
    public string ExternalUserIdentifier
    {
        get
        {
            return GetAttributeValue<string>("externaluseridentifier");
        }
        set
        {
            SetAttributeValue("externaluseridentifier", value);
        }
    }

    /// <summary>
    /// Select the marital status of the contact for reference in follow-up phone calls and other communications.
    /// </summary>
    [AttributeLogicalName("familystatuscode")]
    [DataMember]
    public OptionSetValue FamilyStatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("familystatuscode");
        }
        set
        {
            SetAttributeValue("familystatuscode", value);
        }
    }

    /// <summary>
    /// Type the fax number for the contact.
    /// </summary>
    [AttributeLogicalName("fax")]
    [DataMember]
    public string Fax
    {
        get
        {
            return GetAttributeValue<string>("fax");
        }
        set
        {
            SetAttributeValue("fax", value);
        }
    }

    /// <summary>
    /// Type the contact's first name to make sure the contact is addressed correctly in sales calls, email, and marketing campaigns.
    /// </summary>
    [AttributeLogicalName("firstname")]
    [DataMember]
    public string FirstName
    {
        get
        {
            return GetAttributeValue<string>("firstname");
        }
        set
        {
            SetAttributeValue("firstname", value);
        }
    }

    /// <summary>
    /// Information about whether to allow following email activity like opens, attachment views and link clicks for emails sent to the contact.
    /// </summary>
    [AttributeLogicalName("followemail")]
    [DataMember]
    public bool? FollowEmail
    {
        get
        {
            return GetAttributeValue<bool?>("followemail");
        }
        set
        {
            SetAttributeValue("followemail", value);
        }
    }

    /// <summary>
    /// Type the URL for the contact's FTP site to enable users to access data and share documents.
    /// </summary>
    [AttributeLogicalName("ftpsiteurl")]
    [DataMember]
    public string FtpSiteUrl
    {
        get
        {
            return GetAttributeValue<string>("ftpsiteurl");
        }
        set
        {
            SetAttributeValue("ftpsiteurl", value);
        }
    }

    /// <summary>
    /// Combines and shows the contact's first and last names so that the full name can be displayed in views and reports.
    /// </summary>
    [AttributeLogicalName("fullname")]
    [DataMember]
    public string FullName
    {
        get
        {
            return GetAttributeValue<string>("fullname");
        }
    }

    /// <summary>
    /// Select the contact's gender to make sure the contact is addressed correctly in sales calls, email, and marketing campaigns.
    /// </summary>
    [AttributeLogicalName("gendercode")]
    [DataMember]
    public OptionSetValue GenderCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("gendercode");
        }
        set
        {
            SetAttributeValue("gendercode", value);
        }
    }

    /// <summary>
    /// Type the passport number or other government ID for the contact for use in documents or reports.
    /// </summary>
    [AttributeLogicalName("governmentid")]
    [DataMember]
    public string GovernmentId
    {
        get
        {
            return GetAttributeValue<string>("governmentid");
        }
        set
        {
            SetAttributeValue("governmentid", value);
        }
    }

    /// <summary>
    /// Select whether the contact has any children for reference in follow-up phone calls and other communications.
    /// </summary>
    [AttributeLogicalName("haschildrencode")]
    [DataMember]
    public OptionSetValue HasChildrenCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("haschildrencode");
        }
        set
        {
            SetAttributeValue("haschildrencode", value);
        }
    }

    /// <summary>
    /// Type a second home phone number for this contact.
    /// </summary>
    [AttributeLogicalName("home2")]
    [DataMember]
    public string Home2
    {
        get
        {
            return GetAttributeValue<string>("home2");
        }
        set
        {
            SetAttributeValue("home2", value);
        }
    }

    /// <summary>
    /// Unique identifier of the data import or data migration that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Select whether the contact exists in a separate accounting or other system, such as Microsoft Dynamics GP or another ERP database, for use in integration processes.
    /// </summary>
    [AttributeLogicalName("isbackofficecustomer")]
    [DataMember]
    public bool? IsBackofficeCustomer
    {
        get
        {
            return GetAttributeValue<bool?>("isbackofficecustomer");
        }
        set
        {
            SetAttributeValue("isbackofficecustomer", value);
        }
    }

    /// <summary>
    /// Type the job title of the contact to make sure the contact is addressed correctly in sales calls, email, and marketing campaigns.
    /// </summary>
    [AttributeLogicalName("jobtitle")]
    [DataMember]
    public string JobTitle
    {
        get
        {
            return GetAttributeValue<string>("jobtitle");
        }
        set
        {
            SetAttributeValue("jobtitle", value);
        }
    }

    /// <summary>
    /// Last name of the contact.
    /// </summary>
    [AttributeLogicalName("lastname")]
    [DataMember]
    public string LastName
    {
        get
        {
            return GetAttributeValue<string>("lastname");
        }
        set
        {
            SetAttributeValue("lastname", value);
        }
    }

    /// <summary>
    /// Contains the date and time stamp of the last on hold time.
    /// </summary>
    [AttributeLogicalName("lastonholdtime")]
    [DataMember]
    public DateTime? LastOnHoldTime
    {
        get
        {
            return GetAttributeValue<DateTime?>("lastonholdtime");
        }
        set
        {
            SetAttributeValue("lastonholdtime", value);
        }
    }

    /// <summary>
    /// Shows the date when the contact was last included in a marketing campaign or quick campaign.
    /// </summary>
    [AttributeLogicalName("lastusedincampaign")]
    [DataMember]
    public DateTime? LastUsedInCampaign
    {
        get
        {
            return GetAttributeValue<DateTime?>("lastusedincampaign");
        }
        set
        {
            SetAttributeValue("lastusedincampaign", value);
        }
    }

    /// <summary>
    /// Select the primary marketing source that directed the contact to your organization.
    /// </summary>
    [AttributeLogicalName("leadsourcecode")]
    [DataMember]
    public OptionSetValue LeadSourceCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("leadsourcecode");
        }
        set
        {
            SetAttributeValue("leadsourcecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ma_score1")]
    [DataMember]
    public string ma_score1
    {
        get
        {
            return GetAttributeValue<string>("ma_score1");
        }
        set
        {
            SetAttributeValue("ma_score1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ma_score2")]
    [DataMember]
    public string ma_score2
    {
        get
        {
            return GetAttributeValue<string>("ma_score2");
        }
        set
        {
            SetAttributeValue("ma_score2", value);
        }
    }

    /// <summary>
    /// Type the name of the contact's manager for use in escalating issues or other follow-up communications with the contact.
    /// </summary>
    [AttributeLogicalName("managername")]
    [DataMember]
    public string ManagerName
    {
        get
        {
            return GetAttributeValue<string>("managername");
        }
        set
        {
            SetAttributeValue("managername", value);
        }
    }

    /// <summary>
    /// Type the phone number for the contact's manager.
    /// </summary>
    [AttributeLogicalName("managerphone")]
    [DataMember]
    public string ManagerPhone
    {
        get
        {
            return GetAttributeValue<string>("managerphone");
        }
        set
        {
            SetAttributeValue("managerphone", value);
        }
    }

    /// <summary>
    /// Whether is only for marketing
    /// </summary>
    [AttributeLogicalName("marketingonly")]
    [DataMember]
    public bool? MarketingOnly
    {
        get
        {
            return GetAttributeValue<bool?>("marketingonly");
        }
        set
        {
            SetAttributeValue("marketingonly", value);
        }
    }

    /// <summary>
    /// Unique identifier of the master contact for merge.
    /// </summary>
    [AttributeLogicalName("masterid")]
    [DataMember]
    public EntityReference MasterId
    {
        get
        {
            return GetAttributeValue<EntityReference>("masterid");
        }
    }

    /// <summary>
    /// Shows whether the account has been merged with a master contact.
    /// </summary>
    [AttributeLogicalName("merged")]
    [DataMember]
    public bool? Merged
    {
        get
        {
            return GetAttributeValue<bool?>("merged");
        }
    }

    /// <summary>
    /// Type the contact's middle name or initial to make sure the contact is addressed correctly.
    /// </summary>
    [AttributeLogicalName("middlename")]
    [DataMember]
    public string MiddleName
    {
        get
        {
            return GetAttributeValue<string>("middlename");
        }
        set
        {
            SetAttributeValue("middlename", value);
        }
    }

    /// <summary>
    /// Mobile phone number for the contact.
    /// </summary>
    [AttributeLogicalName("mobilephone")]
    [DataMember]
    public string MobilePhone
    {
        get
        {
            return GetAttributeValue<string>("mobilephone");
        }
        set
        {
            SetAttributeValue("mobilephone", value);
        }
    }

    /// <summary>
    /// Shows who last updated the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Shows the external party who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedbyexternalparty")]
    [DataMember]
    public EntityReference ModifiedByExternalParty
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedbyexternalparty");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was last updated. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Shows who last updated the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Delivery_Status
    /// </summary>
    [AttributeLogicalName("new_delivery_status")]
    [DataMember]
    public string New_Delivery_Status
    {
        get
        {
            return GetAttributeValue<string>("new_delivery_status");
        }
        set
        {
            SetAttributeValue("new_delivery_status", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("new_fusionscore")]
    [DataMember]
    public int? new_FusionScore
    {
        get
        {
            return GetAttributeValue<int?>("new_fusionscore");
        }
        set
        {
            SetAttributeValue("new_fusionscore", value);
        }
    }

    /// <summary>
    /// Type the contact's nickname.
    /// </summary>
    [AttributeLogicalName("nickname")]
    [DataMember]
    public string NickName
    {
        get
        {
            return GetAttributeValue<string>("nickname");
        }
        set
        {
            SetAttributeValue("nickname", value);
        }
    }

    /// <summary>
    /// Type the number of children the contact has for reference in follow-up phone calls and other communications.
    /// </summary>
    [AttributeLogicalName("numberofchildren")]
    [DataMember]
    public int? NumberOfChildren
    {
        get
        {
            return GetAttributeValue<int?>("numberofchildren");
        }
        set
        {
            SetAttributeValue("numberofchildren", value);
        }
    }

    /// <summary>
    /// Shows how long, in minutes, that the record was on hold.
    /// </summary>
    [AttributeLogicalName("onholdtime")]
    [DataMember]
    public int? OnHoldTime
    {
        get
        {
            return GetAttributeValue<int?>("onholdtime");
        }
    }

    /// <summary>
    /// Shows the lead that the contact was created if the contact was created by converting a lead in Microsoft Dynamics 365. This is used to relate the contact to the data on the originating lead for use in reporting and analytics.
    /// </summary>
    [AttributeLogicalName("originatingleadid")]
    [DataMember]
    public EntityReference OriginatingLeadId
    {
        get
        {
            return GetAttributeValue<EntityReference>("originatingleadid");
        }
        set
        {
            SetAttributeValue("originatingleadid", value);
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Enter the user or team who is assigned to manage the record. This field is updated every time the record is assigned to a different user.
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the business unit that owns the contact.
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier of the team who owns the contact.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    [DataMember]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier of the user who owns the contact.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// Type the pager number for the contact.
    /// </summary>
    [AttributeLogicalName("pager")]
    [DataMember]
    public string Pager
    {
        get
        {
            return GetAttributeValue<string>("pager");
        }
        set
        {
            SetAttributeValue("pager", value);
        }
    }

    /// <summary>
    /// Unique identifier of the parent contact.
    /// </summary>
    [AttributeLogicalName("parentcontactid")]
    [DataMember]
    public EntityReference ParentContactId
    {
        get
        {
            return GetAttributeValue<EntityReference>("parentcontactid");
        }
    }

    /// <summary>
    /// Select the parent account or parent contact for the contact to provide a quick link to additional details, such as financial information, activities, and opportunities.
    /// </summary>
    [AttributeLogicalName("parentcustomerid")]
    [DataMember]
    public EntityReference ParentCustomerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("parentcustomerid");
        }
        set
        {
            SetAttributeValue("parentcustomerid", value);
        }
    }

    /// <summary>
    /// Shows whether the contact participates in workflow rules.
    /// </summary>
    [AttributeLogicalName("participatesinworkflow")]
    [DataMember]
    public bool? ParticipatesInWorkflow
    {
        get
        {
            return GetAttributeValue<bool?>("participatesinworkflow");
        }
        set
        {
            SetAttributeValue("participatesinworkflow", value);
        }
    }

    /// <summary>
    /// Select the payment terms to indicate when the customer needs to pay the total amount.
    /// </summary>
    [AttributeLogicalName("paymenttermscode")]
    [DataMember]
    public OptionSetValue PaymentTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("paymenttermscode");
        }
        set
        {
            SetAttributeValue("paymenttermscode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessdivisionid")]
    [DataMember]
    public EntityReference po_BusinessDivisionid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessdivisionid");
        }
        set
        {
            SetAttributeValue("po_businessdivisionid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cintergy_contact_id")]
    [DataMember]
    public int? po_Cintergy_Contact_ID
    {
        get
        {
            return GetAttributeValue<int?>("po_cintergy_contact_id");
        }
        set
        {
            SetAttributeValue("po_cintergy_contact_id", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cintergy_contactid")]
    [DataMember]
    public string po_Cintergy_ContactID
    {
        get
        {
            return GetAttributeValue<string>("po_cintergy_contactid");
        }
        set
        {
            SetAttributeValue("po_cintergy_contactid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cintergy_createdby")]
    [DataMember]
    public string po_cintergy_createdby
    {
        get
        {
            return GetAttributeValue<string>("po_cintergy_createdby");
        }
        set
        {
            SetAttributeValue("po_cintergy_createdby", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cintergydatecreated")]
    [DataMember]
    public DateTime? po_CintergyDateCreated
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_cintergydatecreated");
        }
        set
        {
            SetAttributeValue("po_cintergydatecreated", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_createdby_userid")]
    [DataMember]
    public int? po_createdby_userid
    {
        get
        {
            return GetAttributeValue<int?>("po_createdby_userid");
        }
        set
        {
            SetAttributeValue("po_createdby_userid", value);
        }
    }

    /// <summary>
    /// Cintergy import fields
    /// </summary>
    [AttributeLogicalName("po_markfordelete")]
    [DataMember]
    public bool? po_MarkforDelete
    {
        get
        {
            return GetAttributeValue<bool?>("po_markfordelete");
        }
        set
        {
            SetAttributeValue("po_markfordelete", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_primary")]
    [DataMember]
    public bool? po_Primary
    {
        get
        {
            return GetAttributeValue<bool?>("po_primary");
        }
        set
        {
            SetAttributeValue("po_primary", value);
        }
    }

    /// <summary>
    /// Select the preferred day of the week for service appointments.
    /// </summary>
    [AttributeLogicalName("preferredappointmentdaycode")]
    [DataMember]
    public OptionSetValue PreferredAppointmentDayCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredappointmentdaycode");
        }
        set
        {
            SetAttributeValue("preferredappointmentdaycode", value);
        }
    }

    /// <summary>
    /// Select the preferred time of day for service appointments.
    /// </summary>
    [AttributeLogicalName("preferredappointmenttimecode")]
    [DataMember]
    public OptionSetValue PreferredAppointmentTimeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredappointmenttimecode");
        }
        set
        {
            SetAttributeValue("preferredappointmenttimecode", value);
        }
    }

    /// <summary>
    /// Select the preferred method of contact.
    /// </summary>
    [AttributeLogicalName("preferredcontactmethodcode")]
    [DataMember]
    public OptionSetValue PreferredContactMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredcontactmethodcode");
        }
        set
        {
            SetAttributeValue("preferredcontactmethodcode", value);
        }
    }

    /// <summary>
    /// Choose the contact's preferred service facility or equipment to make sure services are scheduled correctly for the customer.
    /// </summary>
    [AttributeLogicalName("preferredequipmentid")]
    [DataMember]
    public EntityReference PreferredEquipmentId
    {
        get
        {
            return GetAttributeValue<EntityReference>("preferredequipmentid");
        }
        set
        {
            SetAttributeValue("preferredequipmentid", value);
        }
    }

    /// <summary>
    /// Choose the contact's preferred service to make sure services are scheduled correctly for the customer.
    /// </summary>
    [AttributeLogicalName("preferredserviceid")]
    [DataMember]
    public EntityReference PreferredServiceId
    {
        get
        {
            return GetAttributeValue<EntityReference>("preferredserviceid");
        }
        set
        {
            SetAttributeValue("preferredserviceid", value);
        }
    }

    /// <summary>
    /// Choose the regular or preferred customer service representative for reference when scheduling service activities for the contact.
    /// </summary>
    [AttributeLogicalName("preferredsystemuserid")]
    [DataMember]
    public EntityReference PreferredSystemUserId
    {
        get
        {
            return GetAttributeValue<EntityReference>("preferredsystemuserid");
        }
        set
        {
            SetAttributeValue("preferredsystemuserid", value);
        }
    }

    /// <summary>
    /// Shows the ID of the process.
    /// </summary>
    [AttributeLogicalName("processid")]
    [DataMember]
    public Guid? ProcessId
    {
        get
        {
            return GetAttributeValue<Guid?>("processid");
        }
        set
        {
            SetAttributeValue("processid", value);
        }
    }

    /// <summary>
    /// Type the salutation of the contact to make sure the contact is addressed correctly in sales calls, email messages, and marketing campaigns.
    /// </summary>
    [AttributeLogicalName("salutation")]
    [DataMember]
    public string Salutation
    {
        get
        {
            return GetAttributeValue<string>("salutation");
        }
        set
        {
            SetAttributeValue("salutation", value);
        }
    }

    /// <summary>
    /// Select a shipping method for deliveries sent to this address.
    /// </summary>
    [AttributeLogicalName("shippingmethodcode")]
    [DataMember]
    public OptionSetValue ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("shippingmethodcode");
        }
        set
        {
            SetAttributeValue("shippingmethodcode", value);
        }
    }

    /// <summary>
    /// Choose the service level agreement (SLA) that you want to apply to the Contact record.
    /// </summary>
    [AttributeLogicalName("slaid")]
    [DataMember]
    public EntityReference SLAId
    {
        get
        {
            return GetAttributeValue<EntityReference>("slaid");
        }
        set
        {
            SetAttributeValue("slaid", value);
        }
    }

    /// <summary>
    /// Last SLA that was applied to this case. This field is for internal use only.
    /// </summary>
    [AttributeLogicalName("slainvokedid")]
    [DataMember]
    public EntityReference SLAInvokedId
    {
        get
        {
            return GetAttributeValue<EntityReference>("slainvokedid");
        }
    }

    /// <summary>
    /// Type the name of the contact's spouse or partner for reference during calls, events, or other communications with the contact.
    /// </summary>
    [AttributeLogicalName("spousesname")]
    [DataMember]
    public string SpousesName
    {
        get
        {
            return GetAttributeValue<string>("spousesname");
        }
        set
        {
            SetAttributeValue("spousesname", value);
        }
    }

    /// <summary>
    /// Shows the ID of the stage.
    /// </summary>
    [AttributeLogicalName("stageid")]
    [DataMember]
    public Guid? StageId
    {
        get
        {
            return GetAttributeValue<Guid?>("stageid");
        }
        set
        {
            SetAttributeValue("stageid", value);
        }
    }

    /// <summary>
    /// Shows whether the contact is active or inactive. Inactive contacts are read-only and can't be edited unless they are reactivated.
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public ContactState? StateCode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (ContactState)Enum.ToObject(typeof(ContactState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Select the contact's status.
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("subscriptionid")]
    [DataMember]
    public Guid? SubscriptionId
    {
        get
        {
            return GetAttributeValue<Guid?>("subscriptionid");
        }
        set
        {
            SetAttributeValue("subscriptionid", value);
        }
    }

    /// <summary>
    /// Type the suffix used in the contact's name, such as Jr. or Sr. to make sure the contact is addressed correctly in sales calls, email, and marketing campaigns.
    /// </summary>
    [AttributeLogicalName("suffix")]
    [DataMember]
    public string Suffix
    {
        get
        {
            return GetAttributeValue<string>("suffix");
        }
        set
        {
            SetAttributeValue("suffix", value);
        }
    }

    /// <summary>
    /// Type the main phone number for this contact.
    /// </summary>
    [AttributeLogicalName("telephone1")]
    [DataMember]
    public string Telephone1
    {
        get
        {
            return GetAttributeValue<string>("telephone1");
        }
        set
        {
            SetAttributeValue("telephone1", value);
        }
    }

    /// <summary>
    /// Type a second phone number for this contact.
    /// </summary>
    [AttributeLogicalName("telephone2")]
    [DataMember]
    public string Telephone2
    {
        get
        {
            return GetAttributeValue<string>("telephone2");
        }
        set
        {
            SetAttributeValue("telephone2", value);
        }
    }

    /// <summary>
    /// Type a third phone number for this contact.
    /// </summary>
    [AttributeLogicalName("telephone3")]
    [DataMember]
    public string Telephone3
    {
        get
        {
            return GetAttributeValue<string>("telephone3");
        }
        set
        {
            SetAttributeValue("telephone3", value);
        }
    }

    /// <summary>
    /// Select a region or territory for the contact for use in segmentation and analysis.
    /// </summary>
    [AttributeLogicalName("territorycode")]
    [DataMember]
    public OptionSetValue TerritoryCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("territorycode");
        }
        set
        {
            SetAttributeValue("territorycode", value);
        }
    }

    /// <summary>
    /// Total time spent for emails (read and write) and meetings by me in relation to the contact record.
    /// </summary>
    [AttributeLogicalName("timespentbymeonemailandmeetings")]
    [DataMember]
    public string TimeSpentByMeOnEmailAndMeetings
    {
        get
        {
            return GetAttributeValue<string>("timespentbymeonemailandmeetings");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Choose the local currency for the record to make sure budgets are reported in the correct currency.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("traversedpath")]
    [DataMember]
    public string TraversedPath
    {
        get
        {
            return GetAttributeValue<string>("traversedpath");
        }
        set
        {
            SetAttributeValue("traversedpath", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version number of the contact.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_acceptedtermsconditions")]
    [DataMember]
    public bool? vtx_acceptedtermsconditions
    {
        get
        {
            return GetAttributeValue<bool?>("vtx_acceptedtermsconditions");
        }
        set
        {
            SetAttributeValue("vtx_acceptedtermsconditions", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_acceptedtermsconditionsdate")]
    [DataMember]
    public DateTime? vtx_AcceptedTermsConditionsDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("vtx_acceptedtermsconditionsdate");
        }
        set
        {
            SetAttributeValue("vtx_acceptedtermsconditionsdate", value);
        }
    }

    /// <summary>
    /// This will be used as a way to change the Vertex user's password, if a value is present here then the plugin 'ContactPasswordEncryption' will encrypt the password and update the vtx_password column.
    /// </summary>
    [AttributeLogicalName("vtx_changepasswordto")]
    [DataMember]
    public string vtx_changepasswordto
    {
        get
        {
            return GetAttributeValue<string>("vtx_changepasswordto");
        }
        set
        {
            SetAttributeValue("vtx_changepasswordto", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_createdfromportal")]
    [DataMember]
    public bool? vtx_CreatedFromPortal
    {
        get
        {
            return GetAttributeValue<bool?>("vtx_createdfromportal");
        }
        set
        {
            SetAttributeValue("vtx_createdfromportal", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_donotcreatedefaultroles")]
    [DataMember]
    public bool? vtx_DoNotCreateDefaultRoles
    {
        get
        {
            return GetAttributeValue<bool?>("vtx_donotcreatedefaultroles");
        }
        set
        {
            SetAttributeValue("vtx_donotcreatedefaultroles", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_password")]
    [DataMember]
    public string vtx_Password
    {
        get
        {
            return GetAttributeValue<string>("vtx_password");
        }
        set
        {
            SetAttributeValue("vtx_password", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_passwordexpirationdate")]
    [DataMember]
    public DateTime? vtx_PasswordExpirationDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("vtx_passwordexpirationdate");
        }
        set
        {
            SetAttributeValue("vtx_passwordexpirationdate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_portaluser")]
    [DataMember]
    public bool? vtx_PortalUser
    {
        get
        {
            return GetAttributeValue<bool?>("vtx_portaluser");
        }
        set
        {
            SetAttributeValue("vtx_portaluser", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_rejectedtermsconditionsdate")]
    [DataMember]
    public DateTime? vtx_RejectedTermsConditionsDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("vtx_rejectedtermsconditionsdate");
        }
        set
        {
            SetAttributeValue("vtx_rejectedtermsconditionsdate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_requirepasswordchange")]
    [DataMember]
    public bool? vtx_RequirePasswordChange
    {
        get
        {
            return GetAttributeValue<bool?>("vtx_requirepasswordchange");
        }
        set
        {
            SetAttributeValue("vtx_requirepasswordchange", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("vtx_username")]
    [DataMember]
    public string vtx_Username
    {
        get
        {
            return GetAttributeValue<string>("vtx_username");
        }
        set
        {
            SetAttributeValue("vtx_username", value);
        }
    }

    /// <summary>
    /// Type the contact's professional or personal website or blog URL.
    /// </summary>
    [AttributeLogicalName("websiteurl")]
    [DataMember]
    public string WebSiteUrl
    {
        get
        {
            return GetAttributeValue<string>("websiteurl");
        }
        set
        {
            SetAttributeValue("websiteurl", value);
        }
    }

    /// <summary>
    /// Type the phonetic spelling of the contact's first name, if the name is specified in Japanese, to make sure the name is pronounced correctly in phone calls with the contact.
    /// </summary>
    [AttributeLogicalName("yomifirstname")]
    [DataMember]
    public string YomiFirstName
    {
        get
        {
            return GetAttributeValue<string>("yomifirstname");
        }
        set
        {
            SetAttributeValue("yomifirstname", value);
        }
    }

    /// <summary>
    /// Shows the combined Yomi first and last names of the contact so that the full phonetic name can be displayed in views and reports.
    /// </summary>
    [AttributeLogicalName("yomifullname")]
    [DataMember]
    public string YomiFullName
    {
        get
        {
            return GetAttributeValue<string>("yomifullname");
        }
    }

    /// <summary>
    /// Type the phonetic spelling of the contact's last name, if the name is specified in Japanese, to make sure the name is pronounced correctly in phone calls with the contact.
    /// </summary>
    [AttributeLogicalName("yomilastname")]
    [DataMember]
    public string YomiLastName
    {
        get
        {
            return GetAttributeValue<string>("yomilastname");
        }
        set
        {
            SetAttributeValue("yomilastname", value);
        }
    }

    /// <summary>
    /// Type the phonetic spelling of the contact's middle name, if the name is specified in Japanese, to make sure the name is pronounced correctly in phone calls with the contact.
    /// </summary>
    [AttributeLogicalName("yomimiddlename")]
    [DataMember]
    public string YomiMiddleName
    {
        get
        {
            return GetAttributeValue<string>("yomimiddlename");
        }
        set
        {
            SetAttributeValue("yomimiddlename", value);
        }
    }
}

[DataContract]
public enum ContactState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}