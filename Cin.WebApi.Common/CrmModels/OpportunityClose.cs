﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("opportunityclose")]
public class OpportunityClose : Entity
{
    /// <summary>
    /// Unique identifier of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("activityid")]
    [DataMember]
    public Guid? ActivityId
    {
        get
        {
            return GetAttributeValue<Guid?>("activityid");
        }
        set
        {
            SetAttributeValue("activityid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("activityid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            ActivityId = value;
        }
    }

    /// <summary>
    /// Type of activity.
    /// </summary>
    [AttributeLogicalName("activitytypecode")]
    [DataMember]
    public string ActivityTypeCode
    {
        get
        {
            return GetAttributeValue<string>("activitytypecode");
        }
        set
        {
            SetAttributeValue("activitytypecode", value);
        }
    }

    /// <summary>
    /// Actual duration of the opportunity close activity in minutes.
    /// </summary>
    [AttributeLogicalName("actualdurationminutes")]
    [DataMember]
    public int? ActualDurationMinutes
    {
        get
        {
            return GetAttributeValue<int?>("actualdurationminutes");
        }
        set
        {
            SetAttributeValue("actualdurationminutes", value);
        }
    }

    /// <summary>
    /// Actual end time of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("actualend")]
    [DataMember]
    public DateTime? ActualEnd
    {
        get
        {
            return GetAttributeValue<DateTime?>("actualend");
        }
        set
        {
            SetAttributeValue("actualend", value);
        }
    }

    /// <summary>
    /// Actual revenue generated for the opportunity.
    /// </summary>
    [AttributeLogicalName("actualrevenue")]
    [DataMember]
    public Money ActualRevenue
    {
        get
        {
            return GetAttributeValue<Money>("actualrevenue");
        }
        set
        {
            SetAttributeValue("actualrevenue", value);
        }
    }

    /// <summary>
    /// Shows the Actual Revenue field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("actualrevenue_base")]
    [DataMember]
    public Money ActualRevenue_Base
    {
        get
        {
            return GetAttributeValue<Money>("actualrevenue_base");
        }
        set
        {
            SetAttributeValue("actualrevenue_base", value);
        }
    }

    /// <summary>
    /// Actual start time of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("actualstart")]
    [DataMember]
    public DateTime? ActualStart
    {
        get
        {
            return GetAttributeValue<DateTime?>("actualstart");
        }
        set
        {
            SetAttributeValue("actualstart", value);
        }
    }

    /// <summary>
    /// Category of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("category")]
    [DataMember]
    public string Category
    {
        get
        {
            return GetAttributeValue<string>("category");
        }
        set
        {
            SetAttributeValue("category", value);
        }
    }

    /// <summary>
    /// Unique identifier of the competitor with which the opportunity close activity is associated.
    /// </summary>
    [AttributeLogicalName("competitorid")]
    [DataMember]
    public EntityReference CompetitorId
    {
        get
        {
            return GetAttributeValue<EntityReference>("competitorid");
        }
        set
        {
            SetAttributeValue("competitorid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who created the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
        set
        {
            SetAttributeValue("createdby", value);
        }
    }

    /// <summary>
    /// Shows the external party who created the record.
    /// </summary>
    [AttributeLogicalName("createdbyexternalparty")]
    [DataMember]
    public EntityReference CreatedByExternalParty
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdbyexternalparty");
        }
        set
        {
            SetAttributeValue("createdbyexternalparty", value);
        }
    }

    /// <summary>
    /// Date and time when the opportunity close activity was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
        set
        {
            SetAttributeValue("createdon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the opportunityclose.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
        set
        {
            SetAttributeValue("createdonbehalfby", value);
        }
    }

    /// <summary>
    /// Activity that is created automatically when an opportunity is closed, containing information such as the description of the closing and actual revenue.
    /// </summary>
    [AttributeLogicalName("description")]
    [DataMember]
    public string Description
    {
        get
        {
            return GetAttributeValue<string>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /// <summary>
    /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
        set
        {
            SetAttributeValue("exchangerate", value);
        }
    }

    /// <summary>
    /// Unique identifier of the data import or data migration that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Information about whether the opportunity close activity was billed as part of resolving a case.
    /// </summary>
    [AttributeLogicalName("isbilled")]
    [DataMember]
    public bool? IsBilled
    {
        get
        {
            return GetAttributeValue<bool?>("isbilled");
        }
        set
        {
            SetAttributeValue("isbilled", value);
        }
    }

    /// <summary>
    /// Information regarding whether the activity is a regular activity type or event type.
    /// </summary>
    [AttributeLogicalName("isregularactivity")]
    [DataMember]
    public bool? IsRegularActivity
    {
        get
        {
            return GetAttributeValue<bool?>("isregularactivity");
        }
        set
        {
            SetAttributeValue("isregularactivity", value);
        }
    }

    /// <summary>
    /// Information that specifies if the opportunity close activity was created from a workflow rule.
    /// </summary>
    [AttributeLogicalName("isworkflowcreated")]
    [DataMember]
    public bool? IsWorkflowCreated
    {
        get
        {
            return GetAttributeValue<bool?>("isworkflowcreated");
        }
        set
        {
            SetAttributeValue("isworkflowcreated", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who last modified the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
        set
        {
            SetAttributeValue("modifiedby", value);
        }
    }

    /// <summary>
    /// Shows the external party who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedbyexternalparty")]
    [DataMember]
    public EntityReference ModifiedByExternalParty
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedbyexternalparty");
        }
        set
        {
            SetAttributeValue("modifiedbyexternalparty", value);
        }
    }

    /// <summary>
    /// Date and time when the opportunity close activity was last modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
        set
        {
            SetAttributeValue("modifiedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who last modified the opportunityclose.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
        set
        {
            SetAttributeValue("modifiedonbehalfby", value);
        }
    }

    /// <summary>
    /// Unique identifier of the opportunity closed.
    /// </summary>
    [AttributeLogicalName("opportunityid")]
    [DataMember]
    public EntityReference OpportunityId
    {
        get
        {
            return GetAttributeValue<EntityReference>("opportunityid");
        }
        set
        {
            SetAttributeValue("opportunityid", value);
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user or team who owns the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the business unit that owns the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
        set
        {
            SetAttributeValue("owningbusinessunit", value);
        }
    }

    /// <summary>
    /// Unique identifier of the team who owns the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    [DataMember]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
        set
        {
            SetAttributeValue("owningteam", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who owns the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
        set
        {
            SetAttributeValue("owninguser", value);
        }
    }

    /// <summary>
    /// Scheduled duration of the opportunity close activity, specified in minutes.
    /// </summary>
    [AttributeLogicalName("scheduleddurationminutes")]
    [DataMember]
    public int? ScheduledDurationMinutes
    {
        get
        {
            return GetAttributeValue<int?>("scheduleddurationminutes");
        }
        set
        {
            SetAttributeValue("scheduleddurationminutes", value);
        }
    }

    /// <summary>
    /// Scheduled end time of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("scheduledend")]
    [DataMember]
    public DateTime? ScheduledEnd
    {
        get
        {
            return GetAttributeValue<DateTime?>("scheduledend");
        }
        set
        {
            SetAttributeValue("scheduledend", value);
        }
    }

    /// <summary>
    /// Scheduled start time of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("scheduledstart")]
    [DataMember]
    public DateTime? ScheduledStart
    {
        get
        {
            return GetAttributeValue<DateTime?>("scheduledstart");
        }
        set
        {
            SetAttributeValue("scheduledstart", value);
        }
    }

    /// <summary>
    /// Unique identifier of the service with which the opportunity close activity is associated.
    /// </summary>
    [AttributeLogicalName("serviceid")]
    [DataMember]
    public EntityReference ServiceId
    {
        get
        {
            return GetAttributeValue<EntityReference>("serviceid");
        }
        set
        {
            SetAttributeValue("serviceid", value);
        }
    }

    /// <summary>
    /// Shows whether the opportunity close activity is open, completed, or canceled. By default, opportunity close activities are completed unless the opportunity is reactivated, which updates them to canceled.
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public OpportunityCloseState? StateCode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (OpportunityCloseState)Enum.ToObject(typeof(OpportunityCloseState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// Subcategory of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("subcategory")]
    [DataMember]
    public string Subcategory
    {
        get
        {
            return GetAttributeValue<string>("subcategory");
        }
        set
        {
            SetAttributeValue("subcategory", value);
        }
    }

    /// <summary>
    /// Subject associated with the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("subject")]
    [DataMember]
    public string Subject
    {
        get
        {
            return GetAttributeValue<string>("subject");
        }
        set
        {
            SetAttributeValue("subject", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Choose the local currency for the record to make sure budgets are reported in the correct currency.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version number of the opportunity close activity.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
        set
        {
            SetAttributeValue("versionnumber", value);
        }
    }
}

[DataContract]
public enum OpportunityCloseState
{

    [EnumMember()]
    Open = 0,

    [EnumMember()]
    Completed = 1,

    [EnumMember()]
    Canceled = 2,
}


[EntityLogicalName("closeopportunity")]
public class CloseOpportunity
{
    public int Status { get; set; }
    public CloseOpportunityActivity OpportunityClose { get; set; }

}
public class CloseOpportunityActivity
{
    public string OpportunityId { get; set; }
    public int OpportunityStateCode { get; set; }
    public int OpportunityStatusCode { get; set; }
    public string Subject { get; set; }
}