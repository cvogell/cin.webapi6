﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels;

[DataContract()]
[EntityLogicalName("account")]
public class Account : Entity
{

    public const string EntityLogicalName = "account";
    public const int EntityTypeCode = 1;

    /// <summary>
    /// Select a category to indicate whether the customer account is standard or preferred.
    /// </summary>
    [AttributeLogicalName("accountcategorycode")]
    [DataMember]
    public OptionSetValue AccountCategoryCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("accountcategorycode");
        }
        set
        {
            SetAttributeValue("accountcategorycode", value);
        }
    }

    /// <summary>
    /// Select a classification code to indicate the potential value of the customer account based on the projected return on investment, cooperation level, sales cycle length or other criteria.
    /// </summary>
    [AttributeLogicalName("accountclassificationcode")]
    [DataMember]
    public OptionSetValue AccountClassificationCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("accountclassificationcode");
        }
        set
        {
            SetAttributeValue("accountclassificationcode", value);
        }
    }

    /// <summary>
    /// Unique identifier of the account.
    /// </summary>
    [AttributeLogicalName("accountid")]
    [DataMember]
    public Guid? AccountId
    {
        get
        {
            return GetAttributeValue<Guid?>("accountid");
        }
        set
        {
            SetAttributeValue("accountid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("accountid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            AccountId = value;
        }
    }

    /// <summary>
    /// Type an ID number or code for the account to quickly search and identify the account in system views.
    /// </summary>
    [AttributeLogicalName("accountnumber")]
    [DataMember]
    public string AccountNumber
    {
        get
        {
            return GetAttributeValue<string>("accountnumber");
        }
        set
        {
            SetAttributeValue("accountnumber", value);
        }
    }

    /// <summary>
    /// Select a rating to indicate the value of the customer account.
    /// </summary>
    [AttributeLogicalName("accountratingcode")]
    [DataMember]
    public OptionSetValue AccountRatingCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("accountratingcode");
        }
        set
        {
            SetAttributeValue("accountratingcode", value);
        }
    }

    /// <summary>
    /// Unique identifier for address 1.
    /// </summary>
    [AttributeLogicalName("address1_addressid")]
    [DataMember]
    public Guid? Address1_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("address1_addressid");
        }
        set
        {
            SetAttributeValue("address1_addressid", value);
        }
    }

    /// <summary>
    /// Select the primary address type.
    /// </summary>
    [AttributeLogicalName("address1_addresstypecode")]
    [DataMember]
    public OptionSetValue Address1_AddressTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address1_addresstypecode");
        }
        set
        {
            SetAttributeValue("address1_addresstypecode", value);
        }
    }

    /// <summary>
    /// Type the city for the primary address.
    /// </summary>
    [AttributeLogicalName("address1_city")]
    [DataMember]
    public string Address1_City
    {
        get
        {
            return GetAttributeValue<string>("address1_city");
        }
        set
        {
            SetAttributeValue("address1_city", value);
        }
    }

    /// <summary>
    /// Shows the complete primary address.
    /// </summary>
    [AttributeLogicalName("address1_composite")]
    [DataMember]
    public string Address1_Composite
    {
        get
        {
            return GetAttributeValue<string>("address1_composite");
        }
    }

    /// <summary>
    /// Type the country or region for the primary address.
    /// </summary>
    [AttributeLogicalName("address1_country")]
    [DataMember]
    public string Address1_Country
    {
        get
        {
            return GetAttributeValue<string>("address1_country");
        }
        set
        {
            SetAttributeValue("address1_country", value);
        }
    }

    /// <summary>
    /// Type the county for the primary address.
    /// </summary>
    [AttributeLogicalName("address1_county")]
    [DataMember]
    public string Address1_County
    {
        get
        {
            return GetAttributeValue<string>("address1_county");
        }
        set
        {
            SetAttributeValue("address1_county", value);
        }
    }

    /// <summary>
    /// Type the fax number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address1_fax")]
    [DataMember]
    public string Address1_Fax
    {
        get
        {
            return GetAttributeValue<string>("address1_fax");
        }
        set
        {
            SetAttributeValue("address1_fax", value);
        }
    }

    /// <summary>
    /// Select the freight terms for the primary address to make sure shipping orders are processed correctly.
    /// </summary>
    [AttributeLogicalName("address1_freighttermscode")]
    [DataMember]
    public OptionSetValue Address1_FreightTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address1_freighttermscode");
        }
        set
        {
            SetAttributeValue("address1_freighttermscode", value);
        }
    }

    /// <summary>
    /// Type the latitude value for the primary address for use in mapping and other applications.
    /// </summary>
    [AttributeLogicalName("address1_latitude")]
    [DataMember]
    public double? Address1_Latitude
    {
        get
        {
            return GetAttributeValue<double?>("address1_latitude");
        }
        set
        {
            SetAttributeValue("address1_latitude", value);
        }
    }

    /// <summary>
    /// Type the first line of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_line1")]
    [DataMember]
    public string Address1_Line1
    {
        get
        {
            return GetAttributeValue<string>("address1_line1");
        }
        set
        {
            SetAttributeValue("address1_line1", value);
        }
    }

    /// <summary>
    /// Type the second line of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_line2")]
    [DataMember]
    public string Address1_Line2
    {
        get
        {
            return GetAttributeValue<string>("address1_line2");
        }
        set
        {
            SetAttributeValue("address1_line2", value);
        }
    }

    /// <summary>
    /// Type the third line of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_line3")]
    [DataMember]
    public string Address1_Line3
    {
        get
        {
            return GetAttributeValue<string>("address1_line3");
        }
        set
        {
            SetAttributeValue("address1_line3", value);
        }
    }

    /// <summary>
    /// Type the longitude value for the primary address for use in mapping and other applications.
    /// </summary>
    [AttributeLogicalName("address1_longitude")]
    [DataMember]
    public double? Address1_Longitude
    {
        get
        {
            return GetAttributeValue<double?>("address1_longitude");
        }
        set
        {
            SetAttributeValue("address1_longitude", value);
        }
    }

    /// <summary>
    /// Type a descriptive name for the primary address, such as Corporate Headquarters.
    /// </summary>
    [AttributeLogicalName("address1_name")]
    [DataMember]
    public string Address1_Name
    {
        get
        {
            return GetAttributeValue<string>("address1_name");
        }
        set
        {
            SetAttributeValue("address1_name", value);
        }
    }

    /// <summary>
    /// Type the ZIP Code or postal code for the primary address.
    /// </summary>
    [AttributeLogicalName("address1_postalcode")]
    [DataMember]
    public string Address1_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("address1_postalcode");
        }
        set
        {
            SetAttributeValue("address1_postalcode", value);
        }
    }

    /// <summary>
    /// Type the post office box number of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_postofficebox")]
    [DataMember]
    public string Address1_PostOfficeBox
    {
        get
        {
            return GetAttributeValue<string>("address1_postofficebox");
        }
        set
        {
            SetAttributeValue("address1_postofficebox", value);
        }
    }

    /// <summary>
    /// Type the name of the main contact at the account's primary address.
    /// </summary>
    [AttributeLogicalName("address1_primarycontactname")]
    [DataMember]
    public string Address1_PrimaryContactName
    {
        get
        {
            return GetAttributeValue<string>("address1_primarycontactname");
        }
        set
        {
            SetAttributeValue("address1_primarycontactname", value);
        }
    }

    /// <summary>
    /// Select a shipping method for deliveries sent to this address.
    /// </summary>
    [AttributeLogicalName("address1_shippingmethodcode")]
    [DataMember]
    public OptionSetValue Address1_ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address1_shippingmethodcode");
        }
        set
        {
            SetAttributeValue("address1_shippingmethodcode", value);
        }
    }

    /// <summary>
    /// Type the state or province of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_stateorprovince")]
    [DataMember]
    public string Address1_stateorprovince
    {
        get
        {
            return GetAttributeValue<string>("address1_stateorprovince");
        }
        set
        {
            SetAttributeValue("address1_stateorprovince", value);
        }
    }

    /// <summary>
    /// Type the main phone number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address1_telephone1")]
    [DataMember]
    public string Address1_Telephone1
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone1");
        }
        set
        {
            SetAttributeValue("address1_telephone1", value);
        }
    }

    /// <summary>
    /// Type a second phone number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address1_telephone2")]
    [DataMember]
    public string Address1_Telephone2
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone2");
        }
        set
        {
            SetAttributeValue("address1_telephone2", value);
        }
    }

    /// <summary>
    /// Type a third phone number associated with the primary address.
    /// </summary>
    [AttributeLogicalName("address1_telephone3")]
    [DataMember]
    public string Address1_Telephone3
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone3");
        }
        set
        {
            SetAttributeValue("address1_telephone3", value);
        }
    }

    /// <summary>
    /// Type the UPS zone of the primary address to make sure shipping charges are calculated correctly and deliveries are made promptly, if shipped by UPS.
    /// </summary>
    [AttributeLogicalName("address1_upszone")]
    [DataMember]
    public string Address1_UPSZone
    {
        get
        {
            return GetAttributeValue<string>("address1_upszone");
        }
        set
        {
            SetAttributeValue("address1_upszone", value);
        }
    }

    /// <summary>
    /// Select the time zone, or UTC offset, for this address so that other people can reference it when they contact someone at this address.
    /// </summary>
    [AttributeLogicalName("address1_utcoffset")]
    [DataMember]
    public int? Address1_UTCOffset
    {
        get
        {
            return GetAttributeValue<int?>("address1_utcoffset");
        }
        set
        {
            SetAttributeValue("address1_utcoffset", value);
        }
    }

    /// <summary>
    /// Unique identifier for address 2.
    /// </summary>
    [AttributeLogicalName("address2_addressid")]
    [DataMember]
    public Guid? Address2_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("address2_addressid");
        }
        set
        {
            SetAttributeValue("address2_addressid", value);
        }
    }

    /// <summary>
    /// Select the secondary address type.
    /// </summary>
    [AttributeLogicalName("address2_addresstypecode")]
    [DataMember]
    public OptionSetValue Address2_AddressTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address2_addresstypecode");
        }
        set
        {
            SetAttributeValue("address2_addresstypecode", value);
        }
    }

    /// <summary>
    /// Type the city for the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_city")]
    [DataMember]
    public string Address2_City
    {
        get
        {
            return GetAttributeValue<string>("address2_city");
        }
        set
        {
            SetAttributeValue("address2_city", value);
        }
    }

    /// <summary>
    /// Shows the complete secondary address.
    /// </summary>
    [AttributeLogicalName("address2_composite")]
    [DataMember]
    public string Address2_Composite
    {
        get
        {
            return GetAttributeValue<string>("address2_composite");
        }
    }

    /// <summary>
    /// Type the country or region for the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_country")]
    [DataMember]
    public string Address2_Country
    {
        get
        {
            return GetAttributeValue<string>("address2_country");
        }
        set
        {
            SetAttributeValue("address2_country", value);
        }
    }

    /// <summary>
    /// Type the county for the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_county")]
    [DataMember]
    public string Address2_County
    {
        get
        {
            return GetAttributeValue<string>("address2_county");
        }
        set
        {
            SetAttributeValue("address2_county", value);
        }
    }

    /// <summary>
    /// Type the fax number associated with the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_fax")]
    [DataMember]
    public string Address2_Fax
    {
        get
        {
            return GetAttributeValue<string>("address2_fax");
        }
        set
        {
            SetAttributeValue("address2_fax", value);
        }
    }

    /// <summary>
    /// Select the freight terms for the secondary address to make sure shipping orders are processed correctly.
    /// </summary>
    [AttributeLogicalName("address2_freighttermscode")]
    [DataMember]
    public OptionSetValue Address2_FreightTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address2_freighttermscode");
        }
        set
        {
            SetAttributeValue("address2_freighttermscode", value);
        }
    }

    /// <summary>
    /// Type the latitude value for the secondary address for use in mapping and other applications.
    /// </summary>
    [AttributeLogicalName("address2_latitude")]
    [DataMember]
    public double? Address2_Latitude
    {
        get
        {
            return GetAttributeValue<double?>("address2_latitude");
        }
        set
        {
            SetAttributeValue("address2_latitude", value);
        }
    }

    /// <summary>
    /// Type the first line of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_line1")]
    [DataMember]
    public string Address2_Line1
    {
        get
        {
            return GetAttributeValue<string>("address2_line1");
        }
        set
        {
            SetAttributeValue("address2_line1", value);
        }
    }

    /// <summary>
    /// Type the second line of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_line2")]
    [DataMember]
    public string Address2_Line2
    {
        get
        {
            return GetAttributeValue<string>("address2_line2");
        }
        set
        {
            SetAttributeValue("address2_line2", value);
        }
    }

    /// <summary>
    /// Type the third line of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_line3")]
    [DataMember]
    public string Address2_Line3
    {
        get
        {
            return GetAttributeValue<string>("address2_line3");
        }
        set
        {
            SetAttributeValue("address2_line3", value);
        }
    }

    /// <summary>
    /// Type the longitude value for the secondary address for use in mapping and other applications.
    /// </summary>
    [AttributeLogicalName("address2_longitude")]
    [DataMember]
    public double? Address2_Longitude
    {
        get
        {
            return GetAttributeValue<double?>("address2_longitude");
        }
        set
        {
            SetAttributeValue("address2_longitude", value);
        }
    }

    /// <summary>
    /// Type a descriptive name for the secondary address, such as Corporate Headquarters.
    /// </summary>
    [AttributeLogicalName("address2_name")]
    [DataMember]
    public string Address2_Name
    {
        get
        {
            return GetAttributeValue<string>("address2_name");
        }
        set
        {
            SetAttributeValue("address2_name", value);
        }
    }

    /// <summary>
    /// Type the ZIP Code or postal code for the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_postalcode")]
    [DataMember]
    public string Address2_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("address2_postalcode");
        }
        set
        {
            SetAttributeValue("address2_postalcode", value);
        }
    }

    /// <summary>
    /// Type the post office box number of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_postofficebox")]
    [DataMember]
    public string Address2_PostOfficeBox
    {
        get
        {
            return GetAttributeValue<string>("address2_postofficebox");
        }
        set
        {
            SetAttributeValue("address2_postofficebox", value);
        }
    }

    /// <summary>
    /// Type the name of the main contact at the account's secondary address.
    /// </summary>
    [AttributeLogicalName("address2_primarycontactname")]
    [DataMember]
    public string Address2_PrimaryContactName
    {
        get
        {
            return GetAttributeValue<string>("address2_primarycontactname");
        }
        set
        {
            SetAttributeValue("address2_primarycontactname", value);
        }
    }

    /// <summary>
    /// Select a shipping method for deliveries sent to this address.
    /// </summary>
    [AttributeLogicalName("address2_shippingmethodcode")]
    [DataMember]
    public OptionSetValue Address2_ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address2_shippingmethodcode");
        }
        set
        {
            SetAttributeValue("address2_shippingmethodcode", value);
        }
    }

    /// <summary>
    /// Type the state or province of the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_stateorprovince")]
    [DataMember]
    public string Address2_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("address2_stateorprovince");
        }
        set
        {
            SetAttributeValue("address2_stateorprovince", value);
        }
    }

    /// <summary>
    /// Type the main phone number associated with the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_telephone1")]
    [DataMember]
    public string Address2_Telephone1
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone1");
        }
        set
        {
            SetAttributeValue("address2_telephone1", value);
        }
    }

    /// <summary>
    /// Type a second phone number associated with the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_telephone2")]
    [DataMember]
    public string Address2_Telephone2
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone2");
        }
        set
        {
            SetAttributeValue("address2_telephone2", value);
        }
    }

    /// <summary>
    /// Type a third phone number associated with the secondary address.
    /// </summary>
    [AttributeLogicalName("address2_telephone3")]
    [DataMember]
    public string Address2_Telephone3
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone3");
        }
        set
        {
            SetAttributeValue("address2_telephone3", value);
        }
    }

    /// <summary>
    /// Type the UPS zone of the secondary address to make sure shipping charges are calculated correctly and deliveries are made promptly, if shipped by UPS.
    /// </summary>
    [AttributeLogicalName("address2_upszone")]
    [DataMember]
    public string Address2_UPSZone
    {
        get
        {
            return GetAttributeValue<string>("address2_upszone");
        }
        set
        {
            SetAttributeValue("address2_upszone", value);
        }
    }

    /// <summary>
    /// Select the time zone, or UTC offset, for this address so that other people can reference it when they contact someone at this address.
    /// </summary>
    [AttributeLogicalName("address2_utcoffset")]
    [DataMember]
    public int? Address2_UTCOffset
    {
        get
        {
            return GetAttributeValue<int?>("address2_utcoffset");
        }
        set
        {
            SetAttributeValue("address2_utcoffset", value);
        }
    }

    /// <summary>
    /// For system use only.
    /// </summary>
    [AttributeLogicalName("aging30")]
    [DataMember]
    public Money Aging30
    {
        get
        {
            return GetAttributeValue<Money>("aging30");
        }
    }

    /// <summary>
    /// The base currency equivalent of the aging 30 field.
    /// </summary>
    [AttributeLogicalName("aging30_base")]
    [DataMember]
    public Money Aging30_Base
    {
        get
        {
            return GetAttributeValue<Money>("aging30_base");
        }
    }

    /// <summary>
    /// For system use only.
    /// </summary>
    [AttributeLogicalName("aging60")]
    [DataMember]
    public Money Aging60
    {
        get
        {
            return GetAttributeValue<Money>("aging60");
        }
    }

    /// <summary>
    /// The base currency equivalent of the aging 60 field.
    /// </summary>
    [AttributeLogicalName("aging60_base")]
    [DataMember]
    public Money Aging60_Base
    {
        get
        {
            return GetAttributeValue<Money>("aging60_base");
        }
    }

    /// <summary>
    /// For system use only.
    /// </summary>
    [AttributeLogicalName("aging90")]
    [DataMember]
    public Money Aging90
    {
        get
        {
            return GetAttributeValue<Money>("aging90");
        }
    }

    /// <summary>
    /// The base currency equivalent of the aging 90 field.
    /// </summary>
    [AttributeLogicalName("aging90_base")]
    [DataMember]
    public Money Aging90_Base
    {
        get
        {
            return GetAttributeValue<Money>("aging90_base");
        }
    }

    /// <summary>
    /// Select the legal designation or other business type of the account for contracts or reporting purposes.
    /// </summary>
    [AttributeLogicalName("businesstypecode")]
    [DataMember]
    public OptionSetValue BusinessTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("businesstypecode");
        }
        set
        {
            SetAttributeValue("businesstypecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("cdi_allowtextmessages")]
    [DataMember]
    public bool? cdi_allowtextmessages
    {
        get
        {
            return GetAttributeValue<bool?>("cdi_allowtextmessages");
        }
        set
        {
            SetAttributeValue("cdi_allowtextmessages", value);
        }
    }

    /// <summary>
    /// Shows who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Shows the external party who created the record.
    /// </summary>
    [AttributeLogicalName("createdbyexternalparty")]
    [DataMember]
    public EntityReference CreatedByExternalParty
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdbyexternalparty");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Type the credit limit of the account. This is a useful reference when you address invoice and accounting issues with the customer.
    /// </summary>
    [AttributeLogicalName("creditlimit")]
    [DataMember]
    public Money CreditLimit
    {
        get
        {
            return GetAttributeValue<Money>("creditlimit");
        }
        set
        {
            SetAttributeValue("creditlimit", value);
        }
    }

    /// <summary>
    /// Shows the credit limit converted to the system's default base currency for reporting purposes.
    /// </summary>
    [AttributeLogicalName("creditlimit_base")]
    [DataMember]
    public Money CreditLimit_Base
    {
        get
        {
            return GetAttributeValue<Money>("creditlimit_base");
        }
    }

    /// <summary>
    /// Select whether the credit for the account is on hold. This is a useful reference while addressing the invoice and accounting issues with the customer.
    /// </summary>
    [AttributeLogicalName("creditonhold")]
    [DataMember]
    public bool? CreditOnHold
    {
        get
        {
            return GetAttributeValue<bool?>("creditonhold");
        }
        set
        {
            SetAttributeValue("creditonhold", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_account_info_changed")]
    [DataMember]
    public OptionSetValue ctas_Account_Info_Changed
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_account_info_changed");
        }
        set
        {
            SetAttributeValue("ctas_account_info_changed", value);
        }
    }

    /// <summary>
    /// Best Prospect for FS
    /// </summary>
    [AttributeLogicalName("ctas_best_prospect_fs")]
    [DataMember]
    public int? ctas_Best_Prospect_FS
    {
        get
        {
            return GetAttributeValue<int?>("ctas_best_prospect_fs");
        }
        set
        {
            SetAttributeValue("ctas_best_prospect_fs", value);
        }
    }

    /// <summary>
    /// Best Prospect Ratings FS
    /// </summary>
    [AttributeLogicalName("ctas_best_prospect_rating_fs")]
    [DataMember]
    public OptionSetValue ctas_Best_Prospect_Rating_FS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_best_prospect_rating_fs");
        }
        set
        {
            SetAttributeValue("ctas_best_prospect_rating_fs", value);
        }
    }

    /// <summary>
    /// Best Prospect Rating UR
    /// </summary>
    [AttributeLogicalName("ctas_best_prospect_rating_ur")]
    [DataMember]
    public OptionSetValue ctas_Best_Prospect_Rating_UR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_best_prospect_rating_ur");
        }
        set
        {
            SetAttributeValue("ctas_best_prospect_rating_ur", value);
        }
    }

    /// <summary>
    /// Best Prospect. Values range from 1 to 16.
    /// </summary>
    [AttributeLogicalName("ctas_best_prospect_ur")]
    [DataMember]
    public int? ctas_Best_Prospect_UR
    {
        get
        {
            return GetAttributeValue<int?>("ctas_best_prospect_ur");
        }
        set
        {
            SetAttributeValue("ctas_best_prospect_ur", value);
        }
    }

    /// <summary>
    /// Used for Call Center Campaign OSV
    /// </summary>
    [AttributeLogicalName("ctas_call_center_notes")]
    [DataMember]
    public string ctas_Call_Center_Notes
    {
        get
        {
            return GetAttributeValue<string>("ctas_call_center_notes");
        }
        set
        {
            SetAttributeValue("ctas_call_center_notes", value);
        }
    }

    /// <summary>
    /// Used for Call Center OSV
    /// </summary>
    [AttributeLogicalName("ctas_call_result")]
    [DataMember]
    public OptionSetValue ctas_Call_Result
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_call_result");
        }
        set
        {
            SetAttributeValue("ctas_call_result", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_category_fps_1")]
    [DataMember]
    public OptionSetValue ctas_Category_FPS_1
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_category_fps_1");
        }
        set
        {
            SetAttributeValue("ctas_category_fps_1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_category_fps_2")]
    [DataMember]
    public OptionSetValue ctas_Category_FPS_2
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_category_fps_2");
        }
        set
        {
            SetAttributeValue("ctas_category_fps_2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_category_fps_3")]
    [DataMember]
    public OptionSetValue ctas_Category_FPS_3
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_category_fps_3");
        }
        set
        {
            SetAttributeValue("ctas_category_fps_3", value);
        }
    }

    /// <summary>
    /// LU
    /// </summary>
    [AttributeLogicalName("ctas_cintascom_lu")]
    [DataMember]
    public EntityReference ctas_Cintascom_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_cintascom_lu");
        }
        set
        {
            SetAttributeValue("ctas_cintascom_lu", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_competitor_fps_2")]
    [DataMember]
    public EntityReference ctas_Competitor_FPS_2
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_competitor_fps_2");
        }
        set
        {
            SetAttributeValue("ctas_competitor_fps_2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_competitor_fps_3")]
    [DataMember]
    public EntityReference ctas_Competitor_FPS_3
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_competitor_fps_3");
        }
        set
        {
            SetAttributeValue("ctas_competitor_fps_3", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_competitor_update_date_ww")]
    [DataMember]
    public DateTime? ctas_Competitor_Update_Date_WW
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_competitor_update_date_ww");
        }
        set
        {
            SetAttributeValue("ctas_competitor_update_date_ww", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_competitor_ww")]
    [DataMember]
    public EntityReference ctas_Competitor_WW
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_competitor_ww");
        }
        set
        {
            SetAttributeValue("ctas_competitor_ww", value);
        }
    }

    /// <summary>
    /// This field indicates the record was created by lead coming in through Cintas.Com or other lead source
    /// </summary>
    [AttributeLogicalName("ctas_created_from_lead")]
    [DataMember]
    public bool? ctas_Created_from_Lead
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_created_from_lead");
        }
        set
        {
            SetAttributeValue("ctas_created_from_lead", value);
        }
    }

    /// <summary>
    /// Household ID for CRM accounts
    /// </summary>
    [AttributeLogicalName("ctas_crm_hhid")]
    [DataMember]
    public string ctas_CRM_HHID
    {
        get
        {
            return GetAttributeValue<string>("ctas_crm_hhid");
        }
        set
        {
            SetAttributeValue("ctas_crm_hhid", value);
        }
    }

    /// <summary>
    /// Specifies if record is a customer. Used with Vertex Services
    /// </summary>
    [AttributeLogicalName("ctas_crmvertexcustomer")]
    [DataMember]
    public bool? ctas_CRMVertexCustomer
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_crmvertexcustomer");
        }
        set
        {
            SetAttributeValue("ctas_crmvertexcustomer", value);
        }
    }

    /// <summary>
    /// Customer Number field. Used with Vertex Services.
    /// </summary>
    [AttributeLogicalName("ctas_crmvertexcustomernumber")]
    [DataMember]
    public string ctas_CRMVertexCustomerNumber
    {
        get
        {
            return GetAttributeValue<string>("ctas_crmvertexcustomernumber");
        }
        set
        {
            SetAttributeValue("ctas_crmvertexcustomernumber", value);
        }
    }

    /// <summary>
    /// Determines if the customer is enabled to use Vertex. This field is used by Vertex Services.
    /// </summary>
    [AttributeLogicalName("ctas_crmvertexenabled")]
    [DataMember]
    public bool? ctas_CRMVertexEnabled
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_crmvertexenabled");
        }
        set
        {
            SetAttributeValue("ctas_crmvertexenabled", value);
        }
    }

    /// <summary>
    /// Customer Budget in dollar amount used for Territory Account Reps.
    /// </summary>
    [AttributeLogicalName("ctas_customer_budget")]
    [DataMember]
    public Money ctas_Customer_Budget
    {
        get
        {
            return GetAttributeValue<Money>("ctas_customer_budget");
        }
        set
        {
            SetAttributeValue("ctas_customer_budget", value);
        }
    }

    /// <summary>
    /// Value of the Customer Budget in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_customer_budget_base")]
    [DataMember]
    public Money ctas_customer_budget_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_customer_budget_base");
        }
    }

    /// <summary>
    /// Do not call for FAS
    /// </summary>
    [AttributeLogicalName("ctas_dnc_fas")]
    [DataMember]
    public bool? ctas_DNC_FAS
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_dnc_fas");
        }
        set
        {
            SetAttributeValue("ctas_dnc_fas", value);
        }
    }

    /// <summary>
    /// Do not call for Fire
    /// </summary>
    [AttributeLogicalName("ctas_dnc_fire")]
    [DataMember]
    public bool? ctas_DNC_Fire
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_dnc_fire");
        }
        set
        {
            SetAttributeValue("ctas_dnc_fire", value);
        }
    }

    /// <summary>
    /// Do not call for FS
    /// </summary>
    [AttributeLogicalName("ctas_dnc_fs")]
    [DataMember]
    public bool? ctas_DNC_FS
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_dnc_fs");
        }
        set
        {
            SetAttributeValue("ctas_dnc_fs", value);
        }
    }

    /// <summary>
    /// The  do not call message.
    /// </summary>
    [AttributeLogicalName("ctas_dnc_message")]
    [DataMember]
    public string ctas_DNC_Message
    {
        get
        {
            return GetAttributeValue<string>("ctas_dnc_message");
        }
        set
        {
            SetAttributeValue("ctas_dnc_message", value);
        }
    }

    /// <summary>
    /// Do not call for UR
    /// </summary>
    [AttributeLogicalName("ctas_dnc_ur")]
    [DataMember]
    public bool? ctas_DNC_UR
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_dnc_ur");
        }
        set
        {
            SetAttributeValue("ctas_dnc_ur", value);
        }
    }

    /// <summary>
    /// Unique Dynamics Number which.
    /// </summary>
    [AttributeLogicalName("ctas_dynamics_num")]
    [DataMember]
    public string ctas_Dynamics_Num
    {
        get
        {
            return GetAttributeValue<string>("ctas_dynamics_num");
        }
        set
        {
            SetAttributeValue("ctas_dynamics_num", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_estimated_close_date_ww")]
    [DataMember]
    public DateTime? ctas_Estimated_Close_Date_WW
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_estimated_close_date_ww");
        }
        set
        {
            SetAttributeValue("ctas_estimated_close_date_ww", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_estimated_volume_ww")]
    [DataMember]
    public Money ctas_Estimated_Volume_WW
    {
        get
        {
            return GetAttributeValue<Money>("ctas_estimated_volume_ww");
        }
        set
        {
            SetAttributeValue("ctas_estimated_volume_ww", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_WW in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_estimated_volume_ww_base")]
    [DataMember]
    public Money ctas_estimated_volume_ww_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_estimated_volume_ww_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_estimatedclosedate_fas")]
    [DataMember]
    public DateTime? ctas_EstimatedCloseDate_FAS
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_estimatedclosedate_fas");
        }
        set
        {
            SetAttributeValue("ctas_estimatedclosedate_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_favorite_ur")]
    [DataMember]
    public bool? ctas_Favorite_UR
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_favorite_ur");
        }
        set
        {
            SetAttributeValue("ctas_favorite_ur", value);
        }
    }

    /// <summary>
    /// FRC Portfolio Flag
    /// </summary>
    [AttributeLogicalName("ctas_frc_portfolio_flag")]
    [DataMember]
    public OptionSetValue ctas_FRC_Portfolio_Flag
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_frc_portfolio_flag");
        }
        set
        {
            SetAttributeValue("ctas_frc_portfolio_flag", value);
        }
    }

    /// <summary>
    /// Flag which identifies records as a government office.
    /// </summary>
    [AttributeLogicalName("ctas_government_flag")]
    [DataMember]
    public bool? ctas_Government_Flag
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_government_flag");
        }
        set
        {
            SetAttributeValue("ctas_government_flag", value);
        }
    }

    /// <summary>
    /// Note: CAUTION: Before contacting or engaging with this prospect, reference policy C65- Exhibit A. This form MUST be submitted and reviewed by legal on ANY Government Affiliated accounts!
    /// </summary>
    [AttributeLogicalName("ctas_government_note")]
    [DataMember]
    public string ctas_Government_Note
    {
        get
        {
            return GetAttributeValue<string>("ctas_government_note");
        }
        set
        {
            SetAttributeValue("ctas_government_note", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_government_notes")]
    [DataMember]
    public string ctas_Government_Notes
    {
        get
        {
            return GetAttributeValue<string>("ctas_government_notes");
        }
        set
        {
            SetAttributeValue("ctas_government_notes", value);
        }
    }

    /// <summary>
    /// Lookup field for GPO Healthcare
    /// </summary>
    [AttributeLogicalName("ctas_gpo1_healthcare")]
    [DataMember]
    public EntityReference ctas_GPO1_Healthcare
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_gpo1_healthcare");
        }
        set
        {
            SetAttributeValue("ctas_gpo1_healthcare", value);
        }
    }

    /// <summary>
    /// GPO 2 for Healthcare
    /// </summary>
    [AttributeLogicalName("ctas_gpo2_healthcare")]
    [DataMember]
    public EntityReference ctas_GPO2_Healthcare
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_gpo2_healthcare");
        }
        set
        {
            SetAttributeValue("ctas_gpo2_healthcare", value);
        }
    }

    /// <summary>
    /// Growth Connection Link for First Aid And Safety.
    /// </summary>
    [AttributeLogicalName("ctas_growth_connection_fas")]
    [DataMember]
    public string ctas_Growth_Connection_FAS
    {
        get
        {
            return GetAttributeValue<string>("ctas_growth_connection_fas");
        }
        set
        {
            SetAttributeValue("ctas_growth_connection_fas", value);
        }
    }

    /// <summary>
    /// Growth Connection for Uniform Rental
    /// </summary>
    [AttributeLogicalName("ctas_growth_connection_ren")]
    [DataMember]
    public string ctas_Growth_Connection_Ren
    {
        get
        {
            return GetAttributeValue<string>("ctas_growth_connection_ren");
        }
        set
        {
            SetAttributeValue("ctas_growth_connection_ren", value);
        }
    }

    /// <summary>
    /// Flag added to Health Care Inside Sales Form to allow reps to flag accounts that they want in their portfolio.
    /// </summary>
    [AttributeLogicalName("ctas_hc_inside_sales_portfolio_flag")]
    [DataMember]
    public OptionSetValue ctas_HC_Inside_Sales_Portfolio_Flag
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_hc_inside_sales_portfolio_flag");
        }
        set
        {
            SetAttributeValue("ctas_hc_inside_sales_portfolio_flag", value);
        }
    }

    /// <summary>
    /// Link to field history update table.
    /// </summary>
    [AttributeLogicalName("ctas_history_field_update")]
    [DataMember]
    public EntityReference ctas_History_Field_Update
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_history_field_update");
        }
        set
        {
            SetAttributeValue("ctas_history_field_update", value);
        }
    }

    /// <summary>
    /// Lookup for IDN Healthcare
    /// </summary>
    [AttributeLogicalName("ctas_idn_healthcare")]
    [DataMember]
    public EntityReference ctas_IDN_Healthcare
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_idn_healthcare");
        }
        set
        {
            SetAttributeValue("ctas_idn_healthcare", value);
        }
    }

    /// <summary>
    /// Living Wage Field
    /// </summary>
    [AttributeLogicalName("ctas_living_wage")]
    [DataMember]
    public string ctas_Living_Wage
    {
        get
        {
            return GetAttributeValue<string>("ctas_living_wage");
        }
        set
        {
            SetAttributeValue("ctas_living_wage", value);
        }
    }

    /// <summary>
    /// Message Displayed when the Living Wage field is populated
    /// </summary>
    [AttributeLogicalName("ctas_living_wage_msg")]
    [DataMember]
    public string ctas_Living_Wage_Msg
    {
        get
        {
            return GetAttributeValue<string>("ctas_living_wage_msg");
        }
        set
        {
            SetAttributeValue("ctas_living_wage_msg", value);
        }
    }

    /// <summary>
    /// Management Company for national accounts
    /// </summary>
    [AttributeLogicalName("ctas_managment_company")]
    [DataMember]
    public EntityReference ctas_Managment_Company
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_managment_company");
        }
        set
        {
            SetAttributeValue("ctas_managment_company", value);
        }
    }

    /// <summary>
    /// Mark As field for Cross Sell Division.
    /// </summary>
    [AttributeLogicalName("ctas_markas_cross_sell")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_Cross_Sell
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_cross_sell");
        }
        set
        {
            SetAttributeValue("ctas_markas_cross_sell", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_deep_clean")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_Deep_Clean
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_deep_clean");
        }
        set
        {
            SetAttributeValue("ctas_markas_deep_clean", value);
        }
    }

    /// <summary>
    /// Mark as field for F1000 - Values are Prospect and Customer
    /// </summary>
    [AttributeLogicalName("ctas_markas_f1000")]
    [DataMember]
    public OptionSetValue ctas_Markas_F1000
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_f1000");
        }
        set
        {
            SetAttributeValue("ctas_markas_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_fae")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_FAE
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_fae");
        }
        set
        {
            SetAttributeValue("ctas_markas_fae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_fas")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_FAS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_fas");
        }
        set
        {
            SetAttributeValue("ctas_markas_fas", value);
        }
    }

    /// <summary>
    /// Mark as field for FAS Recertification Team
    /// </summary>
    [AttributeLogicalName("ctas_markas_fas_rec")]
    [DataMember]
    public OptionSetValue ctas_Markas_FAS_Rec
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_fas_rec");
        }
        set
        {
            SetAttributeValue("ctas_markas_fas_rec", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_fp")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_FP
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_fp");
        }
        set
        {
            SetAttributeValue("ctas_markas_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_fps")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_FPS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_fps");
        }
        set
        {
            SetAttributeValue("ctas_markas_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_fs")]
    [DataMember]
    public OptionSetValue ctas_Markas_FS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_fs");
        }
        set
        {
            SetAttributeValue("ctas_markas_fs", value);
        }
    }

    /// <summary>
    /// Field allows the rep to mark a record as a favorite and other values in order to exclude / include them in views.
    /// </summary>
    [AttributeLogicalName("ctas_markas_hc_is")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_HC_IS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_hc_is");
        }
        set
        {
            SetAttributeValue("ctas_markas_hc_is", value);
        }
    }

    /// <summary>
    /// Field allows the rep to mark a record as a favorite and other values in order to exclude / include them in views.
    /// </summary>
    [AttributeLogicalName("ctas_markas_hs_ic")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_HS_IC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_hs_ic");
        }
        set
        {
            SetAttributeValue("ctas_markas_hs_ic", value);
        }
    }

    /// <summary>
    /// Field allows the rep to mark a record as a favorite and other values in order to exclude / include them in views.
    /// </summary>
    [AttributeLogicalName("ctas_markas_hs_mam")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_HS_MAM
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_hs_mam");
        }
        set
        {
            SetAttributeValue("ctas_markas_hs_mam", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_mdr_fs")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_MDR_FS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_mdr_fs");
        }
        set
        {
            SetAttributeValue("ctas_markas_mdr_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_mdr_ur")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_MDR_UR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_mdr_ur");
        }
        set
        {
            SetAttributeValue("ctas_markas_mdr_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_sse")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_SSE
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_sse");
        }
        set
        {
            SetAttributeValue("ctas_markas_sse", value);
        }
    }

    /// <summary>
    /// This field allows the rep to mark records to be filtered out later.
    /// </summary>
    [AttributeLogicalName("ctas_markas_ur")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_UR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_ur");
        }
        set
        {
            SetAttributeValue("ctas_markas_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_ur_frc")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_UR_FRC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_ur_frc");
        }
        set
        {
            SetAttributeValue("ctas_markas_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_markas_ww")]
    [DataMember]
    public OptionSetValue ctas_MarkAs_WW
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_markas_ww");
        }
        set
        {
            SetAttributeValue("ctas_markas_ww", value);
        }
    }

    /// <summary>
    /// National Account Affiliation
    /// </summary>
    [AttributeLogicalName("ctas_nat_acct_affiliation")]
    [DataMember]
    public string ctas_Nat_Acct_Affiliation
    {
        get
        {
            return GetAttributeValue<string>("ctas_nat_acct_affiliation");
        }
        set
        {
            SetAttributeValue("ctas_nat_acct_affiliation", value);
        }
    }

    /// <summary>
    /// National Account Affiliation FAS
    /// </summary>
    [AttributeLogicalName("ctas_nat_acct_affiliation_fas")]
    [DataMember]
    public string ctas_Nat_Acct_Affiliation_FAS
    {
        get
        {
            return GetAttributeValue<string>("ctas_nat_acct_affiliation_fas");
        }
        set
        {
            SetAttributeValue("ctas_nat_acct_affiliation_fas", value);
        }
    }

    /// <summary>
    /// National Account Affiliation Fire
    /// </summary>
    [AttributeLogicalName("ctas_nat_acct_affiliation_fire")]
    [DataMember]
    public string ctas_Nat_Acct_Affiliation_Fire
    {
        get
        {
            return GetAttributeValue<string>("ctas_nat_acct_affiliation_fire");
        }
        set
        {
            SetAttributeValue("ctas_nat_acct_affiliation_fire", value);
        }
    }

    /// <summary>
    /// National Account Affiliation Rental
    /// </summary>
    [AttributeLogicalName("ctas_nat_acct_affiliation_ren")]
    [DataMember]
    public string ctas_Nat_Acct_Affiliation_Ren
    {
        get
        {
            return GetAttributeValue<string>("ctas_nat_acct_affiliation_ren");
        }
        set
        {
            SetAttributeValue("ctas_nat_acct_affiliation_ren", value);
        }
    }

    /// <summary>
    /// Identifies company as a National Account.
    /// </summary>
    [AttributeLogicalName("ctas_national_account_flag")]
    [DataMember]
    public bool? ctas_National_Account_Flag
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_national_account_flag");
        }
        set
        {
            SetAttributeValue("ctas_national_account_flag", value);
        }
    }

    /// <summary>
    /// Contains a option set of number of employees for a company
    /// </summary>
    [AttributeLogicalName("ctas_no_employees_opt")]
    [DataMember]
    public OptionSetValue ctas_No_Employees_Opt
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_no_employees_opt");
        }
        set
        {
            SetAttributeValue("ctas_no_employees_opt", value);
        }
    }

    /// <summary>
    /// Parent Company and Brand
    /// </summary>
    [AttributeLogicalName("ctas_parent_company_brand")]
    [DataMember]
    public EntityReference ctas_Parent_Company_Brand
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_parent_company_brand");
        }
        set
        {
            SetAttributeValue("ctas_parent_company_brand", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_potentialservices_ww")]
    [DataMember]
    public OptionSetValue ctas_PotentialServices_WW
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_potentialservices_ww");
        }
        set
        {
            SetAttributeValue("ctas_potentialservices_ww", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_ppr_deep_clean")]
    [DataMember]
    public int? ctas_PPR_Deep_Clean
    {
        get
        {
            return GetAttributeValue<int?>("ctas_ppr_deep_clean");
        }
        set
        {
            SetAttributeValue("ctas_ppr_deep_clean", value);
        }
    }

    /// <summary>
    /// PRD link for FAS
    /// </summary>
    [AttributeLogicalName("ctas_prd_fas")]
    [DataMember]
    public string ctas_PRD_FAS
    {
        get
        {
            return GetAttributeValue<string>("ctas_prd_fas");
        }
        set
        {
            SetAttributeValue("ctas_prd_fas", value);
        }
    }

    /// <summary>
    /// PRD Link for Fire
    /// </summary>
    [AttributeLogicalName("ctas_prd_fire")]
    [DataMember]
    public string ctas_PRD_Fire
    {
        get
        {
            return GetAttributeValue<string>("ctas_prd_fire");
        }
        set
        {
            SetAttributeValue("ctas_prd_fire", value);
        }
    }

    /// <summary>
    /// PRD link for Rental
    /// </summary>
    [AttributeLogicalName("ctas_prd_rental")]
    [DataMember]
    public string ctas_PRD_Rental
    {
        get
        {
            return GetAttributeValue<string>("ctas_prd_rental");
        }
        set
        {
            SetAttributeValue("ctas_prd_rental", value);
        }
    }

    /// <summary>
    /// This is a option set with a list of reasons why a new account is being created
    /// </summary>
    [AttributeLogicalName("ctas_reason_for_a_new_account")]
    [DataMember]
    public OptionSetValue ctas_Reason_for_a_New_Account
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_reason_for_a_new_account");
        }
        set
        {
            SetAttributeValue("ctas_reason_for_a_new_account", value);
        }
    }

    /// <summary>
    /// This is a option set used to identify the type of sale the FAE is working
    /// </summary>
    [AttributeLogicalName("ctas_sale_frequency")]
    [DataMember]
    public OptionSetValue ctas_Sale_Frequency
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_sale_frequency");
        }
        set
        {
            SetAttributeValue("ctas_sale_frequency", value);
        }
    }

    /// <summary>
    /// this identifies records which are in Sales Fusion.
    /// </summary>
    [AttributeLogicalName("ctas_sales_fusion_flag")]
    [DataMember]
    public bool? ctas_Sales_Fusion_Flag
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_sales_fusion_flag");
        }
        set
        {
            SetAttributeValue("ctas_sales_fusion_flag", value);
        }
    }

    /// <summary>
    /// Sales Stage - Drop Down.
    /// </summary>
    [AttributeLogicalName("ctas_sales_stage")]
    [DataMember]
    public OptionSetValue ctas_Sales_Stage
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_sales_stage");
        }
        set
        {
            SetAttributeValue("ctas_sales_stage", value);
        }
    }

    /// <summary>
    /// Holds the URL of the Sharepoint site.
    /// </summary>
    [AttributeLogicalName("ctas_share_point_site")]
    [DataMember]
    public string ctas_Share_Point_Site
    {
        get
        {
            return GetAttributeValue<string>("ctas_share_point_site");
        }
        set
        {
            SetAttributeValue("ctas_share_point_site", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_soldaccountstatus_fs_ww")]
    [DataMember]
    public OptionSetValue ctas_SoldAccountStatus_FS_WW
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_soldaccountstatus_fs_ww");
        }
        set
        {
            SetAttributeValue("ctas_soldaccountstatus_fs_ww", value);
        }
    }

    /// <summary>
    /// Option set for square footage.
    /// </summary>
    [AttributeLogicalName("ctas_square_footage_opt")]
    [DataMember]
    public OptionSetValue ctas_Square_Footage_Opt
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_square_footage_opt");
        }
        set
        {
            SetAttributeValue("ctas_square_footage_opt", value);
        }
    }

    /// <summary>
    /// This is a flag for the SSE's to add and remove accounts from their portfolio
    /// </summary>
    [AttributeLogicalName("ctas_sse_portfolio_flag")]
    [DataMember]
    public OptionSetValue ctas_SSE_Portfolio_Flag
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_sse_portfolio_flag");
        }
        set
        {
            SetAttributeValue("ctas_sse_portfolio_flag", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_target_flag_cat_tar")]
    [DataMember]
    public OptionSetValue ctas_Target_Flag_CAT_TAR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_target_flag_cat_tar");
        }
        set
        {
            SetAttributeValue("ctas_target_flag_cat_tar", value);
        }
    }

    /// <summary>
    /// Target Flag for First Aid Account Executive.
    /// </summary>
    [AttributeLogicalName("ctas_target_flag_fae")]
    [DataMember]
    public OptionSetValue ctas_Target_Flag_FAE
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_target_flag_fae");
        }
        set
        {
            SetAttributeValue("ctas_target_flag_fae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_target_flag_fs_deepclean")]
    [DataMember]
    public OptionSetValue ctas_Target_Flag_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_target_flag_fs_deepclean");
        }
        set
        {
            SetAttributeValue("ctas_target_flag_fs_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_target_flag_fs_mdr")]
    [DataMember]
    public OptionSetValue ctas_Target_Flag_FS_MDR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_target_flag_fs_mdr");
        }
        set
        {
            SetAttributeValue("ctas_target_flag_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_target_flag_hs_ic")]
    [DataMember]
    public OptionSetValue ctas_Target_Flag_HS_IC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_target_flag_hs_ic");
        }
        set
        {
            SetAttributeValue("ctas_target_flag_hs_ic", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_target_flag_ur_frc")]
    [DataMember]
    public OptionSetValue ctas_Target_Flag_UR_FRC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_target_flag_ur_frc");
        }
        set
        {
            SetAttributeValue("ctas_target_flag_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_targetflag_hc_has")]
    [DataMember]
    public OptionSetValue ctas_TargetFlag_HC_HAS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_targetflag_hc_has");
        }
        set
        {
            SetAttributeValue("ctas_targetflag_hc_has", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_targetflag_ww")]
    [DataMember]
    public OptionSetValue ctas_TargetFlag_WW
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_targetflag_ww");
        }
        set
        {
            SetAttributeValue("ctas_targetflag_ww", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_test_favorite")]
    [DataMember]
    public OptionSetValue ctas_Test_Favorite
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_test_favorite");
        }
        set
        {
            SetAttributeValue("ctas_test_favorite", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_xdate_fps1")]
    [DataMember]
    public OptionSetValue ctas_XDate_FPS1
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_xdate_fps1");
        }
        set
        {
            SetAttributeValue("ctas_xdate_fps1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_xdate_fps2")]
    [DataMember]
    public OptionSetValue ctas_XDate_FPS2
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_xdate_fps2");
        }
        set
        {
            SetAttributeValue("ctas_xdate_fps2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_xdate_fps3")]
    [DataMember]
    public OptionSetValue ctas_XDate_FPS3
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_xdate_fps3");
        }
        set
        {
            SetAttributeValue("ctas_xdate_fps3", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_xdate_update_date_fps_2")]
    [DataMember]
    public DateTime? ctas_XDate_Update_Date_FPS_2
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_xdate_update_date_fps_2");
        }
        set
        {
            SetAttributeValue("ctas_xdate_update_date_fps_2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_xdate_update_date_fps_3")]
    [DataMember]
    public DateTime? ctas_XDate_Update_Date_FPS_3
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_xdate_update_date_fps_3");
        }
        set
        {
            SetAttributeValue("ctas_xdate_update_date_fps_3", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_xdate_update_date_ur")]
    [DataMember]
    public DateTime? ctas_xdate_update_date_UR
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_xdate_update_date_ur");
        }
        set
        {
            SetAttributeValue("ctas_xdate_update_date_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_xdate_update_date_ww")]
    [DataMember]
    public DateTime? ctas_XDate_Update_Date_WW
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_xdate_update_date_ww");
        }
        set
        {
            SetAttributeValue("ctas_xdate_update_date_ww", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_xdate_ww")]
    [DataMember]
    public DateTime? ctas_XDate_WW
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_xdate_ww");
        }
        set
        {
            SetAttributeValue("ctas_xdate_ww", value);
        }
    }

    /// <summary>
    /// Select the size category or range of the account for segmentation and reporting purposes.
    /// </summary>
    [AttributeLogicalName("customersizecode")]
    [DataMember]
    public OptionSetValue CustomerSizeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("customersizecode");
        }
        set
        {
            SetAttributeValue("customersizecode", value);
        }
    }

    /// <summary>
    /// Select the category that best describes the relationship between the account and your organization.
    /// </summary>
    [AttributeLogicalName("customertypecode")]
    [DataMember]
    public OptionSetValue CustomerTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("customertypecode");
        }
        set
        {
            SetAttributeValue("customertypecode", value);
        }
    }

    /// <summary>
    /// Choose the default price list associated with the account to make sure the correct product prices for this customer are applied in sales opportunities, quotes, and orders.
    /// </summary>
    [AttributeLogicalName("defaultpricelevelid")]
    [DataMember]
    public EntityReference DefaultPriceLevelId
    {
        get
        {
            return GetAttributeValue<EntityReference>("defaultpricelevelid");
        }
        set
        {
            SetAttributeValue("defaultpricelevelid", value);
        }
    }

    /// <summary>
    /// Type additional information to describe the account, such as an excerpt from the company's website.
    /// </summary>
    [AttributeLogicalName("description")]
    [DataMember]
    public string Description
    {
        get
        {
            return GetAttributeValue<string>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /// <summary>
    /// Select whether the account allows bulk email sent through campaigns. If Do Not Allow is selected, the account can be added to marketing lists, but is excluded from email.
    /// </summary>
    [AttributeLogicalName("donotbulkemail")]
    [DataMember]
    public bool? DoNotBulkEMail
    {
        get
        {
            return GetAttributeValue<bool?>("donotbulkemail");
        }
        set
        {
            SetAttributeValue("donotbulkemail", value);
        }
    }

    /// <summary>
    /// Select whether the account allows bulk postal mail sent through marketing campaigns or quick campaigns. If Do Not Allow is selected, the account can be added to marketing lists, but will be excluded from the postal mail.
    /// </summary>
    [AttributeLogicalName("donotbulkpostalmail")]
    [DataMember]
    public bool? DoNotBulkPostalMail
    {
        get
        {
            return GetAttributeValue<bool?>("donotbulkpostalmail");
        }
        set
        {
            SetAttributeValue("donotbulkpostalmail", value);
        }
    }

    /// <summary>
    /// Select whether the account allows direct email sent from Microsoft Dynamics 365.
    /// </summary>
    [AttributeLogicalName("donotemail")]
    [DataMember]
    public bool? DoNotEMail
    {
        get
        {
            return GetAttributeValue<bool?>("donotemail");
        }
        set
        {
            SetAttributeValue("donotemail", value);
        }
    }

    /// <summary>
    /// Select whether the account allows faxes. If Do Not Allow is selected, the account will be excluded from fax activities distributed in marketing campaigns.
    /// </summary>
    [AttributeLogicalName("donotfax")]
    [DataMember]
    public bool? DoNotFax
    {
        get
        {
            return GetAttributeValue<bool?>("donotfax");
        }
        set
        {
            SetAttributeValue("donotfax", value);
        }
    }

    /// <summary>
    /// Select whether the account allows phone calls. If Do Not Allow is selected, the account will be excluded from phone call activities distributed in marketing campaigns.
    /// </summary>
    [AttributeLogicalName("donotphone")]
    [DataMember]
    public bool? DoNotPhone
    {
        get
        {
            return GetAttributeValue<bool?>("donotphone");
        }
        set
        {
            SetAttributeValue("donotphone", value);
        }
    }

    /// <summary>
    /// Select whether the account allows direct mail. If Do Not Allow is selected, the account will be excluded from letter activities distributed in marketing campaigns.
    /// </summary>
    [AttributeLogicalName("donotpostalmail")]
    [DataMember]
    public bool? DoNotPostalMail
    {
        get
        {
            return GetAttributeValue<bool?>("donotpostalmail");
        }
        set
        {
            SetAttributeValue("donotpostalmail", value);
        }
    }

    /// <summary>
    /// Select whether the account accepts marketing materials, such as brochures or catalogs.
    /// </summary>
    [AttributeLogicalName("donotsendmm")]
    [DataMember]
    public bool? DoNotSendMM
    {
        get
        {
            return GetAttributeValue<bool?>("donotsendmm");
        }
        set
        {
            SetAttributeValue("donotsendmm", value);
        }
    }

    /// <summary>
    /// Type the primary email address for the account.
    /// </summary>
    [AttributeLogicalName("emailaddress1")]
    [DataMember]
    public string EMailAddress1
    {
        get
        {
            return GetAttributeValue<string>("emailaddress1");
        }
        set
        {
            SetAttributeValue("emailaddress1", value);
        }
    }

    /// <summary>
    /// Type the secondary email address for the account.
    /// </summary>
    [AttributeLogicalName("emailaddress2")]
    [DataMember]
    public string EMailAddress2
    {
        get
        {
            return GetAttributeValue<string>("emailaddress2");
        }
        set
        {
            SetAttributeValue("emailaddress2", value);
        }
    }

    /// <summary>
    /// Type an alternate email address for the account.
    /// </summary>
    [AttributeLogicalName("emailaddress3")]
    [DataMember]
    public string EMailAddress3
    {
        get
        {
            return GetAttributeValue<string>("emailaddress3");
        }
        set
        {
            SetAttributeValue("emailaddress3", value);
        }
    }

    /// <summary>
    /// Shows the default image for the record.
    /// </summary>
    [AttributeLogicalName("entityimage")]
    [DataMember]
    public byte[] EntityImage
    {
        get
        {
            return GetAttributeValue<byte[]>("entityimage");
        }
        set
        {
            SetAttributeValue("entityimage", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_timestamp")]
    [DataMember]
    public long? EntityImage_Timestamp
    {
        get
        {
            return GetAttributeValue<long?>("entityimage_timestamp");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_url")]
    [DataMember]
    public string EntityImage_URL
    {
        get
        {
            return GetAttributeValue<string>("entityimage_url");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("entityimageid")]
    [DataMember]
    public Guid? EntityImageId
    {
        get
        {
            return GetAttributeValue<Guid?>("entityimageid");
        }
    }

    /// <summary>
    /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Type the fax number for the account.
    /// </summary>
    [AttributeLogicalName("fax")]
    [DataMember]
    public string Fax
    {
        get
        {
            return GetAttributeValue<string>("fax");
        }
        set
        {
            SetAttributeValue("fax", value);
        }
    }

    /// <summary>
    /// Information about whether to allow following email activity like opens, attachment views and link clicks for emails sent to the account.
    /// </summary>
    [AttributeLogicalName("followemail")]
    [DataMember]
    public bool? FollowEmail
    {
        get
        {
            return GetAttributeValue<bool?>("followemail");
        }
        set
        {
            SetAttributeValue("followemail", value);
        }
    }

    /// <summary>
    /// Type the URL for the account's FTP site to enable users to access data and share documents.
    /// </summary>
    [AttributeLogicalName("ftpsiteurl")]
    [DataMember]
    public string FtpSiteURL
    {
        get
        {
            return GetAttributeValue<string>("ftpsiteurl");
        }
        set
        {
            SetAttributeValue("ftpsiteurl", value);
        }
    }

    /// <summary>
    /// Unique identifier of the data import or data migration that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Select the account's primary industry for use in marketing segmentation and demographic analysis.
    /// </summary>
    [AttributeLogicalName("industrycode")]
    [DataMember]
    public OptionSetValue IndustryCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("industrycode");
        }
        set
        {
            SetAttributeValue("industrycode", value);
        }
    }

    /// <summary>
    /// Contains the date and time stamp of the last on hold time.
    /// </summary>
    [AttributeLogicalName("lastonholdtime")]
    [DataMember]
    public DateTime? LastOnHoldTime
    {
        get
        {
            return GetAttributeValue<DateTime?>("lastonholdtime");
        }
        set
        {
            SetAttributeValue("lastonholdtime", value);
        }
    }

    /// <summary>
    /// Shows the date when the account was last included in a marketing campaign or quick campaign.
    /// </summary>
    [AttributeLogicalName("lastusedincampaign")]
    [DataMember]
    public DateTime? LastUsedInCampaign
    {
        get
        {
            return GetAttributeValue<DateTime?>("lastusedincampaign");
        }
        set
        {
            SetAttributeValue("lastusedincampaign", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ma_accountguid")]
    [DataMember]
    public string ma_accountguid
    {
        get
        {
            return GetAttributeValue<string>("ma_accountguid");
        }
        set
        {
            SetAttributeValue("ma_accountguid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ma_savedaccountid")]
    [DataMember]
    public string ma_savedaccountid
    {
        get
        {
            return GetAttributeValue<string>("ma_savedaccountid");
        }
        set
        {
            SetAttributeValue("ma_savedaccountid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ma_score1")]
    [DataMember]
    public int? ma_Score1
    {
        get
        {
            return GetAttributeValue<int?>("ma_score1");
        }
        set
        {
            SetAttributeValue("ma_score1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ma_score2")]
    [DataMember]
    public int? ma_Score2
    {
        get
        {
            return GetAttributeValue<int?>("ma_score2");
        }
        set
        {
            SetAttributeValue("ma_score2", value);
        }
    }

    /// <summary>
    /// Type the market capitalization of the account to identify the company's equity, used as an indicator in financial performance analysis.
    /// </summary>
    [AttributeLogicalName("marketcap")]
    [DataMember]
    public Money MarketCap
    {
        get
        {
            return GetAttributeValue<Money>("marketcap");
        }
        set
        {
            SetAttributeValue("marketcap", value);
        }
    }

    /// <summary>
    /// Shows the market capitalization converted to the system's default base currency.
    /// </summary>
    [AttributeLogicalName("marketcap_base")]
    [DataMember]
    public Money MarketCap_Base
    {
        get
        {
            return GetAttributeValue<Money>("marketcap_base");
        }
    }

    /// <summary>
    /// Whether is only for marketing
    /// </summary>
    [AttributeLogicalName("marketingonly")]
    [DataMember]
    public bool? MarketingOnly
    {
        get
        {
            return GetAttributeValue<bool?>("marketingonly");
        }
        set
        {
            SetAttributeValue("marketingonly", value);
        }
    }

    /// <summary>
    /// Shows the master account that the account was merged with.
    /// </summary>
    [AttributeLogicalName("masterid")]
    [DataMember]
    public EntityReference MasterId
    {
        get
        {
            return GetAttributeValue<EntityReference>("masterid");
        }
    }

    /// <summary>
    /// Shows whether the account has been merged with another account.
    /// </summary>
    [AttributeLogicalName("merged")]
    [DataMember]
    public bool? Merged
    {
        get
        {
            return GetAttributeValue<bool?>("merged");
        }
    }

    /// <summary>
    /// Shows who last updated the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Shows the external party who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedbyexternalparty")]
    [DataMember]
    public EntityReference ModifiedByExternalParty
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedbyexternalparty");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was last updated. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Type the company or business name.
    /// </summary>
    [AttributeLogicalName("name")]
    [DataMember]
    public string Name
    {
        get
        {
            return GetAttributeValue<string>("name");
        }
        set
        {
            SetAttributeValue("name", value);
        }
    }

    /// <summary>
    /// Unique identifier for Contact associated with Account.
    /// </summary>
    [AttributeLogicalName("new_secondarycontact")]
    [DataMember]
    public EntityReference new_SecondaryContact
    {
        get
        {
            return GetAttributeValue<EntityReference>("new_secondarycontact");
        }
        set
        {
            SetAttributeValue("new_secondarycontact", value);
        }
    }

    /// <summary>
    /// Type the number of employees that work at the account for use in marketing segmentation and demographic analysis.
    /// </summary>
    [AttributeLogicalName("numberofemployees")]
    [DataMember]
    public int? NumberOfEmployees
    {
        get
        {
            return GetAttributeValue<int?>("numberofemployees");
        }
        set
        {
            SetAttributeValue("numberofemployees", value);
        }
    }

    /// <summary>
    /// Shows how long, in minutes, that the record was on hold.
    /// </summary>
    [AttributeLogicalName("onholdtime")]
    [DataMember]
    public int? OnHoldTime
    {
        get
        {
            return GetAttributeValue<int?>("onholdtime");
        }
    }

    /// <summary>
    /// Number of open opportunities against an account and its child accounts.
    /// </summary>
    [AttributeLogicalName("opendeals")]
    [DataMember]
    public int? OpenDeals
    {
        get
        {
            return GetAttributeValue<int?>("opendeals");
        }
    }

    /// <summary>
    /// The date time for Open Deals.
    /// </summary>
    [AttributeLogicalName("opendeals_date")]
    [DataMember]
    public DateTime? OpenDeals_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("opendeals_date");
        }
    }

    /// <summary>
    /// State of Open Deals.
    /// </summary>
    [AttributeLogicalName("opendeals_state")]
    [DataMember]
    public int? OpenDeals_State
    {
        get
        {
            return GetAttributeValue<int?>("opendeals_state");
        }
    }

    /// <summary>
    /// Sum of open revenue against an account and its child accounts.
    /// </summary>
    [AttributeLogicalName("openrevenue")]
    [DataMember]
    public Money OpenRevenue
    {
        get
        {
            return GetAttributeValue<Money>("openrevenue");
        }
    }

    /// <summary>
    /// Sum of open revenue against an account and its child accounts.
    /// </summary>
    [AttributeLogicalName("openrevenue_base")]
    [DataMember]
    public Money OpenRevenue_Base
    {
        get
        {
            return GetAttributeValue<Money>("openrevenue_base");
        }
    }

    /// <summary>
    /// The date time for Open Revenue.
    /// </summary>
    [AttributeLogicalName("openrevenue_date")]
    [DataMember]
    public DateTime? OpenRevenue_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("openrevenue_date");
        }
    }

    /// <summary>
    /// State of Open Revenue.
    /// </summary>
    [AttributeLogicalName("openrevenue_state")]
    [DataMember]
    public int? OpenRevenue_State
    {
        get
        {
            return GetAttributeValue<int?>("openrevenue_state");
        }
    }

    /// <summary>
    /// Shows the lead that the account was created from if the account was created by converting a lead in Microsoft Dynamics 365. This is used to relate the account to data on the originating lead for use in reporting and analytics.
    /// </summary>
    [AttributeLogicalName("originatingleadid")]
    [DataMember]
    public EntityReference OriginatingLeadId
    {
        get
        {
            return GetAttributeValue<EntityReference>("originatingleadid");
        }
        set
        {
            SetAttributeValue("originatingleadid", value);
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Enter the user or team who is assigned to manage the record. This field is updated every time the record is assigned to a different user.
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Select the account's ownership structure, such as public or private.
    /// </summary>
    [AttributeLogicalName("ownershipcode")]
    [DataMember]
    public OptionSetValue OwnershipCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ownershipcode");
        }
        set
        {
            SetAttributeValue("ownershipcode", value);
        }
    }

    /// <summary>
    /// Shows the business unit that the record owner belongs to.
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier of the team who owns the account.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    [DataMember]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier of the user who owns the account.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// Choose the parent account associated with this account to show parent and child businesses in reporting and analytics.
    /// </summary>
    [AttributeLogicalName("parentaccountid")]
    [DataMember]
    public EntityReference ParentAccountId
    {
        get
        {
            return GetAttributeValue<EntityReference>("parentaccountid");
        }
        set
        {
            SetAttributeValue("parentaccountid", value);
        }
    }

    /// <summary>
    /// For system use only. Legacy Microsoft Dynamics CRM 3.0 workflow data.
    /// </summary>
    [AttributeLogicalName("participatesinworkflow")]
    [DataMember]
    public bool? ParticipatesInWorkflow
    {
        get
        {
            return GetAttributeValue<bool?>("participatesinworkflow");
        }
        set
        {
            SetAttributeValue("participatesinworkflow", value);
        }
    }

    /// <summary>
    /// Select the payment terms to indicate when the customer needs to pay the total amount.
    /// </summary>
    [AttributeLogicalName("paymenttermscode")]
    [DataMember]
    public OptionSetValue PaymentTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("paymenttermscode");
        }
        set
        {
            SetAttributeValue("paymenttermscode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_abinumber")]
    [DataMember]
    public string po_ABINumber
    {
        get
        {
            return GetAttributeValue<string>("po_abinumber");
        }
        set
        {
            SetAttributeValue("po_abinumber", value);
        }
    }

    /// <summary>
    /// Used for behind the scenes data quality.
    /// </summary>
    [AttributeLogicalName("po_accountname2")]
    [DataMember]
    public string po_AccountName2
    {
        get
        {
            return GetAttributeValue<string>("po_accountname2");
        }
        set
        {
            SetAttributeValue("po_accountname2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_accountnumber")]
    [DataMember]
    public string po_AccountNumber
    {
        get
        {
            return GetAttributeValue<string>("po_accountnumber");
        }
        set
        {
            SetAttributeValue("po_accountnumber", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_accountsoldur")]
    [DataMember]
    public bool? po_AccountSoldUR
    {
        get
        {
            return GetAttributeValue<bool?>("po_accountsoldur");
        }
        set
        {
            SetAttributeValue("po_accountsoldur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_accounttypecode")]
    [DataMember]
    public OptionSetValue po_AccountTypecode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_accounttypecode");
        }
        set
        {
            SetAttributeValue("po_accounttypecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_address1line1_search")]
    [DataMember]
    public string po_address1line1_search
    {
        get
        {
            return GetAttributeValue<string>("po_address1line1_search");
        }
        set
        {
            SetAttributeValue("po_address1line1_search", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_address2_postalcode")]
    [DataMember]
    public EntityReference po_Address2_postalcode
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_address2_postalcode");
        }
        set
        {
            SetAttributeValue("po_address2_postalcode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_addtostatusreport_f1000")]
    [DataMember]
    public bool? po_AddtoStatusReport_F1000
    {
        get
        {
            return GetAttributeValue<bool?>("po_addtostatusreport_f1000");
        }
        set
        {
            SetAttributeValue("po_addtostatusreport_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_affiliationcode_hosp")]
    [DataMember]
    public string po_AffiliationCode_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_affiliationcode_hosp");
        }
        set
        {
            SetAttributeValue("po_affiliationcode_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_affilliationname_hosp")]
    [DataMember]
    public string po_AffilliationName_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_affilliationname_hosp");
        }
        set
        {
            SetAttributeValue("po_affilliationname_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_arbalance3060days")]
    [DataMember]
    public Money po_ARBalance3060Days
    {
        get
        {
            return GetAttributeValue<Money>("po_arbalance3060days");
        }
        set
        {
            SetAttributeValue("po_arbalance3060days", value);
        }
    }

    /// <summary>
    /// Value of the A/R Balance 30-60 Days in base currency.
    /// </summary>
    [AttributeLogicalName("po_arbalance3060days_base")]
    [DataMember]
    public Money po_arbalance3060days_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_arbalance3060days_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_arbalance6090days")]
    [DataMember]
    public Money po_ARBalance6090Days
    {
        get
        {
            return GetAttributeValue<Money>("po_arbalance6090days");
        }
        set
        {
            SetAttributeValue("po_arbalance6090days", value);
        }
    }

    /// <summary>
    /// Value of the A/R Balance 60-90 Days in base currency.
    /// </summary>
    [AttributeLogicalName("po_arbalance6090days_base")]
    [DataMember]
    public Money po_arbalance6090days_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_arbalance6090days_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_arbalance90days")]
    [DataMember]
    public Money po_ARBalance90Days
    {
        get
        {
            return GetAttributeValue<Money>("po_arbalance90days");
        }
        set
        {
            SetAttributeValue("po_arbalance90days", value);
        }
    }

    /// <summary>
    /// Value of the A/R Balance 90+ Days in base currency.
    /// </summary>
    [AttributeLogicalName("po_arbalance90days_base")]
    [DataMember]
    public Money po_arbalance90days_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_arbalance90days_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_bedsstaffed_hc")]
    [DataMember]
    public string po_BedsStaffed_HC
    {
        get
        {
            return GetAttributeValue<string>("po_bedsstaffed_hc");
        }
        set
        {
            SetAttributeValue("po_bedsstaffed_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_billto_hosp")]
    [DataMember]
    public string po_BillTo_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_billto_hosp");
        }
        set
        {
            SetAttributeValue("po_billto_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_brandcode_f1000")]
    [DataMember]
    public string po_BrandCode_F1000
    {
        get
        {
            return GetAttributeValue<string>("po_brandcode_f1000");
        }
        set
        {
            SetAttributeValue("po_brandcode_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_brandcode_hosp")]
    [DataMember]
    public string po_BrandCode_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_brandcode_hosp");
        }
        set
        {
            SetAttributeValue("po_brandcode_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_casino_hosp")]
    [DataMember]
    public bool? po_Casino_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_casino_hosp");
        }
        set
        {
            SetAttributeValue("po_casino_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_chainid_hosp")]
    [DataMember]
    public string po_ChainID_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_chainid_hosp");
        }
        set
        {
            SetAttributeValue("po_chainid_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cintergy_account_id")]
    [DataMember]
    public int? po_Cintergy_Account_ID
    {
        get
        {
            return GetAttributeValue<int?>("po_cintergy_account_id");
        }
        set
        {
            SetAttributeValue("po_cintergy_account_id", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cleanaccountname")]
    [DataMember]
    public string po_CleanAccountName
    {
        get
        {
            return GetAttributeValue<string>("po_cleanaccountname");
        }
        set
        {
            SetAttributeValue("po_cleanaccountname", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cleanaddressline1")]
    [DataMember]
    public string po_CleanAddressLine1
    {
        get
        {
            return GetAttributeValue<string>("po_cleanaddressline1");
        }
        set
        {
            SetAttributeValue("po_cleanaddressline1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cleanaddressline2")]
    [DataMember]
    public string po_CleanAddressLine2
    {
        get
        {
            return GetAttributeValue<string>("po_cleanaddressline2");
        }
        set
        {
            SetAttributeValue("po_cleanaddressline2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cleancityname")]
    [DataMember]
    public string po_CleanCityName
    {
        get
        {
            return GetAttributeValue<string>("po_cleancityname");
        }
        set
        {
            SetAttributeValue("po_cleancityname", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cleanpostalcode")]
    [DataMember]
    public string po_CleanPostalCode
    {
        get
        {
            return GetAttributeValue<string>("po_cleanpostalcode");
        }
        set
        {
            SetAttributeValue("po_cleanpostalcode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cleanstateprovince")]
    [DataMember]
    public string po_CleanStateProvince
    {
        get
        {
            return GetAttributeValue<string>("po_cleanstateprovince");
        }
        set
        {
            SetAttributeValue("po_cleanstateprovince", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cleantelephone1")]
    [DataMember]
    public string po_CleanTelephone1
    {
        get
        {
            return GetAttributeValue<string>("po_cleantelephone1");
        }
        set
        {
            SetAttributeValue("po_cleantelephone1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_cat_tar")]
    [DataMember]
    public EntityReference po_Competitor_Cat_TAR
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_cat_tar");
        }
        set
        {
            SetAttributeValue("po_competitor_cat_tar", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_f1000id")]
    [DataMember]
    public EntityReference po_Competitor_F1000id
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_f1000id");
        }
        set
        {
            SetAttributeValue("po_competitor_f1000id", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fas")]
    [DataMember]
    public EntityReference po_Competitor_FAS
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fas");
        }
        set
        {
            SetAttributeValue("po_competitor_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fas_ae")]
    [DataMember]
    public EntityReference po_Competitor_FAS_AE
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fas_ae");
        }
        set
        {
            SetAttributeValue("po_competitor_fas_ae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fasid")]
    [DataMember]
    public EntityReference po_Competitor_FASid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fasid");
        }
        set
        {
            SetAttributeValue("po_competitor_fasid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fireid")]
    [DataMember]
    public EntityReference po_Competitor_Fireid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fireid");
        }
        set
        {
            SetAttributeValue("po_competitor_fireid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fp")]
    [DataMember]
    public EntityReference po_Competitor_FP
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fp");
        }
        set
        {
            SetAttributeValue("po_competitor_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fps")]
    [DataMember]
    public EntityReference po_Competitor_FPS
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fps");
        }
        set
        {
            SetAttributeValue("po_competitor_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fs")]
    [DataMember]
    public EntityReference po_Competitor_FS
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fs");
        }
        set
        {
            SetAttributeValue("po_competitor_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fs_deepclean")]
    [DataMember]
    public EntityReference po_Competitor_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fs_deepclean");
        }
        set
        {
            SetAttributeValue("po_competitor_fs_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fs_mdr")]
    [DataMember]
    public EntityReference po_Competitor_FS_MDR
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_competitor_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_fsid")]
    [DataMember]
    public EntityReference po_Competitor_FSid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_fsid");
        }
        set
        {
            SetAttributeValue("po_competitor_fsid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_hc_has")]
    [DataMember]
    public EntityReference po_Competitor_HC_HAS
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_hc_has");
        }
        set
        {
            SetAttributeValue("po_competitor_hc_has", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_hc_is")]
    [DataMember]
    public EntityReference po_Competitor_HC_IS
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_hc_is");
        }
        set
        {
            SetAttributeValue("po_competitor_hc_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_hcid")]
    [DataMember]
    public EntityReference po_Competitor_HCid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_hcid");
        }
        set
        {
            SetAttributeValue("po_competitor_hcid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_hs_ic")]
    [DataMember]
    public EntityReference po_Competitor_HS_IC
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_hs_ic");
        }
        set
        {
            SetAttributeValue("po_competitor_hs_ic", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_hs_is")]
    [DataMember]
    public EntityReference po_Competitor_HS_IS
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_hs_is");
        }
        set
        {
            SetAttributeValue("po_competitor_hs_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_shrid")]
    [DataMember]
    public EntityReference po_Competitor_SHRid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_shrid");
        }
        set
        {
            SetAttributeValue("po_competitor_shrid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_cat_tar")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_CAT_TAR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_cat_tar");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_cat_tar", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_f1000")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_F1000
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_f1000");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_fas")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_FAS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_fas");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_fas_ae")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_FAS_AE
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_fas_ae");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_fas_ae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_fp")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_FP
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_fp");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_fps")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_FPS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_fps");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_fs")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_FS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_fs");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_fs_deepclean")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_fs_deepclean");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_fs_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_fs_mdr")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_FS_MDR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_hc")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_HC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_hc");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_hc_has")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_HC_HAS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_hc_has");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_hc_has", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_hc_is")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_HC_IS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_hc_is");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_hc_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_hosp")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_Hosp
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_hosp");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_hs_ic")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_HS_IC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_hs_ic");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_hs_ic", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_hs_is")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_HS_IS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_hs_is");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_hs_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_ur")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_UR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_ur");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_ur_frc")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_UR_FRC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_ur_frc");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_ur_mdr")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_UR_MDR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_update_date_ur_sse")]
    [DataMember]
    public DateTime? po_Competitor_Update_Date_UR_SSE
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_competitor_update_date_ur_sse");
        }
        set
        {
            SetAttributeValue("po_competitor_update_date_ur_sse", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_ur")]
    [DataMember]
    public EntityReference po_Competitor_UR
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_ur");
        }
        set
        {
            SetAttributeValue("po_competitor_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_ur_frc")]
    [DataMember]
    public EntityReference po_Competitor_UR_FRC
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_ur_frc");
        }
        set
        {
            SetAttributeValue("po_competitor_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_ur_mdr")]
    [DataMember]
    public EntityReference po_Competitor_UR_MDR
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_competitor_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_ur_sse")]
    [DataMember]
    public EntityReference po_Competitor_UR_SSE
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_ur_sse");
        }
        set
        {
            SetAttributeValue("po_competitor_ur_sse", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_competitor_urid")]
    [DataMember]
    public EntityReference po_Competitor_URid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_competitor_urid");
        }
        set
        {
            SetAttributeValue("po_competitor_urid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_conference_hosp")]
    [DataMember]
    public bool? po_Conference_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_conference_hosp");
        }
        set
        {
            SetAttributeValue("po_conference_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_convention_hosp")]
    [DataMember]
    public bool? po_Convention_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_convention_hosp");
        }
        set
        {
            SetAttributeValue("po_convention_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_crm_sys_id")]
    [DataMember]
    public string po_CRM_SYS_ID
    {
        get
        {
            return GetAttributeValue<string>("po_crm_sys_id");
        }
        set
        {
            SetAttributeValue("po_crm_sys_id", value);
        }
    }

    /// <summary>
    /// Current Customer of Catalog
    /// </summary>
    [AttributeLogicalName("po_custofcatalog")]
    [DataMember]
    public bool? po_CustofCatalog
    {
        get
        {
            return GetAttributeValue<bool?>("po_custofcatalog");
        }
        set
        {
            SetAttributeValue("po_custofcatalog", value);
        }
    }

    /// <summary>
    /// Current Customer of Chicago Direct Sale
    /// </summary>
    [AttributeLogicalName("po_custofchicagods")]
    [DataMember]
    public bool? po_CustofChicagoDS
    {
        get
        {
            return GetAttributeValue<bool?>("po_custofchicagods");
        }
        set
        {
            SetAttributeValue("po_custofchicagods", value);
        }
    }

    /// <summary>
    /// Current Customer of FAS
    /// </summary>
    [AttributeLogicalName("po_custoffas")]
    [DataMember]
    public bool? po_CustofFAS
    {
        get
        {
            return GetAttributeValue<bool?>("po_custoffas");
        }
        set
        {
            SetAttributeValue("po_custoffas", value);
        }
    }

    /// <summary>
    /// Current Customer of Fire
    /// </summary>
    [AttributeLogicalName("po_custoffire")]
    [DataMember]
    public bool? po_CustofFire
    {
        get
        {
            return GetAttributeValue<bool?>("po_custoffire");
        }
        set
        {
            SetAttributeValue("po_custoffire", value);
        }
    }

    /// <summary>
    /// Current Customer of FS
    /// </summary>
    [AttributeLogicalName("po_custoffs")]
    [DataMember]
    public bool? po_CustofFS
    {
        get
        {
            return GetAttributeValue<bool?>("po_custoffs");
        }
        set
        {
            SetAttributeValue("po_custoffs", value);
        }
    }

    /// <summary>
    /// Current customer of UR
    /// </summary>
    [AttributeLogicalName("po_custofur")]
    [DataMember]
    public bool? po_CustofUR
    {
        get
        {
            return GetAttributeValue<bool?>("po_custofur");
        }
        set
        {
            SetAttributeValue("po_custofur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_dba")]
    [DataMember]
    public string po_DBA
    {
        get
        {
            return GetAttributeValue<string>("po_dba");
        }
        set
        {
            SetAttributeValue("po_dba", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_display6digitnaicscode")]
    [DataMember]
    public EntityReference po_display6digitnaicscode
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_display6digitnaicscode");
        }
        set
        {
            SetAttributeValue("po_display6digitnaicscode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_cat_tar")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_CAT_TAR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_cat_tar");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_cat_tar", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_fas_ae")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_FAS_AE
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_fas_ae");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_fas_ae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_fp")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_FP
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_fp");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_fps")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_FPS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_fps");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_fs")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_FS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_fs");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_fs_deepclean")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_fs_deepclean");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_fs_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_fs_mdr")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_FS_MDR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_hc_has")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_HC_HAS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_hc_has");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_hc_has", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_hc_is")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_HC_IS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_hc_is");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_hc_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_hs_ic")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_HS_IC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_hs_ic");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_hs_ic", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_hs_is")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_HS_IS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_hs_is");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_hs_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_ur")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_UR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_ur");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_ur_frc")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_UR_FRC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_ur_frc");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_ur_mdr")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_UR_MDR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedclosedate_ur_sse")]
    [DataMember]
    public DateTime? po_EstimatedCloseDate_UR_SSE
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_estimatedclosedate_ur_sse");
        }
        set
        {
            SetAttributeValue("po_estimatedclosedate_ur_sse", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume")]
    [DataMember]
    public Money po_EstimatedVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_acct_fas")]
    [DataMember]
    public Money po_EstimatedVolume_Acct_FAS
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_acct_fas");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_acct_fas", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume FAS in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_acct_fas_base")]
    [DataMember]
    public Money po_estimatedvolume_acct_fas_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_acct_fas_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_acct_fire")]
    [DataMember]
    public Money po_EstimatedVolume_Acct_Fire
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_acct_fire");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_acct_fire", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume Fire in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_acct_fire_base")]
    [DataMember]
    public Money po_estimatedvolume_acct_fire_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_acct_fire_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_acct_fs")]
    [DataMember]
    public Money po_EstimatedVolume_Acct_FS
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_acct_fs");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_acct_fs", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume FS in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_acct_fs_base")]
    [DataMember]
    public Money po_estimatedvolume_acct_fs_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_acct_fs_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_acct_ur")]
    [DataMember]
    public Money po_EstimatedVolume_Acct_UR
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_acct_ur");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_acct_ur", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume UR in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_acct_ur_base")]
    [DataMember]
    public Money po_estimatedvolume_acct_ur_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_acct_ur_base");
        }
    }

    /// <summary>
    /// Value of the Estimated Volume in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_base")]
    [DataMember]
    public Money po_estimatedvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_cat_tar")]
    [DataMember]
    public Money po_EstimatedVolume_CAT_TAR
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_cat_tar");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_cat_tar", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_CAT_TAR in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_cat_tar_base")]
    [DataMember]
    public Money po_estimatedvolume_cat_tar_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_cat_tar_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_fas_ae")]
    [DataMember]
    public Money po_EstimatedVolume_FAS_AE
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_fas_ae");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_fas_ae", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_FAS_AE in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_fas_ae_base")]
    [DataMember]
    public Money po_estimatedvolume_fas_ae_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_fas_ae_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_fps")]
    [DataMember]
    public Money po_EstimatedVolume_FPS
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_fps");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_fps", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume FPS in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_fps_base")]
    [DataMember]
    public Money po_estimatedvolume_fps_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_fps_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_fs_deepclean")]
    [DataMember]
    public Money po_EstimatedVolume_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_fs_deepclean");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_fs_deepclean", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_FS_DeepClean in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_fs_deepclean_base")]
    [DataMember]
    public Money po_estimatedvolume_fs_deepclean_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_fs_deepclean_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_fs_mdr")]
    [DataMember]
    public Money po_EstimatedVolume_FS_MDR
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_fs_mdr", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_FS_MDR in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_fs_mdr_base")]
    [DataMember]
    public Money po_estimatedvolume_fs_mdr_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_fs_mdr_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_hc_has")]
    [DataMember]
    public Money po_EstimatedVolume_HC_HAS
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_hc_has");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_hc_has", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_HC_HAS in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_hc_has_base")]
    [DataMember]
    public Money po_estimatedvolume_hc_has_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_hc_has_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_hc_is")]
    [DataMember]
    public Money po_EstimatedVolume_HC_IS
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_hc_is");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_hc_is", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_HC_IS in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_hc_is_base")]
    [DataMember]
    public Money po_estimatedvolume_hc_is_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_hc_is_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_hs_ic")]
    [DataMember]
    public Money po_EstimatedVolume_HS_IC
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_hs_ic");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_hs_ic", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_HS_IC in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_hs_ic_base")]
    [DataMember]
    public Money po_estimatedvolume_hs_ic_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_hs_ic_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_hs_is")]
    [DataMember]
    public Money po_EstimatedVolume_HS_IS
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_hs_is");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_hs_is", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_HS_IS in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_hs_is_base")]
    [DataMember]
    public Money po_estimatedvolume_hs_is_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_hs_is_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_ur_frc")]
    [DataMember]
    public Money po_EstimatedVolume_UR_FRC
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_ur_frc");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_ur_frc", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_UR_FRC in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_ur_frc_base")]
    [DataMember]
    public Money po_estimatedvolume_ur_frc_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_ur_frc_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_ur_mdr")]
    [DataMember]
    public Money po_EstimatedVolume_UR_MDR
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_ur_mdr", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_UR_MDR in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_ur_mdr_base")]
    [DataMember]
    public Money po_estimatedvolume_ur_mdr_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_ur_mdr_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_ur_sse")]
    [DataMember]
    public Money po_EstimatedVolume_UR_SSE
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_ur_sse");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_ur_sse", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume_UR_SSE in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_ur_sse_base")]
    [DataMember]
    public Money po_estimatedvolume_ur_sse_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_ur_sse_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolumefp")]
    [DataMember]
    public Money po_EstimatedVolumeFP
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolumefp");
        }
        set
        {
            SetAttributeValue("po_estimatedvolumefp", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume FP in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolumefp_base")]
    [DataMember]
    public Money po_estimatedvolumefp_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolumefp_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_floors_hosp")]
    [DataMember]
    public int? po_Floors_Hosp
    {
        get
        {
            return GetAttributeValue<int?>("po_floors_hosp");
        }
        set
        {
            SetAttributeValue("po_floors_hosp", value);
        }
    }

    /// <summary>
    /// Sets ID of form. To be used to identify forms/division when data is fed back to the account entity. RR
    /// </summary>
    [AttributeLogicalName("po_formid")]
    [DataMember]
    public string po_FormID
    {
        get
        {
            return GetAttributeValue<string>("po_formid");
        }
        set
        {
            SetAttributeValue("po_formid", value);
        }
    }

    /// <summary>
    /// Sets ID of form. To be used to identify forms/division when data is fed back to the account entity. Field will be hidden RR
    /// </summary>
    [AttributeLogicalName("po_formidur_acct")]
    [DataMember]
    public string po_FormIDUR_Acct
    {
        get
        {
            return GetAttributeValue<string>("po_formidur_acct");
        }
        set
        {
            SetAttributeValue("po_formidur_acct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_fropportunity")]
    [DataMember]
    public OptionSetValue po_FROpportunity
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_fropportunity");
        }
        set
        {
            SetAttributeValue("po_fropportunity", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_futurestrno_hosp")]
    [DataMember]
    public int? po_FutureSTRNo_Hosp
    {
        get
        {
            return GetAttributeValue<int?>("po_futurestrno_hosp");
        }
        set
        {
            SetAttributeValue("po_futurestrno_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_golf_hosp")]
    [DataMember]
    public bool? po_Golf_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_golf_hosp");
        }
        set
        {
            SetAttributeValue("po_golf_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_googlesearch_acct")]
    [DataMember]
    public string po_GoogleSearch_Acct
    {
        get
        {
            return GetAttributeValue<string>("po_googlesearch_acct");
        }
        set
        {
            SetAttributeValue("po_googlesearch_acct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_growthconnection")]
    [DataMember]
    public string po_GrowthConnection
    {
        get
        {
            return GetAttributeValue<string>("po_growthconnection");
        }
        set
        {
            SetAttributeValue("po_growthconnection", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_headquarters")]
    [DataMember]
    public bool? po_Headquarters
    {
        get
        {
            return GetAttributeValue<bool?>("po_headquarters");
        }
        set
        {
            SetAttributeValue("po_headquarters", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_hidehospitality_acct")]
    [DataMember]
    public bool? po_HideHospitality_acct
    {
        get
        {
            return GetAttributeValue<bool?>("po_hidehospitality_acct");
        }
        set
        {
            SetAttributeValue("po_hidehospitality_acct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_hospitalityhide_acct_ur")]
    [DataMember]
    public bool? po_HospitalityHide_acct_UR
    {
        get
        {
            return GetAttributeValue<bool?>("po_hospitalityhide_acct_ur");
        }
        set
        {
            SetAttributeValue("po_hospitalityhide_acct_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_hospitalityshow_acct_ur")]
    [DataMember]
    public bool? po_HospitalityShow_acct_UR
    {
        get
        {
            return GetAttributeValue<bool?>("po_hospitalityshow_acct_ur");
        }
        set
        {
            SetAttributeValue("po_hospitalityshow_acct_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_interiorcorridor_hosp")]
    [DataMember]
    public bool? po_InteriorCorridor_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_interiorcorridor_hosp");
        }
        set
        {
            SetAttributeValue("po_interiorcorridor_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_jeopardy")]
    [DataMember]
    public bool? po_jeopardy
    {
        get
        {
            return GetAttributeValue<bool?>("po_jeopardy");
        }
        set
        {
            SetAttributeValue("po_jeopardy", value);
        }
    }

    /// <summary>
    /// Lost Account
    /// </summary>
    [AttributeLogicalName("po_lostf1000")]
    [DataMember]
    public bool? po_LostF1000
    {
        get
        {
            return GetAttributeValue<bool?>("po_lostf1000");
        }
        set
        {
            SetAttributeValue("po_lostf1000", value);
        }
    }

    /// <summary>
    /// Lost Reason - Why the Account was lost
    /// </summary>
    [AttributeLogicalName("po_lostreasonf1000")]
    [DataMember]
    public string po_LostReasonF1000
    {
        get
        {
            return GetAttributeValue<string>("po_lostreasonf1000");
        }
        set
        {
            SetAttributeValue("po_lostreasonf1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_managementcompany_f1000")]
    [DataMember]
    public string po_ManagementCompany_F1000
    {
        get
        {
            return GetAttributeValue<string>("po_managementcompany_f1000");
        }
        set
        {
            SetAttributeValue("po_managementcompany_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_managementcompany_hc")]
    [DataMember]
    public string po_ManagementCompany_HC
    {
        get
        {
            return GetAttributeValue<string>("po_managementcompany_hc");
        }
        set
        {
            SetAttributeValue("po_managementcompany_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_managementcompany_hosp")]
    [DataMember]
    public string po_ManagementCompany_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_managementcompany_hosp");
        }
        set
        {
            SetAttributeValue("po_managementcompany_hosp", value);
        }
    }

    /// <summary>
    /// Cintergy invalid accounts
    /// </summary>
    [AttributeLogicalName("po_markedfordelete")]
    [DataMember]
    public bool? po_MarkedforDelete
    {
        get
        {
            return GetAttributeValue<bool?>("po_markedfordelete");
        }
        set
        {
            SetAttributeValue("po_markedfordelete", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_mastercurrentcustomer")]
    [DataMember]
    public bool? po_MasterCurrentCustomer
    {
        get
        {
            return GetAttributeValue<bool?>("po_mastercurrentcustomer");
        }
        set
        {
            SetAttributeValue("po_mastercurrentcustomer", value);
        }
    }

    /// <summary>
    /// Unique identifier for MDR Route associated with Account.
    /// </summary>
    [AttributeLogicalName("po_mdr_route")]
    [DataMember]
    public EntityReference po_MDR_Route
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_mdr_route");
        }
        set
        {
            SetAttributeValue("po_mdr_route", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_meetingspace_hosp")]
    [DataMember]
    public bool? po_MeetingSpace_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_meetingspace_hosp");
        }
        set
        {
            SetAttributeValue("po_meetingspace_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_nacnotes")]
    [DataMember]
    public string po_NACNotes
    {
        get
        {
            return GetAttributeValue<string>("po_nacnotes");
        }
        set
        {
            SetAttributeValue("po_nacnotes", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_naicscodeid")]
    [DataMember]
    public EntityReference po_NAICSCodeid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_naicscodeid");
        }
        set
        {
            SetAttributeValue("po_naicscodeid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_naicsdescription")]
    [DataMember]
    public string po_NAICSDescription
    {
        get
        {
            return GetAttributeValue<string>("po_naicsdescription");
        }
        set
        {
            SetAttributeValue("po_naicsdescription", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_nationalaccountcode")]
    [DataMember]
    public string po_NationalAccountCode
    {
        get
        {
            return GetAttributeValue<string>("po_nationalaccountcode");
        }
        set
        {
            SetAttributeValue("po_nationalaccountcode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_noofadmissions_hc")]
    [DataMember]
    public int? po_NoofAdmissions_hc
    {
        get
        {
            return GetAttributeValue<int?>("po_noofadmissions_hc");
        }
        set
        {
            SetAttributeValue("po_noofadmissions_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_numberofemployees")]
    [DataMember]
    public string po_NumberofEmployees
    {
        get
        {
            return GetAttributeValue<string>("po_numberofemployees");
        }
        set
        {
            SetAttributeValue("po_numberofemployees", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_opendate_hosp")]
    [DataMember]
    public DateTime? po_OpenDate_Hosp
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_opendate_hosp");
        }
        set
        {
            SetAttributeValue("po_opendate_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_operationtype_hosp")]
    [DataMember]
    public string po_OperationType_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_operationtype_hosp");
        }
        set
        {
            SetAttributeValue("po_operationtype_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_opvisits_hc")]
    [DataMember]
    public string po_OPVisits_HC
    {
        get
        {
            return GetAttributeValue<string>("po_opvisits_hc");
        }
        set
        {
            SetAttributeValue("po_opvisits_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ownercode_hosp")]
    [DataMember]
    public string po_OwnerCode_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_ownercode_hosp");
        }
        set
        {
            SetAttributeValue("po_ownercode_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_peakspend")]
    [DataMember]
    public OptionSetValue po_PeakSpend
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_peakspend");
        }
        set
        {
            SetAttributeValue("po_peakspend", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_peakspend_promo")]
    [DataMember]
    public OptionSetValue po_PeakSpend_Promo
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_peakspend_promo");
        }
        set
        {
            SetAttributeValue("po_peakspend_promo", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_peakspendcat_tar")]
    [DataMember]
    public OptionSetValue po_PeakSpendCAT_TAR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_peakspendcat_tar");
        }
        set
        {
            SetAttributeValue("po_peakspendcat_tar", value);
        }
    }

    /// <summary>
    /// Place Holder for Testing - Formatted as a Notes field.
    /// </summary>
    [AttributeLogicalName("po_placeholder")]
    [DataMember]
    public string po_PlaceHolder
    {
        get
        {
            return GetAttributeValue<string>("po_placeholder");
        }
        set
        {
            SetAttributeValue("po_placeholder", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_pool_hosp")]
    [DataMember]
    public bool? po_Pool_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_pool_hosp");
        }
        set
        {
            SetAttributeValue("po_pool_hosp", value);
        }
    }

    /// <summary>
    /// Look up to the Postal Code Entity.
    /// </summary>
    [AttributeLogicalName("po_postalcodeid")]
    [DataMember]
    public EntityReference po_PostalCodeid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_postalcodeid");
        }
        set
        {
            SetAttributeValue("po_postalcodeid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_fas")]
    [DataMember]
    public int? po_PPR_FAS
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_fas");
        }
        set
        {
            SetAttributeValue("po_ppr_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_fp")]
    [DataMember]
    public int? po_PPR_FP
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_fp");
        }
        set
        {
            SetAttributeValue("po_ppr_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_fps")]
    [DataMember]
    public int? po_PPR_FPS
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_fps");
        }
        set
        {
            SetAttributeValue("po_ppr_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_fs")]
    [DataMember]
    public int? po_PPR_FS
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_fs");
        }
        set
        {
            SetAttributeValue("po_ppr_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_img")]
    [DataMember]
    public int? po_PPR_IMG
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_img");
        }
        set
        {
            SetAttributeValue("po_ppr_img", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_pa")]
    [DataMember]
    public int? po_PPR_PA
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_pa");
        }
        set
        {
            SetAttributeValue("po_ppr_pa", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_pp")]
    [DataMember]
    public int? po_PPR_PP
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_pp");
        }
        set
        {
            SetAttributeValue("po_ppr_pp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_prg")]
    [DataMember]
    public int? po_PPR_PRG
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_prg");
        }
        set
        {
            SetAttributeValue("po_ppr_prg", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_shr")]
    [DataMember]
    public int? po_PPR_SHR
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_shr");
        }
        set
        {
            SetAttributeValue("po_ppr_shr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_str")]
    [DataMember]
    public int? po_PPR_STR
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_str");
        }
        set
        {
            SetAttributeValue("po_ppr_str", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_tc")]
    [DataMember]
    public int? po_PPR_TC
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_tc");
        }
        set
        {
            SetAttributeValue("po_ppr_tc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_uc")]
    [DataMember]
    public int? po_PPR_UC
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_uc");
        }
        set
        {
            SetAttributeValue("po_ppr_uc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ppr_ur")]
    [DataMember]
    public int? po_PPR_UR
    {
        get
        {
            return GetAttributeValue<int?>("po_ppr_ur");
        }
        set
        {
            SetAttributeValue("po_ppr_ur", value);
        }
    }

    /// <summary>
    /// This field projects the Estimated Close Date based on the value of the Estimated Close Date Field. This field is hidden.
    /// </summary>
    [AttributeLogicalName("po_projectedclosedate_acct_fs")]
    [DataMember]
    public DateTime? po_ProjectedCloseDate_acct_FS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_projectedclosedate_acct_fs");
        }
        set
        {
            SetAttributeValue("po_projectedclosedate_acct_fs", value);
        }
    }

    /// <summary>
    /// This field projects the Estimated Close Date based on the value of the Estimated Close Date Field. This field is hidden.
    /// </summary>
    [AttributeLogicalName("po_projectedclosedate_acct_ur")]
    [DataMember]
    public DateTime? po_ProjectedCloseDate_acct_UR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_projectedclosedate_acct_ur");
        }
        set
        {
            SetAttributeValue("po_projectedclosedate_acct_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_projectedfinishdate_hosp")]
    [DataMember]
    public DateTime? po_ProjectedFinishDate_Hosp
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_projectedfinishdate_hosp");
        }
        set
        {
            SetAttributeValue("po_projectedfinishdate_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_projectedopendate_hosp")]
    [DataMember]
    public DateTime? po_ProjectedOpenDate_Hosp
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_projectedopendate_hosp");
        }
        set
        {
            SetAttributeValue("po_projectedopendate_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_projectid_hc")]
    [DataMember]
    public int? po_ProjectID_HC
    {
        get
        {
            return GetAttributeValue<int?>("po_projectid_hc");
        }
        set
        {
            SetAttributeValue("po_projectid_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_projectid_hosp")]
    [DataMember]
    public int? po_Projectid_hosp
    {
        get
        {
            return GetAttributeValue<int?>("po_projectid_hosp");
        }
        set
        {
            SetAttributeValue("po_projectid_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_projectname_hosp")]
    [DataMember]
    public string po_ProjectName_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_projectname_hosp");
        }
        set
        {
            SetAttributeValue("po_projectname_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_projectoverview_hosp")]
    [DataMember]
    public string po_ProjectOverview_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_projectoverview_hosp");
        }
        set
        {
            SetAttributeValue("po_projectoverview_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_projectphase_hosp")]
    [DataMember]
    public string po_ProjectPhase_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_projectphase_hosp");
        }
        set
        {
            SetAttributeValue("po_projectphase_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_cat_tar")]
    [DataMember]
    public OptionSetValue po_Prospectof_CAT_TAR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_cat_tar");
        }
        set
        {
            SetAttributeValue("po_prospectof_cat_tar", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_deepclean")]
    [DataMember]
    public OptionSetValue po_Prospectof_Deepclean
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_deepclean");
        }
        set
        {
            SetAttributeValue("po_prospectof_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_fas")]
    [DataMember]
    public OptionSetValue po_Prospectof_FAS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_fas");
        }
        set
        {
            SetAttributeValue("po_prospectof_fas", value);
        }
    }

    /// <summary>
    /// Potential services for prospects of First Aid Account Executives.
    /// </summary>
    [AttributeLogicalName("po_prospectof_fas_ae")]
    [DataMember]
    public OptionSetValue po_Prospectof_FAS_AE
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_fas_ae");
        }
        set
        {
            SetAttributeValue("po_prospectof_fas_ae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_fp")]
    [DataMember]
    public OptionSetValue po_Prospectof_FP
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_fp");
        }
        set
        {
            SetAttributeValue("po_prospectof_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_fps")]
    [DataMember]
    public OptionSetValue po_Prospectof_FPS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_fps");
        }
        set
        {
            SetAttributeValue("po_prospectof_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_fs")]
    [DataMember]
    public OptionSetValue po_Prospectof_FS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_fs");
        }
        set
        {
            SetAttributeValue("po_prospectof_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_fs_deepclean")]
    [DataMember]
    public OptionSetValue po_Prospectof_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_fs_deepclean");
        }
        set
        {
            SetAttributeValue("po_prospectof_fs_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_fs_mdr")]
    [DataMember]
    public OptionSetValue po_Prospectof_FS_MDR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_prospectof_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_fsmdr")]
    [DataMember]
    public OptionSetValue po_Prospectof_FSMDR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_fsmdr");
        }
        set
        {
            SetAttributeValue("po_prospectof_fsmdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_hc_has")]
    [DataMember]
    public OptionSetValue po_Prospectof_HC_HAS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_hc_has");
        }
        set
        {
            SetAttributeValue("po_prospectof_hc_has", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_hc_is")]
    [DataMember]
    public OptionSetValue po_Prospectof_HC_IS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_hc_is");
        }
        set
        {
            SetAttributeValue("po_prospectof_hc_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_hs_ic")]
    [DataMember]
    public OptionSetValue po_Prospectof_HS_IC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_hs_ic");
        }
        set
        {
            SetAttributeValue("po_prospectof_hs_ic", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_hs_is")]
    [DataMember]
    public OptionSetValue po_Prospectof_HS_IS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_hs_is");
        }
        set
        {
            SetAttributeValue("po_prospectof_hs_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_promo")]
    [DataMember]
    public OptionSetValue po_Prospectof_Promo
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_promo");
        }
        set
        {
            SetAttributeValue("po_prospectof_promo", value);
        }
    }

    /// <summary>
    /// Prospect of
    /// </summary>
    [AttributeLogicalName("po_prospectof_ur_frc")]
    [DataMember]
    public OptionSetValue po_Prospectof_UR_FRC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_ur_frc");
        }
        set
        {
            SetAttributeValue("po_prospectof_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_ur_mdr")]
    [DataMember]
    public OptionSetValue po_Prospectof_UR_MDR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_prospectof_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_prospectof_ur_sse")]
    [DataMember]
    public OptionSetValue po_Prospectof_UR_SSE
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectof_ur_sse");
        }
        set
        {
            SetAttributeValue("po_prospectof_ur_sse", value);
        }
    }

    /// <summary>
    /// This a option set which shows potenital services for Uniform Rental.
    /// </summary>
    [AttributeLogicalName("po_prospectofur")]
    [DataMember]
    public OptionSetValue po_ProspectofUR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_prospectofur");
        }
        set
        {
            SetAttributeValue("po_prospectofur", value);
        }
    }

    /// <summary>
    /// RECENT ACCOUNTS RENEWED
    /// </summary>
    [AttributeLogicalName("po_recentlyrenewed_f1000")]
    [DataMember]
    public bool? po_RecentlyRenewed_F1000
    {
        get
        {
            return GetAttributeValue<bool?>("po_recentlyrenewed_f1000");
        }
        set
        {
            SetAttributeValue("po_recentlyrenewed_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_restaurant_hosp")]
    [DataMember]
    public bool? po_Restaurant_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_restaurant_hosp");
        }
        set
        {
            SetAttributeValue("po_restaurant_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_rooms_hc")]
    [DataMember]
    public int? po_Rooms_HC
    {
        get
        {
            return GetAttributeValue<int?>("po_rooms_hc");
        }
        set
        {
            SetAttributeValue("po_rooms_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_rooms_hosp")]
    [DataMember]
    public int? po_Rooms_Hosp
    {
        get
        {
            return GetAttributeValue<int?>("po_rooms_hosp");
        }
        set
        {
            SetAttributeValue("po_rooms_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_route_uat")]
    [DataMember]
    public int? po_route_uat
    {
        get
        {
            return GetAttributeValue<int?>("po_route_uat");
        }
        set
        {
            SetAttributeValue("po_route_uat", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_saleswindow_acct_f1000")]
    [DataMember]
    public bool? po_SalesWindow_acct_F1000
    {
        get
        {
            return GetAttributeValue<bool?>("po_saleswindow_acct_f1000");
        }
        set
        {
            SetAttributeValue("po_saleswindow_acct_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_saleswindow_acct_fs")]
    [DataMember]
    public bool? po_SalesWindow_acct_FS
    {
        get
        {
            return GetAttributeValue<bool?>("po_saleswindow_acct_fs");
        }
        set
        {
            SetAttributeValue("po_saleswindow_acct_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_saleswindow_acct_ur")]
    [DataMember]
    public bool? po_SalesWindow_acct_UR
    {
        get
        {
            return GetAttributeValue<bool?>("po_saleswindow_acct_ur");
        }
        set
        {
            SetAttributeValue("po_saleswindow_acct_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_saleswindow_fs_mdr")]
    [DataMember]
    public bool? po_SalesWindow_FS_MDR
    {
        get
        {
            return GetAttributeValue<bool?>("po_saleswindow_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_saleswindow_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_saleswindow_ur_mdr")]
    [DataMember]
    public bool? po_SalesWindow_UR_MDR
    {
        get
        {
            return GetAttributeValue<bool?>("po_saleswindow_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_saleswindow_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_searchstring1_acct")]
    [DataMember]
    public string po_SearchString1_Acct
    {
        get
        {
            return GetAttributeValue<string>("po_searchstring1_acct");
        }
        set
        {
            SetAttributeValue("po_searchstring1_acct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_servicelocation_hospid")]
    [DataMember]
    public EntityReference po_ServiceLocation_Hospid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_servicelocation_hospid");
        }
        set
        {
            SetAttributeValue("po_servicelocation_hospid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_shipto_hosp")]
    [DataMember]
    public string po_ShipTo_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_shipto_hosp");
        }
        set
        {
            SetAttributeValue("po_shipto_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_showcontact")]
    [DataMember]
    public bool? po_ShowContact
    {
        get
        {
            return GetAttributeValue<bool?>("po_showcontact");
        }
        set
        {
            SetAttributeValue("po_showcontact", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_showhealthcare_acct")]
    [DataMember]
    public bool? po_ShowHealthCare_acct
    {
        get
        {
            return GetAttributeValue<bool?>("po_showhealthcare_acct");
        }
        set
        {
            SetAttributeValue("po_showhealthcare_acct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_showhospitality_acct")]
    [DataMember]
    public bool? po_ShowHospitality_acct
    {
        get
        {
            return GetAttributeValue<bool?>("po_showhospitality_acct");
        }
        set
        {
            SetAttributeValue("po_showhospitality_acct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_sold_account_status_hc_is")]
    [DataMember]
    public OptionSetValue po_Sold_Account_Status_HC_IS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_sold_account_status_hc_is");
        }
        set
        {
            SetAttributeValue("po_sold_account_status_hc_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_acct")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_Acct
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_acct");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_acct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_cat_tar")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_CAT_TAR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_cat_tar");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_cat_tar", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_f1000")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_F1000
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_f1000");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_fas")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_FAS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_fas");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_fas_ae")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_FAS_AE
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_fas_ae");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_fas_ae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_fp")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_FP
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_fp");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_fps")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_FPS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_fps");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_fs")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_FS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_fs");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_fs_deepclean")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_fs_deepclean");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_fs_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_fs_mdr")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_FS_MDR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_hc_has")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_HC_HAS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_hc_has");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_hc_has", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_hc_is")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_HC_IS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_hc_is");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_hc_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_hc_mam")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_HC_MAM
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_hc_mam");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_hc_mam", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_hs_ic")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_HS_IC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_hs_ic");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_hs_ic", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_hs_is")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_HS_IS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_hs_is");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_hs_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_promo")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_Promo
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_promo");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_promo", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_ur")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_UR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_ur");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_ur_frc")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_UR_FRC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_ur_frc");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_ur_mdr")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_UR_MDR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldaccountstatus_ur_sse")]
    [DataMember]
    public OptionSetValue po_SoldAccountStatus_UR_SSE
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_soldaccountstatus_ur_sse");
        }
        set
        {
            SetAttributeValue("po_soldaccountstatus_ur_sse", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_solddate_ur")]
    [DataMember]
    public DateTime? po_SoldDate_UR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_solddate_ur");
        }
        set
        {
            SetAttributeValue("po_solddate_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldvolume_fas")]
    [DataMember]
    public string po_SoldVolume_FAS
    {
        get
        {
            return GetAttributeValue<string>("po_soldvolume_fas");
        }
        set
        {
            SetAttributeValue("po_soldvolume_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_sourcesystem")]
    [DataMember]
    public string po_SourceSystem
    {
        get
        {
            return GetAttributeValue<string>("po_sourcesystem");
        }
        set
        {
            SetAttributeValue("po_sourcesystem", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_spa_hosp")]
    [DataMember]
    public bool? po_Spa_Hosp
    {
        get
        {
            return GetAttributeValue<bool?>("po_spa_hosp");
        }
        set
        {
            SetAttributeValue("po_spa_hosp", value);
        }
    }

    /// <summary>
    /// Denotes if the record is being worked by National Accounts.
    /// </summary>
    [AttributeLogicalName("po_specialtyflag_acct")]
    [DataMember]
    public string po_SpecialtyFlag_acct
    {
        get
        {
            return GetAttributeValue<string>("po_specialtyflag_acct");
        }
        set
        {
            SetAttributeValue("po_specialtyflag_acct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_sqfootage")]
    [DataMember]
    public string po_SqFootage
    {
        get
        {
            return GetAttributeValue<string>("po_sqfootage");
        }
        set
        {
            SetAttributeValue("po_sqfootage", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_strnumber_hc")]
    [DataMember]
    public int? po_STRNumber_HC
    {
        get
        {
            return GetAttributeValue<int?>("po_strnumber_hc");
        }
        set
        {
            SetAttributeValue("po_strnumber_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_strnumber_hosp")]
    [DataMember]
    public int? po_STRNumber_Hosp
    {
        get
        {
            return GetAttributeValue<int?>("po_strnumber_hosp");
        }
        set
        {
            SetAttributeValue("po_strnumber_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_acct_fas")]
    [DataMember]
    public OptionSetValue po_TargetFlag_acct_FAS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_acct_fas");
        }
        set
        {
            SetAttributeValue("po_targetflag_acct_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_acct_fp")]
    [DataMember]
    public OptionSetValue po_TargetFlag_acct_FP
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_acct_fp");
        }
        set
        {
            SetAttributeValue("po_targetflag_acct_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_acct_fps")]
    [DataMember]
    public OptionSetValue po_TargetFlag_acct_FPS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_acct_fps");
        }
        set
        {
            SetAttributeValue("po_targetflag_acct_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_acct_frc")]
    [DataMember]
    public OptionSetValue po_TargetFlag_acct_FRC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_acct_frc");
        }
        set
        {
            SetAttributeValue("po_targetflag_acct_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_acct_fs")]
    [DataMember]
    public OptionSetValue po_TargetFlag_acct_FS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_acct_fs");
        }
        set
        {
            SetAttributeValue("po_targetflag_acct_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_acct_hc")]
    [DataMember]
    public OptionSetValue po_TargetFlag_acct_HC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_acct_hc");
        }
        set
        {
            SetAttributeValue("po_targetflag_acct_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_acct_hs")]
    [DataMember]
    public OptionSetValue po_TargetFlag_acct_HS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_acct_hs");
        }
        set
        {
            SetAttributeValue("po_targetflag_acct_hs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_acct_ur")]
    [DataMember]
    public OptionSetValue po_TargetFlag_acct_UR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_acct_ur");
        }
        set
        {
            SetAttributeValue("po_targetflag_acct_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_targetflag_ur_mdr")]
    [DataMember]
    public OptionSetValue po_TargetFlag_UR_MDR
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_targetflag_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_targetflag_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_taxexempt_f1000")]
    [DataMember]
    public bool? po_TaxExempt_F1000
    {
        get
        {
            return GetAttributeValue<bool?>("po_taxexempt_f1000");
        }
        set
        {
            SetAttributeValue("po_taxexempt_f1000", value);
        }
    }

    /// <summary>
    /// TOP RENEWALS ACTIVELY WORKING
    /// </summary>
    [AttributeLogicalName("po_toprenewal_f1000")]
    [DataMember]
    public bool? po_TopRenewal_F1000
    {
        get
        {
            return GetAttributeValue<bool?>("po_toprenewal_f1000");
        }
        set
        {
            SetAttributeValue("po_toprenewal_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_totalmtgspace_hosp")]
    [DataMember]
    public string po_TotalMtgSpace_Hosp
    {
        get
        {
            return GetAttributeValue<string>("po_totalmtgspace_hosp");
        }
        set
        {
            SetAttributeValue("po_totalmtgspace_hosp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ultimate_parent_num")]
    [DataMember]
    public string po_Ultimate_Parent_Num
    {
        get
        {
            return GetAttributeValue<string>("po_ultimate_parent_num");
        }
        set
        {
            SetAttributeValue("po_ultimate_parent_num", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_verified")]
    [DataMember]
    public bool? po_Verified
    {
        get
        {
            return GetAttributeValue<bool?>("po_verified");
        }
        set
        {
            SetAttributeValue("po_verified", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_webaddress")]
    [DataMember]
    public string po_WebAddress
    {
        get
        {
            return GetAttributeValue<string>("po_webaddress");
        }
        set
        {
            SetAttributeValue("po_webaddress", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_fas")]
    [DataMember]
    public DateTime? po_XDate_FAS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_fas");
        }
        set
        {
            SetAttributeValue("po_xdate_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_fas_ae")]
    [DataMember]
    public DateTime? po_XDate_FAS_AE
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_fas_ae");
        }
        set
        {
            SetAttributeValue("po_xdate_fas_ae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_fire")]
    [DataMember]
    public DateTime? po_XDate_Fire
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_fire");
        }
        set
        {
            SetAttributeValue("po_xdate_fire", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_fs")]
    [DataMember]
    public DateTime? po_XDate_FS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_fs");
        }
        set
        {
            SetAttributeValue("po_xdate_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_fs_deepclean")]
    [DataMember]
    public DateTime? po_XDate_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_fs_deepclean");
        }
        set
        {
            SetAttributeValue("po_xdate_fs_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_fs_mdr")]
    [DataMember]
    public DateTime? po_XDate_FS_MDR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_xdate_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_hc_has")]
    [DataMember]
    public DateTime? po_XDate_HC_HAS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_hc_has");
        }
        set
        {
            SetAttributeValue("po_xdate_hc_has", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_cat_ur")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_CAT_UR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_cat_ur");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_cat_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_f1000")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_F1000
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_f1000");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_f1000", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_fas")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_FAS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_fas");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_fas", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_fas_ae")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_FAS_AE
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_fas_ae");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_fas_ae", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_fp")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_FP
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_fp");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_fp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_fps")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_FPS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_fps");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_fps", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_fs")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_FS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_fs");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_fs", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_fs_deepclean")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_FS_DeepClean
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_fs_deepclean");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_fs_deepclean", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_fs_mdr")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_FS_MDR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_fs_mdr");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_fs_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_hc")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_HC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_hc");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_hc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_hc_has")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_HC_HAS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_hc_has");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_hc_has", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_hc_is")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_HC_IS
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_hc_is");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_hc_is", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_ur_frc")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_UR_FRC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_ur_frc");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_ur_mdr")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_UR_MDR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_update_date_ur_sse")]
    [DataMember]
    public DateTime? po_XDate_Update_Date_UR_SSE
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_update_date_ur_sse");
        }
        set
        {
            SetAttributeValue("po_xdate_update_date_ur_sse", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_ur")]
    [DataMember]
    public DateTime? po_XDate_UR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_ur");
        }
        set
        {
            SetAttributeValue("po_xdate_ur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_ur_frc")]
    [DataMember]
    public DateTime? po_XDate_UR_FRC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_ur_frc");
        }
        set
        {
            SetAttributeValue("po_xdate_ur_frc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_ur_mdr")]
    [DataMember]
    public DateTime? po_XDate_UR_MDR
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_ur_mdr");
        }
        set
        {
            SetAttributeValue("po_xdate_ur_mdr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdate_ur_sse")]
    [DataMember]
    public DateTime? po_XDate_UR_SSE
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdate_ur_sse");
        }
        set
        {
            SetAttributeValue("po_xdate_ur_sse", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_xdatefp")]
    [DataMember]
    public DateTime? po_XDateFP
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_xdatefp");
        }
        set
        {
            SetAttributeValue("po_xdatefp", value);
        }
    }

    /// <summary>
    /// Select the preferred day of the week for service appointments.
    /// </summary>
    [AttributeLogicalName("preferredappointmentdaycode")]
    [DataMember]
    public OptionSetValue PreferredAppointmentDayCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredappointmentdaycode");
        }
        set
        {
            SetAttributeValue("preferredappointmentdaycode", value);
        }
    }

    /// <summary>
    /// Select the preferred time of day for service appointments.
    /// </summary>
    [AttributeLogicalName("preferredappointmenttimecode")]
    [DataMember]
    public OptionSetValue PreferredAppointmentTimeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredappointmenttimecode");
        }
        set
        {
            SetAttributeValue("preferredappointmenttimecode", value);
        }
    }

    /// <summary>
    /// Select the preferred method of contact.
    /// </summary>
    [AttributeLogicalName("preferredcontactmethodcode")]
    [DataMember]
    public OptionSetValue PreferredContactMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredcontactmethodcode");
        }
        set
        {
            SetAttributeValue("preferredcontactmethodcode", value);
        }
    }

    /// <summary>
    /// Choose the account's preferred service facility or equipment to make sure services are scheduled correctly for the customer.
    /// </summary>
    [AttributeLogicalName("preferredequipmentid")]
    [DataMember]
    public EntityReference PreferredEquipmentId
    {
        get
        {
            return GetAttributeValue<EntityReference>("preferredequipmentid");
        }
        set
        {
            SetAttributeValue("preferredequipmentid", value);
        }
    }

    /// <summary>
    /// Choose the account's preferred service for reference when you schedule service activities.
    /// </summary>
    [AttributeLogicalName("preferredserviceid")]
    [DataMember]
    public EntityReference PreferredServiceId
    {
        get
        {
            return GetAttributeValue<EntityReference>("preferredserviceid");
        }
        set
        {
            SetAttributeValue("preferredserviceid", value);
        }
    }

    /// <summary>
    /// Choose the preferred service representative for reference when you schedule service activities for the account.
    /// </summary>
    [AttributeLogicalName("preferredsystemuserid")]
    [DataMember]
    public EntityReference PreferredSystemUserId
    {
        get
        {
            return GetAttributeValue<EntityReference>("preferredsystemuserid");
        }
        set
        {
            SetAttributeValue("preferredsystemuserid", value);
        }
    }

    /// <summary>
    /// Choose the primary contact for the account to provide quick access to contact details.
    /// </summary>
    [AttributeLogicalName("primarycontactid")]
    [DataMember]
    public EntityReference PrimaryContactId
    {
        get
        {
            return GetAttributeValue<EntityReference>("primarycontactid");
        }
        set
        {
            SetAttributeValue("primarycontactid", value);
        }
    }

    /// <summary>
    /// Primary Satori ID for Account
    /// </summary>
    [AttributeLogicalName("primarysatoriid")]
    [DataMember]
    public string PrimarySatoriId
    {
        get
        {
            return GetAttributeValue<string>("primarysatoriid");
        }
        set
        {
            SetAttributeValue("primarysatoriid", value);
        }
    }

    /// <summary>
    /// Primary Twitter ID for Account
    /// </summary>
    [AttributeLogicalName("primarytwitterid")]
    [DataMember]
    public string PrimaryTwitterId
    {
        get
        {
            return GetAttributeValue<string>("primarytwitterid");
        }
        set
        {
            SetAttributeValue("primarytwitterid", value);
        }
    }

    /// <summary>
    /// Shows the ID of the process.
    /// </summary>
    [AttributeLogicalName("processid")]
    [DataMember]
    public Guid? ProcessId
    {
        get
        {
            return GetAttributeValue<Guid?>("processid");
        }
        set
        {
            SetAttributeValue("processid", value);
        }
    }

    /// <summary>
    /// Type the annual revenue for the account, used as an indicator in financial performance analysis.
    /// </summary>
    [AttributeLogicalName("revenue")]
    [DataMember]
    public Money Revenue
    {
        get
        {
            return GetAttributeValue<Money>("revenue");
        }
        set
        {
            SetAttributeValue("revenue", value);
        }
    }

    /// <summary>
    /// Shows the annual revenue converted to the system's default base currency. The calculations use the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("revenue_base")]
    [DataMember]
    public Money Revenue_Base
    {
        get
        {
            return GetAttributeValue<Money>("revenue_base");
        }
    }

    /// <summary>
    /// Type the number of shares available to the public for the account. This number is used as an indicator in financial performance analysis.
    /// </summary>
    [AttributeLogicalName("sharesoutstanding")]
    [DataMember]
    public int? SharesOutstanding
    {
        get
        {
            return GetAttributeValue<int?>("sharesoutstanding");
        }
        set
        {
            SetAttributeValue("sharesoutstanding", value);
        }
    }

    /// <summary>
    /// Select a shipping method for deliveries sent to the account's address to designate the preferred carrier or other delivery option.
    /// </summary>
    [AttributeLogicalName("shippingmethodcode")]
    [DataMember]
    public OptionSetValue ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("shippingmethodcode");
        }
        set
        {
            SetAttributeValue("shippingmethodcode", value);
        }
    }

    /// <summary>
    /// Type the Standard Industrial Classification (SIC) code that indicates the account's primary industry of business, for use in marketing segmentation and demographic analysis.
    /// </summary>
    [AttributeLogicalName("sic")]
    [DataMember]
    public string SIC
    {
        get
        {
            return GetAttributeValue<string>("sic");
        }
        set
        {
            SetAttributeValue("sic", value);
        }
    }

    /// <summary>
    /// Choose the service level agreement (SLA) that you want to apply to the Account record.
    /// </summary>
    [AttributeLogicalName("slaid")]
    [DataMember]
    public EntityReference SLAId
    {
        get
        {
            return GetAttributeValue<EntityReference>("slaid");
        }
        set
        {
            SetAttributeValue("slaid", value);
        }
    }

    /// <summary>
    /// Last SLA that was applied to this case. This field is for internal use only.
    /// </summary>
    [AttributeLogicalName("slainvokedid")]
    [DataMember]
    public EntityReference SLAInvokedId
    {
        get
        {
            return GetAttributeValue<EntityReference>("slainvokedid");
        }
    }

    /// <summary>
    /// Shows the ID of the stage.
    /// </summary>
    [AttributeLogicalName("stageid")]
    [DataMember]
    public Guid? StageId
    {
        get
        {
            return GetAttributeValue<Guid?>("stageid");
        }
        set
        {
            SetAttributeValue("stageid", value);
        }
    }

    /// <summary>
    /// Shows whether the account is active or inactive. Inactive accounts are read-only and can't be edited unless they are reactivated.
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public AccountState? StateCode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (AccountState)Enum.ToObject(typeof(AccountState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Select the account's status.
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// Type the stock exchange at which the account is listed to track their stock and financial performance of the company.
    /// </summary>
    [AttributeLogicalName("stockexchange")]
    [DataMember]
    public string StockExchange
    {
        get
        {
            return GetAttributeValue<string>("stockexchange");
        }
        set
        {
            SetAttributeValue("stockexchange", value);
        }
    }

    /// <summary>
    /// Type the main phone number for this account.
    /// </summary>
    [AttributeLogicalName("telephone1")]
    [DataMember]
    public string Telephone1
    {
        get
        {
            return GetAttributeValue<string>("telephone1");
        }
        set
        {
            SetAttributeValue("telephone1", value);
        }
    }

    /// <summary>
    /// Type a second phone number for this account.
    /// </summary>
    [AttributeLogicalName("telephone2")]
    [DataMember]
    public string Telephone2
    {
        get
        {
            return GetAttributeValue<string>("telephone2");
        }
        set
        {
            SetAttributeValue("telephone2", value);
        }
    }

    /// <summary>
    /// Type a third phone number for this account.
    /// </summary>
    [AttributeLogicalName("telephone3")]
    [DataMember]
    public string Telephone3
    {
        get
        {
            return GetAttributeValue<string>("telephone3");
        }
        set
        {
            SetAttributeValue("telephone3", value);
        }
    }

    /// <summary>
    /// Select a region or territory for the account for use in segmentation and analysis.
    /// </summary>
    [AttributeLogicalName("territorycode")]
    [DataMember]
    public OptionSetValue TerritoryCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("territorycode");
        }
        set
        {
            SetAttributeValue("territorycode", value);
        }
    }

    /// <summary>
    /// Choose the sales region or territory for the account to make sure the account is assigned to the correct representative and for use in segmentation and analysis.
    /// </summary>
    [AttributeLogicalName("territoryid")]
    [DataMember]
    public EntityReference TerritoryId
    {
        get
        {
            return GetAttributeValue<EntityReference>("territoryid");
        }
        set
        {
            SetAttributeValue("territoryid", value);
        }
    }

    /// <summary>
    /// Type the stock exchange symbol for the account to track financial performance of the company. You can click the code entered in this field to access the latest trading information from MSN Money.
    /// </summary>
    [AttributeLogicalName("tickersymbol")]
    [DataMember]
    public string TickerSymbol
    {
        get
        {
            return GetAttributeValue<string>("tickersymbol");
        }
        set
        {
            SetAttributeValue("tickersymbol", value);
        }
    }

    /// <summary>
    /// Total time spent for emails (read and write) and meetings by me in relation to account record.
    /// </summary>
    [AttributeLogicalName("timespentbymeonemailandmeetings")]
    [DataMember]
    public string TimeSpentByMeOnEmailAndMeetings
    {
        get
        {
            return GetAttributeValue<string>("timespentbymeonemailandmeetings");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Choose the local currency for the record to make sure budgets are reported in the correct currency.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("traversedpath")]
    [DataMember]
    public string TraversedPath
    {
        get
        {
            return GetAttributeValue<string>("traversedpath");
        }
        set
        {
            SetAttributeValue("traversedpath", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version number of the account.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }

    /// <summary>
    /// Type the account's website URL to get quick details about the company profile.
    /// </summary>
    [AttributeLogicalName("websiteurl")]
    [DataMember]
    public string WebSiteURL
    {
        get
        {
            return GetAttributeValue<string>("websiteurl");
        }
        set
        {
            SetAttributeValue("websiteurl", value);
        }
    }

    /// <summary>
    /// Type the phonetic spelling of the company name, if specified in Japanese, to make sure the name is pronounced correctly in phone calls and other communications.
    /// </summary>
    [AttributeLogicalName("yominame")]
    [DataMember]
    public string YomiName
    {
        get
        {
            return GetAttributeValue<string>("yominame");
        }
        set
        {
            SetAttributeValue("yominame", value);
        }
    }
}

[DataContract()]
public enum AccountState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}