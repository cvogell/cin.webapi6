﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("po_rentalpolicy")]

public class GlobalRentalSetting : Entity
{
    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    internal bool Contains(OptionSetValue po_ThreadColor)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Owner Id
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the business unit that owns the record
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier for the team that owns the record.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    [DataMember]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier for the user that owns the record.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_backgroundcolor")]
    [DataMember]
    public OptionSetValue po_BackgroundColor
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_backgroundcolor");
        }
        set
        {
            SetAttributeValue("po_backgroundcolor", value);
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [AttributeLogicalName("po_name")]
    [DataMember]
    public string po_name
    {
        get
        {
            return GetAttributeValue<string>("po_name");
        }
        set
        {
            SetAttributeValue("po_name", value);
        }
    }

    /// <summary>
    /// Unique identifier for the Cintas agreement process in string form.
    /// </summary>
    [AttributeLogicalName("po_cintasagreementprocessguid")]
    [DataMember]
    public string po_cintasagreementprocessguid
    {
        get
        {
            return GetAttributeValue<string>("po_cintasagreementprocessguid");
        }
        set
        {
            SetAttributeValue("po_cintasagreementprocessguid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the default us currency
    /// </summary>
    [AttributeLogicalName("po_defaultuscurrency")]
    [DataMember]
    public EntityReference po_defaultuscurrency
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_defaultuscurrency");
        }
        set
        {
            SetAttributeValue("po_defaultuscurrency", value);
        }
    }

    /// <summary>
    /// Unique identifier for the quote price level
    /// </summary>
    [AttributeLogicalName("po_quotepricelevelid")]
    [DataMember]
    public EntityReference po_quotepricelevelid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_quotepricelevelid");
        }
        set
        {
            SetAttributeValue("po_quotepricelevelid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_newbusinessopportunitytype")]
    [DataMember]
    public OptionSetValue po_newbusinessopportunitytype
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_newbusinessopportunitytype");
        }
        set
        {
            SetAttributeValue("po_newbusinessopportunitytype", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_poweraddopportunitytype")]
    [DataMember]
    public OptionSetValue po_poweraddopportunitytype
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_poweraddopportunitytype");
        }
        set
        {
            SetAttributeValue("po_poweraddopportunitytype", value);
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [AttributeLogicalName("po_rentalpolicyid")]
    [DataMember]
    public Guid? po_rentalpolicyId
    {
        get
        {
            return GetAttributeValue<Guid?>("po_rentalpolicyid");
        }
        set
        {
            SetAttributeValue("po_rentalpolicyid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("po_rentalpolicyid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            po_rentalpolicyId = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_threadcolor")]
    [DataMember]
    public OptionSetValue po_ThreadColor
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_threadcolor");
        }
        set
        {
            SetAttributeValue("po_threadcolor", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_trimtype")]
    [DataMember]
    public OptionSetValue po_TrimType
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_trimtype");
        }
        set
        {
            SetAttributeValue("po_trimtype", value);
        }
    }

    /// <summary>
    /// Status of the Rental Policy
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public po_rentalpolicyState? statecode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (po_rentalpolicyState)Enum.ToObject(typeof(po_rentalpolicyState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the Rental Policy
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue statuscode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version Number
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}

[DataContract]
public enum po_rentalpolicyState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}