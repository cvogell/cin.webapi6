using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels;


[DataContract()]
public enum ProductRelationshipState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}

/// <summary>
/// Information about the selling relationship between two products, including the relationship type, such as up-sell, cross-sell, substitute, or accessory.
/// </summary>
[DataContract()]
[EntityLogicalName("productsubstitute")]
public class ProductRelationship : Entity
{
    /// <summary>
    /// Shows who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
        set
        {
            SetAttributeValue("createdonbehalfby", value);
        }
    }

    /// <summary>
    /// Select whether the relationship is unidirectional or bidirectional.
    /// </summary>
    [AttributeLogicalName("direction")]
    [DataMember]
    public OptionSetValue Direction
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("direction");
        }
        set
        {
            SetAttributeValue("direction", value);
        }
    }

    /// <summary>
    /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Shows who last updated the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
        set
        {
            SetAttributeValue("modifiedonbehalfby", value);
        }
    }

    /// <summary>
    /// name
    /// </summary>
    [AttributeLogicalName("name")]
    [DataMember]
    public string Name
    {
        get
        {
            return GetAttributeValue<string>("name");
        }
        set
        {
            SetAttributeValue("name", value);
        }
    }

    /// <summary>
    /// Unique identifier for the organization
    /// </summary>
    [AttributeLogicalName("organizationid")]
    [DataMember]
    public EntityReference OrganizationId
    {
        get
        {
            return GetAttributeValue<EntityReference>("organizationid");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Shows the product that the relationship is defined for.
    /// </summary>
    [AttributeLogicalName("productid")]
    [DataMember]
    public EntityReference ProductId
    {
        get
        {
            return GetAttributeValue<EntityReference>("productid");
        }
        set
        {
            SetAttributeValue("productid", value);
        }
    }

    /// <summary>
    /// Shows the unique identifier of the product relationship.
    /// </summary>
    [AttributeLogicalName("productsubstituteid")]
    [DataMember]
    public Guid? ProductSubstituteId
    {
        get
        {
            return GetAttributeValue<Guid?>("productsubstituteid");
        }
        set
        {
            SetAttributeValue("productsubstituteid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("productsubstituteid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            ProductSubstituteId = value;
        }
    }

    /// <summary>
    /// Select the type of the product relationship.
    /// </summary>
    [AttributeLogicalName("salesrelationshiptype")]
    [DataMember]
    public OptionSetValue SalesRelationshipType
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("salesrelationshiptype");
        }
        set
        {
            SetAttributeValue("salesrelationshiptype", value);
        }
    }

    /// <summary>
    /// Select the product relationship's status.
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public ProductRelationshipState? StateCode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (ProductRelationshipState)Enum.ToObject(typeof(ProductRelationshipState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
    }

    /// <summary>
    /// Shows whether the product relationship is active or inactive.
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// Select the related product that the relationship needs to be defined for.
    /// </summary>
    [AttributeLogicalName("substitutedproductid")]
    [DataMember]
    public EntityReference SubstitutedProductId
    {
        get
        {
            return GetAttributeValue<EntityReference>("substitutedproductid");
        }
        set
        {
            SetAttributeValue("substitutedproductid", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Shows the currency associated with the record.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version Number
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }

    //[AttributeLogicalNameAttribute("direction")]
    //public virtual ProductSubstitute_Direction? DirectionEnum
    //{
    //	get
    //	{
    //		return ((ProductSubstitute_Direction?)(EntityOptionSetEnum.GetEnum(this, "direction")));
    //	}
    //	set
    //	{
    //		Direction = value.HasValue ? new OptionSetValue((int)value) : null;
    //	}
    //}

    //[AttributeLogicalNameAttribute("salesrelationshiptype")]
    //public virtual ProductSubstitute_SalesRelationshipType? SalesRelationshipTypeEnum
    //{
    //	get
    //	{
    //		return ((ProductSubstitute_SalesRelationshipType?)(EntityOptionSetEnum.GetEnum(this, "salesrelationshiptype")));
    //	}
    //	set
    //	{
    //		SalesRelationshipType = value.HasValue ? new OptionSetValue((int)value) : null;
    //	}
    //}

    //[AttributeLogicalNameAttribute("statuscode")]
    //public virtual ProductSubstitute_StatusCode? StatusCodeEnum
    //{
    //	get
    //	{
    //		return ((ProductSubstitute_StatusCode?)(EntityOptionSetEnum.GetEnum(this, "statuscode")));
    //	}
    //	set
    //	{
    //		StatusCode = value.HasValue ? new OptionSetValue((int)value) : null;
    //	}
    //}
}