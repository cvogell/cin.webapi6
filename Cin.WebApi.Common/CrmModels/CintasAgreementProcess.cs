﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("po_cintasagreementprocess")]
public class CintasAgreementProcess : Entity
{
    /// <summary>
    /// Unique identifier of the active stage for the Business Process Flow instance.
    /// </summary>
    [AttributeLogicalName("activestageid")]
    public EntityReference ActiveStageId
    {
        get
        {
            return GetAttributeValue<EntityReference>("activestageid");
        }
        set
        {
            SetAttributeValue("activestageid", value);
        }
    }

    /// <summary>
    /// Date and time when current active stage is started
    /// </summary>
    [AttributeLogicalName("activestagestartedon")]
    public DateTime? ActiveStageStartedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("activestagestartedon");
        }
        set
        {
            SetAttributeValue("activestagestartedon", value);
        }
    }

    /// <summary>
    /// Duration of Business Process Flow
    /// </summary>
    [AttributeLogicalName("bpf_duration")]
    public int? bpf_Duration
    {
        get
        {
            return GetAttributeValue<int?>("bpf_duration");
        }
    }

    /// <summary>
    /// Description
    /// </summary>
    [AttributeLogicalName("bpf_name")]
    public string bpf_name
    {
        get
        {
            return GetAttributeValue<string>("bpf_name");
        }
        set
        {
            SetAttributeValue("bpf_name", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("bpf_opportunityid")]
    public EntityReference bpf_opportunityid
    {
        get
        {
            return GetAttributeValue<EntityReference>("bpf_opportunityid");
        }
        set
        {
            SetAttributeValue("bpf_opportunityid", value);
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [AttributeLogicalName("businessprocessflowinstanceid")]
    public Guid? BusinessProcessFlowInstanceId
    {
        get
        {
            return GetAttributeValue<Guid?>("businessprocessflowinstanceid");
        }
        set
        {
            SetAttributeValue("businessprocessflowinstanceid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("businessprocessflowinstanceid")]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            BusinessProcessFlowInstanceId = value;
        }
    }

    /// <summary>
    /// Date and time when Business Process Flow instance is completed.
    /// </summary>
    [AttributeLogicalName("completedon")]
    public DateTime? CompletedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("completedon");
        }
        set
        {
            SetAttributeValue("completedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Unique identifier for the organization
    /// </summary>
    [AttributeLogicalName("organizationid")]
    public EntityReference OrganizationId
    {
        get
        {
            return GetAttributeValue<EntityReference>("organizationid");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the workflow associated to the Business Process Flow instance.
    /// </summary>
    [AttributeLogicalName("processid")]
    public EntityReference ProcessId
    {
        get
        {
            return GetAttributeValue<EntityReference>("processid");
        }
        set
        {
            SetAttributeValue("processid", value);
        }
    }

    /// <summary>
    /// Status of the Cintas Agreement Process
    /// </summary>
    [AttributeLogicalName("statecode")]
    public po_cintasagreementprocessState? statecode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (po_cintasagreementprocessState)Enum.ToObject(typeof(po_cintasagreementprocessState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the Cintas Agreement Process
    /// </summary>
    [AttributeLogicalName("statuscode")]
    public OptionSetValue statuscode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Comma delimited string of process stage ids that represent visited stages of the Business Process Flow instance.
    /// </summary>
    [AttributeLogicalName("traversedpath")]
    public string TraversedPath
    {
        get
        {
            return GetAttributeValue<string>("traversedpath");
        }
        set
        {
            SetAttributeValue("traversedpath", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version Number
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}

[DataContract()]
public enum po_cintasagreementprocessState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}