﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels
{
    [DataContract]
    [EntityLogicalName("opportunity")]
    public class Opportunity : Entity
    {
        /// <summary>
        /// Unique identifier of the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("opportunityid")]
        public Guid? OpportunityId
        {
            get
            {
                return GetAttributeValue<Guid?>("opportunityid");
            }
            set
            {
                SetAttributeValue("opportunityid", value);
                if (value.HasValue)
                {
                    base.Id = value.Value;
                }
                else
                {
                    base.Id = Guid.Empty;
                }
            }
        }

        [DataMember]
        [AttributeLogicalName("opportunityid")]
        public override Guid Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                OpportunityId = value;
            }
        }

        [DataMember]
        [AttributeLogicalName("budgetamount")]
        public Money BudgetAmount
        {
            get
            {
                return GetAttributeValue<Money>("budgetamount");
            }

            set
            {
                SetAttributeValue("budgetamount", value);
            }
        }


        /// <summary>
        /// Unique identifier of the account with which the opportunity is associated.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("accountid")]
        public EntityReference AccountId
        {
            get
            {
                return GetAttributeValue<EntityReference>("accountid");
            }
        }

        /// <summary>
        /// Shows the date and time when the opportunity was closed or canceled.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("actualclosedate")]
        public DateTime? ActualCloseDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("actualclosedate");
            }
            set
            {
                SetAttributeValue("actualclosedate", value);
            }
        }

        /// <summary>
        /// Type the actual revenue amount for the opportunity for reporting and analysis of estimated versus actual sales. Field defaults to the Est. Revenue value when an opportunity is won.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("actualvalue")]
        public Money ActualValue
        {
            get
            {
                return GetAttributeValue<Money>("actualvalue");
            }
            set
            {
                SetAttributeValue("actualvalue", value);

            }
        }
        /// <summary>
        /// Select the likely budget status for the lead's company. This may help determine the lead rating or your sales approach.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("budgetstatus")]
        public OptionSetValue BudgetStatus
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("budgetstatus");
            }
            set
            {
                SetAttributeValue("budgetstatus", value);
            }
        }

        /// <summary>
        /// Shows the campaign that the opportunity was created from. The ID is used for tracking the success of the campaign.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("campaignid")]
        public EntityReference CampaignId
        {
            get
            {
                return GetAttributeValue<EntityReference>("campaignid");
            }
            set
            {
                SetAttributeValue("campaignid", value);
            }
        }

        /// <summary>
        /// Choose whether the proposal feedback has been captured for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("captureproposalfedback")]
        public bool? CaptureProposalFeedback
        {
            get
            {
                return GetAttributeValue<bool?>("captureproposalfeedback");
            }
            set
            {
                SetAttributeValue("captureproposalfeedback", value);
            }
        }

        /// <summary>
        /// Type a number from 0 to 100 that represents the likelihood of closing the opportunity. This can aid the sales team in their efforts to convert the opportunity in a sale.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("closeprobability")]
        public int? CloseProbability
        {
            get
            {
                return GetAttributeValue<int?>("closeprobability");
            }
            set
            {
                SetAttributeValue("closeprobability", value);
            }
        }

        /// <summary>
        /// Select whether a final proposal has been completed for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("completefinalproposal")]
        public bool? CompleteFinalProposal
        {
            get
            {
                return GetAttributeValue<bool?>("completefinalproposal");
            }
            set
            {
                SetAttributeValue("completefinalproposal", value);
            }
        }

        /// <summary>
        /// Select whether an internal review has been completed for this opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("completeinternalreview")]
        public bool? CompleteInternalReview
        {
            get
            {
                return GetAttributeValue<bool?>("completeinternalreview");
            }
            set
            {
                SetAttributeValue("completeinternalreview", value);
            }
        }

        /// <summary>
        /// Select whether the lead confirmed interest in your offerings. This helps in determining the lead quality and the probability of it turning into an opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("confirminterest")]
        public bool? ConfirmInterest
        {
            get
            {
                return GetAttributeValue<bool?>("confirminterest");
            }
            set
            {
                SetAttributeValue("confirminterest", value);
            }
        }

        /// <summary>
        /// Unique identifier of the contact associated with the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("contactid")]
        public EntityReference ContactId
        {
            get
            {
                return GetAttributeValue<EntityReference>("contactid");
            }
        }

        /// <summary>
        /// Shows who created the record.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("createdby")]
        public EntityReference CreatedBy
        {
            get
            {
                return GetAttributeValue<EntityReference>("createdby");
            }
        }

        /// <summary>
        /// Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("createdon")]
        public DateTime? CreatedOn
        {
            get
            {
                return GetAttributeValue<DateTime?>("createdon");
            }
        }

        /// <summary>
        /// Shows who created the record on behalf of another user.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("createdonbehalfby")]
        public EntityReference CreatedOnBehalfBy
        {
            get
            {
                return GetAttributeValue<EntityReference>("createdonbehalfby");
            }
        }

        [DataMember]
        [AttributeLogicalName("ctas_activeproposal")]
        public EntityReference ctas_ActiveProposal
        {
            get
            {
                return GetAttributeValue<EntityReference>("ctas_activeproposal");
            }
            set
            {
                SetAttributeValue("ctas_activeproposal", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_activeproposalagreement")]
        public EntityReference ctas_ActiveProposalAgreement
        {
            get
            {
                return GetAttributeValue<EntityReference>("ctas_activeproposalagreement");
            }
            set
            {
                SetAttributeValue("ctas_activeproposalagreement", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_build_consensus")]
        public bool? ctas_Build_Consensus
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_build_consensus");
            }
            set
            {
                SetAttributeValue("ctas_build_consensus", value);
            }
        }

        /// <summary>
        /// Business Line Used for National Accounts
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_business_line")]
        public OptionSetValue ctas_Business_Line
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_business_line");
            }
            set
            {
                SetAttributeValue("ctas_business_line", value);
            }
        }

        /// <summary>
        /// Business Line for Healthcare
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_business_line_hc")]
        public OptionSetValue ctas_Business_Line_HC
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_business_line_hc");
            }
            set
            {
                SetAttributeValue("ctas_business_line_hc", value);
            }
        }

        /// <summary>
        /// Lookup to Cintas Location
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_cintas_location")]
        public EntityReference ctas_Cintas_Location
        {
            get
            {
                return GetAttributeValue<EntityReference>("ctas_cintas_location");
            }
            set
            {
                SetAttributeValue("ctas_cintas_location", value);
            }
        }

        [DataMember]
        [AttributeLogicalName("ctas_collaborative_effort")]
        public bool? ctas_Collaborative_Effort
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_collaborative_effort");
            }
            set
            {
                SetAttributeValue("ctas_collaborative_effort", value);
            }
        }

        /// <summary>
        /// Competitor for Healthcare.
        /// </summary>
        /// 
        [DataMember]
        [AttributeLogicalName("ctas_competitor_hc")]
        public EntityReference ctas_Competitor_HC
        {
            get
            {
                return GetAttributeValue<EntityReference>("ctas_competitor_hc");
            }
            set
            {
                SetAttributeValue("ctas_competitor_hc", value);
            }
        }

        [DataMember]
        [AttributeLogicalName("ctas_define_the_criteria")]
        public bool? ctas_Define_the_Criteria
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_define_the_criteria");
            }
            set
            {
                SetAttributeValue("ctas_define_the_criteria", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        [DataMember]
        [AttributeLogicalName("ctas_det_financial_impact")]
        public bool? ctas_Det_Financial_Impact
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_det_financial_impact");
            }
            set
            {
                SetAttributeValue("ctas_det_financial_impact", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_determine_constraints")]
        public bool? ctas_Determine_Constraints
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_determine_constraints");
            }
            set
            {
                SetAttributeValue("ctas_determine_constraints", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_dev_eng_strat")]
        public bool? ctas_Dev_Eng_Strat
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_dev_eng_strat");
            }
            set
            {
                SetAttributeValue("ctas_dev_eng_strat", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_ensure_cust_sat")]
        public bool? ctas_Ensure_Cust_Sat
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_ensure_cust_sat");
            }
            set
            {
                SetAttributeValue("ctas_ensure_cust_sat", value);
            }
        }

        /// <summary>
        /// Estimated Weekly Volume
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_est_weekly_volume")]
        public Money ctas_Est_Weekly_Volume
        {
            get
            {
                return GetAttributeValue<Money>("ctas_est_weekly_volume");
            }
            set
            {
                SetAttributeValue("ctas_est_weekly_volume", value);
            }
        }

        /// <summary>
        /// Value of the Est Weekly Volume in base currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_est_weekly_volume_base")]
        public Money ctas_est_weekly_volume_Base
        {
            get
            {
                return GetAttributeValue<Money>("ctas_est_weekly_volume_base");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        [DataMember]
        [AttributeLogicalName("ctas_establish_credibility")]
        public bool? ctas_Establish_Credibility
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_establish_credibility");
            }
            set
            {
                SetAttributeValue("ctas_establish_credibility", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_establish_expectations")]
        public bool? ctas_Establish_Expectations
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_establish_expectations");
            }
            set
            {
                SetAttributeValue("ctas_establish_expectations", value);
            }
        }

        /// <summary>
        /// Revenue for TNI - Currency
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_fire_tni_revenue")]
        public Money ctas_Fire_TNI_Revenue
        {
            get
            {
                return GetAttributeValue<Money>("ctas_fire_tni_revenue");
            }
            set
            {
                SetAttributeValue("ctas_fire_tni_revenue", value);
            }
        }

        /// <summary>
        /// Value of the Fire TNI Revenue in base currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_fire_tni_revenue_base")]
        public Money ctas_fire_tni_revenue_Base
        {
            get
            {
                return GetAttributeValue<Money>("ctas_fire_tni_revenue_base");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_forward_with_prospect")]
        public bool? ctas_Forward_with_Prospect
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_forward_with_prospect");
            }
            set
            {
                SetAttributeValue("ctas_forward_with_prospect", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_hyper_qualification")]
        public bool? ctas_Hyper_Qualification
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_hyper_qualification");
            }
            set
            {
                SetAttributeValue("ctas_hyper_qualification", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        [DataMember]
        [AttributeLogicalName("ctas_manage_change")]
        public bool? ctas_Manage_Change
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_manage_change");
            }
            set
            {
                SetAttributeValue("ctas_manage_change", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("ctas_mitigate_imp_risks")]
        public bool? ctas_Mitigate_Imp_Risks
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_mitigate_imp_risks");
            }
            set
            {
                SetAttributeValue("ctas_mitigate_imp_risks", value);
            }
        }

        /// <summary>
        /// Opportunity Type for Healthcare
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_opportunity_type_hc")]
        public OptionSetValue ctas_Opportunity_Type_HC
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_opportunity_type_hc");
            }
            set
            {
                SetAttributeValue("ctas_opportunity_type_hc", value);
            }
        }

        /// <summary>
        /// Percent to Close contains 25 , 50, 75, 100  % choices.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_percent_to_close")]
        public OptionSetValue ctas_Percent_to_Close
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_percent_to_close");
            }
            set
            {
                SetAttributeValue("ctas_percent_to_close", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_pre_contact_research")]
        public bool? ctas_Pre_contact_Research
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_pre_contact_research");
            }
            set
            {
                SetAttributeValue("ctas_pre_contact_research", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_prepare_present_proposal")]
        public bool? ctas_Prepare_Present_Proposal
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_prepare_present_proposal");
            }
            set
            {
                SetAttributeValue("ctas_prepare_present_proposal", value);
            }
        }

        /// <summary>
        /// Product Line
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_product_line")]
        public string ctas_Product_Line
        {
            get
            {
                return GetAttributeValue<string>("ctas_product_line");
            }
            set
            {
                SetAttributeValue("ctas_product_line", value);
            }
        }

        /// <summary>
        /// Rental or +Uni-Lease
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_program")]
        public OptionSetValue ctas_Program
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_program");
            }
            set
            {
                SetAttributeValue("ctas_program", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_quantify_actual_costs")]
        public bool? ctas_Quantify_Actual_Costs
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_quantify_actual_costs");
            }
            set
            {
                SetAttributeValue("ctas_quantify_actual_costs", value);
            }
        }

        /// <summary>
        /// Unique identifier for F1000 associated with Opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_regardingid")]
        public EntityReference ctas_RegardingId
        {
            get
            {
                return GetAttributeValue<EntityReference>("ctas_regardingid");
            }
            set
            {
                SetAttributeValue("ctas_regardingid", value);
            }
        }

        /// <summary>
        /// Sales Phase - Drop Down
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_sales_phase")]
        public OptionSetValue ctas_Sales_Phase
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_sales_phase");
            }
            set
            {
                SetAttributeValue("ctas_sales_phase", value);
            }
        }

        /// <summary>
        /// Sales Phase for HC
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_sales_phase_hc")]
        public OptionSetValue ctas_Sales_Phase_HC
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_sales_phase_hc");
            }
            set
            {
                SetAttributeValue("ctas_sales_phase_hc", value);
            }
        }

        /// <summary>
        /// Sales Stage for Healthcare
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_sales_stage_hc")]
        public OptionSetValue ctas_Sales_Stage_HC
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_sales_stage_hc");
            }
            set
            {
                SetAttributeValue("ctas_sales_stage_hc", value);
            }
        }

        /// <summary>
        /// Recurring and Non Recurring
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_sales_volume")]
        public OptionSetValue ctas_Sales_Volume
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("ctas_sales_volume");
            }
            set
            {
                SetAttributeValue("ctas_sales_volume", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_shift_emphasis")]
        public bool? ctas_Shift_Emphasis
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_shift_emphasis");
            }
            set
            {
                SetAttributeValue("ctas_shift_emphasis", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_uncover_evidence")]
        public bool? ctas_Uncover_Evidence
        {
            get
            {
                return GetAttributeValue<bool?>("ctas_uncover_evidence");
            }
            set
            {
                SetAttributeValue("ctas_uncover_evidence", value);
            }
        }

        /// <summary>
        /// Type notes about the company or organization associated with the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("currentsituation")]
        public string CurrentSituation
        {
            get
            {
                return GetAttributeValue<string>("currentsituation");
            }
            set
            {
                SetAttributeValue("currentsituation", value);
            }
        }

        /// <summary>
        /// Select the customer account or contact to provide a quick link to additional customer details, such as address, phone number, activities, and orders.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("customerid")]
        public EntityReference CustomerId
        {
            get
            {
                return GetAttributeValue<EntityReference>("customerid");
            }
            set
            {
                SetAttributeValue("customerid", value);
            }
        }

        /// <summary>
        /// Type some notes about the customer's requirements, to help the sales team identify products and services that could meet their requirements.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("customerneed")]
        public string CustomerNeed
        {
            get
            {
                return GetAttributeValue<string>("customerneed");
            }
            set
            {
                SetAttributeValue("customerneed", value);
            }
        }

        /// <summary>
        /// Type notes about the customer's pain points to help the sales team identify products and services that could address these pain points.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("customerpainpoints")]
        public string CustomerPainPoints
        {
            get
            {
                return GetAttributeValue<string>("customerpainpoints");
            }
            set
            {
                SetAttributeValue("customerpainpoints", value);
            }
        }

        /// <summary>
        /// Select whether your notes include information about who makes the purchase decisions at the lead's company.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("decisionmaker")]
        public bool? DecisionMaker
        {
            get
            {
                return GetAttributeValue<bool?>("decisionmaker");
            }
            set
            {
                SetAttributeValue("decisionmaker", value);
            }
        }

        /// <summary>
        /// Type additional information to describe the opportunity, such as possible products to sell or past purchases from the customer.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("description")]
        public string Description
        {
            get
            {
                return GetAttributeValue<string>("description");
            }
            set
            {
                SetAttributeValue("description", value);
            }
        }

        /// <summary>
        /// Select whether a proposal has been developed for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("developproposal")]
        public bool? DevelopProposal
        {
            get
            {
                return GetAttributeValue<bool?>("developproposal");
            }
            set
            {
                SetAttributeValue("developproposal", value);
            }
        }

        /// <summary>
        /// Type the discount amount for the opportunity if the customer is eligible for special savings.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("discountamount")]
        public Money DiscountAmount
        {
            get
            {
                return GetAttributeValue<Money>("discountamount");
            }
            set
            {
                SetAttributeValue("discountamount", value);
            }
        }

        /// <summary>
        /// Shows the Opportunity Discount Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("discountamount_base")]
        public Money DiscountAmount_Base
        {
            get
            {
                return GetAttributeValue<Money>("discountamount_base");
            }
        }

        /// <summary>
        /// Type the discount rate that should be applied to the Product Totals field to include additional savings for the customer in the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("discountpercentage")]
        public decimal? DiscountPercentage
        {
            get
            {
                return GetAttributeValue<decimal?>("discountpercentage");
            }
            set
            {
                SetAttributeValue("discountpercentage", value);
            }
        }

        /// <summary>
        /// Enter the expected closing date of the opportunity to help make accurate revenue forecasts.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("estimatedclosedate")]
        public DateTime? EstimatedCloseDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("estimatedclosedate");
            }
            set
            {
                SetAttributeValue("estimatedclosedate", value);
            }
        }

        /// <summary>
        /// Type the estimated revenue amount to indicate the potential sale or value of the opportunity for revenue forecasting. This field can be either system-populated or editable based on the selection in the Revenue field.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("estimatedvalue")]
        public Money EstimatedValue
        {
            get
            {
                return GetAttributeValue<Money>("estimatedvalue");
            }
            set
            {
                SetAttributeValue("estimatedvalue", value);
            }
        }

        /// <summary>
        /// Shows the Actual Revenue field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("estimatedvalue_base")]
        public Money EstimatedValue_Base
        {
            get
            {
                return GetAttributeValue<Money>("estimatedvalue_base");
            }
        }

        /// <summary>
        /// Select whether the fit between the lead's requirements and your offerings was evaluated.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("evaluatefit")]
        public bool? EvaluateFit
        {
            get
            {
                return GetAttributeValue<bool?>("evaluatefit");
            }
            set
            {
                SetAttributeValue("evaluatefit", value);
            }
        }

        /// <summary>
        /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("exchangerate")]
        public decimal? ExchangeRate
        {
            get
            {
                return GetAttributeValue<decimal?>("exchangerate");
            }
        }

        /// <summary>
        /// Choose whether the sales team has recorded detailed notes on the proposals and the account's responses.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("filedebrief")]
        public bool? FileDebrief
        {
            get
            {
                return GetAttributeValue<bool?>("filedebrief");
            }
            set
            {
                SetAttributeValue("filedebrief", value);
            }
        }

        /// <summary>
        /// Enter the date and time when the final decision of the opportunity was made.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("finaldecisiondate")]
        public DateTime? FinalDecisionDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("finaldecisiondate");
            }
            set
            {
                SetAttributeValue("finaldecisiondate", value);
            }
        }

        /// <summary>
        /// Type the cost of freight or shipping for the products included in the opportunity for use in calculating the Total Amount field.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("freightamount")]
        public Money FreightAmount
        {
            get
            {
                return GetAttributeValue<Money>("freightamount");
            }
            set
            {
                SetAttributeValue("freightamount", value);
            }
        }

        /// <summary>
        /// Shows the Freight Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("freightamount_base")]
        public Money FreightAmount_Base
        {
            get
            {
                return GetAttributeValue<Money>("freightamount_base");
            }
        }

        /// <summary>
        /// Select whether information about competitors is included.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("identifycompetitors")]
        public bool? IdentifyCompetitors
        {
            get
            {
                return GetAttributeValue<bool?>("identifycompetitors");
            }
            set
            {
                SetAttributeValue("identifycompetitors", value);
            }
        }

        /// <summary>
        /// Select whether the customer contacts for this opportunity have been identified.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("identifycustomercontacts")]
        public bool? IdentifyCustomerContacts
        {
            get
            {
                return GetAttributeValue<bool?>("identifycustomercontacts");
            }
            set
            {
                SetAttributeValue("identifycustomercontacts", value);
            }
        }

        /// <summary>
        /// Choose whether you have recorded who will pursue the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("identifypursuitteam")]
        public bool? IdentifyPursuitTeam
        {
            get
            {
                return GetAttributeValue<bool?>("identifypursuitteam");
            }
            set
            {
                SetAttributeValue("identifypursuitteam", value);
            }
        }

        /// <summary>
        /// Unique identifier of the data import or data migration that created this record.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("importsequencenumber")]
        public int? ImportSequenceNumber
        {
            get
            {
                return GetAttributeValue<int?>("importsequencenumber");
            }
            set
            {
                SetAttributeValue("importsequencenumber", value);
            }
        }

        /// <summary>
        /// Choose whether someone from the sales team contacted this lead earlier.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("initialcommunication")]
        public OptionSetValue InitialCommunication
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("initialcommunication");
            }
            set
            {
                SetAttributeValue("initialcommunication", value);
            }
        }

        /// <summary>
        /// Select whether the estimated revenue for the opportunity is calculated automatically based on the products entered or entered manually by a user.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("isrevenuesystemcalculated")]
        public bool? IsRevenueSystemCalculated
        {
            get
            {
                return GetAttributeValue<bool?>("isrevenuesystemcalculated");
            }
            set
            {
                SetAttributeValue("isrevenuesystemcalculated", value);
            }
        }

        /// <summary>
        /// Contains the date time stamp of the last on hold time.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("lastonholdtime")]
        public DateTime? LastOnHoldTime
        {
            get
            {
                return GetAttributeValue<DateTime?>("lastonholdtime");
            }
            set
            {
                SetAttributeValue("lastonholdtime", value);
            }
        }

        /// <summary>
        /// Shows who last updated the record.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("modifiedby")]
        public EntityReference ModifiedBy
        {
            get
            {
                return GetAttributeValue<EntityReference>("modifiedby");
            }
        }

        /// <summary>
        /// Shows the date and time when the record was last updated. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("modifiedon")]
        public DateTime? ModifiedOn
        {
            get
            {
                return GetAttributeValue<DateTime?>("modifiedon");
            }
        }

        /// <summary>
        /// Shows who last updated the record on behalf of another user.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("modifiedonbehalfby")]
        public EntityReference ModifiedOnBehalfBy
        {
            get
            {
                return GetAttributeValue<EntityReference>("modifiedonbehalfby");
            }
        }

        /// <summary>
        /// Type a subject or descriptive name, such as the expected order or company name, for the opportunity. This will be auto populated if left blank
        /// </summary>
        [DataMember]
        [AttributeLogicalName("name")]
        public string Name
        {
            get
            {
                return GetAttributeValue<string>("name");
            }
            set
            {
                SetAttributeValue("name", value);
            }
        }

        /// <summary>
        /// Choose how high the level of need is for the lead's company.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("need")]
        public OptionSetValue Need
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("need");
            }
            set
            {
                SetAttributeValue("need", value);
            }
        }

        /// <summary>
        /// Shows the duration in minutes for which the opportunity was on hold.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("onholdtime")]
        public int? OnHoldTime
        {
            get
            {
                return GetAttributeValue<int?>("onholdtime");
            }
        }

        /// <summary>
        /// Select the expected value or priority of the opportunity based on revenue, customer status, or closing probability.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("opportunityratingcode")]
        public OptionSetValue OpportunityRatingCode
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("opportunityratingcode");
            }
            set
            {
                SetAttributeValue("opportunityratingcode", value);
            }
        }

        /// <summary>
        /// Choose the lead that the opportunity was created from for reporting and analytics. The field is read-only after the opportunity is created and defaults to the correct lead when an opportunity is created from a converted lead.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("originatingleadid")]
        public EntityReference OriginatingLeadId
        {
            get
            {
                return GetAttributeValue<EntityReference>("originatingleadid");
            }
            set
            {
                SetAttributeValue("originatingleadid", value);
            }
        }

        /// <summary>
        /// Date and time that the record was migrated.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("overriddencreatedon")]
        public DateTime? OverriddenCreatedOn
        {
            get
            {
                return GetAttributeValue<DateTime?>("overriddencreatedon");
            }
            set
            {
                SetAttributeValue("overriddencreatedon", value);
            }
        }

        /// <summary>
        /// Enter the user or team who is assigned to manage the record. This field is updated every time the record is assigned to a different user.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ownerid")]
        public EntityReference OwnerId
        {
            get
            {
                return GetAttributeValue<EntityReference>("ownerid");
            }
            set
            {
                SetAttributeValue("ownerid", value);
            }
        }

        /// <summary>
        /// Unique identifier of the business unit that owns the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("owningbusinessunit")]
        public EntityReference OwningBusinessUnit
        {
            get
            {
                return GetAttributeValue<EntityReference>("owningbusinessunit");
            }
        }

        /// <summary>
        /// Unique identifier of the team who owns the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("owningteam")]
        public EntityReference OwningTeam
        {
            get
            {
                return GetAttributeValue<EntityReference>("owningteam");
            }
        }

        /// <summary>
        /// Unique identifier of the user who owns the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("owninguser")]
        public EntityReference OwningUser
        {
            get
            {
                return GetAttributeValue<EntityReference>("owninguser");
            }
        }


        /// <summary>
        /// Choose an account to connect this opportunity to, so that the relationship is visible in reports and analytics, and to provide a quick link to additional details, such as financial information and activities.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("parentaccountid")]
        public EntityReference ParentAccountId
        {
            get
            {
                return GetAttributeValue<EntityReference>("parentaccountid");
            }
            set
            {
                SetAttributeValue("parentaccountid", value);
            }
        }

        /// <summary>
        /// Choose a contact to connect this opportunity to, so that the relationship is visible in reports and analytics.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("parentcontactid")]
        public EntityReference ParentContactId
        {
            get
            {
                return GetAttributeValue<EntityReference>("parentcontactid");
            }
            set
            {
                SetAttributeValue("parentcontactid", value);
            }
        }

        /// <summary>
        /// Information about whether the opportunity participates in workflow rules.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("participatesinworkflow")]
        public bool? ParticipatesInWorkflow
        {
            get
            {
                return GetAttributeValue<bool?>("participatesinworkflow");
            }
            set
            {
                SetAttributeValue("participatesinworkflow", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_13weekenddate")]
        public DateTime? po_13WeekEndDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_13weekenddate");
            }
            set
            {
                SetAttributeValue("po_13weekenddate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_actualvolume")]
        public Money po_ActualVolume
        {
            get
            {
                return GetAttributeValue<Money>("po_actualvolume");
            }
        }

        /// <summary>
        /// Value of the Actual Volume in base currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_actualvolume_base")]
        public Money po_actualvolume_Base
        {
            get
            {
                return GetAttributeValue<Money>("po_actualvolume_base");
            }
        }

        /// <summary>
        /// Last Updated time of rollup field Actual Volume.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_actualvolume_date")]
        public DateTime? po_ActualVolume_Date
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_actualvolume_date");
            }
        }

        /// <summary>
        /// State of rollup field Actual Volume.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_actualvolume_state")]
        public int? po_ActualVolume_State
        {
            get
            {
                return GetAttributeValue<int?>("po_actualvolume_state");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_addtostatusreport")]
        public bool? po_AddtoStatusReport
        {
            get
            {
                return GetAttributeValue<bool?>("po_addtostatusreport");
            }
            set
            {
                SetAttributeValue("po_addtostatusreport", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("ctas_agreementsentdate")]
        public DateTime? ctas_AgreementSentDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("ctas_agreementsentdate");
            }
            set
            {
                SetAttributeValue("ctas_agreementsentdate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_awarenesscampaign")]
        public bool? po_AwarenessCampaign
        {
            get
            {
                return GetAttributeValue<bool?>("po_awarenesscampaign");
            }
            set
            {
                SetAttributeValue("po_awarenesscampaign", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_businessdivision_opp_f1000")]
        public OptionSetValue po_BusinessDivision_opp_F1000
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("po_businessdivision_opp_f1000");
            }
            set
            {
                SetAttributeValue("po_businessdivision_opp_f1000", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_businessdivisionid")]
        public EntityReference po_BusinessDivisionid
        {
            get
            {
                return GetAttributeValue<EntityReference>("po_businessdivisionid");
            }
            set
            {
                SetAttributeValue("po_businessdivisionid", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_businesslineadds")]
        public bool? po_BusinessLineAdds
        {
            get
            {
                return GetAttributeValue<bool?>("po_businesslineadds");
            }
            set
            {
                SetAttributeValue("po_businesslineadds", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_clientresearch")]
        public bool? po_ClientResearch
        {
            get
            {
                return GetAttributeValue<bool?>("po_clientresearch");
            }
            set
            {
                SetAttributeValue("po_clientresearch", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_competitor")]
        public EntityReference po_Competitor
        {
            get
            {
                return GetAttributeValue<EntityReference>("po_competitor");
            }
            set
            {
                SetAttributeValue("po_competitor", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_contractrenewalxdate")]
        public DateTime? po_ContractRenewalXDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_contractrenewalxdate");
            }
            set
            {
                SetAttributeValue("po_contractrenewalxdate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_executivesummary")]
        public string po_ExecutiveSummary
        {
            get
            {
                return GetAttributeValue<string>("po_executivesummary");
            }
            set
            {
                SetAttributeValue("po_executivesummary", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_expectedrenewaldate")]
        public DateTime? po_ExpectedRenewalDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_expectedrenewaldate");
            }
            set
            {
                SetAttributeValue("po_expectedrenewaldate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_firstappointment")]
        public DateTime? po_FirstAppointment
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_firstappointment");
            }
            set
            {
                SetAttributeValue("po_firstappointment", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_firstphonecall")]
        public DateTime? po_FirstPhoneCall
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_firstphonecall");
            }
            set
            {
                SetAttributeValue("po_firstphonecall", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_growthconnection")]
        public bool? po_GrowthConnection
        {
            get
            {
                return GetAttributeValue<bool?>("po_growthconnection");
            }
            set
            {
                SetAttributeValue("po_growthconnection", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_hoppertimeline")]
        public OptionSetValue po_HopperTimeline
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("po_hoppertimeline");
            }
            set
            {
                SetAttributeValue("po_hoppertimeline", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("po_identifyoptions")]
        public bool? po_IdentifyOptions
        {
            get
            {
                return GetAttributeValue<bool?>("po_identifyoptions");
            }
            set
            {
                SetAttributeValue("po_identifyoptions", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_installdate")]
        public DateTime? po_InstallDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_installdate");
            }
            set
            {
                SetAttributeValue("po_installdate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_interviewstakeholders")]
        public bool? po_InterviewStakeholders
        {
            get
            {
                return GetAttributeValue<bool?>("po_interviewstakeholders");
            }
            set
            {
                SetAttributeValue("po_interviewstakeholders", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_jeopardyflag")]
        public bool? po_Jeopardyflag
        {
            get
            {
                return GetAttributeValue<bool?>("po_jeopardyflag");
            }
            set
            {
                SetAttributeValue("po_jeopardyflag", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_markedassoldweeklyvolume")]
        public Money po_MarkedasSoldWeeklyVolume
        {
            get
            {
                return GetAttributeValue<Money>("po_markedassoldweeklyvolume");
            }
            set
            {
                SetAttributeValue("po_markedassoldweeklyvolume", value);
            }
        }

        /// <summary>
        /// Value of the Marked as Sold Weekly Volume in base currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_markedassoldweeklyvolume_base")]
        public Money po_markedassoldweeklyvolume_Base
        {
            get
            {
                return GetAttributeValue<Money>("po_markedassoldweeklyvolume_base");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_newstorefronts")]
        public bool? po_NewStoreFronts
        {
            get
            {
                return GetAttributeValue<bool?>("po_newstorefronts");
            }
            set
            {
                SetAttributeValue("po_newstorefronts", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_no_of_sites_unserved")]
        public int? po_No_of_Sites_Unserved
        {
            get
            {
                return GetAttributeValue<int?>("po_no_of_sites_unserved");
            }
            set
            {
                SetAttributeValue("po_no_of_sites_unserved", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_noofproposals")]
        public int? po_NoofProposals
        {
            get
            {
                return GetAttributeValue<int?>("po_noofproposals");
            }
            set
            {
                SetAttributeValue("po_noofproposals", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_numberofsites")]
        public int? po_NumberofSites
        {
            get
            {
                return GetAttributeValue<int?>("po_numberofsites");
            }
            set
            {
                SetAttributeValue("po_numberofsites", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_opportunitytype")]
        public OptionSetValue po_OpportunityType
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("po_opportunitytype");
            }
            set
            {
                SetAttributeValue("po_opportunitytype", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_orderedkeyedweeklyvolume")]
        public Money po_OrderedKeyedWeeklyVolume
        {
            get
            {
                return GetAttributeValue<Money>("po_orderedkeyedweeklyvolume");
            }
            set
            {
                SetAttributeValue("po_orderedkeyedweeklyvolume", value);
            }
        }

        /// <summary>
        /// Value of the Ordered/Keyed Weekly Volume in base currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_orderedkeyedweeklyvolume_base")]
        public Money po_orderedkeyedweeklyvolume_Base
        {
            get
            {
                return GetAttributeValue<Money>("po_orderedkeyedweeklyvolume_base");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_penetrationoppourtunity")]
        public bool? po_PenetrationOppourtunity
        {
            get
            {
                return GetAttributeValue<bool?>("po_penetrationoppourtunity");
            }
            set
            {
                SetAttributeValue("po_penetrationoppourtunity", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_percentage_of_sites_served")]
        public decimal? po_Percentage_of_Sites_Served
        {
            get
            {
                return GetAttributeValue<decimal?>("po_percentage_of_sites_served");
            }
            set
            {
                SetAttributeValue("po_percentage_of_sites_served", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_potentialvalue")]
        public Money po_PotentialValue
        {
            get
            {
                return GetAttributeValue<Money>("po_potentialvalue");
            }
            set
            {
                SetAttributeValue("po_potentialvalue", value);
            }
        }

        /// <summary>
        /// Value of the Potential Value in base currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_potentialvalue_base")]
        public Money po_potentialvalue_Base
        {
            get
            {
                return GetAttributeValue<Money>("po_potentialvalue_base");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_presentationdate")]
        public DateTime? po_PresentationDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_presentationdate");
            }
            set
            {
                SetAttributeValue("po_presentationdate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_proposalassigned")]
        public bool? po_ProposalAssigned
        {
            get
            {
                return GetAttributeValue<bool?>("po_proposalassigned");
            }
            set
            {
                SetAttributeValue("po_proposalassigned", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_proposalexpireddate")]
        public DateTime? po_ProposalExpiredDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_proposalexpireddate");
            }
            set
            {
                SetAttributeValue("po_proposalexpireddate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_proposalsentdate")]
        public DateTime? po_ProposalSentDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_proposalsentdate");
            }
            set
            {
                SetAttributeValue("po_proposalsentdate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_reasonforagreementrejection")]
        public string po_ReasonforAgreementRejection
        {
            get
            {
                return GetAttributeValue<string>("po_reasonforagreementrejection");
            }
            set
            {
                SetAttributeValue("po_reasonforagreementrejection", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_rejectedby")]
        public string po_RejectedBy
        {
            get
            {
                return GetAttributeValue<string>("po_rejectedby");
            }
            set
            {
                SetAttributeValue("po_rejectedby", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_renewaldate")]
        public DateTime? po_RenewalDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_renewaldate");
            }
            set
            {
                SetAttributeValue("po_renewaldate", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_secureinternalagreement")]
        public bool? po_SecureInternalAgreement
        {
            get
            {
                return GetAttributeValue<bool?>("po_secureinternalagreement");
            }
            set
            {
                SetAttributeValue("po_secureinternalagreement", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_sitevisits")]
        public bool? po_SiteVisits
        {
            get
            {
                return GetAttributeValue<bool?>("po_sitevisits");
            }
            set
            {
                SetAttributeValue("po_sitevisits", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_topopportunity")]
        public bool? po_TopOpportunity
        {
            get
            {
                return GetAttributeValue<bool?>("po_topopportunity");
            }
            set
            {
                SetAttributeValue("po_topopportunity", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_transitionoftrust")]
        public bool? po_TransitionofTrust
        {
            get
            {
                return GetAttributeValue<bool?>("po_transitionoftrust");
            }
            set
            {
                SetAttributeValue("po_transitionoftrust", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_worinstalledvolume")]
        public Money po_WORInstalledVolume
        {
            get
            {
                return GetAttributeValue<Money>("po_worinstalledvolume");
            }
            set
            {
                SetAttributeValue("po_worinstalledvolume", value);
            }
        }

        /// <summary>
        /// Value of the WOR Installed Volume in base currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_worinstalledvolume_base")]
        public Money po_worinstalledvolume_Base
        {
            get
            {
                return GetAttributeValue<Money>("po_worinstalledvolume_base");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_wornewbusinessvolume")]
        public Money po_WORNewBusinessVolume
        {
            get
            {
                return GetAttributeValue<Money>("po_wornewbusinessvolume");
            }
            set
            {
                SetAttributeValue("po_wornewbusinessvolume", value);
            }
        }

        /// <summary>
        /// Value of the WOR New Business Volume in base currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_wornewbusinessvolume_base")]
        public Money po_wornewbusinessvolume_Base
        {
            get
            {
                return GetAttributeValue<Money>("po_wornewbusinessvolume_base");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [AttributeLogicalName("po_xdate")]
        public DateTime? po_XDate
        {
            get
            {
                return GetAttributeValue<DateTime?>("po_xdate");
            }
            set
            {
                SetAttributeValue("po_xdate", value);
            }
        }

        /// <summary>
        /// Select whether the final proposal has been presented to the account.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("presentfinalproposal")]
        public bool? PresentFinalProposal
        {
            get
            {
                return GetAttributeValue<bool?>("presentfinalproposal");
            }
            set
            {
                SetAttributeValue("presentfinalproposal", value);
            }
        }

        /// <summary>
        /// Select whether a proposal for the opportunity has been presented to the account.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("presentproposal")]
        public bool? PresentProposal
        {
            get
            {
                return GetAttributeValue<bool?>("presentproposal");
            }
            set
            {
                SetAttributeValue("presentproposal", value);
            }
        }

        /// <summary>
        /// Choose the price list associated with this record to make sure the products associated with the campaign are offered at the correct prices.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("pricelevelid")]
        public EntityReference PriceLevelId
        {
            get
            {
                return GetAttributeValue<EntityReference>("pricelevelid");
            }
            set
            {
                SetAttributeValue("pricelevelid", value);
            }
        }

        /// <summary>
        /// Pricing error for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("pricingerrorcode")]
        public OptionSetValue PricingErrorCode
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("pricingerrorcode");
            }
            set
            {
                SetAttributeValue("pricingerrorcode", value);
            }
        }

        /// <summary>
        /// Select the priority so that preferred customers or critical issues are handled quickly.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("prioritycode")]
        public OptionSetValue PriorityCode
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("prioritycode");
            }
            set
            {
                SetAttributeValue("prioritycode", value);
            }
        }

        /// <summary>
        /// Shows the ID of the process.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("processid")]
        public Guid? ProcessId
        {
            get
            {
                return GetAttributeValue<Guid?>("processid");
            }
            set
            {
                SetAttributeValue("processid", value);
            }
        }

        /// <summary>
        /// Type notes about the proposed solution for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("proposedsolution")]
        public string ProposedSolution
        {
            get
            {
                return GetAttributeValue<string>("proposedsolution");
            }
            set
            {
                SetAttributeValue("proposedsolution", value);
            }
        }

        /// <summary>
        /// Choose whether an individual or a committee will be involved in the purchase process for the lead.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("purchaseprocess")]
        public OptionSetValue PurchaseProcess
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("purchaseprocess");
            }
            set
            {
                SetAttributeValue("purchaseprocess", value);
            }
        }

        /// <summary>
        /// Choose how long the lead will likely take to make the purchase.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("purchasetimeframe")]
        public OptionSetValue PurchaseTimeframe
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("purchasetimeframe");
            }
            set
            {
                SetAttributeValue("purchasetimeframe", value);
            }
        }

        /// <summary>
        /// Select whether the decision about pursuing the opportunity has been made.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("pursuitdecision")]
        public bool? PursuitDecision
        {
            get
            {
                return GetAttributeValue<bool?>("pursuitdecision");
            }
            set
            {
                SetAttributeValue("pursuitdecision", value);
            }
        }

        /// <summary>
        /// Type comments about the qualification or scoring of the lead.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("qualificationcomments")]
        public string QualificationComments
        {
            get
            {
                return GetAttributeValue<string>("qualificationcomments");
            }
            set
            {
                SetAttributeValue("qualificationcomments", value);
            }
        }

        /// <summary>
        /// Type comments about the quotes associated with the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("quotecomments")]
        public string QuoteComments
        {
            get
            {
                return GetAttributeValue<string>("quotecomments");
            }
            set
            {
                SetAttributeValue("quotecomments", value);
            }
        }

        /// <summary>
        /// Choose whether the proposal feedback has been captured and resolved for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("resolvefeedback")]
        public bool? ResolveFeedback
        {
            get
            {
                return GetAttributeValue<bool?>("resolvefeedback");
            }
            set
            {
                SetAttributeValue("resolvefeedback", value);
            }
        }

        /// <summary>
        /// Select the sales stage of this opportunity to aid the sales team in their efforts to win this opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("salesstage")]
        public OptionSetValue SalesStage
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("salesstage");
            }
            set
            {
                SetAttributeValue("salesstage", value);
            }
        }

        /// <summary>
        /// Select the sales process stage for the opportunity to indicate the probability of closing the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("salesstagecode")]
        public OptionSetValue SalesStageCode
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("salesstagecode");
            }
            set
            {
                SetAttributeValue("salesstagecode", value);
            }
        }

        /// <summary>
        /// Enter the date and time of the prospecting follow-up meeting with the lead.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("schedulefollowup_prospect")]
        public DateTime? ScheduleFollowup_Prospect
        {
            get
            {
                return GetAttributeValue<DateTime?>("schedulefollowup_prospect");
            }
            set
            {
                SetAttributeValue("schedulefollowup_prospect", value);
            }
        }

        /// <summary>
        /// Enter the date and time of the qualifying follow-up meeting with the lead.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("schedulefollowup_qualify")]
        public DateTime? ScheduleFollowup_Qualify
        {
            get
            {
                return GetAttributeValue<DateTime?>("schedulefollowup_qualify");
            }
            set
            {
                SetAttributeValue("schedulefollowup_qualify", value);
            }
        }

        /// <summary>
        /// Enter the date and time of the proposal meeting for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("scheduleproposalmeeting")]
        public DateTime? ScheduleProposalMeeting
        {
            get
            {
                return GetAttributeValue<DateTime?>("scheduleproposalmeeting");
            }
            set
            {
                SetAttributeValue("scheduleproposalmeeting", value);
            }
        }

        /// <summary>
        /// Select whether a thank you note has been sent to the account for considering the proposal.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("sendthankyounote")]
        public bool? SendThankYouNote
        {
            get
            {
                return GetAttributeValue<bool?>("sendthankyounote");
            }
            set
            {
                SetAttributeValue("sendthankyounote", value);
            }
        }

        /// <summary>
        /// Choose the service level agreement (SLA) that you want to apply to the opportunity record.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("slaid")]
        public EntityReference SLAId
        {
            get
            {
                return GetAttributeValue<EntityReference>("slaid");
            }
            set
            {
                SetAttributeValue("slaid", value);
            }
        }

        /// <summary>
        /// Last SLA that was applied to this opportunity. This field is for internal use only.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("slainvokedid")]
        public EntityReference SLAInvokedId
        {
            get
            {
                return GetAttributeValue<EntityReference>("slainvokedid");
            }
        }

        /// <summary>
        /// Shows the ID of the stage.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("stageid")]
        public Guid? StageId
        {
            get
            {
                return GetAttributeValue<Guid?>("stageid");
            }
            set
            {
                SetAttributeValue("stageid", value);
            }
        }

        /// <summary>
        /// Shows whether the opportunity is open, won, or lost. Won and lost opportunities are read-only and can't be edited until they are reactivated.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("statecode")]
        public OpportunityState? StateCode
        {
            get
            {
                OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
                if (optionSet != null)
                {
                    return (OpportunityState)Enum.ToObject(typeof(OpportunityState), optionSet.Value);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value == null)
                {
                    SetAttributeValue("statecode", null);
                }
                else
                {
                    SetAttributeValue("statecode", new OptionSetValue((int)value));
                }
            }
        }

        /// <summary>
        /// Select the opportunity's status.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("statuscode")]
        public OptionSetValue StatusCode
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("statuscode");
            }
            set
            {
                SetAttributeValue("statuscode", value);
            }
        }

        /// <summary>
        /// Shows the ID of the workflow step.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("stepid")]
        public Guid? StepId
        {
            get
            {
                return GetAttributeValue<Guid?>("stepid");
            }
            set
            {
                SetAttributeValue("stepid", value);
            }
        }

        /// <summary>
        /// Shows the current phase in the sales pipeline for the opportunity. This is updated by a workflow.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("stepname")]
        public string StepName
        {
            get
            {
                return GetAttributeValue<string>("stepname");
            }
            set
            {
                SetAttributeValue("stepname", value);
            }
        }

        /// <summary>
        /// Select when the opportunity is likely to be closed.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("timeline")]
        public OptionSetValue TimeLine
        {
            get
            {
                return GetAttributeValue<OptionSetValue>("timeline");
            }
            set
            {
                SetAttributeValue("timeline", value);
            }
        }

        /// <summary>
        /// Total time spent for emails (read and write) and meetings by me in relation to the opportunity record.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("timespentbymeonemailandmeetings")]
        public string TimeSpentByMeOnEmailAndMeetings
        {
            get
            {
                return GetAttributeValue<string>("timespentbymeonemailandmeetings");
            }
        }

        /// <summary>
        /// For internal use only.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("timezoneruleversionnumber")]
        public int? TimeZoneRuleVersionNumber
        {
            get
            {
                return GetAttributeValue<int?>("timezoneruleversionnumber");
            }
            set
            {
                SetAttributeValue("timezoneruleversionnumber", value);
            }
        }

        /// <summary>
        /// Shows the total amount due, calculated as the sum of the products, discounts, freight, and taxes for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totalamount")]
        public Money TotalAmount
        {
            get
            {
                return GetAttributeValue<Money>("totalamount");
            }
            set
            {
                SetAttributeValue("totalamount", value);
            }
        }

        /// <summary>
        /// Shows the Total Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totalamount_base")]
        public Money TotalAmount_Base
        {
            get
            {
                return GetAttributeValue<Money>("totalamount_base");
            }
        }

        /// <summary>
        /// Shows the total product amount for the opportunity, minus any discounts. This value is added to freight and tax amounts in the calculation for the total amount of the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totalamountlessfreight")]
        public Money TotalAmountLessFreight
        {
            get
            {
                return GetAttributeValue<Money>("totalamountlessfreight");
            }
            set
            {
                SetAttributeValue("totalamountlessfreight", value);
            }
        }

        /// <summary>
        /// Shows the Total Pre-Freight Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [AttributeLogicalName("totalamountlessfreight_base")]
        public Money TotalAmountLessFreight_Base
        {
            get
            {
                return GetAttributeValue<Money>("totalamountlessfreight_base");
            }
        }

        /// <summary>
        /// Shows the total discount amount, based on the discount price and rate entered on the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totaldiscountamount")]
        public Money TotalDiscountAmount
        {
            get
            {
                return GetAttributeValue<Money>("totaldiscountamount");
            }
            set
            {
                SetAttributeValue("totaldiscountamount", value);
            }
        }

        /// <summary>
        /// Shows the Total Discount Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totaldiscountamount_base")]
        public Money TotalDiscountAmount_Base
        {
            get
            {
                return GetAttributeValue<Money>("totaldiscountamount_base");
            }
        }

        /// <summary>
        /// Shows the sum of all existing and write-in products included on the opportunity, based on the specified price list and quantities.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totallineitemamount")]
        public Money TotalLineItemAmount
        {
            get
            {
                return GetAttributeValue<Money>("totallineitemamount");
            }
            set
            {
                SetAttributeValue("totallineitemamount", value);
            }
        }

        /// <summary>
        /// Shows the Total Detail Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totallineitemamount_base")]
        public Money TotalLineItemAmount_Base
        {
            get
            {
                return GetAttributeValue<Money>("totallineitemamount_base");
            }
        }

        /// <summary>
        /// Shows the total of the Manual Discount amounts specified on all products included in the opportunity. This value is reflected in the Total Detail Amount field on the opportunity and is added to any discount amount or rate specified on the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totallineitemdiscountamount")]
        public Money TotalLineItemDiscountAmount
        {
            get
            {
                return GetAttributeValue<Money>("totallineitemdiscountamount");
            }
            set
            {
                SetAttributeValue("totallineitemdiscountamount", value);
            }
        }

        /// <summary>
        /// Shows the Total Line Item Discount Amount field to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totallineitemdiscountamount_base")]
        public Money TotalLineItemDiscountAmount_Base
        {
            get
            {
                return GetAttributeValue<Money>("totallineitemdiscountamount_base");
            }
        }

        /// <summary>
        /// Shows the total of the Tax amounts specified on all products included in the opportunity, included in the Total Amount field calculation for the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("totaltax")]
        public Money TotalTax
        {
            get
            {
                return GetAttributeValue<Money>("totaltax");
            }
            set
            {
                SetAttributeValue("totaltax", value);
            }
        }

        /// <summary>
        /// Shows the Total Tax field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
        /// </summary>
        [AttributeLogicalName("totaltax_base")]
        public Money TotalTax_Base
        {
            get
            {
                return GetAttributeValue<Money>("totaltax_base");
            }
        }

        /// <summary>
        /// Choose the local currency for the record to make sure budgets are reported in the correct currency.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("transactioncurrencyid")]
        public EntityReference TransactionCurrencyId
        {
            get
            {
                return GetAttributeValue<EntityReference>("transactioncurrencyid");
            }
            set
            {
                SetAttributeValue("transactioncurrencyid", value);
            }
        }

        /// <summary>
        /// For internal use only.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("traversedpath")]
        public string TraversedPath
        {
            get
            {
                return GetAttributeValue<string>("traversedpath");
            }
            set
            {
                SetAttributeValue("traversedpath", value);
            }
        }

        /// <summary>
        /// Time zone code that was in use when the record was created.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("utcconversiontimezonecode")]
        public int? UTCConversionTimeZoneCode
        {
            get
            {
                return GetAttributeValue<int?>("utcconversiontimezonecode");
            }
            set
            {
                SetAttributeValue("utcconversiontimezonecode", value);
            }
        }

        /// <summary>
        /// Version number of the opportunity.
        /// </summary>
        [DataMember]
        [AttributeLogicalName("versionnumber")]
        public long? VersionNumber
        {
            get
            {
                return GetAttributeValue<long?>("versionnumber");
            }
        }
    }

    [DataContract()]
    public enum OpportunityState
    {

        [EnumMember()]
        Open = 0,

        [EnumMember()]
        Won = 1,

        [EnumMember()]
        Lost = 2,
    }
}