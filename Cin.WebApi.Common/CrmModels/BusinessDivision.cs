﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels;


[DataContract]
[EntityLogicalName("po_businessdivision")]
public class BusinessDivision : Entity
{
    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// An unique numeric ID which can be used to identify a Business Division.
    /// </summary>
    [AttributeLogicalName("ctas_business_division_id")]
    [DataMember]
    public int? ctas_Business_Division_ID
    {
        get
        {
            return GetAttributeValue<int?>("ctas_business_division_id");
        }
        set
        {
            SetAttributeValue("ctas_business_division_id", value);
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Owner Id
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the business unit that owns the record
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier for the team that owns the record.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    [DataMember]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier for the user that owns the record.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_abbreviation")]
    [DataMember]
    public string po_Abbreviation
    {
        get
        {
            return GetAttributeValue<string>("po_abbreviation");
        }
        set
        {
            SetAttributeValue("po_abbreviation", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_agreementexpireindays")]
    [DataMember]
    public int? po_AgreementExpireinDays
    {
        get
        {
            return GetAttributeValue<int?>("po_agreementexpireindays");
        }
        set
        {
            SetAttributeValue("po_agreementexpireindays", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_backgroundcolor")]
    [DataMember]
    public OptionSetValue po_BackgroundColor
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_backgroundcolor");
        }
        set
        {
            SetAttributeValue("po_backgroundcolor", value);
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [AttributeLogicalName("po_businessdivision")]
    [DataMember]
    public string po_businessdivision1
    {
        get
        {
            return GetAttributeValue<string>("po_businessdivision");
        }
        set
        {
            SetAttributeValue("po_businessdivision", value);
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [AttributeLogicalName("po_businessdivisionid")]
    [DataMember]
    public Guid? po_businessdivisionId
    {
        get
        {
            return GetAttributeValue<Guid?>("po_businessdivisionid");
        }
        set
        {
            SetAttributeValue("po_businessdivisionid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("po_businessdivisionid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            po_businessdivisionId = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_emblemadvantagebookprice")]
    public float? po_EmblemAdvantageBookPrice
    {
        get
        {
            return GetAttributeValue<float>("po_emblemadvantagebookprice");
        }
        set
        {
            SetAttributeValue("po_emblemadvantagebookprice", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_makeupadvantagebookprice")]
    public float? po_MakeupAdvantageBookPrice
    {
        get
        {
            return GetAttributeValue<float>("po_makeupadvantagebookprice");
        }
        set
        {
            SetAttributeValue("po_makeupadvantagebookprice", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_parentbusinessdivision")]
    [DataMember]
    public EntityReference po_ParentBusinessDivision
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_parentbusinessdivision");
        }
        set
        {
            SetAttributeValue("po_parentbusinessdivision", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_premiumuniformadvantagebookprice")]
    public float? po_PremiumUniformAdvantageBookPrice
    {
        get
        {
            return GetAttributeValue<float>("po_premiumuniformadvantagebookprice");
        }
        set
        {
            SetAttributeValue("po_premiumuniformadvantagebookprice", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_proposalexpireindays")]
    [DataMember]
    public int? po_ProposalExpireinDays
    {
        get
        {
            return GetAttributeValue<int?>("po_proposalexpireindays");
        }
        set
        {
            SetAttributeValue("po_proposalexpireindays", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_proposallostindays")]
    [DataMember]
    public int? po_ProposalLostinDays
    {
        get
        {
            return GetAttributeValue<int?>("po_proposallostindays");
        }
        set
        {
            SetAttributeValue("po_proposallostindays", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_servicechargebookprice")]
    public float? po_ServiceChargeBookPrice
    {
        get
        {
            return GetAttributeValue<float>("po_servicechargebookprice");
        }
        set
        {
            SetAttributeValue("po_servicechargebookprice", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_uniformadvantagebookprice")]
    public float? po_UniformAdvantageBookPrice
    {
        get
        {
            return GetAttributeValue<float>("po_uniformadvantagebookprice");
        }
        set
        {
            SetAttributeValue("po_uniformadvantagebookprice", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_threadcolor")]
    [DataMember]
    public OptionSetValue po_ThreadColor
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_threadcolor");
        }
        set
        {
            SetAttributeValue("po_threadcolor", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_trimtype")]
    [DataMember]
    public OptionSetValue po_TrimType
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_trimtype");
        }
        set
        {
            SetAttributeValue("po_trimtype", value);
        }
    }

    /// <summary>
    /// Status of the Business Division
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public po_businessdivisionState? statecode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (po_businessdivisionState)Enum.ToObject(typeof(po_businessdivisionState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the Business Division
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue statuscode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}

[DataContract]
public enum po_businessdivisionState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}