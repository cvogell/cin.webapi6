﻿using Microsoft.Xrm.Sdk;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk.Client;

namespace Cin.WebApi.CrmModels;


[DataContract]
[EntityLogicalName("po_nacprocessing")]
public class NacProcessing : Entity
{

    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// ABI number from the account entity
    /// </summary>
    [AttributeLogicalName("ctas_abi_account")]
    [DataMember]
    public string ctas_ABI_Account
    {
        get
        {
            return GetAttributeValue<string>("ctas_abi_account");
        }
        set
        {
            SetAttributeValue("ctas_abi_account", value);
        }
    }

    /// <summary>
    /// Account Keyed
    /// </summary>
    [AttributeLogicalName("ctas_acct_keyed")]
    [DataMember]
    public bool? ctas_Acct_Keyed
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_acct_keyed");
        }
        set
        {
            SetAttributeValue("ctas_acct_keyed", value);
        }
    }

    /// <summary>
    /// Account Keyed
    /// </summary>
    [AttributeLogicalName("ctas_acct_keyed_2")]
    [DataMember]
    public OptionSetValue ctas_Acct_Keyed_2
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_acct_keyed_2");
        }
        set
        {
            SetAttributeValue("ctas_acct_keyed_2", value);
        }
    }

    /// <summary>
    /// Account Submitted
    /// </summary>
    [AttributeLogicalName("ctas_acct_submitted")]
    [DataMember]
    public bool? ctas_Acct_Submitted
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_acct_submitted");
        }
        set
        {
            SetAttributeValue("ctas_acct_submitted", value);
        }
    }

    /// <summary>
    /// Account Submitted
    /// </summary>
    [AttributeLogicalName("ctas_acct_submitted_2")]
    [DataMember]
    public OptionSetValue ctas_Acct_Submitted_2
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_acct_submitted_2");
        }
        set
        {
            SetAttributeValue("ctas_acct_submitted_2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_agreement_date")]
    [DataMember]
    public DateTime? ctas_Agreement_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_agreement_date");
        }
        set
        {
            SetAttributeValue("ctas_agreement_date", value);
        }
    }

    /// <summary>
    /// The date the information was added to Falcon / SAP
    /// </summary>
    [AttributeLogicalName("ctas_date_added_to_system")]
    [DataMember]
    public DateTime? ctas_Date_Added_To_System
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_date_added_to_system");
        }
        set
        {
            SetAttributeValue("ctas_date_added_to_system", value);
        }
    }

    /// <summary>
    /// The day the product is delivered.
    /// </summary>
    [AttributeLogicalName("ctas_day")]
    [DataMember]
    public int? ctas_Day
    {
        get
        {
            return GetAttributeValue<int?>("ctas_day");
        }
        set
        {
            SetAttributeValue("ctas_day", value);
        }
    }

    /// <summary>
    /// Dynamics Number
    /// </summary>
    [AttributeLogicalName("ctas_dynamics_number")]
    [DataMember]
    public string ctas_Dynamics_Number
    {
        get
        {
            return GetAttributeValue<string>("ctas_dynamics_number");
        }
        set
        {
            SetAttributeValue("ctas_dynamics_number", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_nac_name_lu")]
    [DataMember]
    public EntityReference ctas_NAC_Name_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_nac_name_lu");
        }
        set
        {
            SetAttributeValue("ctas_nac_name_lu", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_nac_notes")]
    [DataMember]
    public string ctas_NAC_Notes
    {
        get
        {
            return GetAttributeValue<string>("ctas_nac_notes");
        }
        set
        {
            SetAttributeValue("ctas_nac_notes", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_nacname")]
    [DataMember]
    public string ctas_NACName
    {
        get
        {
            return GetAttributeValue<string>("ctas_nacname");
        }
        set
        {
            SetAttributeValue("ctas_nacname", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_partial_install")]
    [DataMember]
    public bool? ctas_Partial_Install
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_partial_install");
        }
        set
        {
            SetAttributeValue("ctas_partial_install", value);
        }
    }

    /// <summary>
    /// This will be populated if the rep is a SRIT and there is a rep with the same division covering the postal code.
    /// </summary>
    [AttributeLogicalName("ctas_primary_rep")]
    [DataMember]
    public EntityReference ctas_Primary_Rep
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_primary_rep");
        }
        set
        {
            SetAttributeValue("ctas_primary_rep", value);
        }
    }

    /// <summary>
    /// Route for account
    /// </summary>
    [AttributeLogicalName("ctas_route")]
    [DataMember]
    public int? ctas_Route
    {
        get
        {
            return GetAttributeValue<int?>("ctas_route");
        }
        set
        {
            SetAttributeValue("ctas_route", value);
        }
    }

    /// <summary>
    /// Designates in the rep who sold the account was a Sales Rep in Training.
    /// </summary>
    [AttributeLogicalName("ctas_srit")]
    [DataMember]
    public bool? ctas_SRIT
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_srit");
        }
        set
        {
            SetAttributeValue("ctas_srit", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_subject_sold_form")]
    [DataMember]
    public string ctas_Subject_Sold_Form
    {
        get
        {
            return GetAttributeValue<string>("ctas_subject_sold_form");
        }
        set
        {
            SetAttributeValue("ctas_subject_sold_form", value);
        }
    }

    /// <summary>
    /// Exchange rate for the currency associated with the entity with respect to the base currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Owner Id
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the business unit that owns the record
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier for the team that owns the record.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    [DataMember]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier for the user that owns the record.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_accountname_lkup_nac")]
    [DataMember]
    public EntityReference po_AccountName_LKUp_NAC
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_accountname_lkup_nac");
        }
        set
        {
            SetAttributeValue("po_accountname_lkup_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_address1_nac")]
    [DataMember]
    public string po_Address1_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_address1_nac");
        }
        set
        {
            SetAttributeValue("po_address1_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_address2_nac")]
    [DataMember]
    public string po_Address2_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_address2_nac");
        }
        set
        {
            SetAttributeValue("po_address2_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessdivision")]
    [DataMember]
    public EntityReference po_BusinessDivision
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessdivision");
        }
        set
        {
            SetAttributeValue("po_businessdivision", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessline_nac")]
    [DataMember]
    public OptionSetValue po_BusinessLine_NAC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_businessline_nac");
        }
        set
        {
            SetAttributeValue("po_businessline_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessstructure")]
    [DataMember]
    public EntityReference po_BusinessStructure
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessstructure");
        }
        set
        {
            SetAttributeValue("po_businessstructure", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_city_nac")]
    [DataMember]
    public string po_City_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_city_nac");
        }
        set
        {
            SetAttributeValue("po_city_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_customernumber_nac")]
    [DataMember]
    public string po_CustomerNumber_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_customernumber_nac");
        }
        set
        {
            SetAttributeValue("po_customernumber_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_day_nac")]
    [DataMember]
    public OptionSetValue po_Day_NAC
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_day_nac");
        }
        set
        {
            SetAttributeValue("po_day_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_ur_nac")]
    [DataMember]
    public Money po_EstimatedVolume_UR_NAC
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_ur_nac");
        }
        set
        {
            SetAttributeValue("po_estimatedvolume_ur_nac", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume UR in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedvolume_ur_nac_base")]
    [DataMember]
    public Money po_estimatedvolume_ur_nac_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedvolume_ur_nac_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_installdate_nac")]
    [DataMember]
    public DateTime? po_InstallDate_NAC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_installdate_nac");
        }
        set
        {
            SetAttributeValue("po_installdate_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_locationnumber")]
    [DataMember]
    public EntityReference po_LocationNumber
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_locationnumber");
        }
        set
        {
            SetAttributeValue("po_locationnumber", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_mark_as_sold")]
    [DataMember]
    public EntityReference po_Mark_As_Sold
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_mark_as_sold");
        }
        set
        {
            SetAttributeValue("po_mark_as_sold", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_mark_as_sold_lu")]
    [DataMember]
    public EntityReference po_Mark_as_Sold_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_mark_as_sold_lu");
        }
        set
        {
            SetAttributeValue("po_mark_as_sold_lu", value);
        }
    }

    /// <summary>
    /// Unique identifier for Account associated with NAC Processing.
    /// </summary>
    [AttributeLogicalName("po_nacaccountlookupid")]
    [DataMember]
    public EntityReference po_NACAccountLookupId
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_nacaccountlookupid");
        }
        set
        {
            SetAttributeValue("po_nacaccountlookupid", value);
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [AttributeLogicalName("po_nacprocessingid")]
    [DataMember]
    public Guid? po_nacprocessingId
    {
        get
        {
            return GetAttributeValue<Guid?>("po_nacprocessingid");
        }
        set
        {
            SetAttributeValue("po_nacprocessingid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("po_nacprocessingid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            po_nacprocessingId = value;
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [AttributeLogicalName("po_name")]
    [DataMember]
    public string po_name
    {
        get
        {
            return GetAttributeValue<string>("po_name");
        }
        set
        {
            SetAttributeValue("po_name", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_notes_nac")]
    [DataMember]
    public string po_Notes_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_notes_nac");
        }
        set
        {
            SetAttributeValue("po_notes_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_poweradd_nac")]
    [DataMember]
    public bool? po_PowerAdd_NAC
    {
        get
        {
            return GetAttributeValue<bool?>("po_poweradd_nac");
        }
        set
        {
            SetAttributeValue("po_poweradd_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_route_nac")]
    [DataMember]
    public string po_Route_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_route_nac");
        }
        set
        {
            SetAttributeValue("po_route_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_salesrep_nac")]
    [DataMember]
    public string po_SalesRep_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_salesrep_nac");
        }
        set
        {
            SetAttributeValue("po_salesrep_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_solddate_nac")]
    [DataMember]
    public DateTime? po_SoldDate_NAC
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_solddate_nac");
        }
        set
        {
            SetAttributeValue("po_solddate_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldvolume_nac")]
    [DataMember]
    public Money po_SoldVolume_NAC
    {
        get
        {
            return GetAttributeValue<Money>("po_soldvolume_nac");
        }
        set
        {
            SetAttributeValue("po_soldvolume_nac", value);
        }
    }

    /// <summary>
    /// Value of the Sold Volume in base currency.
    /// </summary>
    [AttributeLogicalName("po_soldvolume_nac_base")]
    [DataMember]
    public Money po_soldvolume_nac_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_soldvolume_nac_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_state_nac")]
    [DataMember]
    public string po_State_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_state_nac");
        }
        set
        {
            SetAttributeValue("po_state_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_week_nac")]
    [DataMember]
    public OptionSetValue po_Week_Nac
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_week_nac");
        }
        set
        {
            SetAttributeValue("po_week_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_weeklytotal_nac")]
    [DataMember]
    public Money po_WeeklyTotal_NAC
    {
        get
        {
            return GetAttributeValue<Money>("po_weeklytotal_nac");
        }
        set
        {
            SetAttributeValue("po_weeklytotal_nac", value);
        }
    }

    /// <summary>
    /// Value of the Weekly Total in base currency.
    /// </summary>
    [AttributeLogicalName("po_weeklytotal_nac_base")]
    [DataMember]
    public Money po_weeklytotal_nac_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_weeklytotal_nac_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_worvolume_nac")]
    [DataMember]
    public Money po_WORVolume_NAC
    {
        get
        {
            return GetAttributeValue<Money>("po_worvolume_nac");
        }
        set
        {
            SetAttributeValue("po_worvolume_nac", value);
        }
    }

    /// <summary>
    /// Value of the WOR Volume in base currency.
    /// </summary>
    [AttributeLogicalName("po_worvolume_nac_base")]
    [DataMember]
    public Money po_worvolume_nac_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_worvolume_nac_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_zippostalcode_nac")]
    [DataMember]
    public string po_ZipPostalCode_NAC
    {
        get
        {
            return GetAttributeValue<string>("po_zippostalcode_nac");
        }
        set
        {
            SetAttributeValue("po_zippostalcode_nac", value);
        }
    }

    /// <summary>
    /// Status of the NAC Processing
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public po_nacprocessingState? statecode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (po_nacprocessingState)Enum.ToObject(typeof(po_nacprocessingState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the NAC Processing
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue statuscode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the currency associated with the entity.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}

[DataContract]
public enum po_nacprocessingState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}
