﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("po_soldactivity")]
/// <summary>
/// Track Sold account activity.
/// </summary>
public class MarkAsSold : Entity
{
    /// <summary>
    /// Additional information provided by the external application as JSON. For internal use only.
    /// </summary>
    [AttributeLogicalName("activityadditionalparams")]
    [DataMember]
    public string ActivityAdditionalParams
    {
        get
        {
            return GetAttributeValue<string>("activityadditionalparams");
        }
        set
        {
            SetAttributeValue("activityadditionalparams", value);
        }
    }

    /// <summary>
    /// Unique identifier of the activity.
    /// </summary>
    [AttributeLogicalName("activityid")]
    [DataMember]
    public Guid? ActivityId
    {
        get
        {
            return GetAttributeValue<Guid?>("activityid");
        }
        set
        {
            SetAttributeValue("activityid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("activityid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            ActivityId = value;
        }
    }

    /// <summary>
    /// Type of activity.
    /// </summary>
    [AttributeLogicalName("activitytypecode")]
    [DataMember]
    public string ActivityTypeCode
    {
        get
        {
            return GetAttributeValue<string>("activitytypecode");
        }
    }

    /// <summary>
    /// Actual duration of the activity in minutes.
    /// </summary>
    [AttributeLogicalName("actualdurationminutes")]
    [DataMember]
    public int? ActualDurationMinutes
    {
        get
        {
            return GetAttributeValue<int?>("actualdurationminutes");
        }
        set
        {
            SetAttributeValue("actualdurationminutes", value);
        }
    }

    /// <summary>
    /// Actual end time of the activity.
    /// </summary>
    [AttributeLogicalName("actualend")]
    [DataMember]
    public DateTime? ActualEnd
    {
        get
        {
            return GetAttributeValue<DateTime?>("actualend");
        }
        set
        {
            SetAttributeValue("actualend", value);
        }
    }

    /// <summary>
    /// Actual start time of the activity.
    /// </summary>
    [AttributeLogicalName("actualstart")]
    [DataMember]
    public DateTime? ActualStart
    {
        get
        {
            return GetAttributeValue<DateTime?>("actualstart");
        }
        set
        {
            SetAttributeValue("actualstart", value);
        }
    }

    ///// <summary>
    ///// Blind Carbon-copy (bcc) recipients of the activity.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("bcc")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> Bcc
    //{
    //    get
    //    {
    //        Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("bcc");
    //        if (((collection != null)
    //                    && (collection.Entities != null)))
    //        {
    //            return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //        }
    //        else
    //        {
    //            return null;
    //        }
    //    }
    //    set
    //    {
    //        if ((value == null))
    //        {
    //            this.SetAttributeValue("bcc", value);
    //        }
    //        else
    //        {
    //            this.SetAttributeValue("bcc", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //        }
    //    }
    //}

    ///// <summary>
    ///// Carbon-copy (cc) recipients of the activity.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("cc")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> Cc
    //{
    //    get
    //    {
    //        Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("cc");
    //        if (((collection != null)
    //                    && (collection.Entities != null)))
    //        {
    //            return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //        }
    //        else
    //        {
    //            return null;
    //        }
    //    }
    //    set
    //    {
    //        if ((value == null))
    //        {
    //            this.SetAttributeValue("cc", value);
    //        }
    //        else
    //        {
    //            this.SetAttributeValue("cc", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //        }
    //    }
    //}

    /// <summary>
    /// Shows how contact about the social activity originated, such as from Twitter or Facebook. This field is read-only.
    /// </summary>
    [AttributeLogicalName("community")]
    [DataMember]
    public OptionSetValue Community
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("community");
        }
        set
        {
            SetAttributeValue("community", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who created the activity.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the activity was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the activitypointer.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_13weekwindowprocessed")]
    [DataMember]
    public bool? ctas_13WeekWindowProcessed
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_13weekwindowprocessed");
        }
        set
        {
            SetAttributeValue("ctas_13weekwindowprocessed", value);
        }
    }

    /// <summary>
    /// ABI Number
    /// </summary>
    [AttributeLogicalName("ctas_abi_number")]
    [DataMember]
    public string ctas_ABI_Number
    {
        get
        {
            return GetAttributeValue<string>("ctas_abi_number");
        }
        set
        {
            SetAttributeValue("ctas_abi_number", value);
        }
    }

    /// <summary>
    /// AED Purchase
    /// </summary>
    [AttributeLogicalName("ctas_aed_purchase")]
    [DataMember]
    public Money ctas_AED_Purchase
    {
        get
        {
            return GetAttributeValue<Money>("ctas_aed_purchase");
        }
        set
        {
            SetAttributeValue("ctas_aed_purchase", value);
        }
    }

    /// <summary>
    /// Value of the AED Purchase in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_aed_purchase_base")]
    [DataMember]
    public Money ctas_aed_purchase_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_aed_purchase_base");
        }
    }

    /// <summary>
    /// AED Sales
    /// </summary>
    [AttributeLogicalName("ctas_aed_sales")]
    [DataMember]
    public Money ctas_AED_Sales
    {
        get
        {
            return GetAttributeValue<Money>("ctas_aed_sales");
        }
        set
        {
            SetAttributeValue("ctas_aed_sales", value);
        }
    }

    /// <summary>
    /// Value of the AED in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_aed_sales_base")]
    [DataMember]
    public Money ctas_aed_sales_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_aed_sales_base");
        }
    }

    /// <summary>
    /// AED Service
    /// </summary>
    [AttributeLogicalName("ctas_aed_service")]
    [DataMember]
    public Money ctas_AED_Service
    {
        get
        {
            return GetAttributeValue<Money>("ctas_aed_service");
        }
        set
        {
            SetAttributeValue("ctas_aed_service", value);
        }
    }

    /// <summary>
    /// Value of the AED Service in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_aed_service_base")]
    [DataMember]
    public Money ctas_aed_service_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_aed_service_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_agreement_date")]
    [DataMember]
    public DateTime? ctas_Agreement_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_agreement_date");
        }
        set
        {
            SetAttributeValue("ctas_agreement_date", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_amended_date")]
    [DataMember]
    public DateTime? ctas_Amended_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_amended_date");
        }
        set
        {
            SetAttributeValue("ctas_amended_date", value);
        }
    }

    /// <summary>
    /// Lookup to the Cintas.com Leads Entity
    /// </summary>
    [AttributeLogicalName("ctas_cintascom_lu")]
    [DataMember]
    public EntityReference ctas_Cintascom_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_cintascom_lu");
        }
        set
        {
            SetAttributeValue("ctas_cintascom_lu", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_closed_record_check")]
    [DataMember]
    public bool? ctas_Closed_Record_Check
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_closed_record_check");
        }
        set
        {
            SetAttributeValue("ctas_closed_record_check", value);
        }
    }

    /// <summary>
    /// The day the product will be delivered.
    /// </summary>
    [AttributeLogicalName("ctas_day")]
    [DataMember]
    public int? ctas_Day
    {
        get
        {
            return GetAttributeValue<int?>("ctas_day");
        }
        set
        {
            SetAttributeValue("ctas_day", value);
        }
    }

    /// <summary>
    /// Dynamics Number auto populated by the system
    /// </summary>
    [AttributeLogicalName("ctas_dynamics_number")]
    [DataMember]
    public string ctas_Dynamics_Number
    {
        get
        {
            return GetAttributeValue<string>("ctas_dynamics_number");
        }
        set
        {
            SetAttributeValue("ctas_dynamics_number", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_estimated_sold_date")]
    [DataMember]
    public DateTime? ctas_Estimated_Sold_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_estimated_sold_date");
        }
        set
        {
            SetAttributeValue("ctas_estimated_sold_date", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_estimated_volume")]
    [DataMember]
    public Money ctas_Estimated_Volume
    {
        get
        {
            return GetAttributeValue<Money>("ctas_estimated_volume");
        }
        set
        {
            SetAttributeValue("ctas_estimated_volume", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Volume in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_estimated_volume_base")]
    [DataMember]
    public Money ctas_estimated_volume_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_estimated_volume_base");
        }
    }

    /// <summary>
    /// Eyewash
    /// </summary>
    [AttributeLogicalName("ctas_eyewash")]
    [DataMember]
    public Money ctas_Eyewash
    {
        get
        {
            return GetAttributeValue<Money>("ctas_eyewash");
        }
        set
        {
            SetAttributeValue("ctas_eyewash", value);
        }
    }

    /// <summary>
    /// Value of the Eyewash in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_eyewash_base")]
    [DataMember]
    public Money ctas_eyewash_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_eyewash_base");
        }
    }

    /// <summary>
    /// Dollars attributive to First Aid.
    /// </summary>
    [AttributeLogicalName("ctas_first_aid")]
    [DataMember]
    public Money ctas_First_Aid
    {
        get
        {
            return GetAttributeValue<Money>("ctas_first_aid");
        }
        set
        {
            SetAttributeValue("ctas_first_aid", value);
        }
    }

    /// <summary>
    /// Value of the First Aid in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_first_aid_base")]
    [DataMember]
    public Money ctas_first_aid_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_first_aid_base");
        }
    }

    /// <summary>
    /// Lookup to History FIeld Entity
    /// </summary>
    [AttributeLogicalName("ctas_history_field_lu")]
    [DataMember]
    public EntityReference ctas_History_Field_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_history_field_lu");
        }
        set
        {
            SetAttributeValue("ctas_history_field_lu", value);
        }
    }

    /// <summary>
    /// Lookup field to lookup value table. This will be a list of item numbers.
    /// </summary>
    [AttributeLogicalName("ctas_item_lookup")]
    [DataMember]
    public EntityReference ctas_Item_Lookup
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_item_lookup");
        }
        set
        {
            SetAttributeValue("ctas_item_lookup", value);
        }
    }

    /// <summary>
    /// Lead Source for FAS
    /// </summary>
    [AttributeLogicalName("ctas_lead_source_fas")]
    [DataMember]
    public OptionSetValue ctas_Lead_Source_FAS
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_lead_source_fas");
        }
        set
        {
            SetAttributeValue("ctas_lead_source_fas", value);
        }
    }

    /// <summary>
    /// MLA
    /// </summary>
    [AttributeLogicalName("ctas_mla")]
    [DataMember]
    public bool? ctas_MLA
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_mla");
        }
        set
        {
            SetAttributeValue("ctas_mla", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_nac_name")]
    [DataMember]
    public string ctas_NAC_Name
    {
        get
        {
            return GetAttributeValue<string>("ctas_nac_name");
        }
        set
        {
            SetAttributeValue("ctas_nac_name", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_nac_name_lu")]
    [DataMember]
    public EntityReference ctas_NAC_Name_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_nac_name_lu");
        }
        set
        {
            SetAttributeValue("ctas_nac_name_lu", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_national_account")]
    [DataMember]
    public bool? ctas_National_Account
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_national_account");
        }
        set
        {
            SetAttributeValue("ctas_national_account", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_national_account_lookup")]
    [DataMember]
    public EntityReference ctas_National_Account_Lookup
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_national_account_lookup");
        }
        set
        {
            SetAttributeValue("ctas_national_account_lookup", value);
        }
    }

    /// <summary>
    /// # of Cabinets
    /// </summary>
    [AttributeLogicalName("ctas_num_cabinets")]
    [DataMember]
    public int? ctas_Num_Cabinets
    {
        get
        {
            return GetAttributeValue<int?>("ctas_num_cabinets");
        }
        set
        {
            SetAttributeValue("ctas_num_cabinets", value);
        }
    }

    /// <summary>
    /// Other
    /// </summary>
    [AttributeLogicalName("ctas_other")]
    [DataMember]
    public Money ctas_Other
    {
        get
        {
            return GetAttributeValue<Money>("ctas_other");
        }
        set
        {
            SetAttributeValue("ctas_other", value);
        }
    }

    /// <summary>
    /// Value of the Other in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_other_base")]
    [DataMember]
    public Money ctas_other_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_other_base");
        }
    }

    /// <summary>
    /// Lookup to the Postal Code Table. This will be used to bring the primary rep over when a SRIT makes the sale.
    /// </summary>
    [AttributeLogicalName("ctas_postal_code_lu")]
    [DataMember]
    public EntityReference ctas_Postal_Code_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_postal_code_lu");
        }
        set
        {
            SetAttributeValue("ctas_postal_code_lu", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_power_add")]
    [DataMember]
    public bool? ctas_Power_Add
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_power_add");
        }
        set
        {
            SetAttributeValue("ctas_power_add", value);
        }
    }

    /// <summary>
    /// If the rep who owns the record is a SRIT then this will be populated with the Sales Rep who the SRIT is under
    /// </summary>
    [AttributeLogicalName("ctas_primary_rep")]
    [DataMember]
    public EntityReference ctas_Primary_Rep
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_primary_rep");
        }
        set
        {
            SetAttributeValue("ctas_primary_rep", value);
        }
    }

    /// <summary>
    /// This will be used to identify activities which are related to referred leads from the Cintas.com Lead entity.
    /// </summary>
    [AttributeLogicalName("ctas_ref_lead_user_eq_owner")]
    [DataMember]
    public bool? ctas_Ref_Lead_User_eq_Owner
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_ref_lead_user_eq_owner");
        }
        set
        {
            SetAttributeValue("ctas_ref_lead_user_eq_owner", value);
        }
    }

    /// <summary>
    /// Current Route number
    /// </summary>
    [AttributeLogicalName("ctas_route")]
    [DataMember]
    public int? ctas_Route
    {
        get
        {
            return GetAttributeValue<int?>("ctas_route");
        }
        set
        {
            SetAttributeValue("ctas_route", value);
        }
    }

    /// <summary>
    /// Safety
    /// </summary>
    [AttributeLogicalName("ctas_safety")]
    [DataMember]
    public Money ctas_Safety
    {
        get
        {
            return GetAttributeValue<Money>("ctas_safety");
        }
        set
        {
            SetAttributeValue("ctas_safety", value);
        }
    }

    /// <summary>
    /// Value of the Safety in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_safety_base")]
    [DataMember]
    public Money ctas_safety_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_safety_base");
        }
    }

    /// <summary>
    /// Safety Center
    /// </summary>
    [AttributeLogicalName("ctas_safety_center")]
    [DataMember]
    public Money ctas_Safety_Center
    {
        get
        {
            return GetAttributeValue<Money>("ctas_safety_center");
        }
        set
        {
            SetAttributeValue("ctas_safety_center", value);
        }
    }

    /// <summary>
    /// Value of the Safety Center in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_safety_center_base")]
    [DataMember]
    public Money ctas_safety_center_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_safety_center_base");
        }
    }

    /// <summary>
    /// Safety Center SA
    /// </summary>
    [AttributeLogicalName("ctas_safety_center_sa")]
    [DataMember]
    public Money ctas_Safety_Center_SA
    {
        get
        {
            return GetAttributeValue<Money>("ctas_safety_center_sa");
        }
        set
        {
            SetAttributeValue("ctas_safety_center_sa", value);
        }
    }

    /// <summary>
    /// Value of the Safety Center SA in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_safety_center_sa_base")]
    [DataMember]
    public Money ctas_safety_center_sa_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_safety_center_sa_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_srit")]
    [DataMember]
    public bool? ctas_SRIT
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_srit");
        }
        set
        {
            SetAttributeValue("ctas_srit", value);
        }
    }

    /// <summary>
    /// Tar Account Number
    /// </summary>
    [AttributeLogicalName("ctas_tar_acct_num")]
    [DataMember]
    public string ctas_Tar_Acct_Num
    {
        get
        {
            return GetAttributeValue<string>("ctas_tar_acct_num");
        }
        set
        {
            SetAttributeValue("ctas_tar_acct_num", value);
        }
    }

    /// <summary>
    /// TAR Amount
    /// </summary>
    [AttributeLogicalName("ctas_tar_amount")]
    [DataMember]
    public Money ctas_TAR_Amount
    {
        get
        {
            return GetAttributeValue<Money>("ctas_tar_amount");
        }
        set
        {
            SetAttributeValue("ctas_tar_amount", value);
        }
    }

    /// <summary>
    /// Value of the Amount in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_tar_amount_base")]
    [DataMember]
    public Money ctas_tar_amount_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_tar_amount_base");
        }
    }

    /// <summary>
    /// Date for Tar's
    /// </summary>
    [AttributeLogicalName("ctas_tar_date")]
    [DataMember]
    public DateTime? ctas_Tar_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_tar_date");
        }
        set
        {
            SetAttributeValue("ctas_tar_date", value);
        }
    }

    /// <summary>
    /// Tar Order Number
    /// </summary>
    [AttributeLogicalName("ctas_tar_order_num")]
    [DataMember]
    public string ctas_Tar_Order_Num
    {
        get
        {
            return GetAttributeValue<string>("ctas_tar_order_num");
        }
        set
        {
            SetAttributeValue("ctas_tar_order_num", value);
        }
    }

    /// <summary>
    /// TAR Shipping
    /// </summary>
    [AttributeLogicalName("ctas_tar_shipping")]
    [DataMember]
    public Money ctas_Tar_Shipping
    {
        get
        {
            return GetAttributeValue<Money>("ctas_tar_shipping");
        }
        set
        {
            SetAttributeValue("ctas_tar_shipping", value);
        }
    }

    /// <summary>
    /// Value of the Shipping in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_tar_shipping_base")]
    [DataMember]
    public Money ctas_tar_shipping_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_tar_shipping_base");
        }
    }

    /// <summary>
    /// TAR Vendor
    /// </summary>
    [AttributeLogicalName("ctas_tar_vendor")]
    [DataMember]
    public EntityReference ctas_Tar_Vendor
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_tar_vendor");
        }
        set
        {
            SetAttributeValue("ctas_tar_vendor", value);
        }
    }

    /// <summary>
    /// TAR Vendor Order #
    /// </summary>
    [AttributeLogicalName("ctas_tar_vendor_order")]
    [DataMember]
    public string ctas_TAR_Vendor_Order
    {
        get
        {
            return GetAttributeValue<string>("ctas_tar_vendor_order");
        }
        set
        {
            SetAttributeValue("ctas_tar_vendor_order", value);
        }
    }

    /// <summary>
    /// TAR Vendor Tracking Number
    /// </summary>
    [AttributeLogicalName("ctas_tar_vendor_tracking")]
    [DataMember]
    public string ctas_Tar_Vendor_Tracking
    {
        get
        {
            return GetAttributeValue<string>("ctas_tar_vendor_tracking");
        }
        set
        {
            SetAttributeValue("ctas_tar_vendor_tracking", value);
        }
    }

    /// <summary>
    /// Dollars attributed to training in FAS
    /// </summary>
    [AttributeLogicalName("ctas_training_fas")]
    [DataMember]
    public Money ctas_Training_FAS
    {
        get
        {
            return GetAttributeValue<Money>("ctas_training_fas");
        }
        set
        {
            SetAttributeValue("ctas_training_fas", value);
        }
    }

    /// <summary>
    /// Value of the Training in base currency.
    /// </summary>
    [AttributeLogicalName("ctas_training_fas_base")]
    [DataMember]
    public Money ctas_training_fas_Base
    {
        get
        {
            return GetAttributeValue<Money>("ctas_training_fas_base");
        }
    }

    /// <summary>
    /// This will identify sales to be removed from the overall FAS Sales.
    /// </summary>
    [AttributeLogicalName("ctas_win_60_sales")]
    [DataMember]
    public bool? ctas_Win_60_Sales
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_win_60_sales");
        }
        set
        {
            SetAttributeValue("ctas_win_60_sales", value);
        }
    }

    ///// <summary>
    ///// Customer with which the activity is associated.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("customers")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> Customers
    //{
    //    get
    //    {
    //        Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("customers");
    //        if (((collection != null)
    //                    && (collection.Entities != null)))
    //        {
    //            return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //        }
    //        else
    //        {
    //            return null;
    //        }
    //    }
    //    set
    //    {
    //        if ((value == null))
    //        {
    //            this.SetAttributeValue("customers", value);
    //        }
    //        else
    //        {
    //            this.SetAttributeValue("customers", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //        }
    //    }
    //}

    /// <summary>
    /// Date and time when the delivery of the activity was last attempted.
    /// </summary>
    [AttributeLogicalName("deliverylastattemptedon")]
    [DataMember]
    public DateTime? DeliveryLastAttemptedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("deliverylastattemptedon");
        }
    }

    /// <summary>
    /// Priority of delivery of the activity to the email server.
    /// </summary>
    [AttributeLogicalName("deliveryprioritycode")]
    [DataMember]
    public OptionSetValue DeliveryPriorityCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("deliveryprioritycode");
        }
        set
        {
            SetAttributeValue("deliveryprioritycode", value);
        }
    }

    /// <summary>
    /// Description of the activity.
    /// </summary>
    [AttributeLogicalName("description")]
    [DataMember]
    public string Description
    {
        get
        {
            return GetAttributeValue<string>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /// <summary>
    /// The message id of activity which is returned from Exchange Server.
    /// </summary>
    [AttributeLogicalName("exchangeitemid")]
    [DataMember]
    public string ExchangeItemId
    {
        get
        {
            return GetAttributeValue<string>("exchangeitemid");
        }
        set
        {
            SetAttributeValue("exchangeitemid", value);
        }
    }

    /// <summary>
    /// Exchange rate for the currency associated with the activitypointer with respect to the base currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Shows the web link of Activity of type email.
    /// </summary>
    [AttributeLogicalName("exchangeweblink")]
    [DataMember]
    public string ExchangeWebLink
    {
        get
        {
            return GetAttributeValue<string>("exchangeweblink");
        }
        set
        {
            SetAttributeValue("exchangeweblink", value);
        }
    }

    ///// <summary>
    ///// Person who the activity is from.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("from")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> From
    //{
    //	get
    //	{
    //		Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("from");
    //		if (((collection != null)
    //					&& (collection.Entities != null)))
    //		{
    //			return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //		}
    //		else
    //		{
    //			return null;
    //		}
    //	}
    //	set
    //	{
    //		if ((value == null))
    //		{
    //			this.SetAttributeValue("from", value);
    //		}
    //		else
    //		{
    //			this.SetAttributeValue("from", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //		}
    //	}
    //}

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Type of instance of a recurring series.
    /// </summary>
    [AttributeLogicalName("instancetypecode")]
    [DataMember]
    public OptionSetValue InstanceTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("instancetypecode");
        }
    }

    /// <summary>
    /// Information regarding whether the activity was billed as part of resolving a case.
    /// </summary>
    [AttributeLogicalName("isbilled")]
    [DataMember]
    public bool? IsBilled
    {
        get
        {
            return GetAttributeValue<bool?>("isbilled");
        }
        set
        {
            SetAttributeValue("isbilled", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("ismapiprivate")]
    [DataMember]
    public bool? IsMapiPrivate
    {
        get
        {
            return GetAttributeValue<bool?>("ismapiprivate");
        }
        set
        {
            SetAttributeValue("ismapiprivate", value);
        }
    }

    /// <summary>
    /// Information regarding whether the activity is a regular activity type or event type.
    /// </summary>
    [AttributeLogicalName("isregularactivity")]
    [DataMember]
    public bool? IsRegularActivity
    {
        get
        {
            return GetAttributeValue<bool?>("isregularactivity");
        }
    }

    /// <summary>
    /// Information regarding whether the activity was created from a workflow rule.
    /// </summary>
    [AttributeLogicalName("isworkflowcreated")]
    [DataMember]
    public bool? IsWorkflowCreated
    {
        get
        {
            return GetAttributeValue<bool?>("isworkflowcreated");
        }
        set
        {
            SetAttributeValue("isworkflowcreated", value);
        }
    }

    /// <summary>
    /// Contains the date and time stamp of the last on hold time.
    /// </summary>
    [AttributeLogicalName("lastonholdtime")]
    [DataMember]
    public DateTime? LastOnHoldTime
    {
        get
        {
            return GetAttributeValue<DateTime?>("lastonholdtime");
        }
        set
        {
            SetAttributeValue("lastonholdtime", value);
        }
    }

    /// <summary>
    /// Left the voice mail
    /// </summary>
    [AttributeLogicalName("leftvoicemail")]
    [DataMember]
    public bool? LeftVoiceMail
    {
        get
        {
            return GetAttributeValue<bool?>("leftvoicemail");
        }
        set
        {
            SetAttributeValue("leftvoicemail", value);
        }
    }

    /// <summary>
    /// Unique identifier of user who last modified the activity.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when activity was last modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who last modified the activitypointer.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("new_test")]
    [DataMember]
    public string new_test
    {
        get
        {
            return GetAttributeValue<string>("new_test");
        }
        set
        {
            SetAttributeValue("new_test", value);
        }
    }

    /// <summary>
    /// Shows how long, in minutes, that the record was on hold.
    /// </summary>
    [AttributeLogicalName("onholdtime")]
    [DataMember]
    public int? OnHoldTime
    {
        get
        {
            return GetAttributeValue<int?>("onholdtime");
        }
    }

    ///// <summary>
    ///// List of optional attendees for the activity.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("optionalattendees")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> OptionalAttendees
    //{
    //	get
    //	{
    //		Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("optionalattendees");
    //		if (((collection != null)
    //					&& (collection.Entities != null)))
    //		{
    //			return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //		}
    //		else
    //		{
    //			return null;
    //		}
    //	}
    //	set
    //	{
    //		if ((value == null))
    //		{
    //			this.SetAttributeValue("optionalattendees", value);
    //		}
    //		else
    //		{
    //			this.SetAttributeValue("optionalattendees", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //		}
    //	}
    //}

    ///// <summary>
    ///// Person who organized the activity.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("organizer")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> Organizer
    //{
    //	get
    //	{
    //		Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("organizer");
    //		if (((collection != null)
    //					&& (collection.Entities != null)))
    //		{
    //			return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //		}
    //		else
    //		{
    //			return null;
    //		}
    //	}
    //	set
    //	{
    //		if ((value == null))
    //		{
    //			this.SetAttributeValue("organizer", value);
    //		}
    //		else
    //		{
    //			this.SetAttributeValue("organizer", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //		}
    //	}
    //}

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user or team who owns the activity.
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the business unit that owns the activity.
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier of the team that owns the activity.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    [DataMember]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier of the user that owns the activity.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    ///// <summary>
    ///// Outsource vendor with which activity is associated.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("partners")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> Partners
    //{
    //	get
    //	{
    //		Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("partners");
    //		if (((collection != null)
    //					&& (collection.Entities != null)))
    //		{
    //			return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //		}
    //		else
    //		{
    //			return null;
    //		}
    //	}
    //	set
    //	{
    //		if ((value == null))
    //		{
    //			this.SetAttributeValue("partners", value);
    //		}
    //		else
    //		{
    //			this.SetAttributeValue("partners", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //		}
    //	}
    //}

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_accountsold")]
    [DataMember]
    public bool? po_AccountSold
    {
        get
        {
            return GetAttributeValue<bool?>("po_accountsold");
        }
        set
        {
            SetAttributeValue("po_accountsold", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_actual_sold_date")]
    [DataMember]
    public DateTime? po_Actual_Sold_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_actual_sold_date");
        }
        set
        {
            SetAttributeValue("po_actual_sold_date", value);
        }
    }

    /// <summary>
    /// Sold Volume comes from NAC
    /// </summary>
    [AttributeLogicalName("po_actual_sold_volume")]
    [DataMember]
    public Money po_Actual_Sold_Volume
    {
        get
        {
            return GetAttributeValue<Money>("po_actual_sold_volume");
        }
        set
        {
            SetAttributeValue("po_actual_sold_volume", value);
        }
    }

    /// <summary>
    /// Value of the Actual Sold Volume in base currency.
    /// </summary>
    [AttributeLogicalName("po_actual_sold_volume_base")]
    [DataMember]
    public Money po_actual_sold_volume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_actual_sold_volume_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessdivison")]
    [DataMember]
    public EntityReference po_BusinessDivison
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessdivison");
        }
        set
        {
            SetAttributeValue("po_businessdivison", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessstructure")]
    [DataMember]
    public EntityReference po_BusinessStructure
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessstructure");
        }
        set
        {
            SetAttributeValue("po_businessstructure", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_estimatedsoldvolume")]
    [DataMember]
    public Money po_EstimatedSoldVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedsoldvolume");
        }
        set
        {
            SetAttributeValue("po_estimatedsoldvolume", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Sold Volume in base currency.
    /// </summary>
    [AttributeLogicalName("po_estimatedsoldvolume_base")]
    [DataMember]
    public Money po_estimatedsoldvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedsoldvolume_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_formid_soldacct")]
    [DataMember]
    public string po_FormID_soldacct
    {
        get
        {
            return GetAttributeValue<string>("po_formid_soldacct");
        }
        set
        {
            SetAttributeValue("po_formid_soldacct", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_locationnumber")]
    [DataMember]
    public EntityReference po_LocationNumber
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_locationnumber");
        }
        set
        {
            SetAttributeValue("po_locationnumber", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_markcomplete")]
    [DataMember]
    public bool? po_MarkComplete
    {
        get
        {
            return GetAttributeValue<bool?>("po_markcomplete");
        }
        set
        {
            SetAttributeValue("po_markcomplete", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_nac")]
    [DataMember]
    public EntityReference po_NAC
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_nac");
        }
        set
        {
            SetAttributeValue("po_nac", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_parentalbusinessdivision")]
    [DataMember]
    public EntityReference po_ParentalBusinessDivision
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_parentalbusinessdivision");
        }
        set
        {
            SetAttributeValue("po_parentalbusinessdivision", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_sold_acct_type")]
    [DataMember]
    public OptionSetValue po_Sold_Acct_Type
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("po_sold_acct_type");
        }
        set
        {
            SetAttributeValue("po_sold_acct_type", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_solddate")]
    [DataMember]
    public DateTime? po_SoldDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_solddate");
        }
        set
        {
            SetAttributeValue("po_solddate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_soldvolume")]
    [DataMember]
    public Money po_SoldVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_soldvolume");
        }
        set
        {
            SetAttributeValue("po_soldvolume", value);
        }
    }

    /// <summary>
    /// Value of the Sold Volume in base currency.
    /// </summary>
    [AttributeLogicalName("po_soldvolume_base")]
    [DataMember]
    public Money po_soldvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_soldvolume_base");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("postponeactivityprocessinguntil")]
    [DataMember]
    public DateTime? PostponeActivityProcessingUntil
    {
        get
        {
            return GetAttributeValue<DateTime?>("postponeactivityprocessinguntil");
        }
    }

    /// <summary>
    /// Priority of the activity.
    /// </summary>
    [AttributeLogicalName("prioritycode")]
    [DataMember]
    public OptionSetValue PriorityCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("prioritycode");
        }
        set
        {
            SetAttributeValue("prioritycode", value);
        }
    }

    /// <summary>
    /// Unique identifier of the Process.
    /// </summary>
    [AttributeLogicalName("processid")]
    [DataMember]
    public Guid? ProcessId
    {
        get
        {
            return GetAttributeValue<Guid?>("processid");
        }
        set
        {
            SetAttributeValue("processid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the object with which the activity is associated.
    /// </summary>
    [AttributeLogicalName("regardingobjectid")]
    [DataMember]
    public EntityReference RegardingObjectId
    {
        get
        {
            return GetAttributeValue<EntityReference>("regardingobjectid");
        }
        set
        {
            SetAttributeValue("regardingobjectid", value);
        }
    }

    ///// <summary>
    ///// List of required attendees for the activity.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("requiredattendees")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> RequiredAttendees
    //{
    //	get
    //	{
    //		Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("requiredattendees");
    //		if (((collection != null)
    //					&& (collection.Entities != null)))
    //		{
    //			return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //		}
    //		else
    //		{
    //			return null;
    //		}
    //	}
    //	set
    //	{
    //		if ((value == null))
    //		{
    //			this.SetAttributeValue("requiredattendees", value);
    //		}
    //		else
    //		{
    //			this.SetAttributeValue("requiredattendees", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //		}
    //	}
    //}

    ///// <summary>
    ///// Users or facility/equipment that are required for the activity.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("resources")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> Resources
    //{
    //	get
    //	{
    //		Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("resources");
    //		if (((collection != null)
    //					&& (collection.Entities != null)))
    //		{
    //			return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //		}
    //		else
    //		{
    //			return null;
    //		}
    //	}
    //	set
    //	{
    //		if ((value == null))
    //		{
    //			this.SetAttributeValue("resources", value);
    //		}
    //		else
    //		{
    //			this.SetAttributeValue("resources", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //		}
    //	}
    //}

    /// <summary>
    /// Scheduled duration of the activity, specified in minutes.
    /// </summary>
    [AttributeLogicalName("scheduleddurationminutes")]
    [DataMember]
    public int? ScheduledDurationMinutes
    {
        get
        {
            return GetAttributeValue<int?>("scheduleddurationminutes");
        }
        set
        {
            SetAttributeValue("scheduleddurationminutes", value);
        }
    }

    /// <summary>
    /// Scheduled end time of the activity.
    /// </summary>
    [AttributeLogicalName("scheduledend")]
    [DataMember]
    public DateTime? ScheduledEnd
    {
        get
        {
            return GetAttributeValue<DateTime?>("scheduledend");
        }
        set
        {
            SetAttributeValue("scheduledend", value);
        }
    }

    /// <summary>
    /// Scheduled start time of the activity.
    /// </summary>
    [AttributeLogicalName("scheduledstart")]
    [DataMember]
    public DateTime? ScheduledStart
    {
        get
        {
            return GetAttributeValue<DateTime?>("scheduledstart");
        }
        set
        {
            SetAttributeValue("scheduledstart", value);
        }
    }

    /// <summary>
    /// Unique identifier of the mailbox associated with the sender of the email message.
    /// </summary>
    [AttributeLogicalName("sendermailboxid")]
    [DataMember]
    public EntityReference SenderMailboxId
    {
        get
        {
            return GetAttributeValue<EntityReference>("sendermailboxid");
        }
    }

    /// <summary>
    /// Date and time when the activity was sent.
    /// </summary>
    [AttributeLogicalName("senton")]
    [DataMember]
    public DateTime? SentOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("senton");
        }
    }

    /// <summary>
    /// Uniqueidentifier specifying the id of recurring series of an instance.
    /// </summary>
    [AttributeLogicalName("seriesid")]
    [DataMember]
    public Guid? SeriesId
    {
        get
        {
            return GetAttributeValue<Guid?>("seriesid");
        }
    }

    /// <summary>
    /// Unique identifier of an associated service.
    /// </summary>
    [AttributeLogicalName("serviceid")]
    [DataMember]
    public EntityReference ServiceId
    {
        get
        {
            return GetAttributeValue<EntityReference>("serviceid");
        }
        set
        {
            SetAttributeValue("serviceid", value);
        }
    }

    /// <summary>
    /// Choose the service level agreement (SLA) that you want to apply to the case record.
    /// </summary>
    [AttributeLogicalName("slaid")]
    [DataMember]
    public EntityReference SLAId
    {
        get
        {
            return GetAttributeValue<EntityReference>("slaid");
        }
        set
        {
            SetAttributeValue("slaid", value);
        }
    }

    /// <summary>
    /// Last SLA that was applied to this case. This field is for internal use only.
    /// </summary>
    [AttributeLogicalName("slainvokedid")]
    [DataMember]
    public EntityReference SLAInvokedId
    {
        get
        {
            return GetAttributeValue<EntityReference>("slainvokedid");
        }
    }

    /// <summary>
    /// Shows the date and time by which the activities are sorted.
    /// </summary>
    [AttributeLogicalName("sortdate")]
    [DataMember]
    public DateTime? SortDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("sortdate");
        }
        set
        {
            SetAttributeValue("sortdate", value);
        }
    }

    /// <summary>
    /// Unique identifier of the Stage.
    /// </summary>
    [AttributeLogicalName("stageid")]
    [DataMember]
    public Guid? StageId
    {
        get
        {
            return GetAttributeValue<Guid?>("stageid");
        }
        set
        {
            SetAttributeValue("stageid", value);
        }
    }

    ///// <summary>
    ///// Status of the activity.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statecode")]
    //[DataMember]
    //public System.Nullable<CintasDynamics.po_soldactivityState> StateCode
    //{
    //	get
    //	{
    //		Microsoft.Xrm.Sdk.OptionSetValue optionSet = this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statecode");
    //		if ((optionSet != null))
    //		{
    //			return ((CintasDynamics.po_soldactivityState)(System.Enum.ToObject(typeof(CintasDynamics.po_soldactivityState), optionSet.Value)));
    //		}
    //		else
    //		{
    //			return null;
    //		}
    //	}
    //	set
    //	{
    //		if ((value == null))
    //		{
    //			this.SetAttributeValue("statecode", null);
    //		}
    //		else
    //		{
    //			this.SetAttributeValue("statecode", new Microsoft.Xrm.Sdk.OptionSetValue(((int)(value))));
    //		}
    //	}
    //}

    /// <summary>
    /// Reason for the status of the activity.
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// Subject associated with the activity.
    /// </summary>
    [AttributeLogicalName("subject")]
    [DataMember]
    public string Subject
    {
        get
        {
            return GetAttributeValue<string>("subject");
        }
        set
        {
            SetAttributeValue("subject", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    ///// <summary>
    ///// Person who is the receiver of the activity.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("to")]
    //[DataMember]
    //public System.Collections.Generic.IEnumerable<CintasDynamics.ActivityParty> To
    //{
    //	get
    //	{
    //		Microsoft.Xrm.Sdk.EntityCollection collection = this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityCollection>("to");
    //		if (((collection != null)
    //					&& (collection.Entities != null)))
    //		{
    //			return System.Linq.Enumerable.Cast<CintasDynamics.ActivityParty>(collection.Entities);
    //		}
    //		else
    //		{
    //			return null;
    //		}
    //	}
    //	set
    //	{
    //		if ((value == null))
    //		{
    //			this.SetAttributeValue("to", value);
    //		}
    //		else
    //		{
    //			this.SetAttributeValue("to", new Microsoft.Xrm.Sdk.EntityCollection(new System.Collections.Generic.List<Microsoft.Xrm.Sdk.Entity>(value)));
    //		}
    //	}
    //}

    /// <summary>
    /// Unique identifier of the currency associated with the activitypointer.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// A comma separated list of string values representing the unique identifiers of stages in a Business Process Flow Instance in the order that they occur.
    /// </summary>
    [AttributeLogicalName("traversedpath")]
    [DataMember]
    public string TraversedPath
    {
        get
        {
            return GetAttributeValue<string>("traversedpath");
        }
        set
        {
            SetAttributeValue("traversedpath", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version number of the activity.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}