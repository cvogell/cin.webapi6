﻿using Microsoft.Xrm.Sdk;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract()]
[Microsoft.Xrm.Sdk.Client.EntityLogicalName("productassociation")]
public class ProductAssociation : Entity
{
    /// <summary>
    /// Select a product to add to the bundle or kit.
    /// </summary>
    [AttributeLogicalName("associatedproduct")]
    [DataMember]
    public EntityReference AssociatedProduct
    {
        get
        {
            return GetAttributeValue<EntityReference>("associatedproduct");
        }
        set
        {
            SetAttributeValue("associatedproduct", value);
        }
    }

    /// <summary>
    /// Shows who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Internal Use Only
    /// </summary>
    [AttributeLogicalName("dmtimportstate")]
    [DataMember]
    public int? DMTImportState
    {
        get
        {
            return GetAttributeValue<int?>("dmtimportstate");
        }
        set
        {
            SetAttributeValue("dmtimportstate", value);
        }
    }

    /// <summary>
    /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Shows the data import or data migration that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Shows who last updated the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was last updated.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Shows the organization associated with the record.
    /// </summary>
    [AttributeLogicalName("organizationid")]
    [DataMember]
    public EntityReference OrganizationId
    {
        get
        {
            return GetAttributeValue<EntityReference>("organizationid");
        }
    }

    /// <summary>
    /// Shows the date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_group")]
    [DataMember]
    public EntityReference po_Group
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_group");
        }
        set
        {
            SetAttributeValue("po_group", value);
        }
    }

    /// <summary>
    /// Shows the unique identifier of the product association.
    /// </summary>
    [AttributeLogicalName("productassociationid")]
    [DataMember]
    public Guid? ProductAssociationId
    {
        get
        {
            return GetAttributeValue<Guid?>("productassociationid");
        }
        set
        {
            SetAttributeValue("productassociationid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("productassociationid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            ProductAssociationId = value;
        }
    }

    /// <summary>
    /// Select a bundle or a kit.
    /// </summary>
    [AttributeLogicalName("productid")]
    [DataMember]
    public EntityReference ProductId
    {
        get
        {
            return GetAttributeValue<EntityReference>("productid");
        }
        set
        {
            SetAttributeValue("productid", value);
        }
    }

    /// <summary>
    /// Select whether the associated product is required or optional.
    /// </summary>
    [AttributeLogicalName("productisrequired")]
    [DataMember]
    public OptionSetValue ProductIsRequired
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("productisrequired");
        }
        set
        {
            SetAttributeValue("productisrequired", value);
        }
    }

    /// <summary>
    /// Shows whether the item has properties that can be customized.
    /// </summary>
    [AttributeLogicalName("propertycustomizationstatus")]
    [DataMember]
    public OptionSetValue PropertyCustomizationStatus
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("propertycustomizationstatus");
        }
        set
        {
            SetAttributeValue("propertycustomizationstatus", value);
        }
    }

    /// <summary>
    /// Type the quantity of the products added to the bundle or kit.
    /// </summary>
    [AttributeLogicalName("quantity")]
    [DataMember]
    public decimal? Quantity
    {
        get
        {
            return GetAttributeValue<decimal?>("quantity");
        }
        set
        {
            SetAttributeValue("quantity", value);
        }
    }

    ///// <summary>
    ///// Shows whether the associated product is active or inactive.
    ///// </summary>
    //[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statecode")]
    //[DataMember]
    //public System.Nullable<CintasDynamics.ProductAssociationState> statecode
    //{
    //    get
    //    {
    //        Microsoft.Xrm.Sdk.OptionSetValue optionSet = this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statecode");
    //        if ((optionSet != null))
    //        {
    //            return ((CintasDynamics.ProductAssociationState)(System.Enum.ToObject(typeof(CintasDynamics.ProductAssociationState), optionSet.Value)));
    //        }
    //        else
    //        {
    //            return null;
    //        }
    //    }
    //    set
    //    {
    //        if ((value == null))
    //        {
    //            this.SetAttributeValue("statecode", null);
    //        }
    //        else
    //        {
    //            this.SetAttributeValue("statecode", new Microsoft.Xrm.Sdk.OptionSetValue(((int)(value))));
    //        }
    //    }
    //}

    /// <summary>
    /// Select the associated product's status.
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue statuscode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// Shows the currency associated with the record.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// Shows the unit of the product association.
    /// </summary>
    [AttributeLogicalName("uomid")]
    [DataMember]
    public EntityReference UoMId
    {
        get
        {
            return GetAttributeValue<EntityReference>("uomid");
        }
        set
        {
            SetAttributeValue("uomid", value);
        }
    }

    /// <summary>
    /// Shows the version number of the product association.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }

}