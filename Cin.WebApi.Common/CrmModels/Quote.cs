﻿using Microsoft.Xrm.Sdk;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract()]
[Microsoft.Xrm.Sdk.Client.EntityLogicalName("quote")]
public class Quote : Entity
{

    /// <summary>
    /// Unique identifier of the account with which the quote is associated.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("accountid")]
    public EntityReference AccountId
    {
        get
        {
            return GetAttributeValue<EntityReference>("accountid");
        }
        set
        {
            SetAttributeValue("accountid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_addressid")]
    public Guid? BillTo_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("billto_addressid");
        }
        set
        {
            SetAttributeValue("billto_addressid", value);
        }
    }

    /// <summary>
    /// Type the city for the customer's billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_city")]
    public string BillTo_City
    {
        get
        {
            return GetAttributeValue<string>("billto_city");
        }
        set
        {
            SetAttributeValue("billto_city", value);

        }
    }

    /// <summary>
    /// Shows the complete Bill To address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_composite")]
    public string BillTo_Composite
    {
        get
        {
            return GetAttributeValue<string>("billto_composite");
        }
    }

    /// <summary>
    /// Type the primary contact name at the customer's billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_contactname")]
    public string BillTo_ContactName
    {
        get
        {
            return GetAttributeValue<string>("billto_contactname");
        }
        set
        {
            SetAttributeValue("billto_contactname", value);
        }
    }

    /// <summary>
    /// Type the country or region for the customer's billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_country")]
    public string BillTo_Country
    {
        get
        {
            return GetAttributeValue<string>("billto_country");
        }
        set
        {
            SetAttributeValue("billto_country", value);
        }
    }

    /// <summary>
    /// Type the fax number for the customer's billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_fax")]
    public string BillTo_Fax
    {
        get
        {
            return GetAttributeValue<string>("billto_fax");
        }
        set
        {
            SetAttributeValue("billto_fax", value);
        }
    }

    /// <summary>
    /// Type the first line of the customer's billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_line1")]
    public string BillTo_Line1
    {
        get
        {
            return GetAttributeValue<string>("billto_line1");
        }
        set
        {
            SetAttributeValue("billto_line1", value);
        }
    }

    /// <summary>
    /// Type the second line of the customer's billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_line2")]
    public string BillTo_Line2
    {
        get
        {
            return GetAttributeValue<string>("billto_line2");
        }
        set
        {
            SetAttributeValue("billto_line2", value);
        }
    }

    /// <summary>
    /// Type the third line of the billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_line3")]
    public string BillTo_Line3
    {
        get
        {
            return GetAttributeValue<string>("billto_line3");
        }
        set
        {
            SetAttributeValue("billto_line3", value);
        }
    }

    /// <summary>
    /// Type a name for the customer's billing address, such as "Headquarters" or "Field office", to identify the address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_name")]
    public string BillTo_Name
    {
        get
        {
            return GetAttributeValue<string>("billto_name");
        }
        set
        {
            SetAttributeValue("billto_name", value);
        }
    }

    /// <summary>
    /// Type the ZIP Code or postal code for the billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_postalcode")]
    public string BillTo_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("billto_postalcode");
        }
        set
        {
            SetAttributeValue("billto_postalcode", value);
        }
    }

    /// <summary>
    /// Type the state or province for the billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_stateorprovince")]
    public string BillTo_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("billto_stateorprovince");
        }
        set
        {
            SetAttributeValue("billto_stateorprovince", value);
        }
    }

    /// <summary>
    /// Type the phone number for the customer's billing address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("billto_telephone")]
    public string BillTo_Telephone
    {
        get
        {
            return GetAttributeValue<string>("billto_telephone");
        }
        set
        {
            SetAttributeValue("billto_telephone", value);
        }
    }

    /// <summary>
    /// Shows the campaign that the order was created from.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("campaignid")]
    public EntityReference CampaignId
    {
        get
        {
            return GetAttributeValue<EntityReference>("campaignid");
        }
        set
        {
            SetAttributeValue("campaignid", value);
        }
    }

    /// <summary>
    /// Enter the date when the quote was closed to indicate the expiration, revision, or cancellation date.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("closedon")]
    public DateTime? ClosedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("closedon");
        }
        set
        {
            SetAttributeValue("closedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the contact associated with the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("contactid")]
    public EntityReference ContactId
    {
        get
        {
            return GetAttributeValue<EntityReference>("contactid");
        }
    }

    /// <summary>
    /// Shows who created the record.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("createdby")]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("createdon")]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("createdonbehalfby")]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Select the customer account or contact to provide a quick link to additional customer details, such as account information, activities, and opportunities.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("customerid")]
    public EntityReference CustomerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("customerid");
        }
        set
        {
            SetAttributeValue("customerid", value);
        }
    }

    /// <summary>
    /// Type additional information to describe the quote, such as the products or services offered or details about the customer's product preferences.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("description")]
    public string Description
    {
        get
        {
            return GetAttributeValue<string>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /// <summary>
    /// Type the discount amount for the quote if the customer is eligible for special savings.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("discountamount")]
    public Money DiscountAmount
    {
        get
        {
            return GetAttributeValue<Money>("discountamount");
        }
        set
        {
            SetAttributeValue("discountamount", value);
        }
    }

    /// <summary>
    /// Shows the Discount Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("discountamount_base")]
    public Money DiscountAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("discountamount_base");
        }
    }

    /// <summary>
    /// Type the discount rate that should be applied to the Detail Amount field to include additional savings for the customer in the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("discountpercentage")]
    public decimal? DiscountPercentage
    {
        get
        {
            return GetAttributeValue<decimal?>("discountpercentage");
        }
        set
        {
            SetAttributeValue("discountpercentage", value);
        }
    }

    /// <summary>
    /// Enter the date when the quote pricing is effective or was first communicated to the customer.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("effectivefrom")]
    public DateTime? EffectiveFrom
    {
        get
        {
            return GetAttributeValue<DateTime?>("effectivefrom");
        }
        set
        {
            SetAttributeValue("effectivefrom", value);
        }
    }

    /// <summary>
    /// Enter the expiration date or last day the quote pricing is effective for the customer.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("effectiveto")]
    public DateTime? EffectiveTo
    {
        get
        {
            return GetAttributeValue<DateTime?>("effectiveto");
        }
        set
        {
            SetAttributeValue("effectiveto", value);
        }
    }

    /// <summary>
    /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("exchangerate")]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Enter the date a decision or order is due from the customer to indicate the expiration date of the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("expireson")]
    public DateTime? ExpiresOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("expireson");
        }
        set
        {
            SetAttributeValue("expireson", value);
        }
    }

    /// <summary>
    /// Type the cost of freight or shipping for the products included in the quote for use in calculating the Total Amount field.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("freightamount")]
    public Money FreightAmount
    {
        get
        {
            return GetAttributeValue<Money>("freightamount");
        }
        set
        {
            SetAttributeValue("freightamount", value);
        }
    }

    /// <summary>
    /// Shows the Freight Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("freightamount_base")]
    public Money FreightAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("freightamount_base");
        }
    }

    /// <summary>
    /// Select the freight terms to make sure shipping charges are processed correctly.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("freighttermscode")]
    public OptionSetValue FreightTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("freighttermscode");
        }
        set
        {
            SetAttributeValue("freighttermscode", value);
        }
    }

    /// <summary>
    /// Unique identifier of the data import or data migration that created this record.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("importsequencenumber")]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Contains the date time stamp of the last on hold time.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("lastonholdtime")]
    public DateTime? LastOnHoldTime
    {
        get
        {
            return GetAttributeValue<DateTime?>("lastonholdtime");
        }
        set
        {
            SetAttributeValue("lastonholdtime", value);
        }
    }

    /// <summary>
    /// Shows who last updated the record.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("modifiedby")]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was last updated. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("modifiedon")]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Shows who last updated the record on behalf of another user.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("modifiedonbehalfby")]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Type a descriptive name for the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("name")]
    public string Name
    {
        get
        {
            return GetAttributeValue<string>("name");
        }
        set
        {
            SetAttributeValue("name", value);
        }
    }

    /// <summary>
    /// Shows the duration in minutes for which the quote was on hold.
    /// </summary>
    [AttributeLogicalName("onholdtime")]
    public int? OnHoldTime
    {
        get
        {
            return GetAttributeValue<int?>("onholdtime");
        }
    }

    /// <summary>
    /// Choose the opportunity that the quote is related to for reporting and analytics.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("opportunityid")]
    public EntityReference OpportunityId
    {
        get
        {
            return GetAttributeValue<EntityReference>("opportunityid");
        }
        set
        {
            SetAttributeValue("opportunityid", value);
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("overriddencreatedon")]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Enter the user or team who is assigned to manage the record. This field is updated every time the record is assigned to a different user.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("ownerid")]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the business unit that owns the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("owningbusinessunit")]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier of the team who owns the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("owningteam")]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier of the user who owns the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("owninguser")]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// Select the payment terms to indicate when the customer needs to pay the total amount.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("paymenttermscode")]
    public OptionSetValue PaymentTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("paymenttermscode");
        }
        set
        {
            SetAttributeValue("paymenttermscode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_13weekcalcfield")]
    public DateTime? po_13WeekCalcField
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_13weekcalcfield");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_actualvolume")]
    public Money po_ActualVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_actualvolume");
        }
        set
        {
            SetAttributeValue("po_actualvolume", value);
        }
    }

    /// <summary>
    /// Value of the Actual Volume in base currency.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_actualvolume_base")]
    public Money po_actualvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_actualvolume_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_businessdivison")]
    public EntityReference po_BusinessDivison
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessdivison");
        }
        set
        {
            SetAttributeValue("po_businessdivison", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_estimatedweeklyvolume")]
    public Money po_EstimatedWeeklyVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedweeklyvolume");
        }
        set
        {
            SetAttributeValue("po_estimatedweeklyvolume", value);
        }
    }

    /// <summary>
    /// Value of the Estimated Weekly Volume in base currency.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_estimatedweeklyvolume_base")]
    public Money po_estimatedweeklyvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_estimatedweeklyvolume_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_markedassoldweeklyvolume")]
    public Money po_MarkedasSoldWeeklyVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_markedassoldweeklyvolume");
        }
        set
        {
            SetAttributeValue("po_markedassoldweeklyvolume", value);
        }
    }

    /// <summary>
    /// Value of the Marked as Sold Weekly Volume in base currency.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_markedassoldweeklyvolume_base")]
    public Money po_markedassoldweeklyvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_markedassoldweeklyvolume_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_orderedkeyedweeklyvolume")]
    public Money po_OrderedKeyedWeeklyVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_orderedkeyedweeklyvolume");
        }
        set
        {
            SetAttributeValue("po_orderedkeyedweeklyvolume", value);
        }
    }

    /// <summary>
    /// Value of the Ordered/Keyed Weekly Volume in base currency.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_orderedkeyedweeklyvolume_base")]
    public Money po_orderedkeyedweeklyvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_orderedkeyedweeklyvolume_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_wornbinstalledvolume")]
    public Money po_WORNBInstalledVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_wornbinstalledvolume");
        }
        set
        {
            SetAttributeValue("po_wornbinstalledvolume", value);
        }
    }

    /// <summary>
    /// Value of the WOR NB Installed Volume in base currency.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_wornbinstalledvolume_base")]
    public Money po_wornbinstalledvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_wornbinstalledvolume_base");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_wornewbusinessvolume")]
    public Money po_WORNewBusinessVolume
    {
        get
        {
            return GetAttributeValue<Money>("po_wornewbusinessvolume");
        }
        set
        {
            SetAttributeValue("po_wornewbusinessvolume", value);
        }
    }

    /// <summary>
    /// Value of the WOR New Business Volume in base currency.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("po_wornewbusinessvolume_base")]
    public Money po_wornewbusinessvolume_Base
    {
        get
        {
            return GetAttributeValue<Money>("po_wornewbusinessvolume_base");
        }
    }

    /// <summary>
    /// Choose the price list associated with this record to make sure the products associated with the campaign are offered at the correct prices.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("pricelevelid")]
    public EntityReference PriceLevelId
    {
        get
        {
            return GetAttributeValue<EntityReference>("pricelevelid");
        }
        set
        {
            SetAttributeValue("pricelevelid", value);
        }
    }

    /// <summary>
    /// Pricing error for the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("pricingerrorcode")]
    public OptionSetValue PricingErrorCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("pricingerrorcode");
        }
        set
        {
            SetAttributeValue("pricingerrorcode", value);
        }
    }

    /// <summary>
    /// Shows the ID of the process.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("processid")]
    public Guid? ProcessId
    {
        get
        {
            return GetAttributeValue<Guid?>("processid");
        }
        set
        {
            SetAttributeValue("processid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("quoteid")]
    public Guid? QuoteId
    {
        get
        {
            return GetAttributeValue<Guid?>("quoteid");
        }
        set
        {
            SetAttributeValue("quoteid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [DataMember]
    [AttributeLogicalName("quoteid")]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            QuoteId = value;
        }
    }

    /// <summary>
    /// Shows the quote number for customer reference and searching capabilities. The number cannot be modified.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("quotenumber")]
    public string QuoteNumber
    {
        get
        {
            return GetAttributeValue<string>("quotenumber");
        }
        set
        {
            SetAttributeValue("quotenumber", value);
        }
    }

    /// <summary>
    /// Enter the delivery date requested by the customer for all products in the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("requestdeliveryby")]
    public DateTime? RequestDeliveryBy
    {
        get
        {
            return GetAttributeValue<DateTime?>("requestdeliveryby");
        }
        set
        {
            SetAttributeValue("requestdeliveryby", value);
        }
    }

    /// <summary>
    /// Shows the version number of the quote for revision history tracking.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("revisionnumber")]
    public int? RevisionNumber
    {
        get
        {
            return GetAttributeValue<int?>("revisionnumber");
        }
    }

    /// <summary>
    /// Select a shipping method for deliveries sent to this address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shippingmethodcode")]
    public OptionSetValue ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("shippingmethodcode");
        }
        set
        {
            SetAttributeValue("shippingmethodcode", value);
        }
    }

    /// <summary>
    /// Unique identifier of the shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_addressid")]
    public Guid? ShipTo_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("shipto_addressid");
        }
        set
        {
            SetAttributeValue("shipto_addressid", value);
        }
    }

    /// <summary>
    /// Type the city for the customer's shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_city")]
    public string ShipTo_City
    {
        get
        {
            return GetAttributeValue<string>("shipto_city");
        }
        set
        {
            SetAttributeValue("shipto_city", value);
        }
    }

    /// <summary>
    /// Shows the complete Ship To address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_composite")]
    public string ShipTo_Composite
    {
        get
        {
            return GetAttributeValue<string>("shipto_composite");
        }
    }

    /// <summary>
    /// Type the primary contact name at the customer's shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_contactname")]
    public string ShipTo_ContactName
    {
        get
        {
            return GetAttributeValue<string>("shipto_contactname");
        }
        set
        {
            SetAttributeValue("shipto_contactname", value);
        }
    }

    /// <summary>
    /// Type the country or region for the customer's shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_country")]
    public string ShipTo_Country
    {
        get
        {
            return GetAttributeValue<string>("shipto_country");
        }
        set
        {
            SetAttributeValue("shipto_country", value);
        }
    }

    /// <summary>
    /// Type the fax number for the customer's shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_fax")]
    public string ShipTo_Fax
    {
        get
        {
            return GetAttributeValue<string>("shipto_fax");
        }
        set
        {
            SetAttributeValue("shipto_fax", value);
        }
    }

    /// <summary>
    /// Select the freight terms to make sure shipping orders are processed correctly.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_freighttermscode")]
    public OptionSetValue ShipTo_FreightTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("shipto_freighttermscode");
        }
        set
        {
            SetAttributeValue("shipto_freighttermscode", value);
        }
    }

    /// <summary>
    /// Type the first line of the customer's shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_line1")]
    public string ShipTo_Line1
    {
        get
        {
            return GetAttributeValue<string>("shipto_line1");
        }
        set
        {
            SetAttributeValue("shipto_line1", value);
        }
    }

    /// <summary>
    /// Type the second line of the customer's shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_line2")]
    public string ShipTo_Line2
    {
        get
        {
            return GetAttributeValue<string>("shipto_line2");
        }
        set
        {
            SetAttributeValue("shipto_line2", value);
        }
    }

    /// <summary>
    /// Type the third line of the shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_line3")]
    public string ShipTo_Line3
    {
        get
        {
            return GetAttributeValue<string>("shipto_line3");
        }
        set
        {
            SetAttributeValue("shipto_line3", value);
        }
    }

    /// <summary>
    /// Type a name for the customer's shipping address, such as "Headquarters" or "Field office", to identify the address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_name")]
    public string ShipTo_Name
    {
        get
        {
            return GetAttributeValue<string>("shipto_name");
        }
        set
        {
            SetAttributeValue("shipto_name", value);
        }
    }

    /// <summary>
    /// Type the ZIP Code or postal code for the shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_postalcode")]
    public string ShipTo_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("shipto_postalcode");
        }
        set
        {
            SetAttributeValue("shipto_postalcode", value);
        }
    }

    /// <summary>
    /// Type the state or province for the shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_stateorprovince")]
    public string ShipTo_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("shipto_stateorprovince");
        }
        set
        {
            SetAttributeValue("shipto_stateorprovince", value);
        }
    }

    /// <summary>
    /// Type the phone number for the customer's shipping address.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("shipto_telephone")]
    public string ShipTo_Telephone
    {
        get
        {
            return GetAttributeValue<string>("shipto_telephone");
        }
        set
        {
            SetAttributeValue("shipto_telephone", value);
        }
    }

    /// <summary>
    /// Choose the service level agreement (SLA) that you want to apply to the quote record.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("slaid")]
    public EntityReference SLAId
    {
        get
        {
            return GetAttributeValue<EntityReference>("slaid");
        }
        set
        {
            SetAttributeValue("slaid", value);
        }
    }

    /// <summary>
    /// Last SLA that was applied to this quote. This field is for internal use only.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("slainvokedid")]
    public EntityReference SLAInvokedId
    {
        get
        {
            return GetAttributeValue<EntityReference>("slainvokedid");
        }
    }

    /// <summary>
    /// Shows the ID of the stage.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("stageid")]
    public Guid? StageId
    {
        get
        {
            return GetAttributeValue<Guid?>("stageid");
        }
        set
        {
            SetAttributeValue("stageid", value);
        }
    }


    /// <summary>
    /// Shows whether the quote is draft, active, won, or closed. Only draft quotes can be edited.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("statecode")]
    public QuoteState? StateCode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (QuoteState)Enum.ToObject(typeof(QuoteState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {

            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }

        }
    }

    /// <summary>
    /// Select the quote's status.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("statuscode")]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("timezoneruleversionnumber")]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Shows the total amount due, calculated as the sum of the products, discounts, freight, and taxes for the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totalamount")]
    public Money TotalAmount
    {
        get
        {
            return GetAttributeValue<Money>("totalamount");
        }
        set
        {
            SetAttributeValue("totalamount", value);
        }
    }

    /// <summary>
    /// Shows the Total Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totalamount_base")]
    public Money TotalAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("totalamount_base");
        }
    }

    /// <summary>
    /// Shows the total product amount for the quote, minus any discounts. This value is added to freight and tax amounts in the calculation for the total amount due for the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totalamountlessfreight")]
    public Money TotalAmountLessFreight
    {
        get
        {
            return GetAttributeValue<Money>("totalamountlessfreight");
        }
        set
        {
            SetAttributeValue("totalamountlessfreight", value);
        }
    }

    /// <summary>
    /// Shows the Pre-Freight Amount converted field to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totalamountlessfreight_base")]
    public Money TotalAmountLessFreight_Base
    {
        get
        {
            return GetAttributeValue<Money>("totalamountlessfreight_base");
        }
    }

    /// <summary>
    /// Shows the total discount amount, based on the discount price and rate entered on the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totaldiscountamount")]
    public Money TotalDiscountAmount
    {
        get
        {
            return GetAttributeValue<Money>("totaldiscountamount");
        }
        set
        {
            SetAttributeValue("totaldiscountamount", value);
        }
    }

    /// <summary>
    /// Shows the Total Discount Amount converted field to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totaldiscountamount_base")]
    public Money TotalDiscountAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("totaldiscountamount_base");
        }
    }

    /// <summary>
    /// Shows the sum of all existing and write-in products included on the quote, based on the specified price list and quantities.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totallineitemamount")]
    public Money TotalLineItemAmount
    {
        get
        {
            return GetAttributeValue<Money>("totallineitemamount");
        }
        set
        {
            SetAttributeValue("totallineitemamount", value);
        }
    }

    /// <summary>
    /// Shows the Detail Amount field converted to the system's default base currency. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totallineitemamount_base")]
    public Money TotalLineItemAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("totallineitemamount_base");
        }
    }

    /// <summary>
    /// Shows the total of the Manual Discount amounts specified on all products included in the quote. This value is reflected in the Detail Amount field on the quote and is added to any discount amount or rate specified on the quote
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totallineitemdiscountamount")]
    public Money TotalLineItemDiscountAmount
    {
        get
        {
            return GetAttributeValue<Money>("totallineitemdiscountamount");
        }
        set
        {
            SetAttributeValue("totallineitemdiscountamount", value);
        }
    }

    /// <summary>
    /// Shows the Total Line Item Discount Amount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totallineitemdiscountamount_base")]
    public Money TotalLineItemDiscountAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("totallineitemdiscountamount_base");
        }
    }

    /// <summary>
    /// Shows the total of the Tax amounts specified on all products included in the quote, included in the Total Amount due calculation for the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totaltax")]
    public Money TotalTax
    {
        get
        {
            return GetAttributeValue<Money>("totaltax");
        }
        set
        {
            SetAttributeValue("totaltax", value);
        }
    }

    /// <summary>
    /// Shows the Total Tax field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("totaltax_base")]
    public Money TotalTax_Base
    {
        get
        {
            return GetAttributeValue<Money>("totaltax_base");
        }
    }

    /// <summary>
    /// Choose the local currency for the record to make sure budgets are reported in the correct currency.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("transactioncurrencyid")]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("traversedpath")]
    public string TraversedPath
    {
        get
        {
            return GetAttributeValue<string>("traversedpath");
        }
        set
        {
            SetAttributeValue("traversedpath", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("utcconversiontimezonecode")]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version number of the quote.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("versionnumber")]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
        set
        {
            SetAttributeValue("versionnumber", value);
        }
    }

    /// <summary>
    /// Select whether the products included in the quote should be shipped to the specified address or held until the customer calls with further pick up or delivery instructions.
    /// </summary>
    [DataMember]
    [AttributeLogicalName("willcall")]
    public bool? WillCall
    {
        get
        {
            return GetAttributeValue<bool?>("willcall");
        }
        set
        {
            SetAttributeValue("willcall", value);
        }
    }


}

[DataContract()]
public enum QuoteState
{

    [EnumMember()]
    Draft = 0,

    [EnumMember()]
    Active = 1,

    [EnumMember()]
    Won = 2,

    [EnumMember()]
    Closed = 3,
}