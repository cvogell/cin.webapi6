﻿using Microsoft.Xrm.Sdk;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;


public class Appointment : Entity
{
    [DataMember]
    [AttributeLogicalName("activityid")]
    public Guid? ActivityId
    {
        get
        {
            return GetAttributeValue<Guid?>("activityid");
        }
        set
        {
            SetAttributeValue("activityid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [DataMember]
    [AttributeLogicalName("activityid")]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            ActivityId = value;
        }
    }

    [DataMember]
    [AttributeLogicalName("scheduledstart")]
    public EntityReference ScheduledStart
    {
        get
        {
            return GetAttributeValue<EntityReference>("scheduledstart");
        }
        set
        {
            SetAttributeValue("scheduledstart", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("scheduledend")]
    public EntityReference ScheduledEnd
    {
        get
        {
            return GetAttributeValue<EntityReference>("scheduledend");
        }
        set
        {
            SetAttributeValue("scheduledend", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("regardingobjectid")]
    public EntityReference RegardingObjectId
    {
        get
        {
            return GetAttributeValue<EntityReference>("regardingobjectid");
        }
        set
        {
            SetAttributeValue("regardingobjectid", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("instancetypecode")]
    public EntityReference InstanceTypeCode
    {
        get
        {
            return GetAttributeValue<EntityReference>("instancetypecode");
        }
        set
        {
            SetAttributeValue("instancetypecode", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("prioritycode")]
    public EntityReference PriorityCode
    {
        get
        {
            return GetAttributeValue<EntityReference>("prioritycode");
        }
        set
        {
            SetAttributeValue("prioritycode", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("location")]
    public EntityReference Location
    {
        get
        {
            return GetAttributeValue<EntityReference>("location");
        }
        set
        {
            SetAttributeValue("location", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("po_meeting_with")]
    public EntityReference po_Meeting_With
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_meeting_with");
        }
        set
        {
            SetAttributeValue("po_meeting_with", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("description")]
    public EntityReference Description
    {
        get
        {
            return GetAttributeValue<EntityReference>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /*[DataMember]
    [AttributeLogicalName("requiredattendees")]
    public Microsoft.Xrm.Sdk.EntityReference RequiredAttendees
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("requiredattendees");
        }
        set
        {
            this.SetAttributeValue("requiredattendees", value);
        }
    }*/

    [DataMember]
    [AttributeLogicalName("ownerid")]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /*[DataMember]
    [AttributeLogicalName("optionalattendees")]
    public Microsoft.Xrm.Sdk.EntityReference OptionalAttendees
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("optionalattendees");
        }
        set
        {
            this.SetAttributeValue("optionalattendees", value);
        }
    }*/

    [DataMember]
    [AttributeLogicalName("modifiedon")]
    public EntityReference ModifiedOn
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedon");
        }
        set
        {
            SetAttributeValue("modifiedon", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("createdon")]
    public EntityReference CreatedOn
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdon");
        }
        set
        {
            SetAttributeValue("createdon", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("ctas_appointment_type")]
    public EntityReference ctas_Appointment_Type
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_appointment_type");
        }
        set
        {
            SetAttributeValue("ctas_appointment_type", value);
        }
    }

    [DataMember]
    [AttributeLogicalName("statecode")]
    public EntityReference StateCode
    {
        get
        {
            return GetAttributeValue<EntityReference>("statecode");
        }
        set
        {
            SetAttributeValue("statecode", value);
        }
    }
}