﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("po_customer")]
public class CustomerDetails : Entity
{
    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Cross Sell Lookup
    /// </summary>
    [AttributeLogicalName("ctas_cross_sell")]
    [DataMember]
    public EntityReference ctas_Cross_Sell
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_cross_sell");
        }
        set
        {
            SetAttributeValue("ctas_cross_sell", value);
        }
    }

    /// <summary>
    /// SAP ID from the Data Warehouse
    /// </summary>
    [AttributeLogicalName("ctas_sap_id")]
    [DataMember]
    public string ctas_SAP_ID
    {
        get
        {
            return GetAttributeValue<string>("ctas_sap_id");
        }
        set
        {
            SetAttributeValue("ctas_sap_id", value);
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Unique identifier for the organization
    /// </summary>
    [AttributeLogicalName("organizationid")]
    [DataMember]
    public EntityReference OrganizationId
    {
        get
        {
            return GetAttributeValue<EntityReference>("organizationid");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_abimatchcondition")]
    [DataMember]
    public string po_ABIMatchCondition
    {
        get
        {
            return GetAttributeValue<string>("po_abimatchcondition");
        }
        set
        {
            SetAttributeValue("po_abimatchcondition", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_acctstatus")]
    [DataMember]
    public string po_Acctstatus
    {
        get
        {
            return GetAttributeValue<string>("po_acctstatus");
        }
        set
        {
            SetAttributeValue("po_acctstatus", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_billtonumber")]
    [DataMember]
    public string po_Billtonumber
    {
        get
        {
            return GetAttributeValue<string>("po_billtonumber");
        }
        set
        {
            SetAttributeValue("po_billtonumber", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessdivisionid")]
    [DataMember]
    public EntityReference po_businessdivisionid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessdivisionid");
        }
        set
        {
            SetAttributeValue("po_businessdivisionid", value);
        }
    }

    /// <summary>
    /// Unique identifier for Cintas Location associated with Customer Detail.
    /// </summary>
    [AttributeLogicalName("po_cintaslocationid")]
    [DataMember]
    public EntityReference po_CintasLocationId
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_cintaslocationid");
        }
        set
        {
            SetAttributeValue("po_cintaslocationid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cintergymarkedfordelete")]
    [DataMember]
    public bool? po_cintergymarkedfordelete
    {
        get
        {
            return GetAttributeValue<bool?>("po_cintergymarkedfordelete");
        }
        set
        {
            SetAttributeValue("po_cintergymarkedfordelete", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cleancustomername")]
    [DataMember]
    public string po_CleanCustomerName
    {
        get
        {
            return GetAttributeValue<string>("po_cleancustomername");
        }
        set
        {
            SetAttributeValue("po_cleancustomername", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contact1name")]
    [DataMember]
    public string po_Contact1Name
    {
        get
        {
            return GetAttributeValue<string>("po_contact1name");
        }
        set
        {
            SetAttributeValue("po_contact1name", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contact1title")]
    [DataMember]
    public string po_Contact1Title
    {
        get
        {
            return GetAttributeValue<string>("po_contact1title");
        }
        set
        {
            SetAttributeValue("po_contact1title", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contact2name")]
    [DataMember]
    public string po_Contact2Name
    {
        get
        {
            return GetAttributeValue<string>("po_contact2name");
        }
        set
        {
            SetAttributeValue("po_contact2name", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contact2title")]
    [DataMember]
    public string po_Contact2Title
    {
        get
        {
            return GetAttributeValue<string>("po_contact2title");
        }
        set
        {
            SetAttributeValue("po_contact2title", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractdateadded")]
    [DataMember]
    public DateTime? po_ContractDateAdded
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_contractdateadded");
        }
        set
        {
            SetAttributeValue("po_contractdateadded", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractdatelost")]
    [DataMember]
    public DateTime? po_ContractDateLost
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_contractdatelost");
        }
        set
        {
            SetAttributeValue("po_contractdatelost", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractexpiration")]
    [DataMember]
    public DateTime? po_ContractExpiration
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_contractexpiration");
        }
        set
        {
            SetAttributeValue("po_contractexpiration", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractlength")]
    [DataMember]
    public int? po_ContractLength
    {
        get
        {
            return GetAttributeValue<int?>("po_contractlength");
        }
        set
        {
            SetAttributeValue("po_contractlength", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractname")]
    [DataMember]
    public string po_ContractName
    {
        get
        {
            return GetAttributeValue<string>("po_contractname");
        }
        set
        {
            SetAttributeValue("po_contractname", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractnumber")]
    [DataMember]
    public int? po_ContractNumber
    {
        get
        {
            return GetAttributeValue<int?>("po_contractnumber");
        }
        set
        {
            SetAttributeValue("po_contractnumber", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractrenewalcode")]
    [DataMember]
    public string po_ContractRenewalCode
    {
        get
        {
            return GetAttributeValue<string>("po_contractrenewalcode");
        }
        set
        {
            SetAttributeValue("po_contractrenewalcode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractrenewaldate")]
    [DataMember]
    public DateTime? po_ContractRenewalDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_contractrenewaldate");
        }
        set
        {
            SetAttributeValue("po_contractrenewaldate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contractstatus")]
    [DataMember]
    public string po_ContractStatus
    {
        get
        {
            return GetAttributeValue<string>("po_contractstatus");
        }
        set
        {
            SetAttributeValue("po_contractstatus", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contracttype")]
    [DataMember]
    public string po_ContractType
    {
        get
        {
            return GetAttributeValue<string>("po_contracttype");
        }
        set
        {
            SetAttributeValue("po_contracttype", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_contracttypedescription")]
    [DataMember]
    public string po_ContractTypeDescription
    {
        get
        {
            return GetAttributeValue<string>("po_contracttypedescription");
        }
        set
        {
            SetAttributeValue("po_contracttypedescription", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_corporatecode")]
    [DataMember]
    public string po_CorporateCode
    {
        get
        {
            return GetAttributeValue<string>("po_corporatecode");
        }
        set
        {
            SetAttributeValue("po_corporatecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_corporatecodefire")]
    [DataMember]
    public string po_CorporateCodeFire
    {
        get
        {
            return GetAttributeValue<string>("po_corporatecodefire");
        }
        set
        {
            SetAttributeValue("po_corporatecodefire", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_currentrtenum")]
    [DataMember]
    public int? po_CurrentRteNum
    {
        get
        {
            return GetAttributeValue<int?>("po_currentrtenum");
        }
        set
        {
            SetAttributeValue("po_currentrtenum", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_customer_active")]
    [DataMember]
    public string po_CUSTOMER_ACTIVE
    {
        get
        {
            return GetAttributeValue<string>("po_customer_active");
        }
        set
        {
            //this.SetAttributeValue("po_customer_active", value);
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [AttributeLogicalName("po_customerid")]
    [DataMember]
    public Guid? po_customerId
    {
        get
        {
            return GetAttributeValue<Guid?>("po_customerid");
        }
        set
        {
            SetAttributeValue("po_customerid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("po_customerid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            po_customerId = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_customername")]
    [DataMember]
    public string po_CustomerName
    {
        get
        {
            return GetAttributeValue<string>("po_customername");
        }
        set
        {
            SetAttributeValue("po_customername", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_customernumber")]
    [DataMember]
    public string po_customernumber
    {
        get
        {
            return GetAttributeValue<string>("po_customernumber");
        }
        set
        {
            SetAttributeValue("po_customernumber", value);
        }
    }

    /// <summary>
    /// Unique identifier for Account associated with Customer.
    /// </summary>
    [AttributeLogicalName("po_customertoaccount")]
    [DataMember]
    public EntityReference po_customertoaccount
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_customertoaccount");
        }
        set
        {
            SetAttributeValue("po_customertoaccount", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_dateadded")]
    [DataMember]
    public DateTime? po_DateAdded
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_dateadded");
        }
        set
        {
            SetAttributeValue("po_dateadded", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_dateinitcontr")]
    [DataMember]
    public DateTime? po_DateInitContr
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_dateinitcontr");
        }
        set
        {
            SetAttributeValue("po_dateinitcontr", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_datelost")]
    [DataMember]
    public DateTime? po_DateLost
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_datelost");
        }
        set
        {
            SetAttributeValue("po_datelost", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_delday")]
    [DataMember]
    public int? po_DelDay
    {
        get
        {
            return GetAttributeValue<int?>("po_delday");
        }
        set
        {
            SetAttributeValue("po_delday", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_delfreq")]
    [DataMember]
    public string po_DelFreq
    {
        get
        {
            return GetAttributeValue<string>("po_delfreq");
        }
        set
        {
            SetAttributeValue("po_delfreq", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_delweek")]
    [DataMember]
    public int? po_DelWeek
    {
        get
        {
            return GetAttributeValue<int?>("po_delweek");
        }
        set
        {
            SetAttributeValue("po_delweek", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_dmemail")]
    [DataMember]
    public string po_Dmemail
    {
        get
        {
            return GetAttributeValue<string>("po_dmemail");
        }
        set
        {
            SetAttributeValue("po_dmemail", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_dmname")]
    [DataMember]
    public string po_DmName
    {
        get
        {
            return GetAttributeValue<string>("po_dmname");
        }
        set
        {
            SetAttributeValue("po_dmname", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_dmphn1")]
    [DataMember]
    public string po_DmPhn1
    {
        get
        {
            return GetAttributeValue<string>("po_dmphn1");
        }
        set
        {
            SetAttributeValue("po_dmphn1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_dmzipcode")]
    [DataMember]
    public string po_DmZipCode
    {
        get
        {
            return GetAttributeValue<string>("po_dmzipcode");
        }
        set
        {
            SetAttributeValue("po_dmzipcode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_fklocno")]
    [DataMember]
    public string po_FkLocNo
    {
        get
        {
            return GetAttributeValue<string>("po_fklocno");
        }
        set
        {
            SetAttributeValue("po_fklocno", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_householdid")]
    [DataMember]
    public string po_HouseholdId
    {
        get
        {
            return GetAttributeValue<string>("po_householdid");
        }
        set
        {
            SetAttributeValue("po_householdid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_installationdate")]
    [DataMember]
    public DateTime? po_InstallationDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_installationdate");
        }
        set
        {
            SetAttributeValue("po_installationdate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_lostholddate")]
    [DataMember]
    public DateTime? po_LostHoldDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_lostholddate");
        }
        set
        {
            SetAttributeValue("po_lostholddate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_lostholdreason")]
    [DataMember]
    public string po_LostHoldReason
    {
        get
        {
            return GetAttributeValue<string>("po_lostholdreason");
        }
        set
        {
            SetAttributeValue("po_lostholdreason", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_mlacode")]
    [DataMember]
    public string po_MLACode
    {
        get
        {
            return GetAttributeValue<string>("po_mlacode");
        }
        set
        {
            SetAttributeValue("po_mlacode", value);
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [AttributeLogicalName("po_name")]
    [DataMember]
    public string po_name
    {
        get
        {
            return GetAttributeValue<string>("po_name");
        }
        set
        {
            SetAttributeValue("po_name", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_outofbusiness")]
    [DataMember]
    public string po_OutOfBusiness
    {
        get
        {
            return GetAttributeValue<string>("po_outofbusiness");
        }
        set
        {
            SetAttributeValue("po_outofbusiness", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_ownercode")]
    [DataMember]
    public string po_Ownercode
    {
        get
        {
            return GetAttributeValue<string>("po_ownercode");
        }
        set
        {
            SetAttributeValue("po_ownercode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_routestatus")]
    [DataMember]
    public string po_RouteStatus
    {
        get
        {
            return GetAttributeValue<string>("po_routestatus");
        }
        set
        {
            SetAttributeValue("po_routestatus", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_routetype")]
    [DataMember]
    public string po_RouteType
    {
        get
        {
            return GetAttributeValue<string>("po_routetype");
        }
        set
        {
            SetAttributeValue("po_routetype", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_shipaddressline1")]
    [DataMember]
    public string po_ShipAddressLine1
    {
        get
        {
            return GetAttributeValue<string>("po_shipaddressline1");
        }
        set
        {
            SetAttributeValue("po_shipaddressline1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_shipaddressline2")]
    [DataMember]
    public string po_ShipAddressLine2
    {
        get
        {
            return GetAttributeValue<string>("po_shipaddressline2");
        }
        set
        {
            SetAttributeValue("po_shipaddressline2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_shipcity")]
    [DataMember]
    public string po_ShipCity
    {
        get
        {
            return GetAttributeValue<string>("po_shipcity");
        }
        set
        {
            SetAttributeValue("po_shipcity", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_shipcountrycode")]
    [DataMember]
    public string po_ShipCountryCode
    {
        get
        {
            return GetAttributeValue<string>("po_shipcountrycode");
        }
        set
        {
            SetAttributeValue("po_shipcountrycode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_shippostalcode")]
    [DataMember]
    public string po_ShipPostalCode
    {
        get
        {
            return GetAttributeValue<string>("po_shippostalcode");
        }
        set
        {
            SetAttributeValue("po_shippostalcode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_shipstate")]
    [DataMember]
    public string po_ShipState
    {
        get
        {
            return GetAttributeValue<string>("po_shipstate");
        }
        set
        {
            SetAttributeValue("po_shipstate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_shiptonumber")]
    [DataMember]
    public string po_ShipToNumber
    {
        get
        {
            return GetAttributeValue<string>("po_shiptonumber");
        }
        set
        {
            SetAttributeValue("po_shiptonumber", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_sourcesystem")]
    [DataMember]
    public string po_SourceSystem
    {
        get
        {
            return GetAttributeValue<string>("po_sourcesystem");
        }
        set
        {
            SetAttributeValue("po_sourcesystem", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_status")]
    [DataMember]
    public string po_Status
    {
        get
        {
            return GetAttributeValue<string>("po_status");
        }
        set
        {
            SetAttributeValue("po_status", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_stopacctnumber")]
    [DataMember]
    public string po_StopAcctNumber
    {
        get
        {
            return GetAttributeValue<string>("po_stopacctnumber");
        }
        set
        {
            SetAttributeValue("po_stopacctnumber", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_uniquecontractnumber")]
    [DataMember]
    public string po_UniqueContractNumber
    {
        get
        {
            return GetAttributeValue<string>("po_uniquecontractnumber");
        }
        set
        {
            SetAttributeValue("po_uniquecontractnumber", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_updatedate")]
    [DataMember]
    public DateTime? po_UpdateDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_updatedate");
        }
        set
        {
            SetAttributeValue("po_updatedate", value);
        }
    }

    /// <summary>
    /// Status of the Customer
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public po_customerState? statecode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (po_customerState)Enum.ToObject(typeof(po_customerState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the Customer
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue statuscode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}

[DataContract]
public enum po_customerState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}