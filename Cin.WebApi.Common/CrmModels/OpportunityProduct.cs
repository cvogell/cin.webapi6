﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("opportunityproduct")]
public class OpportunityProduct : Entity
{
    /// <summary>
    /// Shows the total price of the opportunity product, based on the price per unit, volume discount, and quantity.
    /// </summary>
    [AttributeLogicalName("baseamount")]
    [DataMember]
    public Money BaseAmount
    {
        get
        {
            return GetAttributeValue<Money>("baseamount");
        }
        set
        {
            SetAttributeValue("baseamount", value);
        }
    }

    /// <summary>
    /// Shows the Amount field converted to the system's default base currency. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("baseamount_base")]
    [DataMember]
    public Money BaseAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("baseamount_base");
        }
    }

    /// <summary>
    /// Shows who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Type additional information to describe the opportunity product, such as manufacturing details.
    /// </summary>
    [AttributeLogicalName("description")]
    [DataMember]
    public string Description
    {
        get
        {
            return GetAttributeValue<string>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /// <summary>
    /// The default image for the entity.
    /// </summary>
    [AttributeLogicalName("entityimage")]
    [DataMember]
    public byte[] EntityImage
    {
        get
        {
            return GetAttributeValue<byte[]>("entityimage");
        }
        set
        {
            SetAttributeValue("entityimage", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_timestamp")]
    [DataMember]
    public long? EntityImage_Timestamp
    {
        get
        {
            return GetAttributeValue<long?>("entityimage_timestamp");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_url")]
    [DataMember]
    public string EntityImage_URL
    {
        get
        {
            return GetAttributeValue<string>("entityimage_url");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("entityimageid")]
    [DataMember]
    public Guid? EntityImageId
    {
        get
        {
            return GetAttributeValue<Guid?>("entityimageid");
        }
    }

    /// <summary>
    /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Shows the total amount due for the opportunity product, calculated on the Amount value minus the Manual Discount amount.
    /// </summary>
    [AttributeLogicalName("extendedamount")]
    [DataMember]
    public Money ExtendedAmount
    {
        get
        {
            return GetAttributeValue<Money>("extendedamount");
        }
        set
        {
            SetAttributeValue("extendedamount", value);
        }
    }

    /// <summary>
    /// Shows the Extended Amount field converted to the system's default base currency. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("extendedamount_base")]
    [DataMember]
    public Money ExtendedAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("extendedamount_base");
        }
    }

    /// <summary>
    /// Unique identifier of the data import or data migration that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Select whether the pricing on the opportunity product reflects an override of the product catalog pricing.
    /// </summary>
    [AttributeLogicalName("ispriceoverridden")]
    [DataMember]
    public bool? IsPriceOverridden
    {
        get
        {
            return GetAttributeValue<bool?>("ispriceoverridden");
        }
        set
        {
            SetAttributeValue("ispriceoverridden", value);
        }
    }

    /// <summary>
    /// For system use only.
    /// </summary>
    [AttributeLogicalName("isproductoverridden")]
    [DataMember]
    public bool? IsProductOverridden
    {
        get
        {
            return GetAttributeValue<bool?>("isproductoverridden");
        }
        set
        {
            SetAttributeValue("isproductoverridden", value);
        }
    }

    /// <summary>
    /// Type the line item number for the opportunity product to easily identify the product in the opportunity documents and make sure it's listed in the correct order.
    /// </summary>
    [AttributeLogicalName("lineitemnumber")]
    [DataMember]
    public int? LineItemNumber
    {
        get
        {
            return GetAttributeValue<int?>("lineitemnumber");
        }
        set
        {
            SetAttributeValue("lineitemnumber", value);
        }
    }

    /// <summary>
    /// Type the manual discount amount for the opportunity product to deduct any negotiated or other savings from the product total.
    /// </summary>
    [AttributeLogicalName("manualdiscountamount")]
    [DataMember]
    public Money ManualDiscountAmount
    {
        get
        {
            return GetAttributeValue<Money>("manualdiscountamount");
        }
        set
        {
            SetAttributeValue("manualdiscountamount", value);
        }
    }

    /// <summary>
    /// Shows the Manual Discount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("manualdiscountamount_base")]
    [DataMember]
    public Money ManualDiscountAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("manualdiscountamount_base");
        }
    }

    /// <summary>
    /// Shows who last updated the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was last updated. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who last modified the opportunityproduct.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Unique identifier of the opportunity with which the opportunity product is associated.
    /// </summary>
    [AttributeLogicalName("opportunityid")]
    [DataMember]
    public EntityReference OpportunityId
    {
        get
        {
            return GetAttributeValue<EntityReference>("opportunityid");
        }
        set
        {
            SetAttributeValue("opportunityid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the opportunity product.
    /// </summary>
    [AttributeLogicalName("opportunityproductid")]
    [DataMember]
    public Guid? OpportunityProductId
    {
        get
        {
            return GetAttributeValue<Guid?>("opportunityproductid");
        }
        set
        {
            SetAttributeValue("opportunityproductid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("opportunityproductid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            OpportunityProductId = value;
        }
    }

    /// <summary>
    /// Select the status of the opportunity product.
    /// </summary>
    [AttributeLogicalName("opportunitystatecode")]
    [DataMember]
    public OptionSetValue OpportunityStateCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("opportunitystatecode");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user or team who owns the opportunity product.
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
    }

    /// <summary>
    /// Unique identifier of the business unit that owns the opportunity product.
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public Guid? OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<Guid?>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier of the user who owns the opportunity product.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public Guid? OwningUser
    {
        get
        {
            return GetAttributeValue<Guid?>("owninguser");
        }
    }

    /// <summary>
    /// Choose the parent bundle associated with this product
    /// </summary>
    [AttributeLogicalName("parentbundleid")]
    [DataMember]
    public Guid? ParentBundleId
    {
        get
        {
            return GetAttributeValue<Guid?>("parentbundleid");
        }
        set
        {
            SetAttributeValue("parentbundleid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_inventory")]
    [DataMember]
    public decimal? po_Inventory
    {
        get
        {
            return GetAttributeValue<decimal?>("po_inventory");
        }
        set
        {
            SetAttributeValue("po_inventory", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_itemdelfreq")]
    [DataMember]
    public string po_ItemDelFreq
    {
        get
        {
            return GetAttributeValue<string>("po_itemdelfreq");
        }
        set
        {
            SetAttributeValue("po_itemdelfreq", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_itemno")]
    [DataMember]
    public string po_ItemNo
    {
        get
        {
            return GetAttributeValue<string>("po_itemno");
        }
        set
        {
            SetAttributeValue("po_itemno", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_usagecd")]
    [DataMember]
    public string po_UsageCD
    {
        get
        {
            return GetAttributeValue<string>("po_usagecd");
        }
        set
        {
            SetAttributeValue("po_usagecd", value);
        }
    }

    /// <summary>
    /// Shows the price per unit of the opportunity product, based on the price list specified on the parent opportunity.
    /// </summary>
    [AttributeLogicalName("priceperunit")]
    [DataMember]
    public Money PricePerUnit
    {
        get
        {
            return GetAttributeValue<Money>("priceperunit");
        }
        set
        {
            SetAttributeValue("priceperunit", value);
        }
    }

    /// <summary>
    /// Shows the Price Per Unit field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("priceperunit_base")]
    [DataMember]
    public Money PricePerUnit_Base
    {
        get
        {
            return GetAttributeValue<Money>("priceperunit_base");
        }
    }

    /// <summary>
    /// Select the pricing error for the opportunity product.
    /// </summary>
    [AttributeLogicalName("pricingerrorcode")]
    [DataMember]
    public OptionSetValue PricingErrorCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("pricingerrorcode");
        }
        set
        {
            SetAttributeValue("pricingerrorcode", value);
        }
    }

    /// <summary>
    /// Unique identifier of the product line item association with bundle in the opportunity
    /// </summary>
    [AttributeLogicalName("productassociationid")]
    [DataMember]
    public Guid? ProductAssociationId
    {
        get
        {
            return GetAttributeValue<Guid?>("productassociationid");
        }
        set
        {
            SetAttributeValue("productassociationid", value);
        }
    }

    /// <summary>
    /// Type a detailed product description or additional notes about the opportunity product, such as talking points or product updates, that will assist the sales team when they discuss the product with the customer.
    /// </summary>
    [AttributeLogicalName("productdescription")]
    [DataMember]
    public string ProductDescription
    {
        get
        {
            return GetAttributeValue<string>("productdescription");
        }
        set
        {
            SetAttributeValue("productdescription", value);
        }
    }

    /// <summary>
    /// Choose the product to include on the opportunity to link the product's pricing and other information to the opportunity.
    /// </summary>
    [AttributeLogicalName("productid")]
    [DataMember]
    public EntityReference ProductId
    {
        get
        {
            return GetAttributeValue<EntityReference>("productid");
        }
        set
        {
            SetAttributeValue("productid", value);
        }
    }

    /// <summary>
    /// Product Type
    /// </summary>
    [AttributeLogicalName("producttypecode")]
    [DataMember]
    public OptionSetValue ProductTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("producttypecode");
        }
        set
        {
            SetAttributeValue("producttypecode", value);
        }
    }

    /// <summary>
    /// Status of the property configuration.
    /// </summary>
    [AttributeLogicalName("propertyconfigurationstatus")]
    [DataMember]
    public OptionSetValue PropertyConfigurationStatus
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("propertyconfigurationstatus");
        }
        set
        {
            SetAttributeValue("propertyconfigurationstatus", value);
        }
    }

    /// <summary>
    /// Type the amount or quantity of the product the customer would like to purchase.
    /// </summary>
    [AttributeLogicalName("quantity")]
    [DataMember]
    public decimal? Quantity
    {
        get
        {
            return GetAttributeValue<decimal?>("quantity");
        }
        set
        {
            SetAttributeValue("quantity", value);
        }
    }

    /// <summary>
    /// Shows the ID of the data that maintains the sequence.
    /// </summary>
    [AttributeLogicalName("sequencenumber")]
    [DataMember]
    public int? SequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("sequencenumber");
        }
        set
        {
            SetAttributeValue("sequencenumber", value);
        }
    }

    /// <summary>
    /// Type the tax amount to be applied on the opportunity product.
    /// </summary>
    [AttributeLogicalName("tax")]
    [DataMember]
    public Money Tax
    {
        get
        {
            return GetAttributeValue<Money>("tax");
        }
        set
        {
            SetAttributeValue("tax", value);
        }
    }

    /// <summary>
    /// Shows the Tax field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("tax_base")]
    [DataMember]
    public Money Tax_Base
    {
        get
        {
            return GetAttributeValue<Money>("tax_base");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Choose the local currency for the record to make sure budgets are reported in the correct currency.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
    }

    /// <summary>
    /// Choose the unit of measurement for the base unit quantity for this purchase, such as each or dozen.
    /// </summary>
    [AttributeLogicalName("uomid")]
    [DataMember]
    public EntityReference UoMId
    {
        get
        {
            return GetAttributeValue<EntityReference>("uomid");
        }
        set
        {
            SetAttributeValue("uomid", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version number of the opportunity product.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }

    /// <summary>
    /// Shows the discount amount per unit if a specified volume is purchased. Configure volume discounts in the Product Catalog in the Settings area.
    /// </summary>
    [AttributeLogicalName("volumediscountamount")]
    [DataMember]
    public Money VolumeDiscountAmount
    {
        get
        {
            return GetAttributeValue<Money>("volumediscountamount");
        }
    }

    /// <summary>
    /// Shows the Volume Discount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("volumediscountamount_base")]
    [DataMember]
    public Money VolumeDiscountAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("volumediscountamount_base");
        }
    }
}