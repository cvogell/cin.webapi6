﻿using Microsoft.Xrm.Sdk;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract()]
[Microsoft.Xrm.Sdk.Client.EntityLogicalName("quotedetail")]
public class QuoteDetail : Entity
{

    public const string EntityLogicalName = "quotedetail";
    public const int EntityTypeCode = 1085;

    /// <summary>
    /// Shows the total price of the quote product, based on the price per unit, volume discount, and quantity.
    /// </summary>
    [AttributeLogicalName("baseamount")]
    [DataMember]
    public Money BaseAmount
    {
        get
        {
            return GetAttributeValue<Money>("baseamount");
        }
        set
        {
            SetAttributeValue("baseamount", value);
        }
    }

    /// <summary>
    /// Shows the Amount field converted to the system's default base currency. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("baseamount_base")]
    [DataMember]
    public Money BaseAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("baseamount_base");
        }
    }

    /// <summary>
    /// Shows who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Shows who created the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Type additional information to describe the quote product, such as manufacturing details or acceptable substitutions.
    /// </summary>
    [AttributeLogicalName("description")]
    [DataMember]
    public string Description
    {
        get
        {
            return GetAttributeValue<string>("description");
        }
        set
        {
            SetAttributeValue("description", value);
        }
    }

    /// <summary>
    /// Shows the conversion rate of the record's currency. The exchange rate is used to convert all money fields in the record from the local currency to the system's default currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// Shows the total amount due for the quote product, based on the sum of the unit price, quantity, discounts ,and tax.
    /// </summary>
    [AttributeLogicalName("extendedamount")]
    [DataMember]
    public Money ExtendedAmount
    {
        get
        {
            return GetAttributeValue<Money>("extendedamount");
        }
        set
        {
            SetAttributeValue("extendedamount", value);
        }
    }

    /// <summary>
    /// Shows the Extended Amount field converted to the system's default base currency. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("extendedamount_base")]
    [DataMember]
    public Money ExtendedAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("extendedamount_base");
        }
    }

    /// <summary>
    /// Unique identifier of the data import or data migration that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Select whether the price per unit is fixed at the value in the specified price list or can be overridden by users who have edit rights to the quote product.
    /// </summary>
    [AttributeLogicalName("ispriceoverridden")]
    [DataMember]
    public bool? IsPriceOverridden
    {
        get
        {
            return GetAttributeValue<bool?>("ispriceoverridden");
        }
        set
        {
            SetAttributeValue("ispriceoverridden", value);
        }
    }

    /// <summary>
    /// Select whether the product exists in the Microsoft Dynamics 365 product catalog or is a write-in product specific to the quote.
    /// </summary>
    [AttributeLogicalName("isproductoverridden")]
    [DataMember]
    public bool? IsProductOverridden
    {
        get
        {
            return GetAttributeValue<bool?>("isproductoverridden");
        }
        set
        {
            SetAttributeValue("isproductoverridden", value);
        }
    }

    /// <summary>
    /// Type the line item number for the quote product to easily identify the product in the quote and make sure it's listed in the correct order.
    /// </summary>
    [AttributeLogicalName("lineitemnumber")]
    [DataMember]
    public int? LineItemNumber
    {
        get
        {
            return GetAttributeValue<int?>("lineitemnumber");
        }
        set
        {
            SetAttributeValue("lineitemnumber", value);
        }
    }

    /// <summary>
    /// Type the manual discount amount for the quote product to deduct any negotiated or other savings from the product total on the quote.
    /// </summary>
    [AttributeLogicalName("manualdiscountamount")]
    [DataMember]
    public Money ManualDiscountAmount
    {
        get
        {
            return GetAttributeValue<Money>("manualdiscountamount");
        }
        set
        {
            SetAttributeValue("manualdiscountamount", value);
        }
    }

    /// <summary>
    /// Shows the Manual Discount field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("manualdiscountamount_base")]
    [DataMember]
    public Money ManualDiscountAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("manualdiscountamount_base");
        }
    }

    /// <summary>
    /// Shows who last updated the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Shows the date and time when the record was last updated. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Shows who last updated the record on behalf of another user.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user or team who owns the quote detail.
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
    }

    /// <summary>
    /// Unique identifier of the business unit that owns the quote detail.
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public Guid? OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<Guid?>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier of the user who owns the quote detail.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public Guid? OwningUser
    {
        get
        {
            return GetAttributeValue<Guid?>("owninguser");
        }
    }

    /// <summary>
    /// Choose the parent bundle associated with this product
    /// </summary>
    [AttributeLogicalName("parentbundleid")]
    [DataMember]
    public Guid? ParentBundleId
    {
        get
        {
            return GetAttributeValue<Guid?>("parentbundleid");
        }
        set
        {
            SetAttributeValue("parentbundleid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_inventory")]
    [DataMember]
    public decimal? po_Inventory
    {
        get
        {
            return GetAttributeValue<decimal?>("po_inventory");
        }
        set
        {
            SetAttributeValue("po_inventory", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_itemdelfreq")]
    [DataMember]
    public string po_ItemDelFreq
    {
        get
        {
            return GetAttributeValue<string>("po_itemdelfreq");
        }
        set
        {
            SetAttributeValue("po_itemdelfreq", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_itemno")]
    [DataMember]
    public string po_ItemNo
    {
        get
        {
            return GetAttributeValue<string>("po_itemno");
        }
        set
        {
            SetAttributeValue("po_itemno", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_usagecd")]
    [DataMember]
    public string po_UsageCD
    {
        get
        {
            return GetAttributeValue<string>("po_usagecd");
        }
        set
        {
            SetAttributeValue("po_usagecd", value);
        }
    }

    /// <summary>
    /// Type the price per unit of the quote product. The default is to the value in the price list specified on the quote for existing products.
    ///Name changed to unit price
    /// </summary>
    [AttributeLogicalName("priceperunit")]
    [DataMember]
    public Money PricePerUnit
    {
        get
        {
            return GetAttributeValue<Money>("priceperunit");
        }
        set
        {
            SetAttributeValue("priceperunit", value);
        }
    }

    /// <summary>
    /// Shows the Price Per Unit field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("priceperunit_base")]
    [DataMember]
    public Money PricePerUnit_Base
    {
        get
        {
            return GetAttributeValue<Money>("priceperunit_base");
        }
    }

    /// <summary>
    /// Select the type of pricing error, such as a missing or invalid product, or missing quantity.
    /// </summary>
    [AttributeLogicalName("pricingerrorcode")]
    [DataMember]
    public OptionSetValue PricingErrorCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("pricingerrorcode");
        }
        set
        {
            SetAttributeValue("pricingerrorcode", value);
        }
    }

    /// <summary>
    /// Unique identifier of the product line item association with bundle in the quote
    /// </summary>
    [AttributeLogicalName("productassociationid")]
    [DataMember]
    public Guid? ProductAssociationId
    {
        get
        {
            return GetAttributeValue<Guid?>("productassociationid");
        }
        set
        {
            SetAttributeValue("productassociationid", value);
        }
    }

    /// <summary>
    /// Type a name or description to identify the type of write-in product included in the quote.
    /// </summary>
    [AttributeLogicalName("productdescription")]
    [DataMember]
    public string ProductDescription
    {
        get
        {
            return GetAttributeValue<string>("productdescription");
        }
        set
        {
            SetAttributeValue("productdescription", value);
        }
    }

    /// <summary>
    /// Choose the product to include on the quote to link the product's pricing and other information to the quote.
    /// </summary>
    [AttributeLogicalName("productid")]
    [DataMember]
    public EntityReference ProductId
    {
        get
        {
            return GetAttributeValue<EntityReference>("productid");
        }
        set
        {
            SetAttributeValue("productid", value);
        }
    }

    /// <summary>
    /// Product Type
    /// </summary>
    [AttributeLogicalName("producttypecode")]
    [DataMember]
    public OptionSetValue ProductTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("producttypecode");
        }
        set
        {
            SetAttributeValue("producttypecode", value);
        }
    }

    /// <summary>
    /// Status of the property configuration.
    /// </summary>
    [AttributeLogicalName("propertyconfigurationstatus")]
    [DataMember]
    public OptionSetValue PropertyConfigurationStatus
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("propertyconfigurationstatus");
        }
        set
        {
            SetAttributeValue("propertyconfigurationstatus", value);
        }
    }

    /// <summary>
    /// Type the amount or quantity of the product requested by the customer.
    /// </summary>
    [AttributeLogicalName("quantity")]
    [DataMember]
    public decimal? Quantity
    {
        get
        {
            return GetAttributeValue<decimal?>("quantity");
        }
        set
        {
            SetAttributeValue("quantity", value);
        }
    }

    /// <summary>
    /// Unique identifier of the product line item in the quote.
    /// </summary>
    [AttributeLogicalName("quotedetailid")]
    [DataMember]
    public Guid? QuoteDetailId
    {
        get
        {
            return GetAttributeValue<Guid?>("quotedetailid");
        }
        set
        {
            SetAttributeValue("quotedetailid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("quotedetailid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            QuoteDetailId = value;
        }
    }

    /// <summary>
    /// Unique identifier of the quote for the quote product.
    /// </summary>
    [AttributeLogicalName("quoteid")]
    [DataMember]
    public EntityReference QuoteId
    {
        get
        {
            return GetAttributeValue<EntityReference>("quoteid");
        }
        set
        {
            SetAttributeValue("quoteid", value);
        }
    }

    /// <summary>
    /// Status of the quote product.
    /// </summary>
    [AttributeLogicalName("quotestatecode")]
    [DataMember]
    public OptionSetValue QuoteStateCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("quotestatecode");
        }
    }

    /// <summary>
    /// Enter the delivery date requested by the customer for the quote product.
    /// </summary>
    [AttributeLogicalName("requestdeliveryby")]
    [DataMember]
    public DateTime? RequestDeliveryBy
    {
        get
        {
            return GetAttributeValue<DateTime?>("requestdeliveryby");
        }
        set
        {
            SetAttributeValue("requestdeliveryby", value);
        }
    }

    /// <summary>
    /// Choose the user responsible for the sale of the quote product.
    /// </summary>
    [AttributeLogicalName("salesrepid")]
    [DataMember]
    public EntityReference SalesRepId
    {
        get
        {
            return GetAttributeValue<EntityReference>("salesrepid");
        }
        set
        {
            SetAttributeValue("salesrepid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the data that maintains the sequence.
    /// </summary>
    [AttributeLogicalName("sequencenumber")]
    [DataMember]
    public int? SequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("sequencenumber");
        }
        set
        {
            SetAttributeValue("sequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_addressid")]
    [DataMember]
    public Guid? ShipTo_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("shipto_addressid");
        }
        set
        {
            SetAttributeValue("shipto_addressid", value);
        }
    }

    /// <summary>
    /// Type the city for the customer's shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_city")]
    [DataMember]
    public string ShipTo_City
    {
        get
        {
            return GetAttributeValue<string>("shipto_city");
        }
        set
        {
            SetAttributeValue("shipto_city", value);
        }
    }

    /// <summary>
    /// Type the primary contact name at the customer's shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_contactname")]
    [DataMember]
    public string ShipTo_ContactName
    {
        get
        {
            return GetAttributeValue<string>("shipto_contactname");
        }
        set
        {
            SetAttributeValue("shipto_contactname", value);
        }
    }

    /// <summary>
    /// Type the country or region for the customer's shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_country")]
    [DataMember]
    public string ShipTo_Country
    {
        get
        {
            return GetAttributeValue<string>("shipto_country");
        }
        set
        {
            SetAttributeValue("shipto_country", value);
        }
    }

    /// <summary>
    /// Type the fax number for the customer's shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_fax")]
    [DataMember]
    public string ShipTo_Fax
    {
        get
        {
            return GetAttributeValue<string>("shipto_fax");
        }
        set
        {
            SetAttributeValue("shipto_fax", value);
        }
    }

    /// <summary>
    /// Select the freight terms to make sure shipping orders are processed correctly.
    /// </summary>
    [AttributeLogicalName("shipto_freighttermscode")]
    [DataMember]
    public OptionSetValue ShipTo_FreightTermsCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("shipto_freighttermscode");
        }
        set
        {
            SetAttributeValue("shipto_freighttermscode", value);
        }
    }

    /// <summary>
    /// Type the first line of the customer's shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_line1")]
    [DataMember]
    public string ShipTo_Line1
    {
        get
        {
            return GetAttributeValue<string>("shipto_line1");
        }
        set
        {
            SetAttributeValue("shipto_line1", value);
        }
    }

    /// <summary>
    /// Type the second line of the customer's shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_line2")]
    [DataMember]
    public string ShipTo_Line2
    {
        get
        {
            return GetAttributeValue<string>("shipto_line2");
        }
        set
        {
            SetAttributeValue("shipto_line2", value);
        }
    }

    /// <summary>
    /// Type the third line of the shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_line3")]
    [DataMember]
    public string ShipTo_Line3
    {
        get
        {
            return GetAttributeValue<string>("shipto_line3");
        }
        set
        {
            SetAttributeValue("shipto_line3", value);
        }
    }

    /// <summary>
    /// Type a name for the customer's shipping address, such as "Headquarters" or "Field office", to identify the address.
    /// </summary>
    [AttributeLogicalName("shipto_name")]
    [DataMember]
    public string ShipTo_Name
    {
        get
        {
            return GetAttributeValue<string>("shipto_name");
        }
        set
        {
            SetAttributeValue("shipto_name", value);
        }
    }

    /// <summary>
    /// Type the ZIP Code or postal code for the shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_postalcode")]
    [DataMember]
    public string ShipTo_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("shipto_postalcode");
        }
        set
        {
            SetAttributeValue("shipto_postalcode", value);
        }
    }

    /// <summary>
    /// Type the state or province for the shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_stateorprovince")]
    [DataMember]
    public string ShipTo_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("shipto_stateorprovince");
        }
        set
        {
            SetAttributeValue("shipto_stateorprovince", value);
        }
    }

    /// <summary>
    /// Type the phone number for the customer's shipping address.
    /// </summary>
    [AttributeLogicalName("shipto_telephone")]
    [DataMember]
    public string ShipTo_Telephone
    {
        get
        {
            return GetAttributeValue<string>("shipto_telephone");
        }
        set
        {
            SetAttributeValue("shipto_telephone", value);
        }
    }

    /// <summary>
    /// Type the tax amount for the quote product.
    /// </summary>
    [AttributeLogicalName("tax")]
    [DataMember]
    public Money Tax
    {
        get
        {
            return GetAttributeValue<Money>("tax");
        }
        set
        {
            SetAttributeValue("tax", value);
        }
    }

    /// <summary>
    /// Shows the Tax field converted to the system's default base currency for reporting purposes. The calculation uses the exchange rate specified in the Currencies area.
    /// </summary>
    [AttributeLogicalName("tax_base")]
    [DataMember]
    public Money Tax_Base
    {
        get
        {
            return GetAttributeValue<Money>("tax_base");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Choose the local currency for the record to make sure budgets are reported in the correct currency.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// Choose the unit of measurement for the base unit quantity for this purchase, such as each or dozen.
    /// </summary>
    [AttributeLogicalName("uomid")]
    [DataMember]
    public EntityReference UoMId
    {
        get
        {
            return GetAttributeValue<EntityReference>("uomid");
        }
        set
        {
            SetAttributeValue("uomid", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version number of the quote detail.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
        set
        {
            SetAttributeValue("versionnumber", value);
        }
    }

    /// <summary>
    /// Shows the discount amount per unit if a specified volume is purchased. Configure volume discounts in the Product Catalog in the Settings area.
    /// </summary>
    [AttributeLogicalName("volumediscountamount")]
    [DataMember]
    public Money VolumeDiscountAmount
    {
        get
        {
            return GetAttributeValue<Money>("volumediscountamount");
        }
        set
        {
            SetAttributeValue("volumediscountamount", value);
        }
    }

    /// <summary>
    /// Shows the discount amount per unit if a specified volume is purchased. Configure volume discounts in the Product Catalog in the Settings area.
    /// </summary>
    [AttributeLogicalName("volumediscountamount_base")]
    [DataMember]
    public Money VolumeDiscountAmount_Base
    {
        get
        {
            return GetAttributeValue<Money>("volumediscountamount_base");
        }
        set
        {
            SetAttributeValue("volumediscountamount_base", value);
        }
    }

    /// <summary>
    /// Select whether the quote product should be shipped to the specified address or held until the customer calls with further pick up or delivery instructions.
    /// </summary>
    [AttributeLogicalName("willcall")]
    [DataMember]
    public bool? WillCall
    {
        get
        {
            return GetAttributeValue<bool?>("willcall");
        }
        set
        {
            SetAttributeValue("willcall", value);
        }
    }

}