﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract()]
[EntityLogicalName("leadreferral")]
public class LeadReferral : Entity
{
    public const string EntityLogicalName = "account";
    public const int EntityTypeCode = 1;
    /// <summary>
    /// Unique identifier of the account.
    /// </summary>
    [AttributeLogicalName("accountid")]
    [DataMember]
    public Guid? AccountId
    {
        get
        {
            return GetAttributeValue<Guid?>("accountid");
        }
        set
        {
            SetAttributeValue("accountid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("accountid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            AccountId = value;
        }
    }

    /// <summary>
    /// Type the city for the primary address.
    /// </summary>
    [AttributeLogicalName("address1_city")]
    [DataMember]
    public string Address1_City
    {
        get
        {
            return GetAttributeValue<string>("address1_city");
        }
        set
        {
            SetAttributeValue("address1_city", value);
        }
    }

    /// <summary>
    /// Type the main phone number for this account.
    /// </summary>
    [AttributeLogicalName("telephone1")]
    [DataMember]
    public string Telephone1
    {
        get
        {
            return GetAttributeValue<string>("telephone1");
        }
        set
        {
            SetAttributeValue("telephone1", value);
        }
    }
    /// <summary>
    /// Type the first line of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_line1")]
    [DataMember]
    public string Address1_Line1
    {
        get
        {
            return GetAttributeValue<string>("address1_line1");
        }
        set
        {
            SetAttributeValue("address1_line1", value);
        }
    }
    /// <summary>
    /// Type the state or province of the primary address.
    /// </summary>
    [AttributeLogicalName("address1_stateorprovince")]
    [DataMember]
    public string Address1_stateorprovince
    {
        get
        {
            return GetAttributeValue<string>("address1_stateorprovince");
        }
        set
        {
            SetAttributeValue("address1_stateorprovince", value);
        }
    }
    /// <summary>
    /// Current Customer of Catalog
    /// </summary>
    [AttributeLogicalName("po_custofcatalog")]
    [DataMember]
    public bool? po_CustofCatalog
    {
        get
        {
            return GetAttributeValue<bool?>("po_custofcatalog");
        }
        set
        {
            SetAttributeValue("po_custofcatalog", value);
        }
    }

    /// <summary>
    /// Current Customer of Chicago Direct Sale
    /// </summary>
    [AttributeLogicalName("po_custofchicagods")]
    [DataMember]
    public bool? po_CustofChicagoDS
    {
        get
        {
            return GetAttributeValue<bool?>("po_custofchicagods");
        }
        set
        {
            SetAttributeValue("po_custofchicagods", value);
        }
    }

    /// <summary>
    /// Current Customer of FAS
    /// </summary>
    [AttributeLogicalName("po_custoffas")]
    [DataMember]
    public bool? po_CustofFAS
    {
        get
        {
            return GetAttributeValue<bool?>("po_custoffas");
        }
        set
        {
            SetAttributeValue("po_custoffas", value);
        }
    }

    /// <summary>
    /// Current Customer of Fire
    /// </summary>
    [AttributeLogicalName("po_custoffire")]
    [DataMember]
    public bool? po_CustofFire
    {
        get
        {
            return GetAttributeValue<bool?>("po_custoffire");
        }
        set
        {
            SetAttributeValue("po_custoffire", value);
        }
    }

    /// <summary>
    /// Current Customer of FS
    /// </summary>
    [AttributeLogicalName("po_custoffs")]
    [DataMember]
    public bool? po_CustofFS
    {
        get
        {
            return GetAttributeValue<bool?>("po_custoffs");
        }
        set
        {
            SetAttributeValue("po_custoffs", value);
        }
    }

    /// <summary>
    /// Current customer of UR
    /// </summary>
    [AttributeLogicalName("po_custofur")]
    [DataMember]
    public bool? po_CustofUR
    {
        get
        {
            return GetAttributeValue<bool?>("po_custofur");
        }
        set
        {
            SetAttributeValue("po_custofur", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_dba")]
    [DataMember]
    public string po_DBA
    {
        get
        {
            return GetAttributeValue<string>("po_dba");
        }
        set
        {
            SetAttributeValue("po_dba", value);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_naicscodeid")]
    [DataMember]
    public EntityReference po_NAICSCodeid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_naicscodeid");
        }
        set
        {
            SetAttributeValue("po_naicscodeid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_naicsdescription")]
    [DataMember]
    public string po_NAICSDescription
    {
        get
        {
            return GetAttributeValue<string>("po_naicsdescription");
        }
        set
        {
            SetAttributeValue("po_naicsdescription", value);
        }
    }
    /// <summary>
    /// Contains a option set of number of employees for a company
    /// </summary>
    [AttributeLogicalName("ctas_no_employees_opt")]
    [DataMember]
    public OptionSetValue ctas_No_Employees_Opt
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_no_employees_opt");
        }
        set
        {
            SetAttributeValue("ctas_no_employees_opt", value);
        }
    }
    /// <summary>
    /// Option set for square footage.
    /// </summary>
    [AttributeLogicalName("ctas_square_footage_opt")]
    [DataMember]
    public OptionSetValue ctas_Square_Footage_Opt
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_square_footage_opt");
        }
        set
        {
            SetAttributeValue("ctas_square_footage_opt", value);
        }
    }
    /// <summary>
    /// Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }
    /// <summary>
    /// Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics 365 options.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }
}