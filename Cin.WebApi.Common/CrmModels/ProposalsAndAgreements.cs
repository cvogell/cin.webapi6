﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("ctas_proposalsandagreements")]
public class ProposalsAndAgreements : Entity
{
    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
        set
        {
            SetAttributeValue("createdonbehalfby", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_azuredocumenturl")]
    public string ctas_AzureDocumentURL
    {
        get
        {
            return GetAttributeValue<string>("ctas_azuredocumenturl");
        }
        set
        {
            SetAttributeValue("ctas_azuredocumenturl", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_customersigneddate")]
    public DateTime? ctas_CustomerSignedDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_customersigneddate");
        }
        set
        {
            SetAttributeValue("ctas_customersigneddate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_documentlocation")]
    public OptionSetValue ctas_DocumentLocation
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_documentlocation");
        }
        set
        {
            SetAttributeValue("ctas_documentlocation", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_documenttype")]
    public OptionSetValue ctas_DocumentType
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_documenttype");
        }
        set
        {
            SetAttributeValue("ctas_documenttype", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_documenturl")]
    public string ctas_DocumentURL
    {
        get
        {
            return GetAttributeValue<string>("ctas_documenturl");
        }
        set
        {
            SetAttributeValue("ctas_documenturl", value);
        }
    }

    /// <summary>
    /// This field will hold the guid of docusign envelope id. Only populated in the parent proposals and agreements.
    /// </summary>
    [AttributeLogicalName("ctas_docusignenvelopeid")]
    public string ctas_DocusignEnvelopeId
    {
        get
        {
            return GetAttributeValue<string>("ctas_docusignenvelopeid");
        }
        set
        {
            SetAttributeValue("ctas_docusignenvelopeid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_expirationdate")]
    public DateTime? ctas_ExpirationDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_expirationdate");
        }
        set
        {

            SetAttributeValue("ctas_expirationdate", value);
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [AttributeLogicalName("ctas_name")]
    public string ctas_name
    {
        get
        {
            return GetAttributeValue<string>("ctas_name");
        }
        set
        {
            SetAttributeValue("ctas_name", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_opportunity")]
    public EntityReference ctas_Opportunity
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_opportunity");
        }
        set
        {
            SetAttributeValue("ctas_opportunity", value);
        }
    }

    /// <summary>
    /// This will represent the current revision of the document. Old revisions will have this field populated with the current one. The current revision will be the only one linked to an Opportunity.
    /// </summary>
    [AttributeLogicalName("ctas_propagrmtcurrentrevisionid")]
    public EntityReference ctas_PropAgrmtCurrentRevisionId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_propagrmtcurrentrevisionid");
        }
        set
        {
            SetAttributeValue("ctas_propagrmtcurrentrevisionid", value);
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [AttributeLogicalName("ctas_proposalsandagreementsid")]
    public Guid? ctas_proposalsandagreementsId
    {
        get
        {
            return GetAttributeValue<Guid?>("ctas_proposalsandagreementsid");
        }
        set
        {
            SetAttributeValue("ctas_proposalsandagreementsid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("ctas_proposalsandagreementsid")]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            ctas_proposalsandagreementsId = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_sentdate")]
    public DateTime? ctas_SentDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_sentdate");
        }
        set
        {
            SetAttributeValue("ctas_sentdate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_signaturetype")]
    public OptionSetValue ctas_SignatureType
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_signaturetype");
        }
        set
        {
            SetAttributeValue("ctas_signaturetype", value);
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
        set
        {
            SetAttributeValue("modifiedonbehalfby", value);
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Owner Id
    /// </summary>
    [AttributeLogicalName("ownerid")]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the business unit that owns the record
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier for the team that owns the record.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier for the user that owns the record.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// Status of the Proposals and Agreements
    /// </summary>
    [AttributeLogicalName("statecode")]
    public ctas_proposalsandagreementsState? StateCode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (ctas_proposalsandagreementsState)Enum.ToObject(typeof(ctas_proposalsandagreementsState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the Proposals and Agreements
    /// </summary>
    [AttributeLogicalName("statuscode")]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version Number
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}

[DataContract()]
public enum ctas_proposalsandagreementsState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}