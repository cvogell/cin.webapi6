using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels;

[DataContract()]
public enum po_agreementtermvariationState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}

[DataContract]
[EntityLogicalName("po_agreementtermvariation")]
public class AgreementTermVariation : Entity
{
    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
        set
        {
            SetAttributeValue("createdonbehalfby", value);
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
        set
        {
            SetAttributeValue("modifiedonbehalfby", value);
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Owner Id
    /// </summary>
    [AttributeLogicalName("ownerid")]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the business unit that owns the record
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier for the team that owns the record.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier for the user that owns the record.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [AttributeLogicalName("po_agreementtermvariationid")]
    public Guid? po_agreementtermvariationId
    {
        get
        {
            return GetAttributeValue<Guid?>("po_agreementtermvariationid");
        }
        set
        {
            SetAttributeValue("po_agreementtermvariationid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("po_agreementtermvariationid")]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            po_agreementtermvariationId = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_parentterm")]
    public EntityReference po_ParentTerm
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_parentterm");
        }
        set
        {
            SetAttributeValue("po_parentterm", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_sortorder")]
    public int? po_SortOrder
    {
        get
        {
            return GetAttributeValue<int?>("po_sortorder");
        }
        set
        {
            SetAttributeValue("po_sortorder", value);
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [AttributeLogicalName("po_termvariationcode")]
    public string po_termvariationcode
    {
        get
        {
            return GetAttributeValue<string>("po_termvariationcode");
        }
        set
        {
            SetAttributeValue("po_termvariationcode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_termvariationtext")]
    public string po_TermVariationText
    {
        get
        {
            return GetAttributeValue<string>("po_termvariationtext");
        }
        set
        {
            SetAttributeValue("po_termvariationtext", value);
        }
    }

    /// <summary>
    /// Status of the Agreement Term Variation
    /// </summary>
    [AttributeLogicalName("statecode")]
    public po_agreementtermvariationState? StateCode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (po_agreementtermvariationState)Enum.ToObject(typeof(po_agreementtermvariationState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the Agreement Term Variation
    /// </summary>
    [AttributeLogicalName("statuscode")]
    public OptionSetValue StatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version Number
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}
