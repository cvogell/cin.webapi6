﻿using Microsoft.Xrm.Sdk;
using System.Runtime.Serialization;


namespace Cin.WebApi.CrmModels;

[DataContract()]
[Microsoft.Xrm.Sdk.Client.EntityLogicalName("systemuser")]
public class SystemUser : Entity
{
    public const string EntityLogicalName = "systemuser";
    public const int EntityTypeCode = 8;

    /// <summary>
    /// Type of user.
    /// </summary>
    [AttributeLogicalName("accessmode")]
    [DataMember]
    public OptionSetValue AccessMode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("accessmode");
        }
        set
        {
            SetAttributeValue("accessmode", value);
        }
    }

    /// <summary>
    /// Unique identifier for address 1.
    /// </summary>
    [AttributeLogicalName("address1_addressid")]
    [DataMember]
    public Guid? Address1_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("address1_addressid");
        }
        set
        {
            SetAttributeValue("address1_addressid", value);
        }
    }

    /// <summary>
    /// Type of address for address 1, such as billing, shipping, or primary address.
    /// </summary>
    [AttributeLogicalName("address1_addresstypecode")]
    [DataMember]
    public OptionSetValue Address1_AddressTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address1_addresstypecode");
        }
        set
        {
            SetAttributeValue("address1_addresstypecode", value);
        }
    }

    /// <summary>
    /// City name for address 1.
    /// </summary>
    [AttributeLogicalName("address1_city")]
    [DataMember]
    public string Address1_City
    {
        get
        {
            return GetAttributeValue<string>("address1_city");
        }
        set
        {
            SetAttributeValue("address1_city", value);
        }
    }

    /// <summary>
    /// Shows the complete primary address.
    /// </summary>
    [AttributeLogicalName("address1_composite")]
    [DataMember]
    public string Address1_Composite
    {
        get
        {
            return GetAttributeValue<string>("address1_composite");
        }
    }

    /// <summary>
    /// Country/region name in address 1.
    /// </summary>
    [AttributeLogicalName("address1_country")]
    [DataMember]
    public string Address1_Country
    {
        get
        {
            return GetAttributeValue<string>("address1_country");
        }
        set
        {
            SetAttributeValue("address1_country", value);
        }
    }

    /// <summary>
    /// County name for address 1.
    /// </summary>
    [AttributeLogicalName("address1_county")]
    [DataMember]
    public string Address1_County
    {
        get
        {
            return GetAttributeValue<string>("address1_county");
        }
        set
        {
            SetAttributeValue("address1_county", value);
        }
    }

    /// <summary>
    /// Fax number for address 1.
    /// </summary>
    [AttributeLogicalName("address1_fax")]
    [DataMember]
    public string Address1_Fax
    {
        get
        {
            return GetAttributeValue<string>("address1_fax");
        }
        set
        {
            SetAttributeValue("address1_fax", value);
        }
    }

    /// <summary>
    /// Latitude for address 1.
    /// </summary>
    [AttributeLogicalName("address1_latitude")]
    [DataMember]
    public double? Address1_Latitude
    {
        get
        {
            return GetAttributeValue<double?>("address1_latitude");
        }
        set
        {
            SetAttributeValue("address1_latitude", value);
        }
    }

    /// <summary>
    /// First line for entering address 1 information.
    /// </summary>
    [AttributeLogicalName("address1_line1")]
    [DataMember]
    public string Address1_Line1
    {
        get
        {
            return GetAttributeValue<string>("address1_line1");
        }
        set
        {
            SetAttributeValue("address1_line1", value);
        }
    }

    /// <summary>
    /// Second line for entering address 1 information.
    /// </summary>
    [AttributeLogicalName("address1_line2")]
    [DataMember]
    public string Address1_Line2
    {
        get
        {
            return GetAttributeValue<string>("address1_line2");
        }
        set
        {
            SetAttributeValue("address1_line2", value);
        }
    }

    /// <summary>
    /// Third line for entering address 1 information.
    /// </summary>
    [AttributeLogicalName("address1_line3")]
    [DataMember]
    public string Address1_Line3
    {
        get
        {
            return GetAttributeValue<string>("address1_line3");
        }
        set
        {
            SetAttributeValue("address1_line3", value);
        }
    }

    /// <summary>
    /// Longitude for address 1.
    /// </summary>
    [AttributeLogicalName("address1_longitude")]
    [DataMember]
    public double? Address1_Longitude
    {
        get
        {
            return GetAttributeValue<double?>("address1_longitude");
        }
        set
        {
            SetAttributeValue("address1_longitude", value);
        }
    }

    /// <summary>
    /// Name to enter for address 1.
    /// </summary>
    [AttributeLogicalName("address1_name")]
    [DataMember]
    public string Address1_Name
    {
        get
        {
            return GetAttributeValue<string>("address1_name");
        }
        set
        {
            SetAttributeValue("address1_name", value);
        }
    }

    /// <summary>
    /// ZIP Code or postal code for address 1.
    /// </summary>
    [AttributeLogicalName("address1_postalcode")]
    [DataMember]
    public string Address1_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("address1_postalcode");
        }
        set
        {
            SetAttributeValue("address1_postalcode", value);
        }
    }

    /// <summary>
    /// Post office box number for address 1.
    /// </summary>
    [AttributeLogicalName("address1_postofficebox")]
    [DataMember]
    public string Address1_PostOfficeBox
    {
        get
        {
            return GetAttributeValue<string>("address1_postofficebox");
        }
        set
        {
            SetAttributeValue("address1_postofficebox", value);
        }
    }

    /// <summary>
    /// Method of shipment for address 1.
    /// </summary>
    [AttributeLogicalName("address1_shippingmethodcode")]
    [DataMember]
    public OptionSetValue Address1_ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address1_shippingmethodcode");
        }
        set
        {
            SetAttributeValue("address1_shippingmethodcode", value);
        }
    }

    /// <summary>
    /// State or province for address 1.
    /// </summary>
    [AttributeLogicalName("address1_stateorprovince")]
    [DataMember]
    public string Address1_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("address1_stateorprovince");
        }
        set
        {
            SetAttributeValue("address1_stateorprovince", value);
        }
    }

    /// <summary>
    /// First telephone number associated with address 1.
    /// </summary>
    [AttributeLogicalName("address1_telephone1")]
    [DataMember]
    public string Address1_Telephone1
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone1");
        }
        set
        {
            SetAttributeValue("address1_telephone1", value);
        }
    }

    /// <summary>
    /// Second telephone number associated with address 1.
    /// </summary>
    [AttributeLogicalName("address1_telephone2")]
    [DataMember]
    public string Address1_Telephone2
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone2");
        }
        set
        {
            SetAttributeValue("address1_telephone2", value);
        }
    }

    /// <summary>
    /// Third telephone number associated with address 1.
    /// </summary>
    [AttributeLogicalName("address1_telephone3")]
    [DataMember]
    public string Address1_Telephone3
    {
        get
        {
            return GetAttributeValue<string>("address1_telephone3");
        }
        set
        {
            SetAttributeValue("address1_telephone3", value);
        }
    }

    /// <summary>
    /// United Parcel Service (UPS) zone for address 1.
    /// </summary>
    [AttributeLogicalName("address1_upszone")]
    [DataMember]
    public string Address1_UPSZone
    {
        get
        {
            return GetAttributeValue<string>("address1_upszone");
        }
        set
        {
            SetAttributeValue("address1_upszone", value);
        }
    }

    /// <summary>
    /// UTC offset for address 1. This is the difference between local time and standard Coordinated Universal Time.
    /// </summary>
    [AttributeLogicalName("address1_utcoffset")]
    [DataMember]
    public int? Address1_UTCOffset
    {
        get
        {
            return GetAttributeValue<int?>("address1_utcoffset");
        }
        set
        {
            SetAttributeValue("address1_utcoffset", value);
        }
    }

    /// <summary>
    /// Unique identifier for address 2.
    /// </summary>
    [AttributeLogicalName("address2_addressid")]
    [DataMember]
    public Guid? Address2_AddressId
    {
        get
        {
            return GetAttributeValue<Guid?>("address2_addressid");
        }
        set
        {
            SetAttributeValue("address2_addressid", value);
        }
    }

    /// <summary>
    /// Type of address for address 2, such as billing, shipping, or primary address.
    /// </summary>
    [AttributeLogicalName("address2_addresstypecode")]
    [DataMember]
    public OptionSetValue Address2_AddressTypeCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address2_addresstypecode");
        }
        set
        {
            SetAttributeValue("address2_addresstypecode", value);
        }
    }

    /// <summary>
    /// City name for address 2.
    /// </summary>
    [AttributeLogicalName("address2_city")]
    [DataMember]
    public string Address2_City
    {
        get
        {
            return GetAttributeValue<string>("address2_city");
        }
        set
        {
            SetAttributeValue("address2_city", value);
        }
    }

    /// <summary>
    /// Shows the complete secondary address.
    /// </summary>
    [AttributeLogicalName("address2_composite")]
    [DataMember]
    public string Address2_Composite
    {
        get
        {
            return GetAttributeValue<string>("address2_composite");
        }
    }

    /// <summary>
    /// Country/region name in address 2.
    /// </summary>
    [AttributeLogicalName("address2_country")]
    [DataMember]
    public string Address2_Country
    {
        get
        {
            return GetAttributeValue<string>("address2_country");
        }
        set
        {
            SetAttributeValue("address2_country", value);
        }
    }

    /// <summary>
    /// County name for address 2.
    /// </summary>
    [AttributeLogicalName("address2_county")]
    [DataMember]
    public string Address2_County
    {
        get
        {
            return GetAttributeValue<string>("address2_county");
        }
        set
        {
            SetAttributeValue("address2_county", value);
        }
    }

    /// <summary>
    /// Fax number for address 2.
    /// </summary>
    [AttributeLogicalName("address2_fax")]
    [DataMember]
    public string Address2_Fax
    {
        get
        {
            return GetAttributeValue<string>("address2_fax");
        }
        set
        {
            SetAttributeValue("address2_fax", value);
        }
    }

    /// <summary>
    /// Latitude for address 2.
    /// </summary>
    [AttributeLogicalName("address2_latitude")]
    [DataMember]
    public double? Address2_Latitude
    {
        get
        {
            return GetAttributeValue<double?>("address2_latitude");
        }
        set
        {
            SetAttributeValue("address2_latitude", value);
        }
    }

    /// <summary>
    /// First line for entering address 2 information.
    /// </summary>
    [AttributeLogicalName("address2_line1")]
    [DataMember]
    public string Address2_Line1
    {
        get
        {
            return GetAttributeValue<string>("address2_line1");
        }
        set
        {
            SetAttributeValue("address2_line1", value);
        }
    }

    /// <summary>
    /// Second line for entering address 2 information.
    /// </summary>
    [AttributeLogicalName("address2_line2")]
    [DataMember]
    public string Address2_Line2
    {
        get
        {
            return GetAttributeValue<string>("address2_line2");
        }
        set
        {
            SetAttributeValue("address2_line2", value);
        }
    }

    /// <summary>
    /// Third line for entering address 2 information.
    /// </summary>
    [AttributeLogicalName("address2_line3")]
    [DataMember]
    public string Address2_Line3
    {
        get
        {
            return GetAttributeValue<string>("address2_line3");
        }
        set
        {
            SetAttributeValue("address2_line3", value);
        }
    }

    /// <summary>
    /// Longitude for address 2.
    /// </summary>
    [AttributeLogicalName("address2_longitude")]
    [DataMember]
    public double? Address2_Longitude
    {
        get
        {
            return GetAttributeValue<double?>("address2_longitude");
        }
        set
        {
            SetAttributeValue("address2_longitude", value);
        }
    }

    /// <summary>
    /// Name to enter for address 2.
    /// </summary>
    [AttributeLogicalName("address2_name")]
    [DataMember]
    public string Address2_Name
    {
        get
        {
            return GetAttributeValue<string>("address2_name");
        }
        set
        {
            SetAttributeValue("address2_name", value);
        }
    }

    /// <summary>
    /// ZIP Code or postal code for address 2.
    /// </summary>
    [AttributeLogicalName("address2_postalcode")]
    [DataMember]
    public string Address2_PostalCode
    {
        get
        {
            return GetAttributeValue<string>("address2_postalcode");
        }
        set
        {
            SetAttributeValue("address2_postalcode", value);
        }
    }

    /// <summary>
    /// Post office box number for address 2.
    /// </summary>
    [AttributeLogicalName("address2_postofficebox")]
    [DataMember]
    public string Address2_PostOfficeBox
    {
        get
        {
            return GetAttributeValue<string>("address2_postofficebox");
        }
        set
        {
            SetAttributeValue("address2_postofficebox", value);
        }
    }

    /// <summary>
    /// Method of shipment for address 2.
    /// </summary>
    [AttributeLogicalName("address2_shippingmethodcode")]
    [DataMember]
    public OptionSetValue Address2_ShippingMethodCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("address2_shippingmethodcode");
        }
        set
        {
            SetAttributeValue("address2_shippingmethodcode", value);
        }
    }

    /// <summary>
    /// State or province for address 2.
    /// </summary>
    [AttributeLogicalName("address2_stateorprovince")]
    [DataMember]
    public string Address2_StateOrProvince
    {
        get
        {
            return GetAttributeValue<string>("address2_stateorprovince");
        }
        set
        {
            SetAttributeValue("address2_stateorprovince", value);
        }
    }

    /// <summary>
    /// First telephone number associated with address 2.
    /// </summary>
    [AttributeLogicalName("address2_telephone1")]
    [DataMember]
    public string Address2_Telephone1
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone1");
        }
        set
        {
            SetAttributeValue("address2_telephone1", value);
        }
    }

    /// <summary>
    /// Second telephone number associated with address 2.
    /// </summary>
    [AttributeLogicalName("address2_telephone2")]
    [DataMember]
    public string Address2_Telephone2
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone2");
        }
        set
        {
            SetAttributeValue("address2_telephone2", value);
        }
    }

    /// <summary>
    /// Third telephone number associated with address 2.
    /// </summary>
    [AttributeLogicalName("address2_telephone3")]
    [DataMember]
    public string Address2_Telephone3
    {
        get
        {
            return GetAttributeValue<string>("address2_telephone3");
        }
        set
        {
            SetAttributeValue("address2_telephone3", value);
        }
    }

    /// <summary>
    /// United Parcel Service (UPS) zone for address 2.
    /// </summary>
    [AttributeLogicalName("address2_upszone")]
    [DataMember]
    public string Address2_UPSZone
    {
        get
        {
            return GetAttributeValue<string>("address2_upszone");
        }
        set
        {
            SetAttributeValue("address2_upszone", value);
        }
    }

    /// <summary>
    /// UTC offset for address 2. This is the difference between local time and standard Coordinated Universal Time.
    /// </summary>
    [AttributeLogicalName("address2_utcoffset")]
    [DataMember]
    public int? Address2_UTCOffset
    {
        get
        {
            return GetAttributeValue<int?>("address2_utcoffset");
        }
        set
        {
            SetAttributeValue("address2_utcoffset", value);
        }
    }

    /// <summary>
    /// The identifier for the application. This is used to access data in another application.
    /// </summary>
    [AttributeLogicalName("applicationid")]
    [DataMember]
    public Guid? ApplicationId
    {
        get
        {
            return GetAttributeValue<Guid?>("applicationid");
        }
        set
        {
            SetAttributeValue("applicationid", value);
        }
    }

    /// <summary>
    /// The URI used as a unique logical identifier for the external app. This can be used to validate the application.
    /// </summary>
    [AttributeLogicalName("applicationiduri")]
    [DataMember]
    public string ApplicationIdUri
    {
        get
        {
            return GetAttributeValue<string>("applicationiduri");
        }
    }

    /// <summary>
    /// This is the application directory object Id.
    /// </summary>
    [AttributeLogicalName("azureactivedirectoryobjectid")]
    [DataMember]
    public Guid? AzureActiveDirectoryObjectId
    {
        get
        {
            return GetAttributeValue<Guid?>("azureactivedirectoryobjectid");
        }
    }

    /// <summary>
    /// Unique identifier of the business unit with which the user is associated.
    /// </summary>
    [AttributeLogicalName("businessunitid")]
    [DataMember]
    public EntityReference BusinessUnitId
    {
        get
        {
            return GetAttributeValue<EntityReference>("businessunitid");
        }
        set
        {
            SetAttributeValue("businessunitid", value);
        }
    }

    /// <summary>
    /// Fiscal calendar associated with the user.
    /// </summary>
    [AttributeLogicalName("calendarid")]
    [DataMember]
    public EntityReference CalendarId
    {
        get
        {
            return GetAttributeValue<EntityReference>("calendarid");
        }
        set
        {
            SetAttributeValue("calendarid", value);
        }
    }

    /// <summary>
    /// License type of user.
    /// </summary>
    [AttributeLogicalName("caltype")]
    [DataMember]
    public OptionSetValue CALType
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("caltype");
        }
        set
        {
            SetAttributeValue("caltype", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who created the user.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the user was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the systemuser.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Check to add user to TG
    /// </summary>
    [AttributeLogicalName("ctas_add_to_tg")]
    [DataMember]
    public bool? ctas_Add_to_TG
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_add_to_tg");
        }
        set
        {
            SetAttributeValue("ctas_add_to_tg", value);
        }
    }

    /// <summary>
    /// This confirms the email for the TG's have been sent
    /// </summary>
    [AttributeLogicalName("ctas_added_tg_email_sent")]
    [DataMember]
    public bool? ctas_Added_TG_Email_Sent
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_added_tg_email_sent");
        }
        set
        {
            SetAttributeValue("ctas_added_tg_email_sent", value);
        }
    }

    /// <summary>
    /// Administrative Assistant 1 for Directors.
    /// </summary>
    [AttributeLogicalName("ctas_admin_assistant_dir1")]
    [DataMember]
    public EntityReference ctas_Admin_Assistant_Dir1
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_admin_assistant_dir1");
        }
        set
        {
            SetAttributeValue("ctas_admin_assistant_dir1", value);
        }
    }

    /// <summary>
    /// Administrative Assistant2 for Directors.
    /// </summary>
    [AttributeLogicalName("ctas_admin_assistant_dir2")]
    [DataMember]
    public EntityReference ctas_Admin_Assistant_Dir2
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_admin_assistant_dir2");
        }
        set
        {
            SetAttributeValue("ctas_admin_assistant_dir2", value);
        }
    }

    /// <summary>
    /// Administrative Assistant for Sr VP
    /// </summary>
    [AttributeLogicalName("ctas_admin_assistant_sr_vp")]
    [DataMember]
    public EntityReference ctas_Admin_Assistant_Sr_VP
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_admin_assistant_sr_vp");
        }
        set
        {
            SetAttributeValue("ctas_admin_assistant_sr_vp", value);
        }
    }

    /// <summary>
    /// Administrative Assistant for VP's
    /// </summary>
    [AttributeLogicalName("ctas_admin_assistant_vp")]
    [DataMember]
    public EntityReference ctas_Admin_Assistant_VP
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_admin_assistant_vp");
        }
        set
        {
            SetAttributeValue("ctas_admin_assistant_vp", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_admin_assistant2_sr_vp")]
    [DataMember]
    public EntityReference ctas_Admin_Assistant2_Sr_VP
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_admin_assistant2_sr_vp");
        }
        set
        {
            SetAttributeValue("ctas_admin_assistant2_sr_vp", value);
        }
    }

    /// <summary>
    /// Administrative Assistant 2 for VP's
    /// </summary>
    [AttributeLogicalName("ctas_admin_assistant2_vp")]
    [DataMember]
    public EntityReference ctas_Admin_Assistant2_VP
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_admin_assistant2_vp");
        }
        set
        {
            SetAttributeValue("ctas_admin_assistant2_vp", value);
        }
    }

    /// <summary>
    /// This is a checkbox which can be used by the Admin's to select a specific user for training.
    /// </summary>
    [AttributeLogicalName("ctas_admin_view_mode")]
    [DataMember]
    public bool? ctas_Admin_View_Mode
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_admin_view_mode");
        }
        set
        {
            SetAttributeValue("ctas_admin_view_mode", value);
        }
    }

    /// <summary>
    /// This will be used to give administrative assistant the same access as a manager
    /// </summary>
    [AttributeLogicalName("ctas_assistant_1")]
    [DataMember]
    public EntityReference ctas_Assistant_1
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_assistant_1");
        }
        set
        {
            SetAttributeValue("ctas_assistant_1", value);
        }
    }

    /// <summary>
    /// This will allow Administrative assistant to have access to the same thing the manager sees.
    /// </summary>
    [AttributeLogicalName("ctas_assistant_2")]
    [DataMember]
    public EntityReference ctas_Assistant_2
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_assistant_2");
        }
        set
        {
            SetAttributeValue("ctas_assistant_2", value);
        }
    }

    /// <summary>
    /// Unique identifier which defines which business division the user belongs too.
    /// </summary>
    [AttributeLogicalName("ctas_business_division_id")]
    [DataMember]
    public int? ctas_Business_Division_ID
    {
        get
        {
            return GetAttributeValue<int?>("ctas_business_division_id");
        }
        set
        {
            SetAttributeValue("ctas_business_division_id", value);
        }
    }

    /// <summary>
    /// Cintergy Legacy ID
    /// </summary>
    [AttributeLogicalName("ctas_cintergy_user_id")]
    [DataMember]
    public int? ctas_Cintergy_User_ID
    {
        get
        {
            return GetAttributeValue<int?>("ctas_cintergy_user_id");
        }
        set
        {
            SetAttributeValue("ctas_cintergy_user_id", value);
        }
    }

    /// <summary>
    /// Used for reporting
    /// </summary>
    [AttributeLogicalName("ctas_director_for_reporting")]
    [DataMember]
    public EntityReference ctas_Director_for_Reporting
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_director_for_reporting");
        }
        set
        {
            SetAttributeValue("ctas_director_for_reporting", value);
        }
    }

    /// <summary>
    /// This value comes from E-Reg and determines if the user has access to Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_dynamics_access_flag")]
    [DataMember]
    public bool? ctas_Dynamics_Access_Flag
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_dynamics_access_flag");
        }
        set
        {
            SetAttributeValue("ctas_dynamics_access_flag", value);
        }
    }

    /// <summary>
    /// EBM Lookup
    /// </summary>
    [AttributeLogicalName("ctas_ebm")]
    [DataMember]
    public EntityReference ctas_EBM
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_ebm");
        }
        set
        {
            SetAttributeValue("ctas_ebm", value);
        }
    }

    /// <summary>
    /// This title will be used in emails sent out by the reps.
    /// </summary>
    [AttributeLogicalName("ctas_email_title")]
    [DataMember]
    public string ctas_Email_Title
    {
        get
        {
            return GetAttributeValue<string>("ctas_email_title");
        }
        set
        {
            SetAttributeValue("ctas_email_title", value);
        }
    }

    /// <summary>
    /// Exclude from Reporting- This will be used to exclude users from certain reports. Reports: Weekly Activity Reports
    /// </summary>
    [AttributeLogicalName("ctas_exclude_from_reporting")]
    [DataMember]
    public bool? ctas_Exclude_from_Reporting
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_exclude_from_reporting");
        }
        set
        {
            SetAttributeValue("ctas_exclude_from_reporting", value);
        }
    }

    /// <summary>
    /// Hybrid Business Division ID
    /// </summary>
    [AttributeLogicalName("ctas_hybrid_bus_div_id")]
    [DataMember]
    public int? ctas_Hybrid_Bus_Div_Id
    {
        get
        {
            return GetAttributeValue<int?>("ctas_hybrid_bus_div_id");
        }
        set
        {
            SetAttributeValue("ctas_hybrid_bus_div_id", value);
        }
    }

    /// <summary>
    /// Stores the secondary business division the rep covers
    /// </summary>
    [AttributeLogicalName("ctas_hybrid_business_division")]
    [DataMember]
    public EntityReference ctas_Hybrid_Business_Division
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_hybrid_business_division");
        }
        set
        {
            SetAttributeValue("ctas_hybrid_business_division", value);
        }
    }

    /// <summary>
    /// If the Rep has more than one Division then this box will be checked Yes
    /// </summary>
    [AttributeLogicalName("ctas_hybrid_rep")]
    [DataMember]
    public bool? ctas_Hybrid_Rep
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_hybrid_rep");
        }
        set
        {
            SetAttributeValue("ctas_hybrid_rep", value);
        }
    }

    /// <summary>
    /// Cintas Location Market
    /// </summary>
    [AttributeLogicalName("ctas_loc_market")]
    [DataMember]
    public string ctas_Loc_Market
    {
        get
        {
            return GetAttributeValue<string>("ctas_loc_market");
        }
        set
        {
            SetAttributeValue("ctas_loc_market", value);
        }
    }

    /// <summary>
    /// This will be used to allow a secondary manager access to what the managers sees.
    /// </summary>
    [AttributeLogicalName("ctas_manager_2")]
    [DataMember]
    public EntityReference ctas_Manager_2
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_manager_2");
        }
        set
        {
            SetAttributeValue("ctas_manager_2", value);
        }
    }

    /// <summary>
    /// Secondary Manager for the Director.
    /// </summary>
    [AttributeLogicalName("ctas_manager_director")]
    [DataMember]
    public EntityReference ctas_Manager_Director
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_manager_director");
        }
        set
        {
            SetAttributeValue("ctas_manager_director", value);
        }
    }

    /// <summary>
    /// Used for Reporting
    /// </summary>
    [AttributeLogicalName("ctas_manager_for_reporting")]
    [DataMember]
    public EntityReference ctas_Manager_for_Reporting
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_manager_for_reporting");
        }
        set
        {
            SetAttributeValue("ctas_manager_for_reporting", value);
        }
    }

    /// <summary>
    /// Additional Manager for a Rep
    /// </summary>
    [AttributeLogicalName("ctas_manager_rep")]
    [DataMember]
    public EntityReference ctas_Manager_Rep
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_manager_rep");
        }
        set
        {
            SetAttributeValue("ctas_manager_rep", value);
        }
    }

    /// <summary>
    /// Secondary Manger for Sr VP
    /// </summary>
    [AttributeLogicalName("ctas_manager_sr_vp")]
    [DataMember]
    public EntityReference ctas_Manager_Sr_VP
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_manager_sr_vp");
        }
        set
        {
            SetAttributeValue("ctas_manager_sr_vp", value);
        }
    }

    /// <summary>
    /// Secondary VP.
    /// </summary>
    [AttributeLogicalName("ctas_manager_vp")]
    [DataMember]
    public EntityReference ctas_Manager_VP
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_manager_vp");
        }
        set
        {
            SetAttributeValue("ctas_manager_vp", value);
        }
    }

    /// <summary>
    /// This will be used to allow a secondary NAC to have access to the system.
    /// </summary>
    [AttributeLogicalName("ctas_nacsalessupportid2")]
    [DataMember]
    public EntityReference ctas_nacsalessupportid2
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_nacsalessupportid2");
        }
        set
        {
            SetAttributeValue("ctas_nacsalessupportid2", value);
        }
    }

    /// <summary>
    /// User has requested to Opt Out of Outlook Sync
    /// </summary>
    [AttributeLogicalName("ctas_opt_out_ol_sync")]
    [DataMember]
    public bool? ctas_Opt_Out_Ol_Sync
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_opt_out_ol_sync");
        }
        set
        {
            SetAttributeValue("ctas_opt_out_ol_sync", value);
        }
    }

    /// <summary>
    /// List of Partner Roles.
    /// </summary>
    [AttributeLogicalName("ctas_partner_role")]
    [DataMember]
    public OptionSetValue ctas_Partner_Role
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_partner_role");
        }
        set
        {
            SetAttributeValue("ctas_partner_role", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("ctas_partner_type")]
    [DataMember]
    public OptionSetValue ctas_Partner_Type
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_partner_type");
        }
        set
        {
            SetAttributeValue("ctas_partner_type", value);
        }
    }

    /// <summary>
    /// Sr Vice President for Reporting
    /// </summary>
    [AttributeLogicalName("ctas_sr_vice_president_for_reporting")]
    [DataMember]
    public EntityReference ctas_Sr_Vice_President_for_Reporting
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_sr_vice_president_for_reporting");
        }
        set
        {
            SetAttributeValue("ctas_sr_vice_president_for_reporting", value);
        }
    }

    /// <summary>
    /// This is a temporary field which will be used to assign users to teams for the initial rollout. Once E-reg is setup and verified working this should be deleted.
    /// </summary>
    [AttributeLogicalName("ctas_team_check")]
    [DataMember]
    public bool? ctas_Team_Check
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_team_check");
        }
        set
        {
            SetAttributeValue("ctas_team_check", value);
        }
    }

    /// <summary>
    /// Team Lookup
    /// </summary>
    [AttributeLogicalName("ctas_team_lu")]
    [DataMember]
    public EntityReference ctas_Team_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_team_lu");
        }
        set
        {
            SetAttributeValue("ctas_team_lu", value);
        }
    }

    /// <summary>
    /// Used for testing
    /// </summary>
    [AttributeLogicalName("ctas_temp_field")]
    [DataMember]
    public bool? ctas_Temp_Field
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_temp_field");
        }
        set
        {
            SetAttributeValue("ctas_temp_field", value);
        }
    }

    /// <summary>
    /// Used for intimal load of users into the system.
    /// </summary>
    [AttributeLogicalName("ctas_temp_manager")]
    [DataMember]
    public string ctas_Temp_Manager
    {
        get
        {
            return GetAttributeValue<string>("ctas_temp_manager");
        }
        set
        {
            SetAttributeValue("ctas_temp_manager", value);
        }
    }

    /// <summary>
    /// used to update NACs into for initial load into the system.
    /// </summary>
    [AttributeLogicalName("ctas_temp_nac")]
    [DataMember]
    public string ctas_Temp_NAC
    {
        get
        {
            return GetAttributeValue<string>("ctas_temp_nac");
        }
        set
        {
            SetAttributeValue("ctas_temp_nac", value);
        }
    }

    /// <summary>
    /// User Group
    /// </summary>
    [AttributeLogicalName("ctas_user_group")]
    [DataMember]
    public OptionSetValue ctas_User_Group
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("ctas_user_group");
        }
        set
        {
            SetAttributeValue("ctas_user_group", value);
        }
    }

    /// <summary>
    /// Lookup to User Reporting Table
    /// </summary>
    [AttributeLogicalName("ctas_user_reporting")]
    [DataMember]
    public EntityReference ctas_User_Reporting
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_user_reporting");
        }
        set
        {
            SetAttributeValue("ctas_user_reporting", value);
        }
    }

    /// <summary>
    /// Used for reporting
    /// </summary>
    [AttributeLogicalName("ctas_vice_president_for_reporting")]
    [DataMember]
    public EntityReference ctas_Vice_President_for_Reporting
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_vice_president_for_reporting");
        }
        set
        {
            SetAttributeValue("ctas_vice_president_for_reporting", value);
        }
    }

    /// <summary>
    /// Zone Leader for Cross Sell
    /// </summary>
    [AttributeLogicalName("ctas_zone_leader")]
    [DataMember]
    public EntityReference ctas_Zone_Leader
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_zone_leader");
        }
        set
        {
            SetAttributeValue("ctas_zone_leader", value);
        }
    }

    /// <summary>
    /// Zone Name
    /// </summary>
    [AttributeLogicalName("ctas_zone_name")]
    [DataMember]
    public string ctas_Zone_Name
    {
        get
        {
            return GetAttributeValue<string>("ctas_zone_name");
        }
        set
        {
            SetAttributeValue("ctas_zone_name", value);
        }
    }

    /// <summary>
    /// Zone Number = MSA
    /// </summary>
    [AttributeLogicalName("ctas_zone_number")]
    [DataMember]
    public int? ctas_Zone_Number
    {
        get
        {
            return GetAttributeValue<int?>("ctas_zone_number");
        }
        set
        {
            SetAttributeValue("ctas_zone_number", value);
        }
    }

    /// <summary>
    /// Indicates if default outlook filters have been populated.
    /// </summary>
    [AttributeLogicalName("defaultfilterspopulated")]
    [DataMember]
    public bool? DefaultFiltersPopulated
    {
        get
        {
            return GetAttributeValue<bool?>("defaultfilterspopulated");
        }
    }

    /// <summary>
    /// Select the mailbox associated with this user.
    /// </summary>
    [AttributeLogicalName("defaultmailbox")]
    [DataMember]
    public EntityReference DefaultMailbox
    {
        get
        {
            return GetAttributeValue<EntityReference>("defaultmailbox");
        }
    }

    /// <summary>
    /// Type a default folder name for the user's OneDrive For Business location.
    /// </summary>
    [AttributeLogicalName("defaultodbfoldername")]
    [DataMember]
    public string DefaultOdbFolderName
    {
        get
        {
            return GetAttributeValue<string>("defaultodbfoldername");
        }
    }

    /// <summary>
    /// Reason for disabling the user.
    /// </summary>
    [AttributeLogicalName("disabledreason")]
    [DataMember]
    public string DisabledReason
    {
        get
        {
            return GetAttributeValue<string>("disabledreason");
        }
    }

    /// <summary>
    /// Whether to display the user in service views.
    /// </summary>
    [AttributeLogicalName("displayinserviceviews")]
    [DataMember]
    public bool? DisplayInServiceViews
    {
        get
        {
            return GetAttributeValue<bool?>("displayinserviceviews");
        }
        set
        {
            SetAttributeValue("displayinserviceviews", value);
        }
    }

    /// <summary>
    /// Active Directory domain of which the user is a member.
    /// </summary>
    [AttributeLogicalName("domainname")]
    [DataMember]
    public string DomainName
    {
        get
        {
            return GetAttributeValue<string>("domainname");
        }
        set
        {
            SetAttributeValue("domainname", value);
        }
    }

    /// <summary>
    /// Shows the status of the primary email address.
    /// </summary>
    [AttributeLogicalName("emailrouteraccessapproval")]
    [DataMember]
    public OptionSetValue EmailRouterAccessApproval
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("emailrouteraccessapproval");
        }
        set
        {
            SetAttributeValue("emailrouteraccessapproval", value);
        }
    }

    /// <summary>
    /// Employee identifier for the user.
    /// </summary>
    [AttributeLogicalName("employeeid")]
    [DataMember]
    public string EmployeeId
    {
        get
        {
            return GetAttributeValue<string>("employeeid");
        }
        set
        {
            SetAttributeValue("employeeid", value);
        }
    }

    /// <summary>
    /// Shows the default image for the record.
    /// </summary>
    [AttributeLogicalName("entityimage")]
    [DataMember]
    public byte[] EntityImage
    {
        get
        {
            return GetAttributeValue<byte[]>("entityimage");
        }
        set
        {
            SetAttributeValue("entityimage", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_timestamp")]
    [DataMember]
    public long? EntityImage_Timestamp
    {
        get
        {
            return GetAttributeValue<long?>("entityimage_timestamp");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("entityimage_url")]
    [DataMember]
    public string EntityImage_URL
    {
        get
        {
            return GetAttributeValue<string>("entityimage_url");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("entityimageid")]
    [DataMember]
    public Guid? EntityImageId
    {
        get
        {
            return GetAttributeValue<Guid?>("entityimageid");
        }
    }

    /// <summary>
    /// Exchange rate for the currency associated with the systemuser with respect to the base currency.
    /// </summary>
    [AttributeLogicalName("exchangerate")]
    [DataMember]
    public decimal? ExchangeRate
    {
        get
        {
            return GetAttributeValue<decimal?>("exchangerate");
        }
    }

    /// <summary>
    /// First name of the user.
    /// </summary>
    [AttributeLogicalName("firstname")]
    [DataMember]
    public string FirstName
    {
        get
        {
            return GetAttributeValue<string>("firstname");
        }
        set
        {
            SetAttributeValue("firstname", value);
        }
    }

    /// <summary>
    /// Full name of the user.
    /// </summary>
    [AttributeLogicalName("fullname")]
    [DataMember]
    public string FullName
    {
        get
        {
            return GetAttributeValue<string>("fullname");
        }
    }

    /// <summary>
    /// Government identifier for the user.
    /// </summary>
    [AttributeLogicalName("governmentid")]
    [DataMember]
    public string GovernmentId
    {
        get
        {
            return GetAttributeValue<string>("governmentid");
        }
        set
        {
            SetAttributeValue("governmentid", value);
        }
    }

    /// <summary>
    /// Home phone number for the user.
    /// </summary>
    [AttributeLogicalName("homephone")]
    [DataMember]
    public string HomePhone
    {
        get
        {
            return GetAttributeValue<string>("homephone");
        }
        set
        {
            SetAttributeValue("homephone", value);
        }
    }

    /// <summary>
    /// Unique identifier of the data import or data migration that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Incoming email delivery method for the user.
    /// </summary>
    [AttributeLogicalName("incomingemaildeliverymethod")]
    [DataMember]
    public OptionSetValue IncomingEmailDeliveryMethod
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("incomingemaildeliverymethod");
        }
        set
        {
            SetAttributeValue("incomingemaildeliverymethod", value);
        }
    }

    /// <summary>
    /// Internal email address for the user.
    /// </summary>
    [AttributeLogicalName("internalemailaddress")]
    [DataMember]
    public string InternalEMailAddress
    {
        get
        {
            return GetAttributeValue<string>("internalemailaddress");
        }
        set
        {
            SetAttributeValue("internalemailaddress", value);
        }
    }

    /// <summary>
    /// User invitation status.
    /// </summary>
    [AttributeLogicalName("invitestatuscode")]
    [DataMember]
    public OptionSetValue InviteStatusCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("invitestatuscode");
        }
        set
        {
            SetAttributeValue("invitestatuscode", value);
        }
    }

    /// <summary>
    /// Information about whether the user is enabled.
    /// </summary>
    [AttributeLogicalName("isdisabled")]
    [DataMember]
    public bool? IsDisabled
    {
        get
        {
            return GetAttributeValue<bool?>("isdisabled");
        }
    }

    /// <summary>
    /// Shows the status of approval of the email address by O365 Admin.
    /// </summary>
    [AttributeLogicalName("isemailaddressapprovedbyo365admin")]
    [DataMember]
    public bool? IsEmailAddressApprovedByO365Admin
    {
        get
        {
            return GetAttributeValue<bool?>("isemailaddressapprovedbyo365admin");
        }
    }

    /// <summary>
    /// Check if user is an integration user.
    /// </summary>
    [AttributeLogicalName("isintegrationuser")]
    [DataMember]
    public bool? IsIntegrationUser
    {
        get
        {
            return GetAttributeValue<bool?>("isintegrationuser");
        }
        set
        {
            SetAttributeValue("isintegrationuser", value);
        }
    }

    /// <summary>
    /// Information about whether the user is licensed.
    /// </summary>
    [AttributeLogicalName("islicensed")]
    [DataMember]
    public bool? IsLicensed
    {
        get
        {
            return GetAttributeValue<bool?>("islicensed");
        }
        set
        {
            SetAttributeValue("islicensed", value);
        }
    }

    /// <summary>
    /// Information about whether the user is synced with the directory.
    /// </summary>
    [AttributeLogicalName("issyncwithdirectory")]
    [DataMember]
    public bool? IsSyncWithDirectory
    {
        get
        {
            return GetAttributeValue<bool?>("issyncwithdirectory");
        }
        set
        {
            SetAttributeValue("issyncwithdirectory", value);
        }
    }

    /// <summary>
    /// Job title of the user.
    /// </summary>
    [AttributeLogicalName("jobtitle")]
    [DataMember]
    public string JobTitle
    {
        get
        {
            return GetAttributeValue<string>("jobtitle");
        }
        set
        {
            SetAttributeValue("jobtitle", value);
        }
    }

    /// <summary>
    /// Last name of the user.
    /// </summary>
    [AttributeLogicalName("lastname")]
    [DataMember]
    public string LastName
    {
        get
        {
            return GetAttributeValue<string>("lastname");
        }
        set
        {
            SetAttributeValue("lastname", value);
        }
    }

    /// <summary>
    /// Middle name of the user.
    /// </summary>
    [AttributeLogicalName("middlename")]
    [DataMember]
    public string MiddleName
    {
        get
        {
            return GetAttributeValue<string>("middlename");
        }
        set
        {
            SetAttributeValue("middlename", value);
        }
    }

    /// <summary>
    /// Mobile alert email address for the user.
    /// </summary>
    [AttributeLogicalName("mobilealertemail")]
    [DataMember]
    public string MobileAlertEMail
    {
        get
        {
            return GetAttributeValue<string>("mobilealertemail");
        }
        set
        {
            SetAttributeValue("mobilealertemail", value);
        }
    }

    /// <summary>
    /// Items contained with a particular SystemUser.
    /// </summary>
    [AttributeLogicalName("mobileofflineprofileid")]
    [DataMember]
    public EntityReference MobileOfflineProfileId
    {
        get
        {
            return GetAttributeValue<EntityReference>("mobileofflineprofileid");
        }
        set
        {
            SetAttributeValue("mobileofflineprofileid", value);
        }
    }

    /// <summary>
    /// Mobile phone number for the user.
    /// </summary>
    [AttributeLogicalName("mobilephone")]
    [DataMember]
    public string MobilePhone
    {
        get
        {
            return GetAttributeValue<string>("mobilephone");
        }
        set
        {
            SetAttributeValue("mobilephone", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who last modified the user.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the user was last modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who last modified the systemuser.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("new_emaillogon")]
    [DataMember]
    public string new_emaillogon
    {
        get
        {
            return GetAttributeValue<string>("new_emaillogon");
        }
        set
        {
            SetAttributeValue("new_emaillogon", value);
        }
    }

    /// <summary>
    /// Nickname of the user.
    /// </summary>
    [AttributeLogicalName("nickname")]
    [DataMember]
    public string NickName
    {
        get
        {
            return GetAttributeValue<string>("nickname");
        }
        set
        {
            SetAttributeValue("nickname", value);
        }
    }

    /// <summary>
    /// Unique identifier of the organization associated with the user.
    /// </summary>
    [AttributeLogicalName("organizationid")]
    [DataMember]
    public Guid? OrganizationId
    {
        get
        {
            return GetAttributeValue<Guid?>("organizationid");
        }
    }

    /// <summary>
    /// Outgoing email delivery method for the user.
    /// </summary>
    [AttributeLogicalName("outgoingemaildeliverymethod")]
    [DataMember]
    public OptionSetValue OutgoingEmailDeliveryMethod
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("outgoingemaildeliverymethod");
        }
        set
        {
            SetAttributeValue("outgoingemaildeliverymethod", value);
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Unique identifier of the manager of the user.
    /// </summary>
    [AttributeLogicalName("parentsystemuserid")]
    [DataMember]
    public EntityReference ParentSystemUserId
    {
        get
        {
            return GetAttributeValue<EntityReference>("parentsystemuserid");
        }
        set
        {
            SetAttributeValue("parentsystemuserid", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("passporthi")]
    [DataMember]
    public int? PassportHi
    {
        get
        {
            return GetAttributeValue<int?>("passporthi");
        }
        set
        {
            SetAttributeValue("passporthi", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("passportlo")]
    [DataMember]
    public int? PassportLo
    {
        get
        {
            return GetAttributeValue<int?>("passportlo");
        }
        set
        {
            SetAttributeValue("passportlo", value);
        }
    }

    /// <summary>
    /// Personal email address of the user.
    /// </summary>
    [AttributeLogicalName("personalemailaddress")]
    [DataMember]
    public string PersonalEMailAddress
    {
        get
        {
            return GetAttributeValue<string>("personalemailaddress");
        }
        set
        {
            SetAttributeValue("personalemailaddress", value);
        }
    }

    /// <summary>
    /// URL for the Website on which a photo of the user is located.
    /// </summary>
    [AttributeLogicalName("photourl")]
    [DataMember]
    public string PhotoUrl
    {
        get
        {
            return GetAttributeValue<string>("photourl");
        }
        set
        {
            SetAttributeValue("photourl", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessdivisionid")]
    [DataMember]
    public EntityReference po_BusinessDivisionid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessdivisionid");
        }
        set
        {
            SetAttributeValue("po_businessdivisionid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessstructure")]
    [DataMember]
    public EntityReference po_BusinessStructure
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessstructure");
        }
        set
        {
            SetAttributeValue("po_businessstructure", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cintastlocationid")]
    [DataMember]
    public EntityReference po_CintastLocationid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_cintastlocationid");
        }
        set
        {
            SetAttributeValue("po_cintastlocationid", value);
        }
    }

    /// <summary>
    /// Enter Cintas for Cintas Partner or Vendor Name eg: Power Objects
    /// </summary>
    [AttributeLogicalName("po_company_name")]
    [DataMember]
    public string po_Company_Name
    {
        get
        {
            return GetAttributeValue<string>("po_company_name");
        }
        set
        {
            SetAttributeValue("po_company_name", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_hiredate")]
    [DataMember]
    public DateTime? po_HireDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_hiredate");
        }
        set
        {
            SetAttributeValue("po_hiredate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_hiredatecurrent")]
    [DataMember]
    public DateTime? po_HireDateCurrent
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_hiredatecurrent");
        }
        set
        {
            SetAttributeValue("po_hiredatecurrent", value);
        }
    }

    /// <summary>
    /// MailChimp lists which user can see.
    /// </summary>
    [AttributeLogicalName("po_mailchimplists")]
    [DataMember]
    public string po_mailchimplists
    {
        get
        {
            return GetAttributeValue<string>("po_mailchimplists");
        }
        set
        {
            SetAttributeValue("po_mailchimplists", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_nacsalessupportid")]
    [DataMember]
    public EntityReference po_NACSalesSupportid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_nacsalessupportid");
        }
        set
        {
            SetAttributeValue("po_nacsalessupportid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_partnerid")]
    [DataMember]
    public string po_PartnerID
    {
        get
        {
            return GetAttributeValue<string>("po_partnerid");
        }
        set
        {
            SetAttributeValue("po_partnerid", value);
        }
    }

    /// <summary>
    /// Name of the folders for email templates.
    /// </summary>
    [AttributeLogicalName("po_templatefolders")]
    [DataMember]
    public string po_templatefolders
    {
        get
        {
            return GetAttributeValue<string>("po_templatefolders");
        }
        set
        {
            SetAttributeValue("po_templatefolders", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_terminationdate")]
    [DataMember]
    public DateTime? po_TerminationDate
    {
        get
        {
            return GetAttributeValue<DateTime?>("po_terminationdate");
        }
        set
        {
            SetAttributeValue("po_terminationdate", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_vendor_account")]
    [DataMember]
    public bool? po_Vendor_Account
    {
        get
        {
            return GetAttributeValue<bool?>("po_vendor_account");
        }
        set
        {
            SetAttributeValue("po_vendor_account", value);
        }
    }

    /// <summary>
    /// User's position in hierarchical security model.
    /// </summary>
    [AttributeLogicalName("positionid")]
    [DataMember]
    public EntityReference PositionId
    {
        get
        {
            return GetAttributeValue<EntityReference>("positionid");
        }
        set
        {
            SetAttributeValue("positionid", value);
        }
    }

    /// <summary>
    /// Preferred address for the user.
    /// </summary>
    [AttributeLogicalName("preferredaddresscode")]
    [DataMember]
    public OptionSetValue PreferredAddressCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredaddresscode");
        }
        set
        {
            SetAttributeValue("preferredaddresscode", value);
        }
    }

    /// <summary>
    /// Preferred email address for the user.
    /// </summary>
    [AttributeLogicalName("preferredemailcode")]
    [DataMember]
    public OptionSetValue PreferredEmailCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredemailcode");
        }
        set
        {
            SetAttributeValue("preferredemailcode", value);
        }
    }

    /// <summary>
    /// Preferred phone number for the user.
    /// </summary>
    [AttributeLogicalName("preferredphonecode")]
    [DataMember]
    public OptionSetValue PreferredPhoneCode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("preferredphonecode");
        }
        set
        {
            SetAttributeValue("preferredphonecode", value);
        }
    }

    /// <summary>
    /// Shows the ID of the process.
    /// </summary>
    [AttributeLogicalName("processid")]
    [DataMember]
    public Guid? ProcessId
    {
        get
        {
            return GetAttributeValue<Guid?>("processid");
        }
        set
        {
            SetAttributeValue("processid", value);
        }
    }

    /// <summary>
    /// Unique identifier of the default queue for the user.
    /// </summary>
    [AttributeLogicalName("queueid")]
    [DataMember]
    public EntityReference QueueId
    {
        get
        {
            return GetAttributeValue<EntityReference>("queueid");
        }
        set
        {
            SetAttributeValue("queueid", value);
        }
    }

    /// <summary>
    /// Salutation for correspondence with the user.
    /// </summary>
    [AttributeLogicalName("salutation")]
    [DataMember]
    public string Salutation
    {
        get
        {
            return GetAttributeValue<string>("salutation");
        }
        set
        {
            SetAttributeValue("salutation", value);
        }
    }

    /// <summary>
    /// Check if user is a setup user.
    /// </summary>
    [AttributeLogicalName("setupuser")]
    [DataMember]
    public bool? SetupUser
    {
        get
        {
            return GetAttributeValue<bool?>("setupuser");
        }
        set
        {
            SetAttributeValue("setupuser", value);
        }
    }

    /// <summary>
    /// SharePoint Work Email Address
    /// </summary>
    [AttributeLogicalName("sharepointemailaddress")]
    [DataMember]
    public string SharePointEmailAddress
    {
        get
        {
            return GetAttributeValue<string>("sharepointemailaddress");
        }
        set
        {
            SetAttributeValue("sharepointemailaddress", value);
        }
    }

    /// <summary>
    /// Site at which the user is located.
    /// </summary>
    [AttributeLogicalName("siteid")]
    [DataMember]
    public EntityReference SiteId
    {
        get
        {
            return GetAttributeValue<EntityReference>("siteid");
        }
        set
        {
            SetAttributeValue("siteid", value);
        }
    }

    /// <summary>
    /// Skill set of the user.
    /// </summary>
    [AttributeLogicalName("skills")]
    [DataMember]
    public string Skills
    {
        get
        {
            return GetAttributeValue<string>("skills");
        }
        set
        {
            SetAttributeValue("skills", value);
        }
    }

    /// <summary>
    /// Shows the ID of the stage.
    /// </summary>
    [AttributeLogicalName("stageid")]
    [DataMember]
    public Guid? StageId
    {
        get
        {
            return GetAttributeValue<Guid?>("stageid");
        }
        set
        {
            SetAttributeValue("stageid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the user.
    /// </summary>
    [AttributeLogicalName("systemuserid")]
    [DataMember]
    public Guid? SystemUserId
    {
        get
        {
            return GetAttributeValue<Guid?>("systemuserid");
        }
        set
        {
            SetAttributeValue("systemuserid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("systemuserid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            SystemUserId = value;
        }
    }

    /// <summary>
    /// Unique identifier of the territory to which the user is assigned.
    /// </summary>
    [AttributeLogicalName("territoryid")]
    [DataMember]
    public EntityReference TerritoryId
    {
        get
        {
            return GetAttributeValue<EntityReference>("territoryid");
        }
        set
        {
            SetAttributeValue("territoryid", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Title of the user.
    /// </summary>
    [AttributeLogicalName("title")]
    [DataMember]
    public string Title
    {
        get
        {
            return GetAttributeValue<string>("title");
        }
        set
        {
            SetAttributeValue("title", value);
        }
    }

    /// <summary>
    /// Unique identifier of the currency associated with the systemuser.
    /// </summary>
    [AttributeLogicalName("transactioncurrencyid")]
    [DataMember]
    public EntityReference TransactionCurrencyId
    {
        get
        {
            return GetAttributeValue<EntityReference>("transactioncurrencyid");
        }
        set
        {
            SetAttributeValue("transactioncurrencyid", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("traversedpath")]
    [DataMember]
    public string TraversedPath
    {
        get
        {
            return GetAttributeValue<string>("traversedpath");
        }
        set
        {
            SetAttributeValue("traversedpath", value);
        }
    }

    /// <summary>
    /// Shows the type of user license.
    /// </summary>
    [AttributeLogicalName("userlicensetype")]
    [DataMember]
    public int? UserLicenseType
    {
        get
        {
            return GetAttributeValue<int?>("userlicensetype");
        }
        set
        {
            SetAttributeValue("userlicensetype", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// Version number of the user.
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }

    /// <summary>
    /// Windows Live ID
    /// </summary>
    [AttributeLogicalName("windowsliveid")]
    [DataMember]
    public string WindowsLiveID
    {
        get
        {
            return GetAttributeValue<string>("windowsliveid");
        }
        set
        {
            SetAttributeValue("windowsliveid", value);
        }
    }

    /// <summary>
    /// User's Yammer login email address
    /// </summary>
    [AttributeLogicalName("yammeremailaddress")]
    [DataMember]
    public string YammerEmailAddress
    {
        get
        {
            return GetAttributeValue<string>("yammeremailaddress");
        }
        set
        {
            SetAttributeValue("yammeremailaddress", value);
        }
    }

    /// <summary>
    /// User's Yammer ID
    /// </summary>
    [AttributeLogicalName("yammeruserid")]
    [DataMember]
    public string YammerUserId
    {
        get
        {
            return GetAttributeValue<string>("yammeruserid");
        }
        set
        {
            SetAttributeValue("yammeruserid", value);
        }
    }

    /// <summary>
    /// Pronunciation of the first name of the user, written in phonetic hiragana or katakana characters.
    /// </summary>
    [AttributeLogicalName("yomifirstname")]
    [DataMember]
    public string YomiFirstName
    {
        get
        {
            return GetAttributeValue<string>("yomifirstname");
        }
        set
        {
            SetAttributeValue("yomifirstname", value);
        }
    }

    /// <summary>
    /// Pronunciation of the full name of the user, written in phonetic hiragana or katakana characters.
    /// </summary>
    [AttributeLogicalName("yomifullname")]
    [DataMember]
    public string YomiFullName
    {
        get
        {
            return GetAttributeValue<string>("yomifullname");
        }
    }

    /// <summary>
    /// Pronunciation of the last name of the user, written in phonetic hiragana or katakana characters.
    /// </summary>
    [AttributeLogicalName("yomilastname")]
    [DataMember]
    public string YomiLastName
    {
        get
        {
            return GetAttributeValue<string>("yomilastname");
        }
        set
        {
            SetAttributeValue("yomilastname", value);
        }
    }

    /// <summary>
    /// Pronunciation of the middle name of the user, written in phonetic hiragana or katakana characters.
    /// </summary>
    [AttributeLogicalName("yomimiddlename")]
    [DataMember]
    public string YomiMiddleName
    {
        get
        {
            return GetAttributeValue<string>("yomimiddlename");
        }
        set
        {
            SetAttributeValue("yomimiddlename", value);
        }
    }
}