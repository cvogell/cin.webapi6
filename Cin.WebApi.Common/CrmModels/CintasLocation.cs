﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Runtime.Serialization;

namespace Cin.WebApi.CrmModels;

[DataContract]
[EntityLogicalName("po_cintaslocation")]
public class CintasLocation : Entity
{
    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [AttributeLogicalName("createdby")]
    [DataMember]
    public EntityReference CreatedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [AttributeLogicalName("createdon")]
    [DataMember]
    public DateTime? CreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [AttributeLogicalName("createdonbehalfby")]
    [DataMember]
    public EntityReference CreatedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Business Division Field 1
    /// </summary>
    [AttributeLogicalName("ctas_business_division")]
    [DataMember]
    public EntityReference ctas_Business_Division
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_business_division");
        }
        set
        {
            SetAttributeValue("ctas_business_division", value);
        }
    }

    /// <summary>
    /// Business Division 2
    /// </summary>
    [AttributeLogicalName("ctas_business_division_2")]
    [DataMember]
    public EntityReference ctas_Business_Division_2
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_business_division_2");
        }
        set
        {
            SetAttributeValue("ctas_business_division_2", value);
        }
    }

    /// <summary>
    /// Count of Deep Clean Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_deep_clean_reps")]
    [DataMember]
    public int? ctas_Count_of_Deep_Clean_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_deep_clean_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_deep_clean_reps", value);
        }
    }

    /// <summary>
    /// Count of FAE's at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_faes")]
    [DataMember]
    public int? ctas_Count_of_FAEs
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_faes");
        }
        set
        {
            SetAttributeValue("ctas_count_of_faes", value);
        }
    }

    /// <summary>
    /// Count of FAS Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_fas_reps")]
    [DataMember]
    public int? ctas_Count_of_FAS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_fas_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_fas_reps", value);
        }
    }

    /// <summary>
    /// Count of FP Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_fp_reps")]
    [DataMember]
    public int? ctas_Count_of_FP_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_fp_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_fp_reps", value);
        }
    }

    /// <summary>
    /// Count of FPS Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_fps_reps")]
    [DataMember]
    public int? ctas_Count_of_FPS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_fps_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_fps_reps", value);
        }
    }

    /// <summary>
    /// Count of FS Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_fs_reps")]
    [DataMember]
    public int? ctas_Count_of_FS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_fs_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_fs_reps", value);
        }
    }

    /// <summary>
    /// Count of SRIT Deep Clean Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_srit_deep_clean_reps")]
    [DataMember]
    public int? ctas_Count_of_SRIT_Deep_Clean_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_srit_deep_clean_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_srit_deep_clean_reps", value);
        }
    }

    /// <summary>
    /// Count of SRIT FAS Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_srit_fas_reps")]
    [DataMember]
    public int? ctas_Count_of_SRIT_FAS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_srit_fas_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_srit_fas_reps", value);
        }
    }

    /// <summary>
    /// Count of SRIT FP Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_srit_fp_reps")]
    [DataMember]
    public int? ctas_Count_of_SRIT_FP_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_srit_fp_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_srit_fp_reps", value);
        }
    }

    /// <summary>
    /// Count of SRIT FPS Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_srit_fps_reps")]
    [DataMember]
    public int? ctas_Count_of_SRIT_FPS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_srit_fps_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_srit_fps_reps", value);
        }
    }

    /// <summary>
    /// Count of SRIT FS Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_srit_fs_reps")]
    [DataMember]
    public int? ctas_Count_of_SRIT_FS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_srit_fs_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_srit_fs_reps", value);
        }
    }

    /// <summary>
    /// Count of SRIT UR Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_srit_ur_reps")]
    [DataMember]
    public int? ctas_Count_of_SRIT_UR_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_srit_ur_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_srit_ur_reps", value);
        }
    }

    /// <summary>
    /// Count of SSR Routes at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_ssr_routes")]
    [DataMember]
    public int? ctas_Count_of_SSR_Routes
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_ssr_routes");
        }
        set
        {
            SetAttributeValue("ctas_count_of_ssr_routes", value);
        }
    }

    /// <summary>
    /// Count of UR Reps at the location
    /// </summary>
    [AttributeLogicalName("ctas_count_of_ur_reps")]
    [DataMember]
    public int? ctas_Count_of_UR_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_count_of_ur_reps");
        }
        set
        {
            SetAttributeValue("ctas_count_of_ur_reps", value);
        }
    }

    /// <summary>
    /// Enterprise Business Manager
    /// </summary>
    [AttributeLogicalName("ctas_ebm_lu")]
    [DataMember]
    public EntityReference ctas_EBM_LU
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_ebm_lu");
        }
        set
        {
            SetAttributeValue("ctas_ebm_lu", value);
        }
    }

    /// <summary>
    /// Flag to exclude locations from weekly reporting.
    /// </summary>
    [AttributeLogicalName("ctas_exclude_reporting")]
    [DataMember]
    public bool? ctas_Exclude_Reporting
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_exclude_reporting");
        }
        set
        {
            SetAttributeValue("ctas_exclude_reporting", value);
        }
    }

    /// <summary>
    /// General Manager at the location. Note: this is a text field because the GM is not a user in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_general_manager")]
    [DataMember]
    public string ctas_General_Manager
    {
        get
        {
            return GetAttributeValue<string>("ctas_general_manager");
        }
        set
        {
            SetAttributeValue("ctas_general_manager", value);
        }
    }

    /// <summary>
    /// Indicates that this is a G&K Acquisition location
    /// </summary>
    [AttributeLogicalName("ctas_gk_loc")]
    [DataMember]
    public bool? ctas_GK_Loc
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_gk_loc");
        }
        set
        {
            SetAttributeValue("ctas_gk_loc", value);
        }
    }

    /// <summary>
    /// The main phone number to be used for Location Finder
    /// </summary>
    [AttributeLogicalName("ctas_main_phone")]
    [DataMember]
    public string ctas_Main_Phone
    {
        get
        {
            return GetAttributeValue<string>("ctas_main_phone");
        }
        set
        {
            SetAttributeValue("ctas_main_phone", value);
        }
    }

    /// <summary>
    /// Phone Deep Clean
    /// </summary>
    [AttributeLogicalName("ctas_phone_dc")]
    [DataMember]
    public string ctas_Phone_DC
    {
        get
        {
            return GetAttributeValue<string>("ctas_phone_dc");
        }
        set
        {
            SetAttributeValue("ctas_phone_dc", value);
        }
    }

    /// <summary>
    /// Phone First Aid & Safety
    /// </summary>
    [AttributeLogicalName("ctas_phone_fas")]
    [DataMember]
    public string ctas_Phone_FAS
    {
        get
        {
            return GetAttributeValue<string>("ctas_phone_fas");
        }
        set
        {
            SetAttributeValue("ctas_phone_fas", value);
        }
    }

    /// <summary>
    /// Phone Fire Protection
    /// </summary>
    [AttributeLogicalName("ctas_phone_fp")]
    [DataMember]
    public string ctas_Phone_FP
    {
        get
        {
            return GetAttributeValue<string>("ctas_phone_fp");
        }
        set
        {
            SetAttributeValue("ctas_phone_fp", value);
        }
    }

    /// <summary>
    /// Phone Fire Protection Specialist
    /// </summary>
    [AttributeLogicalName("ctas_phone_fps")]
    [DataMember]
    public string ctas_Phone_FPS
    {
        get
        {
            return GetAttributeValue<string>("ctas_phone_fps");
        }
        set
        {
            SetAttributeValue("ctas_phone_fps", value);
        }
    }

    /// <summary>
    /// Phone Facility Services
    /// </summary>
    [AttributeLogicalName("ctas_phone_fs")]
    [DataMember]
    public string ctas_Phone_FS
    {
        get
        {
            return GetAttributeValue<string>("ctas_phone_fs");
        }
        set
        {
            SetAttributeValue("ctas_phone_fs", value);
        }
    }

    /// <summary>
    /// Master Location Phone Number
    /// </summary>
    [AttributeLogicalName("ctas_phone_master")]
    [DataMember]
    public string ctas_Phone_Master
    {
        get
        {
            return GetAttributeValue<string>("ctas_phone_master");
        }
        set
        {
            SetAttributeValue("ctas_phone_master", value);
        }
    }

    /// <summary>
    /// Phone Uniform Rental
    /// </summary>
    [AttributeLogicalName("ctas_phone_ur")]
    [DataMember]
    public string ctas_Phone_UR
    {
        get
        {
            return GetAttributeValue<string>("ctas_phone_ur");
        }
        set
        {
            SetAttributeValue("ctas_phone_ur", value);
        }
    }

    /// <summary>
    /// Projected Count of Deep Clean Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_deep_clean_reps")]
    [DataMember]
    public int? ctas_Proj_Cnt_of_Deep_Clean_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_deep_clean_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_deep_clean_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of FAE's
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_faes")]
    [DataMember]
    public int? ctas_Proj_Cnt_of_FAEs
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_faes");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_faes", value);
        }
    }

    /// <summary>
    /// Projected Count of FAS Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_fas_reps")]
    [DataMember]
    public int? ctas_Proj_Cnt_of_FAS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_fas_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_fas_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of FP Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_fp_reps")]
    [DataMember]
    public int? ctas_Proj_Cnt_of_FP_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_fp_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_fp_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of FPS Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_fps_reps")]
    [DataMember]
    public int? ctas_Proj_Cnt_of_FPS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_fps_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_fps_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of FS Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_fs_reps")]
    [DataMember]
    public int? ctas_Proj_Cnt_of_FS_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_fs_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_fs_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of MDR Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_mdr_reps")]
    [DataMember]
    public int? ctas_proj_cnt_of_mdr_reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_mdr_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_mdr_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of SRIT DC Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_srit_dc_reps")]
    [DataMember]
    public int? ctas_proj_cnt_of_srit_dc_reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_srit_dc_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_srit_dc_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of SRIT FAS Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_srit_fas_reps")]
    [DataMember]
    public int? ctas_proj_cnt_of_srit_fas_reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_srit_fas_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_srit_fas_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of SRIT FP Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_srit_fp_reps")]
    [DataMember]
    public int? ctas_proj_cnt_of_srit_fp_reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_srit_fp_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_srit_fp_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of SRIT FPS Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_srit_fps_reps")]
    [DataMember]
    public int? ctas_proj_cnt_of_srit_fps_reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_srit_fps_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_srit_fps_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of SRIT FS Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_srit_fs_reps")]
    [DataMember]
    public int? ctas_proj_cnt_of_srit_fs_reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_srit_fs_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_srit_fs_reps", value);
        }
    }

    /// <summary>
    /// Projected Count SRIT UR Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_srit_ur_reps")]
    [DataMember]
    public int? ctas_proj_cnt_of_srit_ur_reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_srit_ur_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_srit_ur_reps", value);
        }
    }

    /// <summary>
    /// Projected Count of UR Reps
    /// </summary>
    [AttributeLogicalName("ctas_proj_cnt_of_ur_reps")]
    [DataMember]
    public int? ctas_Proj_Cnt_of_UR_Reps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_proj_cnt_of_ur_reps");
        }
        set
        {
            SetAttributeValue("ctas_proj_cnt_of_ur_reps", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_deep_clean")]
    [DataMember]
    public int? ctas_Rep_Cnt_Deep_Clean
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_deep_clean");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_deep_clean", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_fae")]
    [DataMember]
    public int? ctas_Rep_Cnt_FAE
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_fae");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_fae", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_fas")]
    [DataMember]
    public int? ctas_Rep_Cnt_FAS
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_fas");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_fas", value);
        }
    }

    /// <summary>
    /// Total Count of Reps in Dynamics for FAS
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_fas_total")]
    [DataMember]
    public int? ctas_rep_cnt_fas_total
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_fas_total");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_fas_total", value);
        }
    }

    /// <summary>
    /// Total Count of Reps in Dynamics for Fire
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_fire_total")]
    [DataMember]
    public int? ctas_rep_cnt_fire_total
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_fire_total");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_fire_total", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_fp")]
    [DataMember]
    public int? ctas_Rep_Cnt_FP
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_fp");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_fp", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_fps")]
    [DataMember]
    public int? ctas_Rep_Cnt_FPS
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_fps");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_fps", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_fs")]
    [DataMember]
    public int? ctas_Rep_Cnt_FS
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_fs");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_fs", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_mdr_fs")]
    [DataMember]
    public int? ctas_rep_cnt_mdr_fs
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_mdr_fs");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_mdr_fs", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_mdr_ur")]
    [DataMember]
    public int? ctas_rep_cnt_mdr_ur
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_mdr_ur");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_mdr_ur", value);
        }
    }

    /// <summary>
    /// Total Count of Reps in Dynamics for Rental
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_ren_total")]
    [DataMember]
    public int? ctas_rep_cnt_ren_total
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_ren_total");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_ren_total", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_srit_deep_clean")]
    [DataMember]
    public int? ctas_rep_cnt_srit_deep_clean
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_srit_deep_clean");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_srit_deep_clean", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_srit_fas")]
    [DataMember]
    public int? ctas_rep_cnt_srit_fas
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_srit_fas");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_srit_fas", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_srit_fp")]
    [DataMember]
    public int? ctas_rep_cnt_srit_fp
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_srit_fp");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_srit_fp", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_srit_fps")]
    [DataMember]
    public int? ctas_rep_cnt_srit_fps
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_srit_fps");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_srit_fps", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_srit_fs")]
    [DataMember]
    public int? ctas_rep_cnt_srit_fs
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_srit_fs");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_srit_fs", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_srit_ur")]
    [DataMember]
    public int? ctas_rep_cnt_srit_ur
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_srit_ur");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_srit_ur", value);
        }
    }

    /// <summary>
    /// Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_rep_cnt_ur")]
    [DataMember]
    public int? ctas_Rep_Cnt_UR
    {
        get
        {
            return GetAttributeValue<int?>("ctas_rep_cnt_ur");
        }
        set
        {
            SetAttributeValue("ctas_rep_cnt_ur", value);
        }
    }

    /// <summary>
    /// Service Flag Deep Clean
    /// </summary>
    [AttributeLogicalName("ctas_serv_flag_dc")]
    [DataMember]
    public bool? ctas_Serv_Flag_DC
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_serv_flag_dc");
        }
        set
        {
            SetAttributeValue("ctas_serv_flag_dc", value);
        }
    }

    /// <summary>
    /// Service Flag First Aid & Safety
    /// </summary>
    [AttributeLogicalName("ctas_serv_flag_fas")]
    [DataMember]
    public bool? ctas_Serv_Flag_FAS
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_serv_flag_fas");
        }
        set
        {
            SetAttributeValue("ctas_serv_flag_fas", value);
        }
    }

    /// <summary>
    /// Service Flag Fire Protection
    /// </summary>
    [AttributeLogicalName("ctas_serv_flag_fp")]
    [DataMember]
    public bool? ctas_Serv_Flag_FP
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_serv_flag_fp");
        }
        set
        {
            SetAttributeValue("ctas_serv_flag_fp", value);
        }
    }

    /// <summary>
    /// Service Flag Fire Protection Specialist
    /// </summary>
    [AttributeLogicalName("ctas_serv_flag_fps")]
    [DataMember]
    public bool? ctas_Serv_Flag_FPS
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_serv_flag_fps");
        }
        set
        {
            SetAttributeValue("ctas_serv_flag_fps", value);
        }
    }

    /// <summary>
    /// Service Flag Facility Services
    /// </summary>
    [AttributeLogicalName("ctas_serv_flag_fs")]
    [DataMember]
    public bool? ctas_Serv_Flag_FS
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_serv_flag_fs");
        }
        set
        {
            SetAttributeValue("ctas_serv_flag_fs", value);
        }
    }

    /// <summary>
    /// Service Flag Uniform Rental
    /// </summary>
    [AttributeLogicalName("ctas_serv_flag_ur")]
    [DataMember]
    public bool? ctas_Serv_Flag_UR
    {
        get
        {
            return GetAttributeValue<bool?>("ctas_serv_flag_ur");
        }
        set
        {
            SetAttributeValue("ctas_serv_flag_ur", value);
        }
    }

    /// <summary>
    /// Total Count of Reps in Dynamics
    /// </summary>
    [AttributeLogicalName("ctas_x_test_count")]
    [DataMember]
    public int? ctas_X_Test_Count
    {
        get
        {
            return GetAttributeValue<int?>("ctas_x_test_count");
        }
    }

    /// <summary>
    /// Last Updated time of rollup field x-Test Count.
    /// </summary>
    [AttributeLogicalName("ctas_x_test_count_date")]
    [DataMember]
    public DateTime? ctas_X_Test_Count_Date
    {
        get
        {
            return GetAttributeValue<DateTime?>("ctas_x_test_count_date");
        }
    }

    /// <summary>
    /// State of rollup field x-Test Count.
    /// </summary>
    [AttributeLogicalName("ctas_x_test_count_state")]
    [DataMember]
    public int? ctas_X_Test_Count_State
    {
        get
        {
            return GetAttributeValue<int?>("ctas_x_test_count_state");
        }
    }

    /// <summary>
    /// Lookup to User Entity. This is the partner who is in charge of the zone
    /// </summary>
    [AttributeLogicalName("ctas_zone_leader")]
    [DataMember]
    public EntityReference ctas_Zone_Leader
    {
        get
        {
            return GetAttributeValue<EntityReference>("ctas_zone_leader");
        }
        set
        {
            SetAttributeValue("ctas_zone_leader", value);
        }
    }

    /// <summary>
    /// Zone Name
    /// </summary>
    [AttributeLogicalName("ctas_zone_name")]
    [DataMember]
    public string ctas_Zone_Name
    {
        get
        {
            return GetAttributeValue<string>("ctas_zone_name");
        }
        set
        {
            SetAttributeValue("ctas_zone_name", value);
        }
    }

    /// <summary>
    /// Zone Number for Cross Sell
    /// </summary>
    [AttributeLogicalName("ctas_zone_num")]
    [DataMember]
    public int? ctas_Zone_Num
    {
        get
        {
            return GetAttributeValue<int?>("ctas_zone_num");
        }
        set
        {
            SetAttributeValue("ctas_zone_num", value);
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [AttributeLogicalName("importsequencenumber")]
    [DataMember]
    public int? ImportSequenceNumber
    {
        get
        {
            return GetAttributeValue<int?>("importsequencenumber");
        }
        set
        {
            SetAttributeValue("importsequencenumber", value);
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedby")]
    [DataMember]
    public EntityReference ModifiedBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [AttributeLogicalName("modifiedon")]
    [DataMember]
    public DateTime? ModifiedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [AttributeLogicalName("modifiedonbehalfby")]
    [DataMember]
    public EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return GetAttributeValue<EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [AttributeLogicalName("overriddencreatedon")]
    [DataMember]
    public DateTime? OverriddenCreatedOn
    {
        get
        {
            return GetAttributeValue<DateTime?>("overriddencreatedon");
        }
        set
        {
            SetAttributeValue("overriddencreatedon", value);
        }
    }

    /// <summary>
    /// Owner Id
    /// </summary>
    [AttributeLogicalName("ownerid")]
    [DataMember]
    public EntityReference OwnerId
    {
        get
        {
            return GetAttributeValue<EntityReference>("ownerid");
        }
        set
        {
            SetAttributeValue("ownerid", value);
        }
    }

    /// <summary>
    /// Unique identifier for the business unit that owns the record
    /// </summary>
    [AttributeLogicalName("owningbusinessunit")]
    [DataMember]
    public EntityReference OwningBusinessUnit
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier for the team that owns the record.
    /// </summary>
    [AttributeLogicalName("owningteam")]
    [DataMember]
    public EntityReference OwningTeam
    {
        get
        {
            return GetAttributeValue<EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier for the user that owns the record.
    /// </summary>
    [AttributeLogicalName("owninguser")]
    [DataMember]
    public EntityReference OwningUser
    {
        get
        {
            return GetAttributeValue<EntityReference>("owninguser");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_businessdivisionid")]
    [DataMember]
    public EntityReference po_BusinessDivisionid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_businessdivisionid");
        }
        set
        {
            SetAttributeValue("po_businessdivisionid", value);
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [AttributeLogicalName("po_cintaslocation")]
    [DataMember]
    public string po_cintaslocation1
    {
        get
        {
            return GetAttributeValue<string>("po_cintaslocation");
        }
        set
        {
            SetAttributeValue("po_cintaslocation", value);
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [AttributeLogicalName("po_cintaslocationid")]
    [DataMember]
    public Guid? po_cintaslocationId
    {
        get
        {
            return GetAttributeValue<Guid?>("po_cintaslocationid");
        }
        set
        {
            SetAttributeValue("po_cintaslocationid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = Guid.Empty;
            }
        }
    }

    [AttributeLogicalName("po_cintaslocationid")]
    [DataMember]
    public override Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            po_cintaslocationId = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_cintergylocationname")]
    [DataMember]
    public string po_CintergyLocationName
    {
        get
        {
            return GetAttributeValue<string>("po_cintergylocationname");
        }
        set
        {
            SetAttributeValue("po_cintergylocationname", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_city")]
    [DataMember]
    public string po_City
    {
        get
        {
            return GetAttributeValue<string>("po_city");
        }
        set
        {
            SetAttributeValue("po_city", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_country")]
    [DataMember]
    public string po_Country
    {
        get
        {
            return GetAttributeValue<string>("po_country");
        }
        set
        {
            SetAttributeValue("po_country", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_district")]
    [DataMember]
    public string po_District
    {
        get
        {
            return GetAttributeValue<string>("po_district");
        }
        set
        {
            SetAttributeValue("po_district", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_generalnotes_cc")]
    [DataMember]
    public string po_GeneralNotes_CC
    {
        get
        {
            return GetAttributeValue<string>("po_generalnotes_cc");
        }
        set
        {
            SetAttributeValue("po_generalnotes_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_locationaddressline1")]
    [DataMember]
    public string po_LocationAddressLine1
    {
        get
        {
            return GetAttributeValue<string>("po_locationaddressline1");
        }
        set
        {
            SetAttributeValue("po_locationaddressline1", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_locationaddressline2")]
    [DataMember]
    public string po_LocationAddressLine2
    {
        get
        {
            return GetAttributeValue<string>("po_locationaddressline2");
        }
        set
        {
            SetAttributeValue("po_locationaddressline2", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_locationid")]
    [DataMember]
    public int? po_locationID
    {
        get
        {
            return GetAttributeValue<int?>("po_locationid");
        }
        set
        {
            SetAttributeValue("po_locationid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_locationname")]
    [DataMember]
    public string po_LocationName
    {
        get
        {
            return GetAttributeValue<string>("po_locationname");
        }
        set
        {
            SetAttributeValue("po_locationname", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_marketid")]
    [DataMember]
    public int? po_MarketID
    {
        get
        {
            return GetAttributeValue<int?>("po_marketid");
        }
        set
        {
            SetAttributeValue("po_marketid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_marketname_ctas_loc")]
    [DataMember]
    public string po_MarketName_Ctas_loc
    {
        get
        {
            return GetAttributeValue<string>("po_marketname_ctas_loc");
        }
        set
        {
            SetAttributeValue("po_marketname_ctas_loc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_officeemail")]
    [DataMember]
    public string po_OfficeEmail
    {
        get
        {
            return GetAttributeValue<string>("po_officeemail");
        }
        set
        {
            SetAttributeValue("po_officeemail", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_officefax")]
    [DataMember]
    public string po_OfficeFax
    {
        get
        {
            return GetAttributeValue<string>("po_officefax");
        }
        set
        {
            SetAttributeValue("po_officefax", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_officephone")]
    [DataMember]
    public string po_OfficePhone
    {
        get
        {
            return GetAttributeValue<string>("po_officephone");
        }
        set
        {
            SetAttributeValue("po_officephone", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone1_cc")]
    [DataMember]
    public string po_Phone1_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone1_cc");
        }
        set
        {
            SetAttributeValue("po_phone1_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone1ext_cc")]
    [DataMember]
    public string po_Phone1Ext_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone1ext_cc");
        }
        set
        {
            SetAttributeValue("po_phone1ext_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone1notes_cc")]
    [DataMember]
    public string po_Phone1Notes_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone1notes_cc");
        }
        set
        {
            SetAttributeValue("po_phone1notes_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone2_cc")]
    [DataMember]
    public string po_Phone2_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone2_cc");
        }
        set
        {
            SetAttributeValue("po_phone2_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone2ext_cc")]
    [DataMember]
    public string po_Phone2Ext_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone2ext_cc");
        }
        set
        {
            SetAttributeValue("po_phone2ext_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone2notes_cc")]
    [DataMember]
    public string po_Phone2Notes_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone2notes_cc");
        }
        set
        {
            SetAttributeValue("po_phone2notes_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone3_cc")]
    [DataMember]
    public string po_Phone3_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone3_cc");
        }
        set
        {
            SetAttributeValue("po_phone3_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone3ext_cc")]
    [DataMember]
    public string po_Phone3Ext_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone3ext_cc");
        }
        set
        {
            SetAttributeValue("po_phone3ext_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone3notes_cc")]
    [DataMember]
    public string po_Phone3Notes_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone3notes_cc");
        }
        set
        {
            SetAttributeValue("po_phone3notes_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone4_cc")]
    [DataMember]
    public string po_Phone4_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone4_cc");
        }
        set
        {
            SetAttributeValue("po_phone4_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone4ext_cc")]
    [DataMember]
    public string po_Phone4Ext_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone4ext_cc");
        }
        set
        {
            SetAttributeValue("po_phone4ext_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone4notes_cc")]
    [DataMember]
    public string po_Phone4Notes_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone4notes_cc");
        }
        set
        {
            SetAttributeValue("po_phone4notes_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone5_cc")]
    [DataMember]
    public string po_Phone5_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone5_cc");
        }
        set
        {
            SetAttributeValue("po_phone5_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone5ext_cc")]
    [DataMember]
    public string po_Phone5Ext_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone5ext_cc");
        }
        set
        {
            SetAttributeValue("po_phone5ext_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_phone5notes_cc")]
    [DataMember]
    public string po_Phone5Notes_CC
    {
        get
        {
            return GetAttributeValue<string>("po_phone5notes_cc");
        }
        set
        {
            SetAttributeValue("po_phone5notes_cc", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_postalcodeid")]
    [DataMember]
    public EntityReference po_PostalCodeid
    {
        get
        {
            return GetAttributeValue<EntityReference>("po_postalcodeid");
        }
        set
        {
            SetAttributeValue("po_postalcodeid", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_postalcodetext")]
    [DataMember]
    public string po_PostalCodeText
    {
        get
        {
            return GetAttributeValue<string>("po_postalcodetext");
        }
        set
        {
            SetAttributeValue("po_postalcodetext", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("po_state")]
    [DataMember]
    public string po_State
    {
        get
        {
            return GetAttributeValue<string>("po_state");
        }
        set
        {
            SetAttributeValue("po_state", value);
        }
    }

    /// <summary>
    /// Status of the Cintas Location
    /// </summary>
    [AttributeLogicalName("statecode")]
    [DataMember]
    public po_cintaslocationState? statecode
    {
        get
        {
            OptionSetValue optionSet = GetAttributeValue<OptionSetValue>("statecode");
            if (optionSet != null)
            {
                return (po_cintaslocationState)Enum.ToObject(typeof(po_cintaslocationState), optionSet.Value);
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                SetAttributeValue("statecode", null);
            }
            else
            {
                SetAttributeValue("statecode", new OptionSetValue((int)value));
            }
        }
    }

    /// <summary>
    /// Reason for the status of the Cintas Location
    /// </summary>
    [AttributeLogicalName("statuscode")]
    [DataMember]
    public OptionSetValue statuscode
    {
        get
        {
            return GetAttributeValue<OptionSetValue>("statuscode");
        }
        set
        {
            SetAttributeValue("statuscode", value);
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [AttributeLogicalName("timezoneruleversionnumber")]
    [DataMember]
    public int? TimeZoneRuleVersionNumber
    {
        get
        {
            return GetAttributeValue<int?>("timezoneruleversionnumber");
        }
        set
        {
            SetAttributeValue("timezoneruleversionnumber", value);
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [AttributeLogicalName("utcconversiontimezonecode")]
    [DataMember]
    public int? UTCConversionTimeZoneCode
    {
        get
        {
            return GetAttributeValue<int?>("utcconversiontimezonecode");
        }
        set
        {
            SetAttributeValue("utcconversiontimezonecode", value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeLogicalName("versionnumber")]
    [DataMember]
    public long? VersionNumber
    {
        get
        {
            return GetAttributeValue<long?>("versionnumber");
        }
    }
}

[DataContract]
public enum po_cintaslocationState
{

    [EnumMember()]
    Active = 0,

    [EnumMember()]
    Inactive = 1,
}