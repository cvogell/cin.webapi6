﻿using Microsoft.Extensions.Logging;

namespace Cin.WebApi;
public abstract class CommonBase
{
    protected readonly ILogger log;
    public CommonBase(ILogger logger)
    {
        log = logger;
    }
    #region logging

    protected void debug(string message)
    {
        log.LogDebug(message);
    }

    protected void info(string message)
    {
        log.LogInformation(message);
    }

    protected void warning(string message, Exception? ex = null)
    {
        log.LogWarning(ex, message);
    }
    protected void error(string message, Exception? ex = null)
    {
        log.LogError(ex, message);
    }

    protected void critical(string message, Exception? ex = null)
    {
        log.LogCritical(ex, message);
    }
    #endregion

}
