﻿#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace Cin.WebApi.Config;

public class CrmConfig : ConfigOptionsBase<CrmConfig>
{
    public string CRMODataBatchPath { get; set; }
    public string CRMODataFetchXMLPath { get; set; }
    public string CRMODataWhoAmIPath { get; set; }
    public string CRMODataStringMapPath { get; set; }
    public string CRMODataEntityPath { get; set; }
    public string CRMODataEntityReferenceString { get; set; }
    public string CRMODataCreateEntityUrl { get; set; }
    public string CRMODataUpdateEntityUrl { get; set; }
    public string NotificationEmailSubject { get; set; }
    public string CRMURL { get; set; }
    public string CRMServicePath { get; set; }
    public bool LogWhoIs { get; set; } = false;
    public string? TestBearer { get; set; } = null;

    protected override string SectionName => "CrmConfig";
}
