﻿#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace Cin.WebApi.Config;

public class AppConfig : ConfigOptionsBase<AppConfig>
{
    public string MetadataUrl { get; set; }
    public string ModelNamespace { get; set; }
    public string UseDeltas { get; set; } = "Y";
    public string DocumentStorageConnection { get; set; }
    public string TraceLogContainer { get; set; }
    public string TraceLogSource { get; set; }
    public string TraceLogLevel { get; set; } = "VERBOSE";
    public string Authority { get; set; }
    public string ClientId { get; set; }
    public string ClientSecret { get; set; }
    public string TenantId { get; set; }

    public int LastXDays{get;set;}
    public int NextXDays{get;set;}
    public string AzureAccountName{get;set;}
    public string AzureAccountKey{get;set;}
    public string ArchiveUrl{get;set;}
    public string ArchiveLogTableName{get;set;}
    public string CRMTestingUserToken{get;set;}

    protected override string SectionName => "AppConfig";
}
