﻿#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace Cin.WebApi.Config;

public class QueryConfig : ConfigOptionsBase<QueryConfig>
{
    public string ACCOUNTQUERYID { get; set; }
    public string ACTIVITYQUERYID { get; set; }
    public string AGREEMENTTERMQUERYID { get; set; }
    public string AGREEMENTTERMVARIATIONQUERYID { get; set; }
    public string APPOINTMENTQUERYID { get; set; }
    public string BUSINESSDIVISIONQUERYID { get; set; }
    public string CINTASAGREEMENTPROCESSQUERYID { get; set; }
    public string CINTASLOCATIONQUERYID { get; set; }
    public string COMPETITORFSQUERYID { get; set; }
    public string COMPETITORURQUERYID { get; set; }
    public string CONTACTQUERYID { get; set; }
    public string CUSTOMERDETAILSQUERYID { get; set; }
    public string LEADREFERRALQUERYID { get; set; }
    public string MARKASSOLDQUERYID { get; set; }
    public string NACPROCESSINGQUERYID { get; set; }
    public string OPPORTUNITYPRODUCTQUERYID { get; set; }
    public string OPPORTUNITYQUERYID { get; set; }
    public string POSTALCODEALIGNMENTQUERYID { get; set; }
    public string PO_NAICSCODEPRODUCTBUNDLEASSOCIATIONQUERYID { get; set; }
    public string PO_BUSINESSDIVISIONBUNDLEASSOCIATIONQUERYID { get; set; }
    public string PRODUCTASSOCIATIONQUERYID { get; set; }
    public string PRODUCTRELATIONSHIPQUERYID { get; set; }
    public string PRODUCTQUERYID { get; set; }
    public string PROPOSALSANDAGREEMENTSQUERYID { get; set; }
    public string QUOTEDETAILQUERYID { get; set; }
    public string QUOTEQUERYID { get; set; }
    public string GLOBALRENTALSETTINGQUERYID { get; set; }
    public string SYSTEMUSERQUERYID { get; set; }

    protected override string SectionName => "QueryConfig";
}
