﻿using Cin.WebApi.Extensions;
using Cin.WebApi.Models;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Xrm.Sdk.WebServiceClient;
using Microsoft.Xrm.Sdk;
using System.Net;
using System.ServiceModel.Security;
using System.ServiceModel;
using Cin.WebApi.Config;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.Xml;
using System.Text;
using Microsoft.PowerPlatform.Dataverse.Client;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.PowerPlatform.Dataverse.Client.Model;
using System.Diagnostics.Contracts;

namespace Cin.WebApi.Repos;

/// <summary>
/// All things pertaining to interacting with the CRM system should go into this class.
/// </summary>
public class CrmRepo: BaseRepo//, IDisposable
{
    /// <summary>
    /// The CRM configuration.
    /// </summary>
    private readonly CrmConfig config;
    /// <summary>
    /// Constructor - note that logging and other various methods are contained in the <see cref="BaseRepo"/> class.
    /// </summary>
    /// <remarks>
    /// This class is configured as a Transient service in the app, which is to say that whenever someone 
    /// makes a call to an API they get a new instance of it. 
    /// </remarks>
    /// <param name="logger"></param>
    /// <param name="configuration"></param>
    /// <param name="memoryCache"></param>
    public CrmRepo(ILogger<CrmRepo> logger, IConfiguration configuration, IMemoryCache memoryCache):
        base(logger, configuration, memoryCache)
    {
        config = CrmConfig.Construct(configuration);
        Configuration = configuration;
        CrmPath = new Uri(config.CRMURL + config.CRMServicePath);
        InitClient();
    }

    protected void InitClient()
    {
        Client =
            new OrganizationWebProxyClient(CrmPath,
            new TimeSpan(0, 4, 0), false);
        
    }

    protected Uri CrmPath { get; set; }

    /// <summary>
    /// The proxy client, instantiated in the <see cref="WhoAmI(HttpRequest, WhoAmIRequest)"/> call.
    /// </summary>
    public OrganizationWebProxyClient Client { get; set; }

    /// <summary>
    /// The global configuration object instance.
    /// </summary>
    public IConfiguration Configuration { get; set; }

    /// <summary>
    /// The user's authentication token. This is pulled from the <see cref="WhoAmI(HttpRequest, WhoAmIRequest)"/> method.
    /// </summary>
    public string Token { get; set; }
    public CommonResponse RetrieveUserQuery(HttpRequest request, string entityName)
    {
        CommonResponse response = new CommonResponse();

        string message = string.Empty;

        try
        {
            QueryRequest queryRequest = new QueryRequest();
            //string entityName = Request.RequestUri.AbsolutePath.Replace(Constants.odataRemoval, "").ToUpper();
            string configKey = "QueryConfig:" + entityName.ToUpper() + Constants.queryId;
            queryRequest.QueryId = Configuration[configKey];
            info($"QueryId:{queryRequest.QueryId}");

            var queryId = new Guid(queryRequest.QueryId);

            if (request.Headers.ContainsKey(Constants.userId))
            {
                queryRequest.UserId = request.Headers.FirstOrDefault(x => x.Key.ToUpper() == Constants.userId).Value.FirstOrDefault();

                if (queryRequest.UserId.Length > 0)
                {
                    var callingUser = new EntityReference(Constants.systemUser, new Guid(queryRequest.UserId));
                    info(Constants.callingUser + callingUser);
                    Client.CallerId = callingUser.Id;
                }
            }

            // Create the SavedQuery request
            ExecuteByIdSavedQueryRequest executeUserQueryRequest = new ExecuteByIdSavedQueryRequest()
            {
                EntityId = queryId
            };

            // Execute the SavedQuery request
            ExecuteByIdSavedQueryResponse executeUserQueryResponse =
                (ExecuteByIdSavedQueryResponse)Client.Execute(executeUserQueryRequest);

            // Validate the results
            if (String.IsNullOrEmpty(executeUserQueryResponse.String))
            {
                response.Message = Constants.queryFailure;
                response.ResponseName = Constants.failureResponse;
                warning(response.Message);
                return response;
            }
            Client.CallerId = Guid.Empty;
            var xml = new XmlDocument();
            response.Message = executeUserQueryResponse.String;
            response.ResponseName = Constants.successResponse;
            return response;
        }
        catch (Exception ex)
        {
            //response.Message = DocMgmtAzureFunctions.SMPWrapperLog.LogException(ex, false, false);
            error($"Unexpected error::Message:{ex.Message}", ex);
            response.Message = ex.Message;
            response.ResponseName = Constants.failureResponse;
            return response;
        }
    }


    /// <summary>
    /// Get the user information and token. This method MUST be called for every API method
    /// </summary>
    /// <param name="request"></param>
    /// <param name="whoamI"></param>
    /// <returns></returns>
    public OrganizationResponse WhoAmI(HttpRequest request, WhoAmIRequest whoamI = null)
    {
        
        var authToken = (!string.IsNullOrEmpty(request.GetAuthToken())) ? request.GetAuthToken() : config.TestBearer;
        Token = authToken.Replace("Bearer ", "");
        try
        {
            Client.HeaderToken = Token;
        }
        catch (ObjectDisposedException)
        {
            warning("CRM client was disposed, initializing");
            InitClient();
            Client.HeaderToken = Token;
        }

        whoamI ??= new WhoAmIRequest();
        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //if (Client.State == CommunicationState.Opened) Client.Close();
        if (Client.State != CommunicationState.Opened) Client.Open();
        //Client.Open();

        var resp = Client.Execute(whoamI);
        if (resp == null) return null;
        var who = resp as WhoAmIResponse;        
        Client.CallerId = who.UserId;        

        return who;
        
    }

    public CommonResponse QueryCrm(HttpRequest request, string entityJson = "", string queryId = "")
    {
        debug($"incoming request json: {entityJson}");
        try
        {
            if (string.IsNullOrEmpty(Token))
            {
                throw new MessageSecurityException("authToken is empty");
            }
            else
            {
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                Client.HeaderToken = Token;
                info("Authorization Validated.");

                //sending who am i request just to make sure the token is fresh enough and also use the token's user in client object
                var responseWhoamI = Client.Execute(new WhoAmIRequest());
                string systemUserName =
                    (string)RetrieveFieldFromEntityByKey(request, Entities.SystemUserLogicalName,
                    "systemuserid", ((WhoAmIResponse)responseWhoamI).UserId, Attributes.fullName);

                string[] args = { request.Method.ToString(), Entities.contactName, systemUserName, "" };
                debug(string.Format("Request: {0}, Entity: {1}, User: {2}", args));

            }
        }
        catch (MessageSecurityException)
        {
            //handling the case when the oauth token has expired
            error(@"Security Token Expired! Token: " + Token);
            HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            responseMsg.Headers.Add("WWW-Authenticate", "Security Token Expired!");

            throw new System.Web.Http.HttpResponseException(responseMsg);
        }
        catch (FaultException<OrganizationServiceFault>)
        {
            error(@"Fault Exception attempting to validate token. Token: " + Token);
            HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            responseMsg.Headers.Add("WWW-Authenticate", "Fault Exception attempting to validate token.");

            throw new System.Web.Http.HttpResponseException(responseMsg);
        }
        catch (TimeoutException)
        {
            error("Time Out Error executing WhoAmI call.");
            HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.RequestTimeout);
            responseMsg.Headers.Add("WWW-Authenticate", "Time Out Error executing WhoAmI call.");
            throw new System.Web.Http.HttpResponseException(responseMsg);
        }

        return new CommonResponse();
    }
    public object RetrieveFieldFromEntityByKey(HttpRequest request, string entity, string idfield, 
        object recid, string lookupfield)
    {
        object result = new();
        List<Entity> entityResult = new List<Entity>();

        CommonResponse responseFetchXML = RetrieveFetchXMLQueryWithPagination(request, entity, out entityResult,
            $"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'><entity name='{entity}'><attribute name='{lookupfield}' /><filter type='and'><condition attribute='{idfield}' operator='eq' value='{recid.ToString()}' /></filter></entity></fetch>");

        if (responseFetchXML.ResponseName == Constants.failureResponse)
        {
            return "";
        }

        if (entityResult.Count < 1)
        {
            return "";
        }

        Entity entFound = entityResult.First<Entity>();
        foreach (KeyValuePair<string, object> attr in entFound.Attributes.ToList())
        {
            switch (attr.Key == lookupfield)
            {
                case true:
                    if (!string.IsNullOrEmpty(attr.Value.ToString()))
                        result = attr.Value;
                    break;
            }
        }
        return result;
    }

    public CommonResponse RetrieveFetchXMLQueryWithPagination(
        HttpRequest request, string entityName,
        out List<Entity> returnList, string fetchXML)
    {
        List<Entity> entities = new List<Entity>();
        CommonResponse response = new CommonResponse();
        string message = string.Empty;

        try
        {
            
                //var crmConn = connectionManager.Service;

                if (Client.CallerId == Guid.Empty)
                {
                    response.Message = Constants.connectionFailure;
                    warning(response.Message);
                    response.ResponseName = Constants.failureResponse;
                    returnList = null;
                    return response;
                }

                QueryRequest queryRequest = new QueryRequest();
                //string entityName = Request.RequestUri.AbsolutePath.Replace(Constants.odataRemoval, "").ToUpper();
                string configKey = entityName.ToUpper() + Constants.queryId;
                info($"configKey:{configKey}");
                queryRequest.QueryId = Configuration[$"QueryConfig:{configKey}"];
                info($"QueryId:{queryRequest.QueryId}");

                var queryId = new Guid(queryRequest.QueryId);

                ColumnSet colset = new ColumnSet("fetchxml", "querytype");
                Entity fetchResult =
                    Client.Retrieve("savedquery", queryId, colset);

                ValidateSavedQueryRequest validateRequest = new ValidateSavedQueryRequest()
                {
                    FetchXml = fetchXML,
                    QueryType = (int)fetchResult.Attributes["querytype"]
                };

                try
                {
                    // executing the request will throw an exception if the fetch xml is invalid
                    var validateResponse = (ValidateSavedQueryResponse)Client.Execute(validateRequest);
                }
                catch (Exception)
                {
                    response.Message = Constants.queryFailure;
                    response.ResponseName = Constants.failureResponse;
                    warning(response.Message);
                    returnList = null;
                    return response;
                }

                int fetchCount = 5000;
                int pageNumber = 1;
                string pagingCookie = null;

                while (true)
                {
                    string xml = CreateXml(fetchXML, pagingCookie, pageNumber, fetchCount);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);

                    xml = doc.OuterXml;

                    XmlAttributeCollection attrs = doc.DocumentElement.Attributes;

                    if (pagingCookie != null)
                    {
                        XmlAttribute pagingAttr = doc.CreateAttribute("paging-cookie");
                        pagingAttr.Value = pagingCookie;
                        attrs.Append(pagingAttr);
                    }

                    XmlAttribute pageAttr = doc.CreateAttribute("page");
                    pageAttr.Value = System.Convert.ToString(pageNumber);
                    attrs.Append(pageAttr);

                    XmlAttribute countAttr = doc.CreateAttribute("count");
                    countAttr.Value = System.Convert.ToString(fetchCount);
                    attrs.Append(countAttr);

                    RetrieveMultipleRequest fetchRequest1 = new RetrieveMultipleRequest
                    {
                        Query = new FetchExpression(xml)
                    };

                    RetrieveMultipleResponse retrieveMultipleResponse = (RetrieveMultipleResponse)Client.Execute(fetchRequest1);
                    EntityCollection returnCollection = retrieveMultipleResponse.EntityCollection;

                    entities.AddRange(returnCollection.Entities);
                    //foreach (Entity c in returnCollection.Entities)
                    //{
                    //    entities.Add(c);
                    //}

                    // Check for morerecords, if it returns 1.
                    if (returnCollection.MoreRecords)
                    {
                        // Increment the page number to retrieve the next page.
                        pageNumber++;

                        // Set the paging cookie to the paging cookie returned from current results.                            
                        pagingCookie = returnCollection.PagingCookie;
                    }
                    else
                    {
                        // If no more records in the result nodes, exit the loop.
                        break;
                    }

                }

                response.ResponseName = Constants.successResponse;

                if (entities.Count > 100000)
                    warning(string.Format("Query returned {0} {1} entities", entities.Count, entityName));

                returnList = entities;
                return response;

            
        }
        catch (Exception ex)
        {
            response.Message = ex.Message;
            response.ResponseName = Constants.failureResponse;
            error(string.Format("Unexpected error::Message:{0}||Trace:{1}", ex.Message, ex.StackTrace));
            returnList = null;
            return response;
        }
    }

    protected string CreateXml(string xml, string cookie, int page, int count)
    {
        StringReader stringReader = new StringReader(xml);
        XmlTextReader reader = new XmlTextReader(stringReader);

        // Load document
        XmlDocument doc = new XmlDocument();
        doc.Load(reader);

        return CreateXml(doc, cookie, page, count);
    }

    protected string CreateXml(XmlDocument doc, string cookie, int page, int count)
    {
        XmlAttributeCollection attrs = doc.DocumentElement?.Attributes;

        if (cookie != null)
        {
            XmlAttribute pagingAttr = doc.CreateAttribute("paging-cookie");
            pagingAttr.Value = cookie;
            attrs.Append(pagingAttr);
        }

        XmlAttribute pageAttr = doc.CreateAttribute("page");
        pageAttr.Value = System.Convert.ToString(page);
        attrs.Append(pageAttr);

        XmlAttribute countAttr = doc.CreateAttribute("count");
        countAttr.Value = System.Convert.ToString(count);
        attrs.Append(countAttr);

        StringBuilder sb = new StringBuilder(1024);
        StringWriter stringWriter = new StringWriter(sb);

        XmlTextWriter writer = new XmlTextWriter(stringWriter);
        doc.WriteTo(writer);
        writer.Close();

        return sb.ToString();
    }

    public CommonResponse RetrieveUserQueryWithPagination(HttpRequest request, string entityName, 
        out List<Entity> returnList, string[] fieldList = null)
    {
        List<Entity> entities = new List<Entity>();
        CommonResponse response = new CommonResponse();
        string message = string.Empty;

        try
        {
            if (Client.CallerId == Guid.Empty)
            {
                response.Message = Constants.connectionFailure;
                warning(response.Message);
                response.ResponseName = Constants.failureResponse;
                returnList = null;
                return response;
            }

            QueryRequest queryRequest = new QueryRequest();
            queryRequest.QueryId = GetQueryId(entityName);
            var queryId = new Guid(queryRequest.QueryId);

            ColumnSet colset = new ColumnSet("fetchxml", "querytype");
            Entity fetchResult = Client.Retrieve("savedquery", queryId, colset);

            ValidateSavedQueryRequest validateRequest = new ValidateSavedQueryRequest()
            {
                FetchXml = (string)fetchResult.Attributes["fetchxml"],
                QueryType = (int)fetchResult.Attributes["querytype"]
            };

            try
            {
                // executing the request will throw an exception if the fetch xml is invalid
                var validateResponse = (ValidateSavedQueryResponse)Client.Execute(validateRequest);
            }
            catch (Exception)
            {
                response.Message = Constants.queryFailure;
                response.ResponseName = Constants.failureResponse;
                warning(response.Message);
                returnList = null;
                return response;
            }

            int fetchCount = 5000;
            int pageNumber = 1;
            string pagingCookie = null;

            while (true)
            {
                string xml = CreateXml((string)fetchResult.Attributes["fetchxml"], pagingCookie, pageNumber, fetchCount);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                if (fieldList != null && fieldList.Count() > 0)
                {
                    int orderOffset = -1;
                    foreach (string fieldName in fieldList)
                    {
                        if (fieldName != "")
                        {
                            if (fieldName.Contains("|"))
                            {
                                string[] splitter = { "|" };
                                string[] fieldParts = fieldName.Split(splitter, StringSplitOptions.None);

                                if (fieldParts[0].Trim() == "order")
                                {
                                    orderOffset++;
                                    XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                                    XmlElement entity = (XmlElement)(entityList.Item(0));
                                    XmlNodeList orderList = entity.GetElementsByTagName("order");
                                    if (orderOffset > orderList.Count - 1)
                                    {
                                        //newCondition.SetAttribute("attribute", fieldParts[0]);
                                        //newCondition.SetAttribute("operator", fieldParts[1]);
                                        //newCondition.SetAttribute("value", fieldParts[2]);
                                        //filter.InsertAfter(newCondition, condition);
                                        XmlElement order = (XmlElement)(orderList.Item(0));
                                        XmlElement newOrder = (XmlElement)(order.CloneNode(true));
                                        newOrder.SetAttribute("attribute", fieldParts[1]);
                                        newOrder.SetAttribute(fieldParts[2], fieldParts[2].Trim().ToLowerInvariant() == "descending" ? "true" : "false");
                                        entity.InsertAfter(newOrder, order);
                                    }
                                    else
                                    {
                                        XmlElement order = (XmlElement)(orderList.Item(orderOffset));
                                        order.SetAttribute("attribute", fieldParts[1]);
                                        order.SetAttribute(fieldParts[2], fieldParts[2].Trim().ToLowerInvariant() == "descending" ? "true" : "false");
                                    }
                                }
                                else
                                {
                                    XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                                    XmlElement entity = (XmlElement)(entityList.Item(0));
                                    XmlNodeList filterList = entity.GetElementsByTagName("filter");
                                    XmlElement filter = (XmlElement)(filterList.Item(0));
                                    XmlNodeList conditionList = filter.GetElementsByTagName("condition");

                                    XmlElement condition = (XmlElement)(conditionList.Item(conditionList.Count - 1));
                                    XmlElement newCondition = (XmlElement)(condition.CloneNode(false));
                                    newCondition.SetAttribute("attribute", fieldParts[0]);
                                    newCondition.SetAttribute("operator", fieldParts[1]);
                                    if (fieldParts.Length > 2) newCondition.SetAttribute("value", fieldParts[2]);
                                    filter.InsertAfter(newCondition, condition);
                                }
                            }
                            else if (fieldName.Trim().StartsWith("<condition"))
                            {
                                XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                                XmlElement entity = (XmlElement)(entityList.Item(0));
                                XmlNodeList filterList = entity.GetElementsByTagName("filter");
                                XmlElement filter = (XmlElement)(filterList.Item(0));
                                XmlNodeList conditionList = filter.GetElementsByTagName("condition");
                                XmlElement condition = (XmlElement)(conditionList.Item(conditionList.Count - 1));
                                XmlElement newCondition = (XmlElement)(condition.CloneNode(false));
                                XmlDocument docLoad = XmlUtil.CreateXmlDocument(fieldName);
                                newCondition.SetAttribute("attribute", docLoad.DocumentElement.GetAttribute("attribute"));
                                newCondition.SetAttribute("operator", docLoad.DocumentElement.GetAttribute("operator"));
                                newCondition.RemoveAttribute("value");
                                newCondition.InnerXml = docLoad.DocumentElement.InnerXml;

                                filter.InsertAfter(newCondition, condition);

                            }
                            else
                            {
                                // add fieldName to the xml
                                XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                                XmlElement entity = (XmlElement)(entityList.Item(0));
                                XmlNodeList attribList = entity.GetElementsByTagName("attribute");

                                XmlElement attrib = (XmlElement)(attribList.Item(attribList.Count - 1));
                                XmlElement newAttrib = (XmlElement)(attrib.CloneNode(false));
                                newAttrib.SetAttribute("name", fieldName);
                                entity.InsertAfter(newAttrib, attrib);
                            }
                        }
                    }
                }

            xml = doc.OuterXml;

            RetrieveMultipleRequest fetchRequest1 = new RetrieveMultipleRequest
            {
                Query = new FetchExpression(xml)
            };

                
            RetrieveMultipleResponse retrieveMultipleResponse = 
                (RetrieveMultipleResponse)Client.Execute(fetchRequest1);
                    
            EntityCollection returnCollection = retrieveMultipleResponse.EntityCollection;

            entities.AddRange(returnCollection.Entities);
            //foreach (Entity c in returnCollection.Entities)
            //{
            //    entities.Add(c);
            //}

            // Check for morerecords, if it returns 1.
            if (returnCollection.MoreRecords)
            {
                // Increment the page number to retrieve the next page.
                pageNumber++;

                // Set the paging cookie to the paging cookie returned from current results.                            
                pagingCookie = returnCollection.PagingCookie;
            }
            else
            {
                // If no more records in the result nodes, exit the loop.
                break;
            }
                
            }

                response.ResponseName = Constants.successResponse;

                if (entities.Count > 100000)
                    warning($"Query returned {entities.Count} {entityName} entities");

                returnList = entities;
                return response;

            
        }
        catch (Exception ex)
        {
            response.Message = ex.Message;
            response.ResponseName = Constants.failureResponse;
            error($"{ex.GetType}: {ex.Message}", ex);
            returnList = null;
            return response;
        }
    }

    public CommonResponse RetrieveFetchXMLQueryWithPagination(HttpRequest request, string entityName, string fetchXML)
    {
        List<Entity> entities = new List<Entity>();
        CommonResponse response = new CommonResponse();
        string message = string.Empty;

        try
        {
            //var crmConn = connectionManager.Service;

            if (Client.CallerId == Guid.Empty)
            {
                response.Message = Constants.connectionFailure;
                warning(response.Message);
                response.ResponseName = Constants.failureResponse;

                return response;
            }

            QueryRequest queryRequest = new QueryRequest();
            queryRequest.QueryId = GetQueryId(entityName);

            var queryId = new Guid(queryRequest.QueryId);

            ColumnSet colset = new ColumnSet("fetchxml", "querytype");
            Entity fetchResult = Client.Retrieve("savedquery", queryId, colset);

            ValidateSavedQueryRequest validateRequest = new ValidateSavedQueryRequest()
            {
                FetchXml = fetchXML,
                QueryType = (int)fetchResult.Attributes["querytype"]
            };

            try
            {
                // executing the request will throw an exception if the fetch xml is invalid
                var validateResponse = (ValidateSavedQueryResponse)Client.Execute(validateRequest);
            }
            catch (Exception)
            {
                response.Message = Constants.queryFailure;
                response.ResponseName = Constants.failureResponse;
                warning(response.Message);
                return response;
            }

            int fetchCount = 5000;
            int pageNumber = 1;
            string pagingCookie = null;

            while (true)
            {
                string xml = CreateXml(fetchXML, pagingCookie, pageNumber, fetchCount);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                xml = doc.OuterXml;

                XmlAttributeCollection attrs = doc.DocumentElement.Attributes;

                if (pagingCookie != null)
                {
                    XmlAttribute pagingAttr = doc.CreateAttribute("paging-cookie");
                    pagingAttr.Value = pagingCookie;
                    attrs.Append(pagingAttr);
                }

                XmlAttribute pageAttr = doc.CreateAttribute("page");
                pageAttr.Value = Convert.ToString(pageNumber);
                attrs.Append(pageAttr);

                XmlAttribute countAttr = doc.CreateAttribute("count");
                countAttr.Value = System.Convert.ToString(fetchCount);
                attrs.Append(countAttr);

                RetrieveMultipleRequest fetchRequest1 = new RetrieveMultipleRequest
                {
                    Query = new FetchExpression(xml)
                };

                RetrieveMultipleResponse retrieveMultipleResponse = 
                    (RetrieveMultipleResponse)Client.Execute(fetchRequest1);
                EntityCollection returnCollection = retrieveMultipleResponse.EntityCollection;

                entities.AddRange(returnCollection.Entities);

                // Check for morerecords, if it returns 1.
                if (returnCollection.MoreRecords)
                {
                    // Increment the page number to retrieve the next page.
                    pageNumber++;

                    // Set the paging cookie to the paging cookie returned from current results.                            
                    pagingCookie = returnCollection.PagingCookie;
                }
                else
                {
                    // If no more records in the result nodes, exit the loop.
                    break;
                }

            }

            response.Message = JsonConvert.SerializeObject(entities);
            response.ResponseName = Constants.successResponse;

            return response;

            
        }
        catch (Exception ex)
        {
            response.Message = ex.Message;
            response.ResponseName = Constants.failureResponse;
            error($"{ex.GetType()}:{ex.Message}",ex);
            return response;
        }
    }

    /// <summary>
    /// Get the query ID from the entity name.
    /// </summary>
    /// <param name="entityName"></param>
    /// <returns></returns>
    public string GetQueryId(string entityName)
    {
        var configKey = entityName.ToUpper() + Constants.queryId;
        info($"configKey:{configKey}");
        var queryId = Configuration[$"QueryConfig:{configKey}"];
        info($"queryId: {queryId}");
        return queryId;
    }

    public CommonResponse RetrieveUserQueryWithPagination(HttpRequest request, 
        string entityName, string[] fieldList = null)
    {
        List<Entity> entities = new List<Entity>();
        CommonResponse response = new CommonResponse();
        string message = string.Empty;

        try
        {
            if (Client.CallerId == Guid.Empty)
            {
                response.Message = Constants.connectionFailure;
                warning(response.Message);
                response.ResponseName = Constants.failureResponse;

                return response;
            }

            QueryRequest queryRequest = new QueryRequest();
            queryRequest.QueryId = GetQueryId(entityName);
            var queryId = new Guid(queryRequest.QueryId);

            ColumnSet colset = new ColumnSet("fetchxml", "querytype");
            Entity fetchResult =
                Client.Retrieve("savedquery", queryId, colset);

            ValidateSavedQueryRequest validateRequest = new ValidateSavedQueryRequest()
            {
                FetchXml = (string)fetchResult.Attributes["fetchxml"],
                QueryType = (int)fetchResult.Attributes["querytype"]
            };

            try
            {
                // executing the request will throw an exception if the fetch xml is invalid
                var validateResponse = (ValidateSavedQueryResponse)Client.Execute(validateRequest);
            }
            catch (Exception)
            {
                response.Message = Constants.queryFailure;
                response.ResponseName = Constants.failureResponse;
                warning(response.Message);
                return response;
            }

            int fetchCount = 5000;
            int pageNumber = 1;
            string pagingCookie = null;

            while (true)
            {
                string xml = CreateXml((string)fetchResult.Attributes["fetchxml"], pagingCookie, pageNumber, fetchCount);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                if (fieldList != null && fieldList.Count() > 0)
                {
                    int orderOffset = -1;
                    foreach (string fieldName in fieldList)
                    {
                        if (fieldName != "")
                        {
                            if (fieldName.Contains("|"))
                            {
                                string[] splitter = { "|" };
                                string[] fieldParts = fieldName.Split(splitter, StringSplitOptions.None);

                                if (fieldParts[0].Trim() == "order")
                                {
                                    orderOffset++;
                                    XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                                    XmlElement entity = (XmlElement)(entityList.Item(0));
                                    XmlNodeList orderList = entity.GetElementsByTagName("order");
                                    if (orderOffset > orderList.Count - 1)
                                    {
                                        //newCondition.SetAttribute("attribute", fieldParts[0]);
                                        //newCondition.SetAttribute("operator", fieldParts[1]);
                                        //newCondition.SetAttribute("value", fieldParts[2]);
                                        //filter.InsertAfter(newCondition, condition);
                                        XmlElement order = (XmlElement)(orderList.Item(0));
                                        XmlElement newOrder = (XmlElement)(order.CloneNode(true));
                                        newOrder.SetAttribute("attribute", fieldParts[1]);
                                        newOrder.SetAttribute(fieldParts[2], fieldParts[2].Trim().ToLowerInvariant() == "descending" ? "true" : "false");
                                        entity.InsertAfter(newOrder, order);
                                    }
                                    else
                                    {
                                        XmlElement order = (XmlElement)(orderList.Item(orderOffset));
                                        order.SetAttribute("attribute", fieldParts[1]);
                                        order.SetAttribute(fieldParts[2], fieldParts[2].Trim().ToLowerInvariant() == "descending" ? "true" : "false");
                                    }
                                }
                                else
                                {
                                    XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                                    XmlElement entity = (XmlElement)(entityList.Item(0));
                                    XmlNodeList filterList = entity.GetElementsByTagName("filter");
                                    XmlElement filter = (XmlElement)(filterList.Item(0));
                                    XmlNodeList conditionList = filter.GetElementsByTagName("condition");

                                    XmlElement condition = (XmlElement)(conditionList.Item(conditionList.Count - 1));
                                    XmlElement newCondition = (XmlElement)(condition.CloneNode(false));
                                    newCondition.SetAttribute("attribute", fieldParts[0]);
                                    newCondition.SetAttribute("operator", fieldParts[1]);
                                    newCondition.SetAttribute("value", fieldParts[2]);
                                    filter.InsertAfter(newCondition, condition);
                                }
                            }
                            else if (fieldName.Trim().StartsWith("<condition"))
                            {
                                XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                                XmlElement entity = (XmlElement)(entityList.Item(0));
                                XmlNodeList filterList = entity.GetElementsByTagName("filter");
                                XmlElement filter = (XmlElement)(filterList.Item(0));
                                XmlNodeList conditionList = filter.GetElementsByTagName("condition");
                                XmlElement condition = (XmlElement)(conditionList.Item(conditionList.Count - 1));
                                XmlElement newCondition = (XmlElement)(condition.CloneNode(false));
                                XmlDocument docLoad = XmlUtil.CreateXmlDocument(fieldName);
                                newCondition.SetAttribute("attribute", docLoad.DocumentElement.GetAttribute("attribute"));
                                newCondition.SetAttribute("operator", docLoad.DocumentElement.GetAttribute("operator"));
                                newCondition.RemoveAttribute("value");
                                newCondition.InnerXml = docLoad.DocumentElement.InnerXml;

                                filter.InsertAfter(newCondition, condition);

                            }
                            else
                            {
                                // add fieldName to the xml
                                XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                                XmlElement entity = entityList.Item(0) as XmlElement;
                                XmlNodeList attribList = entity.GetElementsByTagName("attribute");

                                XmlElement attrib = (XmlElement)(attribList.Item(attribList.Count - 1));
                                XmlElement newAttrib = (XmlElement)(attrib.CloneNode(false));
                                newAttrib.SetAttribute("name", fieldName);
                                entity.InsertAfter(newAttrib, attrib);
                            }
                        }
                    }
                }

                xml = doc.OuterXml;

                RetrieveMultipleRequest fetchRequest1 = new RetrieveMultipleRequest
                {
                    Query = new FetchExpression(xml)
                };

                RetrieveMultipleResponse retrieveMultipleResponse = 
                    (RetrieveMultipleResponse)Client.Execute(fetchRequest1);
                EntityCollection returnCollection = retrieveMultipleResponse.EntityCollection;

                entities.AddRange(returnCollection.Entities);

                // Check for morerecords, if it returns 1.
                if (returnCollection.MoreRecords)
                {
                    // Increment the page number to retrieve the next page.
                    pageNumber++;

                    // Set the paging cookie to the paging cookie returned from current results.                            
                    pagingCookie = returnCollection.PagingCookie;
                }
                else
                {
                    // If no more records in the result nodes, exit the loop.
                    break;
                }

            }

            response.Message = JsonConvert.SerializeObject(entities);
            response.ResponseName = Constants.successResponse;

            return response;

            
        }
        catch (Exception ex)
        {
            response.Message = ex.Message;
            response.ResponseName = Constants.failureResponse;
            error($"{ex.GetType()}: {ex.Message}", ex);
            return response;
        }
    }

    /*
    protected bool Disposed { get; set; }
    public void Dispose()
    {
        if (Client.State != CommunicationState.Closed) 
            Client.Close();

        if (!Disposed)
        {
            Client.Dispose();
            Disposed = true;
        }
    }
    */
}
