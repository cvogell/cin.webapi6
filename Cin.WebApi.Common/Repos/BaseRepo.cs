﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cin.WebApi.Repos;

public abstract class BaseRepo
{
    protected readonly IConfiguration config;
    protected readonly ILogger log;
    protected readonly IMemoryCache cache;

    protected BaseRepo(ILogger logger, IConfiguration configuration, IMemoryCache memoryCache)
    {
        log = logger;
        config = configuration;
        cache = memoryCache;
    }

    #region logging

    public void debug(string message)
    {
        log.LogDebug(message);
    }

    public void info(string message)
    {
        log.LogInformation(message);
    }

    public void warning(string message, Exception? ex = null)
    {
        log.LogWarning(ex, message);
    }
    public void error(string message, Exception? ex = null)
    {
        log.LogError(ex, message);
    }

    public void critical(string message, Exception? ex = null)
    {
        log.LogCritical(ex, message);
    }
    #endregion

    #region tokens
    protected string getAuthToken(HttpRequestMessage request)
    {
        if ((request.Headers.Contains("dynamics")) && !string.IsNullOrEmpty(request.Headers.GetValues("dynamics").ToString()))
        {
            IEnumerable<string> headerValues = request.Headers.GetValues("dynamics");
            var id = headerValues.FirstOrDefault();
            string token = id.ToString();
            return token;
        }
        else if ((!request.Headers.Contains("Authorization")) || request.Headers.Authorization == null
            || string.IsNullOrEmpty(request.Headers.Authorization.ToString())
            || request.Headers.Authorization.ToString().Trim().ToUpperInvariant().Equals("BEARER"))
        {

            return "Bearer " + config["AppConfig:CRMTestingUserToken"];
        }
        else
        {
            string token = request.Headers.Authorization.ToString();
            return token;
        }

    }

    #endregion

}
