﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cin.WebApi.OData;
public class OptionSetTable
{
    public string Key { get; set; }
    public Dictionary<string, string> Value { get; set; }
}

public class JsonWhoAmI
{
    public string BusinessUnitId { get; set; }
    public string UserId { get; set; }
    public string UserName { get; set; }
    public string UserEmail { get; set; }
    public string OrganizationId { get; set; }
}
