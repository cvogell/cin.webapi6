﻿using Cin.WebApi.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Reflection;
using Microsoft.Extensions.Logging;
using Cin.WebApi.Config;
using Microsoft.Extensions.Configuration;

namespace Cin.WebApi.OData;
public class ODataWriteHandler<TCustomClass>: CommonBase
    where TCustomClass : class
{
    public HttpResponseMessage CloseEntity(TCustomClass entity2Lose, string name, string oauthToken, KeyValuePair<string, string>[] fieldFormats, CreateResponseResult createResult = null)
    {
        PropertyInfo[] properties = entity2Lose.GetType().GetProperties();
        object propertyValue = null, subPropValue = null;

        JObject jBody = new JObject();
        JObject jSubBody = new JObject();
        foreach (PropertyInfo property in properties)
        {
            string propertyName = property.Name.ToLowerInvariant();
            propertyValue = property.GetValue(entity2Lose);
            if (propertyValue != null)
            {
                if (propertyValue.GetType().GetProperties().Count() == 0 && property.PropertyType.Name.ToLowerInvariant() == "int32")
                    jBody[property.Name] = Convert.ToInt32(propertyValue.ToString());

                if (propertyValue.GetType().GetProperties().Count() > 0)
                {
                    foreach (PropertyInfo prop in propertyValue.GetType().GetProperties())
                    {
                        string subPropName = prop.Name.ToLowerInvariant();
                        subPropValue = prop.GetValue(propertyValue);

                        if (prop.PropertyType.Name.ToLowerInvariant() == "int32")
                            jSubBody[subPropName] = Convert.ToInt32(subPropValue.ToString());
                        else
                        {
                            if (fieldFormats.Where(ff => ff.Key.ToLowerInvariant() == subPropName).Count() > 0)
                            {
                                KeyValuePair<string, string> fieldFormat = fieldFormats.Where(ff => ff.Key.ToLowerInvariant() == subPropName).ToArray()[0];
                                subPropName += "@odata.bind";
                                jSubBody[subPropName] = string.Format(fieldFormat.Value, subPropValue.ToString());
                            }
                            //jSubBody[property.Name.ToLowerInvariant()] = jSubBody;
                        }
                    }
                    jBody[property.Name] = jSubBody;
                }
            }
        }

        var createEntityUrl = string.Concat(crmConfig.CRMURL, string.Format(crmConfig.CRMODataCreateEntityUrl,name));
        using (HttpClient httpClient = new HttpClient())
        {
            httpClient.Timeout = new TimeSpan(0, 2, 0);
            httpClient.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
            httpClient.DefaultRequestHeaders.Add("OData-Version", "4.0");
            httpClient.DefaultRequestHeaders.Add("Accept-Charset", "utf-8");
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, createEntityUrl);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", oauthToken);
            request.Headers.AcceptCharset.Add(StringWithQualityHeaderValue.Parse("utf-8"));
            request.Headers.Add("OData-MaxVersion", "4.0");
            request.Headers.Add("OData-Version", "4.0");
            request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.Headers.Add("Accept", "application/json");
            request.Content = new StringContent(JsonConvert.SerializeObject(jBody));
            request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json; charset=utf-8");

            HttpResponseMessage response = httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;

            if (createResult != null && response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                response.StatusCode = System.Net.HttpStatusCode.Created;
                response.Content = new StringContent(JsonConvert.SerializeObject(createResult));
                response.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json; charset=utf-8");
            }
            return response;
        }
    }

    public HttpResponseMessage SaveEntity(TCustomClass entity2Save, string oDataEntity, string oauthToken, KeyValuePair<string, string>[] fieldFormats, string existingEntityId = null, KeyValuePair<string, string>[] fieldMappings = null, KeyValuePair<string, string>[] fieldTransformations = null, CreateResponseResult createResult = null)
    {
        PropertyInfo[] properties = entity2Save.GetType().GetProperties();
        object propertyValue = null;
        KeyValuePair<string, string> isEntityField = new KeyValuePair<string, string>();
        KeyValuePair<string, string> isDateYYYYMMDD = new KeyValuePair<string, string>();
        KeyValuePair<string, string> isInt = new KeyValuePair<string, string>();
        if (fieldTransformations != null && fieldTransformations.Where(ff => ff.Key.ToLowerInvariant() == "isentityref").Count() > 0)
            isEntityField = fieldTransformations.Where(ff => ff.Key.ToLowerInvariant() == "isentityref").ToArray()[0];
        if (fieldTransformations != null && fieldTransformations.Where(ff => ff.Key.ToLowerInvariant() == "isdateyyyymmdd").Count() > 0)
            isDateYYYYMMDD = fieldTransformations.Where(ff => ff.Key.ToLowerInvariant() == "isdateyyyymmdd").ToArray()[0];
        if (fieldTransformations != null && fieldTransformations.Where(ff => ff.Key.ToLowerInvariant() == "isint").Count() > 0)
            isInt = fieldTransformations.Where(ff => ff.Key.ToLowerInvariant() == "isint").ToArray()[0];

        JObject jBody = new JObject();

        foreach (PropertyInfo property in properties)
        {
            propertyValue = property.GetValue(entity2Save);

            if (propertyValue != null)
            {
                string propertyName = property.Name.ToLowerInvariant();

                if ("object|valuetype".Contains(property.PropertyType.BaseType.Name.ToLowerInvariant()))
                {
                    if (isEntityField.Key != null && isEntityField.Value.ToLowerInvariant() == propertyName)
                    {
                        KeyValuePair<string, string> fieldFormat = fieldFormats.Where(ff => ff.Key.ToLowerInvariant() == propertyName).ToArray()[0];
                        if (fieldMappings != null)
                        {
                            KeyValuePair<string, string> fieldMapping = fieldMappings.Where(ff => ff.Key.ToLowerInvariant() == propertyName).ToArray()[0];
                            propertyName = fieldMapping.Value;
                        }
                        propertyName += "@odata.bind";

                        jBody[propertyName] = string.Format(fieldFormat.Value, propertyValue.ToString());
                    }
                    else
                    {
                        if (propertyName != "__metadata")
                            jBody[property.Name.ToLowerInvariant()] = propertyValue.ToString();
                    }
                }
                else
                {
                    PropertyInfo textProp = property.GetValue(entity2Save).GetType().GetProperties().Where(prop => prop.Name.ToLowerInvariant() == "text").ToArray()[0];
                    object textVal = textProp.GetValue(property.GetValue(entity2Save));
                    if (textVal != null)
                    {
                        //string propertyName = property.Name.ToLowerInvariant();
                        switch (property.PropertyType.BaseType.Name.ToLowerInvariant())
                        {
                            case "dynamicsentitynullreference":
                            case "dynamicsvariantentityreference":
                            case "dynamicsentityreference":
                                if (fieldFormats.Where(ff => ff.Key.ToLowerInvariant() == propertyName).Count() > 0)
                                {
                                    KeyValuePair<string, string> fieldFormat = fieldFormats.Where(ff => ff.Key.ToLowerInvariant() == propertyName).ToArray()[0];
                                    if (fieldMappings != null && fieldMappings.Where(ff => ff.Key.ToLowerInvariant() == propertyName).Count() > 0)
                                    {
                                        KeyValuePair<string, string> fieldMapping = fieldMappings.Where(ff => ff.Key.ToLowerInvariant() == propertyName).ToArray()[0];
                                        propertyName = fieldMapping.Value;
                                    }
                                    propertyName += "@odata.bind";

                                    jBody[propertyName] = string.Format(fieldFormat.Value, textVal.ToString());
                                }
                                break;
                            case "dynamicsbooleanvalue":
                                if (textVal.ToString() == "1")
                                {
                                    jBody[propertyName] = true;
                                }
                                else
                                {
                                    jBody[propertyName] = false;
                                }
                                break;
                            case "dynamicsdatetimevalue":
                                DateTime dt = DateTime.Parse(textVal.ToString());
                                if (isDateYYYYMMDD.Key != null && isDateYYYYMMDD.Value.ToLowerInvariant() == propertyName)
                                    jBody[propertyName] = dt.ToString("yyyy-MM-dd");
                                else
                                    jBody[propertyName] = dt;
                                break;
                            case "dynamicsformattedvalueonly":
                            case "dynamicsformattedvalue":
                                if (isInt.Key != null && isInt.Value.ToLowerInvariant() == propertyName)
                                    jBody[propertyName] = Convert.ToInt32(textVal.ToString());
                                else
                                    jBody[propertyName] = decimal.Parse(textVal.ToString());
                                break;
                            case "dynamicscurrencyvalue":
                                jBody[propertyName] = decimal.Parse(textVal.ToString());
                                break;
                            case "dynamicsoptionsetref":
                            default:
                                jBody[propertyName] = textVal.ToString();
                                break;
                        }

                    }
                }
            }
        }

        var createEntityUrl = string.Concat(crmConfig.CRMURL, 
            string.Format(crmConfig.CRMODataCreateEntityUrl, oDataEntity));
        var updateEntityUrl = string.Concat(crmConfig.CRMURL,
            string.Format(crmConfig.CRMODataUpdateEntityUrl, oDataEntity, existingEntityId));
        using (HttpClient httpClient = new HttpClient())
        {
            httpClient.Timeout = new TimeSpan(0, 2, 0);
            httpClient.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
            httpClient.DefaultRequestHeaders.Add("OData-Version", "4.0");
            httpClient.DefaultRequestHeaders.Add("Accept-Charset", "utf-8");
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
            HttpRequestMessage request = string.IsNullOrEmpty(existingEntityId)
                ? new HttpRequestMessage(HttpMethod.Post, createEntityUrl)
                : new HttpRequestMessage(new HttpMethod("PATCH"), updateEntityUrl);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", oauthToken);
            request.Headers.AcceptCharset.Add(StringWithQualityHeaderValue.Parse("utf-8"));
            request.Headers.Add("OData-MaxVersion", "4.0");
            request.Headers.Add("OData-Version", "4.0");
            request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.Headers.Add("Accept", "application/json");
            if (!string.IsNullOrEmpty(existingEntityId)) request.Headers.Add("If-Match", "*");      //preventing creation of new record if no record found with ID
                                                                                                    //request.Content = new StringContent(jsonBody.ToString());
            request.Content = new StringContent(JsonConvert.SerializeObject(jBody));
            request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json; charset=utf-8");

            HttpResponseMessage response = httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;

            if (createResult != null && response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                response.StatusCode = System.Net.HttpStatusCode.Created;
                response.Content = new StringContent(JsonConvert.SerializeObject(createResult));
                response.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json; charset=utf-8");
            }
            //HttpResponseMessage responseReturn = response;
            //if (createResult != null && response.StatusCode == System.Net.HttpStatusCode.NoContent)
            //    CreateResponseResult cres = request.CreateResponse(System.Net.HttpStatusCode.Created, "");
            //    responseReturn = request.CreateResponse(System.Net.HttpStatusCode.Created, );

            return response;
        }
    }
    protected readonly CrmConfig crmConfig;
    protected readonly IConfiguration config;
    public ODataWriteHandler(ILogger<ODataWriteHandler<TCustomClass>> logger, IConfiguration configuration) : base(logger)
    {
        config = configuration;
        crmConfig = CrmConfig.Construct(config);
    }
}
