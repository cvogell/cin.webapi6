﻿using Cin.WebApi.Models;
using Microsoft.PowerPlatform.Dataverse.Client;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO.Compression;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Cin.WebApi.Config;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace Cin.WebApi.OData;
public class ODataReadHandler: CommonBase
{
    private class JsonResponse
    {
        public JArray value { get; set; }
    }

    public string EmbedXMLInFetchXML(string fetchXML, string[] embedXMLArray)
    {
        string xml = fetchXML;
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xml);

        if (embedXMLArray != null && embedXMLArray.Count() > 0)
        {
            int orderOffset = -1;
            foreach (string embedXML in embedXMLArray)
            {
                if (embedXML != string.Empty)
                {
                    string[] splitter = { "|" };
                    string[] embedXMLParts = embedXML.Split(splitter, StringSplitOptions.None);

                    if (embedXML.Contains("|"))
                    {
                        if (embedXMLParts[0].Trim() == "order")
                        {
                            // revisit for flexibility and error trapping.
                            orderOffset++;
                            XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                            XmlElement entity = (XmlElement)(entityList.Item(0));
                            XmlNodeList orderList = entity.GetElementsByTagName("order");
                            if (orderOffset > orderList.Count - 1)
                            {
                                XmlElement order = (XmlElement)(orderList.Item(0));
                                XmlElement newOrder = (XmlElement)(order.CloneNode(true));
                                newOrder.SetAttribute("attribute", embedXMLParts[1]);
                                newOrder.SetAttribute(embedXMLParts[2], embedXMLParts[2].Trim().ToLowerInvariant() == "descending" ? "true" : "false");
                                entity.InsertAfter(newOrder, order);
                            }
                            else
                            {
                                XmlElement order = (XmlElement)(orderList.Item(orderOffset));
                                order.SetAttribute("attribute", embedXMLParts[1]);
                                order.SetAttribute(embedXMLParts[2], embedXMLParts[2].Trim().ToLowerInvariant() == "descending" ? "true" : "false");
                            }
                        }
                        else
                        {
                            XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                            XmlElement entity = (XmlElement)(entityList.Item(0));
                            XmlNodeList filterList = entity.GetElementsByTagName("filter");
                            if (filterList.Count == 0)
                            {
                                XmlElement newFilter = entity.OwnerDocument.CreateElement("filter");
                                newFilter.SetAttribute("type", "and");
                                entity.AppendChild(newFilter);
                                filterList = entity.GetElementsByTagName("filter");
                            }
                            XmlElement filter = null;
                            for (int i = 0; i < filterList.Count; i++)
                            {
                                if (((XmlElement)(filterList.Item(i))).ParentNode.Name.ToLowerInvariant() == "entity")
                                {
                                    filter = (XmlElement)(filterList.Item(i));
                                    break;
                                }
                            }

                            XmlElement newCondition = entity.OwnerDocument.CreateElement("condition");
                            newCondition.SetAttribute("attribute", embedXMLParts[0]);
                            newCondition.SetAttribute("operator", embedXMLParts[1]);
                            if (embedXMLParts.Length > 2)
                            {
                                string operand = embedXMLParts.Length > 3 ? embedXMLParts[2].ToLowerInvariant() : "value";
                                string operandvalue = embedXMLParts[embedXMLParts.Length > 3 ? 3 : 2];
                                newCondition.SetAttribute(operand, operandvalue);
                            }
                            filter.AppendChild(newCondition);
                        }
                    }
                    else if (embedXML.Trim().StartsWith("<condition"))
                    {
                        // Created to handle older alternate conditional inserts to fetch XML.
                        XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                        XmlElement entity = (XmlElement)(entityList.Item(0));
                        XmlNodeList filterList = entity.GetElementsByTagName("filter");
                        if (filterList.Count == 0)
                        {
                            XmlElement newFilter = entity.OwnerDocument.CreateElement("filter");
                            entity.AppendChild(newFilter);
                            filterList = entity.GetElementsByTagName("filter");
                        }
                        XmlElement filter = (XmlElement)(filterList.Item(0));

                        XmlDocument docLoad = XmlUtil.CreateXmlDocument(embedXML);

                        XmlElement newCondition = entity.OwnerDocument.CreateElement("condition");
                        newCondition.SetAttribute("attribute", docLoad.DocumentElement.GetAttribute("attribute"));
                        newCondition.SetAttribute("operator", docLoad.DocumentElement.GetAttribute("operator"));
                        if (!string.IsNullOrEmpty(docLoad.DocumentElement.GetAttribute("value"))) newCondition.SetAttribute("value", docLoad.DocumentElement.GetAttribute("value"));
                        filter.AppendChild(newCondition);
                    }
                    else if (embedXML.Trim().StartsWith("<"))
                    {
                        // add embedXML to the xml
                        XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                        XmlElement entity = (XmlElement)(entityList.Item(0));

                        XmlElement newEntity = (XmlElement)entity.CloneNode(false);
                        newEntity.InnerXml = embedXML;
                        //XmlNodeList attribList = entity.GetElementsByTagName("attribute");
                        //XmlElement attrib = (XmlElement)(attribList.Item(attribList.Count - 1));

                        foreach (XmlElement newElement in newEntity.ChildNodes)
                            entity.AppendChild(newElement);
                    }
                    else
                    {
                        XmlNodeList entityList = doc.DocumentElement.GetElementsByTagName("entity");
                        XmlElement entity = (XmlElement)(entityList.Item(0));

                        XmlElement newAttrib = entity.OwnerDocument.CreateElement("attribute");
                        newAttrib.SetAttribute("name", embedXML);
                        entity.AppendChild(newAttrib);

                    }
                }
            }
        }

        xml = doc.OuterXml;

        return xml;

    }

    public string GetODataResultString(string oDataQuery, string oDataEntity, string oauthToken)
    {
        List<string> fillerList = new List<string>();
        string fillerVal = null;
        return GetODataResultString(oDataQuery, oDataEntity, oauthToken, ref fillerList, ref fillerVal);
    }

    public string GetODataResultString(string oDataQuery, string oDataEntity, string oauthToken, ref List<string> optionSets, string optionsetEntity)
    {
        string fillerVal = null;
        return GetODataResultString(oDataQuery, oDataEntity, oauthToken, ref optionSets, ref fillerVal, optionsetEntity);
    }

    public string GetFieldFromEntityViaOData(string oauthToken, string entity, string entityKey, string field)
    {
        string result = null;
        using (HttpClient httpClient = new HttpClient())
        {
            httpClient.Timeout = new TimeSpan(0, 2, 0);
            httpClient.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
            httpClient.DefaultRequestHeaders.Add("OData-Version", "4.0");
            httpClient.DefaultRequestHeaders.Add("Accept-Charset", "utf-8");
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");

            var path = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataBatchPath);
            

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, path);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", oauthToken);
            request.Headers.AcceptCharset.Add(StringWithQualityHeaderValue.Parse("utf-8"));
            request.Headers.Add("OData-MaxVersion", "4.0");
            request.Headers.Add("OData-Version", "4.0");
            request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.Headers.Add("Accept", "application/json;odata.metadata=full");

            var entityPath = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataEntityPath);
            MultipartContent batchContent = new MultipartContent("mixed", "batch_" + Guid.NewGuid().ToString());
            HttpMessageContent getRequestContent_entityRefs =
                new HttpMessageContent(
                    new HttpRequestMessage(
                        HttpMethod.Get,
                        string.Format(entity, entity, entityKey, field)));
            getRequestContent_entityRefs.Headers.Remove("Content-Type");
            getRequestContent_entityRefs.Headers.Add("Content-Type", "application/http");
            getRequestContent_entityRefs.Headers.Add("Content-Transfer-Encoding", "binary");

            batchContent.Add(getRequestContent_entityRefs);

            request.Content = batchContent;
            var response = httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;

            if (!response.IsSuccessStatusCode)
            {
                info("gorStr - Call Failed");
                info("gorStr - " + response.StatusCode.ToString());
                info("gorStr - " + response.ReasonPhrase);
                info("gorStr - " + response.RequestMessage?.Method.ToString());

                return string.Empty;
            }
            var responseContent = response.Content;
            var responseStream = responseContent.ReadAsStreamAsync().Result;
            var responseString = string.Empty;

            using (var uncompressed = new MemoryStream())
            {
                using (var compressed = new GZipStream(responseStream, CompressionMode.Decompress))
                {
                    byte[] bytes = new byte[4096];

                    int cnt;

                    while ((cnt = compressed.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        uncompressed.Write(bytes, 0, cnt);
                    }

                    responseString = Encoding.UTF8.GetString(uncompressed.ToArray());
                }
            }

            string[] splitter = { "odata.context" };
            string[] splitResponse = responseString.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            for (int indx = 1; indx < splitResponse.Length; indx++)
            {
                string[] subsplitter = { "--batchresponse" };
                string[] subsplitResponse = splitResponse[indx].Split(subsplitter, StringSplitOptions.RemoveEmptyEntries);
                string curBatchRes = "{" + subsplitResponse[0].Substring(subsplitResponse[0].IndexOf("\"value\":[", StringComparison.OrdinalIgnoreCase)).Replace(@"@odata.", "odata");
                XmlNode returnedXML = JsonConvert.DeserializeXmlNode(curBatchRes, "value");
                if (indx == 1)
                {
                    XmlNodeList nodeList = returnedXML.FirstChild.ChildNodes;

                    foreach (XmlNode node in nodeList)
                    {
                        result = node.ChildNodes[1].InnerText;
                    }
                }
            }
            return result;
        }
    }

    public string GetODataResultString(string oDataQuery, string oDataEntity, string oauthToken, ref string entityRef)
    {
        List<string> fillerList = new List<string>();
        return GetODataResultString(oDataQuery, oDataEntity, oauthToken, ref fillerList, ref entityRef);
    }

    public string GetODataResultString(string oDataQuery, string oDataEntity, string oauthToken, ref List<string> optionSets, ref string entityRef, string optionsetEntity = null)
    {
        using (HttpClient httpClient = new HttpClient())
        {
            //ODataBatchHandler odbh = new DefaultODataBatchHandler(GlobalConfiguration.DefaultServer);
            //odbh.MessageQuotas.MaxOperationsPerChangeset = 10;
            //odbh.MessageQuotas.MaxPartsPerBatch = 10;

            httpClient.Timeout = new TimeSpan(0, 2, 0);
            httpClient.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
            httpClient.DefaultRequestHeaders.Add("OData-Version", "4.0");
            httpClient.DefaultRequestHeaders.Add("Accept-Charset", "utf-8");
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");

            var batchPath = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataBatchPath);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, batchPath);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", oauthToken);
            request.Headers.AcceptCharset.Add(StringWithQualityHeaderValue.Parse("utf-8"));
            request.Headers.Add("OData-MaxVersion", "4.0");
            request.Headers.Add("OData-Version", "4.0");
            request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.Headers.Add("Accept", "application/json;odata.metadata=full");

            MultipartContent batchContent = new MultipartContent("mixed", "batch_" + Guid.NewGuid().ToString());

            var xmlPath = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataFetchXMLPath);
            HttpMessageContent getRequestContent_FetchXmlLookup =
                new HttpMessageContent(
                    new HttpRequestMessage(
                        HttpMethod.Get,
                        string.Format(xmlPath, oDataEntity, oDataQuery)));
            getRequestContent_FetchXmlLookup.Headers.Remove("Content-Type");
            getRequestContent_FetchXmlLookup.Headers.Add("Content-Type", "application/http");
            getRequestContent_FetchXmlLookup.Headers.Add("Content-Transfer-Encoding", "binary");

            batchContent.Add(getRequestContent_FetchXmlLookup);
            var mapPath = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataStringMapPath);

            if (optionSets.Count > 0)
            {
                foreach (string optionSet in optionSets)
                {
                    if (optionSet.GetType().Name.ToLowerInvariant() == "string")
                    {
                        HttpMessageContent getRequestContent_optionSet =
                            new HttpMessageContent(
                                new HttpRequestMessage(
                                    HttpMethod.Get,
                                    string.Format(mapPath, optionSet, optionsetEntity)));
                        getRequestContent_optionSet.Headers.Remove("Content-Type");
                        getRequestContent_optionSet.Headers.Add("Content-Type", "application/http");
                        getRequestContent_optionSet.Headers.Add("Content-Transfer-Encoding", "binary");

                        batchContent.Add(getRequestContent_optionSet);
                    }
                }
            }

            if (!string.IsNullOrEmpty(entityRef))
            {
                HttpMessageContent getRequestContent_entityRefs =
                    new HttpMessageContent(
                        new HttpRequestMessage(
                            HttpMethod.Get,
                            string.Format(mapPath, entityRef, "importmap")));
                getRequestContent_entityRefs.Headers.Remove("Content-Type");
                getRequestContent_entityRefs.Headers.Add("Content-Type", "application/http");
                getRequestContent_entityRefs.Headers.Add("Content-Transfer-Encoding", "binary");

                batchContent.Add(getRequestContent_entityRefs);
            }

            request.Content = batchContent;
            var response = httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;

            if (!response.IsSuccessStatusCode)
            {
                info("gorStr - Call Failed");
                info("gorStr - " + response.StatusCode.ToString());
                info("gorStr - " + response.ReasonPhrase);
                info("gorStr - " + response.RequestMessage.Method.ToString());

                return string.Empty;
            }
            else
            {
                var responseContent = response.Content;
                var responseStream = responseContent.ReadAsStreamAsync().Result;
                var responseString = string.Empty;

                using (var uncompressed = new MemoryStream())
                {
                    using (var compressed = new GZipStream(responseStream, CompressionMode.Decompress))
                    {
                        byte[] bytes = new byte[4096];

                        int cnt;

                        while ((cnt = compressed.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            uncompressed.Write(bytes, 0, cnt);
                        }

                        responseString = Encoding.UTF8.GetString(uncompressed.ToArray());
                    }
                }


                // rework to include optionset tables 
                string result = null;
                string[] splitter = { "odata.context" };
                string[] splitResponse = responseString.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                for (int indx = 1; indx < splitResponse.Length; indx++)
                {
                    string[] subsplitter = { "--batchresponse" };
                    string[] subsplitResponse = splitResponse[indx].Split(subsplitter, StringSplitOptions.RemoveEmptyEntries);
                    string curBatchRes = "{" + 
                        subsplitResponse[0].Substring(
                            subsplitResponse[0].IndexOf("\"value\":[", StringComparison.OrdinalIgnoreCase)).Replace(@"@odata.", "odata");
                    XmlNode returnedXML = JsonConvert.DeserializeXmlNode(curBatchRes, "value");
                    if (indx == 1)
                    {
                        if (returnedXML.FirstChild.FirstChild.ChildNodes.Count < 2)
                            result = returnedXML.FirstChild.FirstChild.FirstChild.FirstChild.InnerText;
                        else
                            result = returnedXML.FirstChild.FirstChild.ChildNodes[1].InnerText;
                    }
                    else
                    {
                        XmlNodeList nodeList = returnedXML.FirstChild.ChildNodes;
                        Dictionary<string, string> kvpDictionary = new Dictionary<string, string>();

                        foreach (XmlNode node in nodeList)
                        {
                            if ((optionSets.Count > indx - 2))
                                kvpDictionary.Add(node.ChildNodes[6].InnerText, node.ChildNodes[1].InnerText);
                            else if (kvpDictionary.Where(curKey => curKey.Key.Equals(node.ChildNodes[1].InnerText)).Count() < 1)
                                kvpDictionary.Add(node.ChildNodes[1].InnerText, node.ChildNodes[6].InnerText);
                        }
                        if ((optionSets.Count > indx - 2))
                            optionSets[indx - 2] = JsonConvert.SerializeObject(kvpDictionary);
                        else
                            entityRef = JsonConvert.SerializeObject(kvpDictionary);
                    }
                }

                return result;
            }
        }
    }

    public string CreateODataFetchXml(string xml, string cookie, int page, int count)
    {
        StringReader stringReader = new StringReader(xml);
        XmlTextReader reader = new XmlTextReader(stringReader);

        // Load document
        XmlDocument doc = new XmlDocument();
        doc.Load(reader);

        return CreateODataFetchXml(doc, cookie, page, count);
    }

    public string CreateODataFetchXml(XmlDocument doc, string cookie, int page, int count)
    {
        XmlAttributeCollection attrs = doc.DocumentElement.Attributes;

        XmlAttribute pageAttr = doc.CreateAttribute("page");
        pageAttr.Value = System.Convert.ToString(page);
        attrs.Append(pageAttr);

        XmlAttribute countAttr = doc.CreateAttribute("count");
        countAttr.Value = System.Convert.ToString(count);
        attrs.Append(countAttr);

        StringBuilder sb = new StringBuilder(1024);
        StringWriter stringWriter = new StringWriter(sb);

        XmlTextWriter writer = new XmlTextWriter(stringWriter);
        doc.WriteTo(writer);
        writer.Close();

        return sb.ToString();
    }

    public string GetODataResultSetAsString(string oDataQuery, string oDataEntity, string oauthToken)
    {
        using (HttpClient httpClient = new HttpClient())
        {
            //ODataBatchHandler odbh = new DefaultODataBatchHandler(GlobalConfiguration.DefaultServer);
            //odbh.MessageQuotas.MaxOperationsPerChangeset = 10;
            //odbh.MessageQuotas.MaxPartsPerBatch = 10;

            httpClient.Timeout = new TimeSpan(0, 2, 0);
            httpClient.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
            httpClient.DefaultRequestHeaders.Add("OData-Version", "4.0");
            httpClient.DefaultRequestHeaders.Add("Accept-Charset", "utf-8");
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");

            StringBuilder sbResp = new StringBuilder();
            int fetchCount = 5000;
            int pageNumber = 1;
            string pagingCookie = null;

            var batchUrl = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataBatchPath);
            var xmlUrl = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataFetchXMLPath);
            while (true)
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, batchUrl);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", oauthToken);
                request.Headers.AcceptCharset.Add(StringWithQualityHeaderValue.Parse("utf-8"));
                request.Headers.Add("OData-MaxVersion", "4.0");
                request.Headers.Add("OData-Version", "4.0");
                request.Headers.Add("Accept-Encoding", "gzip, deflate");
                request.Headers.Add("Accept", "application/json;odata.metadata=minimal");

                string xml = CreateODataFetchXml(oDataQuery, pagingCookie, pageNumber, fetchCount);

                MultipartContent batchContent = new MultipartContent("mixed", "batch_" + Guid.NewGuid().ToString());

                HttpMessageContent getRequestContent_1 =
                    new HttpMessageContent(
                        new HttpRequestMessage(
                            HttpMethod.Get,
                            string.Format(xmlUrl, oDataEntity, xml)));
                getRequestContent_1.Headers.Remove("Content-Type");
                getRequestContent_1.Headers.Add("Content-Type", "application/http");
                getRequestContent_1.Headers.Add("Content-Transfer-Encoding", "binary");

                batchContent.Add(getRequestContent_1);

                request.Content = batchContent;
                HttpResponseMessage response = httpClient.SendAsync(request, 
                    HttpCompletionOption.ResponseContentRead).Result;

                if (!response.IsSuccessStatusCode)
                {
                    info("gorSet - Call Failed");
                    info("gorSet - " + response.StatusCode.ToString());
                    info("gorSet - " + response.ReasonPhrase);
                    info("gorSet - " + response.RequestMessage.Method.ToString());

                    return string.Empty;
                }
                else
                {
                    var responseContent = response.Content;
                    var responseStream = responseContent.ReadAsStreamAsync().Result;
                    var responseString =    string.Empty;

                    using (var uncompressed = new MemoryStream())
                    {
                        using (var compressed = new GZipStream(responseStream, CompressionMode.Decompress))
                        {
                            byte[] bytes = new byte[4096];

                            int cnt;

                            while ((cnt = compressed.Read(bytes, 0, bytes.Length)) != 0)
                            {
                                uncompressed.Write(bytes, 0, cnt);
                            }

                            responseString = Encoding.UTF8.GetString(uncompressed.ToArray());
                        }
                    }

                    int frontMarker;
                    string frontLookup = "@odata.context";

                    int backMarker;
                    frontMarker = responseString.IndexOf(frontLookup, StringComparison.OrdinalIgnoreCase);

                    string frontcutResponse = responseString.Remove(0, (frontMarker - 2)); // should cut the front
                    backMarker = frontcutResponse.IndexOf("--batchresponse", StringComparison.OrdinalIgnoreCase) - 2;
                    string fullcutResponse = frontcutResponse.Remove(backMarker, ((frontcutResponse.Length - 1) - backMarker)); // should cut the back
                    fullcutResponse = fullcutResponse.Replace(@"@odata.", "odata");
                    JsonResponse jresp = JsonConvert.DeserializeObject<JsonResponse>(fullcutResponse);
                    sbResp.Append(JsonConvert.SerializeObject(jresp.value));

                    if (responseString.Contains("<cookie pagenumber"))
                    {
                        pageNumber++;
                        string pagingCookieSub = responseString.Substring(responseString.IndexOf("pagingcookie=\\\"") + 15);
                        pagingCookieSub = pagingCookieSub.Substring(0, pagingCookieSub.IndexOf("\\\""));
                        pagingCookie = pagingCookieSub;
                    }
                    else
                        break;
                }
            }
            return sbResp.ToString().Replace("][", ",");
        }
    }

    public object ODataRetrieveFieldFromEntityByKey(string entity, string idfield, object recid, 
        string lookupfield, string odataentity, string oauthToken)
    {
        string result = GetODataResultString(
            $"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'><entity name='{entity}'><attribute name='{lookupfield}' /><filter type='and'><condition attribute='{idfield}' operator='eq' value='{recid}' /></filter></entity></fetch>",
            odataentity, oauthToken);

        return result;
    }

    public JsonWhoAmI GetJsonWhoAmI(string oauthToken)
    {
        var batchpath = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataBatchPath);
        var whoamiPath = string.Concat(crmConfig.CRMURL, crmConfig.CRMODataWhoAmIPath);
        using (HttpClient httpClient = new HttpClient())
        {
            //ODataBatchHandler odbh = new DefaultODataBatchHandler(GlobalConfiguration.DefaultServer);
            //odbh.MessageQuotas.MaxOperationsPerChangeset = 10;
            //odbh.MessageQuotas.MaxPartsPerBatch = 10;

            httpClient.Timeout = new TimeSpan(0, 2, 0);
            httpClient.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
            httpClient.DefaultRequestHeaders.Add("OData-Version", "4.0");
            httpClient.DefaultRequestHeaders.Add("Accept-Charset", "utf-8");
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, batchpath);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", oauthToken);
            request.Headers.AcceptCharset.Add(StringWithQualityHeaderValue.Parse("utf-8"));
            request.Headers.Add("OData-MaxVersion", "4.0");
            request.Headers.Add("OData-Version", "4.0");
            request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.Headers.Add("Accept", "application/json;odata.metadata=full");

            MultipartContent batchContent = new MultipartContent("mixed", "batch_" + Guid.NewGuid().ToString());

            HttpMessageContent getRequestContent_FetchXmlLookup =
                new HttpMessageContent(
                    new HttpRequestMessage(
                        HttpMethod.Get,
                        whoamiPath));
            getRequestContent_FetchXmlLookup.Headers.Remove("Content-Type");
            getRequestContent_FetchXmlLookup.Headers.Add("Content-Type", "application/http");
            getRequestContent_FetchXmlLookup.Headers.Add("Content-Transfer-Encoding", "binary");

            batchContent.Add(getRequestContent_FetchXmlLookup);
            request.Content = batchContent;
            var response = httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;

            if (!response.IsSuccessStatusCode)
            {
                info("gorWhoAmI - Call Failed");
                info("gorWhoAmI - " + response.StatusCode.ToString());
                info("gorWhoAmI - " + response.ReasonPhrase);
                info("gorWhoAmI - " + response.RequestMessage.Method.ToString());

                return null;
            }
            else
            {
                var responseContent = response.Content;
                var responseStream = responseContent.ReadAsStreamAsync().Result;
                var responseString = string.Empty;

                using (var uncompressed = new MemoryStream())
                {
                    using (var compressed = new GZipStream(responseStream, CompressionMode.Decompress))
                    {
                        byte[] bytes = new byte[4096];

                        int cnt;

                        while ((cnt = compressed.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            uncompressed.Write(bytes, 0, cnt);
                        }

                        responseString = Encoding.UTF8.GetString(uncompressed.ToArray());
                    }
                }


                // rework to include optionset tables 
                JsonWhoAmI result;
                string[] splitter = { "{\"@odata.context" };
                string[] splitResponse = responseString.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                string[] subsplitter = { "--batchresponse" };
                string[] subsplitResponse = splitResponse[1].Split(subsplitter, StringSplitOptions.RemoveEmptyEntries);
                string strResult = "{\"@odata.context" + subsplitResponse[0];
                strResult.Replace("@", string.Empty).Replace("odata.", "odata");
                result = JsonConvert.DeserializeObject<JsonWhoAmI>(strResult);

                result.UserName = (string)ODataRetrieveFieldFromEntityByKey(Entities.SystemUserLogicalName, "systemuserid", result.UserId, Attributes.fullName, "systemusers", oauthToken);
                result.UserEmail = (string)ODataRetrieveFieldFromEntityByKey(Entities.SystemUserLogicalName, "systemuserid", result.UserId, Attributes.internalEmailAddress, "systemusers", oauthToken);

                return result;
            }
        }
    }

    public string GetEntityFetchXML(string odataEntity, string oauthToken, ref List<string> optionSets, string optionSetEntity)
    {
        string entityRef = null;

        return GetEntityFetchXML(odataEntity, oauthToken, ref optionSets, optionSetEntity, ref entityRef);
    }
    public string GetEntityFetchXML(string odataEntity, string oauthToken, ref List<string> optionSets, string optionSetEntity, ref string entityRef)
    {
        var configPath = string.Concat("QueryConfig:", odataEntity.ToUpper(), Constants.queryId);
        string fetchXML = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'><entity name='savedquery'><attribute name='fetchxml' /><attribute name='querytype' /><attribute name='savedqueryid' /><filter type='and'><condition attribute='savedqueryid' operator='eq' value='" + 
            config[configPath] + @"' /></filter><order attribute='name' descending='true' /></entity></fetch>";

        string result;
        try
        {
            result = GetODataResultString(fetchXML, "savedqueries", oauthToken, ref optionSets, ref entityRef, optionSetEntity);
        }
        catch (Exception ex)
        {
            info("GetEntityFetchXML - error getting results");
            info(ex.Message);
            info(ex.Source);
            info(ex.StackTrace);
            Exception newexc = ex;
            while (newexc.InnerException != null)
            {
                newexc = newexc.InnerException;
                info("gorSet - inner exception");
                info(newexc.Message);
                info(newexc.Source);
                info(newexc.StackTrace);
            }
            throw ex;
        }

        return result;
    }

    protected IConfiguration config;
    protected CrmConfig crmConfig;
    public ODataReadHandler(ILogger<ODataReadHandler> logger, IConfiguration configuration): base(logger) 
    { 
        crmConfig = CrmConfig.Construct(configuration);
    }
}
