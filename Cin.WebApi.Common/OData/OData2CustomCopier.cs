﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Newtonsoft.Json;
using System.Reflection;

namespace Cin.WebApi.OData;
public class OData2CustomCopier<TDynamicsModel, TCustomModel>
    where TDynamicsModel : class
    where TCustomModel : class
{
    public void GenerateSubTemplates(ref TCustomModel subTemplate)
    {
        PropertyInfo[] subTemplateProperties = subTemplate.GetType().GetProperties();
        foreach (PropertyInfo subTemplateProperty in subTemplateProperties)
        {
            string subTemplateName = subTemplateProperty.Name.ToLowerInvariant();
            string subTemplateTypeName = subTemplateProperty.PropertyType.Name.ToLowerInvariant();
            string subTemplateBaseTypeName = subTemplateProperty.PropertyType.BaseType.Name.ToLowerInvariant();

            if (!"object|valuetype".Contains(subTemplateBaseTypeName))
            {
                if (subTemplateBaseTypeName != "guid")
                {
                    var assemblyName = subTemplateProperty.PropertyType.Namespace;
                    var typeFullName = subTemplateProperty.PropertyType.FullName;
                    var code = $"namespace GeneratedScript {{using {assemblyName}; public class RunScript {{public static {typeFullName} Eval() {{return new {typeFullName}();}}}}}}";

                    SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(code);
                    var references = AppDomain.CurrentDomain.GetAssemblies()
                                        .Where(a => !a.IsDynamic)
                                        .Select(a => a.Location)
                                        .Where(s => !string.IsNullOrEmpty(s))
                                        .Where(s => !s.Contains("xunit"))
                                        .Select(s => MetadataReference.CreateFromFile(s)).ToList();

                    CSharpCompilation compilation = CSharpCompilation.Create(assemblyName, new[] { syntaxTree }, references, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
                    using (var memoryStream = new MemoryStream())
                    {
                        var result = compilation.Emit(memoryStream);

                        if (result.Success)
                        {
                            memoryStream.Seek(0, SeekOrigin.Begin);
                            Assembly assembly = Assembly.Load(memoryStream.ToArray());

                            var testClassType = assembly.GetType("GeneratedScript.RunScript");
                            var newSubObj = testClassType.GetMethod("Eval").Invoke(null, null);
                            subTemplateProperty.SetValue(subTemplate, newSubObj);
                        }
                    }
                }
            }
        }
    }

    public TCustomModel Clone(TCustomModel original)
    {
        return JsonConvert.DeserializeObject<TCustomModel>(JsonConvert.SerializeObject(original));
    }

    public void Copy(TDynamicsModel parent, ref TCustomModel child, TCustomModel subObjRef = null, List<OptionSetTable> optionSetList = null, Dictionary<string, string> entityRefTable = null, Dictionary<string, string> entityXRef = null)
    {
        PropertyInfo[] parentProperties = parent.GetType().GetProperties();
        //PropertyInfo[] childProperties = child.GetType().GetProperties();

        bool bProcessed = false;

        PropertyInfo[] childProperties = child.GetType().GetProperties();

        foreach (PropertyInfo parentProperty in parentProperties)
        {
            bProcessed = false;
            if (parentProperty.GetValue(parent) == null) continue;
            object parentValue = parentProperty.GetValue(parent);

            string parentName = parentProperty.Name.ToLowerInvariant();
            string parentTypeName = parentProperty.PropertyType.Name.ToLowerInvariant();


            foreach (PropertyInfo childProperty in childProperties.Where(curChild => parentName.Contains(curChild.Name.ToLowerInvariant())))
            {
                string childName = childProperty.Name.ToLowerInvariant();
                string childTypeName = childProperty.PropertyType.Name.ToLowerInvariant();
                string childBaseTypeName = childProperty.PropertyType.BaseType.Name.ToLowerInvariant();

                if (parentName == childName)
                    if (parentTypeName == childTypeName)
                    {
                        bProcessed = true;
                        childProperty.SetValue(child, parentValue);
                    }
                    else if (parentTypeName != "string" && childTypeName == "string")
                        switch (parentTypeName)
                        {
                            case "double":
                                bProcessed = true;
                                childProperty.SetValue(child, ((double)parentValue).ToString());
                                break;
                            case "float":
                                bProcessed = true;
                                childProperty.SetValue(child, ((float)parentValue).ToString());
                                break;
                            case "int":
                            case "int32":
                                bProcessed = true;
                                childProperty.SetValue(child, ((int)parentValue).ToString());
                                break;
                            case "long":
                            case "int64":
                                bProcessed = true;
                                childProperty.SetValue(child, ((long)parentValue).ToString());
                                break;
                            default:
                                break;
                        }
                    else
                    {
                        if (childProperty.GetValue(child) == null && subObjRef != null)
                            if (!"object|valuetype".Contains(childBaseTypeName))
                            {
                                if (childBaseTypeName != "guid")
                                {
                                    var newSubObj = child.GetType().GetProperties().Where(curChild => parentName.Contains(curChild.Name.ToLowerInvariant())).First();
                                    var newSubVal = newSubObj.GetValue(subObjRef);
                                    childProperty.SetValue(child, newSubVal);
                                }
                            }
                        //ChatakonduV E0143633 added below line
                        string childObjName = !"object|valuetype".Contains(childBaseTypeName) ? childBaseTypeName : childTypeName;
                        switch (!"object|valuetype".Contains(childBaseTypeName) ? childBaseTypeName : childTypeName)
                        {
                            case "dynamicsoptionsetref":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        if (subProp.Name.ToLowerInvariant() == "formattedvalue" || subProp.Name.ToLowerInvariant() == "text")
                                            subProp.SetValue(subObj, parentValue.ToString());
                                        if (subProp.Name.ToLowerInvariant() == "name")
                                        {
                                            if (optionSetList != null && optionSetList.Count > 0)
                                            {
                                                string sValue = "";
                                                try
                                                {
                                                    sValue = optionSetList.Where(l => l.Key == childName).First().Value.Where(s => s.Key == parentValue.ToString()).First().Value;
                                                    subProp.SetValue(subObj, sValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    subProp.SetValue(subObj, null);
                                                }
                                            }
                                        }
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "dynamicsoptionsetrefboolean":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    int parentValueInt = 0;
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        if (parentValue.ToString().ToLowerInvariant() == "true")
                                        {
                                            parentValue = "True";
                                            parentValueInt = 1;
                                        }
                                        else
                                            parentValue = "False";

                                        if (subProp.Name.ToLowerInvariant() == "text")
                                            subProp.SetValue(subObj, parentValue.ToString());

                                        if (subProp.Name.ToLowerInvariant() == "name")
                                        {
                                            if (optionSetList != null && optionSetList.Count > 0)
                                            {
                                                string sValue = "";
                                                try
                                                {
                                                    sValue = optionSetList.Where(l => l.Key == childName).First().Value.Where(s => s.Key == parentValueInt.ToString()).First().Value;
                                                    subProp.SetValue(subObj, sValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    subProp.SetValue(subObj, null);
                                                }
                                            }
                                        }
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            //ChatakonduV E0143633 Added new case not to pouplate formatttedvalue and just populate text, name properties
                            case "dynamicsoptionsetrefnoname":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        if (subProp.Name.ToLowerInvariant() == "text")
                                            subProp.SetValue(subObj, parentValue.ToString());
                                        if (subProp.Name.ToLowerInvariant() == "name")
                                        {
                                            if (optionSetList != null && optionSetList.Count > 0)
                                            {
                                                string sValue = optionSetList.Where(l => l.Key == childName).First().Value.Where(s => s.Key == parentValue.ToString()).First().Value;
                                                subProp.SetValue(subObj, sValue);
                                            }
                                        }
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "dynamicsformattedvalueonly": //ChatakonduV E0143633
                            case "dynamicsformattedvalue":
                                //int intParentValue = 0;
                                //Int32.TryParse(parentValue.ToString(), out intParentValue);
                                if (parentValue != null)// && intParentValue != 0)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        if (childObjName == "dynamicsformattedvalueonly" && subProp.Name.ToLowerInvariant() == "text")
                                            subProp.SetValue(subObj, parentValue.ToString() == "0.0" ? "0" : Double.Parse((string)parentValue).ToString("0.##"));

                                        if (childObjName == "dynamicsformattedvalue" && (subProp.Name.ToLowerInvariant() == "formattedvalue" || subProp.Name.ToLowerInvariant() == "text"))
                                            subProp.SetValue(subObj, parentValue.ToString());
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            //ChatakonduV E0143633 Added new case to format numeric number with commas in thousands place
                            case "dynamicsformattednumericvalue":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        if (subProp.Name.ToLowerInvariant() == "formattedvalue")
                                            subProp.SetValue(subObj, string.Format("{0:n0}", parentValue));
                                        if (subProp.Name.ToLowerInvariant() == "text")
                                            subProp.SetValue(subObj, parentValue.ToString());
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "guid":
                                bProcessed = true;
                                childProperty.SetValue(child, new Guid((string)parentValue));
                                break;
                            case "dynamicsbooleanvalue":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        if (subProp.Name.ToLowerInvariant() == "name")
                                            subProp.SetValue(subObj, Boolean.Parse(parentValue.ToString()) ? "Yes" : "No");
                                        else if (subProp.Name.ToLowerInvariant() == "text")
                                            subProp.SetValue(subObj, Boolean.Parse(parentValue.ToString()) ? "1" : "0");
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "dynamicscurrencyvalue":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        if (subProp.Name.ToLowerInvariant() == "name")
                                            subProp.SetValue(subObj, Double.Parse((string)parentValue).ToString("C2"));
                                        else if (subProp.Name.ToLowerInvariant() == "formattedvalue")
                                            subProp.SetValue(subObj, Double.Parse((string)parentValue).ToString("C2"));
                                        else if (subProp.Name.ToLowerInvariant() == "text")
                                            //ChatakonduV E0143633 modified below line
                                            subProp.SetValue(subObj, parentValue.ToString() == "0.0" ? "0" : Double.Parse((string)parentValue).ToString("0.##"));
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "dynamicsintvalue":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        if (subProp.Name.ToLowerInvariant() == "name")
                                            subProp.SetValue(subObj, Int32.Parse((string)parentValue).ToString());
                                        else if (subProp.Name.ToLowerInvariant() == "text")
                                            subProp.SetValue(subObj, parentValue.ToString());
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "dynamicsdatetimegmtvalue":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        switch (subProp.Name.ToLowerInvariant())
                                        {
                                            case "text":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToString() + " +0:00");
                                                break;
                                        }
                                    }
                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "dynamicsdatetimelocalvalue":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        switch (subProp.Name.ToLowerInvariant())
                                        {
                                            case "text":
                                                string timeoffset = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time").GetUtcOffset(DateTime.Parse(parentValue.ToString())).ToString();
                                                var dateObj = DateTime.Parse(parentValue.ToString()).ToLocalTime().ToString("yyyy-MM-ddTHH:mm:ss")
                                                    + timeoffset.Substring(0, timeoffset.Length - 3);

                                                subProp.SetValue(subObj, dateObj);
                                                break;
                                            case "date":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToLocalTime().ToShortDateString());
                                                break;
                                            case "time":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToLocalTime().ToShortTimeString());
                                                break;
                                        }
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "dynamicsdatetimeutcvalue":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        switch (subProp.Name.ToLowerInvariant())
                                        {
                                            case "text":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToUniversalTime().ToString());
                                                break;
                                            case "date":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToUniversalTime().ToShortDateString());
                                                break;
                                            case "time":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToUniversalTime().ToShortTimeString());
                                                break;
                                        }
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                            case "dynamicsdatetimevalue":
                                if (parentValue != null)
                                {
                                    var subObj = childProperty.GetValue(child);
                                    PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                    foreach (var subProp in subProps)
                                    {
                                        switch (subProp.Name.ToLowerInvariant())
                                        {
                                            case "text":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToString());
                                                break;
                                            case "date":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToShortDateString());
                                                break;
                                            case "time":
                                                subProp.SetValue(subObj, DateTime.Parse(parentValue.ToString()).ToShortTimeString());
                                                break;
                                        }
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }
                                break;
                        }
                    }
                else
                {
                    if (childProperty.GetValue(child) == null && subObjRef != null)
                        if (!"object|valuetype".Contains(childBaseTypeName))
                        {
                            //ChatakonduV E0143633 modified below line
                            if (childBaseTypeName == "dynamicsentityreference" || childBaseTypeName == "dynamicsentitynullreference")
                            {
                                var newSubObj = child.GetType().GetProperties().Where(curChild => childName == curChild.Name.ToLowerInvariant()).First();
                                childProperty.SetValue(child, newSubObj.GetValue(subObjRef));
                            }
                        }
                    //ChatakonduV E0143633 added below line
                    string childObjName = !"object|valuetype".Contains(childBaseTypeName) ? childBaseTypeName : childTypeName;
                    switch (!"object|valuetype".Contains(childBaseTypeName) ? childBaseTypeName : childTypeName)
                    {
                        //ChatakonduV E0143633 added below line
                        case "dynamicsentitynullreference":
                        case "dynamicsvariantentityreference":
                        case "dynamicsentityreference":
                            if (parentValue != null)
                            {
                                var subObj = childProperty.GetValue(child);
                                PropertyInfo[] subProps = subObj.GetType().GetProperties();
                                foreach (var subProp in subProps)
                                {
                                    if (subProp.Name.ToLowerInvariant() == "text" && parentProperty.Name.ToLowerInvariant().Contains(childProperty.Name.ToLowerInvariant() + "_value"))
                                        subProp.SetValue(subObj, parentValue.ToString());
                                    else if (subProp.Name.ToLowerInvariant() == "name" && !parentProperty.Name.ToLowerInvariant().Contains(childProperty.Name.ToLowerInvariant() + "_value"))
                                        subProp.SetValue(subObj, parentValue.ToString());
                                    //ChatakonduV E0143633 added below 2 conditions
                                    else if (subProp.Name.ToLowerInvariant() == "dsc" && !parentProperty.Name.ToLowerInvariant().Contains(childProperty.Name.ToLowerInvariant() + "_value"))
                                    {
                                        if (childObjName == "dynamicsentityreference")
                                            subProp.SetValue(subObj, "");
                                        else
                                            subProp.SetValue(subObj, null);
                                    }
                                    else if (subProp.Name.ToLowerInvariant() == "yomi" && !parentProperty.Name.ToLowerInvariant().Contains(childProperty.Name.ToLowerInvariant() + "_value"))
                                    {
                                        if (childObjName == "dynamicsentityreference")
                                            subProp.SetValue(subObj, parentValue.ToString());
                                        else
                                            subProp.SetValue(subObj, null);
                                    }
                                    //ChatakonduV E0143633 end conditions
                                    else if (subProp.Name.ToLowerInvariant() == "type")
                                    {
                                        if (childObjName == "dynamicsvariantentityreference")
                                        {

                                        }
                                        else if (childObjName == "dynamicsentitynullreference")
                                            subProp.SetValue(subObj, null);
                                        else if (entityRefTable != null && entityXRef != null)
                                        {
                                            string sValue = "";
                                            try
                                            {
                                                sValue = entityRefTable.Where(l => l.Key == entityXRef.Where(k => k.Key.ToLowerInvariant() == childProperty.Name.ToLowerInvariant()).First().Value).First().Value;
                                                subProp.SetValue(subObj, sValue);
                                            }
                                            catch (Exception ex)
                                            {
                                                subProp.SetValue(subObj, null);
                                            }
                                        }
                                    }

                                    bProcessed = true;
                                    childProperty.SetValue(child, subObj);
                                }

                                bProcessed = true;
                                childProperty.SetValue(child, subObj);
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (bProcessed) break;
            }
        }
    }
}
